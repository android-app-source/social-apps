.class public Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/31w;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Lcom/facebook/resources/ui/FbTextView;

.field public e:LX/BJX;

.field public f:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

.field public g:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

.field public h:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

.field private i:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

.field public j:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;

.field public k:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;

.field public l:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;",
            "Lcom/facebook/base/fragment/FbFragment;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;",
            "LX/2rw;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/BJV;

.field public o:LX/BJW;

.field public p:LX/BJo;

.field public q:LX/BKO;

.field public r:I

.field private s:Landroid/support/v4/view/ViewPager;

.field public t:LX/BJU;

.field public u:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1776345
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1776346
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;

    const/16 v1, 0x12cb

    invoke-static {v2, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v2}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v1

    check-cast v1, LX/0if;

    invoke-static {v2}, LX/31w;->b(LX/0QB;)LX/31w;

    move-result-object v2

    check-cast v2, LX/31w;

    iput-object p0, p1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->a:LX/0Or;

    iput-object v1, p1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->b:LX/0if;

    iput-object v2, p1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->c:LX/31w;

    return-void
.end method

.method public static a(LX/2rw;)Z
    .locals 1

    .prologue
    .line 1776344
    sget-object v0, LX/2rw;->UNDIRECTED:LX/2rw;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 11

    .prologue
    .line 1776298
    new-instance v0, LX/BLh;

    invoke-direct {v0, p0}, LX/BLh;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;)V

    move-object v1, v0

    .line 1776299
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v0

    .line 1776300
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1776301
    iget-object v2, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->f:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    invoke-virtual {v2, v0}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->setProfilePictureUrl(Landroid/net/Uri;)V

    .line 1776302
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->f:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    invoke-virtual {v0, v1}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1776303
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->g:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    invoke-virtual {v0, v1}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1776304
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->h:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    invoke-virtual {v0, v1}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1776305
    iget-object v3, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->f:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    sget-object v4, LX/2rw;->UNDIRECTED:LX/2rw;

    iget-object v5, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->g:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    sget-object v6, LX/2rw;->USER:LX/2rw;

    iget-object v7, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->h:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    sget-object v8, LX/2rw;->GROUP:LX/2rw;

    invoke-static/range {v3 .. v8}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->m:LX/0P1;

    .line 1776306
    const/4 v9, 0x0

    .line 1776307
    iget-object v3, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->f:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->l(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;)Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->g:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    .line 1776308
    iget-object v6, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->j:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;

    if-nez v6, :cond_0

    .line 1776309
    iget-object v6, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v6, v6

    .line 1776310
    invoke-static {v6}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->b(Landroid/os/Bundle;)Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;

    move-result-object v6

    iput-object v6, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->j:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;

    .line 1776311
    iget-object v6, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->j:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;

    iget-object v7, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->t:LX/BJU;

    .line 1776312
    iput-object v7, v6, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->p:LX/BJU;

    .line 1776313
    iget-object v6, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->j:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;

    iget-object v7, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->e:LX/BJX;

    .line 1776314
    iput-object v7, v6, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->j:LX/BJX;

    .line 1776315
    :cond_0
    iget-object v6, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->j:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;

    move-object v6, v6

    .line 1776316
    iget-object v7, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->h:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    .line 1776317
    iget-object v8, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->k:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;

    if-nez v8, :cond_1

    .line 1776318
    iget-object v8, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v8, v8

    .line 1776319
    invoke-static {v8}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->b(Landroid/os/Bundle;)Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;

    move-result-object v8

    iput-object v8, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->k:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;

    .line 1776320
    iget-object v8, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->k:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;

    iget-object v10, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->t:LX/BJU;

    .line 1776321
    iput-object v10, v8, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->m:LX/BJU;

    .line 1776322
    iget-object v8, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->k:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;

    iget-object v10, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->e:LX/BJX;

    .line 1776323
    iput-object v10, v8, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->j:LX/BJX;

    .line 1776324
    :cond_1
    iget-object v8, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->k:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;

    move-object v8, v8

    .line 1776325
    invoke-static/range {v3 .. v8}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->l:LX/0P1;

    .line 1776326
    iget-object v3, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->p:LX/BJo;

    invoke-virtual {v3}, LX/BJo;->b()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1776327
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v5

    .line 1776328
    iget-object v3, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->m:LX/0P1;

    invoke-virtual {v3}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v3

    invoke-virtual {v3}, LX/0Py;->asList()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    :goto_0
    if-ge v9, v7, :cond_3

    invoke-virtual {v6, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    .line 1776329
    iget-object v8, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->c:LX/31w;

    iget-object v4, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->m:LX/0P1;

    invoke-virtual {v4, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2rw;

    invoke-virtual {v8, v4}, LX/31w;->a(LX/2rw;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1776330
    iget-object v4, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->l:LX/0P1;

    invoke-virtual {v4, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v5, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1776331
    :cond_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 1776332
    :cond_3
    invoke-virtual {v5}, LX/0P2;->b()LX/0P1;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->l:LX/0P1;

    .line 1776333
    :cond_4
    :goto_1
    new-instance v0, LX/BLf;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/BLf;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;LX/0gc;)V

    .line 1776334
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->s:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 1776335
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->s:Landroid/support/v4/view/ViewPager;

    new-instance v1, LX/BLg;

    invoke-direct {v1, p0}, LX/BLg;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 1776336
    return-void

    .line 1776337
    :cond_5
    iget-object v3, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->p:LX/BJo;

    invoke-virtual {v3}, LX/BJo;->c()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1776338
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v6

    .line 1776339
    iget-object v3, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->m:LX/0P1;

    invoke-virtual {v3}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v3

    invoke-virtual {v3}, LX/0Py;->asList()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v5, v9

    :goto_2
    if-ge v5, v8, :cond_7

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    .line 1776340
    iget-object v4, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->m:LX/0P1;

    invoke-virtual {v4, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2rw;

    invoke-static {v4}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->a(LX/2rw;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1776341
    iget-object v4, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->l:LX/0P1;

    invoke-virtual {v4, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v6, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1776342
    :cond_6
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_2

    .line 1776343
    :cond_7
    invoke-virtual {v6}, LX/0P2;->b()LX/0P1;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->l:LX/0P1;

    goto :goto_1
.end method

.method private k()V
    .locals 8

    .prologue
    .line 1776276
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->p:LX/BJo;

    .line 1776277
    iget-object v1, v0, LX/BJo;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v1, v1, LX/BKm;->e:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-object v0, v1

    .line 1776278
    iget-object v0, v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    .line 1776279
    sget-object v1, LX/BLi;->a:[I

    invoke-virtual {v0}, LX/2rw;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1776280
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected target type \""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\"."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1776281
    :pswitch_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->r:I

    .line 1776282
    :goto_0
    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->r(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;)V

    .line 1776283
    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->q(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;)V

    .line 1776284
    const/16 v7, 0x8

    const/4 v2, 0x0

    .line 1776285
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->p:LX/BJo;

    invoke-virtual {v0}, LX/BJo;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1776286
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->m:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_1
    if-ge v3, v5, :cond_1

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    .line 1776287
    iget-object v6, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->c:LX/31w;

    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->m:LX/0P1;

    invoke-virtual {v1, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2rw;

    invoke-virtual {v6, v1}, LX/31w;->a(LX/2rw;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1776288
    invoke-virtual {v0, v7}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->setVisibility(I)V

    .line 1776289
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 1776290
    :cond_1
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->p:LX/BJo;

    invoke-virtual {v0}, LX/BJo;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1776291
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->m:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    :goto_2
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    .line 1776292
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->m:LX/0P1;

    invoke-virtual {v1, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2rw;

    invoke-static {v1}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->a(LX/2rw;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1776293
    invoke-virtual {v0, v7}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->setVisibility(I)V

    .line 1776294
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 1776295
    :cond_3
    return-void

    .line 1776296
    :pswitch_1
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->r:I

    goto :goto_0

    .line 1776297
    :pswitch_2
    const/4 v0, 0x2

    iput v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->r:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static l(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;)Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;
    .locals 2

    .prologue
    .line 1776269
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->i:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    if-nez v0, :cond_0

    .line 1776270
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1776271
    invoke-static {v0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->b(Landroid/os/Bundle;)Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->i:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    .line 1776272
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->i:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->q:LX/BKO;

    invoke-virtual {v0, v1}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->a(LX/BKO;)V

    .line 1776273
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->i:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->e:LX/BJX;

    .line 1776274
    iput-object v1, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->n:LX/BJX;

    .line 1776275
    :cond_0
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->i:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    return-object v0
.end method

.method public static p(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;)Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;
    .locals 2

    .prologue
    .line 1776347
    iget v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->r:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->r:I

    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->l:LX/0P1;

    invoke-virtual {v1}, LX/0P1;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 1776348
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->r:I

    .line 1776349
    :cond_1
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->l:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v0

    iget v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->r:I

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    return-object v0
.end method

.method public static q(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1776262
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->l:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    .line 1776263
    invoke-virtual {v0, v1}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->setIsSelected(Z)V

    .line 1776264
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1776265
    :cond_0
    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->p(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;)Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->setIsSelected(Z)V

    .line 1776266
    iget-object v2, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->d:Lcom/facebook/resources/ui/FbTextView;

    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->f:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->p(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;)Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    move-result-object v3

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1776267
    return-void

    .line 1776268
    :cond_1
    const/16 v0, 0x8

    goto :goto_1
.end method

.method public static r(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;)V
    .locals 2

    .prologue
    .line 1776259
    iget v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->r:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1776260
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->s:Landroid/support/v4/view/ViewPager;

    iget v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->r:I

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 1776261
    :cond_0
    return-void
.end method


# virtual methods
.method public final b()Z
    .locals 2

    .prologue
    .line 1776258
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->f:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->p(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;)Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    move-result-object v1

    if-ne v0, v1, :cond_0

    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->l(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;)Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, 0x7e4173ec

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1776243
    const v0, 0x7f030fcd

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1776244
    const-class v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;

    invoke-static {v1, p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1776245
    const v1, 0x7f0d1602

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    iput-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->f:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    .line 1776246
    const v1, 0x7f0d2622

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    iput-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->g:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    .line 1776247
    const v1, 0x7f0d2623

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    iput-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->h:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    .line 1776248
    const v1, 0x7f0d0508

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/glyph/GlyphView;

    .line 1776249
    new-instance v3, LX/BLd;

    invoke-direct {v3, p0}, LX/BLd;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;)V

    invoke-virtual {v1, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1776250
    const v1, 0x7f0d14af

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    iput-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 1776251
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->d:Lcom/facebook/resources/ui/FbTextView;

    new-instance v3, LX/BLe;

    invoke-direct {v3, p0}, LX/BLe;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;)V

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1776252
    const v1, 0x7f0d15b3

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/ViewPager;

    iput-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->s:Landroid/support/v4/view/ViewPager;

    .line 1776253
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->s:Landroid/support/v4/view/ViewPager;

    iget-boolean v3, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->u:Z

    invoke-virtual {v1, v3}, Landroid/support/v4/view/ViewPager;->setSaveEnabled(Z)V

    .line 1776254
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->s:Landroid/support/v4/view/ViewPager;

    const/4 v3, 0x3

    invoke-virtual {v1, v3}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 1776255
    invoke-direct {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->c()V

    .line 1776256
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->b:LX/0if;

    sget-object v3, LX/0ig;->k:LX/0ih;

    const-string v4, "target_privacy_picker_visible"

    invoke-virtual {v1, v3, v4}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1776257
    const/16 v1, 0x2b

    const v3, 0x17074dd

    invoke-static {v5, v1, v3, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x5ee52898

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1776240
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1776241
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->s:Landroid/support/v4/view/ViewPager;

    iget-boolean v2, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->u:Z

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setSaveEnabled(Z)V

    .line 1776242
    const/16 v1, 0x2b

    const v2, -0x5b0bc9a0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x609d8171

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1776235
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1776236
    iget-boolean v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->u:Z

    if-nez v1, :cond_0

    .line 1776237
    invoke-direct {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->k()V

    .line 1776238
    :goto_0
    const v1, 0x36e98fda

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void

    .line 1776239
    :cond_0
    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->q(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;)V

    goto :goto_0
.end method
