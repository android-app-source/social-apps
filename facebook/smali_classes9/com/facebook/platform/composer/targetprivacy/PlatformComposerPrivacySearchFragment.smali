.class public Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

.field public c:LX/BKO;

.field public d:LX/BJW;

.field public e:LX/BJV;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1776175
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static b(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;)Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;
    .locals 2

    .prologue
    .line 1776176
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;->b:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    if-nez v0, :cond_0

    .line 1776177
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1776178
    invoke-static {v0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->b(Landroid/os/Bundle;)Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;->b:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    .line 1776179
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;->b:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;->c:LX/BKO;

    invoke-virtual {v0, v1}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->a(LX/BKO;)V

    .line 1776180
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;->b:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    const/4 v1, 0x1

    .line 1776181
    iput-boolean v1, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->v:Z

    .line 1776182
    :cond_0
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;->b:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1776183
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1776184
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v0

    check-cast v0, LX/0if;

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;->a:LX/0if;

    .line 1776185
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, 0x73c30c3c

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1776186
    const v0, 0x7f030fc4

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1776187
    const v1, 0x7f0d0a0a

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 1776188
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f082494

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1776189
    const v1, 0x7f0d0508

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1776190
    new-instance v3, LX/BLa;

    invoke-direct {v3, p0}, LX/BLa;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1776191
    const v1, 0x7f0d14af

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 1776192
    new-instance v3, LX/BLb;

    invoke-direct {v3, p0}, LX/BLb;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;)V

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1776193
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v1, v1

    .line 1776194
    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    .line 1776195
    const v3, 0x7f0d2613

    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;->b(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;)Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 1776196
    invoke-virtual {v1}, LX/0hH;->b()I

    .line 1776197
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;->a:LX/0if;

    sget-object v3, LX/0ig;->k:LX/0ih;

    const-string v4, "target_privacy_picker_privacy_search"

    invoke-virtual {v1, v3, v4}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1776198
    const/16 v1, 0x2b

    const v3, 0x4081d2cc

    invoke-static {v5, v1, v3, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method
