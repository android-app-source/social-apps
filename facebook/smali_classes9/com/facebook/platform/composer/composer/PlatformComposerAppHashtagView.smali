.class public Lcom/facebook/platform/composer/composer/PlatformComposerAppHashtagView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/resources/ui/FbTextView;

.field private b:Lcom/facebook/fbui/glyph/GlyphView;

.field public c:LX/BJJ;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1771761
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1771762
    invoke-direct {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerAppHashtagView;->a()V

    .line 1771763
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1771764
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1771765
    invoke-direct {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerAppHashtagView;->a()V

    .line 1771766
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1771767
    const v0, 0x7f030fb7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1771768
    const v0, 0x7f0d25f2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAppHashtagView;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 1771769
    const v0, 0x7f0d25f3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAppHashtagView;->b:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1771770
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAppHashtagView;->b:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/BJI;

    invoke-direct {v1, p0}, LX/BJI;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerAppHashtagView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1771771
    return-void
.end method


# virtual methods
.method public setAppProvidedHashtag(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1771772
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAppHashtagView;->a:Lcom/facebook/resources/ui/FbTextView;

    if-nez p1, :cond_0

    const-string p1, ""

    :cond_0
    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1771773
    return-void
.end method

.method public setListener(LX/BJJ;)V
    .locals 0

    .prologue
    .line 1771774
    iput-object p1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAppHashtagView;->c:LX/BJJ;

    .line 1771775
    return-void
.end method
