.class public Lcom/facebook/platform/composer/composer/PlatformComposerActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/BKR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1771639
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Z)Landroid/content/Intent;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1771652
    if-nez p1, :cond_0

    .line 1771653
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1771654
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/platform/composer/composer/PlatformComposerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1771655
    const/high16 v1, 0x20000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1771656
    const-string v1, "extra_composer_internal_session_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1771657
    const-string v1, "extra_composer_configuration"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1771658
    if-eqz p3, :cond_1

    .line 1771659
    const-string v1, "skip_publish"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1771660
    :cond_1
    return-object v0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/platform/composer/composer/PlatformComposerActivity;

    invoke-static {v0}, LX/BKR;->b(LX/0QB;)LX/BKR;

    move-result-object v0

    check-cast v0, LX/BKR;

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerActivity;->p:LX/BKR;

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1771647
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1771648
    invoke-static {p0, p0}, Lcom/facebook/platform/composer/composer/PlatformComposerActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1771649
    const v0, 0x7f030fb5

    invoke-virtual {p0, v0}, Lcom/facebook/platform/composer/composer/PlatformComposerActivity;->setContentView(I)V

    .line 1771650
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerActivity;->p:LX/BKR;

    const v1, 0x7f0d0a58

    invoke-virtual {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, p1, p0, v1, v2}, LX/BKR;->a(Landroid/os/Bundle;Lcom/facebook/base/activity/FbFragmentActivity;ILandroid/content/Intent;)V

    .line 1771651
    return-void
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 1771643
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerActivity;->p:LX/BKR;

    if-eqz v0, :cond_0

    .line 1771644
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerActivity;->p:LX/BKR;

    invoke-virtual {v0}, LX/BKR;->b()V

    .line 1771645
    :goto_0
    return-void

    .line 1771646
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x75084e8c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1771640
    invoke-static {p0}, LX/BKR;->a(Landroid/app/Activity;)V

    .line 1771641
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStop()V

    .line 1771642
    const/16 v1, 0x23

    const v2, 0x47192288

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
