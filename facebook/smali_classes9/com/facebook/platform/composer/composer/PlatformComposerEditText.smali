.class public Lcom/facebook/platform/composer/composer/PlatformComposerEditText;
.super Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/8oi;


# static fields
.field private static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Z

.field public final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/BJm;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Landroid/text/TextWatcher;

.field private g:LX/1ln;

.field private h:Landroid/net/Uri;

.field private i:LX/8zF;

.field private j:LX/1aX;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1aX",
            "<",
            "LX/1af;",
            ">;"
        }
    .end annotation
.end field

.field private k:Landroid/view/View;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1772395
    const-class v0, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;

    const-string v1, "platform_native_share"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 1772398
    invoke-direct {p0, p1, p2}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1772399
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->e:Ljava/util/Set;

    .line 1772400
    const-class v0, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;

    invoke-static {v0, p0}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1772401
    new-instance v0, LX/BJj;

    invoke-direct {v0, p0}, LX/BJj;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerEditText;)V

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->f:Landroid/text/TextWatcher;

    .line 1772402
    new-instance v0, LX/8zF;

    invoke-virtual {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/8zF;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->i:LX/8zF;

    .line 1772403
    new-instance v0, LX/1Uo;

    invoke-virtual {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 1772404
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->j:LX/1aX;

    .line 1772405
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->f:Landroid/text/TextWatcher;

    invoke-virtual {p0, v0}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1772406
    iput-object p0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->b:LX/8oi;

    .line 1772407
    return-void
.end method

.method private a(Landroid/widget/EditText;)V
    .locals 5

    .prologue
    .line 1772408
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->g:LX/1ln;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1772409
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->j:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->h()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {v0}, LX/1af;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    check-cast v0, Landroid/text/SpannableStringBuilder;

    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->g:LX/1ln;

    iget-object v3, p0, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->h:Landroid/net/Uri;

    invoke-virtual {p1}, Landroid/widget/EditText;->getPaint()Landroid/text/TextPaint;

    move-result-object v4

    invoke-virtual {v4}, Landroid/text/TextPaint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Paint$FontMetrics;->ascent:F

    invoke-static {v1, v0, v2, v3, v4}, LX/8zF;->a(Landroid/graphics/drawable/Drawable;Landroid/text/SpannableStringBuilder;LX/1ln;Landroid/net/Uri;F)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 1772410
    invoke-virtual {p1}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v1

    .line 1772411
    invoke-virtual {p1}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v2

    .line 1772412
    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1772413
    invoke-virtual {p1, v1, v2}, Landroid/widget/EditText;->setSelection(II)V

    .line 1772414
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v0

    check-cast v0, LX/1Ad;

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->b:LX/1Ad;

    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 1772371
    invoke-virtual {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 1772372
    new-instance v1, LX/BJk;

    invoke-direct {v1, p0, v0}, LX/BJk;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerEditText;Landroid/view/ViewTreeObserver;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1772373
    return-void
.end method

.method private d()V
    .locals 4

    .prologue
    .line 1772415
    invoke-virtual {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->isPopupShowing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->k:Landroid/view/View;

    if-nez v0, :cond_1

    .line 1772416
    :cond_0
    :goto_0
    return-void

    .line 1772417
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->getSelectionStart()I

    move-result v0

    .line 1772418
    invoke-virtual {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->getLayout()Landroid/text/Layout;

    move-result-object v1

    .line 1772419
    if-eqz v1, :cond_0

    .line 1772420
    invoke-virtual {v1, v0}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v0

    .line 1772421
    invoke-virtual {v1, v0}, Landroid/text/Layout;->getLineBaseline(I)I

    move-result v2

    .line 1772422
    invoke-virtual {v1, v0}, Landroid/text/Layout;->getLineAscent(I)I

    move-result v0

    .line 1772423
    add-int/2addr v0, v2

    .line 1772424
    invoke-virtual {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->getLineHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->getPaddingTop()I

    move-result v2

    add-int/2addr v1, v2

    .line 1772425
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v3, -0x1

    invoke-direct {v2, v3, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1772426
    iput v0, v2, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 1772427
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->k:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private getEndIndex()I
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 1772428
    invoke-virtual {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->getText()Landroid/text/Editable;

    move-result-object v5

    .line 1772429
    invoke-virtual {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v5}, Landroid/text/Editable;->length()I

    move-result v1

    const-class v2, LX/8zE;

    invoke-interface {v0, v4, v1, v2}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8zE;

    .line 1772430
    invoke-interface {v5}, Landroid/text/Editable;->length()I

    move-result v2

    .line 1772431
    array-length v6, v0

    move v3, v4

    :goto_0
    if-ge v3, v6, :cond_0

    aget-object v1, v0, v3

    .line 1772432
    invoke-interface {v5, v1}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v1

    .line 1772433
    const/4 v7, -0x1

    if-eq v1, v7, :cond_1

    if-ge v1, v2, :cond_1

    .line 1772434
    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    goto :goto_0

    .line 1772435
    :cond_0
    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public static getTextWithEntities(Lcom/facebook/platform/composer/composer/PlatformComposerEditText;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1772436
    invoke-virtual {p0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getUserText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 1772437
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-static {v2}, LX/8oj;->a(Landroid/text/Editable;)Ljava/util/List;

    move-result-object v0

    invoke-static {v1, v0, v3, v3}, LX/16z;->a(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1772438
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->getTextWithEntities(Lcom/facebook/platform/composer/composer/PlatformComposerEditText;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1772439
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BJm;

    .line 1772440
    invoke-interface {v0, v1}, LX/BJm;->b(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)V

    goto :goto_0

    .line 1772441
    :cond_0
    return-void
.end method

.method public final a(LX/1ln;)V
    .locals 0

    .prologue
    .line 1772442
    iput-object p1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->g:LX/1ln;

    .line 1772443
    invoke-direct {p0, p0}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->a(Landroid/widget/EditText;)V

    .line 1772444
    return-void
.end method

.method public final a(LX/BJm;)V
    .locals 1

    .prologue
    .line 1772396
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1772397
    return-void
.end method

.method public final a(Landroid/text/SpannableStringBuilder;)V
    .locals 3

    .prologue
    .line 1772386
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getUserText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1772387
    invoke-virtual {v0, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1772388
    invoke-virtual {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->getSelectionStart()I

    move-result v1

    .line 1772389
    invoke-virtual {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->getSelectionEnd()I

    move-result v2

    .line 1772390
    invoke-virtual {p0, v0}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1772391
    invoke-virtual {p0, v1, v2}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->setSelection(II)V

    .line 1772392
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->g:LX/1ln;

    if-eqz v0, :cond_0

    .line 1772393
    invoke-direct {p0, p0}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->a(Landroid/widget/EditText;)V

    .line 1772394
    :cond_0
    return-void
.end method

.method public final extendSelection(I)V
    .locals 1

    .prologue
    .line 1772383
    invoke-direct {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->getEndIndex()I

    move-result v0

    .line 1772384
    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-super {p0, v0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->extendSelection(I)V

    .line 1772385
    return-void
.end method

.method public getUserText()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 1772382
    invoke-virtual {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->getEndIndex()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x146474bd

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1772379
    invoke-super {p0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->onAttachedToWindow()V

    .line 1772380
    invoke-direct {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->c()V

    .line 1772381
    const/16 v1, 0x2d

    const v2, -0x120fd187

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSelectionChanged(II)V
    .locals 1

    .prologue
    .line 1772374
    invoke-super {p0, p1, p2}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->onSelectionChanged(II)V

    .line 1772375
    invoke-direct {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->getEndIndex()I

    move-result v0

    .line 1772376
    if-ltz p2, :cond_0

    if-gt p2, v0, :cond_0

    if-ltz p1, :cond_0

    if-le p1, v0, :cond_1

    .line 1772377
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->setSelection(II)V

    .line 1772378
    :cond_1
    return-void
.end method

.method public final onTextContextMenuItem(I)Z
    .locals 1

    .prologue
    .line 1772368
    const v0, 0x1020022

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->d:Z

    .line 1772369
    invoke-super {p0, p1}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->onTextContextMenuItem(I)Z

    move-result v0

    return v0

    .line 1772370
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final performFiltering(Ljava/lang/CharSequence;I)V
    .locals 0

    .prologue
    .line 1772365
    invoke-direct {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->d()V

    .line 1772366
    invoke-super {p0, p1, p2}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->performFiltering(Ljava/lang/CharSequence;I)V

    .line 1772367
    return-void
.end method

.method public setMinutiaeUri(Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 1772357
    iput-object p1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->h:Landroid/net/Uri;

    .line 1772358
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->b:LX/1Ad;

    sget-object v1, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->j:LX/1aX;

    .line 1772359
    iget-object v2, v1, LX/1aX;->f:LX/1aZ;

    move-object v1, v2

    .line 1772360
    invoke-virtual {v0, v1}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, p1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    new-instance v1, LX/BJl;

    invoke-direct {v1, p0}, LX/BJl;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerEditText;)V

    invoke-virtual {v0, v1}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1772361
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->j:LX/1aX;

    invoke-virtual {v1, v0}, LX/1aX;->a(LX/1aZ;)V

    .line 1772362
    invoke-virtual {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1772363
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->j:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->d()V

    .line 1772364
    :cond_0
    return-void
.end method

.method public setSelection(I)V
    .locals 1

    .prologue
    .line 1772354
    invoke-direct {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->getEndIndex()I

    move-result v0

    .line 1772355
    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-super {p0, v0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->setSelection(I)V

    .line 1772356
    return-void
.end method

.method public final setSelection(II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1772351
    invoke-direct {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->getEndIndex()I

    move-result v0

    .line 1772352
    invoke-static {v2, p1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v2, p2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-super {p0, v1, v0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->setSelection(II)V

    .line 1772353
    return-void
.end method
