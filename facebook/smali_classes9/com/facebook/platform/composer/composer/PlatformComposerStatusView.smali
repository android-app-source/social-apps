.class public Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:Landroid/widget/EditText;

.field private c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public d:Lcom/facebook/platform/composer/composer/PlatformComposerEditText;

.field private e:Lcom/facebook/platform/composer/composer/PlatformComposerAppHashtagView;

.field private f:LX/1aX;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1aX",
            "<",
            "LX/1af;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Landroid/view/View$OnLongClickListener;

.field private final h:Landroid/view/View$OnTouchListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1773912
    const-class v0, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;

    const-string v1, "composer"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1773907
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1773908
    new-instance v0, LX/BKW;

    invoke-direct {v0, p0}, LX/BKW;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;)V

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->g:Landroid/view/View$OnLongClickListener;

    .line 1773909
    new-instance v0, LX/BKX;

    invoke-direct {v0, p0}, LX/BKX;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;)V

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->h:Landroid/view/View$OnTouchListener;

    .line 1773910
    invoke-direct {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->b()V

    .line 1773911
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1773902
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1773903
    new-instance v0, LX/BKW;

    invoke-direct {v0, p0}, LX/BKW;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;)V

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->g:Landroid/view/View$OnLongClickListener;

    .line 1773904
    new-instance v0, LX/BKX;

    invoke-direct {v0, p0}, LX/BKX;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;)V

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->h:Landroid/view/View$OnTouchListener;

    .line 1773905
    invoke-direct {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->b()V

    .line 1773906
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1773891
    const v0, 0x7f030fcb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1773892
    const v0, 0x7f0d1913

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->d:Lcom/facebook/platform/composer/composer/PlatformComposerEditText;

    .line 1773893
    const v0, 0x7f0d1602

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1773894
    const v0, 0x7f0d261d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/platform/composer/composer/PlatformComposerAppHashtagView;

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->e:Lcom/facebook/platform/composer/composer/PlatformComposerAppHashtagView;

    .line 1773895
    new-instance v0, LX/1Uo;

    invoke-virtual {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 1773896
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->f:LX/1aX;

    .line 1773897
    const v0, 0x7f0d261c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->b:Landroid/widget/EditText;

    .line 1773898
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->g:Landroid/view/View$OnLongClickListener;

    invoke-virtual {p0, v0}, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1773899
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->h:Landroid/view/View$OnTouchListener;

    invoke-virtual {p0, v0}, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1773900
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1773901
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1773878
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1773879
    return-void
.end method

.method public final a(Landroid/text/SpannableStringBuilder;)V
    .locals 2

    .prologue
    .line 1773888
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->b:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1773889
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->b:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1773890
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x2e592ad4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1773913
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onAttachedToWindow()V

    .line 1773914
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->f:LX/1aX;

    if-eqz v1, :cond_0

    .line 1773915
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->f:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->d()V

    .line 1773916
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x78a1e50b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x549aab83

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1773884
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onDetachedFromWindow()V

    .line 1773885
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->f:LX/1aX;

    if-eqz v1, :cond_0

    .line 1773886
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->f:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->f()V

    .line 1773887
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x456cf458

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 1773880
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onFinishTemporaryDetach()V

    .line 1773881
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->f:LX/1aX;

    if-eqz v0, :cond_0

    .line 1773882
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->f:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->d()V

    .line 1773883
    :cond_0
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 1773874
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onStartTemporaryDetach()V

    .line 1773875
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->f:LX/1aX;

    if-eqz v0, :cond_0

    .line 1773876
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->f:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->f()V

    .line 1773877
    :cond_0
    return-void
.end method

.method public setAppProvidedHashtag(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1773870
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->e:Lcom/facebook/platform/composer/composer/PlatformComposerAppHashtagView;

    invoke-virtual {v0, p1}, Lcom/facebook/platform/composer/composer/PlatformComposerAppHashtagView;->setAppProvidedHashtag(Ljava/lang/String;)V

    .line 1773871
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->e:Lcom/facebook/platform/composer/composer/PlatformComposerAppHashtagView;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/platform/composer/composer/PlatformComposerAppHashtagView;->setVisibility(I)V

    .line 1773872
    return-void

    .line 1773873
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setListener(LX/BJJ;)V
    .locals 1

    .prologue
    .line 1773866
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->e:Lcom/facebook/platform/composer/composer/PlatformComposerAppHashtagView;

    if-eqz v0, :cond_0

    .line 1773867
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->e:Lcom/facebook/platform/composer/composer/PlatformComposerAppHashtagView;

    .line 1773868
    iput-object p1, v0, Lcom/facebook/platform/composer/composer/PlatformComposerAppHashtagView;->c:LX/BJJ;

    .line 1773869
    :cond_0
    return-void
.end method

.method public setProfileImage(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1773863
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1773864
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v2, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1773865
    return-void
.end method
