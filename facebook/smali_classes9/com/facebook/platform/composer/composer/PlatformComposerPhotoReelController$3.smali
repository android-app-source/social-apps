.class public final Lcom/facebook/platform/composer/composer/PlatformComposerPhotoReelController$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Ljava/util/List;

.field public final synthetic c:I

.field public final synthetic d:LX/BKU;


# direct methods
.method public constructor <init>(LX/BKU;ILjava/util/List;I)V
    .locals 0

    .prologue
    .line 1773806
    iput-object p1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerPhotoReelController$3;->d:LX/BKU;

    iput p2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerPhotoReelController$3;->a:I

    iput-object p3, p0, Lcom/facebook/platform/composer/composer/PlatformComposerPhotoReelController$3;->b:Ljava/util/List;

    iput p4, p0, Lcom/facebook/platform/composer/composer/PlatformComposerPhotoReelController$3;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 1773807
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerPhotoReelController$3;->d:LX/BKU;

    iget-object v0, v0, LX/BKU;->m:Landroid/widget/ScrollView;

    if-nez v0, :cond_0

    .line 1773808
    :goto_0
    return-void

    .line 1773809
    :cond_0
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerPhotoReelController$3;->d:LX/BKU;

    iget-object v0, v0, LX/BKU;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 1773810
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerPhotoReelController$3;->d:LX/BKU;

    iget-object v1, v1, LX/BKU;->i:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v5

    .line 1773811
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerPhotoReelController$3;->d:LX/BKU;

    iget-object v1, v1, LX/BKU;->i:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingTop()I

    move-result v6

    .line 1773812
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerPhotoReelController$3;->d:LX/BKU;

    iget-object v1, v1, LX/BKU;->i:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingBottom()I

    move-result v7

    .line 1773813
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerPhotoReelController$3;->d:LX/BKU;

    iget-object v1, v1, LX/BKU;->i:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    .line 1773814
    iget-object v3, p0, Lcom/facebook/platform/composer/composer/PlatformComposerPhotoReelController$3;->d:LX/BKU;

    iget-object v3, v3, LX/BKU;->i:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    .line 1773815
    iget-object v4, p0, Lcom/facebook/platform/composer/composer/PlatformComposerPhotoReelController$3;->d:LX/BKU;

    iget-object v4, v4, LX/BKU;->m:Landroid/widget/ScrollView;

    invoke-virtual {v4, v2}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    sub-int/2addr v4, v5

    add-int/2addr v4, v6

    .line 1773816
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1773817
    sub-int/2addr v0, v1

    sub-int v9, v0, v3

    move v1, v2

    move v3, v2

    .line 1773818
    :goto_1
    iget v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerPhotoReelController$3;->a:I

    if-ge v1, v0, :cond_1

    .line 1773819
    int-to-float v10, v9

    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerPhotoReelController$3;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    div-float v0, v10, v0

    float-to-int v0, v0

    .line 1773820
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v8, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1773821
    add-int/2addr v3, v0

    .line 1773822
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1773823
    :cond_1
    sub-int v0, v5, v6

    sub-int/2addr v0, v7

    sub-int/2addr v0, v3

    iget v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerPhotoReelController$3;->a:I

    div-int v3, v0, v1

    move v1, v4

    .line 1773824
    :goto_2
    iget v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerPhotoReelController$3;->c:I

    if-ge v2, v0, :cond_2

    .line 1773825
    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    .line 1773826
    add-int/lit8 v2, v2, 0x1

    move v1, v0

    goto :goto_2

    .line 1773827
    :cond_2
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerPhotoReelController$3;->d:LX/BKU;

    iget-object v0, v0, LX/BKU;->o:LX/ATO;

    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerPhotoReelController$3;->d:LX/BKU;

    iget-boolean v2, v2, LX/BKU;->n:Z

    invoke-virtual {v0, v2}, LX/ATO;->c(Z)V

    .line 1773828
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerPhotoReelController$3;->d:LX/BKU;

    iget-object v0, v0, LX/BKU;->m:Landroid/widget/ScrollView;

    new-instance v2, Lcom/facebook/platform/composer/composer/PlatformComposerPhotoReelController$3$1;

    invoke-direct {v2, p0, v1}, Lcom/facebook/platform/composer/composer/PlatformComposerPhotoReelController$3$1;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerPhotoReelController$3;I)V

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0
.end method
