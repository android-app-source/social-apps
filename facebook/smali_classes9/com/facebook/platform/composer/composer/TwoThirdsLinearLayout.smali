.class public Lcom/facebook/platform/composer/composer/TwoThirdsLinearLayout;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1773929
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1773930
    invoke-direct {p0}, Lcom/facebook/platform/composer/composer/TwoThirdsLinearLayout;->a()V

    .line 1773931
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1773926
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1773927
    invoke-direct {p0}, Lcom/facebook/platform/composer/composer/TwoThirdsLinearLayout;->a()V

    .line 1773928
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1773923
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1773924
    invoke-direct {p0}, Lcom/facebook/platform/composer/composer/TwoThirdsLinearLayout;->a()V

    .line 1773925
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1773921
    const-class v0, Lcom/facebook/platform/composer/composer/TwoThirdsLinearLayout;

    invoke-static {v0, p0}, Lcom/facebook/platform/composer/composer/TwoThirdsLinearLayout;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1773922
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/platform/composer/composer/TwoThirdsLinearLayout;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/platform/composer/composer/TwoThirdsLinearLayout;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v0

    check-cast v0, LX/0hB;

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/TwoThirdsLinearLayout;->a:LX/0hB;

    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 1773917
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/TwoThirdsLinearLayout;->a:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1773918
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/TwoThirdsLinearLayout;->a:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->d()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    div-int/lit8 v1, v1, 0x3

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 1773919
    invoke-super {p0, v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->onMeasure(II)V

    .line 1773920
    return-void
.end method
