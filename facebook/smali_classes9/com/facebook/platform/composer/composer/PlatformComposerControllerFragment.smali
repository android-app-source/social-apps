.class public Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field private a:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/facebook/base/fragment/FbFragment;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

.field public c:LX/BJX;

.field public d:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;

.field public final e:LX/BJU;

.field public final f:LX/BJV;

.field public final g:LX/BJW;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1772215
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1772216
    new-instance v0, LX/BJU;

    invoke-direct {v0, p0}, LX/BJU;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;)V

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->e:LX/BJU;

    .line 1772217
    new-instance v0, LX/BJV;

    invoke-direct {v0, p0}, LX/BJV;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;)V

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->f:LX/BJV;

    .line 1772218
    new-instance v0, LX/BJW;

    invoke-direct {v0, p0}, LX/BJW;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;)V

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->g:LX/BJW;

    return-void
.end method

.method public static a(Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;Lcom/facebook/base/fragment/FbFragment;)V
    .locals 3

    .prologue
    .line 1772194
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->c(Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;)Ljava/util/Stack;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/base/fragment/FbFragment;

    .line 1772195
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v1, v1

    .line 1772196
    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    .line 1772197
    if-eqz v0, :cond_0

    .line 1772198
    invoke-virtual {v1, v0}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    .line 1772199
    :cond_0
    instance-of v2, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;

    if-eqz v2, :cond_1

    .line 1772200
    check-cast v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;

    const/4 v2, 0x1

    .line 1772201
    iput-boolean v2, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->u:Z

    .line 1772202
    :cond_1
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1772203
    const v0, 0x7f0d25fd

    invoke-virtual {v1, v0, p1}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 1772204
    :cond_2
    invoke-virtual {v1}, LX/0hH;->b()I

    .line 1772205
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->c(Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;)Ljava/util/Stack;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1772206
    return-void
.end method

.method public static c(Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;)Ljava/util/Stack;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Stack",
            "<",
            "Lcom/facebook/base/fragment/FbFragment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1772212
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->a:Ljava/util/Stack;

    if-nez v0, :cond_0

    .line 1772213
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->a:Ljava/util/Stack;

    .line 1772214
    :cond_0
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->a:Ljava/util/Stack;

    return-object v0
.end method

.method public static l(Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;)Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;
    .locals 2

    .prologue
    .line 1772219
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1772220
    new-instance v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;

    invoke-direct {v1}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;-><init>()V

    .line 1772221
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1772222
    move-object v0, v1

    .line 1772223
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->f:LX/BJV;

    .line 1772224
    iput-object v1, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;->e:LX/BJV;

    .line 1772225
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->g:LX/BJW;

    .line 1772226
    iput-object v1, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;->d:LX/BJW;

    .line 1772227
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->b:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    .line 1772228
    iget-object p0, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->as:LX/BKO;

    move-object v1, p0

    .line 1772229
    iput-object v1, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;->c:LX/BKO;

    .line 1772230
    return-object v0
.end method

.method public static n(Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;)V
    .locals 3

    .prologue
    .line 1772207
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    .line 1772208
    if-eqz v1, :cond_0

    .line 1772209
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v2, "input_method"

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1772210
    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1772211
    :cond_0
    return-void
.end method


# virtual methods
.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, 0x2eb7528b

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1772173
    const v0, 0x7f030fbb

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1772174
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v3

    .line 1772175
    if-eqz p3, :cond_0

    .line 1772176
    const v1, 0x7f0d25fd

    invoke-virtual {v3, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iput-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->b:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    .line 1772177
    :cond_0
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->b:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    if-nez v1, :cond_1

    .line 1772178
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 1772179
    new-instance v4, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-direct {v4}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;-><init>()V

    .line 1772180
    invoke-virtual {v4, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1772181
    move-object v1, v4

    .line 1772182
    iput-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->b:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    .line 1772183
    invoke-virtual {v3}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    .line 1772184
    const v3, 0x7f0d25fd

    iget-object v4, p0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->b:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-virtual {v1, v3, v4}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 1772185
    invoke-virtual {v1}, LX/0hH;->b()I

    .line 1772186
    :cond_1
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->b:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    .line 1772187
    iget-object v3, p0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->c:LX/BJX;

    if-nez v3, :cond_2

    .line 1772188
    new-instance v3, LX/BJX;

    invoke-direct {v3, p0}, LX/BJX;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;)V

    iput-object v3, p0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->c:LX/BJX;

    .line 1772189
    :cond_2
    iget-object v3, p0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->c:LX/BJX;

    move-object v3, v3

    .line 1772190
    iput-object v3, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->at:LX/BJX;

    .line 1772191
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->c(Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;)Ljava/util/Stack;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Stack;->clear()V

    .line 1772192
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->c(Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;)Ljava/util/Stack;

    move-result-object v1

    iget-object v3, p0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->b:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-virtual {v1, v3}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1772193
    const/16 v1, 0x2b

    const v3, -0x365bef42

    invoke-static {v5, v1, v3, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method
