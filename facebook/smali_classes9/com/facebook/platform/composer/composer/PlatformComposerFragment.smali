.class public Lcom/facebook/platform/composer/composer/PlatformComposerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public A:LX/AR2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/BKk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/BJN;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/926;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Dh;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;

.field private H:LX/BKj;

.field public I:LX/BKm;

.field private J:J

.field private K:LX/BJm;

.field private L:Lcom/facebook/platform/composer/composer/PlatformComposerEditText;

.field public M:Z

.field public N:LX/0wd;

.field public O:LX/0wd;

.field public P:LX/0wd;

.field public Q:Landroid/view/View;

.field public R:Landroid/view/View;

.field public S:I

.field public T:LX/BLr;

.field private U:LX/BLv;

.field public V:LX/6WS;

.field private W:Lcom/facebook/widget/ScrollingAwareScrollView;

.field public X:Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;

.field public Y:LX/BJG;

.field private Z:LX/BJR;

.field public a:LX/BJS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private aa:Landroid/view/View;

.field private ab:Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;

.field private ac:Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;

.field public ad:LX/BKt;

.field public ae:Z

.field private af:Ljava/lang/CharSequence;

.field public ag:Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;

.field public ah:Lcom/facebook/privacy/model/SelectablePrivacyData;

.field public ai:LX/BKU;

.field public aj:Z

.field private final ak:LX/BJx;

.field private final al:LX/BK8;

.field private final am:LX/BKE;

.field private final an:LX/BKG;

.field private final ao:LX/BKK;

.field private final ap:LX/BKL;

.field private final aq:LX/BKM;

.field public final ar:LX/8Q5;

.field public final as:LX/BKO;

.field public at:LX/BJX;

.field public final au:LX/93X;

.field public final av:LX/BJo;

.field private final aw:LX/BJp;

.field private final ax:LX/BJq;

.field public b:LX/BJi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/BKz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/8zA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/BKu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0wW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8RJ;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/BLw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/BLs;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/BKV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/BJH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/BKn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/BKi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/75F;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/75Q;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/31w;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/BKe;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1773410
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1773411
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ae:Z

    .line 1773412
    new-instance v0, LX/BJx;

    invoke-direct {v0, p0}, LX/BJx;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ak:LX/BJx;

    .line 1773413
    new-instance v0, LX/BK8;

    invoke-direct {v0, p0}, LX/BK8;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->al:LX/BK8;

    .line 1773414
    new-instance v0, LX/BKF;

    invoke-direct {v0, p0}, LX/BKF;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->am:LX/BKE;

    .line 1773415
    new-instance v0, LX/BKG;

    invoke-direct {v0, p0}, LX/BKG;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->an:LX/BKG;

    .line 1773416
    new-instance v0, LX/BKK;

    invoke-direct {v0, p0}, LX/BKK;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ao:LX/BKK;

    .line 1773417
    new-instance v0, LX/BKL;

    invoke-direct {v0, p0}, LX/BKL;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ap:LX/BKL;

    .line 1773418
    new-instance v0, LX/BKM;

    invoke-direct {v0, p0}, LX/BKM;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->aq:LX/BKM;

    .line 1773419
    new-instance v0, LX/BKN;

    invoke-direct {v0, p0}, LX/BKN;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ar:LX/8Q5;

    .line 1773420
    new-instance v0, LX/BKO;

    invoke-direct {v0, p0}, LX/BKO;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->as:LX/BKO;

    .line 1773421
    new-instance v0, LX/BJn;

    invoke-direct {v0, p0}, LX/BJn;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->au:LX/93X;

    .line 1773422
    new-instance v0, LX/BJo;

    invoke-direct {v0, p0}, LX/BJo;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->av:LX/BJo;

    .line 1773423
    new-instance v0, LX/BJp;

    invoke-direct {v0, p0}, LX/BJp;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->aw:LX/BJp;

    .line 1773424
    new-instance v0, LX/BJq;

    invoke-direct {v0, p0}, LX/BJq;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ax:LX/BJq;

    .line 1773425
    return-void
.end method

.method public static A(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)Z
    .locals 1

    .prologue
    .line 1773426
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getPlatformConfiguration()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getPlatformConfiguration()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static B(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V
    .locals 0

    .prologue
    .line 1773427
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->E(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    .line 1773428
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->K(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    .line 1773429
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->u(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    .line 1773430
    return-void
.end method

.method public static E(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V
    .locals 13

    .prologue
    .line 1773431
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->i()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1773432
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->L:Lcom/facebook/platform/composer/composer/PlatformComposerEditText;

    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v1, v1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v1}, Lcom/facebook/platform/composer/model/PlatformComposition;->i()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->setMinutiaeUri(Landroid/net/Uri;)V

    .line 1773433
    :cond_0
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->L:Lcom/facebook/platform/composer/composer/PlatformComposerEditText;

    const/4 v4, 0x1

    .line 1773434
    iget-object v5, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v5, v5, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v5}, Lcom/facebook/platform/composer/model/PlatformComposition;->u()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    const/4 v5, 0x0

    move v6, v5

    :goto_0
    if-ge v6, v8, :cond_2

    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/ipc/model/FacebookProfile;

    .line 1773435
    iget-wide v9, v5, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)J

    move-result-wide v11

    cmp-long v9, v9, v11

    if-eqz v9, :cond_1

    iget-object v9, v5, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    if-eqz v9, :cond_1

    .line 1773436
    iget-object v5, v5, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    .line 1773437
    :goto_1
    move-object v2, v5

    .line 1773438
    new-instance v3, LX/8z6;

    invoke-direct {v3}, LX/8z6;-><init>()V

    .line 1773439
    iput-boolean v4, v3, LX/8z6;->f:Z

    .line 1773440
    move-object v3, v3

    .line 1773441
    iput-boolean v4, v3, LX/8z6;->g:Z

    .line 1773442
    move-object v3, v3

    .line 1773443
    new-instance v4, LX/BK7;

    invoke-direct {v4, p0}, LX/BK7;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    .line 1773444
    iput-object v4, v3, LX/8z6;->h:LX/8z9;

    .line 1773445
    move-object v3, v3

    .line 1773446
    iget-object v4, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v4, v4, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v4}, Lcom/facebook/platform/composer/model/PlatformComposition;->i()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v4

    .line 1773447
    iput-object v4, v3, LX/8z6;->a:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1773448
    invoke-virtual {v3, v2}, LX/8z6;->b(Ljava/lang/String;)LX/8z6;

    .line 1773449
    iget-object v5, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v5, v5, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v5}, Lcom/facebook/platform/composer/model/PlatformComposition;->u()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    const/4 v5, 0x0

    move v6, v5

    :goto_2
    if-ge v6, v8, :cond_4

    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/ipc/model/FacebookProfile;

    .line 1773450
    iget-wide v9, v5, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)J

    move-result-wide v11

    cmp-long v5, v9, v11

    if-nez v5, :cond_3

    .line 1773451
    iget-object v5, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v5, v5, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v5}, Lcom/facebook/platform/composer/model/PlatformComposition;->u()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    .line 1773452
    :goto_3
    move v2, v5

    .line 1773453
    iput v2, v3, LX/8z6;->e:I

    .line 1773454
    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v2

    .line 1773455
    iput-object v2, v3, LX/8z6;->b:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1773456
    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->h:LX/8zA;

    invoke-virtual {v3}, LX/8z6;->a()LX/8z5;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/8zA;->a(LX/8z5;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    .line 1773457
    move-object v1, v2

    .line 1773458
    invoke-virtual {v0, v1}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->a(Landroid/text/SpannableStringBuilder;)V

    .line 1773459
    return-void

    .line 1773460
    :cond_1
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto/16 :goto_0

    .line 1773461
    :cond_2
    const/4 v5, 0x0

    goto :goto_1

    .line 1773462
    :cond_3
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_2

    .line 1773463
    :cond_4
    iget-object v5, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v5, v5, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v5}, Lcom/facebook/platform/composer/model/PlatformComposition;->u()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    goto :goto_3
.end method

.method public static I(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)J
    .locals 2

    .prologue
    .line 1773464
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 1773465
    iget-object v1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v1

    .line 1773466
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static K(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 1773467
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->aa:Landroid/view/View;

    if-nez v0, :cond_1

    .line 1773468
    :cond_0
    :goto_0
    return-void

    .line 1773469
    :cond_1
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-boolean v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->c:Z

    if-nez v0, :cond_2

    .line 1773470
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->aa:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1773471
    :cond_2
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->t:LX/BKi;

    iget-boolean v0, v0, LX/BKi;->e:Z

    if-nez v0, :cond_3

    .line 1773472
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->aa:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1773473
    invoke-direct {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->R()V

    goto :goto_0

    .line 1773474
    :cond_3
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->aa:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1773475
    invoke-direct {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->L()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-boolean v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->d:Z

    if-eqz v0, :cond_5

    .line 1773476
    :cond_4
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ab:Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;

    invoke-virtual {v0, v2}, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->setVisibility(I)V

    .line 1773477
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ac:Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;

    invoke-virtual {v0, v1}, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->setVisibility(I)V

    .line 1773478
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ab:Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;

    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->M(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)Z

    move-result v1

    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v2, v2, LX/BKm;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->a(ZLcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    goto :goto_0

    .line 1773479
    :cond_5
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    if-eqz v0, :cond_0

    .line 1773480
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ab:Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;

    invoke-virtual {v0, v1}, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->setVisibility(I)V

    .line 1773481
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ac:Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;

    invoke-virtual {v0, v2}, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->setVisibility(I)V

    .line 1773482
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ac:Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;

    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->M(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)Z

    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v1, v1, LX/BKm;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object v1, v1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->c()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->a(Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;Lcom/facebook/graphql/model/GraphQLAlbum;)V

    goto :goto_0
.end method

.method private L()Z
    .locals 1

    .prologue
    .line 1773483
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v0, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static M(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)Z
    .locals 1

    .prologue
    .line 1773484
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->s()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static P(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1773485
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1773486
    if-eqz v0, :cond_0

    .line 1773487
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->w:LX/0if;

    sget-object v2, LX/0ig;->k:LX/0ih;

    const-string v3, "finish_activity"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1773488
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->w:LX/0if;

    sget-object v2, LX/0ig;->k:LX/0ih;

    invoke-virtual {v1, v2}, LX/0if;->c(LX/0ih;)V

    .line 1773489
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 1773490
    invoke-virtual {v0, v4, v4}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 1773491
    :cond_0
    return-void
.end method

.method private R()V
    .locals 1

    .prologue
    .line 1773492
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->U:LX/BLv;

    if-eqz v0, :cond_0

    .line 1773493
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->U:LX/BLv;

    invoke-virtual {v0}, LX/BLv;->a()V

    .line 1773494
    :cond_0
    return-void
.end method

.method private a(Landroid/view/ViewStub;)LX/BLv;
    .locals 11

    .prologue
    .line 1773495
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->n:LX/BLw;

    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ao:LX/BKK;

    .line 1773496
    new-instance v2, LX/BLv;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-static {v0}, LX/BKi;->a(LX/0QB;)LX/BKi;

    move-result-object v7

    check-cast v7, LX/BKi;

    invoke-static {v0}, LX/8Rp;->b(LX/0QB;)LX/8Rp;

    move-result-object v8

    check-cast v8, LX/8Rp;

    const-class v3, LX/BLk;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/BLk;

    invoke-static {v0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v10

    check-cast v10, Ljava/lang/Boolean;

    move-object v3, p1

    move-object v4, v1

    invoke-direct/range {v2 .. v10}, LX/BLv;-><init>(Landroid/view/ViewStub;LX/BKK;LX/03V;Landroid/content/res/Resources;LX/BKi;LX/8Rp;LX/BLk;Ljava/lang/Boolean;)V

    .line 1773497
    move-object v0, v2

    .line 1773498
    return-object v0
.end method

.method private static a(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;LX/BJS;LX/BJi;LX/0Or;LX/0SG;LX/BKz;LX/0Ot;LX/0Ot;LX/8zA;LX/BKu;LX/0Sh;LX/0wW;Landroid/content/res/Resources;LX/0Ot;LX/BLw;LX/BLs;LX/BKV;LX/BJH;LX/BKn;LX/03V;LX/BKi;LX/75F;LX/75Q;LX/0if;LX/31w;LX/BKe;LX/1Ck;LX/AR2;LX/BKk;LX/BJN;LX/0ad;LX/926;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/platform/composer/composer/PlatformComposerFragment;",
            "LX/BJS;",
            "LX/BJi;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0SG;",
            "LX/BKz;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/8zA;",
            "LX/BKu;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0wW;",
            "Landroid/content/res/Resources;",
            "LX/0Ot",
            "<",
            "LX/8RJ;",
            ">;",
            "LX/BLw;",
            "LX/BLs;",
            "LX/BKV;",
            "LX/BJH;",
            "LX/BKn;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/BKi;",
            "LX/75F;",
            "LX/75Q;",
            "Lcom/facebook/funnellogger/FunnelLogger;",
            "LX/31w;",
            "LX/BKe;",
            "LX/1Ck;",
            "LX/AR2;",
            "LX/BKk;",
            "LX/BJN;",
            "LX/0ad;",
            "LX/926;",
            "LX/0Ot",
            "<",
            "LX/7Dh;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1773499
    iput-object p1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->a:LX/BJS;

    iput-object p2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->b:LX/BJi;

    iput-object p3, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->c:LX/0Or;

    iput-object p4, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->d:LX/0SG;

    iput-object p5, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->e:LX/BKz;

    iput-object p6, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->f:LX/0Ot;

    iput-object p7, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->g:LX/0Ot;

    iput-object p8, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->h:LX/8zA;

    iput-object p9, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->i:LX/BKu;

    iput-object p10, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->j:LX/0Sh;

    iput-object p11, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->k:LX/0wW;

    iput-object p12, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->l:Landroid/content/res/Resources;

    iput-object p13, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->m:LX/0Ot;

    iput-object p14, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->n:LX/BLw;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->o:LX/BLs;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->p:LX/BKV;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->q:LX/BJH;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->r:LX/BKn;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->s:LX/03V;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->t:LX/BKi;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->u:LX/75F;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->v:LX/75Q;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->w:LX/0if;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->x:LX/31w;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->y:LX/BKe;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->z:LX/1Ck;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->A:LX/AR2;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->B:LX/BKk;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->C:LX/BJN;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->D:LX/0ad;

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->E:LX/926;

    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->F:LX/0Ot;

    return-void
.end method

.method public static a(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;Lcom/facebook/composer/minutiae/model/MinutiaeObject;)V
    .locals 2

    .prologue
    .line 1773500
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    invoke-virtual {v0}, LX/BKm;->a()LX/BKl;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v1, v1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v1}, Lcom/facebook/platform/composer/model/PlatformComposition;->A()LX/BKp;

    move-result-object v1

    .line 1773501
    iput-object p1, v1, LX/BKp;->h:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1773502
    move-object v1, v1

    .line 1773503
    invoke-virtual {v1}, LX/BKp;->b()Lcom/facebook/platform/composer/model/PlatformComposition;

    move-result-object v1

    .line 1773504
    iput-object v1, v0, LX/BKl;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    .line 1773505
    move-object v0, v0

    .line 1773506
    invoke-virtual {v0}, LX/BKl;->a()LX/BKm;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    .line 1773507
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 36

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v34

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    const-class v3, LX/BJS;

    move-object/from16 v0, v34

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/BJS;

    invoke-static/range {v34 .. v34}, LX/BJi;->a(LX/0QB;)LX/BJi;

    move-result-object v4

    check-cast v4, LX/BJi;

    const/16 v5, 0x12cb

    move-object/from16 v0, v34

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static/range {v34 .. v34}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    const-class v7, LX/BKz;

    move-object/from16 v0, v34

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/BKz;

    const/16 v8, 0x19c6

    move-object/from16 v0, v34

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x455

    move-object/from16 v0, v34

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static/range {v34 .. v34}, LX/8zA;->a(LX/0QB;)LX/8zA;

    move-result-object v10

    check-cast v10, LX/8zA;

    const-class v11, LX/BKu;

    move-object/from16 v0, v34

    invoke-interface {v0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/BKu;

    invoke-static/range {v34 .. v34}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v12

    check-cast v12, LX/0Sh;

    invoke-static/range {v34 .. v34}, LX/0wW;->a(LX/0QB;)LX/0wW;

    move-result-object v13

    check-cast v13, LX/0wW;

    invoke-static/range {v34 .. v34}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v14

    check-cast v14, Landroid/content/res/Resources;

    const/16 v15, 0x2fe5

    move-object/from16 v0, v34

    invoke-static {v0, v15}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const-class v16, LX/BLw;

    move-object/from16 v0, v34

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v16

    check-cast v16, LX/BLw;

    const-class v17, LX/BLs;

    move-object/from16 v0, v34

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v17

    check-cast v17, LX/BLs;

    const-class v18, LX/BKV;

    move-object/from16 v0, v34

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v18

    check-cast v18, LX/BKV;

    const-class v19, LX/BJH;

    move-object/from16 v0, v34

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v19

    check-cast v19, LX/BJH;

    invoke-static/range {v34 .. v34}, LX/BKn;->a(LX/0QB;)LX/BKn;

    move-result-object v20

    check-cast v20, LX/BKn;

    invoke-static/range {v34 .. v34}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v21

    check-cast v21, LX/03V;

    invoke-static/range {v34 .. v34}, LX/BKi;->a(LX/0QB;)LX/BKi;

    move-result-object v22

    check-cast v22, LX/BKi;

    invoke-static/range {v34 .. v34}, LX/75F;->a(LX/0QB;)LX/75F;

    move-result-object v23

    check-cast v23, LX/75F;

    invoke-static/range {v34 .. v34}, LX/75Q;->a(LX/0QB;)LX/75Q;

    move-result-object v24

    check-cast v24, LX/75Q;

    invoke-static/range {v34 .. v34}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v25

    check-cast v25, LX/0if;

    invoke-static/range {v34 .. v34}, LX/31w;->a(LX/0QB;)LX/31w;

    move-result-object v26

    check-cast v26, LX/31w;

    invoke-static/range {v34 .. v34}, LX/BKe;->a(LX/0QB;)LX/BKe;

    move-result-object v27

    check-cast v27, LX/BKe;

    invoke-static/range {v34 .. v34}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v28

    check-cast v28, LX/1Ck;

    const-class v29, LX/AR2;

    move-object/from16 v0, v34

    move-object/from16 v1, v29

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v29

    check-cast v29, LX/AR2;

    const-class v30, LX/BKk;

    move-object/from16 v0, v34

    move-object/from16 v1, v30

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v30

    check-cast v30, LX/BKk;

    invoke-static/range {v34 .. v34}, LX/BJN;->a(LX/0QB;)LX/BJN;

    move-result-object v31

    check-cast v31, LX/BJN;

    invoke-static/range {v34 .. v34}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v32

    check-cast v32, LX/0ad;

    invoke-static/range {v34 .. v34}, LX/926;->a(LX/0QB;)LX/926;

    move-result-object v33

    check-cast v33, LX/926;

    const/16 v35, 0x3572

    invoke-static/range {v34 .. v35}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v34

    invoke-static/range {v2 .. v34}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->a(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;LX/BJS;LX/BJi;LX/0Or;LX/0SG;LX/BKz;LX/0Ot;LX/0Ot;LX/8zA;LX/BKu;LX/0Sh;LX/0wW;Landroid/content/res/Resources;LX/0Ot;LX/BLw;LX/BLs;LX/BKV;LX/BJH;LX/BKn;LX/03V;LX/BKi;LX/75F;LX/75Q;LX/0if;LX/31w;LX/BKe;LX/1Ck;LX/AR2;LX/BKk;LX/BJN;LX/0ad;LX/926;LX/0Ot;)V

    return-void
.end method

.method private a(Z)V
    .locals 10

    .prologue
    .line 1773508
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ai:LX/BKU;

    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v1, v1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v1}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v1

    const/16 v5, 0x8

    const/4 v3, 0x0

    .line 1773509
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1773510
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    if-eq v2, v7, :cond_6

    move v2, v6

    .line 1773511
    :goto_0
    move v2, v2

    .line 1773512
    if-nez v2, :cond_3

    iget-object v2, v0, LX/BKU;->f:LX/0ad;

    sget-short v4, LX/BJ8;->e:S

    invoke-interface {v2, v4, v3}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, v0, LX/BKU;->p:Z

    if-nez v2, :cond_3

    .line 1773513
    iget-object v2, v0, LX/BKU;->j:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1773514
    iget-object v2, v0, LX/BKU;->i:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 1773515
    :goto_1
    iget-object v2, v0, LX/BKU;->k:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v2}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_5

    .line 1773516
    iget-object v2, v0, LX/BKU;->k:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v2}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->a()V

    .line 1773517
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 1773518
    const/4 v2, 0x0

    move v4, v2

    :goto_2
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    const/4 v5, 0x5

    invoke-static {v2, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    if-ge v4, v2, :cond_2

    .line 1773519
    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1773520
    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 1773521
    const/4 v9, 0x1

    .line 1773522
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    const/4 v7, 0x5

    invoke-static {v5, v9, v7}, LX/0yq;->a(III)I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    .line 1773523
    sget-object v7, LX/BJ9;->a:[[[I

    aget-object v5, v7, v5

    aget-object v5, v5, v4

    .line 1773524
    new-instance v7, LX/4Yt;

    invoke-direct {v7}, LX/4Yt;-><init>()V

    const/4 v8, 0x0

    aget v8, v5, v8

    .line 1773525
    iput v8, v7, LX/4Yt;->E:I

    .line 1773526
    move-object v7, v7

    .line 1773527
    aget v8, v5, v9

    .line 1773528
    iput v8, v7, LX/4Yt;->F:I

    .line 1773529
    move-object v7, v7

    .line 1773530
    const/4 v8, 0x2

    aget v8, v5, v8

    .line 1773531
    iput v8, v7, LX/4Yt;->D:I

    .line 1773532
    move-object v7, v7

    .line 1773533
    const/4 v8, 0x3

    aget v5, v5, v8

    .line 1773534
    iput v5, v7, LX/4Yt;->C:I

    .line 1773535
    move-object v5, v7

    .line 1773536
    invoke-virtual {v5}, LX/4Yt;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    move-result-object v5

    move-object v7, v5

    .line 1773537
    if-eqz v7, :cond_0

    .line 1773538
    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v5

    .line 1773539
    iget-object v8, v5, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    move-object v5, v8

    .line 1773540
    sget-object v8, LX/4gQ;->Video:LX/4gQ;

    if-ne v5, v8, :cond_1

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    :goto_3
    invoke-static {v5}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    .line 1773541
    new-instance v8, LX/39x;

    invoke-direct {v8}, LX/39x;-><init>()V

    new-instance v9, LX/4XB;

    invoke-direct {v9}, LX/4XB;-><init>()V

    invoke-virtual {v9}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v9

    .line 1773542
    iput-object v9, v8, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1773543
    move-object v8, v8

    .line 1773544
    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1773545
    iput-object v2, v8, LX/39x;->w:Ljava/lang/String;

    .line 1773546
    move-object v2, v8

    .line 1773547
    iput-object v5, v2, LX/39x;->p:LX/0Px;

    .line 1773548
    move-object v2, v2

    .line 1773549
    invoke-static {v7}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    .line 1773550
    iput-object v5, v2, LX/39x;->o:LX/0Px;

    .line 1773551
    move-object v2, v2

    .line 1773552
    invoke-virtual {v2}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    invoke-virtual {v6, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1773553
    :cond_0
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_2

    .line 1773554
    :cond_1
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto :goto_3

    .line 1773555
    :cond_2
    new-instance v2, LX/39x;

    invoke-direct {v2}, LX/39x;-><init>()V

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    .line 1773556
    iput-object v4, v2, LX/39x;->q:LX/0Px;

    .line 1773557
    move-object v2, v2

    .line 1773558
    invoke-virtual {v2}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    move-object v4, v2

    .line 1773559
    iget-object v2, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 1773560
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v5

    .line 1773561
    new-array v6, v5, [LX/1aZ;

    .line 1773562
    iget-object v2, v0, LX/BKU;->d:LX/1Ad;

    invoke-virtual {v2}, LX/1Ad;->o()LX/1Ad;

    move-result-object v2

    const-class v7, LX/BKU;

    invoke-static {v7}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v7

    invoke-virtual {v2, v7}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    .line 1773563
    :goto_4
    if-ge v3, v5, :cond_4

    .line 1773564
    iget-object v7, v0, LX/BKU;->d:LX/1Ad;

    .line 1773565
    iget-object v2, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 1773566
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v2

    aput-object v2, v6, v3

    .line 1773567
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_4

    .line 1773568
    :cond_3
    iget-object v2, v0, LX/BKU;->j:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 1773569
    iget-object v2, v0, LX/BKU;->i:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 1773570
    :cond_4
    iget-object v2, v0, LX/BKU;->c:LX/26G;

    invoke-virtual {v2, v4}, LX/26G;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/26K;

    move-result-object v2

    .line 1773571
    iget-object v3, v0, LX/BKU;->b:LX/26H;

    invoke-virtual {v3, v2}, LX/26H;->a(LX/26L;)LX/26O;

    move-result-object v2

    .line 1773572
    iget-object v3, v0, LX/BKU;->k:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v3, v2, v6}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->a(LX/26O;[LX/1aZ;)V

    .line 1773573
    iget-object v2, v0, LX/BKU;->k:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    sub-int/2addr v3, v5

    add-int/lit8 v3, v3, 0x1

    .line 1773574
    iput v3, v2, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->l:I

    .line 1773575
    :cond_5
    iget-object v2, v0, LX/BKU;->o:LX/ATO;

    invoke-virtual {v2, v1, p1}, LX/ATO;->a(LX/0Px;Z)V

    .line 1773576
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->u(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    .line 1773577
    return-void

    .line 1773578
    :cond_6
    invoke-virtual {v1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1773579
    invoke-static {v2}, LX/7kq;->d(Lcom/facebook/composer/attachments/ComposerAttachment;)Z

    move-result v4

    if-eqz v4, :cond_7

    iget-object v4, v0, LX/BKU;->g:LX/7Dh;

    invoke-virtual {v4}, LX/7Dh;->b()Z

    move-result v4

    if-nez v4, :cond_8

    :cond_7
    move v2, v6

    .line 1773580
    goto/16 :goto_0

    .line 1773581
    :cond_8
    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1773582
    if-nez v4, :cond_9

    move v2, v6

    .line 1773583
    goto/16 :goto_0

    .line 1773584
    :cond_9
    iget-boolean v8, v4, Lcom/facebook/photos/base/media/PhotoItem;->f:Z

    move v8, v8

    .line 1773585
    if-eqz v8, :cond_a

    .line 1773586
    iget-boolean v2, v4, Lcom/facebook/photos/base/media/PhotoItem;->d:Z

    move v2, v2

    .line 1773587
    goto/16 :goto_0

    .line 1773588
    :cond_a
    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    iget-object v8, v0, LX/BKU;->e:LX/0W3;

    invoke-static {v2, v8}, LX/7E2;->a(Ljava/lang/String;LX/0W3;)LX/7E1;

    move-result-object v2

    .line 1773589
    iget-boolean v8, v2, LX/7E1;->b:Z

    move v8, v8

    .line 1773590
    if-eqz v8, :cond_b

    .line 1773591
    iput-boolean v7, v4, Lcom/facebook/photos/base/media/PhotoItem;->d:Z

    .line 1773592
    iget-object v6, v2, LX/7E1;->a:Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    move-object v2, v6

    .line 1773593
    iput-object v2, v4, Lcom/facebook/photos/base/media/PhotoItem;->e:Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    .line 1773594
    move v2, v7

    .line 1773595
    goto/16 :goto_0

    :cond_b
    move v2, v6

    .line 1773596
    goto/16 :goto_0
.end method

.method public static a$redex0(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;ILandroid/content/Intent;Z)V
    .locals 6

    .prologue
    const/4 v2, -0x1

    .line 1773597
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1773598
    if-nez v0, :cond_0

    .line 1773599
    :goto_0
    return-void

    .line 1773600
    :cond_0
    if-eqz p3, :cond_1

    if-eq p1, v2, :cond_1

    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->t:LX/BKi;

    iget-boolean v1, v1, LX/BKi;->d:Z

    if-eqz v1, :cond_1

    .line 1773601
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->b:LX/BJi;

    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->aw:LX/BJp;

    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    .line 1773602
    invoke-virtual {v0, v2}, LX/BJi;->a(LX/BKm;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1773603
    iget-object v3, v2, LX/BKm;->a:Ljava/lang/String;

    iput-object v3, v0, LX/BJi;->f:Ljava/lang/String;

    .line 1773604
    iget-object v3, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v3}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    iput v3, v0, LX/BJi;->g:I

    .line 1773605
    iget-object v3, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v3}, Lcom/facebook/platform/composer/model/PlatformComposition;->f()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v3

    iput v3, v0, LX/BJi;->h:I

    .line 1773606
    const-string v3, "open_dialog"

    invoke-static {v0, v3}, LX/BJi;->a$redex0(LX/BJi;Ljava/lang/String;)V

    .line 1773607
    invoke-virtual {v1}, LX/BJp;->a()Landroid/content/Context;

    move-result-object v3

    .line 1773608
    new-instance v4, LX/0ju;

    invoke-direct {v4, v3}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 1773609
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, LX/0ju;->a(Z)LX/0ju;

    .line 1773610
    const v5, 0x7f0824a1

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 1773611
    const v5, 0x7f0824a2

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 1773612
    new-instance v5, LX/BJZ;

    invoke-direct {v5, v0, v1}, LX/BJZ;-><init>(LX/BJi;LX/BJp;)V

    .line 1773613
    new-instance p0, LX/BJa;

    invoke-direct {p0, v0, v2, v1}, LX/BJa;-><init>(LX/BJi;LX/BKm;LX/BJp;)V

    .line 1773614
    new-instance p1, LX/BJb;

    invoke-direct {p1, v0}, LX/BJb;-><init>(LX/BJi;)V

    .line 1773615
    const p2, 0x7f082486

    invoke-virtual {v3, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 1773616
    const p3, 0x7f0824a0

    invoke-virtual {v3, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1773617
    invoke-virtual {v4, p2, v5}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1773618
    invoke-virtual {v4, v3, p0}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1773619
    invoke-virtual {v4, p1}, LX/0ju;->a(Landroid/content/DialogInterface$OnDismissListener;)LX/0ju;

    .line 1773620
    invoke-virtual {v4}, LX/0ju;->a()LX/2EJ;

    move-result-object v3

    .line 1773621
    invoke-virtual {v3}, LX/2EJ;->show()V

    .line 1773622
    :goto_1
    goto/16 :goto_0

    .line 1773623
    :cond_1
    invoke-virtual {v0, p1, p2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1773624
    iget-boolean v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ae:Z

    if-nez v0, :cond_3

    .line 1773625
    if-ne p1, v2, :cond_2

    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->t:LX/BKi;

    iget-object v0, v0, LX/BKi;->a:LX/BKf;

    sget-object v1, LX/BKf;->FULL:LX/BKf;

    if-eq v0, v1, :cond_2

    .line 1773626
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->O:LX/0wd;

    const-wide v2, 0x3feb333333333333L    # 0.85

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    goto/16 :goto_0

    .line 1773627
    :cond_2
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->P:LX/0wd;

    const-wide/high16 v2, 0x3ff8000000000000L    # 1.5

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    goto/16 :goto_0

    .line 1773628
    :cond_3
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->P(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    goto/16 :goto_0

    .line 1773629
    :cond_4
    invoke-static {v0, v2}, LX/BJi;->b(LX/BJi;LX/BKm;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1773630
    new-instance v3, Lcom/facebook/platform/composer/draftpost/DraftPostComposerCancelDialogFragment;

    invoke-direct {v3}, Lcom/facebook/platform/composer/draftpost/DraftPostComposerCancelDialogFragment;-><init>()V

    .line 1773631
    new-instance v4, LX/BJc;

    invoke-direct {v4, v0, v2, v1, v3}, LX/BJc;-><init>(LX/BJi;LX/BKm;LX/BJp;Lcom/facebook/platform/composer/draftpost/DraftPostComposerCancelDialogFragment;)V

    .line 1773632
    iput-object v4, v3, Lcom/facebook/platform/composer/draftpost/DraftPostComposerCancelDialogFragment;->m:Landroid/view/View$OnClickListener;

    .line 1773633
    new-instance v4, LX/BJd;

    invoke-direct {v4, v0, v2, v1, v3}, LX/BJd;-><init>(LX/BJi;LX/BKm;LX/BJp;Lcom/facebook/platform/composer/draftpost/DraftPostComposerCancelDialogFragment;)V

    .line 1773634
    iput-object v4, v3, Lcom/facebook/platform/composer/draftpost/DraftPostComposerCancelDialogFragment;->n:Landroid/view/View$OnClickListener;

    .line 1773635
    new-instance v4, LX/BJe;

    invoke-direct {v4, v0, v1}, LX/BJe;-><init>(LX/BJi;LX/BJp;)V

    .line 1773636
    iput-object v4, v3, Lcom/facebook/platform/composer/draftpost/DraftPostComposerCancelDialogFragment;->o:Landroid/content/DialogInterface$OnDismissListener;

    .line 1773637
    new-instance v4, LX/BJf;

    invoke-direct {v4, v0, v2}, LX/BJf;-><init>(LX/BJi;LX/BKm;)V

    .line 1773638
    iput-object v4, v3, Lcom/facebook/platform/composer/draftpost/DraftPostComposerCancelDialogFragment;->p:Landroid/content/DialogInterface$OnCancelListener;

    .line 1773639
    iget-object v4, v0, LX/BJi;->a:LX/BJ7;

    iget-object v5, v2, LX/BKm;->a:Ljava/lang/String;

    .line 1773640
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v2, LX/BJ6;->DRAFT_DIALOG_SEEN:LX/BJ6;

    iget-object v2, v2, LX/BJ6;->name:Ljava/lang/String;

    invoke-direct {v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "composer_session_id"

    invoke-virtual {v0, v2, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-static {v4, v0}, LX/BJ7;->a(LX/BJ7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1773641
    iget-object v4, v1, LX/BJp;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-virtual {v4}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v4

    move-object v4, v4

    .line 1773642
    const-string v5, "COMPOSER_CANCEL_DIALOG"

    invoke-virtual {v3, v4, v5}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1773643
    goto :goto_1

    .line 1773644
    :cond_5
    new-instance v3, LX/0ju;

    invoke-virtual {v1}, LX/BJp;->a()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, LX/0ju;-><init>(Landroid/content/Context;)V

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v3

    const v4, 0x7f082485

    invoke-virtual {v3, v4}, LX/0ju;->a(I)LX/0ju;

    move-result-object v3

    const v4, 0x7f082486

    new-instance v5, LX/BJY;

    invoke-direct {v5, v0, v1}, LX/BJY;-><init>(LX/BJi;LX/BJp;)V

    invoke-virtual {v3, v4, v5}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v3

    const v4, 0x7f082487

    new-instance v5, LX/BJh;

    invoke-direct {v5, v0, v1}, LX/BJh;-><init>(LX/BJi;LX/BJp;)V

    invoke-virtual {v3, v4, v5}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v3

    new-instance v4, LX/BJg;

    invoke-direct {v4, v0, v1}, LX/BJg;-><init>(LX/BJi;LX/BJp;)V

    invoke-virtual {v3, v4}, LX/0ju;->a(Landroid/content/DialogInterface$OnCancelListener;)LX/0ju;

    move-result-object v3

    invoke-virtual {v3}, LX/0ju;->a()LX/2EJ;

    move-result-object v3

    invoke-virtual {v3}, LX/2EJ;->show()V

    .line 1773645
    goto/16 :goto_1
.end method

.method public static a$redex0(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;LX/0Px;ZZZ)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;ZZZ)V"
        }
    .end annotation

    .prologue
    .line 1773646
    if-eqz p3, :cond_3

    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->A()LX/BKp;

    move-result-object v0

    .line 1773647
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1773648
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1773649
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1773650
    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v5

    if-nez v5, :cond_4

    .line 1773651
    const/4 v5, 0x0

    .line 1773652
    :cond_0
    :goto_1
    move-object v1, v5

    .line 1773653
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1773654
    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1773655
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1773656
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v1, v1

    .line 1773657
    invoke-virtual {v0, v1}, LX/BKp;->a(LX/0Px;)LX/BKp;

    move-result-object v0

    .line 1773658
    :goto_2
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    invoke-virtual {v1}, LX/BKm;->a()LX/BKl;

    move-result-object v1

    invoke-virtual {v0}, LX/BKp;->b()Lcom/facebook/platform/composer/model/PlatformComposition;

    move-result-object v0

    .line 1773659
    iput-object v0, v1, LX/BKl;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    .line 1773660
    move-object v0, v1

    .line 1773661
    invoke-virtual {v0}, LX/BKl;->a()LX/BKm;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    .line 1773662
    if-eqz p2, :cond_2

    .line 1773663
    invoke-direct {p0, p4}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->a(Z)V

    .line 1773664
    :cond_2
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->m$redex0(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    .line 1773665
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->y(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    .line 1773666
    return-void

    .line 1773667
    :cond_3
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->A()LX/BKp;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/BKp;->a(LX/0Px;)LX/BKp;

    move-result-object v0

    goto :goto_2

    .line 1773668
    :cond_4
    iget-object v5, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v5, v5, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v5}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    const/4 v5, 0x0

    move v6, v5

    :goto_3
    if-ge v6, v8, :cond_5

    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1773669
    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v9

    invoke-virtual {v5}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object p3

    invoke-virtual {v9, p3}, Lcom/facebook/ipc/media/MediaItem;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 1773670
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_3

    :cond_5
    move-object v5, v1

    .line 1773671
    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;Lcom/facebook/composer/privacy/model/ComposerPrivacyData;Z)V
    .locals 6

    .prologue
    .line 1773672
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    invoke-virtual {v0}, LX/BKm;->a()LX/BKl;

    move-result-object v0

    .line 1773673
    iput-object p1, v0, LX/BKl;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    .line 1773674
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v1, v1, LX/BKm;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object v1, v1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v2, p1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    const/4 v3, 0x0

    .line 1773675
    if-eqz v1, :cond_0

    if-nez v2, :cond_3

    .line 1773676
    :cond_0
    :goto_0
    move v1, v3

    .line 1773677
    if-eqz v1, :cond_1

    .line 1773678
    const/4 v1, 0x1

    .line 1773679
    iput-boolean v1, v0, LX/BKl;->l:Z

    .line 1773680
    :cond_1
    invoke-virtual {v0}, LX/BKl;->a()LX/BKm;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    .line 1773681
    invoke-direct {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->L()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1773682
    iget-object v0, p1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ah:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1773683
    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v0, :cond_2

    .line 1773684
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    invoke-virtual {v0}, LX/BKm;->a()LX/BKl;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v1, v1, LX/BKm;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object v1, v1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-virtual {v1}, Lcom/facebook/privacy/model/SelectablePrivacyData;->b()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v1

    .line 1773685
    iput-object v1, v0, LX/BKl;->g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1773686
    move-object v0, v0

    .line 1773687
    invoke-virtual {v0}, LX/BKl;->a()LX/BKm;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    .line 1773688
    :cond_2
    return-void

    .line 1773689
    :cond_3
    iget-object v4, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-eqz v4, :cond_4

    iget-object v4, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v4, v4, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-eqz v4, :cond_4

    iget-object v4, v2, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-eqz v4, :cond_4

    iget-object v4, v2, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v4, v4, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-eqz v4, :cond_4

    iget-object v4, v2, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v4, v4, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iget-object v5, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v5, v5, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1773690
    :cond_4
    iget-object v4, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v4, v4

    .line 1773691
    if-eqz v4, :cond_0

    .line 1773692
    iget-object v4, v2, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v4, v4

    .line 1773693
    if-eqz v4, :cond_0

    .line 1773694
    iget-object v4, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v4, v4

    .line 1773695
    iget-object v5, v2, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v5, v5

    .line 1773696
    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1773697
    iget-boolean v4, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    move v4, v4

    .line 1773698
    iget-boolean v5, v2, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    move v5, v5

    .line 1773699
    if-eq v4, v5, :cond_0

    :cond_5
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1773399
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    .line 1773400
    if-nez v0, :cond_0

    .line 1773401
    :goto_0
    return-void

    .line 1773402
    :cond_0
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->w:LX/0if;

    sget-object v2, LX/0ig;->k:LX/0ih;

    const-string v3, "send_failure"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1773403
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->w:LX/0if;

    sget-object v2, LX/0ig;->k:LX/0ih;

    invoke-virtual {v1, v2}, LX/0if;->c(LX/0ih;)V

    .line 1773404
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1773405
    const-string v2, "com.facebook.platform.extra.COMPOSER_ERROR"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1773406
    if-eqz p1, :cond_1

    .line 1773407
    const-string v2, "com.facebook.platform.extra.COMPOSER_EXCEPTION"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1773408
    :cond_1
    const/4 v2, 0x0

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v3, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1773409
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method private b(ILandroid/content/Intent;)V
    .locals 18

    .prologue
    .line 1773700
    const/4 v2, -0x1

    move/from16 v0, p1

    if-eq v0, v2, :cond_1

    .line 1773701
    :cond_0
    :goto_0
    return-void

    .line 1773702
    :cond_1
    const-string v2, "extra_photo_items_list"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v8

    .line 1773703
    const-string v2, "extras_taggable_gallery_creative_editing_data_list"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v9

    .line 1773704
    if-eqz v9, :cond_2

    .line 1773705
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v3

    if-ne v2, v3, :cond_5

    const/4 v2, 0x1

    :goto_1
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 1773706
    :cond_2
    const-string v2, "extra_are_media_items_modified"

    const/4 v3, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 1773707
    if-eqz v2, :cond_0

    .line 1773708
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v10

    .line 1773709
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v11

    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v12

    const/4 v2, 0x0

    move v7, v2

    :goto_2
    if-ge v7, v12, :cond_8

    invoke-virtual {v11, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1773710
    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->i()I

    move-result v3

    int-to-long v14, v3

    .line 1773711
    const/4 v6, 0x0

    .line 1773712
    const/4 v5, 0x0

    .line 1773713
    const/4 v3, 0x0

    move v4, v3

    :goto_3
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v3

    if-ge v4, v3, :cond_a

    .line 1773714
    invoke-interface {v8, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {v3}, Lcom/facebook/ipc/media/MediaItem;->c()J

    move-result-wide v16

    cmp-long v3, v16, v14

    if-nez v3, :cond_6

    .line 1773715
    invoke-interface {v8, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1773716
    if-eqz v9, :cond_9

    .line 1773717
    invoke-interface {v9, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1773718
    :goto_4
    if-eqz v3, :cond_7

    .line 1773719
    invoke-static {v3}, LX/7kv;->a(Lcom/facebook/ipc/media/MediaItem;)LX/7kv;

    move-result-object v3

    .line 1773720
    if-eqz v4, :cond_3

    .line 1773721
    invoke-virtual {v3, v4}, LX/7kv;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)LX/7kv;

    .line 1773722
    :cond_3
    :goto_5
    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->d()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 1773723
    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->d()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/7kv;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/7kv;

    .line 1773724
    :cond_4
    invoke-virtual {v3}, LX/7kv;->a()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v2

    invoke-virtual {v10, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1773725
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    goto :goto_2

    .line 1773726
    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 1773727
    :cond_6
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_3

    .line 1773728
    :cond_7
    invoke-static {v2}, LX/7kv;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)LX/7kv;

    move-result-object v3

    goto :goto_5

    .line 1773729
    :cond_8
    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3, v4, v5}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->a$redex0(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;LX/0Px;ZZZ)V

    .line 1773730
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ai:LX/BKU;

    invoke-static {v8}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/BKU;->a(LX/0Px;)V

    goto/16 :goto_0

    :cond_9
    move-object v4, v5

    goto :goto_4

    :cond_a
    move-object v4, v5

    move-object v3, v6

    goto :goto_4
.end method

.method public static k(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)Landroid/content/Intent;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1772889
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 1772890
    const-string v2, "skip_publish"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 1772891
    if-eqz v2, :cond_1

    move-object v1, v0

    .line 1772892
    :goto_0
    iget-object v3, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->e:LX/BKz;

    iget-object v4, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v3, v4, v0, v1}, LX/BKz;->a(LX/BKm;Lcom/facebook/user/model/User;LX/AR1;)LX/BKy;

    move-result-object v0

    invoke-virtual {v0}, LX/BKy;->a()Landroid/content/Intent;

    move-result-object v3

    .line 1772893
    if-nez v2, :cond_0

    .line 1772894
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    .line 1772895
    invoke-virtual {v0, v3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1772896
    :cond_0
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->w:LX/0if;

    sget-object v1, LX/0ig;->k:LX/0ih;

    const-string v2, "publish"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1772897
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ai:LX/BKU;

    .line 1772898
    iget-object v1, v0, LX/BKU;->o:LX/ATO;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/ATO;->b(Z)V

    .line 1772899
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1772900
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v5, :cond_2

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1772901
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1772902
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1772903
    :cond_1
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->A:LX/AR2;

    iget-object v3, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->H:LX/BKj;

    invoke-virtual {v1, v3, v0}, LX/AR2;->a(Ljava/lang/Object;LX/ARN;)LX/AR1;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 1772904
    :cond_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1772905
    const-string v0, "attachment_uris"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1772906
    :cond_3
    return-object v3
.end method

.method private l()V
    .locals 15

    .prologue
    .line 1772907
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->Q:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1772908
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ai:LX/BKU;

    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->W:Lcom/facebook/widget/ScrollingAwareScrollView;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->aq:LX/BKM;

    .line 1772909
    iput-object v1, v0, LX/BKU;->m:Landroid/widget/ScrollView;

    .line 1772910
    iget-object v7, v0, LX/BKU;->a:LX/ATP;

    const v8, 0x7f0d0ae5

    invoke-virtual {v1, v8}, Lcom/facebook/widget/ScrollingAwareScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/LinearLayout;

    const/4 v14, 0x0

    move-object v8, v2

    move-object v9, v1

    move v11, v3

    move v12, v4

    move-object v13, v6

    invoke-virtual/range {v7 .. v14}, LX/ATP;->a(LX/0gc;Lcom/facebook/widget/ScrollingAwareScrollView;Landroid/widget/LinearLayout;ZZLjava/lang/Object;Z)LX/ATO;

    move-result-object v7

    iput-object v7, v0, LX/BKU;->o:LX/ATO;

    .line 1772911
    const/high16 v7, 0x40000

    invoke-virtual {v1, v7}, Lcom/facebook/widget/ScrollingAwareScrollView;->setDescendantFocusability(I)V

    .line 1772912
    iget-object v7, v0, LX/BKU;->o:LX/ATO;

    .line 1772913
    iput-object v5, v7, LX/ATO;->M:LX/Hqr;

    .line 1772914
    iget-object v7, v0, LX/BKU;->h:Landroid/view/View;

    const/4 v8, 0x0

    invoke-static {v7, v8}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1772915
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->n()LX/0P1;

    move-result-object v0

    invoke-virtual {v0}, LX/0P1;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1772916
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ai:LX/BKU;

    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v1, v1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v1}, Lcom/facebook/platform/composer/model/PlatformComposition;->n()LX/0P1;

    move-result-object v1

    .line 1772917
    iget-object v2, v0, LX/BKU;->o:LX/ATO;

    invoke-virtual {v2, v1}, LX/ATO;->a(LX/0P1;)V

    .line 1772918
    :cond_0
    return-void
.end method

.method public static m$redex0(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1772919
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v0

    .line 1772920
    invoke-static {v0}, LX/7kq;->r(LX/0Px;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1772921
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->L:Lcom/facebook/platform/composer/composer/PlatformComposerEditText;

    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->l:Landroid/content/res/Resources;

    const v2, 0x7f082484

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1772922
    :goto_0
    return-void

    .line 1772923
    :cond_0
    invoke-static {v0}, LX/7kq;->q(LX/0Px;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1772924
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->L:Lcom/facebook/platform/composer/composer/PlatformComposerEditText;

    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->l:Landroid/content/res/Resources;

    const v2, 0x7f082483

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1772925
    :cond_1
    invoke-static {v0}, LX/7kq;->l(LX/0Px;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v1, v1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v1}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ne v1, v2, :cond_2

    .line 1772926
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->L:Lcom/facebook/platform/composer/composer/PlatformComposerEditText;

    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->l:Landroid/content/res/Resources;

    const v2, 0x7f082481

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1772927
    :cond_2
    invoke-static {v0}, LX/7kq;->l(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-le v0, v2, :cond_3

    .line 1772928
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->L:Lcom/facebook/platform/composer/composer/PlatformComposerEditText;

    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->l:Landroid/content/res/Resources;

    const v2, 0x7f082482

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1772929
    :cond_3
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->L:Lcom/facebook/platform/composer/composer/PlatformComposerEditText;

    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->l:Landroid/content/res/Resources;

    const v2, 0x7f08245a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static s(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1772930
    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    if-nez v2, :cond_1

    .line 1772931
    :cond_0
    :goto_0
    return v0

    .line 1772932
    :cond_1
    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->L:Lcom/facebook/platform/composer/composer/PlatformComposerEditText;

    if-eqz v2, :cond_0

    .line 1772933
    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v2, v2, LX/BKm;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v2, v2, LX/BKm;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLaunchLoggingParams()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->getSourceSurface()LX/21D;

    move-result-object v2

    sget-object v3, LX/21D;->THIRD_PARTY_APP_VIA_FB_API:LX/21D;

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->w()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->w()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/share/model/ComposerAppAttribution;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1772934
    :cond_2
    new-array v2, v1, [Ljava/lang/CharSequence;

    iget-object v3, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->L:Lcom/facebook/platform/composer/composer/PlatformComposerEditText;

    invoke-virtual {v3}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getEncodedText()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {v2}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v3

    .line 1772935
    invoke-direct {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->z()Z

    move-result v4

    .line 1772936
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->A(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)Z

    move-result v5

    .line 1772937
    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/7kq;->l(LX/0Px;)Z

    move-result v6

    .line 1772938
    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/7kq;->j(LX/0Px;)Z

    move-result v7

    .line 1772939
    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->p()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->p()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    if-eqz v2, :cond_6

    move v2, v1

    .line 1772940
    :goto_1
    if-eqz v3, :cond_5

    if-eqz v4, :cond_3

    if-eqz v5, :cond_5

    :cond_3
    if-nez v6, :cond_5

    if-nez v7, :cond_5

    if-eqz v5, :cond_4

    iget-boolean v3, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->aj:Z

    if-nez v3, :cond_5

    :cond_4
    if-eqz v2, :cond_0

    :cond_5
    move v0, v1

    goto/16 :goto_0

    :cond_6
    move v2, v0

    .line 1772941
    goto :goto_1
.end method

.method public static t$redex0(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V
    .locals 3

    .prologue
    .line 1772942
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->U:LX/BLv;

    if-eqz v0, :cond_0

    .line 1772943
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->U:LX/BLv;

    invoke-virtual {v0}, LX/BLv;->a()V

    .line 1772944
    :cond_0
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ag:Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;

    if-eqz v0, :cond_1

    .line 1772945
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ag:Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;

    sget-object v1, LX/BKc;->BIG_POST:LX/BKc;

    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->s(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->b(LX/BKc;Z)V

    .line 1772946
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ag:Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;

    sget-object v1, LX/BKc;->POST:LX/BKc;

    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->s(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->b(LX/BKc;Z)V

    .line 1772947
    :cond_1
    return-void
.end method

.method public static u(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V
    .locals 4

    .prologue
    .line 1772948
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->G:Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;

    if-eqz v0, :cond_1

    .line 1772949
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->G:Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->b()V

    .line 1772950
    :cond_0
    :goto_0
    return-void

    .line 1772951
    :cond_1
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->p()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1772952
    new-instance v0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;

    invoke-direct {v0}, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;-><init>()V

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->G:Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;

    .line 1772953
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->G:Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;

    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->Z:LX/BJR;

    .line 1772954
    iput-object v1, v0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->a:LX/BJR;

    .line 1772955
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    .line 1772956
    const v1, 0x7f0d261e

    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->G:Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;

    const-string v3, "ATTACHMENT_TAG"

    invoke-virtual {v0, v1, v2, v3}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    .line 1772957
    invoke-virtual {v0}, LX/0hH;->c()I

    .line 1772958
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->b()Z

    .line 1772959
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->G:Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->b()V

    goto :goto_0
.end method

.method public static x(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V
    .locals 4

    .prologue
    .line 1772960
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->e(LX/0Px;)LX/0Px;

    move-result-object v0

    .line 1772961
    new-instance v1, LX/8AA;

    sget-object v2, LX/8AB;->COMPOSER_ADD_MORE_MEDIA:LX/8AB;

    invoke-direct {v1, v2}, LX/8AA;-><init>(LX/8AB;)V

    sget-object v2, LX/8A9;->NONE:LX/8A9;

    invoke-virtual {v1, v2}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v1

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/8AA;->a(LX/0Px;)LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->p()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->q()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->r()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->c()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->s()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->e()LX/8AA;

    move-result-object v2

    .line 1772962
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->F:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7Dh;

    invoke-virtual {v1}, LX/7Dh;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1772963
    invoke-virtual {v2}, LX/8AA;->g()LX/8AA;

    .line 1772964
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->F:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7Dh;

    invoke-virtual {v1}, LX/7Dh;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1772965
    invoke-virtual {v2}, LX/8AA;->h()LX/8AA;

    .line 1772966
    :cond_0
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->x:LX/31w;

    iget-object v3, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v3, v3, LX/BKm;->e:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-object v3, v3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-virtual {v1, v3}, LX/31w;->a(LX/2rw;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1772967
    invoke-virtual {v2}, LX/8AA;->b()LX/8AA;

    .line 1772968
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v3, v3, LX/BKm;->a:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    move-object v0, v1

    .line 1772969
    const/16 v1, 0x32

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1772970
    return-void
.end method

.method public static y(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1772971
    invoke-direct {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->z()Z

    move-result v4

    .line 1772972
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->A(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)Z

    move-result v5

    .line 1772973
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->l(LX/0Px;)Z

    move-result v6

    .line 1772974
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->j(LX/0Px;)Z

    move-result v7

    .line 1772975
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->i()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 1772976
    :goto_0
    iget-object v3, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v3, v3, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v3}, Lcom/facebook/platform/composer/model/PlatformComposition;->c()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v3

    if-eqz v3, :cond_3

    move v3, v1

    .line 1772977
    :goto_1
    iget-object v8, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ag:Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;

    sget-object v9, LX/BKc;->PHOTO:LX/BKc;

    if-nez v4, :cond_4

    if-nez v5, :cond_4

    move v4, v1

    :goto_2
    invoke-virtual {v8, v9, v4}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->c(LX/BKc;Z)V

    .line 1772978
    iget-object v4, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ag:Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;

    sget-object v5, LX/BKc;->ALBUM:LX/BKc;

    if-eqz v6, :cond_5

    if-nez v7, :cond_5

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->e:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-object v0, v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v8, LX/2rw;->UNDIRECTED:LX/2rw;

    if-ne v0, v8, :cond_5

    move v0, v1

    :goto_3
    invoke-virtual {v4, v5, v0}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->c(LX/BKc;Z)V

    .line 1772979
    iget-object v4, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ag:Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;

    sget-object v5, LX/BKc;->MINUTIAE:LX/BKc;

    if-nez v3, :cond_6

    move v0, v1

    :goto_4
    invoke-virtual {v4, v5, v0}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->c(LX/BKc;Z)V

    .line 1772980
    iget-object v3, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ag:Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;

    sget-object v4, LX/BKc;->PHOTO:LX/BKc;

    if-nez v6, :cond_0

    if-eqz v7, :cond_7

    :cond_0
    move v0, v1

    :goto_5
    invoke-virtual {v3, v4, v0}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->a(LX/BKc;Z)V

    .line 1772981
    iget-object v3, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ag:Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;

    sget-object v4, LX/BKc;->ALBUM:LX/BKc;

    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->c()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    if-eqz v0, :cond_8

    move v0, v1

    :goto_6
    invoke-virtual {v3, v4, v0}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->a(LX/BKc;Z)V

    .line 1772982
    iget-object v3, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ag:Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;

    sget-object v4, LX/BKc;->PEOPLE_TAGGING:LX/BKc;

    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->u()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    move v0, v1

    :goto_7
    invoke-virtual {v3, v4, v0}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->a(LX/BKc;Z)V

    .line 1772983
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ag:Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;

    sget-object v3, LX/BKc;->MINUTIAE:LX/BKc;

    iget-object v4, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v4, v4, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v4}, Lcom/facebook/platform/composer/model/PlatformComposition;->r()Z

    move-result v4

    invoke-virtual {v0, v3, v4}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->a(LX/BKc;Z)V

    .line 1772984
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ag:Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;

    sget-object v3, LX/BKc;->PLACE_TAGGING:LX/BKc;

    iget-object v4, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v4, v4, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v4}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v4

    if-eqz v4, :cond_a

    :goto_8
    invoke-virtual {v0, v3, v1}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->a(LX/BKc;Z)V

    .line 1772985
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->t:LX/BKi;

    iget-boolean v0, v0, LX/BKi;->f:Z

    if-eqz v0, :cond_1

    .line 1772986
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ag:Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->a()V

    .line 1772987
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 1772988
    goto/16 :goto_0

    :cond_3
    move v3, v2

    .line 1772989
    goto/16 :goto_1

    :cond_4
    move v4, v2

    .line 1772990
    goto/16 :goto_2

    :cond_5
    move v0, v2

    .line 1772991
    goto :goto_3

    :cond_6
    move v0, v2

    .line 1772992
    goto :goto_4

    :cond_7
    move v0, v2

    .line 1772993
    goto :goto_5

    :cond_8
    move v0, v2

    .line 1772994
    goto :goto_6

    :cond_9
    move v0, v2

    .line 1772995
    goto :goto_7

    :cond_a
    move v1, v2

    .line 1772996
    goto :goto_8
.end method

.method private z()Z
    .locals 1

    .prologue
    .line 1772997
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->p()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->p()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1772998
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1772999
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1773000
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->w:LX/0if;

    sget-object v1, LX/0ig;->k:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->a(LX/0ih;)V

    .line 1773001
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->w:LX/0if;

    sget-object v1, LX/0ig;->k:LX/0ih;

    const-string v2, "on_fragment_created"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1773002
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    if-nez v0, :cond_0

    .line 1773003
    if-nez p1, :cond_3

    .line 1773004
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1773005
    const-string v1, "extra_composer_configuration"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1773006
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->r:LX/BKn;

    .line 1773007
    invoke-static {v1, v0}, LX/BKn;->b(LX/BKn;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)Lcom/facebook/platform/composer/model/PlatformComposition;

    move-result-object v2

    .line 1773008
    new-instance v3, LX/BKl;

    invoke-direct {v3}, LX/BKl;-><init>()V

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1773009
    iput-object v4, v3, LX/BKl;->a:Ljava/lang/String;

    .line 1773010
    move-object v3, v3

    .line 1773011
    new-instance v4, LX/7lP;

    invoke-direct {v4}, LX/7lP;-><init>()V

    const/4 p1, 0x0

    .line 1773012
    iput-boolean p1, v4, LX/7lP;->a:Z

    .line 1773013
    move-object v4, v4

    .line 1773014
    const/4 p1, 0x1

    .line 1773015
    iput-boolean p1, v4, LX/7lP;->c:Z

    .line 1773016
    move-object v4, v4

    .line 1773017
    invoke-virtual {v4}, LX/7lP;->a()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v4

    .line 1773018
    iput-object v4, v3, LX/BKl;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    .line 1773019
    move-object v3, v3

    .line 1773020
    iput-object v0, v3, LX/BKl;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1773021
    move-object v3, v3

    .line 1773022
    new-instance v4, LX/AP3;

    invoke-direct {v4}, LX/AP3;-><init>()V

    invoke-virtual {v4}, LX/AP3;->a()Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    move-result-object v4

    .line 1773023
    iput-object v4, v3, LX/BKl;->h:Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    .line 1773024
    move-object v3, v3

    .line 1773025
    iput-object v2, v3, LX/BKl;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    .line 1773026
    move-object v2, v3

    .line 1773027
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/BKl;->a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)LX/BKl;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v3

    .line 1773028
    iput-object v3, v2, LX/BKl;->i:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    .line 1773029
    move-object v2, v2

    .line 1773030
    invoke-virtual {v2}, LX/BKl;->a()LX/BKm;

    move-result-object v2

    move-object v0, v2

    .line 1773031
    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    .line 1773032
    :cond_0
    :goto_0
    new-instance v0, LX/BK4;

    invoke-direct {v0, p0}, LX/BK4;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    move-object v0, v0

    .line 1773033
    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->K:LX/BJm;

    .line 1773034
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ax:LX/BJq;

    .line 1773035
    new-instance v1, LX/BKj;

    invoke-direct {v1, v0}, LX/BKj;-><init>(LX/BJq;)V

    .line 1773036
    move-object v0, v1

    .line 1773037
    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->H:LX/BKj;

    .line 1773038
    invoke-direct {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->z()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->A(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->t:LX/BKi;

    iget-boolean v0, v0, LX/BKi;->g:Z

    if-eqz v0, :cond_1

    .line 1773039
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->t:LX/BKi;

    sget-object v1, LX/BKf;->MINI:LX/BKf;

    iput-object v1, v0, LX/BKi;->a:LX/BKf;

    .line 1773040
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->t:LX/BKi;

    sget-object v1, LX/BKh;->BOTTOM:LX/BKh;

    iput-object v1, v0, LX/BKi;->b:LX/BKh;

    .line 1773041
    :cond_1
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->w:LX/0if;

    sget-object v1, LX/0ig;->k:LX/0ih;

    .line 1773042
    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    if-nez v2, :cond_4

    .line 1773043
    :cond_2
    const-string v2, "unknown_attachment"

    .line 1773044
    :goto_1
    move-object v2, v2

    .line 1773045
    invoke-virtual {v0, v1, v2}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 1773046
    return-void

    .line 1773047
    :cond_3
    new-instance v0, LX/BKl;

    invoke-direct {v0}, LX/BKl;-><init>()V

    const-string v1, "session_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1773048
    iput-object v1, v0, LX/BKl;->a:Ljava/lang/String;

    .line 1773049
    move-object v1, v0

    .line 1773050
    const-string v0, "composition"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/platform/composer/model/PlatformComposition;

    .line 1773051
    iput-object v0, v1, LX/BKl;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    .line 1773052
    move-object v1, v1

    .line 1773053
    const-string v0, "configuration"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1773054
    iput-object v0, v1, LX/BKl;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1773055
    move-object v1, v1

    .line 1773056
    const-string v0, "viewer_coordinates"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerLocation;

    .line 1773057
    iput-object v0, v1, LX/BKl;->f:Lcom/facebook/ipc/composer/model/ComposerLocation;

    .line 1773058
    move-object v1, v1

    .line 1773059
    const-string v0, "target_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v1, v0}, LX/BKl;->a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)LX/BKl;

    move-result-object v1

    const-string v0, "privacy_override"

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1773060
    iput-object v0, v1, LX/BKl;->g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1773061
    move-object v1, v1

    .line 1773062
    const-string v0, "audience_educator_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    .line 1773063
    iput-object v0, v1, LX/BKl;->h:Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    .line 1773064
    move-object v0, v1

    .line 1773065
    const-string v1, "has_shown_tag_place_tip_or_suggestion"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 1773066
    iput-boolean v1, v0, LX/BKl;->m:Z

    .line 1773067
    move-object v0, v0

    .line 1773068
    const-string v1, "privacy_has_changed"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 1773069
    iput-boolean v1, v0, LX/BKl;->l:Z

    .line 1773070
    move-object v0, v0

    .line 1773071
    const-string v1, "post_composition_shown"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 1773072
    iput-boolean v1, v0, LX/BKl;->n:Z

    .line 1773073
    move-object v1, v0

    .line 1773074
    const-string v0, "page_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerPageData;

    .line 1773075
    iput-object v0, v1, LX/BKl;->i:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    .line 1773076
    move-object v0, v1

    .line 1773077
    const-string v1, "draft_load_attempts"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 1773078
    iput v1, v0, LX/BKl;->j:I

    .line 1773079
    move-object v0, v0

    .line 1773080
    const-string v1, "status_view_scroll_pos"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 1773081
    iput v1, v0, LX/BKl;->k:I

    .line 1773082
    move-object v0, v0

    .line 1773083
    new-instance v1, LX/7lP;

    invoke-direct {v1}, LX/7lP;-><init>()V

    const/4 v2, 0x0

    .line 1773084
    iput-boolean v2, v1, LX/7lP;->a:Z

    .line 1773085
    move-object v1, v1

    .line 1773086
    const/4 v2, 0x1

    .line 1773087
    iput-boolean v2, v1, LX/7lP;->c:Z

    .line 1773088
    move-object v1, v1

    .line 1773089
    invoke-virtual {v1}, LX/7lP;->a()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v1

    .line 1773090
    iput-object v1, v0, LX/BKl;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    .line 1773091
    move-object v1, v0

    .line 1773092
    const-string v0, "rich_text_style"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    .line 1773093
    iput-object v0, v1, LX/BKl;->o:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    .line 1773094
    move-object v0, v1

    .line 1773095
    invoke-virtual {v0}, LX/BKl;->a()LX/BKm;

    move-result-object v0

    move-object v0, v0

    .line 1773096
    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    goto/16 :goto_0

    .line 1773097
    :cond_4
    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/7kq;->j(LX/0Px;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1773098
    const-string v2, "video_attachment"

    goto/16 :goto_1

    .line 1773099
    :cond_5
    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/7kq;->l(LX/0Px;)Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_6

    .line 1773100
    const-string v2, "multi_photo_attachment"

    goto/16 :goto_1

    .line 1773101
    :cond_6
    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/7kq;->l(LX/0Px;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1773102
    const-string v2, "single_photo_attachment"

    goto/16 :goto_1

    .line 1773103
    :cond_7
    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->p()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v2

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->p()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 1773104
    const-string v2, "link_attachment"

    goto/16 :goto_1

    .line 1773105
    :cond_8
    const-string v2, "text_only_attachment"

    goto/16 :goto_1
.end method

.method public final a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 2
    .param p2    # Lcom/facebook/auth/viewercontext/ViewerContext;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1772875
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    invoke-virtual {v0}, LX/BKm;->a()LX/BKl;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/BKl;->a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)LX/BKl;

    move-result-object v0

    invoke-virtual {v0}, LX/BKl;->a()LX/BKm;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    .line 1772876
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v1, LX/2rw;->PAGE:LX/2rw;

    if-ne v0, v1, :cond_0

    .line 1772877
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    invoke-virtual {v0}, LX/BKm;->a()LX/BKl;

    move-result-object v0

    invoke-static {}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->newBuilder()Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPostAsPageViewerContext(Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v1

    .line 1772878
    iput-object v1, v0, LX/BKl;->i:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    .line 1772879
    move-object v0, v0

    .line 1772880
    invoke-virtual {v0}, LX/BKl;->a()LX/BKm;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    .line 1772881
    :goto_0
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ad:LX/BKt;

    sget-object v1, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v0, v1}, LX/BKt;->a(LX/5L2;)V

    .line 1772882
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->y(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    .line 1772883
    invoke-direct {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->R()V

    .line 1772884
    return-void

    .line 1772885
    :cond_0
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    invoke-virtual {v0}, LX/BKm;->a()LX/BKl;

    move-result-object v0

    const/4 v1, 0x0

    .line 1772886
    iput-object v1, v0, LX/BKl;->i:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    .line 1772887
    move-object v0, v0

    .line 1772888
    invoke-virtual {v0}, LX/BKl;->a()LX/BKm;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    goto :goto_0
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 1773106
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->w:LX/0if;

    sget-object v1, LX/0ig;->k:LX/0ih;

    const-string v2, "system_back_button_pressed"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1773107
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->a$redex0(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;ILandroid/content/Intent;Z)V

    .line 1773108
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 14

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x4f893179

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1773109
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1773110
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->A(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1773111
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getPlatformConfiguration()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v0

    .line 1773112
    if-nez v0, :cond_3

    .line 1773113
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Must have platform configuration to share OG."

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08249d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v0, v2}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->a$redex0(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 1773114
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->w()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->w()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/share/model/ComposerAppAttribution;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->w()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/share/model/ComposerAppAttribution;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->w()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/share/model/ComposerAppAttribution;->a()Ljava/lang/String;

    move-result-object v0

    const-string v2, "828784963877854"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1773115
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->w()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v0

    .line 1773116
    if-nez v0, :cond_4

    .line 1773117
    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08249e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v0, v2}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->a$redex0(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 1773118
    :cond_1
    :goto_1
    :try_start_0
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->j()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-eqz v0, :cond_2

    .line 1773119
    iget-object v6, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v6, v6, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v6}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->j()J

    move-result-wide v6

    .line 1773120
    iget-object v8, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->z:LX/1Ck;

    sget-object v9, LX/BKP;->FETCH_PLACE_INFO:LX/BKP;

    iget-object v10, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->C:LX/BJN;

    .line 1773121
    new-instance v11, LX/2rd;

    invoke-direct {v11}, LX/2rd;-><init>()V

    move-object v11, v11

    .line 1773122
    const-string v12, "page_id"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v11

    check-cast v11, LX/2rd;

    invoke-static {v11}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v11

    .line 1773123
    iget-object v12, v10, LX/BJN;->d:LX/0tX;

    invoke-virtual {v12, v11}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v11

    new-instance v12, LX/BJM;

    invoke-direct {v12, v10}, LX/BJM;-><init>(LX/BJN;)V

    iget-object v13, v10, LX/BJN;->c:Ljava/util/concurrent/Executor;

    invoke-static {v11, v12, v13}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v11

    move-object v6, v11

    .line 1773124
    new-instance v7, LX/BKC;

    invoke-direct {v7, p0}, LX/BKC;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    invoke-virtual {v8, v9, v6, v7}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1773125
    :cond_2
    :goto_2
    const v0, 0x8d1c1bf

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 1773126
    :catch_0
    move-exception v0

    .line 1773127
    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->s:LX/03V;

    const-string v3, "platform_composer_bad_place_id"

    invoke-virtual {v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1773128
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    invoke-virtual {v0}, LX/BKm;->a()LX/BKl;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->A()LX/BKp;

    move-result-object v2

    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->newBuilder()LX/5RO;

    move-result-object v3

    invoke-virtual {v3}, LX/5RO;->b()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v3

    .line 1773129
    iput-object v3, v2, LX/BKp;->g:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    .line 1773130
    move-object v2, v2

    .line 1773131
    invoke-virtual {v2}, LX/BKp;->b()Lcom/facebook/platform/composer/model/PlatformComposition;

    move-result-object v2

    .line 1773132
    iput-object v2, v0, LX/BKl;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    .line 1773133
    move-object v0, v0

    .line 1773134
    invoke-virtual {v0}, LX/BKl;->a()LX/BKm;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    .line 1773135
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->E(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    .line 1773136
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->y(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    goto :goto_2

    .line 1773137
    :cond_3
    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->z:LX/1Ck;

    sget-object v3, LX/BKP;->FETCH_ROBOTEXT:LX/BKP;

    iget-object v4, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->C:LX/BJN;

    iget-object v5, v0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->ogActionJsonForRobotext:Ljava/lang/String;

    iget-object v0, v0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->ogActionType:Ljava/lang/String;

    iget-object p1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object p1, p1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {p1}, Lcom/facebook/platform/composer/model/PlatformComposition;->w()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object p1

    invoke-virtual {v4, v5, v0, p1}, LX/BJN;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v4, LX/BKA;

    invoke-direct {v4, p0}, LX/BKA;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    invoke-virtual {v2, v3, v0, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto/16 :goto_0

    .line 1773138
    :cond_4
    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->z:LX/1Ck;

    sget-object v3, LX/BKP;->FETCH_APP_NAME:LX/BKP;

    iget-object v4, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->C:LX/BJN;

    invoke-virtual {v0}, Lcom/facebook/share/model/ComposerAppAttribution;->a()Ljava/lang/String;

    move-result-object v5

    .line 1773139
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 1773140
    new-instance v7, Lcom/facebook/platform/server/protocol/GetAppNameMethod$Params;

    invoke-direct {v7, v5}, Lcom/facebook/platform/server/protocol/GetAppNameMethod$Params;-><init>(Ljava/lang/String;)V

    .line 1773141
    const-string v8, "app_name"

    invoke-virtual {v6, v8, v7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1773142
    iget-object v7, v4, LX/BJN;->a:LX/0aG;

    const-string v8, "platform_get_app_name"

    const p1, -0x6f44b133

    invoke-static {v7, v8, v6, p1}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v6

    invoke-interface {v6}, LX/1MF;->start()LX/1ML;

    move-result-object v6

    .line 1773143
    new-instance v7, LX/BJL;

    invoke-direct {v7, v4}, LX/BJL;-><init>(LX/BJN;)V

    iget-object v8, v4, LX/BJN;->c:Ljava/util/concurrent/Executor;

    invoke-static {v6, v7, v8}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    move-object v4, v6

    .line 1773144
    new-instance v5, LX/BKB;

    invoke-direct {v5, p0, v0}, LX/BKB;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;Lcom/facebook/share/model/ComposerAppAttribution;)V

    invoke-virtual {v2, v3, v4, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto/16 :goto_1
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p3    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1773145
    sparse-switch p1, :sswitch_data_0

    .line 1773146
    :goto_0
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->y(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    .line 1773147
    return-void

    .line 1773148
    :sswitch_0
    const/4 v0, -0x1

    if-eq p2, v0, :cond_2

    .line 1773149
    :goto_1
    goto :goto_0

    .line 1773150
    :sswitch_1
    const/4 v0, -0x1

    if-eq p2, v0, :cond_4

    .line 1773151
    :goto_2
    goto :goto_0

    .line 1773152
    :sswitch_2
    const/4 v0, -0x1

    if-eq p2, v0, :cond_6

    .line 1773153
    :goto_3
    goto :goto_0

    .line 1773154
    :sswitch_3
    if-nez p2, :cond_7

    .line 1773155
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->w:LX/0if;

    sget-object v1, LX/0ig;->k:LX/0ih;

    const-string v2, "place_tag_cancelled"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1773156
    :cond_0
    :goto_4
    goto :goto_0

    .line 1773157
    :sswitch_4
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1773158
    const/4 v0, -0x1

    if-eq p2, v0, :cond_c

    .line 1773159
    :goto_5
    goto :goto_0

    .line 1773160
    :sswitch_5
    invoke-direct {p0, p2, p3}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->b(ILandroid/content/Intent;)V

    goto :goto_0

    .line 1773161
    :sswitch_6
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->Y:LX/BJG;

    .line 1773162
    const/4 v1, -0x1

    if-ne p2, v1, :cond_1

    .line 1773163
    const-string v1, "extra_album"

    invoke-static {p3, v1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 1773164
    iget-object v2, v0, LX/BJG;->g:LX/BK9;

    invoke-virtual {v2, v1}, LX/BK9;->a(Lcom/facebook/graphql/model/GraphQLAlbum;)V

    .line 1773165
    :cond_1
    goto :goto_0

    .line 1773166
    :cond_2
    const/4 v0, 0x0

    .line 1773167
    if-eqz p3, :cond_3

    const-string p1, "minutiae_object"

    invoke-virtual {p3, p1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 1773168
    const-string v0, "minutiae_object"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1773169
    :cond_3
    invoke-static {p0, v0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->a(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;Lcom/facebook/composer/minutiae/model/MinutiaeObject;)V

    .line 1773170
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->B(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    goto :goto_1

    .line 1773171
    :cond_4
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    invoke-virtual {v0}, LX/BKm;->a()LX/BKl;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v1, v1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v1}, Lcom/facebook/platform/composer/model/PlatformComposition;->A()LX/BKp;

    move-result-object v1

    const/4 p1, 0x1

    .line 1773172
    iput-boolean p1, v1, LX/BKp;->k:Z

    .line 1773173
    move-object v1, v1

    .line 1773174
    invoke-virtual {v1}, LX/BKp;->b()Lcom/facebook/platform/composer/model/PlatformComposition;

    move-result-object v1

    .line 1773175
    iput-object v1, v0, LX/BKl;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    .line 1773176
    move-object v0, v0

    .line 1773177
    invoke-virtual {v0}, LX/BKl;->a()LX/BKm;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    .line 1773178
    const-string v0, "full_profiles"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1773179
    const-string v0, "full_profiles"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1773180
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    invoke-virtual {v1}, LX/BKm;->a()LX/BKl;

    move-result-object v1

    iget-object p1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object p1, p1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {p1}, Lcom/facebook/platform/composer/model/PlatformComposition;->A()LX/BKp;

    move-result-object p1

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/BKp;->b(LX/0Px;)LX/BKp;

    move-result-object v0

    invoke-virtual {v0}, LX/BKp;->b()Lcom/facebook/platform/composer/model/PlatformComposition;

    move-result-object v0

    .line 1773181
    iput-object v0, v1, LX/BKl;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    .line 1773182
    move-object v0, v1

    .line 1773183
    invoke-virtual {v0}, LX/BKl;->a()LX/BKm;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    .line 1773184
    :cond_5
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->B(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    .line 1773185
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ad:LX/BKt;

    sget-object v1, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v0, v1}, LX/BKt;->a(LX/5L2;)V

    goto/16 :goto_2

    .line 1773186
    :cond_6
    const-string v0, "extra_composer_target_data"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 1773187
    const-string v1, "extra_actor_viewer_context"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1773188
    invoke-virtual {p0, v0, v1}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    goto/16 :goto_3

    .line 1773189
    :cond_7
    const/4 v0, -0x1

    if-eq p2, v0, :cond_8

    .line 1773190
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->w:LX/0if;

    sget-object v1, LX/0ig;->k:LX/0ih;

    const-string v2, "place_tag_selected"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1773191
    :cond_8
    if-eqz p3, :cond_0

    .line 1773192
    const-string v0, "extra_xed_location"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1773193
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    invoke-virtual {v0}, LX/BKm;->a()LX/BKl;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v1, v1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v1}, Lcom/facebook/platform/composer/model/PlatformComposition;->A()LX/BKp;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)LX/5RO;

    move-result-object v2

    invoke-virtual {v2}, LX/5RO;->a()LX/5RO;

    move-result-object v2

    invoke-virtual {v2}, LX/5RO;->b()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    .line 1773194
    iput-object v2, v1, LX/BKp;->g:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    .line 1773195
    move-object v1, v1

    .line 1773196
    invoke-virtual {v1}, LX/BKp;->b()Lcom/facebook/platform/composer/model/PlatformComposition;

    move-result-object v1

    .line 1773197
    iput-object v1, v0, LX/BKl;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    .line 1773198
    move-object v0, v0

    .line 1773199
    invoke-virtual {v0}, LX/BKl;->a()LX/BKm;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    .line 1773200
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->E(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    .line 1773201
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->u(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    .line 1773202
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->y(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    .line 1773203
    goto/16 :goto_4

    .line 1773204
    :cond_9
    const-string v0, "text_only_place"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1773205
    const-string v0, "text_only_place"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1773206
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    invoke-virtual {v1}, LX/BKm;->a()LX/BKl;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->A()LX/BKp;

    move-result-object v2

    iget-object p2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object p2, p2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {p2}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object p2

    invoke-static {p2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)LX/5RO;

    move-result-object p2

    invoke-virtual {p2, v0}, LX/5RO;->a(Ljava/lang/String;)LX/5RO;

    move-result-object p2

    invoke-virtual {p2}, LX/5RO;->b()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object p2

    .line 1773207
    iput-object p2, v2, LX/BKp;->g:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    .line 1773208
    move-object v2, v2

    .line 1773209
    invoke-virtual {v2}, LX/BKp;->b()Lcom/facebook/platform/composer/model/PlatformComposition;

    move-result-object v2

    .line 1773210
    iput-object v2, v1, LX/BKl;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    .line 1773211
    move-object v1, v1

    .line 1773212
    invoke-virtual {v1}, LX/BKl;->a()LX/BKm;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    .line 1773213
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->E(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    .line 1773214
    goto/16 :goto_4

    .line 1773215
    :cond_a
    const-string v0, "extra_place"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    const-string v1, "extra_implicit_location"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    const-string v2, "minutiae_object"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1773216
    iget-object p1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    invoke-virtual {p1}, LX/BKm;->a()LX/BKl;

    move-result-object p1

    iget-object p2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object p2, p2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {p2}, Lcom/facebook/platform/composer/model/PlatformComposition;->A()LX/BKp;

    move-result-object p2

    iget-object p3, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object p3, p3, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {p3}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object p3

    invoke-static {p3}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)LX/5RO;

    move-result-object p3

    invoke-virtual {p3, v0, v1}, LX/5RO;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;)LX/5RO;

    move-result-object p3

    invoke-virtual {p3}, LX/5RO;->b()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object p3

    .line 1773217
    iput-object p3, p2, LX/BKp;->g:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    .line 1773218
    move-object p2, p2

    .line 1773219
    invoke-virtual {p2}, LX/BKp;->b()Lcom/facebook/platform/composer/model/PlatformComposition;

    move-result-object p2

    .line 1773220
    iput-object p2, p1, LX/BKl;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    .line 1773221
    move-object p1, p1

    .line 1773222
    invoke-virtual {p1}, LX/BKl;->a()LX/BKm;

    move-result-object p1

    iput-object p1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    .line 1773223
    if-eqz v2, :cond_b

    .line 1773224
    invoke-static {p0, v2}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->a(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;Lcom/facebook/composer/minutiae/model/MinutiaeObject;)V

    .line 1773225
    :cond_b
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->E(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    .line 1773226
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->B(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    .line 1773227
    goto/16 :goto_4

    .line 1773228
    :cond_c
    const-string v0, "extra_media_items"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1773229
    const-string v0, "extra_media_items"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1773230
    if-eqz v0, :cond_f

    .line 1773231
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 1773232
    :goto_6
    move-object v3, v0

    .line 1773233
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_e

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 1773234
    iget-object p1, v0, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    move-object v0, p1

    .line 1773235
    sget-object p1, LX/4gQ;->Photo:LX/4gQ;

    if-ne v0, p1, :cond_e

    move v0, v1

    .line 1773236
    :goto_7
    if-eqz v0, :cond_d

    .line 1773237
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->l()LX/5Rn;

    move-result-object v0

    sget-object v2, LX/5Rn;->SAVE_DRAFT:LX/5Rn;

    if-ne v0, v2, :cond_d

    .line 1773238
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    invoke-virtual {v0}, LX/BKm;->a()LX/BKl;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->A()LX/BKp;

    move-result-object v2

    sget-object p1, LX/5Rn;->NORMAL:LX/5Rn;

    .line 1773239
    iput-object p1, v2, LX/BKp;->m:LX/5Rn;

    .line 1773240
    move-object v2, v2

    .line 1773241
    invoke-virtual {v2}, LX/BKp;->b()Lcom/facebook/platform/composer/model/PlatformComposition;

    move-result-object v2

    .line 1773242
    iput-object v2, v0, LX/BKl;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    .line 1773243
    move-object v0, v0

    .line 1773244
    invoke-virtual {v0}, LX/BKl;->a()LX/BKm;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    .line 1773245
    :cond_d
    invoke-static {v3}, Lcom/facebook/composer/attachments/ComposerAttachment;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    invoke-static {p0, v0, v1, v1, v1}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->a$redex0(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;LX/0Px;ZZZ)V

    .line 1773246
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->m$redex0(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    goto/16 :goto_5

    :cond_e
    move v0, v2

    .line 1773247
    goto :goto_7

    .line 1773248
    :cond_f
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1773249
    goto :goto_6

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x14 -> :sswitch_1
        0x1e -> :sswitch_2
        0x28 -> :sswitch_3
        0x32 -> :sswitch_4
        0x3c -> :sswitch_5
        0x3e8 -> :sswitch_6
    .end sparse-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x474bceb3

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 1773250
    const/4 v0, 0x0

    .line 1773251
    sget-object v1, LX/BKD;->a:[I

    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->t:LX/BKi;

    iget-object v2, v2, LX/BKi;->a:LX/BKf;

    invoke-virtual {v2}, LX/BKf;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    move-object v6, v0

    .line 1773252
    :goto_0
    const v0, 0x7f0d25fa

    invoke-virtual {v6, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1773253
    const v1, 0x7f0d0ac2

    invoke-virtual {v6, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->aa:Landroid/view/View;

    .line 1773254
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->t:LX/BKi;

    iget-boolean v1, v1, LX/BKi;->e:Z

    if-nez v1, :cond_0

    .line 1773255
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->aa:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1773256
    :cond_0
    const v1, 0x7f0d050d

    invoke-virtual {v6, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;

    iput-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ab:Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;

    .line 1773257
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ab:Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;

    new-instance v2, LX/BJr;

    invoke-direct {v2, p0}, LX/BJr;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    invoke-virtual {v1, v2}, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1773258
    const v1, 0x7f0d050f

    invoke-virtual {v6, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;

    iput-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ac:Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;

    .line 1773259
    const v1, 0x7f0d1a78

    invoke-virtual {v6, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->Q:Landroid/view/View;

    .line 1773260
    const v1, 0x7f0d2604

    invoke-virtual {v6, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->R:Landroid/view/View;

    .line 1773261
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->Q:Landroid/view/View;

    new-instance v2, LX/BJs;

    invoke-direct {v2, p0}, LX/BJs;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1773262
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->R:Landroid/view/View;

    new-instance v2, LX/BJt;

    invoke-direct {v2, p0}, LX/BJt;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1773263
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->t:LX/BKi;

    iget-object v1, v1, LX/BKi;->a:LX/BKf;

    sget-object v2, LX/BKf;->MINI:LX/BKf;

    if-ne v1, v2, :cond_1

    .line 1773264
    const v1, 0x7f0d25fb

    invoke-virtual {v6, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1773265
    if-eqz v1, :cond_1

    .line 1773266
    new-instance v2, LX/BJu;

    invoke-direct {v2, p0}, LX/BJu;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1773267
    :cond_1
    const v1, 0x7f0d1913

    invoke-virtual {v6, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;

    iput-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->L:Lcom/facebook/platform/composer/composer/PlatformComposerEditText;

    .line 1773268
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->L:Lcom/facebook/platform/composer/composer/PlatformComposerEditText;

    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->K:LX/BJm;

    invoke-virtual {v1, v2}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->a(LX/BJm;)V

    .line 1773269
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->af:Ljava/lang/CharSequence;

    aput-object v3, v1, v2

    invoke-static {v1}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1773270
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->L:Lcom/facebook/platform/composer/composer/PlatformComposerEditText;

    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->af:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1773271
    :cond_2
    const v1, 0x7f0d0aba

    invoke-virtual {v6, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;

    iput-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ag:Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;

    .line 1773272
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ag:Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;

    sget-object v2, LX/BKc;->MINUTIAE:LX/BKc;

    new-instance v3, LX/BJv;

    invoke-direct {v3, p0}, LX/BJv;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    invoke-virtual {v1, v2, v3}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->a(LX/BKc;Landroid/view/View$OnClickListener;)V

    .line 1773273
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ag:Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;

    sget-object v2, LX/BKc;->ALBUM:LX/BKc;

    new-instance v3, LX/BJw;

    invoke-direct {v3, p0}, LX/BJw;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    invoke-virtual {v1, v2, v3}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->a(LX/BKc;Landroid/view/View$OnClickListener;)V

    .line 1773274
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ag:Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;

    sget-object v2, LX/BKc;->PEOPLE_TAGGING:LX/BKc;

    new-instance v3, LX/BJy;

    invoke-direct {v3, p0}, LX/BJy;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    invoke-virtual {v1, v2, v3}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->a(LX/BKc;Landroid/view/View$OnClickListener;)V

    .line 1773275
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ag:Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;

    sget-object v2, LX/BKc;->PLACE_TAGGING:LX/BKc;

    new-instance v3, LX/BJz;

    invoke-direct {v3, p0}, LX/BJz;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    invoke-virtual {v1, v2, v3}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->a(LX/BKc;Landroid/view/View$OnClickListener;)V

    .line 1773276
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ag:Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;

    sget-object v2, LX/BKc;->PHOTO:LX/BKc;

    new-instance v3, LX/BK0;

    invoke-direct {v3, p0}, LX/BK0;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    invoke-virtual {v1, v2, v3}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->a(LX/BKc;Landroid/view/View$OnClickListener;)V

    .line 1773277
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->t:LX/BKi;

    iget-object v1, v1, LX/BKi;->a:LX/BKf;

    sget-object v2, LX/BKf;->MINI:LX/BKf;

    if-ne v1, v2, :cond_9

    .line 1773278
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ag:Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->setMaxVisibleButtonAllowed(I)V

    .line 1773279
    :cond_3
    :goto_1
    new-instance v1, LX/BK1;

    invoke-direct {v1, p0}, LX/BK1;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    .line 1773280
    sget-object v2, LX/BKD;->b:[I

    iget-object v3, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->t:LX/BKi;

    iget-object v3, v3, LX/BKi;->b:LX/BKh;

    invoke-virtual {v3}, LX/BKh;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    .line 1773281
    :goto_2
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->a:LX/BJS;

    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ak:LX/BJx;

    iget-object v3, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->al:LX/BK8;

    invoke-virtual {v1, v2, v3}, LX/BJS;->a(LX/BJx;LX/BK8;)LX/BJR;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->Z:LX/BJR;

    .line 1773282
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v1, v1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v1}, Lcom/facebook/platform/composer/model/PlatformComposition;->p()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 1773283
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    const-string v2, "ATTACHMENT_TAG"

    invoke-virtual {v1, v2}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;

    iput-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->G:Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;

    .line 1773284
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->G:Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;

    if-eqz v1, :cond_4

    .line 1773285
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->G:Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;

    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->Z:LX/BJR;

    .line 1773286
    iput-object v2, v1, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->a:LX/BJR;

    .line 1773287
    :cond_4
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->u(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    .line 1773288
    :cond_5
    const v1, 0x7f0d0aee

    invoke-virtual {v6, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/ScrollingAwareScrollView;

    iput-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->W:Lcom/facebook/widget/ScrollingAwareScrollView;

    .line 1773289
    const v1, 0x7f0d0af1

    invoke-virtual {v6, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;

    iput-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->X:Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;

    .line 1773290
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->t:LX/BKi;

    iget-object v1, v1, LX/BKi;->c:LX/BKg;

    sget-object v2, LX/BKg;->TARGET_PRIVACY:LX/BKg;

    if-ne v1, v2, :cond_a

    .line 1773291
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->X:Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;

    invoke-virtual {v1}, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->a()V

    .line 1773292
    :goto_3
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v1, v1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v1, v1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v1}, Lcom/facebook/platform/composer/model/PlatformComposition;->y()Z

    move-result v1

    if-nez v1, :cond_6

    .line 1773293
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->X:Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;

    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->setAppProvidedHashtag(Ljava/lang/String;)V

    .line 1773294
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->X:Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;

    new-instance v2, LX/BK2;

    invoke-direct {v2, p0}, LX/BK2;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    invoke-virtual {v1, v2}, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->setListener(LX/BJJ;)V

    .line 1773295
    :cond_6
    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->p:LX/BKV;

    const v1, 0x7f0d25fc

    invoke-virtual {v6, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    invoke-virtual {v2, v1}, LX/BKV;->a(Landroid/view/ViewStub;)LX/BKU;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ai:LX/BKU;

    .line 1773296
    invoke-direct {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->z()Z

    move-result v1

    if-nez v1, :cond_7

    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->A(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1773297
    :cond_7
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ai:LX/BKU;

    const/16 v2, 0x8

    .line 1773298
    iget-object v3, v1, LX/BKU;->h:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1773299
    :cond_8
    invoke-direct {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->l()V

    .line 1773300
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ai:LX/BKU;

    const/4 v2, 0x1

    .line 1773301
    iput-boolean v2, v1, LX/BKU;->n:Z

    .line 1773302
    iget-object v3, v1, LX/BKU;->o:LX/ATO;

    invoke-virtual {v3, v2}, LX/ATO;->c(Z)V

    .line 1773303
    if-eqz v2, :cond_c

    .line 1773304
    iget-object v3, v1, LX/BKU;->l:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1773305
    :goto_4
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ai:LX/BKU;

    .line 1773306
    new-instance v2, LX/BK6;

    invoke-direct {v2, p0}, LX/BK6;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    move-object v2, v2

    .line 1773307
    iget-object v3, v1, LX/BKU;->l:Landroid/view/View;

    new-instance v4, LX/BKT;

    invoke-direct {v4, v1, v2}, LX/BKT;-><init>(LX/BKU;LX/ASe;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1773308
    iget-object v3, v1, LX/BKU;->o:LX/ATO;

    .line 1773309
    iput-object v2, v3, LX/ATO;->B:LX/ASe;

    .line 1773310
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->a(Z)V

    .line 1773311
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->L:Lcom/facebook/platform/composer/composer/PlatformComposerEditText;

    invoke-virtual {v1}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->requestFocus()Z

    .line 1773312
    invoke-virtual {v6}, Landroid/view/ViewGroup;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    .line 1773313
    new-instance v2, LX/BK3;

    invoke-direct {v2, p0}, LX/BK3;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1773314
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->k:LX/0wW;

    invoke-virtual {v1}, LX/0wW;->a()LX/0wd;

    move-result-object v1

    const-wide/high16 v2, 0x402e000000000000L    # 15.0

    const-wide/high16 v4, 0x401c000000000000L    # 7.0

    invoke-static {v2, v3, v4, v5}, LX/0wT;->b(DD)LX/0wT;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v1

    new-instance v2, LX/BKQ;

    const/4 v3, 0x0

    invoke-direct {v2, p0}, LX/BKQ;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, LX/0wd;->b(D)LX/0wd;

    move-result-object v1

    invoke-virtual {v1}, LX/0wd;->j()LX/0wd;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->N:LX/0wd;

    .line 1773315
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->k:LX/0wW;

    invoke-virtual {v1}, LX/0wW;->a()LX/0wd;

    move-result-object v1

    const-wide/high16 v2, 0x4049000000000000L    # 50.0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    invoke-static {v2, v3, v4, v5}, LX/0wT;->b(DD)LX/0wT;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v1

    new-instance v2, LX/BKQ;

    const/4 v3, 0x0

    invoke-direct {v2, p0}, LX/BKQ;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v1

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v1, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v1

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v1, v2, v3}, LX/0wd;->b(D)LX/0wd;

    move-result-object v1

    invoke-virtual {v1}, LX/0wd;->j()LX/0wd;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->O:LX/0wd;

    .line 1773316
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->k:LX/0wW;

    invoke-virtual {v1}, LX/0wW;->a()LX/0wd;

    move-result-object v1

    const-wide/high16 v2, 0x402e000000000000L    # 15.0

    const-wide/high16 v4, 0x401c000000000000L    # 7.0

    invoke-static {v2, v3, v4, v5}, LX/0wT;->b(DD)LX/0wT;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v1

    new-instance v2, LX/BKQ;

    const/4 v3, 0x0

    invoke-direct {v2, p0}, LX/BKQ;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, LX/0wd;->b(D)LX/0wd;

    move-result-object v1

    invoke-virtual {v1}, LX/0wd;->j()LX/0wd;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->P:LX/0wd;

    .line 1773317
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->i:LX/BKu;

    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->am:LX/BKE;

    iget-object v3, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->an:LX/BKG;

    invoke-virtual {v1, v2, v3}, LX/BKu;->a(LX/BKE;LX/BKG;)LX/BKt;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ad:LX/BKt;

    .line 1773318
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ad:LX/BKt;

    .line 1773319
    iget-object v2, v1, LX/BKt;->a:LX/93Q;

    invoke-virtual {v2}, LX/93Q;->a()V

    .line 1773320
    invoke-direct {p0, v0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->a(Landroid/view/ViewStub;)LX/BLv;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->U:LX/BLv;

    .line 1773321
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->U:LX/BLv;

    invoke-virtual {v0}, LX/BLv;->a()V

    .line 1773322
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->q:LX/BJH;

    .line 1773323
    new-instance v1, LX/BK9;

    invoke-direct {v1, p0}, LX/BK9;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    move-object v1, v1

    .line 1773324
    new-instance v2, LX/BK5;

    invoke-direct {v2, p0}, LX/BK5;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    move-object v2, v2

    .line 1773325
    iget-object v3, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v3, v3, LX/BKm;->e:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-object v3, v3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v4, LX/2rw;->PAGE:LX/2rw;

    if-ne v3, v4, :cond_b

    iget-object v3, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v3, v3, LX/BKm;->i:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPostAsPageViewerContext()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v4

    :goto_5
    iget-object v3, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/user/model/User;

    .line 1773326
    iget-object v5, v3, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v5, v5

    .line 1773327
    move-object v3, p0

    invoke-virtual/range {v0 .. v5}, LX/BJH;->a(LX/BK9;LX/BK5;Landroid/support/v4/app/Fragment;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/String;)LX/BJG;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->Y:LX/BJG;

    .line 1773328
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->y(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    .line 1773329
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->m$redex0(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    .line 1773330
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ai:LX/BKU;

    .line 1773331
    iget-object v1, v0, LX/BKU;->o:LX/ATO;

    invoke-virtual {v1}, LX/ATO;->p()V

    .line 1773332
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->t$redex0(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    .line 1773333
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->E(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    .line 1773334
    const v0, -0x62a0cf91

    invoke-static {v0, v7}, LX/02F;->f(II)V

    return-object v6

    .line 1773335
    :pswitch_0
    const v0, 0x7f030fbf

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1773336
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ae:Z

    move-object v6, v0

    .line 1773337
    goto/16 :goto_0

    .line 1773338
    :pswitch_1
    const v0, 0x7f030fc0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    move-object v6, v0

    goto/16 :goto_0

    .line 1773339
    :cond_9
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->t:LX/BKi;

    iget-object v1, v1, LX/BKi;->a:LX/BKf;

    sget-object v2, LX/BKf;->FULL:LX/BKf;

    if-ne v1, v2, :cond_3

    .line 1773340
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ag:Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->setMaxVisibleButtonAllowed(I)V

    goto/16 :goto_1

    .line 1773341
    :pswitch_2
    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ag:Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;

    sget-object v3, LX/BKc;->POST:LX/BKc;

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->c(LX/BKc;Z)V

    .line 1773342
    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ag:Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;

    sget-object v3, LX/BKc;->BIG_POST:LX/BKc;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->c(LX/BKc;Z)V

    .line 1773343
    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ag:Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;

    sget-object v3, LX/BKc;->POST:LX/BKc;

    invoke-virtual {v2, v3, v1}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->a(LX/BKc;Landroid/view/View$OnClickListener;)V

    goto/16 :goto_2

    .line 1773344
    :pswitch_3
    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ag:Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;

    sget-object v3, LX/BKc;->POST:LX/BKc;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->c(LX/BKc;Z)V

    .line 1773345
    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ag:Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;

    sget-object v3, LX/BKc;->BIG_POST:LX/BKc;

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->c(LX/BKc;Z)V

    .line 1773346
    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ag:Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;

    sget-object v3, LX/BKc;->BIG_POST:LX/BKc;

    invoke-virtual {v2, v3, v1}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->a(LX/BKc;Landroid/view/View$OnClickListener;)V

    goto/16 :goto_2

    .line 1773347
    :pswitch_4
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ag:Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;

    sget-object v2, LX/BKc;->POST:LX/BKc;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->c(LX/BKc;Z)V

    .line 1773348
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ag:Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;

    sget-object v2, LX/BKc;->BIG_POST:LX/BKc;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->c(LX/BKc;Z)V

    goto/16 :goto_2

    .line 1773349
    :cond_a
    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->X:Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;

    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->setProfileImage(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1773350
    :cond_b
    const/4 v4, 0x0

    goto/16 :goto_5

    .line 1773351
    :cond_c
    iget-object v3, v1, LX/BKU;->l:Landroid/view/View;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6910d2d4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1773352
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->z:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 1773353
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1773354
    const/16 v1, 0x2b

    const v2, 0x18c39bf8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x23c651d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1773355
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1773356
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->L:Lcom/facebook/platform/composer/composer/PlatformComposerEditText;

    invoke-virtual {v3}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getUserText()Ljava/lang/CharSequence;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1773357
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->L:Lcom/facebook/platform/composer/composer/PlatformComposerEditText;

    invoke-virtual {v1}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getUserText()Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->af:Ljava/lang/CharSequence;

    .line 1773358
    :cond_0
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->Y:LX/BJG;

    .line 1773359
    iget-boolean v2, v1, LX/BJG;->l:Z

    if-nez v2, :cond_1

    .line 1773360
    :goto_0
    const/16 v1, 0x2b

    const v2, -0x66591a91

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1773361
    :cond_1
    iget-object v2, v1, LX/BJG;->b:LX/9bY;

    iget-object v3, v1, LX/BJG;->h:LX/BJD;

    invoke-virtual {v2, v3}, LX/0b4;->b(LX/0b2;)Z

    goto :goto_0
.end method

.method public final onResume()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x7291d78d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1773362
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1773363
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->y:LX/BKe;

    .line 1773364
    iget-object v2, v1, LX/BKe;->a:LX/11i;

    iget-object v4, v1, LX/BKe;->c:LX/BKd;

    invoke-interface {v2, v4}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v2

    .line 1773365
    if-eqz v2, :cond_0

    .line 1773366
    const-string v4, "PlatformComposerLaunch"

    const v5, 0x2ea8707f

    invoke-static {v2, v4, v5}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1773367
    :cond_0
    iget-object v2, v1, LX/BKe;->a:LX/11i;

    iget-object v4, v1, LX/BKe;->c:LX/BKd;

    invoke-interface {v2, v4}, LX/11i;->b(LX/0Pq;)V

    .line 1773368
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->Y:LX/BJG;

    invoke-virtual {v1}, LX/BJG;->a()V

    .line 1773369
    const/16 v1, 0x2b

    const v2, 0x5371b5ca

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1773370
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1773371
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    if-eqz v0, :cond_0

    .line 1773372
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    .line 1773373
    const-string v1, "session_id"

    iget-object p0, v0, LX/BKm;->a:Ljava/lang/String;

    invoke-virtual {p1, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1773374
    const-string v1, "composition"

    iget-object p0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {p1, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1773375
    const-string v1, "configuration"

    iget-object p0, v0, LX/BKm;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-virtual {p1, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1773376
    const-string v1, "viewer_coordinates"

    iget-object p0, v0, LX/BKm;->f:Lcom/facebook/ipc/composer/model/ComposerLocation;

    invoke-virtual {p1, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1773377
    const-string v1, "target_data"

    iget-object p0, v0, LX/BKm;->e:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {p1, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1773378
    const-string v1, "privacy_override"

    iget-object p0, v0, LX/BKm;->g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {p1, v1, p0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1773379
    const-string v1, "audience_educator_data"

    iget-object p0, v0, LX/BKm;->h:Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    invoke-virtual {p1, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1773380
    const-string v1, "has_shown_tag_place_tip_or_suggestion"

    iget-boolean p0, v0, LX/BKm;->m:Z

    invoke-virtual {p1, v1, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1773381
    const-string v1, "privacy_has_changed"

    iget-boolean p0, v0, LX/BKm;->k:Z

    invoke-virtual {p1, v1, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1773382
    const-string v1, "post_composition_shown"

    iget-boolean p0, v0, LX/BKm;->n:Z

    invoke-virtual {p1, v1, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1773383
    const-string v1, "page_data"

    iget-object p0, v0, LX/BKm;->i:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    invoke-virtual {p1, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1773384
    const-string v1, "draft_load_attempts"

    iget p0, v0, LX/BKm;->j:I

    invoke-virtual {p1, v1, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1773385
    const-string v1, "status_view_scroll_pos"

    iget p0, v0, LX/BKm;->l:I

    invoke-virtual {p1, v1, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1773386
    const-string v1, "rich_text_style"

    iget-object p0, v0, LX/BKm;->o:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-virtual {p1, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1773387
    :cond_0
    return-void
.end method

.method public final onStart()V
    .locals 13

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x550bf44c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1773388
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 1773389
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->d:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->J:J

    .line 1773390
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->o:LX/BLs;

    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ap:LX/BKL;

    .line 1773391
    new-instance v5, LX/BLr;

    .line 1773392
    new-instance v7, LX/BLt;

    invoke-static {v1}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-direct {v7, v6}, LX/BLt;-><init>(Ljava/lang/Boolean;)V

    .line 1773393
    move-object v6, v7

    .line 1773394
    check-cast v6, LX/BLt;

    invoke-static {v1}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v7

    check-cast v7, LX/0if;

    invoke-static {v1}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v8

    check-cast v8, Landroid/content/res/Resources;

    invoke-static {v1}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v9

    check-cast v9, Ljava/lang/Boolean;

    invoke-static {v1}, LX/31w;->b(LX/0QB;)LX/31w;

    move-result-object v10

    check-cast v10, LX/31w;

    move-object v11, p0

    move-object v12, v2

    invoke-direct/range {v5 .. v12}, LX/BLr;-><init>(LX/BLt;LX/0if;Landroid/content/res/Resources;Ljava/lang/Boolean;LX/31w;Lcom/facebook/platform/composer/composer/PlatformComposerFragment;LX/BKL;)V

    .line 1773395
    move-object v1, v5

    .line 1773396
    iput-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->T:LX/BLr;

    .line 1773397
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->w:LX/0if;

    sget-object v2, LX/0ig;->k:LX/0ih;

    const-string v3, "fragment_onstart"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1773398
    const/16 v1, 0x2b

    const v2, 0x51db8ef9

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
