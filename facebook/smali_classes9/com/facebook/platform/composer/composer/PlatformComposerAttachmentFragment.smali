.class public Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/BJR;

.field public b:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/view/View;

.field private e:Landroid/widget/FrameLayout;

.field private f:Landroid/widget/ImageView;

.field private g:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1771985
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1771986
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->c:LX/0am;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/BJR;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 1771987
    sget-object v0, LX/BJR;->a:LX/0Px;

    move-object v0, v0

    .line 1771988
    invoke-static {p1, v0}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1771989
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->d(Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;)V

    .line 1771990
    :goto_0
    return-void

    .line 1771991
    :cond_0
    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->f:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->g:Z

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1771992
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1771993
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->e:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1771994
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->e:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 1771995
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->e:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->e:Landroid/widget/FrameLayout;

    .line 1771996
    new-instance v3, Lcom/facebook/attachments/angora/AngoraAttachmentView;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/facebook/attachments/angora/AngoraAttachmentView;-><init>(Landroid/content/Context;)V

    .line 1771997
    iget-object v4, p2, LX/BJR;->e:Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentViewBinder;

    .line 1771998
    invoke-virtual {v3}, LX/35n;->a()V

    .line 1771999
    invoke-virtual {v3}, Lcom/facebook/attachments/angora/AngoraAttachmentView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f020a3d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/facebook/attachments/angora/AngoraAttachmentView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1772000
    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1772001
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 1772002
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    const-string v7, "\n"

    const-string v8, " "

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1772003
    new-instance v6, Landroid/text/style/StyleSpan;

    const/4 v7, 0x1

    invoke-direct {v6, v7}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/4 v7, 0x0

    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    const/16 v9, 0x11

    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1772004
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->y()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->y()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 1772005
    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_2

    .line 1772006
    const-string v6, "\n"

    invoke-virtual {v5, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1772007
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->y()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1772008
    :cond_3
    invoke-virtual {v3, v5}, LX/35n;->setTitle(Ljava/lang/CharSequence;)V

    .line 1772009
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 1772010
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/35n;->setContextText(Ljava/lang/CharSequence;)V

    .line 1772011
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v5

    if-eqz v5, :cond_5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLNode;->hn()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    if-eqz v5, :cond_5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLNode;->hn()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLNode;->hn()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-static {v5}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->isAbsolute()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1772012
    iget-object v5, v4, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentViewBinder;->b:LX/1Ad;

    sget-object v6, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentViewBinder;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v5, v6}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLNode;->hn()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    invoke-static {v6}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v5

    invoke-virtual {v5}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/35n;->setSideImageController(LX/1aZ;)V

    .line 1772013
    :cond_5
    invoke-static {p2}, LX/BJR;->g(LX/BJR;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1772014
    invoke-static {p2, v2}, LX/BJR;->a(LX/BJR;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 1772015
    invoke-static {p2, v4, v3, v2}, LX/BJR;->a(LX/BJR;Landroid/view/View;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 1772016
    :cond_6
    move-object v2, v3

    .line 1772017
    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 1772018
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 1772019
    if-eqz v0, :cond_8

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->bj()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->bj()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    .line 1772020
    iget-object v2, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->f:Landroid/widget/ImageView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f082455

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->bj()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v5, v1

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_7
    move v0, v1

    .line 1772021
    goto/16 :goto_1

    .line 1772022
    :cond_8
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->f:Landroid/widget/ImageView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f082456

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method private c()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1772023
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1772024
    iget-boolean v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->g:Z

    if-nez v0, :cond_0

    .line 1772025
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1772026
    :cond_0
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->e:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1772027
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->e:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 1772028
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->e:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->a:LX/BJR;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->e:Landroid/widget/FrameLayout;

    .line 1772029
    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f030fc1

    const/4 p0, 0x0

    invoke-virtual {v4, v5, v3, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 1772030
    invoke-static {v1}, LX/BJR;->g(LX/BJR;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1772031
    invoke-static {v1, v3}, LX/BJR;->a(LX/BJR;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 1772032
    invoke-static {v1, v5, v4, v3}, LX/BJR;->a(LX/BJR;Landroid/view/View;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 1772033
    :cond_1
    move-object v1, v4

    .line 1772034
    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 1772035
    return-void
.end method

.method public static d(Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 1772036
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1772037
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1772038
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->e:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1772039
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->e:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 1772040
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->c:LX/0am;

    .line 1772041
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->b:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 1772042
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1772043
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1772044
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v0

    check-cast v0, LX/1Ck;

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->b:LX/1Ck;

    .line 1772045
    return-void
.end method

.method public final b()V
    .locals 11

    .prologue
    .line 1772046
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->d:Landroid/view/View;

    if-nez v0, :cond_1

    .line 1772047
    :cond_0
    :goto_0
    return-void

    .line 1772048
    :cond_1
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->a:LX/BJR;

    if-nez v0, :cond_2

    .line 1772049
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->d(Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;)V

    goto :goto_0

    .line 1772050
    :cond_2
    const/4 v0, 0x1

    move v0, v0

    .line 1772051
    iput-boolean v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->g:Z

    .line 1772052
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->a:LX/BJR;

    .line 1772053
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, v1, LX/BJR;->g:LX/BJx;

    invoke-virtual {v3}, LX/BJx;->c()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, LX/BJR;->g:LX/BJx;

    invoke-virtual {v3}, LX/BJx;->d()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 1772054
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->a:LX/BJR;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1772055
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1772056
    :cond_3
    invoke-direct {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->c()V

    .line 1772057
    iget-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->b:LX/1Ck;

    const-string v2, "fetchAttachment"

    iget-object v3, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->a:LX/BJR;

    .line 1772058
    iget-object v5, v3, LX/BJR;->g:LX/BJx;

    invoke-virtual {v5}, LX/BJx;->b()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v5

    .line 1772059
    if-eqz v5, :cond_4

    iget-object v6, v5, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->platformSharePreview:Lcom/facebook/ipc/composer/intent/SharePreview;

    if-eqz v6, :cond_4

    iget-object v6, v5, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->platformSharePreview:Lcom/facebook/ipc/composer/intent/SharePreview;

    iget-boolean v6, v6, Lcom/facebook/ipc/composer/intent/SharePreview;->isOverride:Z

    if-nez v6, :cond_4

    .line 1772060
    iget-object v5, v5, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->platformSharePreview:Lcom/facebook/ipc/composer/intent/SharePreview;

    invoke-static {v3, v5}, LX/BJR;->a$redex0(LX/BJR;Lcom/facebook/ipc/composer/intent/SharePreview;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v5

    invoke-static {v5}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    .line 1772061
    :goto_1
    move-object v3, v5

    .line 1772062
    new-instance v4, LX/BJT;

    invoke-direct {v4, p0, v1}, LX/BJT;-><init>(Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto/16 :goto_0

    .line 1772063
    :cond_4
    iget-object v5, v3, LX/BJR;->g:LX/BJx;

    invoke-virtual {v5}, LX/BJx;->c()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v5

    iget-object v5, v5, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    if-eqz v5, :cond_5

    .line 1772064
    new-instance v5, LX/BON;

    invoke-direct {v5}, LX/BON;-><init>()V

    iget-object v6, v3, LX/BJR;->g:LX/BJx;

    invoke-virtual {v6}, LX/BJx;->c()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v6

    iget-object v6, v6, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLEntity;->e()Ljava/lang/String;

    move-result-object v6

    .line 1772065
    iput-object v6, v5, LX/BON;->a:Ljava/lang/String;

    .line 1772066
    move-object v5, v5

    .line 1772067
    iget-object v6, v3, LX/BJR;->g:LX/BJx;

    invoke-virtual {v6}, LX/BJx;->a()Ljava/lang/String;

    move-result-object v6

    .line 1772068
    iput-object v6, v5, LX/BON;->c:Ljava/lang/String;

    .line 1772069
    move-object v5, v5

    .line 1772070
    invoke-virtual {v5}, LX/BON;->a()Lcom/facebook/share/protocol/LinksPreviewParams;

    move-result-object v5

    .line 1772071
    :goto_2
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 1772072
    const-string v6, "linksPreviewParams"

    invoke-virtual {v7, v6, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1772073
    const-string v5, "overridden_viewer_context"

    iget-object v6, v3, LX/BJR;->c:LX/0SI;

    invoke-interface {v6}, LX/0SI;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v6

    invoke-virtual {v7, v5, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1772074
    iget-object v5, v3, LX/BJR;->b:LX/0aG;

    const-string v6, "csh_links_preview"

    sget-object v8, LX/1ME;->BY_EXCEPTION:LX/1ME;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v9

    const v10, -0x606668f5    # -6.50479E-20f

    invoke-static/range {v5 .. v10}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v5

    invoke-interface {v5}, LX/1MF;->start()LX/1ML;

    move-result-object v5

    .line 1772075
    new-instance v6, LX/BJP;

    invoke-direct {v6, v3}, LX/BJP;-><init>(LX/BJR;)V

    .line 1772076
    sget-object v7, LX/131;->INSTANCE:LX/131;

    move-object v7, v7

    .line 1772077
    invoke-static {v5, v6, v7}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    goto :goto_1

    .line 1772078
    :cond_5
    iget-object v5, v3, LX/BJR;->g:LX/BJx;

    invoke-virtual {v5}, LX/BJx;->c()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v5

    iget-object v5, v5, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 1772079
    new-instance v5, LX/BON;

    invoke-direct {v5}, LX/BON;-><init>()V

    iget-object v6, v3, LX/BJR;->g:LX/BJx;

    invoke-virtual {v6}, LX/BJx;->c()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v6

    iget-object v6, v6, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    .line 1772080
    iput-object v6, v5, LX/BON;->b:Ljava/lang/String;

    .line 1772081
    move-object v5, v5

    .line 1772082
    iget-object v6, v3, LX/BJR;->g:LX/BJx;

    invoke-virtual {v6}, LX/BJx;->a()Ljava/lang/String;

    move-result-object v6

    .line 1772083
    iput-object v6, v5, LX/BON;->c:Ljava/lang/String;

    .line 1772084
    move-object v5, v5

    .line 1772085
    invoke-virtual {v5}, LX/BON;->a()Lcom/facebook/share/protocol/LinksPreviewParams;

    move-result-object v5

    goto :goto_2

    .line 1772086
    :cond_6
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "Both the shareable and the link for share are null!"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, LX/0Vg;->a(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    goto/16 :goto_1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x5fc30ccc

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1772087
    const v0, 0x7f030fb8

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 1772088
    const v0, 0x7f0d25f4

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->d:Landroid/view/View;

    .line 1772089
    const v0, 0x7f0d25f5

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->e:Landroid/widget/FrameLayout;

    .line 1772090
    const v0, 0x7f0d25f6

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->f:Landroid/widget/ImageView;

    .line 1772091
    invoke-static {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->d(Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;)V

    .line 1772092
    invoke-virtual {p0}, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->b()V

    .line 1772093
    const/16 v0, 0x2b

    const v3, -0x415ac47e

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x30b72f2a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1772094
    iget-object v1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->b:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 1772095
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 1772096
    const/16 v1, 0x2b

    const v2, 0x59063f9d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
