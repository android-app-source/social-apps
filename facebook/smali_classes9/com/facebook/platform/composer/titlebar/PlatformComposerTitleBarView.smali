.class public Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field public a:Landroid/view/View$OnClickListener;

.field public b:Landroid/view/View$OnClickListener;

.field public c:Landroid/view/View$OnClickListener;

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1776689
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1776690
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1776687
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1776688
    return-void
.end method


# virtual methods
.method public final onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x1134a804

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1776671
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 1776672
    const v0, 0x7f0d0508

    invoke-virtual {p0, v0}, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1776673
    if-eqz v0, :cond_0

    .line 1776674
    new-instance v2, LX/BLx;

    invoke-direct {v2, p0}, LX/BLx;-><init>(Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1776675
    :cond_0
    const v0, 0x7f0d2626

    invoke-virtual {p0, v0}, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1776676
    if-eqz v0, :cond_1

    .line 1776677
    new-instance v2, LX/BLy;

    invoke-direct {v2, p0}, LX/BLy;-><init>(Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1776678
    :cond_1
    const v0, 0x7f0d262a

    invoke-virtual {p0, v0}, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->f:Landroid/widget/TextView;

    .line 1776679
    const v0, 0x7f0d1465

    invoke-virtual {p0, v0}, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->g:Landroid/widget/TextView;

    .line 1776680
    const v0, 0x7f0d262b

    invoke-virtual {p0, v0}, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->h:Landroid/widget/TextView;

    .line 1776681
    const v0, 0x7f0d175e

    invoke-virtual {p0, v0}, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->i:Landroid/widget/TextView;

    .line 1776682
    const v0, 0x7f0d2624

    invoke-virtual {p0, v0}, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->e:Landroid/view/View;

    .line 1776683
    const v0, 0x7f0d2625

    invoke-virtual {p0, v0}, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->d:Landroid/view/View;

    .line 1776684
    iget-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->d:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 1776685
    iget-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->d:Landroid/view/View;

    new-instance v2, LX/BLz;

    invoke-direct {v2, p0}, LX/BLz;-><init>(Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1776686
    :cond_2
    const/16 v0, 0x2d

    const v2, -0x14408f12

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setOnBackClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 1776669
    iput-object p1, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->a:Landroid/view/View$OnClickListener;

    .line 1776670
    return-void
.end method

.method public setOnTitleBarClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 1776667
    iput-object p1, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->b:Landroid/view/View$OnClickListener;

    .line 1776668
    return-void
.end method

.method public setOnToolbarButtonClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 1776665
    iput-object p1, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->c:Landroid/view/View$OnClickListener;

    .line 1776666
    return-void
.end method

.method public setPostButtonEnabled(Z)V
    .locals 1

    .prologue
    .line 1776662
    iget-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->d:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1776663
    iget-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->d:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 1776664
    :cond_0
    return-void
.end method

.method public setShowPostButton(Z)V
    .locals 2

    .prologue
    .line 1776646
    iget-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->e:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1776647
    iget-object v1, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->e:Landroid/view/View;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1776648
    :cond_0
    return-void

    .line 1776649
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setSubtitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1776659
    iget-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->i:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 1776660
    iget-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->i:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1776661
    :cond_0
    return-void
.end method

.method public setSubtitlePrefix(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1776656
    iget-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->h:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 1776657
    iget-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->h:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1776658
    :cond_0
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1776653
    iget-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->g:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 1776654
    iget-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1776655
    :cond_0
    return-void
.end method

.method public setTitlePrefix(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1776650
    iget-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->f:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 1776651
    iget-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1776652
    :cond_0
    return-void
.end method
