.class public Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarBackground;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field private final a:Landroid/graphics/Paint;

.field private final b:Landroid/graphics/Path;

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1776493
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1776494
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarBackground;->a:Landroid/graphics/Paint;

    .line 1776495
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarBackground;->b:Landroid/graphics/Path;

    .line 1776496
    invoke-direct {p0, p1}, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarBackground;->a(Landroid/content/Context;)V

    .line 1776497
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1776498
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1776499
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarBackground;->a:Landroid/graphics/Paint;

    .line 1776500
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarBackground;->b:Landroid/graphics/Path;

    .line 1776501
    invoke-direct {p0, p1}, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarBackground;->a(Landroid/content/Context;)V

    .line 1776502
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1776503
    iget-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarBackground;->a:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarBackground;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0746

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1776504
    const/high16 v0, 0x41200000    # 10.0f

    invoke-static {p1, v0}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarBackground;->c:I

    .line 1776505
    invoke-static {p0}, LX/0vv;->h(Landroid/view/View;)I

    move-result v0

    if-nez v0, :cond_0

    .line 1776506
    iget-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarBackground;->b:Landroid/graphics/Path;

    iget v1, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarBackground;->c:I

    int-to-float v1, v1

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1776507
    :cond_0
    iget-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarBackground;->b:Landroid/graphics/Path;

    iget v1, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarBackground;->c:I

    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarBackground;->c:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1776508
    iget-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarBackground;->b:Landroid/graphics/Path;

    iget v1, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarBackground;->c:I

    int-to-float v1, v1

    invoke-virtual {v0, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1776509
    iget-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarBackground;->b:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 1776510
    return-void
.end method


# virtual methods
.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 1776511
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1776512
    invoke-virtual {p0}, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarBackground;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarBackground;->c:I

    sub-int/2addr v0, v1

    .line 1776513
    invoke-virtual {p0}, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarBackground;->getHeight()I

    move-result v1

    iget v2, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarBackground;->c:I

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x2

    .line 1776514
    int-to-float v0, v0

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1776515
    iget-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarBackground;->b:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarBackground;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1776516
    return-void
.end method
