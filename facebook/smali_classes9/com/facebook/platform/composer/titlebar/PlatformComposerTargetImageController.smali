.class public Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public final a:LX/8Sa;

.field public final b:Lcom/facebook/common/callercontext/CallerContext;

.field public final c:Landroid/view/View;

.field public final d:Landroid/view/View;

.field public final e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final f:Lcom/facebook/fbui/glyph/GlyphView;

.field public final g:Lcom/facebook/fbui/glyph/GlyphView;


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/view/View;LX/0Or;LX/8Sa;)V
    .locals 3
    .param p1    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Landroid/view/View;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/8Sa;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1776392
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1776393
    iput-object p4, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;->a:LX/8Sa;

    .line 1776394
    iput-object p1, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;->d:Landroid/view/View;

    .line 1776395
    iput-object p2, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;->c:Landroid/view/View;

    .line 1776396
    iget-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;->d:Landroid/view/View;

    const v1, 0x7f0d261b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1776397
    iget-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;->c:Landroid/view/View;

    const v1, 0x7f0d2619

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;->f:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1776398
    iget-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;->c:Landroid/view/View;

    const v1, 0x7f0d261a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;->g:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1776399
    const-class v0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 1776400
    invoke-interface {p3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1776401
    iget-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;->d:Landroid/view/View;

    const v2, 0x7f0d2618

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1776402
    iget-object v2, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1776403
    iget-object v0, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;->c:Landroid/view/View;

    const v2, 0x7f0d2618

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1776404
    iget-object v2, p0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1776405
    return-void
.end method
