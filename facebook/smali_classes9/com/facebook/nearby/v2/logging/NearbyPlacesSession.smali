.class public Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/CQB;

.field public b:LX/CQC;

.field public c:Ljava/lang/String;

.field public d:Lcom/facebook/search/results/model/SearchResultsMutableContext;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1885461
    new-instance v0, LX/CQA;

    invoke-direct {v0}, LX/CQA;-><init>()V

    sput-object v0, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/CQB;LX/CQC;)V
    .locals 1

    .prologue
    .line 1885459
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;-><init>(LX/CQB;LX/CQC;Lcom/facebook/search/results/model/SearchResultsMutableContext;)V

    .line 1885460
    return-void
.end method

.method public constructor <init>(LX/CQB;LX/CQC;Lcom/facebook/search/results/model/SearchResultsMutableContext;)V
    .locals 1

    .prologue
    .line 1885462
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1885463
    iput-object p1, p0, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;->a:LX/CQB;

    .line 1885464
    iput-object p2, p0, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;->b:LX/CQC;

    .line 1885465
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;->c:Ljava/lang/String;

    .line 1885466
    iput-object p3, p0, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;->d:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 1885467
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1885454
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1885455
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/CQB;

    iput-object v0, p0, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;->a:LX/CQB;

    .line 1885456
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/CQC;

    iput-object v0, p0, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;->b:LX/CQC;

    .line 1885457
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;->c:Ljava/lang/String;

    .line 1885458
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1885453
    const/4 v0, 0x0

    return v0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1885451
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;->c:Ljava/lang/String;

    .line 1885452
    return-void
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1885447
    iget-object v0, p0, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;->a:LX/CQB;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1885448
    iget-object v0, p0, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;->b:LX/CQC;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1885449
    iget-object v0, p0, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1885450
    return-void
.end method
