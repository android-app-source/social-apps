.class public final Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x66c20030
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel$Serializer;
.end annotation


# instance fields
.field private e:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1886387
    const-class v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1886386
    const-class v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1886384
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1886385
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1886381
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1886382
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1886383
    return-void
.end method

.method public static a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;)Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;
    .locals 8

    .prologue
    .line 1886362
    if-nez p0, :cond_0

    .line 1886363
    const/4 p0, 0x0

    .line 1886364
    :goto_0
    return-object p0

    .line 1886365
    :cond_0
    instance-of v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;

    if-eqz v0, :cond_1

    .line 1886366
    check-cast p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;

    goto :goto_0

    .line 1886367
    :cond_1
    new-instance v0, LX/CQb;

    invoke-direct {v0}, LX/CQb;-><init>()V

    .line 1886368
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;->a()I

    move-result v1

    iput v1, v0, LX/CQb;->a:I

    .line 1886369
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 1886370
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1886371
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1886372
    iget v3, v0, LX/CQb;->a:I

    invoke-virtual {v2, v5, v3, v5}, LX/186;->a(III)V

    .line 1886373
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1886374
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1886375
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1886376
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1886377
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1886378
    new-instance v3, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;

    invoke-direct {v3, v2}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;-><init>(LX/15i;)V

    .line 1886379
    move-object p0, v3

    .line 1886380
    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1886388
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1886389
    iget v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1886357
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1886358
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1886359
    iget v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;->e:I

    invoke-virtual {p1, v1, v0, v1}, LX/186;->a(III)V

    .line 1886360
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1886361
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1886354
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1886355
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1886356
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1886351
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1886352
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;->e:I

    .line 1886353
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1886348
    new-instance v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;

    invoke-direct {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;-><init>()V

    .line 1886349
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1886350
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1886347
    const v0, 0x6429dcac

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1886346
    const v0, -0x17d7360

    return v0
.end method
