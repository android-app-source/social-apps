.class public final Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5e124754
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1889230
    const-class v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1889229
    const-class v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1889227
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1889228
    return-void
.end method

.method private j()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1889225
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel;->e:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel;->e:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    .line 1889226
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel;->e:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1889223
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel;->f:Ljava/lang/String;

    .line 1889224
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1889215
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1889216
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel;->j()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1889217
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1889218
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1889219
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1889220
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1889221
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1889222
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1889207
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1889208
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel;->j()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1889209
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel;->j()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    .line 1889210
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel;->j()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1889211
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel;

    .line 1889212
    iput-object v0, v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel;->e:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    .line 1889213
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1889214
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1889206
    new-instance v0, LX/CQs;

    invoke-direct {v0, p1}, LX/CQs;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1889205
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1889197
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1889198
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1889204
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1889201
    new-instance v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel;

    invoke-direct {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesOldFlowFriendsWhoVisitedFragmentModel;-><init>()V

    .line 1889202
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1889203
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1889200
    const v0, 0x7aa24b2f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1889199
    const v0, 0x25d6af

    return v0
.end method
