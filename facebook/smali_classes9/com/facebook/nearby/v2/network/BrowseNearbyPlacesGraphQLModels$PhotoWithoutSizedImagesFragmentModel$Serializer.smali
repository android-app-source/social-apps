.class public final Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1890150
    const-class v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel;

    new-instance v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1890151
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1890152
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel;LX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 1890153
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1890154
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v6, 0x0

    .line 1890155
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1890156
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1890157
    if-eqz v2, :cond_0

    .line 1890158
    const-string v3, "album"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1890159
    invoke-static {v1, v2, p1, p2}, LX/CRe;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1890160
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1890161
    if-eqz v2, :cond_1

    .line 1890162
    const-string v3, "can_viewer_add_tags"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1890163
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1890164
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 1890165
    cmp-long v4, v2, v6

    if-eqz v4, :cond_2

    .line 1890166
    const-string v4, "created_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1890167
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 1890168
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1890169
    if-eqz v2, :cond_3

    .line 1890170
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1890171
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1890172
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1890173
    if-eqz v2, :cond_4

    .line 1890174
    const-string v3, "image"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1890175
    invoke-static {v1, v2, p1}, LX/CR5;->a(LX/15i;ILX/0nX;)V

    .line 1890176
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1890177
    if-eqz v2, :cond_5

    .line 1890178
    const-string v3, "is_disturbing"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1890179
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1890180
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1890181
    if-eqz v2, :cond_6

    .line 1890182
    const-string v3, "message"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1890183
    invoke-static {v1, v2, p1, p2}, LX/8ZT;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1890184
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 1890185
    cmp-long v4, v2, v6

    if-eqz v4, :cond_7

    .line 1890186
    const-string v4, "modified_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1890187
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 1890188
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1890189
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1890190
    check-cast p1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$Serializer;->a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
