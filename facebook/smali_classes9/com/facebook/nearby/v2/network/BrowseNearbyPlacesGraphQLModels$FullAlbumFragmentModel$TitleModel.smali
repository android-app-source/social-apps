.class public final Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x50a312db
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1885708
    const-class v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1885707
    const-class v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1885705
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1885706
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1885666
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1885667
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1885668
    return-void
.end method

.method public static a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel;)Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel;
    .locals 8

    .prologue
    .line 1885685
    if-nez p0, :cond_0

    .line 1885686
    const/4 p0, 0x0

    .line 1885687
    :goto_0
    return-object p0

    .line 1885688
    :cond_0
    instance-of v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel;

    if-eqz v0, :cond_1

    .line 1885689
    check-cast p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel;

    goto :goto_0

    .line 1885690
    :cond_1
    new-instance v0, LX/CQR;

    invoke-direct {v0}, LX/CQR;-><init>()V

    .line 1885691
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/CQR;->a:Ljava/lang/String;

    .line 1885692
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1885693
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1885694
    iget-object v3, v0, LX/CQR;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1885695
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1885696
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 1885697
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1885698
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1885699
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1885700
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1885701
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1885702
    new-instance v3, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel;

    invoke-direct {v3, v2}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel;-><init>(LX/15i;)V

    .line 1885703
    move-object p0, v3

    .line 1885704
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1885679
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1885680
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1885681
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1885682
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1885683
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1885684
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1885676
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1885677
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1885678
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1885674
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel;->e:Ljava/lang/String;

    .line 1885675
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1885671
    new-instance v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel;

    invoke-direct {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel;-><init>()V

    .line 1885672
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1885673
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1885670
    const v0, -0x3348d284

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1885669
    const v0, -0x726d476c

    return v0
.end method
