.class public final Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/1k1;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1f8820a1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel$Serializer;
.end annotation


# instance fields
.field private e:D

.field private f:D

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1886043
    const-class v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1886042
    const-class v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1886040
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1886041
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1886037
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1886038
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1886039
    return-void
.end method

.method public static a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;)Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;
    .locals 4

    .prologue
    .line 1886027
    if-nez p0, :cond_0

    .line 1886028
    const/4 p0, 0x0

    .line 1886029
    :goto_0
    return-object p0

    .line 1886030
    :cond_0
    instance-of v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    if-eqz v0, :cond_1

    .line 1886031
    check-cast p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    goto :goto_0

    .line 1886032
    :cond_1
    new-instance v0, LX/CQV;

    invoke-direct {v0}, LX/CQV;-><init>()V

    .line 1886033
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;->a()D

    move-result-wide v2

    iput-wide v2, v0, LX/CQV;->a:D

    .line 1886034
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;->b()D

    move-result-wide v2

    iput-wide v2, v0, LX/CQV;->b:D

    .line 1886035
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/CQV;->c:Ljava/lang/String;

    .line 1886036
    invoke-virtual {v0}, LX/CQV;->a()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a()D
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1886025
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1886026
    iget-wide v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;->e:D

    return-wide v0
.end method

.method public final a(LX/186;)I
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    .line 1886044
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1886045
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1886046
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1886047
    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;->e:D

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1886048
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;->f:D

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1886049
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1886050
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1886051
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1886022
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1886023
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1886024
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1886018
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1886019
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;->e:D

    .line 1886020
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;->f:D

    .line 1886021
    return-void
.end method

.method public final b()D
    .locals 2

    .prologue
    .line 1886016
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1886017
    iget-wide v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;->f:D

    return-wide v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1886013
    new-instance v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    invoke-direct {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;-><init>()V

    .line 1886014
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1886015
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1886011
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;->g:Ljava/lang/String;

    .line 1886012
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1886009
    const v0, -0x3084486e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1886010
    const v0, 0x752a03d5

    return v0
.end method
