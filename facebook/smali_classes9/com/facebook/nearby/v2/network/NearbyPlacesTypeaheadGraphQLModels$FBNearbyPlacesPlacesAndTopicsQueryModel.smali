.class public final Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x67abb40c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel$TypeaheadPlacesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1893624
    const-class v0, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1893627
    const-class v0, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1893625
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1893626
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1893616
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1893617
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/util/List;)I

    move-result v0

    .line 1893618
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel;->j()Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel$TypeaheadPlacesModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1893619
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1893620
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1893621
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1893622
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1893623
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1893628
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel;->e:Ljava/util/List;

    .line 1893629
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1893608
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1893609
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel;->j()Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel$TypeaheadPlacesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1893610
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel;->j()Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel$TypeaheadPlacesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel$TypeaheadPlacesModel;

    .line 1893611
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel;->j()Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel$TypeaheadPlacesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1893612
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel;

    .line 1893613
    iput-object v0, v1, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel;->f:Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel$TypeaheadPlacesModel;

    .line 1893614
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1893615
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1893605
    new-instance v0, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel;

    invoke-direct {v0}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel;-><init>()V

    .line 1893606
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1893607
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1893601
    const v0, 0x6ad9bb1

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1893604
    const v0, 0x66d593e1

    return v0
.end method

.method public final j()Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel$TypeaheadPlacesModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTypeaheadPlaces"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1893602
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel;->f:Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel$TypeaheadPlacesModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel$TypeaheadPlacesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel$TypeaheadPlacesModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel;->f:Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel$TypeaheadPlacesModel;

    .line 1893603
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel;->f:Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel$TypeaheadPlacesModel;

    return-object v0
.end method
