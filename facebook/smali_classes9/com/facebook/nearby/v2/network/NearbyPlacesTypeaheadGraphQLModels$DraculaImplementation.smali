.class public final Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1893360
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1893361
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1893362
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1893363
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 1893364
    if-nez p1, :cond_0

    .line 1893365
    :goto_0
    return v1

    .line 1893366
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1893367
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1893368
    :sswitch_0
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1893369
    invoke-virtual {p0, p1, v8, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 1893370
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    move-object v0, p3

    .line 1893371
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    move-object v0, p3

    move v1, v8

    move-wide v2, v6

    .line 1893372
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1893373
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 1893374
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1893375
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1893376
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1893377
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1893378
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    .line 1893379
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1893380
    invoke-virtual {p3, v8, v2}, LX/186;->b(II)V

    .line 1893381
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 1893382
    :sswitch_2
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1893383
    invoke-virtual {p0, p1, v8, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 1893384
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    move-object v0, p3

    .line 1893385
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    move-object v0, p3

    move v1, v8

    move-wide v2, v6

    .line 1893386
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1893387
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x75690538 -> :sswitch_1
        -0x69af1fa3 -> :sswitch_0
        -0x61a0a157 -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1893392
    new-instance v0, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1893388
    sparse-switch p0, :sswitch_data_0

    .line 1893389
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1893390
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x75690538 -> :sswitch_0
        -0x69af1fa3 -> :sswitch_0
        -0x61a0a157 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1893391
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1893357
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$DraculaImplementation;->b(I)V

    .line 1893358
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1893352
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1893353
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1893354
    :cond_0
    iput-object p1, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1893355
    iput p2, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$DraculaImplementation;->b:I

    .line 1893356
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1893359
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1893351
    new-instance v0, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1893348
    iget v0, p0, LX/1vt;->c:I

    .line 1893349
    move v0, v0

    .line 1893350
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1893345
    iget v0, p0, LX/1vt;->c:I

    .line 1893346
    move v0, v0

    .line 1893347
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1893342
    iget v0, p0, LX/1vt;->b:I

    .line 1893343
    move v0, v0

    .line 1893344
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1893339
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1893340
    move-object v0, v0

    .line 1893341
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1893330
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1893331
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1893332
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1893333
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1893334
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1893335
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1893336
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1893337
    invoke-static {v3, v9, v2}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1893338
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1893327
    iget v0, p0, LX/1vt;->c:I

    .line 1893328
    move v0, v0

    .line 1893329
    return v0
.end method
