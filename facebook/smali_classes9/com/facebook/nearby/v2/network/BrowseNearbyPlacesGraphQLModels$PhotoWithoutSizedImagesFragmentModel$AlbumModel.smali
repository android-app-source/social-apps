.class public final Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$AlbumModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;
.implements LX/CQF;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7846852d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$AlbumModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$AlbumModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1890128
    const-class v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$AlbumModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1890080
    const-class v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$AlbumModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1890126
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1890127
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1890123
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1890124
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1890125
    return-void
.end method

.method public static a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$AlbumModel;)Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$AlbumModel;
    .locals 9

    .prologue
    .line 1890100
    if-nez p0, :cond_0

    .line 1890101
    const/4 p0, 0x0

    .line 1890102
    :goto_0
    return-object p0

    .line 1890103
    :cond_0
    instance-of v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$AlbumModel;

    if-eqz v0, :cond_1

    .line 1890104
    check-cast p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$AlbumModel;

    goto :goto_0

    .line 1890105
    :cond_1
    new-instance v0, LX/CR3;

    invoke-direct {v0}, LX/CR3;-><init>()V

    .line 1890106
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$AlbumModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/CR3;->a:Ljava/lang/String;

    .line 1890107
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$AlbumModel;->c()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel;->a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel;)Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel;

    move-result-object v1

    iput-object v1, v0, LX/CR3;->b:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel;

    .line 1890108
    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 1890109
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1890110
    iget-object v3, v0, LX/CR3;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1890111
    iget-object v5, v0, LX/CR3;->b:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel;

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1890112
    const/4 v7, 0x2

    invoke-virtual {v2, v7}, LX/186;->c(I)V

    .line 1890113
    invoke-virtual {v2, v8, v3}, LX/186;->b(II)V

    .line 1890114
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1890115
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1890116
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1890117
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1890118
    invoke-virtual {v3, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1890119
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1890120
    new-instance v3, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$AlbumModel;

    invoke-direct {v3, v2}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$AlbumModel;-><init>(LX/15i;)V

    .line 1890121
    move-object p0, v3

    .line 1890122
    goto :goto_0
.end method

.method private j()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1890098
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$AlbumModel;->f:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$AlbumModel;->f:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel;

    .line 1890099
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$AlbumModel;->f:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1890090
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1890091
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$AlbumModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1890092
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$AlbumModel;->j()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1890093
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1890094
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1890095
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1890096
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1890097
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1890129
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1890130
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$AlbumModel;->j()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1890131
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$AlbumModel;->j()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel;

    .line 1890132
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$AlbumModel;->j()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1890133
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$AlbumModel;

    .line 1890134
    iput-object v0, v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$AlbumModel;->f:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel;

    .line 1890135
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1890136
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1890089
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$AlbumModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1890086
    new-instance v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$AlbumModel;

    invoke-direct {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$AlbumModel;-><init>()V

    .line 1890087
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1890088
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1890084
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$AlbumModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$AlbumModel;->e:Ljava/lang/String;

    .line 1890085
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$AlbumModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1890083
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PhotoWithoutSizedImagesFragmentModel$AlbumModel;->j()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$TitleModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1890082
    const v0, -0x31e077e7    # -6.6912416E8f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1890081
    const v0, 0x3c68e4f

    return v0
.end method
