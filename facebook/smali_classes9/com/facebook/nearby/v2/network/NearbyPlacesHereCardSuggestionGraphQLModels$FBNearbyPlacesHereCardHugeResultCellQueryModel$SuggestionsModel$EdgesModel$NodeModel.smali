.class public final Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/CQM;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x24a0a12b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private A:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private B:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private C:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private D:Z

.field private E:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private F:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private G:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PageTWEFragmentModel$RedirectionInfoModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private H:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$Photo320FragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private I:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private J:Z

.field private K:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private L:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private M:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private N:Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Z

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Z

.field private l:Z

.field private m:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$HoursModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Z

.field private q:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private x:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:Lcom/facebook/graphql/enums/GraphQLPlaceType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private z:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1892686
    const-class v0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1892687
    const-class v0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1892688
    const/16 v0, 0x24

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1892689
    return-void
.end method

.method private M()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1892690
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1892691
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1892692
    :cond_0
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private N()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1892693
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->f:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->f:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;

    .line 1892694
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->f:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;

    return-object v0
.end method

.method private O()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1892695
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->m:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->m:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    .line 1892696
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->m:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    return-object v0
.end method

.method private P()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1892697
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->q:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->q:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    .line 1892698
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->q:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    return-object v0
.end method

.method private Q()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1892699
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->s:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->s:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;

    .line 1892700
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->s:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;

    return-object v0
.end method

.method private R()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1892701
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->t:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->t:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;

    .line 1892702
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->t:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;

    return-object v0
.end method

.method private S()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1892703
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->u:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->u:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;

    .line 1892704
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->u:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;

    return-object v0
.end method

.method private T()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1892705
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->w:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->w:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;

    .line 1892706
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->w:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;

    return-object v0
.end method

.method private U()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1892707
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->A:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->A:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    .line 1892708
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->A:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    return-object v0
.end method

.method private V()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1892709
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->B:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->B:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    .line 1892710
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->B:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    return-object v0
.end method

.method private W()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1892711
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->C:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->C:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;

    .line 1892712
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->C:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;

    return-object v0
.end method

.method private X()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1892713
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->E:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->E:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;

    .line 1892714
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->E:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;

    return-object v0
.end method

.method private Y()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1892825
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->F:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->F:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;

    .line 1892826
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->F:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;

    return-object v0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSavedState;)V
    .locals 4

    .prologue
    .line 1892715
    iput-object p1, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->N:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1892716
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1892717
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1892718
    if-eqz v0, :cond_0

    .line 1892719
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x23

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLSavedState;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1892720
    :cond_0
    return-void

    .line 1892721
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1892819
    iput-object p1, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->r:Ljava/lang/String;

    .line 1892820
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1892821
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1892822
    if-eqz v0, :cond_0

    .line 1892823
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1892824
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic A()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1892818
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->W()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;

    move-result-object v0

    return-object v0
.end method

.method public final B()Z
    .locals 2

    .prologue
    .line 1892816
    const/4 v0, 0x3

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1892817
    iget-boolean v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->D:Z

    return v0
.end method

.method public final synthetic C()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1892815
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->X()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic D()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1892814
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->Y()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final E()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PageTWEFragmentModel$RedirectionInfoModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1892812
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->G:Ljava/util/List;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PageTWEFragmentModel$RedirectionInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->G:Ljava/util/List;

    .line 1892813
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->G:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final F()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$Photo320FragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1892590
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->H:Ljava/util/List;

    const/16 v1, 0x1d

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$Photo320FragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->H:Ljava/util/List;

    .line 1892591
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->H:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final G()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1892810
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->I:Ljava/util/List;

    const/16 v1, 0x1e

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->I:Ljava/util/List;

    .line 1892811
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->I:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final H()Z
    .locals 2

    .prologue
    .line 1892808
    const/4 v0, 0x3

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1892809
    iget-boolean v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->J:Z

    return v0
.end method

.method public final I()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1892806
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->K:Ljava/util/List;

    const/16 v1, 0x20

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->K:Ljava/util/List;

    .line 1892807
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->K:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final J()Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1892804
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->L:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const/16 v1, 0x21

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->L:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 1892805
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->L:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    return-object v0
.end method

.method public final K()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1892802
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->M:Ljava/util/List;

    const/16 v1, 0x22

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->M:Ljava/util/List;

    .line 1892803
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->M:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final L()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1892800
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->N:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/16 v1, 0x23

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->N:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1892801
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->N:Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 32

    .prologue
    .line 1892617
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1892618
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->M()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1892619
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->N()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1892620
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->e()LX/0Px;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/util/List;)I

    move-result v4

    .line 1892621
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->jH_()Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 1892622
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->O()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1892623
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->l()LX/0Px;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v7

    .line 1892624
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1892625
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->P()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1892626
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->p()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1892627
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->Q()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1892628
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->R()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1892629
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->S()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 1892630
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->t()Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v14

    .line 1892631
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->T()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 1892632
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->v()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v16

    .line 1892633
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->w()Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v17

    .line 1892634
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->x()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    .line 1892635
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->U()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 1892636
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->V()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 1892637
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->W()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 1892638
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->X()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 1892639
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->Y()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 1892640
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->E()LX/0Px;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v24

    .line 1892641
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->F()LX/0Px;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v25

    .line 1892642
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->G()LX/0Px;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v26

    .line 1892643
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->I()LX/0Px;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v27

    .line 1892644
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->J()Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v28

    .line 1892645
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->K()LX/0Px;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v29

    .line 1892646
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->L()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v30

    .line 1892647
    const/16 v31, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1892648
    const/16 v31, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1892649
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1892650
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->g:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1892651
    const/4 v2, 0x3

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->h:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1892652
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1892653
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1892654
    const/4 v2, 0x6

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->k:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1892655
    const/4 v2, 0x7

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->l:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1892656
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1892657
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1892658
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1892659
    const/16 v2, 0xb

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->p:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1892660
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1892661
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1892662
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1892663
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1892664
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1892665
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1892666
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1892667
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1892668
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1892669
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1892670
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1892671
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1892672
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1892673
    const/16 v2, 0x19

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->D:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1892674
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1892675
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1892676
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1892677
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1892678
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1892679
    const/16 v2, 0x1f

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->J:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1892680
    const/16 v2, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1892681
    const/16 v2, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1892682
    const/16 v2, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1892683
    const/16 v2, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1892684
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1892685
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1892722
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1892723
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->N()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1892724
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->N()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;

    .line 1892725
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->N()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1892726
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;

    .line 1892727
    iput-object v0, v1, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->f:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;

    .line 1892728
    :cond_0
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->O()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1892729
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->O()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    .line 1892730
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->O()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1892731
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;

    .line 1892732
    iput-object v0, v1, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->m:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    .line 1892733
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->l()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1892734
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->l()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1892735
    if-eqz v2, :cond_2

    .line 1892736
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;

    .line 1892737
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->n:Ljava/util/List;

    move-object v1, v0

    .line 1892738
    :cond_2
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->P()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1892739
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->P()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    .line 1892740
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->P()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1892741
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;

    .line 1892742
    iput-object v0, v1, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->q:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    .line 1892743
    :cond_3
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->Q()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1892744
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->Q()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;

    .line 1892745
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->Q()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1892746
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;

    .line 1892747
    iput-object v0, v1, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->s:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;

    .line 1892748
    :cond_4
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->R()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1892749
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->R()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;

    .line 1892750
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->R()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 1892751
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;

    .line 1892752
    iput-object v0, v1, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->t:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;

    .line 1892753
    :cond_5
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->S()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1892754
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->S()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;

    .line 1892755
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->S()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 1892756
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;

    .line 1892757
    iput-object v0, v1, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->u:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;

    .line 1892758
    :cond_6
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->T()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 1892759
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->T()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;

    .line 1892760
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->T()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 1892761
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;

    .line 1892762
    iput-object v0, v1, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->w:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;

    .line 1892763
    :cond_7
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->U()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 1892764
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->U()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    .line 1892765
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->U()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 1892766
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;

    .line 1892767
    iput-object v0, v1, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->A:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    .line 1892768
    :cond_8
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->V()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 1892769
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->V()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    .line 1892770
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->V()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 1892771
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;

    .line 1892772
    iput-object v0, v1, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->B:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    .line 1892773
    :cond_9
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->W()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 1892774
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->W()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;

    .line 1892775
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->W()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 1892776
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;

    .line 1892777
    iput-object v0, v1, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->C:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;

    .line 1892778
    :cond_a
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->X()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 1892779
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->X()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;

    .line 1892780
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->X()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 1892781
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;

    .line 1892782
    iput-object v0, v1, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->E:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;

    .line 1892783
    :cond_b
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->Y()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 1892784
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->Y()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;

    .line 1892785
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->Y()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 1892786
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;

    .line 1892787
    iput-object v0, v1, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->F:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;

    .line 1892788
    :cond_c
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->E()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 1892789
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->E()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1892790
    if-eqz v2, :cond_d

    .line 1892791
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;

    .line 1892792
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->G:Ljava/util/List;

    move-object v1, v0

    .line 1892793
    :cond_d
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->F()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 1892794
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->F()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1892795
    if-eqz v2, :cond_e

    .line 1892796
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;

    .line 1892797
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->H:Ljava/util/List;

    move-object v1, v0

    .line 1892798
    :cond_e
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1892799
    if-nez v1, :cond_f

    :goto_0
    return-object p0

    :cond_f
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1892592
    new-instance v0, LX/CRl;

    invoke-direct {v0, p1}, LX/CRl;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1892589
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1892580
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1892581
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->g:Z

    .line 1892582
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->h:Z

    .line 1892583
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->k:Z

    .line 1892584
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->l:Z

    .line 1892585
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->p:Z

    .line 1892586
    const/16 v0, 0x19

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->D:Z

    .line 1892587
    const/16 v0, 0x1f

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->J:Z

    .line 1892588
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1892570
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1892571
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->p()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1892572
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1892573
    const/16 v0, 0xd

    iput v0, p2, LX/18L;->c:I

    .line 1892574
    :goto_0
    return-void

    .line 1892575
    :cond_0
    const-string v0, "viewer_saved_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1892576
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->L()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1892577
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1892578
    const/16 v0, 0x23

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1892579
    :cond_1
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1892565
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1892566
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->a(Ljava/lang/String;)V

    .line 1892567
    :cond_0
    :goto_0
    return-void

    .line 1892568
    :cond_1
    const-string v0, "viewer_saved_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1892569
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-direct {p0, p2}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->a(Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1892562
    new-instance v0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;-><init>()V

    .line 1892563
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1892564
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1892561
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->N()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;

    move-result-object v0

    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1892559
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1892560
    iget-boolean v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->g:Z

    return v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 1892557
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1892558
    iget-boolean v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->h:Z

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1892556
    const v0, 0x5f59c192

    return v0
.end method

.method public final e()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1892554
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->i:Ljava/util/List;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->i:Ljava/util/List;

    .line 1892555
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->i:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1892553
    const v0, 0x499e8e7

    return v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 1892551
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1892552
    iget-boolean v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->l:Z

    return v0
.end method

.method public final jH_()Lcom/facebook/graphql/enums/GraphQLPageCategoryType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1892549
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->j:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->j:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    .line 1892550
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->j:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    return-object v0
.end method

.method public final jI_()Z
    .locals 2

    .prologue
    .line 1892547
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1892548
    iget-boolean v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->k:Z

    return v0
.end method

.method public final synthetic k()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1892593
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->O()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$HoursModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1892594
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->n:Ljava/util/List;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$HoursModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->n:Ljava/util/List;

    .line 1892595
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->n:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1892596
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->o:Ljava/lang/String;

    .line 1892597
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Z
    .locals 2

    .prologue
    .line 1892598
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1892599
    iget-boolean v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->p:Z

    return v0
.end method

.method public final synthetic o()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1892600
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->P()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    move-result-object v0

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1892601
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->r:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->r:Ljava/lang/String;

    .line 1892602
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic q()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1892603
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->Q()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic r()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1892604
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->R()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic s()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1892605
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->S()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;

    move-result-object v0

    return-object v0
.end method

.method public final t()Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1892606
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->v:Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->v:Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    .line 1892607
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->v:Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    return-object v0
.end method

.method public final synthetic u()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1892608
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->T()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;

    move-result-object v0

    return-object v0
.end method

.method public final v()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1892609
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->x:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->x:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    .line 1892610
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->x:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    return-object v0
.end method

.method public final w()Lcom/facebook/graphql/enums/GraphQLPlaceType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1892611
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->y:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPlaceType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPlaceType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->y:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 1892612
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->y:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    return-object v0
.end method

.method public final x()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1892613
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->z:Ljava/lang/String;

    const/16 v1, 0x15

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->z:Ljava/lang/String;

    .line 1892614
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->z:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic y()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1892615
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->U()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic z()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1892616
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;->V()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    move-result-object v0

    return-object v0
.end method
