.class public final Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;
.implements LX/5ti;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x177de61c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:J

.field private f:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I

.field private i:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1889468
    const-class v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1889469
    const-class v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1889470
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1889471
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1889472
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1889473
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1889474
    return-void
.end method

.method public static a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;)Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;
    .locals 4

    .prologue
    .line 1889475
    if-nez p0, :cond_0

    .line 1889476
    const/4 p0, 0x0

    .line 1889477
    :goto_0
    return-object p0

    .line 1889478
    :cond_0
    instance-of v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;

    if-eqz v0, :cond_1

    .line 1889479
    check-cast p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;

    goto :goto_0

    .line 1889480
    :cond_1
    new-instance v0, LX/CQx;

    invoke-direct {v0}, LX/CQx;-><init>()V

    .line 1889481
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->jJ_()J

    move-result-wide v2

    iput-wide v2, v0, LX/CQx;->a:J

    .line 1889482
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->e()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;->a(Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;)Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    move-result-object v1

    iput-object v1, v0, LX/CQx;->b:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    .line 1889483
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->jK_()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/CQx;->c:Ljava/lang/String;

    .line 1889484
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->b()I

    move-result v1

    iput v1, v0, LX/CQx;->d:I

    .line 1889485
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->c()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;->a(Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;)Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/CQx;->e:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    .line 1889486
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->j()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;->a(Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;)Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    move-result-object v1

    iput-object v1, v0, LX/CQx;->f:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    .line 1889487
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->d()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;->a(Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;)Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v1

    iput-object v1, v0, LX/CQx;->g:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    .line 1889488
    invoke-virtual {v0}, LX/CQx;->a()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;

    move-result-object p0

    goto :goto_0
.end method

.method private k()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1889489
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->f:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->f:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    .line 1889490
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->f:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    return-object v0
.end method

.method private l()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1889491
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->i:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->i:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    .line 1889492
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->i:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    return-object v0
.end method

.method private m()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1889493
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->j:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->j:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    .line 1889494
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->j:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    return-object v0
.end method

.method private n()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1889495
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->k:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->k:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    .line 1889496
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->k:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 1889497
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1889498
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->k()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1889499
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->jK_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1889500
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->l()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1889501
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->m()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1889502
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->n()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1889503
    const/4 v0, 0x7

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1889504
    iget-wide v2, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->e:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1889505
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1889506
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1889507
    const/4 v0, 0x3

    iget v2, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->h:I

    invoke-virtual {p1, v0, v2, v1}, LX/186;->a(III)V

    .line 1889508
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1889509
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1889510
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 1889511
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1889512
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1889513
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1889514
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->k()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1889515
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->k()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    .line 1889516
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->k()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1889517
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;

    .line 1889518
    iput-object v0, v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->f:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    .line 1889519
    :cond_0
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->l()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1889520
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->l()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    .line 1889521
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->l()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1889522
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;

    .line 1889523
    iput-object v0, v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->i:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    .line 1889524
    :cond_1
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->m()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1889525
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->m()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    .line 1889526
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->m()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1889527
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;

    .line 1889528
    iput-object v0, v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->j:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    .line 1889529
    :cond_2
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->n()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1889530
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->n()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    .line 1889531
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->n()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1889532
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;

    .line 1889533
    iput-object v0, v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->k:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    .line 1889534
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1889535
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    :cond_4
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1889467
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->jK_()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1889463
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1889464
    const-wide/16 v0, 0x0

    invoke-virtual {p1, p2, v2, v0, v1}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->e:J

    .line 1889465
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->h:I

    .line 1889466
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 1889454
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1889455
    iget v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->h:I

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1889451
    new-instance v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;-><init>()V

    .line 1889452
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1889453
    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1889450
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->l()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1889449
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->n()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1889448
    const v0, 0x587c69f4

    return v0
.end method

.method public final synthetic e()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1889456
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->k()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1889457
    const v0, -0x7d2175f

    return v0
.end method

.method public final synthetic j()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1889458
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->m()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    move-result-object v0

    return-object v0
.end method

.method public final jJ_()J
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1889459
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1889460
    iget-wide v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->e:J

    return-wide v0
.end method

.method public final jK_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1889461
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    .line 1889462
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    return-object v0
.end method
