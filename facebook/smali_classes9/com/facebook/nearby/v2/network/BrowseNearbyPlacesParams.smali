.class public Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final b:F

.field private final c:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

.field private final d:F

.field private final e:Landroid/location/Location;

.field private final f:F

.field private final g:I

.field private final h:F

.field private final i:F

.field private final j:F

.field private final k:F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1892431
    new-instance v0, LX/CRh;

    invoke-direct {v0}, LX/CRh;-><init>()V

    sput-object v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1892414
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1892415
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesParams;->c:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 1892416
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesParams;->d:F

    .line 1892417
    const-class v0, Landroid/location/Location;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesParams;->e:Landroid/location/Location;

    .line 1892418
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesParams;->f:F

    .line 1892419
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesParams;->g:I

    .line 1892420
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesParams;->h:F

    .line 1892421
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesParams;->i:F

    .line 1892422
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesParams;->j:F

    .line 1892423
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesParams;->k:F

    .line 1892424
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-nez v0, :cond_0

    .line 1892425
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesParams;->a:LX/0Px;

    .line 1892426
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesParams;->b:F

    .line 1892427
    return-void

    .line 1892428
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1892429
    const-class v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 1892430
    invoke-static {v0}, LX/17G;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesParams;->a:LX/0Px;

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1892432
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1892399
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesParams;->c:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1892400
    iget v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesParams;->d:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1892401
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesParams;->e:Landroid/location/Location;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1892402
    iget v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesParams;->f:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1892403
    iget v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesParams;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1892404
    iget v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesParams;->h:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1892405
    iget v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesParams;->i:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1892406
    iget v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesParams;->j:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1892407
    iget v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesParams;->k:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1892408
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesParams;->a:LX/0Px;

    if-nez v0, :cond_0

    .line 1892409
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1892410
    :goto_0
    iget v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesParams;->b:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1892411
    return-void

    .line 1892412
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1892413
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesParams;->a:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    goto :goto_0
.end method
