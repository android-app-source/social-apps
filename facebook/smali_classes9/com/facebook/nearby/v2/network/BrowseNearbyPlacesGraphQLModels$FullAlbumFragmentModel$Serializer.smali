.class public final Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1885629
    const-class v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel;

    new-instance v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1885630
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1885631
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1885632
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1885633
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1885634
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1885635
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1885636
    if-eqz v2, :cond_0

    .line 1885637
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1885638
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1885639
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1885640
    if-eqz v2, :cond_1

    .line 1885641
    const-string p0, "title"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1885642
    invoke-static {v1, v2, p1}, LX/CR6;->a(LX/15i;ILX/0nX;)V

    .line 1885643
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1885644
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1885628
    check-cast p1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel$Serializer;->a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$FullAlbumFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
