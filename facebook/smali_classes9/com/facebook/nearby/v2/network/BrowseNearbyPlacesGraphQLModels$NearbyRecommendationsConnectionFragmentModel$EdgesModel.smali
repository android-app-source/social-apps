.class public final Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6393e786
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1889591
    const-class v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1889590
    const-class v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1889588
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1889589
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1889543
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1889544
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1889545
    return-void
.end method

.method public static a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel;)Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel;
    .locals 8

    .prologue
    .line 1889568
    if-nez p0, :cond_0

    .line 1889569
    const/4 p0, 0x0

    .line 1889570
    :goto_0
    return-object p0

    .line 1889571
    :cond_0
    instance-of v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel;

    if-eqz v0, :cond_1

    .line 1889572
    check-cast p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel;

    goto :goto_0

    .line 1889573
    :cond_1
    new-instance v0, LX/CQw;

    invoke-direct {v0}, LX/CQw;-><init>()V

    .line 1889574
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel;->a()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;->a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;)Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;

    move-result-object v1

    iput-object v1, v0, LX/CQw;->a:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;

    .line 1889575
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1889576
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1889577
    iget-object v3, v0, LX/CQw;->a:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1889578
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1889579
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 1889580
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1889581
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1889582
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1889583
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1889584
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1889585
    new-instance v3, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel;

    invoke-direct {v3, v2}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel;-><init>(LX/15i;)V

    .line 1889586
    move-object p0, v3

    .line 1889587
    goto :goto_0
.end method

.method private j()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1889566
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel;->e:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel;->e:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;

    .line 1889567
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel;->e:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1889560
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1889561
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel;->j()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1889562
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1889563
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1889564
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1889565
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1889552
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1889553
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel;->j()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1889554
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel;->j()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;

    .line 1889555
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel;->j()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1889556
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel;

    .line 1889557
    iput-object v0, v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel;->e:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;

    .line 1889558
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1889559
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1889551
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel;->j()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel$NodeModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1889548
    new-instance v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel$EdgesModel;-><init>()V

    .line 1889549
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1889550
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1889547
    const v0, 0xd471375

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1889546
    const v0, 0x1608d988

    return v0
.end method
