.class public final Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/CQM;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3e103365
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel$Serializer;
.end annotation


# instance fields
.field private A:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private B:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private C:Z

.field private D:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private E:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private F:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PageTWEFragmentModel$RedirectionInfoModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private G:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$Photo320FragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private H:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private I:Z

.field private J:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private K:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private L:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private M:Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Z

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Z

.field private k:Z

.field private l:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$HoursModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Z

.field private p:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private x:Lcom/facebook/graphql/enums/GraphQLPlaceType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private z:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1887638
    const-class v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1887639
    const-class v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1887640
    const/16 v0, 0x23

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1887641
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1887642
    const/16 v0, 0x23

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1887643
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1887644
    return-void
.end method

.method private M()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1887645
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->e:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->e:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;

    .line 1887646
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->e:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;

    return-object v0
.end method

.method private N()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1887647
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->l:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->l:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    .line 1887648
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->l:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    return-object v0
.end method

.method private O()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1887649
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->p:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->p:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    .line 1887650
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->p:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    return-object v0
.end method

.method private P()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1887651
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->r:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->r:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;

    .line 1887652
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->r:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;

    return-object v0
.end method

.method private Q()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1887479
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->s:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->s:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;

    .line 1887480
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->s:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;

    return-object v0
.end method

.method private R()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1887653
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->t:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->t:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;

    .line 1887654
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->t:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;

    return-object v0
.end method

.method private S()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1887655
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->v:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->v:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;

    .line 1887656
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->v:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;

    return-object v0
.end method

.method private T()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1887657
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->z:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->z:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    .line 1887658
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->z:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    return-object v0
.end method

.method private U()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1887659
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->A:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->A:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    .line 1887660
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->A:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    return-object v0
.end method

.method private V()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1887661
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->B:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->B:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;

    .line 1887662
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->B:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;

    return-object v0
.end method

.method private W()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1887663
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->D:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->D:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;

    .line 1887664
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->D:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;

    return-object v0
.end method

.method private X()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1887735
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->E:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->E:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;

    .line 1887736
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->E:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;

    return-object v0
.end method

.method public static a(LX/CQM;)Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1887665
    if-nez p0, :cond_0

    .line 1887666
    const/4 p0, 0x0

    .line 1887667
    :goto_0
    return-object p0

    .line 1887668
    :cond_0
    instance-of v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;

    if-eqz v0, :cond_1

    .line 1887669
    check-cast p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;

    goto :goto_0

    .line 1887670
    :cond_1
    new-instance v3, LX/CQj;

    invoke-direct {v3}, LX/CQj;-><init>()V

    .line 1887671
    invoke-interface {p0}, LX/CQM;->b()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;->a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;)Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;

    move-result-object v0

    iput-object v0, v3, LX/CQj;->a:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;

    .line 1887672
    invoke-interface {p0}, LX/CQM;->c()Z

    move-result v0

    iput-boolean v0, v3, LX/CQj;->b:Z

    .line 1887673
    invoke-interface {p0}, LX/CQM;->d()Z

    move-result v0

    iput-boolean v0, v3, LX/CQj;->c:Z

    .line 1887674
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    move v0, v1

    .line 1887675
    :goto_1
    invoke-interface {p0}, LX/CQM;->e()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    if-ge v0, v4, :cond_2

    .line 1887676
    invoke-interface {p0}, LX/CQM;->e()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1887677
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1887678
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v3, LX/CQj;->d:LX/0Px;

    .line 1887679
    invoke-interface {p0}, LX/CQM;->jH_()Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    move-result-object v0

    iput-object v0, v3, LX/CQj;->e:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    .line 1887680
    invoke-interface {p0}, LX/CQM;->jI_()Z

    move-result v0

    iput-boolean v0, v3, LX/CQj;->f:Z

    .line 1887681
    invoke-interface {p0}, LX/CQM;->j()Z

    move-result v0

    iput-boolean v0, v3, LX/CQj;->g:Z

    .line 1887682
    invoke-interface {p0}, LX/CQM;->k()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;->a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;)Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    move-result-object v0

    iput-object v0, v3, LX/CQj;->h:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    .line 1887683
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    move v2, v1

    .line 1887684
    :goto_2
    invoke-interface {p0}, LX/CQM;->l()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 1887685
    invoke-interface {p0}, LX/CQM;->l()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$HoursModel;

    invoke-static {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$HoursModel;->a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$HoursModel;)Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$HoursModel;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1887686
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 1887687
    :cond_3
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v3, LX/CQj;->i:LX/0Px;

    .line 1887688
    invoke-interface {p0}, LX/CQM;->m()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/CQj;->j:Ljava/lang/String;

    .line 1887689
    invoke-interface {p0}, LX/CQM;->n()Z

    move-result v0

    iput-boolean v0, v3, LX/CQj;->k:Z

    .line 1887690
    invoke-interface {p0}, LX/CQM;->o()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;->a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;)Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    move-result-object v0

    iput-object v0, v3, LX/CQj;->l:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    .line 1887691
    invoke-interface {p0}, LX/CQM;->p()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/CQj;->m:Ljava/lang/String;

    .line 1887692
    invoke-interface {p0}, LX/CQM;->q()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;->a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;)Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;

    move-result-object v0

    iput-object v0, v3, LX/CQj;->n:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;

    .line 1887693
    invoke-interface {p0}, LX/CQM;->r()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;->a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;)Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;

    move-result-object v0

    iput-object v0, v3, LX/CQj;->o:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;

    .line 1887694
    invoke-interface {p0}, LX/CQM;->s()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;->a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;)Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;

    move-result-object v0

    iput-object v0, v3, LX/CQj;->p:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;

    .line 1887695
    invoke-interface {p0}, LX/CQM;->t()Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    move-result-object v0

    iput-object v0, v3, LX/CQj;->q:Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    .line 1887696
    invoke-interface {p0}, LX/CQM;->u()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;->a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;)Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;

    move-result-object v0

    iput-object v0, v3, LX/CQj;->r:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;

    .line 1887697
    invoke-interface {p0}, LX/CQM;->v()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v0

    iput-object v0, v3, LX/CQj;->s:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    .line 1887698
    invoke-interface {p0}, LX/CQM;->w()Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v0

    iput-object v0, v3, LX/CQj;->t:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 1887699
    invoke-interface {p0}, LX/CQM;->x()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/CQj;->u:Ljava/lang/String;

    .line 1887700
    invoke-interface {p0}, LX/CQM;->y()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;->a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;)Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    move-result-object v0

    iput-object v0, v3, LX/CQj;->v:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    .line 1887701
    invoke-interface {p0}, LX/CQM;->z()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;->a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;)Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    move-result-object v0

    iput-object v0, v3, LX/CQj;->w:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    .line 1887702
    invoke-interface {p0}, LX/CQM;->A()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;->a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;)Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;

    move-result-object v0

    iput-object v0, v3, LX/CQj;->x:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;

    .line 1887703
    invoke-interface {p0}, LX/CQM;->B()Z

    move-result v0

    iput-boolean v0, v3, LX/CQj;->y:Z

    .line 1887704
    invoke-interface {p0}, LX/CQM;->C()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;->a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;)Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;

    move-result-object v0

    iput-object v0, v3, LX/CQj;->z:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;

    .line 1887705
    invoke-interface {p0}, LX/CQM;->D()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;->a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;)Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;

    move-result-object v0

    iput-object v0, v3, LX/CQj;->A:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;

    .line 1887706
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    move v2, v1

    .line 1887707
    :goto_3
    invoke-interface {p0}, LX/CQM;->E()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 1887708
    invoke-interface {p0}, LX/CQM;->E()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PageTWEFragmentModel$RedirectionInfoModel;

    invoke-static {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PageTWEFragmentModel$RedirectionInfoModel;->a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PageTWEFragmentModel$RedirectionInfoModel;)Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PageTWEFragmentModel$RedirectionInfoModel;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1887709
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 1887710
    :cond_4
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v3, LX/CQj;->B:LX/0Px;

    .line 1887711
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    move v2, v1

    .line 1887712
    :goto_4
    invoke-interface {p0}, LX/CQM;->F()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 1887713
    invoke-interface {p0}, LX/CQM;->F()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$Photo320FragmentModel;

    invoke-static {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$Photo320FragmentModel;->a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$Photo320FragmentModel;)Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$Photo320FragmentModel;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1887714
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 1887715
    :cond_5
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v3, LX/CQj;->C:LX/0Px;

    .line 1887716
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    move v0, v1

    .line 1887717
    :goto_5
    invoke-interface {p0}, LX/CQM;->G()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    if-ge v0, v4, :cond_6

    .line 1887718
    invoke-interface {p0}, LX/CQM;->G()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1887719
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 1887720
    :cond_6
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v3, LX/CQj;->D:LX/0Px;

    .line 1887721
    invoke-interface {p0}, LX/CQM;->H()Z

    move-result v0

    iput-boolean v0, v3, LX/CQj;->E:Z

    .line 1887722
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    move v0, v1

    .line 1887723
    :goto_6
    invoke-interface {p0}, LX/CQM;->I()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    if-ge v0, v4, :cond_7

    .line 1887724
    invoke-interface {p0}, LX/CQM;->I()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1887725
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 1887726
    :cond_7
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v3, LX/CQj;->F:LX/0Px;

    .line 1887727
    invoke-interface {p0}, LX/CQM;->J()Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    move-result-object v0

    iput-object v0, v3, LX/CQj;->G:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 1887728
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1887729
    :goto_7
    invoke-interface {p0}, LX/CQM;->K()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v1, v2, :cond_8

    .line 1887730
    invoke-interface {p0}, LX/CQM;->K()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1887731
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 1887732
    :cond_8
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v3, LX/CQj;->H:LX/0Px;

    .line 1887733
    invoke-interface {p0}, LX/CQM;->L()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    iput-object v0, v3, LX/CQj;->I:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1887734
    invoke-virtual {v3}, LX/CQj;->a()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;

    move-result-object p0

    goto/16 :goto_0
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1887756
    iput-boolean p1, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->j:Z

    .line 1887757
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1887758
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1887759
    if-eqz v0, :cond_0

    .line 1887760
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1887761
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic A()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1887755
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->V()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;

    move-result-object v0

    return-object v0
.end method

.method public final B()Z
    .locals 2

    .prologue
    .line 1887753
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1887754
    iget-boolean v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->C:Z

    return v0
.end method

.method public final synthetic C()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1887752
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->W()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic D()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1887751
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->X()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final E()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PageTWEFragmentModel$RedirectionInfoModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1887749
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->F:Ljava/util/List;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$PageTWEFragmentModel$RedirectionInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->F:Ljava/util/List;

    .line 1887750
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->F:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final F()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$Photo320FragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1887762
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->G:Ljava/util/List;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$Photo320FragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->G:Ljava/util/List;

    .line 1887763
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->G:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final G()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1887747
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->H:Ljava/util/List;

    const/16 v1, 0x1d

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->H:Ljava/util/List;

    .line 1887748
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->H:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final H()Z
    .locals 2

    .prologue
    .line 1887745
    const/4 v0, 0x3

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1887746
    iget-boolean v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->I:Z

    return v0
.end method

.method public final I()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1887743
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->J:Ljava/util/List;

    const/16 v1, 0x1f

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->J:Ljava/util/List;

    .line 1887744
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->J:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final J()Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1887741
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->K:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const/16 v1, 0x20

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->K:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 1887742
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->K:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    return-object v0
.end method

.method public final K()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1887739
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->L:Ljava/util/List;

    const/16 v1, 0x21

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->L:Ljava/util/List;

    .line 1887740
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->L:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final L()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1887737
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->M:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/16 v1, 0x22

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->M:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1887738
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->M:Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 31

    .prologue
    .line 1887493
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1887494
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->M()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1887495
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->e()LX/0Px;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/util/List;)I

    move-result v3

    .line 1887496
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->jH_()Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    .line 1887497
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->N()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1887498
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->l()LX/0Px;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v6

    .line 1887499
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->m()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1887500
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->O()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1887501
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->p()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1887502
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->P()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1887503
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->Q()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1887504
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->R()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1887505
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->t()Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v13

    .line 1887506
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->S()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 1887507
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->v()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v15

    .line 1887508
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->w()Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v16

    .line 1887509
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->x()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    .line 1887510
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->T()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 1887511
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->U()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 1887512
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->V()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 1887513
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->W()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 1887514
    invoke-direct/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->X()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 1887515
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->E()LX/0Px;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v23

    .line 1887516
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->F()LX/0Px;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v24

    .line 1887517
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->G()LX/0Px;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v25

    .line 1887518
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->I()LX/0Px;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v26

    .line 1887519
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->J()Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v27

    .line 1887520
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->K()LX/0Px;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v28

    .line 1887521
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->L()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v29

    .line 1887522
    const/16 v30, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1887523
    const/16 v30, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1887524
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->f:Z

    move/from16 v30, v0

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1887525
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->g:Z

    move/from16 v30, v0

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1887526
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1887527
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1887528
    const/4 v2, 0x5

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->j:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1887529
    const/4 v2, 0x6

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->k:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1887530
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1887531
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1887532
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1887533
    const/16 v2, 0xa

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->o:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1887534
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1887535
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1887536
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1887537
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1887538
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1887539
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1887540
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1887541
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1887542
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1887543
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1887544
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1887545
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1887546
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1887547
    const/16 v2, 0x18

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->C:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1887548
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1887549
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1887550
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1887551
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1887552
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1887553
    const/16 v2, 0x1e

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->I:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1887554
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1887555
    const/16 v2, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1887556
    const/16 v2, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1887557
    const/16 v2, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1887558
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1887559
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1887560
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1887561
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->M()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1887562
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->M()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;

    .line 1887563
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->M()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1887564
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;

    .line 1887565
    iput-object v0, v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->e:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;

    .line 1887566
    :cond_0
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->N()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1887567
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->N()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    .line 1887568
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->N()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1887569
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;

    .line 1887570
    iput-object v0, v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->l:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    .line 1887571
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->l()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1887572
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->l()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1887573
    if-eqz v2, :cond_2

    .line 1887574
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;

    .line 1887575
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->m:Ljava/util/List;

    move-object v1, v0

    .line 1887576
    :cond_2
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->O()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1887577
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->O()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    .line 1887578
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->O()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1887579
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;

    .line 1887580
    iput-object v0, v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->p:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    .line 1887581
    :cond_3
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->P()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1887582
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->P()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;

    .line 1887583
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->P()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1887584
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;

    .line 1887585
    iput-object v0, v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->r:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;

    .line 1887586
    :cond_4
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->Q()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1887587
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->Q()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;

    .line 1887588
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->Q()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 1887589
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;

    .line 1887590
    iput-object v0, v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->s:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;

    .line 1887591
    :cond_5
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->R()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1887592
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->R()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;

    .line 1887593
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->R()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 1887594
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;

    .line 1887595
    iput-object v0, v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->t:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;

    .line 1887596
    :cond_6
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->S()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 1887597
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->S()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;

    .line 1887598
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->S()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 1887599
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;

    .line 1887600
    iput-object v0, v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->v:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;

    .line 1887601
    :cond_7
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->T()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 1887602
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->T()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    .line 1887603
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->T()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 1887604
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;

    .line 1887605
    iput-object v0, v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->z:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    .line 1887606
    :cond_8
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->U()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 1887607
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->U()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    .line 1887608
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->U()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 1887609
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;

    .line 1887610
    iput-object v0, v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->A:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    .line 1887611
    :cond_9
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->V()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 1887612
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->V()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;

    .line 1887613
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->V()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 1887614
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;

    .line 1887615
    iput-object v0, v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->B:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesPageProfilePhotoModel$ProfilePhotoModel;

    .line 1887616
    :cond_a
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->W()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 1887617
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->W()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;

    .line 1887618
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->W()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 1887619
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;

    .line 1887620
    iput-object v0, v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->D:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$RatersModel;

    .line 1887621
    :cond_b
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->X()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 1887622
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->X()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;

    .line 1887623
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->X()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 1887624
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;

    .line 1887625
    iput-object v0, v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->E:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;

    .line 1887626
    :cond_c
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->E()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 1887627
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->E()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1887628
    if-eqz v2, :cond_d

    .line 1887629
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;

    .line 1887630
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->F:Ljava/util/List;

    move-object v1, v0

    .line 1887631
    :cond_d
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->F()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 1887632
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->F()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1887633
    if-eqz v2, :cond_e

    .line 1887634
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;

    .line 1887635
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->G:Ljava/util/List;

    move-object v1, v0

    .line 1887636
    :cond_e
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1887637
    if-nez v1, :cond_f

    :goto_0
    return-object p0

    :cond_f
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1887467
    new-instance v0, LX/CQk;

    invoke-direct {v0, p1}, LX/CQk;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1887466
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1887457
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1887458
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->f:Z

    .line 1887459
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->g:Z

    .line 1887460
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->j:Z

    .line 1887461
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->k:Z

    .line 1887462
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->o:Z

    .line 1887463
    const/16 v0, 0x18

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->C:Z

    .line 1887464
    const/16 v0, 0x1e

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->I:Z

    .line 1887465
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1887451
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1887452
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->jI_()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1887453
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1887454
    const/4 v0, 0x5

    iput v0, p2, LX/18L;->c:I

    .line 1887455
    :goto_0
    return-void

    .line 1887456
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1887448
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1887449
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->a(Z)V

    .line 1887450
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1887445
    new-instance v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;

    invoke-direct {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;-><init>()V

    .line 1887446
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1887447
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1887444
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->M()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;

    move-result-object v0

    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1887429
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1887430
    iget-boolean v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->f:Z

    return v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 1887442
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1887443
    iget-boolean v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->g:Z

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1887441
    const v0, 0x6ca33a0a

    return v0
.end method

.method public final e()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1887439
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->h:Ljava/util/List;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->h:Ljava/util/List;

    .line 1887440
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1887438
    const v0, 0x25d6af

    return v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 1887436
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1887437
    iget-boolean v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->k:Z

    return v0
.end method

.method public final jH_()Lcom/facebook/graphql/enums/GraphQLPageCategoryType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1887434
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->i:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->i:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    .line 1887435
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->i:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    return-object v0
.end method

.method public final jI_()Z
    .locals 2

    .prologue
    .line 1887432
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1887433
    iget-boolean v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->j:Z

    return v0
.end method

.method public final synthetic k()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1887431
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->N()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$HoursModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1887468
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->m:Ljava/util/List;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$HoursModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->m:Ljava/util/List;

    .line 1887469
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->m:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1887470
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->n:Ljava/lang/String;

    .line 1887471
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Z
    .locals 2

    .prologue
    .line 1887472
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1887473
    iget-boolean v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->o:Z

    return v0
.end method

.method public final synthetic o()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1887474
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->O()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$LocationModel;

    move-result-object v0

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1887475
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->q:Ljava/lang/String;

    .line 1887476
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic q()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1887477
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->P()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic r()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1887478
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->Q()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageLikersModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic s()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1887481
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->R()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PageVisitsModel;

    move-result-object v0

    return-object v0
.end method

.method public final t()Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1887482
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->u:Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->u:Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    .line 1887483
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->u:Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    return-object v0
.end method

.method public final synthetic u()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1887484
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->S()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$PlaceOpenStatusModel;

    move-result-object v0

    return-object v0
.end method

.method public final v()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1887485
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->w:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->w:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    .line 1887486
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->w:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    return-object v0
.end method

.method public final w()Lcom/facebook/graphql/enums/GraphQLPlaceType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1887487
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->x:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPlaceType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPlaceType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->x:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 1887488
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->x:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    return-object v0
.end method

.method public final x()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1887489
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->y:Ljava/lang/String;

    const/16 v1, 0x14

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->y:Ljava/lang/String;

    .line 1887490
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->y:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic y()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1887491
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->T()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic z()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1887492
    invoke-direct {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;->U()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    move-result-object v0

    return-object v0
.end method
