.class public final Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/CQN;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xbc85164
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePaginationInfoModel$PageInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1888540
    const-class v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1888541
    const-class v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1888542
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1888543
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1888576
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1888577
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1888578
    return-void
.end method

.method public static a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;)Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;
    .locals 13

    .prologue
    .line 1888493
    if-nez p0, :cond_0

    .line 1888494
    const/4 p0, 0x0

    .line 1888495
    :goto_0
    return-object p0

    .line 1888496
    :cond_0
    instance-of v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;

    if-eqz v0, :cond_1

    .line 1888497
    check-cast p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;

    goto :goto_0

    .line 1888498
    :cond_1
    new-instance v2, LX/CQm;

    invoke-direct {v2}, LX/CQm;-><init>()V

    .line 1888499
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->a()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;->a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;)Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

    move-result-object v0

    iput-object v0, v2, LX/CQm;->a:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

    .line 1888500
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1888501
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1888502
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel$EdgesModel;

    invoke-static {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel$EdgesModel;->a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel$EdgesModel;)Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel$EdgesModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1888503
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1888504
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/CQm;->b:LX/0Px;

    .line 1888505
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->c()Z

    move-result v0

    iput-boolean v0, v2, LX/CQm;->c:Z

    .line 1888506
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->d()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePaginationInfoModel$PageInfoModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePaginationInfoModel$PageInfoModel;->a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePaginationInfoModel$PageInfoModel;)Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePaginationInfoModel$PageInfoModel;

    move-result-object v0

    iput-object v0, v2, LX/CQm;->d:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePaginationInfoModel$PageInfoModel;

    .line 1888507
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/CQm;->e:Ljava/lang/String;

    .line 1888508
    const/4 v8, 0x1

    const/4 v12, 0x0

    const/4 v6, 0x0

    .line 1888509
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 1888510
    iget-object v5, v2, LX/CQm;->a:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1888511
    iget-object v7, v2, LX/CQm;->b:LX/0Px;

    invoke-static {v4, v7}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v7

    .line 1888512
    iget-object v9, v2, LX/CQm;->d:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePaginationInfoModel$PageInfoModel;

    invoke-static {v4, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1888513
    iget-object v10, v2, LX/CQm;->e:Ljava/lang/String;

    invoke-virtual {v4, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1888514
    const/4 v11, 0x5

    invoke-virtual {v4, v11}, LX/186;->c(I)V

    .line 1888515
    invoke-virtual {v4, v12, v5}, LX/186;->b(II)V

    .line 1888516
    invoke-virtual {v4, v8, v7}, LX/186;->b(II)V

    .line 1888517
    const/4 v5, 0x2

    iget-boolean v7, v2, LX/CQm;->c:Z

    invoke-virtual {v4, v5, v7}, LX/186;->a(IZ)V

    .line 1888518
    const/4 v5, 0x3

    invoke-virtual {v4, v5, v9}, LX/186;->b(II)V

    .line 1888519
    const/4 v5, 0x4

    invoke-virtual {v4, v5, v10}, LX/186;->b(II)V

    .line 1888520
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 1888521
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 1888522
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 1888523
    invoke-virtual {v5, v12}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1888524
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1888525
    new-instance v5, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;

    invoke-direct {v5, v4}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;-><init>(LX/15i;)V

    .line 1888526
    move-object p0, v5

    .line 1888527
    goto/16 :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1888544
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1888545
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->j()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1888546
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->b()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1888547
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->k()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePaginationInfoModel$PageInfoModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1888548
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1888549
    const/4 v4, 0x5

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1888550
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1888551
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1888552
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1888553
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1888554
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1888555
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1888556
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1888557
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1888558
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->j()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1888559
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->j()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

    .line 1888560
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->j()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1888561
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;

    .line 1888562
    iput-object v0, v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->e:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

    .line 1888563
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->b()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1888564
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->b()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1888565
    if-eqz v2, :cond_1

    .line 1888566
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;

    .line 1888567
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->f:Ljava/util/List;

    move-object v1, v0

    .line 1888568
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->k()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePaginationInfoModel$PageInfoModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1888569
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->k()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePaginationInfoModel$PageInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePaginationInfoModel$PageInfoModel;

    .line 1888570
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->k()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePaginationInfoModel$PageInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1888571
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;

    .line 1888572
    iput-object v0, v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->h:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePaginationInfoModel$PageInfoModel;

    .line 1888573
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1888574
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1888575
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->j()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1888535
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1888536
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->g:Z

    .line 1888537
    return-void
.end method

.method public final b()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1888538
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel$EdgesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->f:Ljava/util/List;

    .line 1888539
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1888532
    new-instance v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;

    invoke-direct {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;-><init>()V

    .line 1888533
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1888534
    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1888530
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1888531
    iget-boolean v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->g:Z

    return v0
.end method

.method public final synthetic d()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePaginationInfoModel$PageInfoModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1888529
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->k()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePaginationInfoModel$PageInfoModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1888528
    const v0, 0x575a784b

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1888491
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->i:Ljava/lang/String;

    .line 1888492
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1888490
    const v0, -0x768ae23e

    return v0
.end method

.method public final j()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1888488
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->e:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->e:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

    .line 1888489
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->e:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

    return-object v0
.end method

.method public final k()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePaginationInfoModel$PageInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1888486
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->h:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePaginationInfoModel$PageInfoModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePaginationInfoModel$PageInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePaginationInfoModel$PageInfoModel;

    iput-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->h:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePaginationInfoModel$PageInfoModel;

    .line 1888487
    iget-object v0, p0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePlaceHugeResultsConnectionFragmentModel;->h:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesNewBrowsePaginationInfoModel$PageInfoModel;

    return-object v0
.end method
