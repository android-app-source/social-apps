.class public final Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1887426
    const-class v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;

    new-instance v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1887427
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1887428
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;LX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 1887281
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1887282
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/16 p0, 0x13

    const/16 v7, 0x12

    const/16 v6, 0x10

    const/4 v5, 0x4

    const/4 v4, 0x3

    .line 1887283
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1887284
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1887285
    if-eqz v2, :cond_0

    .line 1887286
    const-string v3, "address"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1887287
    invoke-static {v1, v2, p1}, LX/CR8;->a(LX/15i;ILX/0nX;)V

    .line 1887288
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1887289
    if-eqz v2, :cond_1

    .line 1887290
    const-string v3, "can_viewer_claim"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1887291
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1887292
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1887293
    if-eqz v2, :cond_2

    .line 1887294
    const-string v3, "can_viewer_rate"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1887295
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1887296
    :cond_2
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 1887297
    if-eqz v2, :cond_3

    .line 1887298
    const-string v2, "category_names"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1887299
    invoke-virtual {v1, v0, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1887300
    :cond_3
    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v2

    .line 1887301
    if-eqz v2, :cond_4

    .line 1887302
    const-string v2, "category_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1887303
    invoke-virtual {v1, v0, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1887304
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1887305
    if-eqz v2, :cond_5

    .line 1887306
    const-string v3, "does_viewer_like"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1887307
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1887308
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1887309
    if-eqz v2, :cond_6

    .line 1887310
    const-string v3, "expressed_as_place"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1887311
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1887312
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1887313
    if-eqz v2, :cond_7

    .line 1887314
    const-string v3, "friends_who_visited"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1887315
    invoke-static {v1, v2, p1, p2}, LX/CRL;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1887316
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1887317
    if-eqz v2, :cond_8

    .line 1887318
    const-string v3, "hours"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1887319
    invoke-static {v1, v2, p1, p2}, LX/CR9;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1887320
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1887321
    if-eqz v2, :cond_9

    .line 1887322
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1887323
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1887324
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1887325
    if-eqz v2, :cond_a

    .line 1887326
    const-string v3, "is_owned"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1887327
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1887328
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1887329
    if-eqz v2, :cond_b

    .line 1887330
    const-string v3, "location"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1887331
    invoke-static {v1, v2, p1}, LX/CRA;->a(LX/15i;ILX/0nX;)V

    .line 1887332
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1887333
    if-eqz v2, :cond_c

    .line 1887334
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1887335
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1887336
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1887337
    if-eqz v2, :cond_d

    .line 1887338
    const-string v3, "overall_star_rating"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1887339
    invoke-static {v1, v2, p1}, LX/CRB;->a(LX/15i;ILX/0nX;)V

    .line 1887340
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1887341
    if-eqz v2, :cond_e

    .line 1887342
    const-string v3, "page_likers"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1887343
    invoke-static {v1, v2, p1}, LX/CRC;->a(LX/15i;ILX/0nX;)V

    .line 1887344
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1887345
    if-eqz v2, :cond_f

    .line 1887346
    const-string v3, "page_visits"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1887347
    invoke-static {v1, v2, p1}, LX/CRD;->a(LX/15i;ILX/0nX;)V

    .line 1887348
    :cond_f
    invoke-virtual {v1, v0, v6}, LX/15i;->g(II)I

    move-result v2

    .line 1887349
    if-eqz v2, :cond_10

    .line 1887350
    const-string v2, "permanently_closed_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1887351
    invoke-virtual {v1, v0, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1887352
    :cond_10
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1887353
    if-eqz v2, :cond_11

    .line 1887354
    const-string v3, "place_open_status"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1887355
    invoke-static {v1, v2, p1}, LX/CRE;->a(LX/15i;ILX/0nX;)V

    .line 1887356
    :cond_11
    invoke-virtual {v1, v0, v7}, LX/15i;->g(II)I

    move-result v2

    .line 1887357
    if-eqz v2, :cond_12

    .line 1887358
    const-string v2, "place_open_status_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1887359
    invoke-virtual {v1, v0, v7}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1887360
    :cond_12
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1887361
    if-eqz v2, :cond_13

    .line 1887362
    const-string v2, "place_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1887363
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1887364
    :cond_13
    const/16 v2, 0x14

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1887365
    if-eqz v2, :cond_14

    .line 1887366
    const-string v3, "price_range_description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1887367
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1887368
    :cond_14
    const/16 v2, 0x15

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1887369
    if-eqz v2, :cond_15

    .line 1887370
    const-string v3, "profilePicture50"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1887371
    invoke-static {v1, v2, p1}, LX/CR5;->a(LX/15i;ILX/0nX;)V

    .line 1887372
    :cond_15
    const/16 v2, 0x16

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1887373
    if-eqz v2, :cond_16

    .line 1887374
    const-string v3, "profilePicture74"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1887375
    invoke-static {v1, v2, p1}, LX/CR5;->a(LX/15i;ILX/0nX;)V

    .line 1887376
    :cond_16
    const/16 v2, 0x17

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1887377
    if-eqz v2, :cond_17

    .line 1887378
    const-string v3, "profile_photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1887379
    invoke-static {v1, v2, p1}, LX/CRX;->a(LX/15i;ILX/0nX;)V

    .line 1887380
    :cond_17
    const/16 v2, 0x18

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1887381
    if-eqz v2, :cond_18

    .line 1887382
    const-string v3, "profile_picture_is_silhouette"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1887383
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1887384
    :cond_18
    const/16 v2, 0x19

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1887385
    if-eqz v2, :cond_19

    .line 1887386
    const-string v3, "raters"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1887387
    invoke-static {v1, v2, p1}, LX/CRF;->a(LX/15i;ILX/0nX;)V

    .line 1887388
    :cond_19
    const/16 v2, 0x1a

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1887389
    if-eqz v2, :cond_1a

    .line 1887390
    const-string v3, "recommendationsByViewerFriends"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1887391
    invoke-static {v1, v2, p1, p2}, LX/CRa;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1887392
    :cond_1a
    const/16 v2, 0x1b

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1887393
    if-eqz v2, :cond_1b

    .line 1887394
    const-string v3, "redirection_info"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1887395
    invoke-static {v1, v2, p1, p2}, LX/CRc;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1887396
    :cond_1b
    const/16 v2, 0x1c

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1887397
    if-eqz v2, :cond_1c

    .line 1887398
    const-string v3, "representative_place_photos"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1887399
    invoke-static {v1, v2, p1, p2}, LX/CRd;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1887400
    :cond_1c
    const/16 v2, 0x1d

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1887401
    if-eqz v2, :cond_1d

    .line 1887402
    const-string v2, "short_category_names"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1887403
    const/16 v2, 0x1d

    invoke-virtual {v1, v0, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1887404
    :cond_1d
    const/16 v2, 0x1e

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1887405
    if-eqz v2, :cond_1e

    .line 1887406
    const-string v3, "should_show_reviews_on_profile"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1887407
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1887408
    :cond_1e
    const/16 v2, 0x1f

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1887409
    if-eqz v2, :cond_1f

    .line 1887410
    const-string v2, "spotlight_locals_snippets"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1887411
    const/16 v2, 0x1f

    invoke-virtual {v1, v0, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1887412
    :cond_1f
    const/16 v2, 0x20

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1887413
    if-eqz v2, :cond_20

    .line 1887414
    const-string v2, "super_category_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1887415
    const/16 v2, 0x20

    invoke-virtual {v1, v0, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1887416
    :cond_20
    const/16 v2, 0x21

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1887417
    if-eqz v2, :cond_21

    .line 1887418
    const-string v2, "viewer_profile_permissions"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1887419
    const/16 v2, 0x21

    invoke-virtual {v1, v0, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1887420
    :cond_21
    const/16 v2, 0x22

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1887421
    if-eqz v2, :cond_22

    .line 1887422
    const-string v2, "viewer_saved_state"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1887423
    const/16 v2, 0x22

    invoke-virtual {v1, v0, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1887424
    :cond_22
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1887425
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1887280
    check-cast p1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel$Serializer;->a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesHugeResultCellPageInformationFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
