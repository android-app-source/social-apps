.class public final Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1779041
    const-class v0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;

    new-instance v1, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1779042
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1779039
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;LX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 1779043
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1779044
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v6, 0x0

    const/4 v5, 0x0

    .line 1779045
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1779046
    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v2

    .line 1779047
    if-eqz v2, :cond_0

    .line 1779048
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1779049
    invoke-static {v1, v0, v5, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1779050
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 1779051
    cmp-long v4, v2, v6

    if-eqz v4, :cond_1

    .line 1779052
    const-string v4, "creation_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1779053
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 1779054
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1779055
    if-eqz v2, :cond_2

    .line 1779056
    const-string v3, "creator"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1779057
    invoke-static {v1, v2, p1, p2}, LX/5u6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1779058
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1779059
    if-eqz v2, :cond_3

    .line 1779060
    const-string v3, "feedback"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1779061
    invoke-static {v1, v2, p1, p2}, LX/4aV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1779062
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1779063
    if-eqz v2, :cond_4

    .line 1779064
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1779065
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1779066
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    .line 1779067
    if-eqz v2, :cond_5

    .line 1779068
    const-string v3, "page_rating"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1779069
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1779070
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1779071
    if-eqz v2, :cond_6

    .line 1779072
    const-string v3, "privacy_scope"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1779073
    invoke-static {v1, v2, p1, p2}, LX/5uE;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1779074
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1779075
    if-eqz v2, :cond_7

    .line 1779076
    const-string v3, "represented_profile"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1779077
    invoke-static {v1, v2, p1}, LX/BNs;->a(LX/15i;ILX/0nX;)V

    .line 1779078
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1779079
    if-eqz v2, :cond_8

    .line 1779080
    const-string v3, "reviewer_context"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1779081
    invoke-static {v1, v2, p1, p2}, LX/5uA;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1779082
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1779083
    if-eqz v2, :cond_9

    .line 1779084
    const-string v3, "story"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1779085
    invoke-static {v1, v2, p1}, LX/5u7;->a(LX/15i;ILX/0nX;)V

    .line 1779086
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1779087
    if-eqz v2, :cond_a

    .line 1779088
    const-string v3, "value"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1779089
    invoke-static {v1, v2, p1}, LX/5u1;->a(LX/15i;ILX/0nX;)V

    .line 1779090
    :cond_a
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1779091
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1779040
    check-cast p1, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel$Serializer;->a(Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
