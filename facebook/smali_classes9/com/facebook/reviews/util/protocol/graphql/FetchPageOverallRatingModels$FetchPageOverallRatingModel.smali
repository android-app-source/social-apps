.class public final Lcom/facebook/reviews/util/protocol/graphql/FetchPageOverallRatingModels$FetchPageOverallRatingModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7d4e4eeb
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reviews/util/protocol/graphql/FetchPageOverallRatingModels$FetchPageOverallRatingModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reviews/util/protocol/graphql/FetchPageOverallRatingModels$FetchPageOverallRatingModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1778798
    const-class v0, Lcom/facebook/reviews/util/protocol/graphql/FetchPageOverallRatingModels$FetchPageOverallRatingModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1778797
    const-class v0, Lcom/facebook/reviews/util/protocol/graphql/FetchPageOverallRatingModels$FetchPageOverallRatingModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1778795
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1778796
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1778789
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1778790
    invoke-virtual {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchPageOverallRatingModels$FetchPageOverallRatingModel;->a()Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1778791
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1778792
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1778793
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1778794
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1778781
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1778782
    invoke-virtual {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchPageOverallRatingModels$FetchPageOverallRatingModel;->a()Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1778783
    invoke-virtual {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchPageOverallRatingModels$FetchPageOverallRatingModel;->a()Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    .line 1778784
    invoke-virtual {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchPageOverallRatingModels$FetchPageOverallRatingModel;->a()Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1778785
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reviews/util/protocol/graphql/FetchPageOverallRatingModels$FetchPageOverallRatingModel;

    .line 1778786
    iput-object v0, v1, Lcom/facebook/reviews/util/protocol/graphql/FetchPageOverallRatingModels$FetchPageOverallRatingModel;->e:Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    .line 1778787
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1778788
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1778780
    new-instance v0, LX/BNV;

    invoke-direct {v0, p1}, LX/BNV;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1778778
    iget-object v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchPageOverallRatingModels$FetchPageOverallRatingModel;->e:Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    iput-object v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchPageOverallRatingModels$FetchPageOverallRatingModel;->e:Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    .line 1778779
    iget-object v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchPageOverallRatingModels$FetchPageOverallRatingModel;->e:Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1778776
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1778777
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1778775
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1778770
    new-instance v0, Lcom/facebook/reviews/util/protocol/graphql/FetchPageOverallRatingModels$FetchPageOverallRatingModel;

    invoke-direct {v0}, Lcom/facebook/reviews/util/protocol/graphql/FetchPageOverallRatingModels$FetchPageOverallRatingModel;-><init>()V

    .line 1778771
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1778772
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1778774
    const v0, 0x112a3e92

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1778773
    const v0, 0x25d6af

    return v0
.end method
