.class public final Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/5tj;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2ba485c8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:J

.field private g:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:I

.field private k:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel$RepresentedProfileModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1779201
    const-class v0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1779120
    const-class v0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1779121
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1779122
    return-void
.end method

.method private n()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1779113
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1779114
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1779115
    :cond_0
    iget-object v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private o()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1779123
    iget-object v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->g:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    iput-object v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->g:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    .line 1779124
    iget-object v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->g:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    return-object v0
.end method

.method private p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1779125
    iget-object v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    iput-object v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    .line 1779126
    iget-object v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    return-object v0
.end method

.method private q()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1779127
    iget-object v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->k:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    iput-object v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->k:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    .line 1779128
    iget-object v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->k:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    return-object v0
.end method

.method private r()Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel$RepresentedProfileModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1779129
    iget-object v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->l:Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel$RepresentedProfileModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel$RepresentedProfileModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel$RepresentedProfileModel;

    iput-object v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->l:Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel$RepresentedProfileModel;

    .line 1779130
    iget-object v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->l:Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel$RepresentedProfileModel;

    return-object v0
.end method

.method private s()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1779131
    iget-object v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->m:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    iput-object v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->m:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    .line 1779132
    iget-object v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->m:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    return-object v0
.end method

.method private t()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1779199
    iget-object v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->n:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    iput-object v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->n:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    .line 1779200
    iget-object v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->n:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    return-object v0
.end method

.method private u()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1779197
    iget-object v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->o:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    iput-object v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->o:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    .line 1779198
    iget-object v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->o:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 14

    .prologue
    .line 1779173
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1779174
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1779175
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->o()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1779176
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1779177
    invoke-virtual {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1779178
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->q()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1779179
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->r()Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel$RepresentedProfileModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1779180
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->s()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1779181
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->t()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1779182
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->u()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 1779183
    const/16 v1, 0xb

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1779184
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1779185
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->f:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1779186
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1779187
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1779188
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1779189
    const/4 v0, 0x5

    iget v1, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->j:I

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1779190
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1779191
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 1779192
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 1779193
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 1779194
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v13}, LX/186;->b(II)V

    .line 1779195
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1779196
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1779135
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1779136
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->o()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1779137
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->o()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    .line 1779138
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->o()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1779139
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;

    .line 1779140
    iput-object v0, v1, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->g:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    .line 1779141
    :cond_0
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1779142
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    .line 1779143
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1779144
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;

    .line 1779145
    iput-object v0, v1, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    .line 1779146
    :cond_1
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->q()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1779147
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->q()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    .line 1779148
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->q()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1779149
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;

    .line 1779150
    iput-object v0, v1, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->k:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    .line 1779151
    :cond_2
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->r()Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel$RepresentedProfileModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1779152
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->r()Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel$RepresentedProfileModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel$RepresentedProfileModel;

    .line 1779153
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->r()Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel$RepresentedProfileModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1779154
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;

    .line 1779155
    iput-object v0, v1, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->l:Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel$RepresentedProfileModel;

    .line 1779156
    :cond_3
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->s()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1779157
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->s()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    .line 1779158
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->s()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1779159
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;

    .line 1779160
    iput-object v0, v1, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->m:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    .line 1779161
    :cond_4
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->t()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1779162
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->t()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    .line 1779163
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->t()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 1779164
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;

    .line 1779165
    iput-object v0, v1, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->n:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    .line 1779166
    :cond_5
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->u()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1779167
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->u()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    .line 1779168
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->u()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 1779169
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;

    .line 1779170
    iput-object v0, v1, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->o:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    .line 1779171
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1779172
    if-nez v1, :cond_7

    :goto_0
    return-object p0

    :cond_7
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1779134
    new-instance v0, LX/BNp;

    invoke-direct {v0, p1}, LX/BNp;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1779133
    invoke-virtual {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 1779116
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1779117
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->f:J

    .line 1779118
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->j:I

    .line 1779119
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1779093
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1779094
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1779095
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 1779096
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1779097
    iget v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->j:I

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1779098
    new-instance v0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;

    invoke-direct {v0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;-><init>()V

    .line 1779099
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1779100
    return-object v0
.end method

.method public final bg_()J
    .locals 2

    .prologue
    .line 1779101
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1779102
    iget-wide v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->f:J

    return-wide v0
.end method

.method public final synthetic bh_()LX/1VU;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1779103
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1779104
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->q()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1779112
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->u()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1779105
    const v0, 0x45c989ee

    return v0
.end method

.method public final synthetic e()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1779092
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->o()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$CreatorModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1779106
    const v0, 0x252222

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1779107
    iget-object v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->i:Ljava/lang/String;

    .line 1779108
    iget-object v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic k()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1779109
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->s()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel$ReviewerContextModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic l()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1779110
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->t()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewCreationFieldsModel$StoryModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic m()Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel$RepresentedProfileModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1779111
    invoke-direct {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel;->r()Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel$RepresentedProfileModel;

    move-result-object v0

    return-object v0
.end method
