.class public final Lcom/facebook/reviews/util/protocol/graphql/FetchSingleViewerReviewGraphQLModels$FetchSingleViewerReviewGraphQLModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x124be12b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/reviews/util/protocol/graphql/FetchSingleViewerReviewGraphQLModels$FetchSingleViewerReviewGraphQLModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/reviews/util/protocol/graphql/FetchSingleViewerReviewGraphQLModels$FetchSingleViewerReviewGraphQLModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1778892
    const-class v0, Lcom/facebook/reviews/util/protocol/graphql/FetchSingleViewerReviewGraphQLModels$FetchSingleViewerReviewGraphQLModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1778891
    const-class v0, Lcom/facebook/reviews/util/protocol/graphql/FetchSingleViewerReviewGraphQLModels$FetchSingleViewerReviewGraphQLModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1778889
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1778890
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1778883
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1778884
    invoke-virtual {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchSingleViewerReviewGraphQLModels$FetchSingleViewerReviewGraphQLModel;->a()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1778885
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1778886
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1778887
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1778888
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1778875
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1778876
    invoke-virtual {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchSingleViewerReviewGraphQLModels$FetchSingleViewerReviewGraphQLModel;->a()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1778877
    invoke-virtual {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchSingleViewerReviewGraphQLModels$FetchSingleViewerReviewGraphQLModel;->a()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;

    .line 1778878
    invoke-virtual {p0}, Lcom/facebook/reviews/util/protocol/graphql/FetchSingleViewerReviewGraphQLModels$FetchSingleViewerReviewGraphQLModel;->a()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1778879
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/reviews/util/protocol/graphql/FetchSingleViewerReviewGraphQLModels$FetchSingleViewerReviewGraphQLModel;

    .line 1778880
    iput-object v0, v1, Lcom/facebook/reviews/util/protocol/graphql/FetchSingleViewerReviewGraphQLModels$FetchSingleViewerReviewGraphQLModel;->e:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;

    .line 1778881
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1778882
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1778864
    new-instance v0, LX/BNc;

    invoke-direct {v0, p1}, LX/BNc;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1778873
    iget-object v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchSingleViewerReviewGraphQLModels$FetchSingleViewerReviewGraphQLModel;->e:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;

    iput-object v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchSingleViewerReviewGraphQLModels$FetchSingleViewerReviewGraphQLModel;->e:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;

    .line 1778874
    iget-object v0, p0, Lcom/facebook/reviews/util/protocol/graphql/FetchSingleViewerReviewGraphQLModels$FetchSingleViewerReviewGraphQLModel;->e:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1778871
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1778872
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1778870
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1778867
    new-instance v0, Lcom/facebook/reviews/util/protocol/graphql/FetchSingleViewerReviewGraphQLModels$FetchSingleViewerReviewGraphQLModel;

    invoke-direct {v0}, Lcom/facebook/reviews/util/protocol/graphql/FetchSingleViewerReviewGraphQLModels$FetchSingleViewerReviewGraphQLModel;-><init>()V

    .line 1778868
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1778869
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1778866
    const v0, -0x6986e4e2

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1778865
    const v0, 0x25d6af

    return v0
.end method
