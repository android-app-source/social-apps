.class public final Lcom/facebook/reviews/util/protocol/graphql/FetchSingleViewerReviewGraphQLModels$FetchSingleViewerReviewGraphQLModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/reviews/util/protocol/graphql/FetchSingleViewerReviewGraphQLModels$FetchSingleViewerReviewGraphQLModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1778851
    const-class v0, Lcom/facebook/reviews/util/protocol/graphql/FetchSingleViewerReviewGraphQLModels$FetchSingleViewerReviewGraphQLModel;

    new-instance v1, Lcom/facebook/reviews/util/protocol/graphql/FetchSingleViewerReviewGraphQLModels$FetchSingleViewerReviewGraphQLModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/reviews/util/protocol/graphql/FetchSingleViewerReviewGraphQLModels$FetchSingleViewerReviewGraphQLModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1778852
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1778853
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/reviews/util/protocol/graphql/FetchSingleViewerReviewGraphQLModels$FetchSingleViewerReviewGraphQLModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1778854
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1778855
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1778856
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1778857
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1778858
    if-eqz v2, :cond_0

    .line 1778859
    const-string p0, "viewer_recommendation"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1778860
    invoke-static {v1, v2, p1, p2}, LX/5u2;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1778861
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1778862
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1778863
    check-cast p1, Lcom/facebook/reviews/util/protocol/graphql/FetchSingleViewerReviewGraphQLModels$FetchSingleViewerReviewGraphQLModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/reviews/util/protocol/graphql/FetchSingleViewerReviewGraphQLModels$FetchSingleViewerReviewGraphQLModel$Serializer;->a(Lcom/facebook/reviews/util/protocol/graphql/FetchSingleViewerReviewGraphQLModels$FetchSingleViewerReviewGraphQLModel;LX/0nX;LX/0my;)V

    return-void
.end method
