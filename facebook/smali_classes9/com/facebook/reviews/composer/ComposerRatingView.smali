.class public Lcom/facebook/reviews/composer/ComposerRatingView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/BNM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/view/ViewGroup;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/LinearLayout;

.field public e:Lcom/facebook/widget/ratingbar/AnimatedRatingBar;

.field public f:Landroid/widget/TextView;

.field public g:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/Cgh;",
            ">;"
        }
    .end annotation
.end field

.field private h:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1927403
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1927404
    invoke-direct {p0}, Lcom/facebook/reviews/composer/ComposerRatingView;->a()V

    .line 1927405
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1927406
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1927407
    invoke-direct {p0}, Lcom/facebook/reviews/composer/ComposerRatingView;->a()V

    .line 1927408
    return-void
.end method

.method private a(II)Landroid/text/SpannableStringBuilder;
    .locals 4

    .prologue
    .line 1927409
    iget-object v0, p0, Lcom/facebook/reviews/composer/ComposerRatingView;->a:LX/BNM;

    invoke-virtual {p0}, Lcom/facebook/reviews/composer/ComposerRatingView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0054

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, p1, v1, p2}, LX/BNM;->a(III)Landroid/text/SpannableString;

    move-result-object v0

    .line 1927410
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1927411
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "  "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/reviews/composer/ComposerRatingView;->h:[Ljava/lang/String;

    add-int/lit8 v3, p1, -0x1

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1927412
    return-object v1
.end method

.method public static synthetic a(Lcom/facebook/reviews/composer/ComposerRatingView;II)Landroid/text/SpannableStringBuilder;
    .locals 1

    .prologue
    .line 1927423
    invoke-direct {p0, p1, p2}, Lcom/facebook/reviews/composer/ComposerRatingView;->a(II)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1927413
    const-class v0, Lcom/facebook/reviews/composer/ComposerRatingView;

    invoke-static {v0, p0}, Lcom/facebook/reviews/composer/ComposerRatingView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1927414
    const v0, 0x7f030348

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1927415
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/reviews/composer/ComposerRatingView;->setOrientation(I)V

    .line 1927416
    const v0, 0x7f0d0ac7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/reviews/composer/ComposerRatingView;->b:Landroid/view/ViewGroup;

    .line 1927417
    const v0, 0x7f0d0ac8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/reviews/composer/ComposerRatingView;->c:Landroid/widget/TextView;

    .line 1927418
    const v0, 0x7f0d0ac9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/ratingbar/AnimatedRatingBar;

    iput-object v0, p0, Lcom/facebook/reviews/composer/ComposerRatingView;->e:Lcom/facebook/widget/ratingbar/AnimatedRatingBar;

    .line 1927419
    const v0, 0x7f0d0aca

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/reviews/composer/ComposerRatingView;->d:Landroid/widget/LinearLayout;

    .line 1927420
    const v0, 0x7f0d0acb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/reviews/composer/ComposerRatingView;->f:Landroid/widget/TextView;

    .line 1927421
    invoke-virtual {p0}, Lcom/facebook/reviews/composer/ComposerRatingView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f100039

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reviews/composer/ComposerRatingView;->h:[Ljava/lang/String;

    .line 1927422
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/reviews/composer/ComposerRatingView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/reviews/composer/ComposerRatingView;

    invoke-static {v0}, LX/BNM;->a(LX/0QB;)LX/BNM;

    move-result-object v0

    check-cast v0, LX/BNM;

    iput-object v0, p0, Lcom/facebook/reviews/composer/ComposerRatingView;->a:LX/BNM;

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1927392
    invoke-direct {p0}, Lcom/facebook/reviews/composer/ComposerRatingView;->e()V

    .line 1927393
    invoke-direct {p0}, Lcom/facebook/reviews/composer/ComposerRatingView;->f()V

    .line 1927394
    iget-object v0, p0, Lcom/facebook/reviews/composer/ComposerRatingView;->b:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1927395
    iget-object v0, p0, Lcom/facebook/reviews/composer/ComposerRatingView;->f:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1927396
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 1927397
    iget-object v0, p0, Lcom/facebook/reviews/composer/ComposerRatingView;->b:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1927398
    iget-object v0, p0, Lcom/facebook/reviews/composer/ComposerRatingView;->f:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1927399
    return-void
.end method

.method public static c$redex0(Lcom/facebook/reviews/composer/ComposerRatingView;I)V
    .locals 0

    .prologue
    .line 1927400
    invoke-static {p0, p1}, Lcom/facebook/reviews/composer/ComposerRatingView;->setRatingSelector(Lcom/facebook/reviews/composer/ComposerRatingView;I)V

    .line 1927401
    invoke-direct {p0}, Lcom/facebook/reviews/composer/ComposerRatingView;->d()V

    .line 1927402
    return-void
.end method

.method private d()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x1f4

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 1927342
    iget-object v0, p0, Lcom/facebook/reviews/composer/ComposerRatingView;->e:Lcom/facebook/widget/ratingbar/AnimatedRatingBar;

    invoke-virtual {v0}, Lcom/facebook/widget/ratingbar/AnimatedRatingBar;->getWidth()I

    move-result v0

    div-int/lit8 v3, v0, 0x2

    .line 1927343
    invoke-virtual {p0}, Lcom/facebook/reviews/composer/ComposerRatingView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0b0d56

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/reviews/composer/ComposerRatingView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b03ef

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    add-float/2addr v0, v4

    iget-object v4, p0, Lcom/facebook/reviews/composer/ComposerRatingView;->e:Lcom/facebook/widget/ratingbar/AnimatedRatingBar;

    invoke-virtual {v4}, Lcom/facebook/widget/ratingbar/AnimatedRatingBar;->getY()F

    move-result v4

    sub-float/2addr v0, v4

    float-to-int v4, v0

    .line 1927344
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    int-to-float v5, v3

    int-to-float v6, v4

    move v3, v1

    move v4, v2

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFFF)V

    .line 1927345
    invoke-virtual {v0, v8, v9}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 1927346
    new-instance v3, LX/Cgc;

    invoke-direct {v3, p0}, LX/Cgc;-><init>(Lcom/facebook/reviews/composer/ComposerRatingView;)V

    invoke-virtual {v0, v3}, Landroid/view/animation/ScaleAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1927347
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v3, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1927348
    invoke-virtual {v3, v8, v9}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 1927349
    iget-object v1, p0, Lcom/facebook/reviews/composer/ComposerRatingView;->e:Lcom/facebook/widget/ratingbar/AnimatedRatingBar;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/ratingbar/AnimatedRatingBar;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1927350
    iget-object v0, p0, Lcom/facebook/reviews/composer/ComposerRatingView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1927351
    iget-object v0, p0, Lcom/facebook/reviews/composer/ComposerRatingView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1927352
    return-void
.end method

.method public static d(Lcom/facebook/reviews/composer/ComposerRatingView;I)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 1927353
    new-instance v4, LX/6WS;

    invoke-virtual {p0}, Lcom/facebook/reviews/composer/ComposerRatingView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v4, v0}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 1927354
    sget-object v0, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v4, v0}, LX/0ht;->a(LX/3AV;)V

    .line 1927355
    invoke-virtual {v4}, LX/5OM;->c()LX/5OG;

    move-result-object v5

    move v3, v2

    .line 1927356
    :goto_0
    const/4 v0, 0x5

    if-gt v3, v0, :cond_2

    .line 1927357
    if-ne v3, p1, :cond_0

    move v1, v2

    .line 1927358
    :goto_1
    if-eqz v1, :cond_1

    const v0, 0x7f0a00d2

    .line 1927359
    :goto_2
    invoke-direct {p0, v3, v0}, Lcom/facebook/reviews/composer/ComposerRatingView;->a(II)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/3Ai;->setCheckable(Z)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v1, LX/Cgg;

    invoke-direct {v1, p0, v3}, LX/Cgg;-><init>(Lcom/facebook/reviews/composer/ComposerRatingView;I)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1927360
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 1927361
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    goto :goto_1

    .line 1927362
    :cond_1
    const v0, 0x7f0a010e

    goto :goto_2

    .line 1927363
    :cond_2
    iget-object v0, p0, Lcom/facebook/reviews/composer/ComposerRatingView;->f:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, LX/0ht;->f(Landroid/view/View;)V

    .line 1927364
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 1927365
    iget-object v0, p0, Lcom/facebook/reviews/composer/ComposerRatingView;->e:Lcom/facebook/widget/ratingbar/AnimatedRatingBar;

    const v1, 0x7f0f002a

    invoke-virtual {v0, v1}, Lcom/facebook/widget/ratingbar/BetterRatingBar;->setAccessibilityTextForEachStar(I)V

    .line 1927366
    iget-object v0, p0, Lcom/facebook/reviews/composer/ComposerRatingView;->e:Lcom/facebook/widget/ratingbar/AnimatedRatingBar;

    new-instance v1, LX/Cgd;

    invoke-direct {v1, p0}, LX/Cgd;-><init>(Lcom/facebook/reviews/composer/ComposerRatingView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/ratingbar/BetterRatingBar;->a(LX/8pS;)V

    .line 1927367
    return-void
.end method

.method private f()V
    .locals 5

    .prologue
    .line 1927368
    invoke-virtual {p0}, Lcom/facebook/reviews/composer/ComposerRatingView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 1927369
    const/4 v0, 0x1

    move v1, v0

    :goto_0
    const/4 v0, 0x5

    if-gt v1, v0, :cond_0

    .line 1927370
    const v0, 0x7f0311d8

    iget-object v3, p0, Lcom/facebook/reviews/composer/ComposerRatingView;->d:Landroid/widget/LinearLayout;

    const/4 v4, 0x0

    invoke-virtual {v2, v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1927371
    const v3, 0x7f0a010e

    invoke-direct {p0, v1, v3}, Lcom/facebook/reviews/composer/ComposerRatingView;->a(II)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1927372
    new-instance v3, LX/Cge;

    invoke-direct {v3, p0, v0, v1}, LX/Cge;-><init>(Lcom/facebook/reviews/composer/ComposerRatingView;Landroid/widget/TextView;I)V

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1927373
    iget-object v3, p0, Lcom/facebook/reviews/composer/ComposerRatingView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1927374
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1927375
    :cond_0
    return-void
.end method

.method public static setRatingSelector(Lcom/facebook/reviews/composer/ComposerRatingView;I)V
    .locals 2

    .prologue
    .line 1927376
    if-lez p1, :cond_0

    const/4 v0, 0x5

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1927377
    iget-object v0, p0, Lcom/facebook/reviews/composer/ComposerRatingView;->f:Landroid/widget/TextView;

    const v1, 0x7f0a00d2

    invoke-direct {p0, p1, v1}, Lcom/facebook/reviews/composer/ComposerRatingView;->a(II)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1927378
    iget-object v0, p0, Lcom/facebook/reviews/composer/ComposerRatingView;->f:Landroid/widget/TextView;

    new-instance v1, LX/Cgf;

    invoke-direct {v1, p0, p1}, LX/Cgf;-><init>(Lcom/facebook/reviews/composer/ComposerRatingView;I)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1927379
    return-void

    .line 1927380
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public setOnRatingChangedListener(LX/Cgh;)V
    .locals 1
    .param p1    # LX/Cgh;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1927381
    invoke-static {p1}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reviews/composer/ComposerRatingView;->g:LX/0am;

    .line 1927382
    return-void
.end method

.method public setPageName(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1927383
    iget-object v0, p0, Lcom/facebook/reviews/composer/ComposerRatingView;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/reviews/composer/ComposerRatingView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081503

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1927384
    return-void
.end method

.method public setRating(Ljava/lang/Integer;)V
    .locals 2

    .prologue
    .line 1927385
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x5

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1927386
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lez v0, :cond_1

    .line 1927387
    invoke-direct {p0}, Lcom/facebook/reviews/composer/ComposerRatingView;->c()V

    .line 1927388
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p0, v0}, Lcom/facebook/reviews/composer/ComposerRatingView;->setRatingSelector(Lcom/facebook/reviews/composer/ComposerRatingView;I)V

    .line 1927389
    :goto_1
    return-void

    .line 1927390
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1927391
    :cond_1
    invoke-direct {p0}, Lcom/facebook/reviews/composer/ComposerRatingView;->b()V

    goto :goto_1
.end method
