.class public Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/B6N;


# static fields
.field public static final f:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/B8r;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/B6l;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/B81;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/B6U;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/B6k;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/B8s;",
            ">;"
        }
    .end annotation
.end field

.field public h:Landroid/widget/LinearLayout;

.field public i:LX/B6T;

.field public j:LX/B7F;

.field private k:Lcom/facebook/common/locale/Country;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1747196
    const-class v0, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->f:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1747189
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1747190
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->setOrientation(I)V

    .line 1747191
    invoke-virtual {p0}, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b09e6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1747192
    invoke-virtual {p0, v0, v2, v0, v2}, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->setPadding(IIII)V

    .line 1747193
    const v0, 0x7f0309d2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1747194
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;

    invoke-static {p1}, LX/B8r;->a(LX/0QB;)LX/B8r;

    move-result-object v3

    check-cast v3, LX/B8r;

    invoke-static {p1}, LX/B6l;->a(LX/0QB;)LX/B6l;

    move-result-object v4

    check-cast v4, LX/B6l;

    invoke-static {p1}, LX/B81;->a(LX/0QB;)LX/B81;

    move-result-object v5

    check-cast v5, LX/B81;

    const-class v6, LX/B6U;

    invoke-interface {p1, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/B6U;

    const-class v0, LX/B6k;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/B6k;

    iput-object v3, v2, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->a:LX/B8r;

    iput-object v4, v2, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->b:LX/B6l;

    iput-object v5, v2, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->c:LX/B81;

    iput-object v6, v2, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->d:LX/B6U;

    iput-object p1, v2, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->e:LX/B6k;

    .line 1747195
    return-void
.end method

.method private static a(LX/B7m;LX/B8s;)V
    .locals 3

    .prologue
    .line 1747187
    invoke-interface {p0}, LX/B7m;->getInputValue()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, LX/B7m;->getInputCustomToken()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0}, LX/B7m;->getBoundedInfoFieldData()Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, LX/B8s;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;)V

    .line 1747188
    return-void
.end method

.method private e()V
    .locals 6

    .prologue
    .line 1747077
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->g:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 1747078
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->g:Ljava/util/HashMap;

    .line 1747079
    :cond_0
    invoke-direct {p0}, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->getLeadGenFieldInputs()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B7m;

    .line 1747080
    invoke-interface {v0}, LX/B7m;->getBoundedInfoFieldData()Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->j()Ljava/lang/String;

    move-result-object v0

    .line 1747081
    iget-object v4, p0, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->g:Ljava/util/HashMap;

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1747082
    iget-object v4, p0, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->g:Ljava/util/HashMap;

    new-instance v5, LX/B8s;

    invoke-direct {v5}, LX/B8s;-><init>()V

    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1747083
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1747084
    :cond_2
    return-void
.end method

.method private getLeadGenFieldInputs()LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/B7m;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1747177
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 1747178
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 1747179
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1747180
    instance-of v0, v1, LX/B7m;

    if-eqz v0, :cond_1

    .line 1747181
    instance-of v0, v1, LX/B8R;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 1747182
    check-cast v0, LX/B8R;

    invoke-virtual {v0}, LX/B8R;->getCountry()Lcom/facebook/common/locale/Country;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->k:Lcom/facebook/common/locale/Country;

    .line 1747183
    :cond_0
    check-cast v1, LX/B7m;

    .line 1747184
    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1747185
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1747186
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private setUpProgressBar(I)V
    .locals 4

    .prologue
    .line 1747160
    const v0, 0x7f0d04de

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    .line 1747161
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->j:LX/B7F;

    .line 1747162
    iget-object v2, v1, LX/B7F;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const/4 v3, 0x1

    .line 1747163
    invoke-static {v2}, LX/2sb;->f(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I

    move-result v1

    if-le v1, v3, :cond_2

    :goto_0
    move v2, v3

    .line 1747164
    move v1, v2

    .line 1747165
    if-nez v1, :cond_0

    .line 1747166
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1747167
    :goto_1
    return-void

    .line 1747168
    :cond_0
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->j:LX/B7F;

    .line 1747169
    iget-object v2, v1, LX/B7F;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v2}, LX/2sb;->f(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I

    move-result v2

    move v1, v2

    .line 1747170
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 1747171
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1747172
    add-int/lit8 v1, p1, 0x1

    .line 1747173
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->j:LX/B7F;

    .line 1747174
    iget-boolean v3, v2, LX/B7F;->f:Z

    move v2, v3

    .line 1747175
    if-eqz v2, :cond_1

    .line 1747176
    :goto_2
    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_1

    :cond_1
    move p1, v1

    goto :goto_2

    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1747155
    invoke-direct {p0}, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->getLeadGenFieldInputs()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B7m;

    .line 1747156
    invoke-interface {v0}, LX/B7m;->a()V

    .line 1747157
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1747158
    :cond_0
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->i:LX/B6T;

    invoke-virtual {v0}, LX/B6T;->a()V

    .line 1747159
    return-void
.end method

.method public final a(LX/B7B;ILX/B7F;ILX/B7w;)V
    .locals 8

    .prologue
    .line 1747118
    check-cast p1, LX/B7N;

    .line 1747119
    iput-object p3, p0, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->j:LX/B7F;

    .line 1747120
    const v0, 0x7f0d18f3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1747121
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->j:LX/B7F;

    invoke-virtual {v1}, LX/B7F;->h()Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->f:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1747122
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1747123
    const v0, 0x7f0d18f4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0d18f6

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0d18f7

    invoke-virtual {p0, v2}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-static {v0, v1, v2}, LX/B6U;->a(Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;)LX/B6T;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->i:LX/B6T;

    .line 1747124
    const v0, 0x7f0d18f7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1747125
    const v1, 0x7f0d0083

    const-string v2, "cta_lead_gen_visit_privacy_page_click"

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTag(ILjava/lang/Object;)V

    .line 1747126
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->i:LX/B6T;

    new-instance v1, LX/B6h;

    invoke-direct {v1, p0, p4}, LX/B6h;-><init>(Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;I)V

    invoke-virtual {v0, v1}, LX/B6T;->a(Landroid/view/View$OnClickListener;)V

    .line 1747127
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->i:LX/B6T;

    const/4 p4, 0x0

    .line 1747128
    new-instance v1, Landroid/text/SpannableString;

    .line 1747129
    iget-object v2, p1, LX/B7N;->e:Ljava/lang/String;

    move-object v2, v2

    .line 1747130
    invoke-direct {v1, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1747131
    new-instance v2, LX/B6i;

    invoke-direct {v2, p0, p1}, LX/B6i;-><init>(Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;LX/B7N;)V

    .line 1747132
    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v3

    invoke-virtual {v1, v2, p4, v3, p4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1747133
    new-instance v2, Landroid/text/style/StyleSpan;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v3

    invoke-virtual {v1, v2, p4, v3, p4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1747134
    new-instance v2, Landroid/text/style/UnderlineSpan;

    invoke-direct {v2}, Landroid/text/style/UnderlineSpan;-><init>()V

    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v3

    invoke-virtual {v1, v2, p4, v3, p4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1747135
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p0}, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a00a1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v3

    invoke-virtual {v1, v2, p4, v3, p4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1747136
    move-object v1, v1

    .line 1747137
    invoke-virtual {v0, p1, v1}, LX/B6T;->a(LX/B7N;Landroid/text/SpannableString;)V

    .line 1747138
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->i:LX/B6T;

    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->j:LX/B7F;

    sget-object v2, LX/B77;->NO_ERROR:LX/B77;

    invoke-virtual {v0, v1, v2, p1}, LX/B6T;->a(LX/B7F;LX/B77;LX/B7N;)V

    .line 1747139
    invoke-direct {p0, p2}, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->setUpProgressBar(I)V

    .line 1747140
    const v0, 0x7f0d18f5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->h:Landroid/widget/LinearLayout;

    .line 1747141
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1747142
    iget-object v0, p1, LX/B7N;->a:LX/0Px;

    move-object v4, v0

    .line 1747143
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v3, v0

    :goto_0
    if-ge v3, v5, :cond_1

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    .line 1747144
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->MESSENGER_CHECKBOX:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->l()Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 1747145
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->h:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v6, 0x7f0309b9

    const/4 v7, 0x0

    invoke-static {v2, v6, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1747146
    :cond_0
    invoke-static {p0, v0}, LX/B81;->a(Landroid/view/ViewGroup;Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;)Landroid/view/View;

    move-result-object v2

    move-object v1, v2

    .line 1747147
    check-cast v1, LX/B7m;

    iget-object v6, p0, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->j:LX/B7F;

    invoke-interface {v1, v0, v6, p2}, LX/B7m;->a(Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;LX/B7F;I)V

    move-object v0, v2

    .line 1747148
    check-cast v0, LX/B7m;

    invoke-interface {v0, p5}, LX/B7m;->setOnDataChangeListener(LX/B7w;)V

    .line 1747149
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1747150
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 1747151
    :cond_1
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->h:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/B7m;

    .line 1747152
    instance-of v1, v0, LX/B8K;

    if-eqz v1, :cond_2

    .line 1747153
    check-cast v0, LX/B8K;

    invoke-virtual {v0}, LX/B8K;->d()V

    .line 1747154
    :cond_2
    return-void
.end method

.method public final a(Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;)V
    .locals 5

    .prologue
    .line 1747112
    invoke-direct {p0}, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->getLeadGenFieldInputs()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B7m;

    .line 1747113
    invoke-interface {v0}, LX/B7m;->getBoundedInfoFieldData()Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1747114
    if-eqz v4, :cond_0

    .line 1747115
    invoke-interface {v0, v4}, LX/B7m;->setInputValue(Ljava/lang/String;)V

    .line 1747116
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1747117
    :cond_1
    return-void
.end method

.method public final b()LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/B8s;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1747105
    invoke-direct {p0}, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->e()V

    .line 1747106
    invoke-direct {p0}, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->getLeadGenFieldInputs()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B7m;

    .line 1747107
    invoke-interface {v0}, LX/B7m;->getBoundedInfoFieldData()Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->j()Ljava/lang/String;

    move-result-object v1

    .line 1747108
    iget-object v5, p0, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->g:Ljava/util/HashMap;

    invoke-virtual {v5, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1747109
    iget-object v5, p0, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->g:Ljava/util/HashMap;

    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/B8s;

    invoke-static {v0, v1}, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->a(LX/B7m;LX/B8s;)V

    .line 1747110
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1747111
    :cond_1
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->g:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1747104
    const/4 v0, 0x0

    return-object v0
.end method

.method public getContentScrollView()Lcom/facebook/widget/FbScrollView;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1747103
    const/4 v0, 0x0

    return-object v0
.end method

.method public final l_(I)LX/B77;
    .locals 8

    .prologue
    .line 1747085
    invoke-direct {p0}, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->e()V

    .line 1747086
    sget-object v2, LX/B77;->NO_ERROR:LX/B77;

    .line 1747087
    invoke-direct {p0}, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->getLeadGenFieldInputs()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v3, v0

    :goto_0
    if-ge v3, v5, :cond_0

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B7m;

    .line 1747088
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->g:Ljava/util/HashMap;

    invoke-interface {v0}, LX/B7m;->getBoundedInfoFieldData()Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/B8s;

    .line 1747089
    if-nez v1, :cond_1

    .line 1747090
    sget-object v2, LX/B77;->UNKNOWN_ERROR:LX/B77;

    .line 1747091
    :cond_0
    return-object v2

    .line 1747092
    :cond_1
    invoke-static {v0, v1}, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->a(LX/B7m;LX/B8s;)V

    .line 1747093
    iget-object v6, p0, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->a:LX/B8r;

    invoke-virtual {v6, v1}, LX/B8r;->a(LX/B8s;)Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->k:Lcom/facebook/common/locale/Country;

    invoke-static {v1, v6}, LX/B8r;->a(LX/B8s;Lcom/facebook/common/locale/Country;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 1747094
    :cond_2
    iget-object v2, v1, LX/B8s;->b:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    move-object v2, v2

    .line 1747095
    invoke-static {v2}, LX/2sb;->a(Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;)LX/B77;

    move-result-object v2

    .line 1747096
    iget-object v6, p0, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->j:LX/B7F;

    invoke-virtual {v6, v2}, LX/B7F;->a(LX/B77;)Ljava/lang/String;

    move-result-object v6

    .line 1747097
    invoke-interface {v0, v6}, LX/B7m;->a(Ljava/lang/String;)V

    .line 1747098
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyUserInfoFormView;->b:LX/B6l;

    const-string v6, "cta_lead_gen_user_info_validation_error"

    .line 1747099
    iget-object v7, v1, LX/B8s;->b:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    move-object v1, v7

    .line 1747100
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v6, v1, p1}, LX/B6l;->a(Ljava/lang/String;Ljava/lang/String;I)V

    move-object v0, v2

    .line 1747101
    :goto_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move-object v2, v0

    goto :goto_0

    .line 1747102
    :cond_3
    invoke-interface {v0}, LX/B7m;->c()V

    move-object v0, v2

    goto :goto_1
.end method
