.class public Lcom/facebook/leadgen/view/LeadGenHeaderBackgroundView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThreadImmediate;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/BA0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1751169
    const-class v0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/leadgen/view/LeadGenHeaderBackgroundView;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1751164
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1751165
    const v0, 0x7f0309cc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1751166
    const-class v0, Lcom/facebook/leadgen/view/LeadGenHeaderBackgroundView;

    invoke-static {v0, p0}, Lcom/facebook/leadgen/view/LeadGenHeaderBackgroundView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1751167
    const v0, 0x7f0d18ee

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/leadgen/view/LeadGenHeaderBackgroundView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1751168
    return-void
.end method

.method private a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;Ljava/lang/String;)V
    .locals 4

    .prologue
    const v3, 0x3ff745d1

    .line 1751159
    if-nez p2, :cond_0

    .line 1751160
    :goto_0
    return-void

    .line 1751161
    :cond_0
    iget-object v0, p0, Lcom/facebook/leadgen/view/LeadGenHeaderBackgroundView;->b:LX/BA0;

    invoke-virtual {v0, p2, v3}, LX/BA0;->a(Ljava/lang/String;F)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/B99;

    invoke-direct {v1, p0, p1}, LX/B99;-><init>(Lcom/facebook/leadgen/view/LeadGenHeaderBackgroundView;Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V

    iget-object v2, p0, Lcom/facebook/leadgen/view/LeadGenHeaderBackgroundView;->a:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1751162
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/leadgen/view/LeadGenHeaderBackgroundView;->setVisibility(I)V

    .line 1751163
    invoke-virtual {p1, v3}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/leadgen/view/LeadGenHeaderBackgroundView;Ljava/util/concurrent/Executor;LX/BA0;)V
    .locals 0

    .prologue
    .line 1751142
    iput-object p1, p0, Lcom/facebook/leadgen/view/LeadGenHeaderBackgroundView;->a:Ljava/util/concurrent/Executor;

    iput-object p2, p0, Lcom/facebook/leadgen/view/LeadGenHeaderBackgroundView;->b:LX/BA0;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/leadgen/view/LeadGenHeaderBackgroundView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/leadgen/view/LeadGenHeaderBackgroundView;

    invoke-static {v1}, LX/0W1;->a(LX/0QB;)LX/0Tf;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {v1}, LX/BA0;->a(LX/0QB;)LX/BA0;

    move-result-object v1

    check-cast v1, LX/BA0;

    invoke-static {p0, v0, v1}, Lcom/facebook/leadgen/view/LeadGenHeaderBackgroundView;->a(Lcom/facebook/leadgen/view/LeadGenHeaderBackgroundView;Ljava/util/concurrent/Executor;LX/BA0;)V

    return-void
.end method


# virtual methods
.method public setUpView(LX/B7K;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 1751143
    iget-object v0, p0, Lcom/facebook/leadgen/view/LeadGenHeaderBackgroundView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1751144
    iget-object v0, p0, Lcom/facebook/leadgen/view/LeadGenHeaderBackgroundView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const v1, 0x3ff745d1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 1751145
    instance-of v0, p1, LX/B7L;

    if-eqz v0, :cond_2

    .line 1751146
    check-cast p1, LX/B7L;

    .line 1751147
    iget-object v0, p1, LX/B7L;->c:Lcom/facebook/graphql/model/GraphQLPhoto;

    move-object v0, v0

    .line 1751148
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1751149
    iget-object v1, p0, Lcom/facebook/leadgen/view/LeadGenHeaderBackgroundView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aC()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/facebook/leadgen/view/LeadGenHeaderBackgroundView;->a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;Ljava/lang/String;)V

    .line 1751150
    iget-object v1, p0, Lcom/facebook/leadgen/view/LeadGenHeaderBackgroundView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sget-object v2, Lcom/facebook/leadgen/view/LeadGenHeaderBackgroundView;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1751151
    :goto_0
    return-void

    .line 1751152
    :cond_0
    iget-object v0, p1, LX/B7L;->d:Landroid/net/Uri;

    move-object v0, v0

    .line 1751153
    if-eqz v0, :cond_1

    .line 1751154
    iget-object v0, p0, Lcom/facebook/leadgen/view/LeadGenHeaderBackgroundView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1751155
    iget-object v1, p1, LX/B7L;->d:Landroid/net/Uri;

    move-object v1, v1

    .line 1751156
    sget-object v2, Lcom/facebook/leadgen/view/LeadGenHeaderBackgroundView;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_0

    .line 1751157
    :cond_1
    iget-object v0, p0, Lcom/facebook/leadgen/view/LeadGenHeaderBackgroundView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_0

    .line 1751158
    :cond_2
    iget-object v0, p0, Lcom/facebook/leadgen/view/LeadGenHeaderBackgroundView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_0
.end method
