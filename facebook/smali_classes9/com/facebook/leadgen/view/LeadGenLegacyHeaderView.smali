.class public Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/B6l;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Landroid/view/View;

.field public d:LX/B7i;

.field private e:Landroid/widget/TextView;

.field public f:LX/B6u;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1751261
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1751262
    const v0, 0x7f0309d1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1751263
    const-class v0, Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;

    invoke-static {v0, p0}, Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1751264
    const v0, 0x7f0d18ce

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;->c:Landroid/view/View;

    .line 1751265
    const v0, 0x7f0d04e8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;->e:Landroid/widget/TextView;

    .line 1751266
    return-void
.end method

.method private static a(Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;LX/B6l;LX/0if;)V
    .locals 0

    .prologue
    .line 1751260
    iput-object p1, p0, Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;->a:LX/B6l;

    iput-object p2, p0, Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;->b:LX/0if;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;

    invoke-static {v1}, LX/B6l;->a(LX/0QB;)LX/B6l;

    move-result-object v0

    check-cast v0, LX/B6l;

    invoke-static {v1}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v1

    check-cast v1, LX/0if;

    invoke-static {p0, v0, v1}, Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;->a(Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;LX/B6l;LX/0if;)V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1751258
    iget-object v0, p0, Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;->c:Landroid/view/View;

    new-instance v1, LX/B9B;

    invoke-direct {v1, p0}, LX/B9B;-><init>(Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1751259
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1751256
    iget-object v0, p0, Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;->c:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1751257
    return-void
.end method

.method public final a(LX/B7F;LX/B7i;LX/B6u;)V
    .locals 2

    .prologue
    .line 1751251
    iput-object p2, p0, Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;->d:LX/B7i;

    .line 1751252
    iget-object v0, p0, Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;->e:Landroid/widget/TextView;

    invoke-virtual {p1}, LX/B7F;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1751253
    iput-object p3, p0, Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;->f:LX/B6u;

    .line 1751254
    invoke-direct {p0}, Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;->b()V

    .line 1751255
    return-void
.end method
