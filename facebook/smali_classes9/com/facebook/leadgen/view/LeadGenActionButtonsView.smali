.class public Lcom/facebook/leadgen/view/LeadGenActionButtonsView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/B6l;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/B7W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Landroid/widget/TextView;

.field public e:LX/B6u;

.field public f:LX/B7F;

.field public g:LX/B6S;

.field public h:LX/D82;

.field private final i:LX/B6V;

.field private final j:Landroid/view/View$OnClickListener;

.field private final k:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1750824
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1750825
    new-instance v0, LX/B8w;

    invoke-direct {v0, p0}, LX/B8w;-><init>(Lcom/facebook/leadgen/view/LeadGenActionButtonsView;)V

    iput-object v0, p0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->i:LX/B6V;

    .line 1750826
    new-instance v0, LX/B8x;

    invoke-direct {v0, p0}, LX/B8x;-><init>(Lcom/facebook/leadgen/view/LeadGenActionButtonsView;)V

    iput-object v0, p0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->j:Landroid/view/View$OnClickListener;

    .line 1750827
    new-instance v0, LX/B8y;

    invoke-direct {v0, p0}, LX/B8y;-><init>(Lcom/facebook/leadgen/view/LeadGenActionButtonsView;)V

    iput-object v0, p0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->k:Landroid/view/View$OnClickListener;

    .line 1750828
    const v0, 0x7f0309bb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1750829
    const-class v0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    invoke-static {v0, p0}, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1750830
    return-void
.end method

.method private static a(Lcom/facebook/leadgen/view/LeadGenActionButtonsView;LX/B6l;LX/B7W;LX/0if;)V
    .locals 0

    .prologue
    .line 1750859
    iput-object p1, p0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->a:LX/B6l;

    iput-object p2, p0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->b:LX/B7W;

    iput-object p3, p0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->c:LX/0if;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    invoke-static {v2}, LX/B6l;->a(LX/0QB;)LX/B6l;

    move-result-object v0

    check-cast v0, LX/B6l;

    invoke-static {v2}, LX/B7W;->a(LX/0QB;)LX/B7W;

    move-result-object v1

    check-cast v1, LX/B7W;

    invoke-static {v2}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v2

    check-cast v2, LX/0if;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->a(Lcom/facebook/leadgen/view/LeadGenActionButtonsView;LX/B6l;LX/B7W;LX/0if;)V

    return-void
.end method

.method private setupCustomDisclaimerPageAcceptButton(Z)V
    .locals 3

    .prologue
    .line 1750854
    invoke-virtual {p0}, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1750855
    iget-object v1, p0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->d:Landroid/widget/TextView;

    const v2, 0x7f0824ea

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1750856
    iget-object v0, p0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1750857
    iget-object v0, p0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->d:Landroid/widget/TextView;

    new-instance v1, LX/B8z;

    invoke-direct {v1, p0}, LX/B8z;-><init>(Lcom/facebook/leadgen/view/LeadGenActionButtonsView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1750858
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1750851
    iget-object v0, p0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->d:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1750852
    iget-object v0, p0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->b:LX/B7W;

    iget-object v1, p0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->i:LX/B6V;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1750853
    return-void
.end method

.method public final a(LX/B7F;LX/B6u;LX/D82;)V
    .locals 2

    .prologue
    .line 1750845
    iput-object p2, p0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->e:LX/B6u;

    .line 1750846
    iput-object p1, p0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->f:LX/B7F;

    .line 1750847
    const v0, 0x7f0d18c3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->d:Landroid/widget/TextView;

    .line 1750848
    iget-object v0, p0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->b:LX/B7W;

    iget-object v1, p0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->i:LX/B6V;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1750849
    iput-object p3, p0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->h:LX/D82;

    .line 1750850
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1750842
    iget-object v0, p0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->e:LX/B6u;

    invoke-virtual {v0}, LX/B6u;->n()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1750843
    :goto_0
    return-void

    .line 1750844
    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->setupCustomDisclaimerPageAcceptButton(Z)V

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1750831
    iget-object v0, p0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->e:LX/B6u;

    invoke-virtual {v0}, LX/B6u;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1750832
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->setupCustomDisclaimerPageAcceptButton(Z)V

    .line 1750833
    :goto_0
    return-void

    .line 1750834
    :cond_0
    iget-object v0, p0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->e:LX/B6u;

    invoke-virtual {v0}, LX/B6u;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1750835
    iget-object v0, p0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->f:LX/B7F;

    invoke-virtual {v1}, LX/B7F;->q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1750836
    iget-object v0, p0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->k:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 1750837
    :cond_1
    iget-object v0, p0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->e:LX/B6u;

    invoke-virtual {v0}, LX/B6u;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1750838
    iget-object v0, p0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->f:LX/B7F;

    invoke-virtual {v1}, LX/B7F;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1750839
    iget-object v0, p0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 1750840
    :cond_2
    iget-object v0, p0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0824e3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1750841
    iget-object v0, p0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method
