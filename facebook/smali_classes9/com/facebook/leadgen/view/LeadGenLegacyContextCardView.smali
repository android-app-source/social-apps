.class public Lcom/facebook/leadgen/view/LeadGenLegacyContextCardView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/B6N;


# static fields
.field private static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThreadImmediate;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/BA0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1751234
    const-class v0, Lcom/facebook/leadgen/view/LeadGenLegacyContextCardView;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/leadgen/view/LeadGenLegacyContextCardView;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1751229
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1751230
    const-class v0, Lcom/facebook/leadgen/view/LeadGenLegacyContextCardView;

    invoke-static {v0, p0}, Lcom/facebook/leadgen/view/LeadGenLegacyContextCardView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1751231
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/leadgen/view/LeadGenLegacyContextCardView;->setOrientation(I)V

    .line 1751232
    const v0, 0x7f0309cf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1751233
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/leadgen/view/LeadGenLegacyContextCardView;

    invoke-static {p0}, LX/0W1;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/BA0;->a(LX/0QB;)LX/BA0;

    move-result-object p0

    check-cast p0, LX/BA0;

    iput-object v1, p1, Lcom/facebook/leadgen/view/LeadGenLegacyContextCardView;->a:Ljava/util/concurrent/Executor;

    iput-object p0, p1, Lcom/facebook/leadgen/view/LeadGenLegacyContextCardView;->b:LX/BA0;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1751177
    return-void
.end method

.method public final a(LX/B7B;ILX/B7F;ILX/B7w;)V
    .locals 8

    .prologue
    .line 1751182
    check-cast p1, LX/B7C;

    .line 1751183
    iget-object v0, p1, LX/B7C;->d:LX/B7K;

    move-object v2, v0

    .line 1751184
    const v0, 0x7f0d18f3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1751185
    iget-object v1, p1, LX/B7C;->d:LX/B7K;

    move-object v1, v1

    .line 1751186
    check-cast v1, LX/B7J;

    .line 1751187
    invoke-interface {v1}, LX/B7J;->b()Landroid/net/Uri;

    move-result-object v1

    sget-object v3, Lcom/facebook/leadgen/view/LeadGenLegacyContextCardView;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1751188
    const v0, 0x7f0d18d1    # 1.8755E38f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1751189
    iget-object v1, p1, LX/B7C;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1751190
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1751191
    const v0, 0x7f0d18ee

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1751192
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1751193
    const v1, 0x3ff745d1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 1751194
    instance-of v1, v2, LX/B7L;

    if-eqz v1, :cond_2

    move-object v1, v2

    .line 1751195
    check-cast v1, LX/B7L;

    .line 1751196
    iget-object v2, v1, LX/B7L;->c:Lcom/facebook/graphql/model/GraphQLPhoto;

    move-object v2, v2

    .line 1751197
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 1751198
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPhoto;->aC()Ljava/lang/String;

    move-result-object v1

    const p4, 0x3ff745d1

    .line 1751199
    if-nez v1, :cond_3

    .line 1751200
    :goto_0
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/leadgen/view/LeadGenLegacyContextCardView;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1751201
    :goto_1
    const/4 v3, 0x0

    .line 1751202
    const v0, 0x7f0d18d2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1751203
    iget-object v1, p1, LX/B7C;->c:LX/0Px;

    move-object v4, v1

    .line 1751204
    if-eqz v4, :cond_0

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1751205
    :cond_0
    return-void

    .line 1751206
    :cond_1
    iget-object v2, v1, LX/B7L;->d:Landroid/net/Uri;

    move-object v1, v2

    .line 1751207
    sget-object v2, Lcom/facebook/leadgen/view/LeadGenLegacyContextCardView;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_1

    .line 1751208
    :cond_2
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_1

    .line 1751209
    :cond_3
    iget-object v3, p0, Lcom/facebook/leadgen/view/LeadGenLegacyContextCardView;->b:LX/BA0;

    invoke-virtual {v3, v1, p4}, LX/BA0;->a(Ljava/lang/String;F)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    new-instance p2, LX/B9A;

    invoke-direct {p2, p0, v0}, LX/B9A;-><init>(Lcom/facebook/leadgen/view/LeadGenLegacyContextCardView;Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V

    iget-object p3, p0, Lcom/facebook/leadgen/view/LeadGenLegacyContextCardView;->a:Ljava/util/concurrent/Executor;

    invoke-static {v3, p2, p3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1751210
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/facebook/leadgen/view/LeadGenLegacyContextCardView;->setVisibility(I)V

    .line 1751211
    invoke-virtual {v0, p4}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    goto :goto_0

    .line 1751212
    :cond_4
    iget-object v1, p1, LX/B7C;->b:Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;

    move-object v5, v1

    .line 1751213
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v6

    move v2, v3

    :goto_2
    if-ge v2, v6, :cond_0

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1751214
    new-instance v7, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/leadgen/view/LeadGenLegacyContextCardView;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {v7, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1751215
    invoke-virtual {p0}, Lcom/facebook/leadgen/view/LeadGenLegacyContextCardView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const p3, 0x7f0b0a04

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p2

    invoke-virtual {v7, v3, p2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1751216
    invoke-virtual {p0}, Lcom/facebook/leadgen/view/LeadGenLegacyContextCardView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const p3, 0x7f0a00a7

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getColor(I)I

    move-result p2

    invoke-virtual {v7, p2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1751217
    sget-object p2, Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;->LIST_STYLE:Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;

    if-ne v5, p2, :cond_5

    .line 1751218
    const/4 p1, 0x0

    .line 1751219
    new-instance p2, Landroid/text/SpannableString;

    invoke-direct {p2, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1751220
    new-instance p3, LX/B8o;

    invoke-virtual {p0}, Lcom/facebook/leadgen/view/LeadGenLegacyContextCardView;->getResources()Landroid/content/res/Resources;

    move-result-object p4

    const p5, 0x7f0b19a3

    invoke-virtual {p4, p5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p4

    float-to-int p4, p4

    invoke-direct {p3, p4}, LX/B8o;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result p4

    invoke-virtual {p2, p3, p1, p4, p1}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1751221
    invoke-virtual {v7, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1751222
    invoke-virtual {p0}, Lcom/facebook/leadgen/view/LeadGenLegacyContextCardView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const p3, 0x7f0b19a2

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p2

    float-to-int p2, p2

    .line 1751223
    new-instance p3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 p4, -0x1

    const/4 p5, -0x2

    invoke-direct {p3, p4, p5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1751224
    invoke-virtual {p3, p1, p1, p1, p2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1751225
    invoke-virtual {v7, p3}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1751226
    :goto_3
    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1751227
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 1751228
    :cond_5
    invoke-virtual {v7, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method

.method public final a(Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;)V
    .locals 0

    .prologue
    .line 1751235
    return-void
.end method

.method public final b()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/B8s;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1751181
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1751180
    const/4 v0, 0x0

    return-object v0
.end method

.method public getContentScrollView()Lcom/facebook/widget/FbScrollView;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1751179
    const/4 v0, 0x0

    return-object v0
.end method

.method public final l_(I)LX/B77;
    .locals 1

    .prologue
    .line 1751178
    const/4 v0, 0x0

    return-object v0
.end method
