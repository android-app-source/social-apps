.class public Lcom/facebook/leadgen/view/LeadGenPageProfileHeaderView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private c:Landroid/widget/TextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1751267
    const-class v0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/leadgen/view/LeadGenPageProfileHeaderView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1751268
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1751269
    const v0, 0x7f0309d4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1751270
    const v0, 0x7f0d04e8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/leadgen/view/LeadGenPageProfileHeaderView;->c:Landroid/widget/TextView;

    .line 1751271
    const v0, 0x7f0d18fa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/leadgen/view/LeadGenPageProfileHeaderView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1751272
    return-void
.end method


# virtual methods
.method public final a(LX/B7K;Z)V
    .locals 3

    .prologue
    .line 1751273
    iget-object v1, p0, Lcom/facebook/leadgen/view/LeadGenPageProfileHeaderView;->c:Landroid/widget/TextView;

    move-object v0, p1

    check-cast v0, LX/B7I;

    invoke-interface {v0}, LX/B7I;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1751274
    iget-object v0, p0, Lcom/facebook/leadgen/view/LeadGenPageProfileHeaderView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    check-cast p1, LX/B7J;

    invoke-interface {p1}, LX/B7J;->b()Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/leadgen/view/LeadGenPageProfileHeaderView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1751275
    if-eqz p2, :cond_0

    .line 1751276
    const v0, 0x7f0d18f9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    .line 1751277
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 1751278
    const/4 v2, 0x0

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1751279
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1751280
    invoke-virtual {p0}, Lcom/facebook/leadgen/view/LeadGenPageProfileHeaderView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1751281
    invoke-virtual {p0}, Lcom/facebook/leadgen/view/LeadGenPageProfileHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1989

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1751282
    invoke-virtual {p0, v0}, Lcom/facebook/leadgen/view/LeadGenPageProfileHeaderView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1751283
    :cond_0
    return-void
.end method
