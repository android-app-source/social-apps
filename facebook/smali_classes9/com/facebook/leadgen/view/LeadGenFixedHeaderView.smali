.class public Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final d:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/B6l;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/B7W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:Landroid/view/View;

.field private f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public g:LX/B7i;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/view/View;

.field private j:Landroid/view/View;

.field public k:LX/B6u;

.field public l:Z

.field public m:I

.field public n:Z

.field public o:Z

.field private final p:LX/B7X;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1751134
    const-class v0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->d:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1751124
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1751125
    new-instance v0, LX/B97;

    invoke-direct {v0, p0}, LX/B97;-><init>(Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;)V

    iput-object v0, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->p:LX/B7X;

    .line 1751126
    const v0, 0x7f0309c2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1751127
    const-class v0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;

    invoke-static {v0, p0}, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1751128
    const v0, 0x7f0d18ce

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->e:Landroid/view/View;

    .line 1751129
    const v0, 0x7f0d04e8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->h:Landroid/widget/TextView;

    .line 1751130
    const v0, 0x7f0d18dc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1751131
    const v0, 0x7f0d18db

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->i:Landroid/view/View;

    .line 1751132
    const v0, 0x7f0d18d0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->j:Landroid/view/View;

    .line 1751133
    return-void
.end method

.method private static a(Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;LX/B6l;LX/0if;LX/B7W;)V
    .locals 0

    .prologue
    .line 1751123
    iput-object p1, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->a:LX/B6l;

    iput-object p2, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->b:LX/0if;

    iput-object p3, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->c:LX/B7W;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;

    invoke-static {v2}, LX/B6l;->a(LX/0QB;)LX/B6l;

    move-result-object v0

    check-cast v0, LX/B6l;

    invoke-static {v2}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v1

    check-cast v1, LX/0if;

    invoke-static {v2}, LX/B7W;->a(LX/0QB;)LX/B7W;

    move-result-object v2

    check-cast v2, LX/B7W;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->a(Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;LX/B6l;LX/0if;LX/B7W;)V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1751120
    iget-object v0, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->e:Landroid/view/View;

    new-instance v1, LX/B98;

    invoke-direct {v1, p0}, LX/B98;-><init>(Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1751121
    iget-object v0, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->c:LX/B7W;

    iget-object v1, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->p:LX/B7X;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1751122
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1751117
    iget-object v0, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1751118
    iget-object v0, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->c:LX/B7W;

    iget-object v1, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->p:LX/B7X;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1751119
    return-void
.end method

.method public final a(LX/B7F;LX/B7i;LX/B6u;Z)V
    .locals 3
    .param p3    # LX/B6u;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1751107
    iput-object p2, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->g:LX/B7i;

    .line 1751108
    iget-object v0, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->h:Landroid/widget/TextView;

    invoke-virtual {p1}, LX/B7F;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1751109
    iput-object p3, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->k:LX/B6u;

    .line 1751110
    iget-object v0, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p1}, LX/B7F;->h()Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1751111
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->n:Z

    .line 1751112
    iput-boolean p4, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->l:Z

    .line 1751113
    invoke-virtual {p1}, LX/B7F;->j()I

    move-result v0

    iput v0, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->m:I

    .line 1751114
    invoke-direct {p0}, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->b()V

    .line 1751115
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->a(Z)V

    .line 1751116
    return-void
.end method

.method public final a(Z)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    const/4 v3, 0x4

    const/4 v1, 0x0

    .line 1751086
    iput-boolean p1, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->o:Z

    .line 1751087
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v0, v2, :cond_0

    const/4 v0, 0x1

    .line 1751088
    :goto_0
    if-nez p1, :cond_3

    .line 1751089
    iget-boolean v2, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->n:Z

    if-eqz v2, :cond_1

    .line 1751090
    iget-object v2, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->e:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1751091
    :goto_1
    iget-object v1, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->h:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1751092
    iget-object v1, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1751093
    iget-object v1, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->j:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1751094
    if-eqz v0, :cond_2

    .line 1751095
    iget-object v0, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->i:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1751096
    :goto_2
    return-void

    :cond_0
    move v0, v1

    .line 1751097
    goto :goto_0

    .line 1751098
    :cond_1
    iget-object v1, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->e:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 1751099
    :cond_2
    iget-object v0, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->i:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    .line 1751100
    :cond_3
    iget-object v2, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->e:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1751101
    iget-object v2, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->h:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1751102
    iget-object v2, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1751103
    iget-object v2, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->j:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1751104
    if-eqz v0, :cond_4

    .line 1751105
    iget-object v0, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->i:Landroid/view/View;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    .line 1751106
    :cond_4
    iget-object v0, p0, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->i:Landroid/view/View;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2
.end method
