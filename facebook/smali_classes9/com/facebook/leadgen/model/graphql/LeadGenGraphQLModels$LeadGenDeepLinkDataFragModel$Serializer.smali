.class public final Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$LeadGenDeepLinkDataFragModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$LeadGenDeepLinkDataFragModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1749602
    const-class v0, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$LeadGenDeepLinkDataFragModel;

    new-instance v1, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$LeadGenDeepLinkDataFragModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$LeadGenDeepLinkDataFragModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1749603
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1749601
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$LeadGenDeepLinkDataFragModel;LX/0nX;LX/0my;)V
    .locals 9

    .prologue
    .line 1749364
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1749365
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v4, 0x0

    .line 1749366
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1749367
    invoke-virtual {v1, v0, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1749368
    if-eqz v2, :cond_0

    .line 1749369
    const-string v3, "agree_to_privacy_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749370
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749371
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2, v4}, LX/15i;->a(III)I

    move-result v2

    .line 1749372
    if-eqz v2, :cond_1

    .line 1749373
    const-string v3, "android_small_screen_phone_threshold"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749374
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1749375
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1749376
    if-eqz v2, :cond_2

    .line 1749377
    const-string v3, "country_code"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749378
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749379
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1749380
    if-eqz v2, :cond_3

    .line 1749381
    const-string v3, "disclaimer_accept_button_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749382
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749383
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1749384
    if-eqz v2, :cond_4

    .line 1749385
    const-string v3, "disclaimer_continue_button_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749386
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749387
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1749388
    if-eqz v2, :cond_5

    .line 1749389
    const-string v3, "error_codes"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749390
    invoke-static {v1, v2, p1, p2}, LX/B8Z;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1749391
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1749392
    if-eqz v2, :cond_6

    .line 1749393
    const-string v3, "error_message_brief"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749394
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749395
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1749396
    if-eqz v2, :cond_7

    .line 1749397
    const-string v3, "error_message_detail"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749398
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749399
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1749400
    if-eqz v2, :cond_8

    .line 1749401
    const-string v3, "fb_data_policy_setting_description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749402
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749403
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1749404
    if-eqz v2, :cond_9

    .line 1749405
    const-string v3, "fb_data_policy_url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749406
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749407
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1749408
    if-eqz v2, :cond_a

    .line 1749409
    const-string v3, "follow_up_action_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749410
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749411
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1749412
    if-eqz v2, :cond_b

    .line 1749413
    const-string v3, "follow_up_action_url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749414
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749415
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1749416
    if-eqz v2, :cond_c

    .line 1749417
    const-string v3, "landing_page_cta"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749418
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749419
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1749420
    if-eqz v2, :cond_d

    .line 1749421
    const-string v3, "landing_page_redirect_instruction"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749422
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749423
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1749424
    if-eqz v2, :cond_28

    .line 1749425
    const-string v3, "lead_gen_data"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749426
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1749427
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1749428
    if-eqz v3, :cond_15

    .line 1749429
    const-string v4, "context_page"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749430
    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 1749431
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1749432
    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 1749433
    if-eqz v4, :cond_e

    .line 1749434
    const-string v5, "context_card_photo"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749435
    invoke-static {v1, v4, p1, p2}, LX/B8X;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1749436
    :cond_e
    invoke-virtual {v1, v3, v6}, LX/15i;->g(II)I

    move-result v4

    .line 1749437
    if-eqz v4, :cond_f

    .line 1749438
    const-string v4, "context_content"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749439
    invoke-virtual {v1, v3, v6}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v4

    invoke-static {v4, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1749440
    :cond_f
    invoke-virtual {v1, v3, v7}, LX/15i;->g(II)I

    move-result v4

    .line 1749441
    if-eqz v4, :cond_10

    .line 1749442
    const-string v4, "context_content_style"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749443
    invoke-virtual {v1, v3, v7}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749444
    :cond_10
    const/4 v4, 0x3

    invoke-virtual {v1, v3, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1749445
    if-eqz v4, :cond_11

    .line 1749446
    const-string v5, "context_cta"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749447
    invoke-virtual {p1, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749448
    :cond_11
    const/4 v4, 0x4

    invoke-virtual {v1, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 1749449
    if-eqz v4, :cond_13

    .line 1749450
    const-string v5, "context_image"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749451
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1749452
    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v5

    .line 1749453
    if-eqz v5, :cond_12

    .line 1749454
    const-string v6, "uri"

    invoke-virtual {p1, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749455
    invoke-virtual {p1, v5}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749456
    :cond_12
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1749457
    :cond_13
    const/4 v4, 0x5

    invoke-virtual {v1, v3, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1749458
    if-eqz v4, :cond_14

    .line 1749459
    const-string v5, "context_title"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749460
    invoke-virtual {p1, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749461
    :cond_14
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1749462
    :cond_15
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1749463
    if-eqz v3, :cond_16

    .line 1749464
    const-string v4, "follow_up_title"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749465
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749466
    :cond_16
    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1749467
    if-eqz v3, :cond_17

    .line 1749468
    const-string v4, "info_fields_data"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749469
    invoke-static {v1, v3, p1, p2}, LX/B8g;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1749470
    :cond_17
    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1749471
    if-eqz v3, :cond_21

    .line 1749472
    const-string v4, "legal_content"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749473
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1749474
    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1749475
    if-eqz v4, :cond_18

    .line 1749476
    const-string v5, "advertiser_privacy_policy_name"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749477
    invoke-virtual {p1, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749478
    :cond_18
    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 1749479
    if-eqz v4, :cond_1a

    .line 1749480
    const-string v5, "checkboxes"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749481
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1749482
    const/4 v5, 0x0

    :goto_0
    invoke-virtual {v1, v4}, LX/15i;->c(I)I

    move-result v6

    if-ge v5, v6, :cond_19

    .line 1749483
    invoke-virtual {v1, v4, v5}, LX/15i;->q(II)I

    move-result v6

    invoke-static {v1, v6, p1, p2}, LX/B8h;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1749484
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 1749485
    :cond_19
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1749486
    :cond_1a
    const/4 v4, 0x2

    invoke-virtual {v1, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 1749487
    if-eqz v4, :cond_1f

    .line 1749488
    const-string v5, "disclaimer_body"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749489
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1749490
    const/4 v5, 0x0

    :goto_1
    invoke-virtual {v1, v4}, LX/15i;->c(I)I

    move-result v6

    if-ge v5, v6, :cond_1e

    .line 1749491
    invoke-virtual {v1, v4, v5}, LX/15i;->q(II)I

    move-result v6

    .line 1749492
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1749493
    const/4 v7, 0x0

    invoke-virtual {v1, v6, v7}, LX/15i;->g(II)I

    move-result v7

    .line 1749494
    if-eqz v7, :cond_1c

    .line 1749495
    const-string v8, "ranges"

    invoke-virtual {p1, v8}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749496
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1749497
    const/4 v8, 0x0

    :goto_2
    invoke-virtual {v1, v7}, LX/15i;->c(I)I

    move-result p0

    if-ge v8, p0, :cond_1b

    .line 1749498
    invoke-virtual {v1, v7, v8}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1, p2}, LX/B8i;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1749499
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 1749500
    :cond_1b
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1749501
    :cond_1c
    const/4 v7, 0x1

    invoke-virtual {v1, v6, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v7

    .line 1749502
    if-eqz v7, :cond_1d

    .line 1749503
    const-string v8, "text"

    invoke-virtual {p1, v8}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749504
    invoke-virtual {p1, v7}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749505
    :cond_1d
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1749506
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 1749507
    :cond_1e
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1749508
    :cond_1f
    const/4 v4, 0x3

    invoke-virtual {v1, v3, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1749509
    if-eqz v4, :cond_20

    .line 1749510
    const-string v5, "disclaimer_title"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749511
    invoke-virtual {p1, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749512
    :cond_20
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1749513
    :cond_21
    const/4 v3, 0x4

    invoke-virtual {v1, v2, v3}, LX/15i;->b(II)Z

    move-result v3

    .line 1749514
    if-eqz v3, :cond_22

    .line 1749515
    const-string v4, "need_split_flow"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749516
    invoke-virtual {p1, v3}, LX/0nX;->a(Z)V

    .line 1749517
    :cond_22
    const/4 v3, 0x5

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1749518
    if-eqz v3, :cond_24

    .line 1749519
    const-string v4, "pages"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749520
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1749521
    const/4 v4, 0x0

    :goto_3
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result v5

    if-ge v4, v5, :cond_23

    .line 1749522
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result v5

    invoke-static {v1, v5, p1, p2}, LX/B8a;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1749523
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 1749524
    :cond_23
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1749525
    :cond_24
    const/4 v3, 0x6

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1749526
    if-eqz v3, :cond_25

    .line 1749527
    const-string v4, "policy_url"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749528
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749529
    :cond_25
    const/4 v3, 0x7

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1749530
    if-eqz v3, :cond_26

    .line 1749531
    const-string v4, "split_flow_request_method"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749532
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749533
    :cond_26
    const/16 v3, 0x8

    invoke-virtual {v1, v2, v3}, LX/15i;->b(II)Z

    move-result v3

    .line 1749534
    if-eqz v3, :cond_27

    .line 1749535
    const-string v4, "split_flow_use_post"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749536
    invoke-virtual {p1, v3}, LX/0nX;->a(Z)V

    .line 1749537
    :cond_27
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1749538
    :cond_28
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1749539
    if-eqz v2, :cond_29

    .line 1749540
    const-string v3, "lead_gen_deep_link_user_status"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749541
    invoke-static {v1, v2, p1}, LX/B8c;->a(LX/15i;ILX/0nX;)V

    .line 1749542
    :cond_29
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1749543
    if-eqz v2, :cond_2a

    .line 1749544
    const-string v3, "page"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749545
    invoke-static {v1, v2, p1, p2}, LX/B8d;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1749546
    :cond_2a
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1749547
    if-eqz v2, :cond_2b

    .line 1749548
    const-string v3, "primary_button_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749549
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749550
    :cond_2b
    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1749551
    if-eqz v2, :cond_2c

    .line 1749552
    const-string v3, "privacy_setting_description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749553
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749554
    :cond_2c
    const/16 v2, 0x13

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1749555
    if-eqz v2, :cond_2d

    .line 1749556
    const-string v3, "progress_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749557
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749558
    :cond_2d
    const/16 v2, 0x14

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1749559
    if-eqz v2, :cond_2e

    .line 1749560
    const-string v3, "secure_sharing_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749561
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749562
    :cond_2e
    const/16 v2, 0x15

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1749563
    if-eqz v2, :cond_2f

    .line 1749564
    const-string v3, "select_text_hint"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749565
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749566
    :cond_2f
    const/16 v2, 0x16

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1749567
    if-eqz v2, :cond_30

    .line 1749568
    const-string v3, "send_description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749569
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749570
    :cond_30
    const/16 v2, 0x17

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1749571
    if-eqz v2, :cond_31

    .line 1749572
    const-string v3, "sent_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749573
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749574
    :cond_31
    const/16 v2, 0x18

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1749575
    if-eqz v2, :cond_32

    .line 1749576
    const-string v3, "short_secure_sharing_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749577
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749578
    :cond_32
    const/16 v2, 0x19

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1749579
    if-eqz v2, :cond_33

    .line 1749580
    const-string v3, "skip_experiments"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749581
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1749582
    :cond_33
    const/16 v2, 0x1a

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1749583
    if-eqz v2, :cond_34

    .line 1749584
    const-string v3, "split_flow_landing_page_hint_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749585
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749586
    :cond_34
    const/16 v2, 0x1b

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1749587
    if-eqz v2, :cond_35

    .line 1749588
    const-string v3, "split_flow_landing_page_hint_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749589
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749590
    :cond_35
    const/16 v2, 0x1c

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1749591
    if-eqz v2, :cond_36

    .line 1749592
    const-string v3, "submit_card_instruction_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749593
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749594
    :cond_36
    const/16 v2, 0x1d

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1749595
    if-eqz v2, :cond_37

    .line 1749596
    const-string v3, "unsubscribe_description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749597
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749598
    :cond_37
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1749599
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1749600
    check-cast p1, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$LeadGenDeepLinkDataFragModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$LeadGenDeepLinkDataFragModel$Serializer;->a(Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$LeadGenDeepLinkDataFragModel;LX/0nX;LX/0my;)V

    return-void
.end method
