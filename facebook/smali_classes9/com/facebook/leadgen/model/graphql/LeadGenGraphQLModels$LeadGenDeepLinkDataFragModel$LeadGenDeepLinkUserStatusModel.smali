.class public final Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$LeadGenDeepLinkDataFragModel$LeadGenDeepLinkUserStatusModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3da29ec0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$LeadGenDeepLinkDataFragModel$LeadGenDeepLinkUserStatusModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$LeadGenDeepLinkDataFragModel$LeadGenDeepLinkUserStatusModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1749312
    const-class v0, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$LeadGenDeepLinkDataFragModel$LeadGenDeepLinkUserStatusModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1749311
    const-class v0, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$LeadGenDeepLinkDataFragModel$LeadGenDeepLinkUserStatusModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1749309
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1749310
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1749303
    iput-boolean p1, p0, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$LeadGenDeepLinkDataFragModel$LeadGenDeepLinkUserStatusModel;->e:Z

    .line 1749304
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1749305
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1749306
    if-eqz v0, :cond_0

    .line 1749307
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1749308
    :cond_0
    return-void
.end method

.method private j()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1749301
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1749302
    iget-boolean v0, p0, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$LeadGenDeepLinkDataFragModel$LeadGenDeepLinkUserStatusModel;->e:Z

    return v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1749299
    iget-object v0, p0, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$LeadGenDeepLinkDataFragModel$LeadGenDeepLinkUserStatusModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$LeadGenDeepLinkDataFragModel$LeadGenDeepLinkUserStatusModel;->f:Ljava/lang/String;

    .line 1749300
    iget-object v0, p0, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$LeadGenDeepLinkDataFragModel$LeadGenDeepLinkUserStatusModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1749297
    iget-object v0, p0, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$LeadGenDeepLinkDataFragModel$LeadGenDeepLinkUserStatusModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$LeadGenDeepLinkDataFragModel$LeadGenDeepLinkUserStatusModel;->g:Ljava/lang/String;

    .line 1749298
    iget-object v0, p0, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$LeadGenDeepLinkDataFragModel$LeadGenDeepLinkUserStatusModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1749288
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1749289
    invoke-direct {p0}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$LeadGenDeepLinkDataFragModel$LeadGenDeepLinkUserStatusModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1749290
    invoke-direct {p0}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$LeadGenDeepLinkDataFragModel$LeadGenDeepLinkUserStatusModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1749291
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1749292
    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$LeadGenDeepLinkDataFragModel$LeadGenDeepLinkUserStatusModel;->e:Z

    invoke-virtual {p1, v2, v3}, LX/186;->a(IZ)V

    .line 1749293
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1749294
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1749295
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1749296
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1749266
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1749267
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1749268
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1749287
    new-instance v0, LX/B8U;

    invoke-direct {v0, p1}, LX/B8U;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1749286
    invoke-direct {p0}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$LeadGenDeepLinkDataFragModel$LeadGenDeepLinkUserStatusModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1749283
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1749284
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$LeadGenDeepLinkDataFragModel$LeadGenDeepLinkUserStatusModel;->e:Z

    .line 1749285
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1749277
    const-string v0, "has_shared_info"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1749278
    invoke-direct {p0}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$LeadGenDeepLinkDataFragModel$LeadGenDeepLinkUserStatusModel;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1749279
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1749280
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 1749281
    :goto_0
    return-void

    .line 1749282
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1749274
    const-string v0, "has_shared_info"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1749275
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$LeadGenDeepLinkDataFragModel$LeadGenDeepLinkUserStatusModel;->a(Z)V

    .line 1749276
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1749271
    new-instance v0, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$LeadGenDeepLinkDataFragModel$LeadGenDeepLinkUserStatusModel;

    invoke-direct {v0}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$LeadGenDeepLinkDataFragModel$LeadGenDeepLinkUserStatusModel;-><init>()V

    .line 1749272
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1749273
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1749270
    const v0, -0x160ee4e6

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1749269
    const v0, 0x2df91497

    return v0
.end method
