.class public final Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1749175
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1749176
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1749173
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1749174
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 12

    .prologue
    .line 1748990
    if-nez p1, :cond_0

    .line 1748991
    const/4 v0, 0x0

    .line 1748992
    :goto_0
    return v0

    .line 1748993
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1748994
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1748995
    :sswitch_0
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$LeadGenContextCardFieldsModel$ContextCardPhotoModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$LeadGenContextCardFieldsModel$ContextCardPhotoModel;

    .line 1748996
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1748997
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v1}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v1

    .line 1748998
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v1

    .line 1748999
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;

    move-result-object v2

    .line 1749000
    invoke-virtual {p3, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1749001
    const/4 v3, 0x3

    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1749002
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1749003
    const/4 v4, 0x4

    invoke-virtual {p0, p1, v4}, LX/15i;->p(II)I

    move-result v4

    .line 1749004
    const v5, -0x16c6d181

    invoke-static {p0, v4, v5, p3}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v4

    .line 1749005
    const/4 v5, 0x5

    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v5

    .line 1749006
    invoke-virtual {p3, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1749007
    const/4 v6, 0x6

    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1749008
    const/4 v6, 0x0

    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 1749009
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1749010
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 1749011
    const/4 v0, 0x3

    invoke-virtual {p3, v0, v3}, LX/186;->b(II)V

    .line 1749012
    const/4 v0, 0x4

    invoke-virtual {p3, v0, v4}, LX/186;->b(II)V

    .line 1749013
    const/4 v0, 0x5

    invoke-virtual {p3, v0, v5}, LX/186;->b(II)V

    .line 1749014
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1749015
    :sswitch_1
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1749016
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1749017
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1749018
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1749019
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1749020
    :sswitch_2
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1749021
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1749022
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    move-result-object v1

    .line 1749023
    invoke-virtual {p3, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1749024
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1749025
    const/4 v2, 0x0

    invoke-virtual {p3, v2, v0}, LX/186;->b(II)V

    .line 1749026
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1749027
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1749028
    :sswitch_3
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1749029
    const v1, 0x427494a6

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1749030
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1749031
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1749032
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v2

    .line 1749033
    const v3, 0x35372279

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v2

    .line 1749034
    const/4 v3, 0x3

    invoke-virtual {p0, p1, v3}, LX/15i;->p(II)I

    move-result v3

    .line 1749035
    const v4, 0x70809421

    invoke-static {p0, v3, v4, p3}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    .line 1749036
    const/4 v4, 0x4

    invoke-virtual {p0, p1, v4}, LX/15i;->b(II)Z

    move-result v4

    .line 1749037
    const/4 v5, 0x5

    invoke-virtual {p0, p1, v5}, LX/15i;->p(II)I

    move-result v5

    .line 1749038
    const v6, 0x41513a61

    invoke-static {p0, v5, v6, p3}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v5

    .line 1749039
    const/4 v6, 0x6

    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v6

    .line 1749040
    invoke-virtual {p3, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1749041
    const/4 v7, 0x7

    invoke-virtual {p0, p1, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v7

    .line 1749042
    invoke-virtual {p3, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1749043
    const/16 v8, 0x8

    invoke-virtual {p0, p1, v8}, LX/15i;->b(II)Z

    move-result v8

    .line 1749044
    const/16 v9, 0x9

    invoke-virtual {p3, v9}, LX/186;->c(I)V

    .line 1749045
    const/4 v9, 0x0

    invoke-virtual {p3, v9, v0}, LX/186;->b(II)V

    .line 1749046
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1749047
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 1749048
    const/4 v0, 0x3

    invoke-virtual {p3, v0, v3}, LX/186;->b(II)V

    .line 1749049
    const/4 v0, 0x4

    invoke-virtual {p3, v0, v4}, LX/186;->a(IZ)V

    .line 1749050
    const/4 v0, 0x5

    invoke-virtual {p3, v0, v5}, LX/186;->b(II)V

    .line 1749051
    const/4 v0, 0x6

    invoke-virtual {p3, v0, v6}, LX/186;->b(II)V

    .line 1749052
    const/4 v0, 0x7

    invoke-virtual {p3, v0, v7}, LX/186;->b(II)V

    .line 1749053
    const/16 v0, 0x8

    invoke-virtual {p3, v0, v8}, LX/186;->a(IZ)V

    .line 1749054
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1749055
    :sswitch_4
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1749056
    const v1, 0x35372279

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 1749057
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v1

    .line 1749058
    const v2, 0x1748b4d7

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v1

    .line 1749059
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1749060
    const/4 v2, 0x0

    invoke-virtual {p3, v2, v0}, LX/186;->b(II)V

    .line 1749061
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1749062
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1749063
    :sswitch_5
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1749064
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1749065
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    move-result-object v1

    .line 1749066
    invoke-virtual {p3, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1749067
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1749068
    const/4 v2, 0x0

    invoke-virtual {p3, v2, v0}, LX/186;->b(II)V

    .line 1749069
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1749070
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1749071
    :sswitch_6
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1749072
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/util/List;)I

    move-result v0

    .line 1749073
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1749074
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1749075
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    move-result-object v2

    .line 1749076
    invoke-virtual {p3, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1749077
    const/4 v3, 0x3

    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    move-result-object v3

    .line 1749078
    invoke-virtual {p3, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1749079
    const/4 v4, 0x4

    invoke-virtual {p0, p1, v4}, LX/15i;->b(II)Z

    move-result v4

    .line 1749080
    const/4 v5, 0x5

    invoke-virtual {p0, p1, v5}, LX/15i;->b(II)Z

    move-result v5

    .line 1749081
    const/4 v6, 0x6

    invoke-virtual {p0, p1, v6}, LX/15i;->b(II)Z

    move-result v6

    .line 1749082
    const/4 v7, 0x7

    invoke-virtual {p0, p1, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v7

    .line 1749083
    invoke-virtual {p3, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1749084
    const/16 v8, 0x8

    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v8

    .line 1749085
    invoke-virtual {p3, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1749086
    const/16 v9, 0x9

    invoke-virtual {p0, p1, v9}, LX/15i;->p(II)I

    move-result v9

    .line 1749087
    const v10, -0x3cb87f45

    invoke-static {p0, v9, v10, p3}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1749088
    const/16 v10, 0xa

    invoke-virtual {p0, p1, v10}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v10

    invoke-static {v10}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v10

    .line 1749089
    invoke-virtual {p3, v10}, LX/186;->b(Ljava/util/List;)I

    move-result v10

    .line 1749090
    const/16 v11, 0xb

    invoke-virtual {p3, v11}, LX/186;->c(I)V

    .line 1749091
    const/4 v11, 0x0

    invoke-virtual {p3, v11, v0}, LX/186;->b(II)V

    .line 1749092
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1749093
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 1749094
    const/4 v0, 0x3

    invoke-virtual {p3, v0, v3}, LX/186;->b(II)V

    .line 1749095
    const/4 v0, 0x4

    invoke-virtual {p3, v0, v4}, LX/186;->a(IZ)V

    .line 1749096
    const/4 v0, 0x5

    invoke-virtual {p3, v0, v5}, LX/186;->a(IZ)V

    .line 1749097
    const/4 v0, 0x6

    invoke-virtual {p3, v0, v6}, LX/186;->a(IZ)V

    .line 1749098
    const/4 v0, 0x7

    invoke-virtual {p3, v0, v7}, LX/186;->b(II)V

    .line 1749099
    const/16 v0, 0x8

    invoke-virtual {p3, v0, v8}, LX/186;->b(II)V

    .line 1749100
    const/16 v0, 0x9

    invoke-virtual {p3, v0, v9}, LX/186;->b(II)V

    .line 1749101
    const/16 v0, 0xa

    invoke-virtual {p3, v0, v10}, LX/186;->b(II)V

    .line 1749102
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1749103
    :sswitch_7
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1749104
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1749105
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v1}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v1

    .line 1749106
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v1

    .line 1749107
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1749108
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1749109
    const/4 v3, 0x3

    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationCondition;

    move-result-object v3

    .line 1749110
    invoke-virtual {p3, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1749111
    const/4 v4, 0x4

    invoke-virtual {p0, p1, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    move-result-object v4

    .line 1749112
    invoke-virtual {p3, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    .line 1749113
    const/4 v5, 0x5

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1749114
    const/4 v5, 0x0

    invoke-virtual {p3, v5, v0}, LX/186;->b(II)V

    .line 1749115
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1749116
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 1749117
    const/4 v0, 0x3

    invoke-virtual {p3, v0, v3}, LX/186;->b(II)V

    .line 1749118
    const/4 v0, 0x4

    invoke-virtual {p3, v0, v4}, LX/186;->b(II)V

    .line 1749119
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1749120
    :sswitch_8
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1749121
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1749122
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v1

    .line 1749123
    const v2, -0x566e6c3

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v1

    .line 1749124
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v2

    .line 1749125
    const v3, -0x3d56ad1c

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v2

    .line 1749126
    const/4 v3, 0x3

    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1749127
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1749128
    const/4 v4, 0x4

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1749129
    const/4 v4, 0x0

    invoke-virtual {p3, v4, v0}, LX/186;->b(II)V

    .line 1749130
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1749131
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 1749132
    const/4 v0, 0x3

    invoke-virtual {p3, v0, v3}, LX/186;->b(II)V

    .line 1749133
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1749134
    :sswitch_9
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1749135
    const v1, -0x92cb730

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1749136
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 1749137
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1749138
    const/4 v3, 0x3

    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1749139
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1749140
    const/4 v4, 0x4

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1749141
    const/4 v4, 0x0

    invoke-virtual {p3, v4, v0}, LX/186;->b(II)V

    .line 1749142
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->a(IZ)V

    .line 1749143
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->a(IZ)V

    .line 1749144
    const/4 v0, 0x3

    invoke-virtual {p3, v0, v3}, LX/186;->b(II)V

    .line 1749145
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1749146
    :sswitch_a
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1749147
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1749148
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1749149
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1749150
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1749151
    :sswitch_b
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1749152
    const v1, 0x404f27f7

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 1749153
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1749154
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1749155
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1749156
    const/4 v2, 0x0

    invoke-virtual {p3, v2, v0}, LX/186;->b(II)V

    .line 1749157
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1749158
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1749159
    :sswitch_c
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1749160
    const v1, -0x798d9439

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1749161
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v1, v2}, LX/15i;->a(III)I

    move-result v1

    .line 1749162
    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {p0, p1, v2, v3}, LX/15i;->a(III)I

    move-result v2

    .line 1749163
    const/4 v3, 0x3

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1749164
    const/4 v3, 0x0

    invoke-virtual {p3, v3, v0}, LX/186;->b(II)V

    .line 1749165
    const/4 v0, 0x1

    const/4 v3, 0x0

    invoke-virtual {p3, v0, v1, v3}, LX/186;->a(III)V

    .line 1749166
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v2, v1}, LX/186;->a(III)V

    .line 1749167
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1749168
    :sswitch_d
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1749169
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1749170
    const/4 v1, 0x2

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1749171
    const/4 v1, 0x1

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1749172
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x798d9439 -> :sswitch_d
        -0x483247c6 -> :sswitch_2
        -0x3d56ad1c -> :sswitch_b
        -0x3cb87f45 -> :sswitch_7
        -0x16c6d181 -> :sswitch_1
        -0x92cb730 -> :sswitch_a
        -0x566e6c3 -> :sswitch_9
        0x4dea405 -> :sswitch_3
        0x1748b4d7 -> :sswitch_5
        0x35372279 -> :sswitch_6
        0x404f27f7 -> :sswitch_c
        0x41513a61 -> :sswitch_4
        0x427494a6 -> :sswitch_0
        0x70809421 -> :sswitch_8
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1748981
    if-nez p0, :cond_0

    move v0, v1

    .line 1748982
    :goto_0
    return v0

    .line 1748983
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 1748984
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1748985
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1748986
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 1748987
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1748988
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1748989
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 1748974
    const/4 v7, 0x0

    .line 1748975
    const/4 v1, 0x0

    .line 1748976
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 1748977
    invoke-static {v2, v3, v0}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1748978
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 1748979
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1748980
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1748973
    new-instance v0, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1748968
    if-eqz p0, :cond_0

    .line 1748969
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1748970
    if-eq v0, p0, :cond_0

    .line 1748971
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1748972
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 5

    .prologue
    const v4, 0x35372279

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1748937
    sparse-switch p2, :sswitch_data_0

    .line 1748938
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1748939
    :sswitch_0
    const-class v0, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$LeadGenContextCardFieldsModel$ContextCardPhotoModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$LeadGenContextCardFieldsModel$ContextCardPhotoModel;

    .line 1748940
    invoke-static {v0, p3}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 1748941
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1748942
    const v1, -0x16c6d181

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1748943
    :goto_0
    :sswitch_1
    return-void

    .line 1748944
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1748945
    const v1, 0x427494a6

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1748946
    invoke-virtual {p0, p1, v3}, LX/15i;->p(II)I

    move-result v0

    .line 1748947
    invoke-static {p0, v0, v4, p3}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1748948
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1748949
    const v1, 0x70809421

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1748950
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1748951
    const v1, 0x41513a61

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1748952
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1748953
    invoke-static {p0, v0, v4, p3}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1748954
    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v0

    .line 1748955
    const v1, 0x1748b4d7

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1748956
    :sswitch_4
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1748957
    const v1, -0x3cb87f45

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1748958
    :sswitch_5
    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v0

    .line 1748959
    const v1, -0x566e6c3

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1748960
    invoke-virtual {p0, p1, v3}, LX/15i;->p(II)I

    move-result v0

    .line 1748961
    const v1, -0x3d56ad1c

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1748962
    :sswitch_6
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1748963
    const v1, -0x92cb730

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1748964
    :sswitch_7
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1748965
    const v1, 0x404f27f7

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1748966
    :sswitch_8
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1748967
    const v1, -0x798d9439

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x798d9439 -> :sswitch_1
        -0x483247c6 -> :sswitch_1
        -0x3d56ad1c -> :sswitch_7
        -0x3cb87f45 -> :sswitch_1
        -0x16c6d181 -> :sswitch_1
        -0x92cb730 -> :sswitch_1
        -0x566e6c3 -> :sswitch_6
        0x4dea405 -> :sswitch_2
        0x1748b4d7 -> :sswitch_1
        0x35372279 -> :sswitch_4
        0x404f27f7 -> :sswitch_8
        0x41513a61 -> :sswitch_3
        0x427494a6 -> :sswitch_0
        0x70809421 -> :sswitch_5
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1748882
    if-nez p1, :cond_0

    move v0, v1

    .line 1748883
    :goto_0
    return v0

    .line 1748884
    :cond_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v2

    .line 1748885
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1748886
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1748887
    invoke-virtual {p0, p1, v1}, LX/15i;->q(II)I

    move-result v3

    .line 1748888
    invoke-static {p0, v3, p2, p3}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    aput v3, v0, v1

    .line 1748889
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1748890
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1748891
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 1748930
    if-eqz p1, :cond_0

    .line 1748931
    invoke-virtual {p0, p1}, LX/15i;->d(I)I

    move-result v1

    .line 1748932
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1748933
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v2

    .line 1748934
    invoke-static {p0, v2, p2, p3}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1748935
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1748936
    :cond_0
    return-void
.end method

.method private static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1748924
    if-eqz p1, :cond_0

    .line 1748925
    invoke-static {p0, p1, p2}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 1748926
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;

    .line 1748927
    if-eq v0, v1, :cond_0

    .line 1748928
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1748929
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1748923
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1748880
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1748881
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1748892
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1748893
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1748894
    :cond_0
    iput-object p1, p0, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1748895
    iput p2, p0, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->b:I

    .line 1748896
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1748897
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1748898
    new-instance v0, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1748899
    iget v0, p0, LX/1vt;->c:I

    .line 1748900
    move v0, v0

    .line 1748901
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1748902
    iget v0, p0, LX/1vt;->c:I

    .line 1748903
    move v0, v0

    .line 1748904
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1748905
    iget v0, p0, LX/1vt;->b:I

    .line 1748906
    move v0, v0

    .line 1748907
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1748908
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1748909
    move-object v0, v0

    .line 1748910
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1748911
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1748912
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1748913
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1748914
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1748915
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1748916
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1748917
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1748918
    invoke-static {v3, v9, v2}, Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/leadgen/model/graphql/LeadGenGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1748919
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1748920
    iget v0, p0, LX/1vt;->c:I

    .line 1748921
    move v0, v0

    .line 1748922
    return v0
.end method
