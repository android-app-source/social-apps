.class public Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1747745
    new-instance v0, LX/B7A;

    invoke-direct {v0}, LX/B7A;-><init>()V

    sput-object v0, Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1747746
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1747747
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;->a:Ljava/lang/String;

    .line 1747748
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;->b:I

    .line 1747749
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;->c:Ljava/util/Map;

    .line 1747750
    iget-object v0, p0, Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;->c:Ljava/util/Map;

    const-class v1, Ljava/util/Map;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readMap(Ljava/util/Map;Ljava/lang/ClassLoader;)V

    .line 1747751
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1747752
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1747753
    iput-object p1, p0, Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;->a:Ljava/lang/String;

    .line 1747754
    iput p2, p0, Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;->b:I

    .line 1747755
    iput-object p3, p0, Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;->c:Ljava/util/Map;

    .line 1747756
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1747757
    iget-object v0, p0, Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1747758
    const/4 v0, 0x0

    .line 1747759
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1747760
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1747761
    iget-object v0, p0, Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1747762
    iget v0, p0, Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1747763
    iget-object v0, p0, Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;->c:Ljava/util/Map;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 1747764
    return-void
.end method
