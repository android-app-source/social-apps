.class public Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/B6l;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/B7W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field public h:LX/B6u;

.field public i:LX/B7i;

.field public j:LX/B6S;

.field public k:LX/B7F;

.field private final l:LX/B6V;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1746915
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1746916
    new-instance v0, LX/B6W;

    invoke-direct {v0, p0}, LX/B6W;-><init>(Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;)V

    iput-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->l:LX/B6V;

    .line 1746917
    const v0, 0x7f0309cd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1746918
    const-class v0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    invoke-static {v0, p0}, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1746919
    return-void
.end method

.method private static a(Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;LX/B6l;LX/B7W;LX/0if;)V
    .locals 0

    .prologue
    .line 1746920
    iput-object p1, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->a:LX/B6l;

    iput-object p2, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->b:LX/B7W;

    iput-object p3, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->c:LX/0if;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    invoke-static {v2}, LX/B6l;->a(LX/0QB;)LX/B6l;

    move-result-object v0

    check-cast v0, LX/B6l;

    invoke-static {v2}, LX/B7W;->a(LX/0QB;)LX/B7W;

    move-result-object v1

    check-cast v1, LX/B7W;

    invoke-static {v2}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v2

    check-cast v2, LX/0if;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->a(Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;LX/B6l;LX/B7W;LX/0if;)V

    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1746900
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAllCaps(Z)V

    .line 1746901
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAllCaps(Z)V

    .line 1746902
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAllCaps(Z)V

    .line 1746903
    invoke-direct {p0}, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->d()V

    .line 1746904
    return-void
.end method

.method private d()V
    .locals 6

    .prologue
    const/16 v5, 0x11

    const/4 v4, -0x1

    .line 1746905
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b198b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-direct {v0, v1, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1746906
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b198b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    invoke-direct {v1, v2, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1746907
    invoke-virtual {p0}, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b19a7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 1746908
    invoke-virtual {p0}, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b19a8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 1746909
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setGravity(I)V

    .line 1746910
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->e:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setGravity(I)V

    .line 1746911
    const/4 v2, 0x5

    invoke-virtual {p0, v2}, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->setGravity(I)V

    .line 1746912
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1746913
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1746914
    return-void
.end method

.method private e()V
    .locals 4

    .prologue
    const/16 v1, 0x8

    .line 1746894
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->e:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 1746895
    :cond_0
    :goto_0
    return-void

    .line 1746896
    :cond_1
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 1746897
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, 0x0

    const/4 v2, -0x1

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 1746898
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->e:Landroid/widget/TextView;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 1746899
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1746888
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1746889
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1746890
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1746891
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1746892
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->b:LX/B7W;

    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->l:LX/B6V;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1746893
    return-void
.end method

.method public final a(LX/B6S;)V
    .locals 2

    .prologue
    .line 1746883
    iput-object p1, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->j:LX/B6S;

    .line 1746884
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->d:Landroid/widget/TextView;

    new-instance v1, LX/B6X;

    invoke-direct {v1, p0}, LX/B6X;-><init>(Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1746885
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->e:Landroid/widget/TextView;

    new-instance v1, LX/B6Y;

    invoke-direct {v1, p0, p1}, LX/B6Y;-><init>(Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;LX/B6S;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1746886
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->g:Landroid/widget/TextView;

    new-instance v1, LX/B6Z;

    invoke-direct {v1, p0}, LX/B6Z;-><init>(Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1746887
    return-void
.end method

.method public final a(LX/B7F;LX/B6u;LX/B7i;)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 1746869
    iput-object p2, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->h:LX/B6u;

    .line 1746870
    iput-object p3, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->i:LX/B7i;

    .line 1746871
    iput-object p1, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->k:LX/B7F;

    .line 1746872
    const v0, 0x7f0d18ef

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->d:Landroid/widget/TextView;

    .line 1746873
    const v0, 0x7f0d18f0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->e:Landroid/widget/TextView;

    .line 1746874
    const v0, 0x7f0d18c3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->f:Landroid/widget/TextView;

    .line 1746875
    const v0, 0x7f0d18f1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->g:Landroid/widget/TextView;

    .line 1746876
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1746877
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1746878
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1746879
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1746880
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->b:LX/B7W;

    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->l:LX/B6V;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1746881
    invoke-direct {p0}, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->c()V

    .line 1746882
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 1746866
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->f:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 1746867
    :goto_0
    return-void

    .line 1746868
    :cond_0
    invoke-virtual {p0, p1}, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->setupCustomDisclaimerPageAcceptButton(Z)V

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 1746839
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->h:LX/B6u;

    invoke-virtual {v0}, LX/B6u;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1746840
    invoke-virtual {p0, v3}, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->setupCustomDisclaimerPageAcceptButton(Z)V

    .line 1746841
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1746842
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1746843
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1746844
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1746845
    :goto_0
    return-void

    .line 1746846
    :cond_0
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->h:LX/B6u;

    invoke-virtual {v0}, LX/B6u;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1746847
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->g:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->k:LX/B7F;

    invoke-virtual {v1}, LX/B7F;->q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1746848
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1746849
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1746850
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1746851
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 1746852
    :cond_1
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1746853
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1746854
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1746855
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1746856
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->h:LX/B6u;

    invoke-virtual {v0}, LX/B6u;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->k:LX/B7F;

    .line 1746857
    iget-boolean v1, v0, LX/B7F;->f:Z

    move v0, v1

    .line 1746858
    if-nez v0, :cond_2

    .line 1746859
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1746860
    :goto_1
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->h:LX/B6u;

    invoke-virtual {v0}, LX/B6u;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1746861
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->k:LX/B7F;

    invoke-virtual {v1}, LX/B7F;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1746862
    invoke-direct {p0}, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->e()V

    goto :goto_0

    .line 1746863
    :cond_2
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0824e2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1746864
    :cond_3
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->e:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0824e3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1746865
    invoke-direct {p0}, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->d()V

    goto/16 :goto_0
.end method

.method public setupCustomDisclaimerPageAcceptButton(Z)V
    .locals 3

    .prologue
    .line 1746832
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->f:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0824ea

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1746833
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1746834
    if-eqz p1, :cond_0

    .line 1746835
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->f:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1746836
    :goto_0
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->f:Landroid/widget/TextView;

    new-instance v1, LX/B6a;

    invoke-direct {v1, p0}, LX/B6a;-><init>(Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1746837
    return-void

    .line 1746838
    :cond_0
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->f:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method
