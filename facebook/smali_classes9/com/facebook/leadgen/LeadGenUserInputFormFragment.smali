.class public Lcom/facebook/leadgen/LeadGenUserInputFormFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/B6H;


# instance fields
.field public A:LX/B6M;

.field public B:LX/B6o;

.field public C:Landroid/view/ViewTreeObserver;

.field public D:LX/D82;

.field public final E:LX/B6z;

.field public a:LX/2sb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/B6v;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/B8q;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0So;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/B6S;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/B79;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/B6l;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/B8t;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/B7G;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/B7W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/B6p;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:Landroid/view/View;

.field public o:LX/B7i;

.field public p:Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;

.field public q:Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;

.field public r:Landroid/support/v4/view/ViewPager;

.field public s:LX/B6u;

.field private t:J

.field public u:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

.field public v:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

.field public w:LX/B7F;

.field public x:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

.field public y:Landroid/os/Bundle;

.field public z:LX/B8p;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1747693
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1747694
    new-instance v0, LX/B70;

    invoke-direct {v0, p0}, LX/B70;-><init>(Lcom/facebook/leadgen/LeadGenUserInputFormFragment;)V

    iput-object v0, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->E:LX/B6z;

    return-void
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/leadgen/LeadGenUserInputFormFragment;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "Lcom/facebook/leadgen/LeadGenUserInputFormFragment;"
        }
    .end annotation

    .prologue
    .line 1747688
    new-instance v0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;

    invoke-direct {v0}, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;-><init>()V

    .line 1747689
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1747690
    const-string v2, "story_attachment"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1747691
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1747692
    return-object v0
.end method

.method private static a(Lcom/facebook/leadgen/LeadGenUserInputFormFragment;LX/2sb;LX/B6v;LX/B8q;LX/0So;LX/B6S;LX/B79;LX/B6l;LX/0Or;LX/B8t;LX/0if;LX/B7G;LX/B7W;LX/B6p;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/leadgen/LeadGenUserInputFormFragment;",
            "LX/2sb;",
            "LX/B6v;",
            "LX/B8q;",
            "LX/0So;",
            "LX/B6S;",
            "LX/B79;",
            "LX/B6l;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;",
            "LX/B8t;",
            "Lcom/facebook/funnellogger/FunnelLogger;",
            "LX/B7G;",
            "LX/B7W;",
            "LX/B6p;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1747687
    iput-object p1, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->a:LX/2sb;

    iput-object p2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->b:LX/B6v;

    iput-object p3, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->c:LX/B8q;

    iput-object p4, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->d:LX/0So;

    iput-object p5, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->e:LX/B6S;

    iput-object p6, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->f:LX/B79;

    iput-object p7, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->g:LX/B6l;

    iput-object p8, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->h:LX/0Or;

    iput-object p9, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->i:LX/B8t;

    iput-object p10, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->j:LX/0if;

    iput-object p11, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->k:LX/B7G;

    iput-object p12, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->l:LX/B7W;

    iput-object p13, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->m:LX/B6p;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 15

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v13

    move-object v0, p0

    check-cast v0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;

    invoke-static {v13}, LX/2sb;->a(LX/0QB;)LX/2sb;

    move-result-object v1

    check-cast v1, LX/2sb;

    const-class v2, LX/B6v;

    invoke-interface {v13, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/B6v;

    const-class v3, LX/B8q;

    invoke-interface {v13, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/B8q;

    invoke-static {v13}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-static {v13}, LX/B6S;->b(LX/0QB;)LX/B6S;

    move-result-object v5

    check-cast v5, LX/B6S;

    invoke-static {v13}, LX/B79;->a(LX/0QB;)LX/B79;

    move-result-object v6

    check-cast v6, LX/B79;

    invoke-static {v13}, LX/B6l;->a(LX/0QB;)LX/B6l;

    move-result-object v7

    check-cast v7, LX/B6l;

    const/16 v8, 0x122d

    invoke-static {v13, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {v13}, LX/B8t;->a(LX/0QB;)LX/B8t;

    move-result-object v9

    check-cast v9, LX/B8t;

    invoke-static {v13}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v10

    check-cast v10, LX/0if;

    const-class v11, LX/B7G;

    invoke-interface {v13, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/B7G;

    invoke-static {v13}, LX/B7W;->a(LX/0QB;)LX/B7W;

    move-result-object v12

    check-cast v12, LX/B7W;

    const-class v14, LX/B6p;

    invoke-interface {v13, v14}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/B6p;

    invoke-static/range {v0 .. v13}, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->a(Lcom/facebook/leadgen/LeadGenUserInputFormFragment;LX/2sb;LX/B6v;LX/B8q;LX/0So;LX/B6S;LX/B79;LX/B6l;LX/0Or;LX/B8t;LX/0if;LX/B7G;LX/B7W;LX/B6p;)V

    return-void
.end method

.method private b(Landroid/os/Bundle;)Z
    .locals 4

    .prologue
    .line 1747675
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->w:LX/B7F;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->s:LX/B6u;

    if-nez v0, :cond_1

    .line 1747676
    :cond_0
    const/4 v0, 0x0

    .line 1747677
    :goto_0
    return v0

    .line 1747678
    :cond_1
    new-instance v0, Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;

    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->w:LX/B7F;

    invoke-virtual {v1}, LX/B7F;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->s:LX/B6u;

    .line 1747679
    iget v3, v2, LX/B6u;->h:I

    move v2, v3

    .line 1747680
    iget-object v3, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->s:LX/B6u;

    invoke-virtual {v3}, LX/B6u;->e()LX/0P1;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;-><init>(Ljava/lang/String;ILjava/util/Map;)V

    .line 1747681
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->f:LX/B79;

    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->w:LX/B7F;

    invoke-virtual {v2}, LX/B7F;->b()Ljava/lang/String;

    move-result-object v2

    .line 1747682
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1747683
    :goto_1
    if-eqz p1, :cond_2

    .line 1747684
    const-string v1, "LEADGEN_FORM_PENDING_INPUT"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1747685
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 1747686
    :cond_3
    iget-object v3, v1, LX/B79;->a:LX/0aq;

    invoke-virtual {v3, v2, v0}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public static e(Lcom/facebook/leadgen/LeadGenUserInputFormFragment;)Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;
    .locals 2

    .prologue
    .line 1747670
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->f:LX/B79;

    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->w:LX/B7F;

    invoke-virtual {v1}, LX/B7F;->b()Ljava/lang/String;

    move-result-object v1

    .line 1747671
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1747672
    const/4 p0, 0x0

    .line 1747673
    :goto_0
    move-object v0, p0

    .line 1747674
    return-object v0

    :cond_0
    iget-object p0, v0, LX/B79;->a:LX/0aq;

    invoke-virtual {p0, v1}, LX/0aq;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1747665
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1747666
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mParentFragment:Landroid/support/v4/app/Fragment;

    move-object v0, v0

    .line 1747667
    check-cast v0, LX/B7i;

    iput-object v0, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->o:LX/B7i;

    .line 1747668
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1747669
    return-void
.end method

.method public final b()Landroid/widget/ScrollView;
    .locals 2

    .prologue
    .line 1747656
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->s:LX/B6u;

    .line 1747657
    iget-object v1, v0, LX/B6u;->g:LX/B6O;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/B6u;->g:LX/B6O;

    .line 1747658
    iget-object p0, v1, LX/B6O;->e:LX/B6N;

    move-object v1, p0

    .line 1747659
    if-nez v1, :cond_1

    .line 1747660
    :cond_0
    const/4 v1, 0x0

    .line 1747661
    :goto_0
    move-object v0, v1

    .line 1747662
    return-object v0

    :cond_1
    iget-object v1, v0, LX/B6u;->g:LX/B6O;

    .line 1747663
    iget-object p0, v1, LX/B6O;->e:LX/B6N;

    move-object v1, p0

    .line 1747664
    invoke-interface {v1}, LX/B6N;->getContentScrollView()Lcom/facebook/widget/FbScrollView;

    move-result-object v1

    goto :goto_0
.end method

.method public final c()Z
    .locals 4

    .prologue
    .line 1747695
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->x:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    invoke-virtual {v0}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1747696
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->j:LX/0if;

    sget-object v1, LX/0ig;->v:LX/0ih;

    const-string v2, "back_while_spinning_on_submit"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1747697
    :cond_0
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->s:LX/B6u;

    .line 1747698
    iget v1, v0, LX/B6u;->h:I

    move v0, v1

    .line 1747699
    if-nez v0, :cond_1

    .line 1747700
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->j:LX/0if;

    sget-object v1, LX/0ig;->v:LX/0ih;

    const-string v2, "cta_lead_gen_back_button_click"

    const-string v3, "0"

    invoke-virtual {v0, v1, v2, v3}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 1747701
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->j:LX/0if;

    sget-object v1, LX/0ig;->v:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    .line 1747702
    :cond_1
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->s:LX/B6u;

    invoke-virtual {v0}, LX/B6u;->b()Z

    move-result v0

    return v0
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 1747643
    const-string v0, ""

    .line 1747644
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->s:LX/B6u;

    .line 1747645
    iget v2, v1, LX/B6u;->h:I

    move v1, v2

    .line 1747646
    if-nez v1, :cond_1

    .line 1747647
    const-string v0, "730537770401723"

    move-object v1, v0

    .line 1747648
    :goto_0
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1747649
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gt;

    .line 1747650
    iput-object v1, v0, LX/0gt;->a:Ljava/lang/String;

    .line 1747651
    move-object v0, v0

    .line 1747652
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0gt;->a(Landroid/content/Context;)V

    .line 1747653
    :cond_0
    return-void

    .line 1747654
    :cond_1
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->s:LX/B6u;

    invoke-virtual {v1}, LX/B6u;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1747655
    const-string v0, "464514803717009"

    move-object v1, v0

    goto :goto_0

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x30a8e50d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1747587
    const v1, 0x7f0309d6

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->n:Landroid/view/View;

    .line 1747588
    iput-object p3, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->y:Landroid/os/Bundle;

    .line 1747589
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->n:Landroid/view/View;

    const/16 p3, 0x8

    const/4 p2, 0x0

    .line 1747590
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 1747591
    const-string v3, "story_attachment"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1747592
    invoke-static {v2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v5

    .line 1747593
    iget-object v3, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->k:LX/B7G;

    invoke-virtual {v3, v2}, LX/B7G;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/B7F;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->w:LX/B7F;

    .line 1747594
    iget-object v3, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->w:LX/B7F;

    invoke-virtual {v3}, LX/B7F;->a()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1747595
    :goto_0
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->n:Landroid/view/View;

    const/16 v2, 0x2b

    const v3, -0x26008ca4

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1

    .line 1747596
    :cond_0
    const v3, 0x7f0d18fc

    invoke-static {v1, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/support/v4/view/ViewPager;

    iput-object v3, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->r:Landroid/support/v4/view/ViewPager;

    .line 1747597
    iget-object v6, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->g:LX/B6l;

    invoke-static {v5}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object p1

    .line 1747598
    iget-object v3, v5, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1747599
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->v()Z

    move-result v3

    invoke-static {v2}, LX/2sb;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v2

    invoke-virtual {v6, p1, v3, v2}, LX/B6l;->a(LX/0lF;ZI)V

    .line 1747600
    invoke-static {p0}, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->e(Lcom/facebook/leadgen/LeadGenUserInputFormFragment;)Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;

    move-result-object v2

    .line 1747601
    if-nez v2, :cond_1

    iget-object v3, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->y:Landroid/os/Bundle;

    if-eqz v3, :cond_1

    .line 1747602
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->y:Landroid/os/Bundle;

    const-string v3, "LEADGEN_FORM_PENDING_INPUT"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;

    .line 1747603
    :cond_1
    iget-object v3, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->b:LX/B6v;

    iget-object v5, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->r:Landroid/support/v4/view/ViewPager;

    iget-object v6, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->w:LX/B7F;

    invoke-virtual {v3, v5, v6, v2}, LX/B6v;->a(Landroid/support/v4/view/ViewPager;LX/B7F;Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;)LX/B6u;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->s:LX/B6u;

    .line 1747604
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->c:LX/B8q;

    iget-object v3, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->s:LX/B6u;

    .line 1747605
    new-instance p1, LX/B8p;

    const-class v5, LX/B6k;

    invoke-interface {v2, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/B6k;

    invoke-static {v2}, LX/B6l;->a(LX/0QB;)LX/B6l;

    move-result-object v6

    check-cast v6, LX/B6l;

    invoke-direct {p1, v5, v6, v3}, LX/B8p;-><init>(LX/B6k;LX/B6l;LX/B6u;)V

    .line 1747606
    move-object v2, p1

    .line 1747607
    iput-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->z:LX/B8p;

    .line 1747608
    const v2, 0x7f0d18ff

    invoke-static {v1, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    iput-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->u:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    .line 1747609
    const v2, 0x7f0d18fe

    invoke-static {v1, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    iput-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->v:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    .line 1747610
    const v2, 0x7f0d1900

    invoke-static {v1, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    iput-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->x:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    .line 1747611
    const v2, 0x7f0d18fd

    invoke-static {v1, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;

    iput-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->p:Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;

    .line 1747612
    const v2, 0x7f0d18fb

    invoke-static {v1, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;

    iput-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->q:Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;

    .line 1747613
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->a:LX/2sb;

    invoke-virtual {v2}, LX/2sb;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1747614
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->u:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    iget-object v3, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->w:LX/B7F;

    iget-object v5, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->s:LX/B6u;

    iget-object v6, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->D:LX/D82;

    invoke-virtual {v2, v3, v5, v6}, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->a(LX/B7F;LX/B6u;LX/D82;)V

    .line 1747615
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->p:Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;

    iget-object v3, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->w:LX/B7F;

    iget-object v5, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->o:LX/B7i;

    iget-object v6, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->s:LX/B6u;

    invoke-virtual {v2, v3, v5, v6, p2}, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->a(LX/B7F;LX/B7i;LX/B6u;Z)V

    .line 1747616
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->u:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    invoke-virtual {v2}, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->b()V

    .line 1747617
    new-instance v2, LX/B6M;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v5, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->p:Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;

    invoke-direct {v2, v3, v5}, LX/B6M;-><init>(Landroid/content/Context;Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;)V

    iput-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->A:LX/B6M;

    .line 1747618
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->u:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    invoke-virtual {v2, p2}, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->setVisibility(I)V

    .line 1747619
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->p:Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;

    invoke-virtual {v2, p2}, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->setVisibility(I)V

    .line 1747620
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->v:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    invoke-virtual {v2, p3}, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->setVisibility(I)V

    .line 1747621
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->q:Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;

    invoke-virtual {v2, p3}, Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;->setVisibility(I)V

    .line 1747622
    :goto_1
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->l:LX/B7W;

    iget-object v3, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->E:LX/B6z;

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b2;)Z

    .line 1747623
    new-instance v2, LX/B71;

    invoke-direct {v2, p0}, LX/B71;-><init>(Lcom/facebook/leadgen/LeadGenUserInputFormFragment;)V

    .line 1747624
    iget-object v3, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->r:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3, v2}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 1747625
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->e:LX/B6S;

    new-instance v3, LX/B72;

    invoke-direct {v3, p0}, LX/B72;-><init>(Lcom/facebook/leadgen/LeadGenUserInputFormFragment;)V

    .line 1747626
    iput-object v3, v2, LX/B6S;->e:LX/B72;

    .line 1747627
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->a:LX/2sb;

    invoke-virtual {v2}, LX/2sb;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1747628
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->u:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    iget-object v3, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->e:LX/B6S;

    .line 1747629
    iput-object v3, v2, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->g:LX/B6S;

    .line 1747630
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->A:LX/B6M;

    .line 1747631
    iget-object v3, v2, LX/B6M;->a:LX/B7W;

    iget-object v5, v2, LX/B6M;->c:LX/B6K;

    invoke-virtual {v3, v5}, LX/0b4;->a(LX/0b2;)Z

    .line 1747632
    :goto_2
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->n:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    .line 1747633
    new-instance v3, LX/B74;

    invoke-direct {v3, p0}, LX/B74;-><init>(Lcom/facebook/leadgen/LeadGenUserInputFormFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1747634
    goto/16 :goto_0

    .line 1747635
    :cond_2
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->v:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    iget-object v3, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->w:LX/B7F;

    iget-object v5, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->s:LX/B6u;

    iget-object v6, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->o:LX/B7i;

    invoke-virtual {v2, v3, v5, v6}, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->a(LX/B7F;LX/B6u;LX/B7i;)V

    .line 1747636
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->q:Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;

    iget-object v3, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->w:LX/B7F;

    iget-object v5, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->o:LX/B7i;

    iget-object v6, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->s:LX/B6u;

    invoke-virtual {v2, v3, v5, v6}, Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;->a(LX/B7F;LX/B7i;LX/B6u;)V

    .line 1747637
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->v:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    invoke-virtual {v2}, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->b()V

    .line 1747638
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->u:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    invoke-virtual {v2, p3}, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->setVisibility(I)V

    .line 1747639
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->p:Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;

    invoke-virtual {v2, p3}, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->setVisibility(I)V

    .line 1747640
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->v:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    invoke-virtual {v2, p2}, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->setVisibility(I)V

    .line 1747641
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->q:Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;

    invoke-virtual {v2, p2}, Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;->setVisibility(I)V

    goto :goto_1

    .line 1747642
    :cond_3
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->v:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    iget-object v3, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->e:LX/B6S;

    invoke-virtual {v2, v3}, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->a(LX/B6S;)V

    goto :goto_2
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6627e130

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1747585
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1747586
    const/16 v1, 0x2b

    const v2, -0x69dc0054

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x1b45a9d9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1747562
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 1747563
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->s:LX/B6u;

    invoke-virtual {v1}, LX/B6u;->j()V

    .line 1747564
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->a:LX/2sb;

    invoke-virtual {v1}, LX/2sb;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1747565
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->u:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    invoke-virtual {v1}, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->a()V

    .line 1747566
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->p:Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;

    invoke-virtual {v1}, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->a()V

    .line 1747567
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->A:LX/B6M;

    .line 1747568
    iget-object v2, v1, LX/B6M;->a:LX/B7W;

    iget-object v4, v1, LX/B6M;->c:LX/B6K;

    invoke-virtual {v2, v4}, LX/0b4;->b(LX/0b2;)Z

    .line 1747569
    :goto_0
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->s:LX/B6u;

    .line 1747570
    const/4 v2, 0x0

    move v4, v2

    :goto_1
    iget-object v2, v1, LX/B6u;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v2

    if-ge v4, v2, :cond_1

    .line 1747571
    iget-object v2, v1, LX/B6u;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2, v4}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1747572
    instance-of v5, v2, LX/B6N;

    if-eqz v5, :cond_0

    .line 1747573
    check-cast v2, LX/B6N;

    invoke-interface {v2}, LX/B6N;->a()V

    .line 1747574
    :cond_0
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    .line 1747575
    :cond_1
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->e:LX/B6S;

    const/4 v2, 0x0

    .line 1747576
    iput-object v2, v1, LX/B6S;->e:LX/B72;

    .line 1747577
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->l:LX/B7W;

    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->E:LX/B6z;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 1747578
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->B:LX/B6o;

    if-nez v1, :cond_3

    .line 1747579
    :goto_2
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->y:Landroid/os/Bundle;

    invoke-direct {p0, v1}, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->b(Landroid/os/Bundle;)Z

    .line 1747580
    const/16 v1, 0x2b

    const v2, 0x36a9b1a3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1747581
    :cond_2
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->v:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    invoke-virtual {v1}, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->a()V

    .line 1747582
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->q:Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;

    invoke-virtual {v1}, Lcom/facebook/leadgen/view/LeadGenLegacyHeaderView;->a()V

    goto :goto_0

    .line 1747583
    :cond_3
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->B:LX/B6o;

    invoke-virtual {v1}, LX/B6o;->b()V

    .line 1747584
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->C:Landroid/view/ViewTreeObserver;

    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->B:LX/B6o;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->removeOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    goto :goto_2
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1747559
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1747560
    invoke-direct {p0, p1}, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->b(Landroid/os/Bundle;)Z

    .line 1747561
    return-void
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x5877741f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1747556
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 1747557
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->d:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->t:J

    .line 1747558
    const/16 v1, 0x2b

    const v2, 0x69e72742

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x552c1673

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1747554
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 1747555
    const/16 v1, 0x2b

    const v2, -0x7d4c3eab

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
