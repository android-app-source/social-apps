.class public final Lcom/facebook/leadgen/input/LeadGenSpinnerSelectInputView$6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/B8F;


# direct methods
.method public constructor <init>(LX/B8F;)V
    .locals 0

    .prologue
    .line 1748582
    iput-object p1, p0, Lcom/facebook/leadgen/input/LeadGenSpinnerSelectInputView$6;->a:LX/B8F;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 1748583
    iget-object v0, p0, Lcom/facebook/leadgen/input/LeadGenSpinnerSelectInputView$6;->a:LX/B8F;

    iget-object v0, v0, LX/B8F;->e:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1748584
    if-nez v0, :cond_1

    .line 1748585
    :cond_0
    :goto_0
    return-void

    .line 1748586
    :cond_1
    invoke-virtual {v0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v1

    .line 1748587
    if-eqz v1, :cond_2

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/text/Layout;->getParagraphDirection(I)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 1748588
    iget-object v1, p0, Lcom/facebook/leadgen/input/LeadGenSpinnerSelectInputView$6;->a:LX/B8F;

    iget-object v1, v1, LX/B8F;->e:Landroid/widget/Spinner;

    const v2, 0x7f021882

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setBackgroundResource(I)V

    .line 1748589
    :goto_1
    invoke-virtual {v0}, Landroid/widget/TextView;->getHint()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 1748590
    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1748591
    :cond_2
    iget-object v1, p0, Lcom/facebook/leadgen/input/LeadGenSpinnerSelectInputView$6;->a:LX/B8F;

    iget-object v1, v1, LX/B8F;->e:Landroid/widget/Spinner;

    const v2, 0x7f021881

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setBackgroundResource(I)V

    goto :goto_1
.end method
