.class public Lcom/facebook/leadgen/deeplink/LeadGenActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# instance fields
.field public p:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private r:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1748058
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 1748042
    new-instance v0, LX/B8S;

    invoke-direct {v0}, LX/B8S;-><init>()V

    move-object v0, v0

    .line 1748043
    const-string v1, "lead_gen_data_id"

    iget-object v2, p0, Lcom/facebook/leadgen/deeplink/LeadGenActivity;->r:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1748044
    const/4 v1, 0x1

    .line 1748045
    iput-boolean v1, v0, LX/0gW;->l:Z

    .line 1748046
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->a:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    .line 1748047
    iget-object v1, p0, Lcom/facebook/leadgen/deeplink/LeadGenActivity;->q:LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "fetch_lead_gen_deep_link_story"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/facebook/leadgen/deeplink/LeadGenActivity;->r:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/leadgen/deeplink/LeadGenActivity;->p:LX/0tX;

    invoke-virtual {v3, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    new-instance v3, LX/B7S;

    invoke-direct {v3, p0}, LX/B7S;-><init>(Lcom/facebook/leadgen/deeplink/LeadGenActivity;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1748048
    return-void
.end method

.method private static a(Lcom/facebook/leadgen/deeplink/LeadGenActivity;LX/0tX;LX/1Ck;)V
    .locals 0

    .prologue
    .line 1748057
    iput-object p1, p0, Lcom/facebook/leadgen/deeplink/LeadGenActivity;->p:LX/0tX;

    iput-object p2, p0, Lcom/facebook/leadgen/deeplink/LeadGenActivity;->q:LX/1Ck;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/leadgen/deeplink/LeadGenActivity;

    invoke-static {v1}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {v1}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    invoke-static {p0, v0, v1}, Lcom/facebook/leadgen/deeplink/LeadGenActivity;->a(Lcom/facebook/leadgen/deeplink/LeadGenActivity;LX/0tX;LX/1Ck;)V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1748051
    invoke-static {p0, p0}, Lcom/facebook/leadgen/deeplink/LeadGenActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1748052
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1748053
    invoke-virtual {p0}, Lcom/facebook/leadgen/deeplink/LeadGenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "lead_gen_data_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/leadgen/deeplink/LeadGenActivity;->r:Ljava/lang/String;

    .line 1748054
    iget-object v0, p0, Lcom/facebook/leadgen/deeplink/LeadGenActivity;->r:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1748055
    :goto_0
    return-void

    .line 1748056
    :cond_0
    invoke-direct {p0}, Lcom/facebook/leadgen/deeplink/LeadGenActivity;->a()V

    goto :goto_0
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x361a34c4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1748049
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStart()V

    .line 1748050
    const/16 v1, 0x23

    const v2, 0x222c3cc2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
