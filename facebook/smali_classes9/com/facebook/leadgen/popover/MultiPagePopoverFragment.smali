.class public Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;
.super Lcom/facebook/widget/popover/PopoverFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/B7i;


# instance fields
.field public m:LX/1AM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:Landroid/view/View;

.field public o:LX/B6H;

.field private p:LX/8qU;

.field private q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/DialogInterface$OnDismissListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1750611
    invoke-direct {p0}, Lcom/facebook/widget/popover/PopoverFragment;-><init>()V

    return-void
.end method

.method public static A(Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;)V
    .locals 2

    .prologue
    .line 1750606
    invoke-virtual {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1750607
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1750608
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 1750609
    invoke-static {v0, v1}, LX/2Na;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 1750610
    :cond_0
    return-void
.end method

.method private B()LX/B6H;
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1750562
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d0807

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, LX/B6H;

    return-object v0
.end method

.method public static a(LX/B6H;LX/0gc;Landroid/view/Window;Landroid/view/View;)Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;
    .locals 2

    .prologue
    .line 1750601
    new-instance v0, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;

    invoke-direct {v0}, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;-><init>()V

    .line 1750602
    iput-object p0, v0, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;->o:LX/B6H;

    .line 1750603
    move-object v1, v0

    .line 1750604
    invoke-virtual {v1, p1, p2, p3}, Lcom/facebook/widget/popover/PopoverFragment;->a(LX/0gc;Landroid/view/Window;Landroid/view/View;)V

    .line 1750605
    return-object v0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;

    invoke-static {p0}, LX/1AM;->a(LX/0QB;)LX/1AM;

    move-result-object p0

    check-cast p0, LX/1AM;

    iput-object p0, p1, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;->m:LX/1AM;

    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1750596
    invoke-direct {p0}, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;->B()LX/B6H;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;->B()LX/B6H;

    move-result-object v0

    invoke-interface {v0}, LX/B6H;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1750597
    :goto_0
    return v1

    .line 1750598
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->f()I

    move-result v0

    if-le v0, v1, :cond_1

    .line 1750599
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->d()V

    goto :goto_0

    .line 1750600
    :cond_1
    invoke-super {p0}, Lcom/facebook/widget/popover/PopoverFragment;->S_()Z

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 1750593
    invoke-super {p0, p1}, Lcom/facebook/widget/popover/PopoverFragment;->a(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    .line 1750594
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object p0

    const/16 p1, 0x20

    invoke-virtual {p0, p1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 1750595
    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1750592
    const-string v0, "lead_gen"

    return-object v0
.end method

.method public final a(LX/B6H;)V
    .locals 2

    .prologue
    .line 1750588
    iput-object p1, p0, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;->o:LX/B6H;

    .line 1750589
    invoke-static {p0}, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;->A(Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;)V

    .line 1750590
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d0807

    check-cast p1, Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v1, p1}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1750591
    return-void
.end method

.method public final a(Landroid/content/DialogInterface$OnDismissListener;)V
    .locals 1

    .prologue
    .line 1750612
    iget-object v0, p0, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;->q:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1750613
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;->q:Ljava/util/List;

    .line 1750614
    :cond_0
    iget-object v0, p0, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;->q:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1750615
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1750583
    invoke-static {p0}, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;->A(Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;)V

    .line 1750584
    invoke-super {p0}, Lcom/facebook/widget/popover/PopoverFragment;->k()V

    .line 1750585
    invoke-direct {p0}, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;->B()LX/B6H;

    move-result-object v0

    invoke-interface {v0}, LX/B6H;->d()V

    .line 1750586
    iget-object v0, p0, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;->m:LX/1AM;

    new-instance v1, LX/1ZJ;

    invoke-direct {v1}, LX/1ZJ;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1750587
    return-void
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 1750582
    const v0, 0x7f030b96

    return v0
.end method

.method public final o()LX/8qU;
    .locals 1

    .prologue
    .line 1750579
    iget-object v0, p0, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;->p:LX/8qU;

    if-nez v0, :cond_0

    .line 1750580
    new-instance v0, LX/B8n;

    invoke-direct {v0, p0}, LX/B8n;-><init>(Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;)V

    iput-object v0, p0, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;->p:LX/8qU;

    .line 1750581
    :cond_0
    iget-object v0, p0, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;->p:LX/8qU;

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x353c0a06    # -6421245.0f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1750574
    invoke-super {p0, p1}, Lcom/facebook/widget/popover/PopoverFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1750575
    const-class v1, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;

    invoke-static {v1, p0}, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1750576
    iget-object v1, p0, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;->o:LX/B6H;

    if-eqz v1, :cond_0

    .line 1750577
    iget-object v1, p0, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;->o:LX/B6H;

    invoke-virtual {p0, v1}, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;->a(LX/B6H;)V

    .line 1750578
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x3e8e68bd

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x42f44b6c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1750569
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/widget/popover/PopoverFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 1750570
    const v2, 0x7f0d0807

    invoke-static {v1, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;->n:Landroid/view/View;

    .line 1750571
    iget-object v2, p0, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;->n:Landroid/view/View;

    new-instance v3, LX/B8l;

    invoke-direct {v3, p0}, LX/B8l;-><init>(Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1750572
    iget-object v2, p0, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;->n:Landroid/view/View;

    new-instance v3, LX/B8m;

    invoke-direct {v3, p0}, LX/B8m;-><init>(Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1750573
    const/16 v2, 0x2b

    const v3, 0x342c0438

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x2a259e4c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1750563
    invoke-super {p0}, Lcom/facebook/widget/popover/PopoverFragment;->onDestroyView()V

    .line 1750564
    const/4 v2, 0x0

    .line 1750565
    iget-object v1, p0, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;->n:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 1750566
    iget-object v1, p0, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;->n:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1750567
    iget-object v1, p0, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;->n:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1750568
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x7871baed

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 1750556
    invoke-super {p0, p1}, Lcom/facebook/widget/popover/PopoverFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 1750557
    iget-object v0, p0, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;->q:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 1750558
    iget-object v0, p0, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/DialogInterface$OnDismissListener;

    .line 1750559
    invoke-interface {v0, p1}, Landroid/content/DialogInterface$OnDismissListener;->onDismiss(Landroid/content/DialogInterface;)V

    goto :goto_0

    .line 1750560
    :cond_0
    iget-object v0, p0, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1750561
    :cond_1
    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x173f8123

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1750553
    invoke-super {p0}, Lcom/facebook/widget/popover/PopoverFragment;->onResume()V

    .line 1750554
    iget-object v1, p0, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;->m:LX/1AM;

    new-instance v2, LX/1ZH;

    invoke-direct {v2}, LX/1ZH;-><init>()V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1750555
    const/16 v1, 0x2b

    const v2, -0x338763ae    # -6.5171784E7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
