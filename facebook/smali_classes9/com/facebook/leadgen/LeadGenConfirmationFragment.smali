.class public Lcom/facebook/leadgen/LeadGenConfirmationFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/B6H;


# static fields
.field public static final i:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/B6k;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/B6l;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0So;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/2sb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/B7G;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/B6p;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Lcom/facebook/widget/FbScrollView;

.field public k:Landroid/view/View;

.field public l:Lcom/facebook/leadgen/LeadGenConfirmationHeaderView;

.field public m:LX/B7i;

.field private n:J

.field public o:I

.field public p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public q:LX/B7F;

.field public r:LX/B6o;

.field public s:Landroid/view/ViewTreeObserver;

.field public t:LX/B7Q;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1746527
    const-class v0, Lcom/facebook/leadgen/LeadGenConfirmationHeaderView;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->i:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1746526
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/B76;)Lcom/facebook/leadgen/LeadGenConfirmationFragment;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/B76;",
            ")",
            "Lcom/facebook/leadgen/LeadGenConfirmationFragment;"
        }
    .end annotation

    .prologue
    .line 1746520
    new-instance v0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;

    invoke-direct {v0}, Lcom/facebook/leadgen/LeadGenConfirmationFragment;-><init>()V

    .line 1746521
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1746522
    const-string v2, "story_attachment"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1746523
    const-string v2, "send_info_mutation_status"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1746524
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1746525
    return-object v0
.end method

.method public static a(Landroid/view/View;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 1746517
    invoke-static {p0, p1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1746518
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1746519
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    .line 1746512
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1746513
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/leadgen/LeadGenConfirmationFragment;

    const-class v3, LX/B6k;

    invoke-interface {p1, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/B6k;

    invoke-static {p1}, LX/B6l;->a(LX/0QB;)LX/B6l;

    move-result-object v4

    check-cast v4, LX/B6l;

    invoke-static {p1}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    const/16 v6, 0x122d

    invoke-static {p1, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {p1}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v7

    check-cast v7, LX/0if;

    invoke-static {p1}, LX/2sb;->a(LX/0QB;)LX/2sb;

    move-result-object v8

    check-cast v8, LX/2sb;

    const-class v9, LX/B7G;

    invoke-interface {p1, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/B7G;

    const-class v0, LX/B6p;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/B6p;

    iput-object v3, v2, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->a:LX/B6k;

    iput-object v4, v2, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->b:LX/B6l;

    iput-object v5, v2, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->c:LX/0So;

    iput-object v6, v2, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->d:LX/0Or;

    iput-object v7, v2, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->e:LX/0if;

    iput-object v8, v2, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->f:LX/2sb;

    iput-object v9, v2, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->g:LX/B7G;

    iput-object p1, v2, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->h:LX/B6p;

    .line 1746514
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mParentFragment:Landroid/support/v4/app/Fragment;

    move-object v0, v0

    .line 1746515
    check-cast v0, LX/B7i;

    iput-object v0, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->m:LX/B7i;

    .line 1746516
    return-void
.end method

.method public final b()Landroid/widget/ScrollView;
    .locals 1

    .prologue
    .line 1746408
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->j:Lcom/facebook/widget/FbScrollView;

    return-object v0
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 1746508
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->m:LX/B7i;

    invoke-interface {v0}, LX/B7i;->b()V

    .line 1746509
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->e:LX/0if;

    sget-object v1, LX/0ig;->v:LX/0ih;

    const-string v2, "click_back_button_after_submit"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1746510
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->e:LX/0if;

    sget-object v1, LX/0ig;->v:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    .line 1746511
    const/4 v0, 0x1

    return v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1746503
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gt;

    const-string v1, "109198879416907"

    .line 1746504
    iput-object v1, v0, LX/0gt;->a:Ljava/lang/String;

    .line 1746505
    move-object v0, v0

    .line 1746506
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0gt;->a(Landroid/content/Context;)V

    .line 1746507
    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 1746497
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1746498
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->f:LX/2sb;

    invoke-virtual {v0}, LX/2sb;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1746499
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 1746500
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1746501
    :cond_0
    :goto_0
    return-void

    .line 1746502
    :cond_1
    iget-object v0, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x53559a2

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1746422
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->f:LX/2sb;

    invoke-virtual {v1}, LX/2sb;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1746423
    const v1, 0x7f0309bd

    invoke-virtual {p1, v1, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->k:Landroid/view/View;

    .line 1746424
    :goto_0
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->k:Landroid/view/View;

    .line 1746425
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 1746426
    const-string v3, "story_attachment"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1746427
    iget-object v3, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->g:LX/B7G;

    invoke-virtual {v3, v2}, LX/B7G;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/B7F;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->q:LX/B7F;

    .line 1746428
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->q:LX/B7F;

    invoke-virtual {v2}, LX/B7F;->a()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1746429
    :goto_1
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->f:LX/2sb;

    invoke-virtual {v1}, LX/2sb;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1746430
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->k:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    .line 1746431
    new-instance v2, LX/B6F;

    invoke-direct {v2, p0}, LX/B6F;-><init>(Lcom/facebook/leadgen/LeadGenConfirmationFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1746432
    :cond_0
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->k:Landroid/view/View;

    const v2, -0x7c36980c

    invoke-static {v2, v0}, LX/02F;->f(II)V

    return-object v1

    .line 1746433
    :cond_1
    const v1, 0x7f0309ce

    invoke-virtual {p1, v1, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->k:Landroid/view/View;

    goto :goto_0

    .line 1746434
    :cond_2
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->q:LX/B7F;

    invoke-virtual {v2}, LX/B7F;->j()I

    move-result v2

    iput v2, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->o:I

    .line 1746435
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->q:LX/B7F;

    .line 1746436
    new-instance v3, LX/B7Q;

    invoke-static {v2}, LX/B7F;->y(LX/B7F;)Lcom/facebook/graphql/model/GraphQLLeadGenData;

    move-result-object p1

    iget-object p2, v2, LX/B7F;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-direct {v3, p1, p2}, LX/B7Q;-><init>(Lcom/facebook/graphql/model/GraphQLLeadGenData;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)V

    move-object v2, v3

    .line 1746437
    iput-object v2, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->t:LX/B7Q;

    .line 1746438
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 1746439
    const-string v3, "send_info_mutation_status"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, LX/B76;

    .line 1746440
    const-string p1, ""

    .line 1746441
    const-string v3, ""

    .line 1746442
    sget-object p2, LX/B6G;->b:[I

    invoke-virtual {v2}, LX/B76;->ordinal()I

    move-result p3

    aget p2, p2, p3

    packed-switch p2, :pswitch_data_0

    .line 1746443
    :goto_2
    iget-object p2, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->k:Landroid/view/View;

    const p3, 0x7f0d18c8

    invoke-static {p2, p3, p1}, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->a(Landroid/view/View;ILjava/lang/String;)V

    .line 1746444
    iget-object p1, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->k:Landroid/view/View;

    const p2, 0x7f0d18c9

    invoke-static {p1, p2, v3}, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->a(Landroid/view/View;ILjava/lang/String;)V

    .line 1746445
    iget-object v3, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->f:LX/2sb;

    invoke-virtual {v3}, LX/2sb;->a()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1746446
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->k:Landroid/view/View;

    const v3, 0x7f0d04f9

    invoke-static {v2, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/leadgen/view/LeadGenPageProfileHeaderView;

    .line 1746447
    iget-object v3, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->k:Landroid/view/View;

    const p1, 0x7f0d18c5

    invoke-static {v3, p1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/leadgen/view/LeadGenHeaderBackgroundView;

    .line 1746448
    iget-object p1, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->q:LX/B7F;

    invoke-virtual {p1}, LX/B7F;->s()LX/B7K;

    move-result-object p1

    iget-object p2, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->q:LX/B7F;

    invoke-virtual {p2}, LX/B7F;->u()Z

    move-result p2

    invoke-virtual {v2, p1, p2}, Lcom/facebook/leadgen/view/LeadGenPageProfileHeaderView;->a(LX/B7K;Z)V

    .line 1746449
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->q:LX/B7F;

    invoke-virtual {v2}, LX/B7F;->u()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1746450
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->q:LX/B7F;

    invoke-virtual {v2}, LX/B7F;->s()LX/B7K;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/facebook/leadgen/view/LeadGenHeaderBackgroundView;->setUpView(LX/B7K;)V

    .line 1746451
    :cond_3
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->k:Landroid/view/View;

    const v3, 0x7f0d18ca

    invoke-static {v2, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;

    .line 1746452
    iget-object v3, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->q:LX/B7F;

    iget-object p1, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->m:LX/B7i;

    const/4 p2, 0x0

    const/4 p3, 0x1

    invoke-virtual {v2, v3, p1, p2, p3}, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->a(LX/B7F;LX/B7i;LX/B6u;Z)V

    .line 1746453
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->k:Landroid/view/View;

    const v3, 0x7f0d18c7

    invoke-static {v2, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/FbScrollView;

    iput-object v2, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->j:Lcom/facebook/widget/FbScrollView;

    .line 1746454
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->j:Lcom/facebook/widget/FbScrollView;

    new-instance v3, Lcom/facebook/leadgen/LeadGenConfirmationFragment$3;

    invoke-direct {v3, p0}, Lcom/facebook/leadgen/LeadGenConfirmationFragment$3;-><init>(Lcom/facebook/leadgen/LeadGenConfirmationFragment;)V

    invoke-virtual {v2, v3}, Lcom/facebook/widget/FbScrollView;->post(Ljava/lang/Runnable;)Z

    .line 1746455
    :goto_3
    const v2, 0x7f0d18cd

    iget-object v3, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->t:LX/B7Q;

    .line 1746456
    iget-object p1, v3, LX/B7Q;->e:Ljava/lang/String;

    move-object v3, p1

    .line 1746457
    invoke-static {v1, v2, v3}, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->a(Landroid/view/View;ILjava/lang/String;)V

    .line 1746458
    const v2, 0x7f0d18cc

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    .line 1746459
    const-string v2, ""

    .line 1746460
    sget-object v3, LX/B6G;->a:[I

    iget-object p2, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->t:LX/B7Q;

    .line 1746461
    iget-object p3, p2, LX/B7Q;->i:LX/B7P;

    move-object p2, p3

    .line 1746462
    invoke-virtual {p2}, LX/B7P;->ordinal()I

    move-result p2

    aget v3, v3, p2

    packed-switch v3, :pswitch_data_1

    .line 1746463
    :goto_4
    const-string v3, "cta_lead_gen_visit_offsite_click"

    .line 1746464
    sget-object p2, LX/1vY;->GENERIC_CALL_TO_ACTION_BUTTON:LX/1vY;

    invoke-static {p1, p2}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 1746465
    const p2, 0x7f0d0083

    invoke-virtual {p1, p2, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1746466
    new-instance p2, LX/B6C;

    invoke-direct {p2, p0, v2}, LX/B6C;-><init>(Lcom/facebook/leadgen/LeadGenConfirmationFragment;Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1746467
    goto/16 :goto_1

    .line 1746468
    :pswitch_0
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->t:LX/B7Q;

    .line 1746469
    iget-object v3, v2, LX/B7Q;->f:Ljava/lang/String;

    move-object v2, v3

    .line 1746470
    goto :goto_4

    .line 1746471
    :pswitch_1
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->t:LX/B7Q;

    .line 1746472
    iget-object v3, v2, LX/B7Q;->g:Ljava/lang/String;

    move-object v3, v3

    .line 1746473
    const v2, 0x7f0d08f8

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/fbui/glyph/GlyphView;

    check-cast v2, Lcom/facebook/fbui/glyph/GlyphView;

    .line 1746474
    const/4 p2, 0x0

    invoke-virtual {v2, p2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    move-object v2, v3

    .line 1746475
    goto :goto_4

    .line 1746476
    :pswitch_2
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->t:LX/B7Q;

    .line 1746477
    iget-object v3, v2, LX/B7Q;->h:Ljava/lang/String;

    move-object v2, v3

    .line 1746478
    goto :goto_4

    .line 1746479
    :pswitch_3
    iget-object v3, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->t:LX/B7Q;

    .line 1746480
    iget-object p1, v3, LX/B7Q;->a:Ljava/lang/String;

    move-object p1, p1

    .line 1746481
    iget-object v3, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->t:LX/B7Q;

    .line 1746482
    iget-object p2, v3, LX/B7Q;->c:Ljava/lang/String;

    move-object v3, p2

    .line 1746483
    goto/16 :goto_2

    .line 1746484
    :pswitch_4
    iget-object v3, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->t:LX/B7Q;

    .line 1746485
    iget-object p1, v3, LX/B7Q;->b:Ljava/lang/String;

    move-object p1, p1

    .line 1746486
    iget-object v3, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->t:LX/B7Q;

    .line 1746487
    iget-object p2, v3, LX/B7Q;->d:Ljava/lang/String;

    move-object v3, p2

    .line 1746488
    goto/16 :goto_2

    .line 1746489
    :cond_4
    iget-object v3, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->k:Landroid/view/View;

    const p1, 0x7f0d18f2

    invoke-static {v3, p1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/leadgen/LeadGenConfirmationHeaderView;

    iput-object v3, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->l:Lcom/facebook/leadgen/LeadGenConfirmationHeaderView;

    .line 1746490
    iget-object v3, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->l:Lcom/facebook/leadgen/LeadGenConfirmationHeaderView;

    iget-object p1, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->q:LX/B7F;

    iget-object p2, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->m:LX/B7i;

    invoke-virtual {v3, p1, p2}, Lcom/facebook/leadgen/LeadGenConfirmationHeaderView;->a(LX/B7F;LX/B7i;)V

    .line 1746491
    iget-object v3, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->l:Lcom/facebook/leadgen/LeadGenConfirmationHeaderView;

    iget p1, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->o:I

    invoke-virtual {v3, p1}, Lcom/facebook/leadgen/LeadGenConfirmationHeaderView;->c(I)V

    .line 1746492
    iget-object v3, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->k:Landroid/view/View;

    const p1, 0x7f0d18f3

    invoke-virtual {v3, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v3, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1746493
    iget-object v3, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 p1, 0x0

    invoke-virtual {v3, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1746494
    sget-object v3, LX/B76;->SUCCESS:LX/B76;

    if-ne v2, v3, :cond_5

    .line 1746495
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v3, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->q:LX/B7F;

    invoke-virtual {v3}, LX/B7F;->h()Landroid/net/Uri;

    move-result-object v3

    sget-object p1, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->i:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto/16 :goto_3

    .line 1746496
    :cond_5
    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const p1, 0x7f02063a

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final onDestroyView()V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x2dfbe80a

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1746414
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 1746415
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->f:LX/2sb;

    invoke-virtual {v1}, LX/2sb;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1746416
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->l:Lcom/facebook/leadgen/LeadGenConfirmationHeaderView;

    invoke-virtual {v1}, Lcom/facebook/leadgen/LeadGenConfirmationHeaderView;->a()V

    .line 1746417
    :goto_0
    const v1, -0x3eb10f4a

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void

    .line 1746418
    :cond_0
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->r:LX/B6o;

    if-nez v1, :cond_1

    .line 1746419
    :goto_1
    goto :goto_0

    .line 1746420
    :cond_1
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->r:LX/B6o;

    invoke-virtual {v1}, LX/B6o;->b()V

    .line 1746421
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->s:Landroid/view/ViewTreeObserver;

    iget-object v2, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->r:LX/B6o;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->removeOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    goto :goto_1
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x59a57d3a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1746411
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 1746412
    iget-object v1, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->c:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->n:J

    .line 1746413
    const/16 v1, 0x2b

    const v2, -0x15c35cea

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x165b0a57

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1746409
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 1746410
    const/16 v1, 0x2b

    const v2, -0xca191ca

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
