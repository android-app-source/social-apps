.class public Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/7N6;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Bak;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;

.field public c:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/caspian/ui/standardheader/ProfileVideoView;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
            ">;"
        }
    .end annotation
.end field

.field public e:Landroid/view/ViewStub;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1799892
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1799893
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1799890
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1799891
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1799888
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1799889
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;

    const/16 v1, 0x188c

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->a:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1799882
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->b:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;

    if-eqz v0, :cond_0

    .line 1799883
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->b:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->setVisibility(I)V

    .line 1799884
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->b:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->setEnableDarkOverlay(Z)V

    .line 1799885
    :cond_0
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->d:LX/0zw;

    if-eqz v0, :cond_1

    .line 1799886
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->d:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->c()V

    .line 1799887
    :cond_1
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1799876
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->b:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;

    if-eqz v0, :cond_0

    .line 1799877
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->b:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->setVisibility(I)V

    .line 1799878
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->b:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->setEnableDarkOverlay(Z)V

    .line 1799879
    :cond_0
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->d:LX/0zw;

    if-eqz v0, :cond_1

    .line 1799880
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bak;

    iget-object v1, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->d:LX/0zw;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, LX/Bak;->a(LX/0zw;I)V

    .line 1799881
    :cond_1
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1799870
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->b:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;

    if-eqz v0, :cond_0

    .line 1799871
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->b:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;

    invoke-virtual {v0, v2}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->setVisibility(I)V

    .line 1799872
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->b:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;

    invoke-virtual {v0, v2}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->setEnableDarkOverlay(Z)V

    .line 1799873
    :cond_0
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->d:LX/0zw;

    if-eqz v0, :cond_1

    .line 1799874
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bak;

    iget-object v1, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->d:LX/0zw;

    invoke-virtual {v0, v1, v2}, LX/Bak;->a(LX/0zw;I)V

    .line 1799875
    :cond_1
    return-void
.end method

.method public getLazyProfileVideoIcon()LX/0zw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0zw",
            "<",
            "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1799869
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->d:LX/0zw;

    return-object v0
.end method

.method public getLazyProfileVideoView()LX/0zw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0zw",
            "<",
            "Lcom/facebook/caspian/ui/standardheader/ProfileVideoView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1799868
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->c:LX/0zw;

    return-object v0
.end method

.method public getProfileEditIconViewStub()Landroid/view/ViewStub;
    .locals 1

    .prologue
    .line 1799867
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->e:Landroid/view/ViewStub;

    return-object v0
.end method

.method public getStandardProfileImageView()Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;
    .locals 1

    .prologue
    .line 1799866
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->b:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;

    return-object v0
.end method

.method public final onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x72b36731

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1799857
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onFinishInflate()V

    .line 1799858
    const-class v0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;

    invoke-static {v0, p0}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1799859
    const v0, 0x7f0d2d7c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;

    iput-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->b:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;

    .line 1799860
    const v0, 0x7f0d2780

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1799861
    new-instance v2, LX/0zw;

    invoke-direct {v2, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v2, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->c:LX/0zw;

    .line 1799862
    const v0, 0x7f0d2788

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1799863
    new-instance v2, LX/0zw;

    invoke-direct {v2, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v2, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->d:LX/0zw;

    .line 1799864
    const v0, 0x7f0d2722

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->e:Landroid/view/ViewStub;

    .line 1799865
    const/16 v0, 0x2d

    const v2, 0x65baf869

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
