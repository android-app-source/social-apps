.class public Lcom/facebook/caspian/ui/standardheader/ProfileVideoView;
.super LX/2oW;
.source ""


# instance fields
.field public m:LX/7MX;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1799547
    invoke-direct {p0, p1}, LX/2oW;-><init>(Landroid/content/Context;)V

    .line 1799548
    invoke-direct {p0}, Lcom/facebook/caspian/ui/standardheader/ProfileVideoView;->c()V

    .line 1799549
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1799550
    invoke-direct {p0, p1, p2}, LX/2oW;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1799551
    invoke-direct {p0}, Lcom/facebook/caspian/ui/standardheader/ProfileVideoView;->c()V

    .line 1799552
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1799537
    invoke-direct {p0, p1, p2, p3}, LX/2oW;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1799538
    invoke-direct {p0}, Lcom/facebook/caspian/ui/standardheader/ProfileVideoView;->c()V

    .line 1799539
    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 1799545
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setShouldCropToFit(Z)V

    .line 1799546
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "LX/0Px",
            "<+",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1799553
    new-instance v0, LX/7MX;

    invoke-direct {v0, p1}, LX/7MX;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/caspian/ui/standardheader/ProfileVideoView;->m:LX/7MX;

    .line 1799554
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/ProfileVideoView;->m:LX/7MX;

    new-instance v1, LX/7Nb;

    invoke-direct {v1, p1}, LX/7Nb;-><init>(Landroid/content/Context;)V

    new-instance v2, Lcom/facebook/video/player/plugins/OverlayImagePlugin;

    invoke-direct {v2, p1}, Lcom/facebook/video/player/plugins/OverlayImagePlugin;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public getCoverViewPlugin()LX/7MX;
    .locals 1

    .prologue
    .line 1799544
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/ProfileVideoView;->m:LX/7MX;

    return-object v0
.end method

.method public getDefaultPlayerOrigin()LX/04D;
    .locals 1

    .prologue
    .line 1799543
    sget-object v0, LX/04D;->PROFILE_VIDEO:LX/04D;

    return-object v0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 1799540
    invoke-super {p0}, LX/2oW;->h()V

    .line 1799541
    sget-object v0, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 1799542
    return-void
.end method
