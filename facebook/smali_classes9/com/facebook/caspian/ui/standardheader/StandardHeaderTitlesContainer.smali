.class public Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""


# instance fields
.field private j:Lcom/facebook/resources/ui/FbTextView;

.field private k:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1799841
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;)V

    .line 1799842
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1799839
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1799840
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1799837
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1799838
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1799829
    const v0, 0x7f0d2d74

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->j:Lcom/facebook/resources/ui/FbTextView;

    .line 1799830
    const v0, 0x7f0d2d75

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->k:Lcom/facebook/resources/ui/FbTextView;

    .line 1799831
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->k:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {}, LX/8tu;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1799832
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->j:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {}, LX/8tu;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1799833
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->j:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setFocusable(Z)V

    .line 1799834
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->j:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1799835
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->k:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1799836
    return-void
.end method


# virtual methods
.method public getTitleTextPaint()Landroid/text/TextPaint;
    .locals 1

    .prologue
    .line 1799828
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->j:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    return-object v0
.end method

.method public final onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x4f13bfd8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1799843
    invoke-super {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->onFinishInflate()V

    .line 1799844
    invoke-direct {p0}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->d()V

    .line 1799845
    const/16 v1, 0x2d

    const v2, 0x499b6454    # 1272970.5f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setSubtitleOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1799826
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->k:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1799827
    return-void
.end method

.method public setSubtitleText(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 1799822
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->k:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1799823
    iget-object v1, p0, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->k:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1799824
    return-void

    .line 1799825
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSubtitleTextAppearance(I)V
    .locals 2

    .prologue
    .line 1799820
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->k:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->k:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/facebook/resources/ui/FbTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 1799821
    return-void
.end method

.method public setTitleText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1799818
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->j:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1799819
    return-void
.end method

.method public setTitleTextAppearance(I)V
    .locals 2

    .prologue
    .line 1799816
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->j:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->j:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/facebook/resources/ui/FbTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 1799817
    return-void
.end method
