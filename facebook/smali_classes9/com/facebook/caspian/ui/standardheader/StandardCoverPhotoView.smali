.class public Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;
.super Lcom/facebook/drawee/view/DraweeView;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/common/callercontext/CallerContext;

.field private c:LX/1bf;

.field public d:LX/1bf;

.field private e:Landroid/graphics/PointF;

.field private f:Landroid/graphics/Paint;

.field private g:Lcom/facebook/fbui/widget/text/ImageWithTextView;

.field private h:Landroid/graphics/drawable/Drawable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Landroid/graphics/drawable/Drawable;

.field private j:LX/Baj;

.field private k:I

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:I

.field public p:Z

.field public q:LX/1cC;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1799697
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1799698
    invoke-direct {p0, p1}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->a(Landroid/content/Context;)V

    .line 1799699
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1799799
    invoke-direct {p0, p1, p2}, Lcom/facebook/drawee/view/DraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1799800
    iput-boolean v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->l:Z

    .line 1799801
    iput-boolean v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->m:Z

    .line 1799802
    iput-boolean v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->n:Z

    .line 1799803
    invoke-direct {p0, p1}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->a(Landroid/content/Context;)V

    .line 1799804
    return-void
.end method

.method private a(IZLX/1bf;LX/1bf;Landroid/graphics/PointF;ZZ[Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;Landroid/view/View$OnClickListener;LX/1cC;ZZ)V
    .locals 2

    .prologue
    .line 1799772
    iput p1, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->k:I

    .line 1799773
    iput-object p5, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->e:Landroid/graphics/PointF;

    .line 1799774
    iput-boolean p12, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->l:Z

    .line 1799775
    iput-boolean p6, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->m:Z

    .line 1799776
    iput-object p9, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 1799777
    iput-boolean p2, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->p:Z

    .line 1799778
    iput-object p3, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->c:LX/1bf;

    .line 1799779
    iput-object p4, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->d:LX/1bf;

    .line 1799780
    invoke-direct {p0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->e()V

    .line 1799781
    invoke-direct {p0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->d()Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p13, :cond_1

    if-eqz p7, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->n:Z

    .line 1799782
    invoke-direct {p0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1799783
    if-eqz p8, :cond_2

    array-length v0, p8

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 1799784
    invoke-virtual {p0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080f64

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p8}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1799785
    :goto_1
    invoke-virtual {p0, v0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1799786
    iput-object p11, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->q:LX/1cC;

    .line 1799787
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->c:LX/1bf;

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_2
    invoke-static {p0, v0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->a$redex0(Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;Z)V

    .line 1799788
    :cond_0
    :goto_3
    invoke-virtual {p0, p10}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1799789
    return-void

    .line 1799790
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1799791
    :cond_2
    if-eqz p8, :cond_3

    array-length v0, p8

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 1799792
    invoke-virtual {p0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080f65

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p8}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1799793
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080072

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1799794
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 1799795
    :cond_5
    if-eqz p13, :cond_0

    .line 1799796
    invoke-virtual {p0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080f69

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1799797
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    iget-object v1, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1Ae;->d(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1799798
    invoke-virtual {p0, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    goto :goto_3
.end method

.method private a(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1799758
    const-class v0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    invoke-static {v0, p0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1799759
    invoke-virtual {p0, v5}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->setClickable(Z)V

    .line 1799760
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f021888

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->i:Landroid/graphics/drawable/Drawable;

    .line 1799761
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f021887

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1799762
    new-instance v0, LX/Baj;

    invoke-virtual {p0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v0, v2}, LX/Baj;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->j:LX/Baj;

    .line 1799763
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b087a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->o:I

    .line 1799764
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f0313b1

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/ImageWithTextView;

    iput-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->g:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    .line 1799765
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->g:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    const/4 v3, -0x1

    invoke-direct {v2, v4, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1799766
    new-instance v0, LX/1Uo;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v0, v2}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    iget-object v2, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->i:Landroid/graphics/drawable/Drawable;

    sget-object v3, LX/1Up;->h:LX/1Up;

    invoke-virtual {v0, v2, v3}, LX/1Uo;->a(Landroid/graphics/drawable/Drawable;LX/1Up;)LX/1Uo;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/1Uo;->f(Landroid/graphics/drawable/Drawable;)LX/1Uo;

    move-result-object v0

    sget-object v1, LX/1Up;->h:LX/1Up;

    invoke-virtual {v0, v1}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v0

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 1799767
    invoke-virtual {p0, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 1799768
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->f:Landroid/graphics/Paint;

    .line 1799769
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->f:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0034

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1799770
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->f:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0416

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1799771
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    const/16 v1, 0x509

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->a:LX/0Or;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;Z)V
    .locals 2

    .prologue
    const/high16 v1, 0x3f000000    # 0.5f

    .line 1799750
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->e:Landroid/graphics/PointF;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->e:Landroid/graphics/PointF;

    move-object v1, v0

    .line 1799751
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    .line 1799752
    invoke-virtual {v0, v1}, LX/1af;->a(Landroid/graphics/PointF;)V

    .line 1799753
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    iget-object v1, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    iget-object v1, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->c:LX/1bf;

    invoke-virtual {v0, v1}, LX/1Ae;->d(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->d:LX/1bf;

    :goto_1
    invoke-virtual {v0, v1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->p()LX/1Ad;

    move-result-object v0

    new-instance v1, LX/Bao;

    invoke-direct {v1, p0}, LX/Bao;-><init>(Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;)V

    invoke-virtual {v0, v1}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1799754
    invoke-virtual {p0, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1799755
    return-void

    .line 1799756
    :cond_0
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v1, v1}, Landroid/graphics/PointF;-><init>(FF)V

    move-object v1, v0

    goto :goto_0

    .line 1799757
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 1799747
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->c:LX/1bf;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->d:LX/1bf;

    if-nez v0, :cond_0

    .line 1799748
    const/4 v0, 0x0

    .line 1799749
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 1799805
    invoke-virtual {p0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1799806
    iget v1, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->k:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1799807
    invoke-virtual {p0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->requestLayout()V

    .line 1799808
    return-void
.end method

.method private getEditIconDrawable()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 1799744
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->h:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 1799745
    invoke-virtual {p0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f021466

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->h:Landroid/graphics/drawable/Drawable;

    .line 1799746
    :cond_0
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->h:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method


# virtual methods
.method public final a(IIZLX/1bf;LX/1bf;Landroid/graphics/PointF;ZZ[Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;Landroid/view/View$OnClickListener;LX/1cC;ZZ)V
    .locals 14

    .prologue
    .line 1799739
    const-string v0, "StandardCoverPhotoView.bindModel"

    const v1, -0x7760cf04

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    move-object v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move/from16 v6, p7

    move/from16 v7, p8

    move-object/from16 v8, p9

    move-object/from16 v9, p10

    move-object/from16 v10, p11

    move-object/from16 v11, p12

    move/from16 v12, p13

    move/from16 v13, p14

    .line 1799740
    :try_start_0
    invoke-direct/range {v0 .. v13}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->a(IZLX/1bf;LX/1bf;Landroid/graphics/PointF;ZZ[Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;Landroid/view/View$OnClickListener;LX/1cC;ZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1799741
    const v0, 0x68b6b7ff

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1799742
    return-void

    .line 1799743
    :catchall_0
    move-exception v0

    const v1, 0x7cd6c03a

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1799738
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->d:LX/1bf;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/high16 v7, 0x3f000000    # 0.5f

    .line 1799718
    invoke-super {p0, p1}, Lcom/facebook/drawee/view/DraweeView;->onDraw(Landroid/graphics/Canvas;)V

    .line 1799719
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->getHeight()I

    move-result v0

    int-to-float v2, v0

    invoke-virtual {p0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->getHeight()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->f:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1799720
    iget-boolean v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->l:Z

    if-eqz v0, :cond_0

    .line 1799721
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->j:LX/Baj;

    invoke-virtual {v0, p1}, LX/Baj;->draw(Landroid/graphics/Canvas;)V

    .line 1799722
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->n:Z

    if-eqz v0, :cond_1

    .line 1799723
    invoke-virtual {p0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->g:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    invoke-virtual {v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    .line 1799724
    invoke-virtual {p0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->g:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    invoke-virtual {v2}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    .line 1799725
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1799726
    int-to-float v0, v0

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1799727
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->g:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->draw(Landroid/graphics/Canvas;)V

    .line 1799728
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1799729
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->m:Z

    if-eqz v0, :cond_3

    .line 1799730
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1799731
    invoke-static {p0}, LX/0vv;->h(Landroid/view/View;)I

    move-result v0

    if-ne v0, v6, :cond_4

    move v0, v6

    .line 1799732
    :goto_0
    if-eqz v0, :cond_2

    .line 1799733
    const/high16 v0, -0x40800000    # -1.0f

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v7

    invoke-virtual {p0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v7

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 1799734
    :cond_2
    invoke-direct {p0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->getEditIconDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1799735
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1799736
    :cond_3
    return-void

    .line 1799737
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onLayout(ZIIII)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1799710
    invoke-super/range {p0 .. p5}, Lcom/facebook/drawee/view/DraweeView;->onLayout(ZIIII)V

    .line 1799711
    if-eqz p1, :cond_1

    .line 1799712
    iget-boolean v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->l:Z

    if-eqz v0, :cond_0

    .line 1799713
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->j:LX/Baj;

    invoke-virtual {p0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->getHeight()I

    move-result v1

    iget v2, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->o:I

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->getHeight()I

    move-result v3

    invoke-virtual {v0, v4, v1, v2, v3}, LX/Baj;->setBounds(IIII)V

    .line 1799714
    :cond_0
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->g:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    iget-object v1, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->g:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    invoke-virtual {v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->g:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    invoke-virtual {v2}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v4, v4, v1, v2}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->layout(IIII)V

    .line 1799715
    iget-boolean v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->m:Z

    if-eqz v0, :cond_1

    .line 1799716
    invoke-direct {p0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->getEditIconDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->getWidth()I

    move-result v1

    invoke-direct {p0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->getEditIconDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->getHeight()I

    move-result v2

    invoke-direct {p0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->getEditIconDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->getHeight()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1799717
    :cond_1
    return-void
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 1799705
    invoke-super {p0, p1, p2}, Lcom/facebook/drawee/view/DraweeView;->onMeasure(II)V

    .line 1799706
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1799707
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 1799708
    iget-object v2, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->g:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    invoke-virtual {v2, v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->measure(II)V

    .line 1799709
    return-void
.end method

.method public setPlaceholderDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 3
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/high16 v2, 0x3f000000    # 0.5f

    .line 1799700
    if-nez p1, :cond_0

    .line 1799701
    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    iget-object v1, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, LX/1af;->b(Landroid/graphics/drawable/Drawable;)V

    .line 1799702
    :goto_0
    return-void

    .line 1799703
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {v0, p1}, LX/1af;->b(Landroid/graphics/drawable/Drawable;)V

    .line 1799704
    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    iget-object v1, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->e:Landroid/graphics/PointF;

    if-nez v1, :cond_1

    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1, v2, v2}, Landroid/graphics/PointF;-><init>(FF)V

    :goto_1
    invoke-virtual {v0, v1}, LX/1af;->b(Landroid/graphics/PointF;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->e:Landroid/graphics/PointF;

    goto :goto_1
.end method
