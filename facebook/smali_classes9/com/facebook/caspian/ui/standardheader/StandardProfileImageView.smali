.class public Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;
.super Lcom/facebook/drawee/view/DraweeView;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:Lcom/facebook/fbui/widget/text/ImageWithTextView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:I

.field private d:Landroid/graphics/drawable/Drawable;

.field private e:Landroid/graphics/drawable/Drawable;

.field private f:Landroid/graphics/drawable/Drawable;

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1799961
    const-class v0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;

    sput-object v0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1799958
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1799959
    invoke-direct {p0, p1}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->a(Landroid/content/Context;)V

    .line 1799960
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1799953
    invoke-direct {p0, p1, p2}, Lcom/facebook/drawee/view/DraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1799954
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->c:I

    .line 1799955
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->g:Ljava/util/List;

    .line 1799956
    invoke-direct {p0, p1}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->a(Landroid/content/Context;)V

    .line 1799957
    return-void
.end method

.method private a(I)Lcom/facebook/fbui/widget/text/ImageWithTextView;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 1799932
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->b:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    if-nez v0, :cond_1

    .line 1799933
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 1799934
    invoke-virtual {p0}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0313b2

    invoke-virtual {p0}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/ImageWithTextView;

    iput-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->b:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    .line 1799935
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->b:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1799936
    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1799937
    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1799938
    :cond_1
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->b:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    return-object v0

    .line 1799939
    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 1799940
    invoke-virtual {p0}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0313b3

    invoke-virtual {p0}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/ImageWithTextView;

    iput-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->b:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    .line 1799941
    new-instance v0, LX/0wM;

    invoke-virtual {p0}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0wM;-><init>(Landroid/content/res/Resources;)V

    .line 1799942
    iget-object v1, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->b:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    const v2, 0x7f0207b5

    invoke-virtual {v0, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1799947
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->setClickable(Z)V

    .line 1799948
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f021888

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->d:Landroid/graphics/drawable/Drawable;

    .line 1799949
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f021121

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->e:Landroid/graphics/drawable/Drawable;

    .line 1799950
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f021887

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->f:Landroid/graphics/drawable/Drawable;

    .line 1799951
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->setEnableDarkOverlay(Z)V

    .line 1799952
    return-void
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    .line 1799962
    invoke-super {p0, p1}, Lcom/facebook/drawee/view/DraweeView;->onDraw(Landroid/graphics/Canvas;)V

    .line 1799963
    iget v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->c:I

    packed-switch v0, :pswitch_data_0

    .line 1799964
    sget-object v0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->a:Ljava/lang/Class;

    const-string v1, "Expected a valid EditAffordance, got: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1799965
    :goto_0
    :pswitch_0
    return-void

    .line 1799966
    :pswitch_1
    invoke-virtual {p0}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->getHeight()I

    move-result v0

    iget v1, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->c:I

    invoke-direct {p0, v1}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->a(I)Lcom/facebook/fbui/widget/text/ImageWithTextView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1799967
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1799968
    const/4 v1, 0x0

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1799969
    iget v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->c:I

    invoke-direct {p0, v0}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->a(I)Lcom/facebook/fbui/widget/text/ImageWithTextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->draw(Landroid/graphics/Canvas;)V

    .line 1799970
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final onLayout(ZIIII)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1799943
    invoke-super/range {p0 .. p5}, Lcom/facebook/drawee/view/DraweeView;->onLayout(ZIIII)V

    .line 1799944
    iget v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->c:I

    if-nez v0, :cond_0

    .line 1799945
    :goto_0
    return-void

    .line 1799946
    :cond_0
    iget v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->c:I

    invoke-direct {p0, v0}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->a(I)Lcom/facebook/fbui/widget/text/ImageWithTextView;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->getMeasuredWidth()I

    move-result v1

    iget v2, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->c:I

    invoke-direct {p0, v2}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->a(I)Lcom/facebook/fbui/widget/text/ImageWithTextView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->layout(IIII)V

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 1799925
    invoke-super {p0, p1, p2}, Lcom/facebook/drawee/view/DraweeView;->onMeasure(II)V

    .line 1799926
    iget v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->c:I

    if-nez v0, :cond_0

    .line 1799927
    :goto_0
    return-void

    .line 1799928
    :cond_0
    iget v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->c:I

    invoke-direct {p0, v0}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->a(I)Lcom/facebook/fbui/widget/text/ImageWithTextView;

    move-result-object v0

    invoke-static {p0}, LX/0vv;->h(Landroid/view/View;)I

    move-result v1

    invoke-static {v0, v1}, LX/0vv;->e(Landroid/view/View;I)V

    .line 1799929
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1799930
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 1799931
    iget v2, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->c:I

    invoke-direct {p0, v2}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->a(I)Lcom/facebook/fbui/widget/text/ImageWithTextView;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->measure(II)V

    goto :goto_0
.end method

.method public setEditAffordance(I)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1799920
    if-eqz p1, :cond_0

    if-eq p1, v0, :cond_0

    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid edit affordance value: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1799921
    iput p1, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->c:I

    .line 1799922
    invoke-virtual {p0}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->requestLayout()V

    .line 1799923
    return-void

    .line 1799924
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setEnableDarkOverlay(Z)V
    .locals 3

    .prologue
    .line 1799894
    if-eqz p1, :cond_1

    .line 1799895
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->g:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1799896
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->g:Ljava/util/List;

    .line 1799897
    iget-object v0, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->g:Ljava/util/List;

    iget-object v1, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->f:Landroid/graphics/drawable/Drawable;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1799898
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a041c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 1799899
    iget-object v1, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->g:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1799900
    :cond_0
    new-instance v0, LX/1Uo;

    invoke-virtual {p0}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    iget-object v1, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->d:Landroid/graphics/drawable/Drawable;

    .line 1799901
    iput-object v1, v0, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 1799902
    move-object v0, v0

    .line 1799903
    iget-object v1, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->e:Landroid/graphics/drawable/Drawable;

    .line 1799904
    iput-object v1, v0, LX/1Uo;->h:Landroid/graphics/drawable/Drawable;

    .line 1799905
    move-object v0, v0

    .line 1799906
    iget-object v1, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->g:Ljava/util/List;

    .line 1799907
    iput-object v1, v0, LX/1Uo;->s:Ljava/util/List;

    .line 1799908
    move-object v0, v0

    .line 1799909
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 1799910
    invoke-virtual {p0, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 1799911
    :goto_0
    return-void

    .line 1799912
    :cond_1
    new-instance v0, LX/1Uo;

    invoke-virtual {p0}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    iget-object v1, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->d:Landroid/graphics/drawable/Drawable;

    .line 1799913
    iput-object v1, v0, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 1799914
    move-object v0, v0

    .line 1799915
    iget-object v1, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->e:Landroid/graphics/drawable/Drawable;

    .line 1799916
    iput-object v1, v0, LX/1Uo;->h:Landroid/graphics/drawable/Drawable;

    .line 1799917
    move-object v0, v0

    .line 1799918
    iget-object v1, p0, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, LX/1Uo;->f(Landroid/graphics/drawable/Drawable;)LX/1Uo;

    move-result-object v0

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 1799919
    invoke-virtual {p0, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    goto :goto_0
.end method
