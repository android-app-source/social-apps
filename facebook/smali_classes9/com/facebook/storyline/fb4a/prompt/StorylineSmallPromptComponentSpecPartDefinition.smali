.class public Lcom/facebook/storyline/fb4a/prompt/StorylineSmallPromptComponentSpecPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/1Qa;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/1Ri;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final d:LX/2tz;

.field private final e:LX/1V0;

.field private final f:Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1kZ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/2tz;LX/1V0;Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;LX/0Or;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/2tz;",
            "LX/1V0;",
            "Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;",
            "LX/0Or",
            "<",
            "LX/1kZ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1780027
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1780028
    iput-object p2, p0, Lcom/facebook/storyline/fb4a/prompt/StorylineSmallPromptComponentSpecPartDefinition;->d:LX/2tz;

    .line 1780029
    iput-object p3, p0, Lcom/facebook/storyline/fb4a/prompt/StorylineSmallPromptComponentSpecPartDefinition;->e:LX/1V0;

    .line 1780030
    iput-object p4, p0, Lcom/facebook/storyline/fb4a/prompt/StorylineSmallPromptComponentSpecPartDefinition;->f:Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;

    .line 1780031
    iput-object p5, p0, Lcom/facebook/storyline/fb4a/prompt/StorylineSmallPromptComponentSpecPartDefinition;->g:LX/0Or;

    .line 1780032
    return-void
.end method

.method private a(LX/1De;LX/1Ri;LX/1Pn;)LX/1X1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1Ri;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1780033
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/prompt/StorylineSmallPromptComponentSpecPartDefinition;->e:LX/1V0;

    move-object v0, p3

    check-cast v0, LX/1Ps;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->h:LX/1Ua;

    invoke-direct {v2, v3}, LX/1X6;-><init>(LX/1Ua;)V

    iget-object v3, p0, Lcom/facebook/storyline/fb4a/prompt/StorylineSmallPromptComponentSpecPartDefinition;->d:LX/2tz;

    const/4 v4, 0x0

    .line 1780034
    new-instance v5, LX/2u4;

    invoke-direct {v5, v3}, LX/2u4;-><init>(LX/2tz;)V

    .line 1780035
    iget-object p0, v3, LX/2tz;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/BOh;

    .line 1780036
    if-nez p0, :cond_0

    .line 1780037
    new-instance p0, LX/BOh;

    invoke-direct {p0, v3}, LX/BOh;-><init>(LX/2tz;)V

    .line 1780038
    :cond_0
    invoke-static {p0, p1, v4, v4, v5}, LX/BOh;->a$redex0(LX/BOh;LX/1De;IILX/2u4;)V

    .line 1780039
    move-object v5, p0

    .line 1780040
    move-object v4, v5

    .line 1780041
    move-object v3, v4

    .line 1780042
    iget-object v4, v3, LX/BOh;->a:LX/2u4;

    iput-object p2, v4, LX/2u4;->a:LX/1Ri;

    .line 1780043
    iget-object v4, v3, LX/BOh;->e:Ljava/util/BitSet;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 1780044
    move-object v3, v3

    .line 1780045
    iget-object v4, v3, LX/BOh;->a:LX/2u4;

    iput-object p3, v4, LX/2u4;->b:LX/1Pn;

    .line 1780046
    iget-object v4, v3, LX/BOh;->e:Ljava/util/BitSet;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 1780047
    move-object v3, v3

    .line 1780048
    invoke-virtual {v3}, LX/1X5;->d()LX/1X1;

    move-result-object v3

    invoke-virtual {v1, p1, v0, v2, v3}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/1aD;LX/1Ri;LX/1Pn;)LX/1dV;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;",
            "LX/1Ri;",
            "TE;)",
            "LX/1dV;"
        }
    .end annotation

    .prologue
    .line 1780049
    iget-object v1, p0, Lcom/facebook/storyline/fb4a/prompt/StorylineSmallPromptComponentSpecPartDefinition;->f:Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;

    new-instance v2, LX/24N;

    iget-object v3, p2, LX/1Ri;->c:LX/AkL;

    iget-object v4, p2, LX/1Ri;->a:LX/1RN;

    move-object v0, p3

    check-cast v0, LX/1Pr;

    iget-object v5, p2, LX/1Ri;->b:LX/0jW;

    invoke-direct {v2, v3, v4, v0, v5}, LX/24N;-><init>(LX/AkL;LX/1RN;LX/1Pr;LX/0jW;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1780050
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(LX/1aD;Ljava/lang/Object;LX/1Pn;)LX/1dV;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/storyline/fb4a/prompt/StorylineSmallPromptComponentSpecPartDefinition;
    .locals 9

    .prologue
    .line 1780051
    const-class v1, Lcom/facebook/storyline/fb4a/prompt/StorylineSmallPromptComponentSpecPartDefinition;

    monitor-enter v1

    .line 1780052
    :try_start_0
    sget-object v0, Lcom/facebook/storyline/fb4a/prompt/StorylineSmallPromptComponentSpecPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1780053
    sput-object v2, Lcom/facebook/storyline/fb4a/prompt/StorylineSmallPromptComponentSpecPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1780054
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1780055
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1780056
    new-instance v3, Lcom/facebook/storyline/fb4a/prompt/StorylineSmallPromptComponentSpecPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/2tz;->a(LX/0QB;)LX/2tz;

    move-result-object v5

    check-cast v5, LX/2tz;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v6

    check-cast v6, LX/1V0;

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;

    const/16 v8, 0x1225

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/facebook/storyline/fb4a/prompt/StorylineSmallPromptComponentSpecPartDefinition;-><init>(Landroid/content/Context;LX/2tz;LX/1V0;Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;LX/0Or;)V

    .line 1780057
    move-object v0, v3

    .line 1780058
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1780059
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/storyline/fb4a/prompt/StorylineSmallPromptComponentSpecPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1780060
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1780061
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1780062
    check-cast p2, LX/1Ri;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/storyline/fb4a/prompt/StorylineSmallPromptComponentSpecPartDefinition;->a(LX/1De;LX/1Ri;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1780063
    check-cast p2, LX/1Ri;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/storyline/fb4a/prompt/StorylineSmallPromptComponentSpecPartDefinition;->a(LX/1De;LX/1Ri;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1Pn;)LX/1dV;
    .locals 1

    .prologue
    .line 1780064
    check-cast p2, LX/1Ri;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/storyline/fb4a/prompt/StorylineSmallPromptComponentSpecPartDefinition;->a(LX/1aD;LX/1Ri;LX/1Pn;)LX/1dV;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1780065
    check-cast p2, LX/1Ri;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/storyline/fb4a/prompt/StorylineSmallPromptComponentSpecPartDefinition;->a(LX/1aD;LX/1Ri;LX/1Pn;)LX/1dV;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1780066
    check-cast p1, LX/1Ri;

    .line 1780067
    if-eqz p1, :cond_0

    iget-object v0, p1, LX/1Ri;->a:LX/1RN;

    invoke-static {v0}, LX/1RT;->a(LX/1RN;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1780068
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
