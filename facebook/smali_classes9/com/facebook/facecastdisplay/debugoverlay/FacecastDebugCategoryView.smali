.class public Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugCategoryView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private final a:Lcom/facebook/resources/ui/FbTextView;

.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/Acm;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/view/LayoutInflater;

.field private final d:Landroid/os/Handler;

.field public e:LX/Acl;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1693102
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugCategoryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1693103
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1693104
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugCategoryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1693105
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1693106
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1693107
    const v0, 0x7f0305c4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1693108
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugCategoryView;->setOrientation(I)V

    .line 1693109
    const v0, 0x7f0d0fda

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugCategoryView;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 1693110
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugCategoryView;->b:Ljava/util/Map;

    .line 1693111
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugCategoryView;->c:Landroid/view/LayoutInflater;

    .line 1693112
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugCategoryView;->d:Landroid/os/Handler;

    .line 1693113
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1693114
    iget-object v0, p0, Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugCategoryView;->b:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Acm;

    .line 1693115
    if-nez v0, :cond_0

    .line 1693116
    iget-object v0, p0, Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugCategoryView;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f0305c5

    invoke-virtual {v0, v1, p0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 1693117
    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const/16 v2, 0xf2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1693118
    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugCategoryView;->addView(Landroid/view/View;)V

    .line 1693119
    new-instance v1, LX/Acm;

    invoke-direct {v1, p0, v0}, LX/Acm;-><init>(Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugCategoryView;Lcom/facebook/resources/ui/FbTextView;)V

    .line 1693120
    iget-object v0, p0, Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugCategoryView;->b:Ljava/util/Map;

    invoke-interface {v0, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v1

    .line 1693121
    :cond_0
    const-string v1, "no_video_id"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1693122
    iget-object v1, v0, LX/Acm;->a:Lcom/facebook/resources/ui/FbTextView;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/CharSequence;

    const-string v3, "id "

    aput-object v3, v2, v4

    const/4 v3, 0x1

    aput-object p2, v2, v3

    const/4 v3, 0x2

    const-string v4, "\n"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    aput-object p1, v2, v3

    invoke-static {v2}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1693123
    :goto_0
    iget-object v1, p0, Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugCategoryView;->d:Landroid/os/Handler;

    iget-object v2, v0, LX/Acm;->b:Ljava/lang/Runnable;

    invoke-static {v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1693124
    iget-object v1, p0, Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugCategoryView;->d:Landroid/os/Handler;

    iget-object v0, v0, LX/Acm;->b:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    const v4, 0x228ad3f9

    invoke-static {v1, v0, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1693125
    return-void

    .line 1693126
    :cond_1
    iget-object v1, v0, LX/Acm;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setListener(LX/Acl;)V
    .locals 0

    .prologue
    .line 1693127
    iput-object p1, p0, Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugCategoryView;->e:LX/Acl;

    .line 1693128
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1693129
    iget-object v0, p0, Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugCategoryView;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1693130
    return-void
.end method
