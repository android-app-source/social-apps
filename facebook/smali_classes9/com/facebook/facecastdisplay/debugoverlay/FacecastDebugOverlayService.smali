.class public Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugOverlayService;
.super Landroid/app/Service;
.source ""


# instance fields
.field public a:LX/Acp;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1693148
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 1693149
    return-void
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 1693150
    new-instance v0, LX/Acn;

    invoke-direct {v0, p0}, LX/Acn;-><init>(Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugOverlayService;)V

    return-object v0
.end method

.method public final onCreate()V
    .locals 10

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x158e90c3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1693137
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 1693138
    const-string v0, "window"

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugOverlayService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 1693139
    new-instance v2, LX/Acp;

    invoke-direct {v2, p0}, LX/Acp;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugOverlayService;->a:LX/Acp;

    .line 1693140
    iget-object v2, p0, Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugOverlayService;->a:LX/Acp;

    .line 1693141
    iput-object v0, v2, LX/Acp;->d:Landroid/view/WindowManager;

    .line 1693142
    new-instance v4, LX/Aco;

    invoke-direct {v4, v2}, LX/Aco;-><init>(LX/Acp;)V

    invoke-virtual {v2, v4}, LX/Acp;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1693143
    invoke-virtual {v2}, LX/Acp;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b19f6

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 1693144
    new-instance v4, Landroid/view/WindowManager$LayoutParams;

    const/4 v6, -0x2

    const/16 v7, 0x7d5

    const/16 v8, 0x8

    const/4 v9, -0x3

    invoke-direct/range {v4 .. v9}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    iput-object v4, v2, LX/Acp;->e:Landroid/view/WindowManager$LayoutParams;

    .line 1693145
    iget-object v4, v2, LX/Acp;->e:Landroid/view/WindowManager$LayoutParams;

    const/16 v5, 0x33

    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 1693146
    iget-object v4, v2, LX/Acp;->d:Landroid/view/WindowManager;

    iget-object v5, v2, LX/Acp;->e:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v4, v2, v5}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1693147
    const/16 v0, 0x25

    const v2, -0x14f592d9

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x330067d1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1693132
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 1693133
    const-string v0, "window"

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugOverlayService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 1693134
    iget-object v2, p0, Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugOverlayService;->a:LX/Acp;

    invoke-interface {v0, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 1693135
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/debugoverlay/FacecastDebugOverlayService;->a:LX/Acp;

    .line 1693136
    const/16 v0, 0x25

    const v2, -0xc4802b9

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
