.class public Lcom/facebook/facecastdisplay/FacecastUserTileView;
.super Lcom/facebook/user/tiles/UserTileView;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1692161
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/FacecastUserTileView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1692162
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1692170
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/FacecastUserTileView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1692171
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1692172
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/user/tiles/UserTileView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1692173
    return-void
.end method


# virtual methods
.method public setParam(LX/Ac5;)V
    .locals 4

    .prologue
    .line 1692163
    new-instance v0, LX/0XI;

    invoke-direct {v0}, LX/0XI;-><init>()V

    sget-object v1, LX/0XG;->FACEBOOK:LX/0XG;

    iget-object v2, p1, LX/Ac5;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    move-result-object v0

    .line 1692164
    iget-object v1, p1, LX/Ac5;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1692165
    new-instance v1, Lcom/facebook/user/model/PicSquareUrlWithSize;

    iget v2, p1, LX/Ac5;->c:I

    iget-object v3, p1, LX/Ac5;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/facebook/user/model/PicSquareUrlWithSize;-><init>(ILjava/lang/String;)V

    .line 1692166
    new-instance v2, Lcom/facebook/user/model/PicSquare;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/facebook/user/model/PicSquare;-><init>(LX/0Px;)V

    .line 1692167
    iput-object v2, v0, LX/0XI;->p:Lcom/facebook/user/model/PicSquare;

    .line 1692168
    :cond_0
    invoke-virtual {v0}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v0

    invoke-virtual {p1}, LX/Ac5;->a()LX/8ue;

    move-result-object v1

    invoke-static {v0, v1}, LX/8t9;->a(Lcom/facebook/user/model/User;LX/8ue;)LX/8t9;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 1692169
    return-void
.end method
