.class public Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;
.super Landroid/support/v7/widget/RecyclerView;
.source ""


# instance fields
.field public final h:LX/AcQ;

.field public final i:Ljava/lang/Runnable;

.field public final j:Landroid/graphics/Rect;

.field public k:Z

.field private l:Z

.field public m:I

.field private n:Landroid/graphics/Paint;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1692587
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1692588
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1692585
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1692586
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1692579
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1692580
    new-instance v0, LX/AcQ;

    invoke-direct {v0, p0, p1}, LX/AcQ;-><init>(Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->h:LX/AcQ;

    .line 1692581
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->h:LX/AcQ;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1692582
    new-instance v0, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView$1;

    invoke-direct {v0, p0}, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView$1;-><init>(Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;)V

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->i:Ljava/lang/Runnable;

    .line 1692583
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->j:Landroid/graphics/Rect;

    .line 1692584
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1692575
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView;->draw(Landroid/graphics/Canvas;)V

    .line 1692576
    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->l:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->k:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->n:Landroid/graphics/Paint;

    if-eqz v0, :cond_0

    .line 1692577
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    int-to-float v3, v0

    iget v4, p0, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->o:F

    iget-object v5, p0, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->n:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1692578
    :cond_0
    return-void
.end method

.method public getFirstCompletelyVisiblePosition()I
    .locals 1

    .prologue
    .line 1692574
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->h:LX/AcQ;

    invoke-virtual {v0}, LX/1P1;->m()I

    move-result v0

    return v0
.end method

.method public getLastCompletelyVisiblePosition()I
    .locals 1

    .prologue
    .line 1692589
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->h:LX/AcQ;

    invoke-virtual {v0}, LX/1P1;->o()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getLayoutManager()LX/1OR;
    .locals 1

    .prologue
    .line 1692572
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->h:LX/AcQ;

    move-object v0, v0

    .line 1692573
    return-object v0
.end method

.method public getLayoutManager()LX/AcQ;
    .locals 1

    .prologue
    .line 1692571
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->h:LX/AcQ;

    return-object v0
.end method

.method public final isOpaque()Z
    .locals 1

    .prologue
    .line 1692568
    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->l:Z

    if-eqz v0, :cond_0

    .line 1692569
    const/4 v0, 0x0

    .line 1692570
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/support/v7/widget/RecyclerView;->isOpaque()Z

    move-result v0

    goto :goto_0
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 1692565
    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->k:Z

    if-eqz v0, :cond_0

    .line 1692566
    new-instance v0, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView$2;

    invoke-direct {v0, p0}, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView$2;-><init>(Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->post(Ljava/lang/Runnable;)Z

    .line 1692567
    :cond_0
    return-void
.end method

.method public setFullScreen(Z)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 1692553
    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->l:Z

    if-ne v0, p1, :cond_1

    .line 1692554
    :cond_0
    :goto_0
    return-void

    .line 1692555
    :cond_1
    iput-boolean p1, p0, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->l:Z

    .line 1692556
    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->l:Z

    if-eqz v0, :cond_2

    .line 1692557
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v2}, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 1692558
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->n:Landroid/graphics/Paint;

    if-nez v0, :cond_0

    .line 1692559
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->n:Landroid/graphics/Paint;

    .line 1692560
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b04b1

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->o:F

    .line 1692561
    new-instance v0, Landroid/graphics/LinearGradient;

    iget v4, p0, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->o:F

    const/4 v6, -0x1

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move v2, v1

    move v3, v1

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    .line 1692562
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->n:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 1692563
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->n:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    goto :goto_0

    .line 1692564
    :cond_2
    invoke-virtual {p0, v5, v2}, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->setLayerType(ILandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public setMinimized(Z)V
    .locals 1

    .prologue
    .line 1692549
    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->k:Z

    if-ne v0, p1, :cond_0

    .line 1692550
    :goto_0
    return-void

    .line 1692551
    :cond_0
    iput-boolean p1, p0, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->k:Z

    .line 1692552
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->requestLayout()V

    goto :goto_0
.end method
