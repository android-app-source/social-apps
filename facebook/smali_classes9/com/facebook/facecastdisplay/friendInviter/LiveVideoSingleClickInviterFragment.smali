.class public Lcom/facebook/facecastdisplay/friendInviter/LiveVideoSingleClickInviterFragment;
.super Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;
.source ""


# static fields
.field public static f:Ljava/lang/String;

.field public static g:Ljava/lang/String;


# instance fields
.field public h:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/AeB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1697098
    const-string v0, "recent_invitee_section"

    sput-object v0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoSingleClickInviterFragment;->f:Ljava/lang/String;

    .line 1697099
    const-string v0, "suggested_section"

    sput-object v0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoSingleClickInviterFragment;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1697097
    invoke-direct {p0}, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoSingleClickInviterFragment;

    invoke-static {p0}, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteHelper;->b(LX/0QB;)Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteHelper;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteHelper;

    invoke-static {p0}, LX/AeB;->a(LX/0QB;)LX/AeB;

    move-result-object p0

    check-cast p0, LX/AeB;

    iput-object v1, p1, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoSingleClickInviterFragment;->h:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteHelper;

    iput-object p0, p1, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoSingleClickInviterFragment;->i:LX/AeB;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1697093
    invoke-super {p0, p1}, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->a(Landroid/os/Bundle;)V

    .line 1697094
    const-class v0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoSingleClickInviterFragment;

    invoke-static {v0, p0}, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoSingleClickInviterFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1697095
    iget-object v0, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoSingleClickInviterFragment;->i:LX/AeB;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoSingleClickInviterFragment;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/AeB;->a(Ljava/lang/String;)V

    .line 1697096
    return-void
.end method

.method public final a(Lcom/facebook/widget/singleclickinvite/SingleClickInviteUserToken;)V
    .locals 3

    .prologue
    .line 1697086
    invoke-virtual {p1}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->p()Ljava/lang/String;

    move-result-object v0

    .line 1697087
    iget-object v1, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoSingleClickInviterFragment;->h:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteHelper;

    iget-object v2, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoSingleClickInviterFragment;->j:Ljava/lang/String;

    .line 1697088
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteHelper;->a(Ljava/lang/String;LX/0Px;)V

    .line 1697089
    iget-object v1, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoSingleClickInviterFragment;->i:LX/AeB;

    .line 1697090
    iget-object v2, v1, LX/AeB;->g:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 1697091
    :goto_0
    return-void

    .line 1697092
    :cond_0
    iget-object v2, v1, LX/AeB;->b:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1697085
    iget-object v0, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoSingleClickInviterFragment;->i:LX/AeB;

    invoke-virtual {v0, p1}, LX/AeB;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 1697100
    sget-object v0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoSingleClickInviterFragment;->f:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1697101
    const v0, 0x7f080c30

    .line 1697102
    :goto_0
    return v0

    .line 1697103
    :cond_0
    sget-object v0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoSingleClickInviterFragment;->g:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1697104
    const v0, 0x7f080c31

    goto :goto_0

    .line 1697105
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1697084
    return-void
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1697083
    sget-object v0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoSingleClickInviterFragment;->f:Ljava/lang/String;

    sget-object v1, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoSingleClickInviterFragment;->g:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final k()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 1697082
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->b:LX/0TD;

    new-instance v1, LX/AeD;

    invoke-direct {v1, p0}, LX/AeD;-><init>(Lcom/facebook/facecastdisplay/friendInviter/LiveVideoSingleClickInviterFragment;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final m()LX/2RS;
    .locals 1

    .prologue
    .line 1697081
    sget-object v0, LX/2RS;->COMMUNICATION_RANK:LX/2RS;

    return-object v0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 1697077
    const/4 v0, 0x1

    return v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x1ed183a5

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1697078
    invoke-super {p0, p1}, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1697079
    iget-object v1, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->a:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    iget-object v2, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->a:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getImeOptions()I

    move-result v2

    const v3, -0x2000001

    and-int/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setImeOptions(I)V

    .line 1697080
    const/16 v1, 0x2b

    const v2, -0x1dffeb6e

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
