.class public Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/1b4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:LX/Adv;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1696268
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 1696269
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;

    invoke-static {p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-static {p0}, LX/1b4;->b(LX/0QB;)LX/1b4;

    move-result-object p0

    check-cast p0, LX/1b4;

    iput-object v1, p1, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;->m:Ljava/lang/Boolean;

    iput-object p0, p1, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;->n:LX/1b4;

    return-void
.end method


# virtual methods
.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2e8484c7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1696270
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1696271
    iget-object v1, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v1, v1

    .line 1696272
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const v2, 0x7f0e0280

    invoke-virtual {v1, v2}, Landroid/view/Window;->setWindowAnimations(I)V

    .line 1696273
    const/16 v1, 0x2b

    const v2, -0x3f128dd0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x67dd7b74

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1696274
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1696275
    const-class v1, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;

    invoke-static {v1, p0}, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;->a(Ljava/lang/Class;LX/02k;)V

    .line 1696276
    const v1, 0x7f0e0390

    invoke-virtual {p0, v3, v1}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 1696277
    const/16 v1, 0x2b

    const v2, -0x10872117

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0xe7647d8

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1696278
    const v1, 0x7f030a29

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x174b398c

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 1696279
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 1696280
    iget-object v0, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;->p:LX/Adv;

    if-eqz v0, :cond_0

    .line 1696281
    iget-object v0, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;->p:LX/Adv;

    .line 1696282
    iget-object p0, v0, LX/Adv;->a:LX/Ae2;

    invoke-virtual {p0}, LX/Ae2;->f()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1696283
    iget-object p0, v0, LX/Adv;->a:LX/Ae2;

    invoke-static {p0}, LX/Ae2;->v$redex0(LX/Ae2;)V

    .line 1696284
    :cond_0
    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x10201062

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1696285
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onResume()V

    .line 1696286
    iget-object v1, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v1, v1

    .line 1696287
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 1696288
    const/16 v1, 0x2b

    const v2, -0x12ebb21e    # -2.868621E27f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 9

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x71d1e819

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1696289
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onStart()V

    .line 1696290
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 1696291
    const v2, 0x7f0d19b8

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 1696292
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v4, 0x7f040038

    invoke-static {v2, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v5

    .line 1696293
    const/4 v2, 0x1

    invoke-virtual {v5, v2}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 1696294
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 1696295
    const v4, 0x7f0d19ba

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    .line 1696296
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    const v6, 0x7f0400c9

    invoke-static {v4, v6}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v6

    .line 1696297
    iget-object v4, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v4, v4

    .line 1696298
    const v7, 0x7f0d12a1

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 1696299
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f0400c9

    invoke-static {v7, v8}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v7

    .line 1696300
    invoke-virtual {v2, v6}, Landroid/widget/FrameLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1696301
    invoke-virtual {v4, v7}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1696302
    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1696303
    const/16 v1, 0x2b

    const v2, -0x52a8d69e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    .line 1696304
    invoke-super {p0, p1, p2}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1696305
    check-cast p1, Landroid/view/ViewGroup;

    invoke-static {p1}, LX/AVI;->a(Landroid/view/ViewGroup;)LX/AVI;

    .line 1696306
    const v0, 0x7f0d02c4

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1696307
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1696308
    const-string v2, "is_show_caspian_style"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1696309
    const-string v2, "is_sticky_header_off"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1696310
    const v2, 0x41e065f

    iget v3, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;->r:I

    if-ne v2, v3, :cond_0

    .line 1696311
    new-instance v2, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteGroupMembersFragment;

    invoke-direct {v2}, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteGroupMembersFragment;-><init>()V

    .line 1696312
    const-string v3, "group_feed_id"

    iget-object v4, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;->q:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1696313
    invoke-virtual {v2, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1696314
    iput-object p0, v2, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteGroupMembersFragment;->y:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;

    .line 1696315
    iget-object v3, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;->o:Ljava/lang/String;

    .line 1696316
    iput-object v3, v2, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteGroupMembersFragment;->z:Ljava/lang/String;

    .line 1696317
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v3

    invoke-virtual {v3}, LX/0gc;->a()LX/0hH;

    move-result-object v3

    const v4, 0x7f0d19ba

    invoke-virtual {v3, v4, v2}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v2

    invoke-virtual {v2}, LX/0hH;->b()I

    .line 1696318
    const v1, 0x7f080bdb

    move v4, v1

    move-object v1, v0

    move v0, v4

    .line 1696319
    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 1696320
    new-instance v0, LX/Ae7;

    invoke-direct {v0, p0}, LX/Ae7;-><init>(Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;)V

    .line 1696321
    const v1, 0x7f0d09a9

    invoke-virtual {p0, v1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1696322
    const v1, 0x7f0d19b9

    invoke-virtual {p0, v1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1696323
    return-void

    .line 1696324
    :cond_0
    iget-object v2, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;->o:Ljava/lang/String;

    if-nez v2, :cond_2

    .line 1696325
    :goto_1
    iget-object v1, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;->m:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f080bdc

    move v4, v1

    move-object v1, v0

    move v0, v4

    goto :goto_0

    :cond_1
    const v1, 0x7f080bda

    move v4, v1

    move-object v1, v0

    move v0, v4

    goto :goto_0

    .line 1696326
    :cond_2
    iget-object v2, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;->n:LX/1b4;

    invoke-virtual {v2}, LX/1b4;->aa()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1696327
    new-instance v2, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoSingleClickInviterFragment;

    invoke-direct {v2}, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoSingleClickInviterFragment;-><init>()V

    .line 1696328
    invoke-virtual {v2, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1696329
    iget-object v3, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;->o:Ljava/lang/String;

    .line 1696330
    iput-object v3, v2, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoSingleClickInviterFragment;->j:Ljava/lang/String;

    .line 1696331
    :goto_2
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v3

    invoke-virtual {v3}, LX/0gc;->a()LX/0hH;

    move-result-object v3

    const v4, 0x7f0d19ba

    invoke-virtual {v3, v4, v2}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v2

    invoke-virtual {v2}, LX/0hH;->b()I

    goto :goto_1

    .line 1696332
    :cond_3
    new-instance v2, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterFragment;

    invoke-direct {v2}, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterFragment;-><init>()V

    .line 1696333
    invoke-virtual {v2, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1696334
    iput-object p0, v2, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterFragment;->y:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;

    .line 1696335
    iget-object v3, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;->o:Ljava/lang/String;

    .line 1696336
    iput-object v3, v2, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterFragment;->z:Ljava/lang/String;

    .line 1696337
    goto :goto_2
.end method
