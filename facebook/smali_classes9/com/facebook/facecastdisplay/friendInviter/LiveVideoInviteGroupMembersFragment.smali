.class public Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteGroupMembersFragment;
.super Lcom/facebook/groups/members/GroupsMembersSelectorFragment;
.source ""


# instance fields
.field public w:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/AeB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1696922
    invoke-direct {p0}, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteGroupMembersFragment;

    invoke-static {p0}, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteHelper;->b(LX/0QB;)Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteHelper;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteHelper;

    invoke-static {p0}, LX/AeB;->a(LX/0QB;)LX/AeB;

    move-result-object p0

    check-cast p0, LX/AeB;

    iput-object v1, p1, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteGroupMembersFragment;->w:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteHelper;

    iput-object p0, p1, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteGroupMembersFragment;->x:LX/AeB;

    return-void
.end method


# virtual methods
.method public final I()Z
    .locals 1

    .prologue
    .line 1696923
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1696924
    invoke-super {p0, p1}, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->a(Landroid/os/Bundle;)V

    .line 1696925
    const-class v0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteGroupMembersFragment;

    invoke-static {v0, p0}, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteGroupMembersFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1696926
    iget-object v0, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteGroupMembersFragment;->x:LX/AeB;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteGroupMembersFragment;->z:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/AeB;->a(Ljava/lang/String;)V

    .line 1696927
    return-void
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1696928
    iget-object v0, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteGroupMembersFragment;->x:LX/AeB;

    invoke-virtual {v0, p1}, LX/AeB;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final y()V
    .locals 3

    .prologue
    .line 1696929
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->o()LX/0Px;

    move-result-object v0

    .line 1696930
    iget-object v1, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteGroupMembersFragment;->w:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteHelper;

    iget-object v2, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteGroupMembersFragment;->z:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteHelper;->a(Ljava/lang/String;LX/0Px;)V

    .line 1696931
    iget-object v1, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteGroupMembersFragment;->x:LX/AeB;

    invoke-virtual {v1, v0}, LX/AeB;->a(Ljava/util/List;)V

    .line 1696932
    iget-object v0, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteGroupMembersFragment;->y:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;

    if-eqz v0, :cond_0

    .line 1696933
    iget-object v0, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteGroupMembersFragment;->y:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;

    .line 1696934
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1696935
    :cond_0
    return-void
.end method
