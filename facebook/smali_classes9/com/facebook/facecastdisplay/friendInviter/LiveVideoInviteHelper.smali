.class public Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteHelper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private final a:LX/0aG;

.field public final b:LX/0kL;

.field private final c:LX/0Sh;

.field private final d:LX/1b4;


# direct methods
.method public constructor <init>(LX/0aG;LX/0kL;LX/0Sh;LX/1b4;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1696941
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1696942
    iput-object p1, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteHelper;->a:LX/0aG;

    .line 1696943
    iput-object p2, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteHelper;->b:LX/0kL;

    .line 1696944
    iput-object p3, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteHelper;->c:LX/0Sh;

    .line 1696945
    iput-object p4, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteHelper;->d:LX/1b4;

    .line 1696946
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteHelper;
    .locals 5

    .prologue
    .line 1696947
    new-instance v4, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteHelper;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v0

    check-cast v0, LX/0aG;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v1

    check-cast v1, LX/0kL;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-static {p0}, LX/1b4;->b(LX/0QB;)LX/1b4;

    move-result-object v3

    check-cast v3, LX/1b4;

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteHelper;-><init>(LX/0aG;LX/0kL;LX/0Sh;LX/1b4;)V

    .line 1696948
    return-object v4
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/0Px;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1696949
    if-eqz p1, :cond_0

    .line 1696950
    new-instance v0, Lcom/facebook/facecastdisplay/protocol/LiveVideoInviteFriendsParams;

    invoke-direct {v0, p1, p2}, Lcom/facebook/facecastdisplay/protocol/LiveVideoInviteFriendsParams;-><init>(Ljava/lang/String;LX/0Px;)V

    .line 1696951
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1696952
    const-string v1, "liveVideoInviteFriendsParamKey"

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1696953
    iget-object v0, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteHelper;->a:LX/0aG;

    const-string v1, "live_video_invite_friends"

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const-class v4, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterFragment;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, 0x5acc8f3f

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 1696954
    iget-object v1, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteHelper;->d:LX/1b4;

    invoke-virtual {v1}, LX/1b4;->aa()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1696955
    :cond_0
    :goto_0
    return-void

    .line 1696956
    :cond_1
    iget-object v1, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteHelper;->c:LX/0Sh;

    new-instance v2, LX/AeC;

    invoke-direct {v2, p0}, LX/AeC;-><init>(Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteHelper;)V

    invoke-virtual {v1, v0, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_0
.end method
