.class public Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterFragment;
.super Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static u:Ljava/lang/String;

.field public static v:Ljava/lang/String;


# instance fields
.field public w:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/AeB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1696746
    const-string v0, "recent_invitee_section"

    sput-object v0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterFragment;->u:Ljava/lang/String;

    .line 1696747
    const-string v0, "suggested_section"

    sput-object v0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterFragment;->v:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1696745
    invoke-direct {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterFragment;

    invoke-static {p0}, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteHelper;->b(LX/0QB;)Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteHelper;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteHelper;

    invoke-static {p0}, LX/AeB;->a(LX/0QB;)LX/AeB;

    move-result-object p0

    check-cast p0, LX/AeB;

    iput-object v1, p1, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterFragment;->w:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteHelper;

    iput-object p0, p1, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterFragment;->x:LX/AeB;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1696726
    invoke-super {p0, p1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(Landroid/os/Bundle;)V

    .line 1696727
    const-class v0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterFragment;

    invoke-static {v0, p0}, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1696728
    iget-object v0, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterFragment;->x:LX/AeB;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterFragment;->z:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/AeB;->a(Ljava/lang/String;)V

    .line 1696729
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->j:LX/8tB;

    instance-of v0, v0, LX/BWn;

    if-eqz v0, :cond_0

    .line 1696730
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->j:LX/8tB;

    check-cast v0, LX/BWn;

    const/4 v1, 0x1

    .line 1696731
    iput-boolean v1, v0, LX/BWn;->j:Z

    .line 1696732
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1696744
    iget-object v0, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterFragment;->x:LX/AeB;

    invoke-virtual {v0, p1}, LX/AeB;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 1696738
    sget-object v0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterFragment;->u:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1696739
    const v0, 0x7f080c30

    .line 1696740
    :goto_0
    return v0

    .line 1696741
    :cond_0
    sget-object v0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterFragment;->v:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1696742
    const v0, 0x7f080c31

    goto :goto_0

    .line 1696743
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x5433699b

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1696735
    invoke-super {p0, p1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1696736
    iget-object v1, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    iget-object v2, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getImeOptions()I

    move-result v2

    const v3, -0x2000001

    and-int/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setImeOptions(I)V

    .line 1696737
    const/16 v1, 0x2b

    const v2, 0x7500c2ff

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final p()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 1696734
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->c:LX/0TD;

    new-instance v1, LX/Ae8;

    invoke-direct {v1, p0}, LX/Ae8;-><init>(Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterFragment;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final r()LX/2RS;
    .locals 1

    .prologue
    .line 1696733
    sget-object v0, LX/2RS;->COMMUNICATION_RANK:LX/2RS;

    return-object v0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 1696725
    const/4 v0, 0x1

    return v0
.end method

.method public final u()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1696724
    sget-object v0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterFragment;->u:Ljava/lang/String;

    sget-object v1, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterFragment;->v:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final y()V
    .locals 3

    .prologue
    .line 1696717
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->o()LX/0Px;

    move-result-object v0

    .line 1696718
    iget-object v1, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterFragment;->w:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteHelper;

    iget-object v2, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterFragment;->z:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoInviteHelper;->a(Ljava/lang/String;LX/0Px;)V

    .line 1696719
    iget-object v1, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterFragment;->x:LX/AeB;

    invoke-virtual {v1, v0}, LX/AeB;->a(Ljava/util/List;)V

    .line 1696720
    iget-object v0, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterFragment;->y:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;

    if-eqz v0, :cond_0

    .line 1696721
    iget-object v0, p0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterFragment;->y:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;

    .line 1696722
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1696723
    :cond_0
    return-void
.end method
