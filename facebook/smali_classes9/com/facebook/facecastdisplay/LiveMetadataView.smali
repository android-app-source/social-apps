.class public Lcom/facebook/facecastdisplay/LiveMetadataView;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final m:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public j:LX/1xc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/1xv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/1xa;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final o:Landroid/widget/TextView;

.field private final p:Landroid/widget/TextView;

.field private final q:Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;

.field private final r:Landroid/text/TextPaint;

.field private s:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1692877
    const-class v0, Lcom/facebook/facecastdisplay/LiveMetadataView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/facecastdisplay/LiveMetadataView;->m:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1692930
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/LiveMetadataView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1692931
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1692928
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/LiveMetadataView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1692929
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1692918
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1692919
    const-class v0, Lcom/facebook/facecastdisplay/LiveMetadataView;

    invoke-static {v0, p0}, Lcom/facebook/facecastdisplay/LiveMetadataView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1692920
    const v0, 0x7f030a1c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1692921
    const v0, 0x7f0d1993

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1692922
    const v0, 0x7f0d1994

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->o:Landroid/widget/TextView;

    .line 1692923
    const v0, 0x7f0d1995

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->p:Landroid/widget/TextView;

    .line 1692924
    const v0, 0x7f0d1996

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->q:Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;

    .line 1692925
    new-instance v0, Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->r:Landroid/text/TextPaint;

    .line 1692926
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->r:Landroid/text/TextPaint;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b004e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 1692927
    return-void
.end method

.method private static a(Lcom/facebook/facecastdisplay/LiveMetadataView;LX/1xc;LX/1xv;LX/1xa;)V
    .locals 0

    .prologue
    .line 1692917
    iput-object p1, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->j:LX/1xc;

    iput-object p2, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->k:LX/1xv;

    iput-object p3, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->l:LX/1xa;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/facecastdisplay/LiveMetadataView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/facecastdisplay/LiveMetadataView;

    invoke-static {v2}, LX/1xc;->a(LX/0QB;)LX/1xc;

    move-result-object v0

    check-cast v0, LX/1xc;

    invoke-static {v2}, LX/1xv;->a(LX/0QB;)LX/1xv;

    move-result-object v1

    check-cast v1, LX/1xv;

    invoke-static {v2}, LX/1xa;->a(LX/0QB;)LX/1xa;

    move-result-object v2

    check-cast v2, LX/1xa;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/facecastdisplay/LiveMetadataView;->a(Lcom/facebook/facecastdisplay/LiveMetadataView;LX/1xc;LX/1xv;LX/1xa;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLActor;ZLjava/lang/String;)V
    .locals 6
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1692912
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->q:Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->aF()Z

    move-result v1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v2

    const-string v3, "BROADCASTER_CONTEXT"

    const-string v4, "BROADCASTER_CONTEXT"

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1692913
    if-eqz p2, :cond_0

    .line 1692914
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->q:Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;

    const-string v1, "BROADCASTER_CONTEXT"

    invoke-virtual {v0, p1, v1}, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->a(Lcom/facebook/graphql/model/GraphQLActor;Ljava/lang/String;)V

    .line 1692915
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->q:Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->setVisibility(I)V

    .line 1692916
    return-void
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 1692932
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->s:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v0, :cond_0

    .line 1692933
    :goto_0
    return-void

    .line 1692934
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->l:LX/1xa;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->s:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->r:Landroid/text/TextPaint;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/1xa;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/text/TextPaint;I)LX/1z4;

    move-result-object v0

    .line 1692935
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->p:Landroid/widget/TextView;

    iget-object v0, v0, LX/1z4;->b:Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1692910
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->q:Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->setVisibility(I)V

    .line 1692911
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 1692908
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->q:Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;

    invoke-virtual {v0}, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->a()V

    .line 1692909
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1692905
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 1692906
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 1692907
    return-void
.end method

.method public setProfilePicture(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1692900
    if-eqz p1, :cond_0

    .line 1692901
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1692902
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/facecastdisplay/LiveMetadataView;->m:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1692903
    :goto_0
    return-void

    .line 1692904
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setStoryProps(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1692891
    iput-object p1, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->s:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1692892
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1692893
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1692894
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->k:LX/1xv;

    invoke-virtual {v1, p1}, LX/1xv;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1y2;

    move-result-object v1

    invoke-virtual {v1}, LX/1y2;->a()LX/1y2;

    move-result-object v1

    invoke-virtual {v1}, LX/1y2;->c()LX/1y2;

    move-result-object v1

    invoke-virtual {v1}, LX/1y2;->d()Landroid/text/Spannable;

    move-result-object v1

    .line 1692895
    invoke-static {v0}, LX/1xc;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->j:LX/1xc;

    invoke-virtual {v2, v0, v1}, LX/1xc;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1692896
    :goto_0
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->o:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1692897
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/LiveMetadataView;->d()V

    .line 1692898
    return-void

    :cond_0
    move-object v0, v1

    .line 1692899
    goto :goto_0
.end method

.method public setSubtitle(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1692886
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1692887
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->p:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1692888
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->p:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1692889
    :goto_0
    return-void

    .line 1692890
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->p:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setTextColor(I)V
    .locals 1

    .prologue
    .line 1692883
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->o:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1692884
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->p:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1692885
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1692878
    if-eqz p1, :cond_0

    .line 1692879
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->o:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1692880
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->o:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1692881
    :goto_0
    return-void

    .line 1692882
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveMetadataView;->o:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
