.class public Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:F

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1692096
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1692097
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1692094
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1692095
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1692098
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1692099
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;->a:F

    .line 1692100
    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 1692078
    iget v1, p0, Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;->a:F

    const/4 v2, 0x0

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    .line 1692079
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 1692080
    :goto_0
    return-void

    .line 1692081
    :cond_0
    invoke-static {v0, p1}, Landroid/view/View;->getDefaultSize(II)I

    move-result v3

    move v1, v0

    move v2, v0

    .line 1692082
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1692083
    invoke-virtual {p0, v1}, Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1692084
    int-to-float v0, v3

    iget v5, p0, Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;->a:F

    mul-float/2addr v0, v5

    float-to-int v0, v0

    iget v5, p0, Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;->b:I

    add-int/2addr v5, v0

    .line 1692085
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1692086
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;->getPaddingTop()I

    move-result v6

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;->getPaddingBottom()I

    move-result v7

    add-int/2addr v6, v7

    iget v7, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v6, v7

    iget v7, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v6, v7

    .line 1692087
    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v5, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 1692088
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {p2, v6, v0}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v0

    .line 1692089
    invoke-virtual {v4, v5, v0}, Landroid/view/View;->measure(II)V

    .line 1692090
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, v6

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 1692091
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1692092
    :cond_1
    invoke-static {v2, p2}, Landroid/view/View;->resolveSize(II)I

    move-result v0

    .line 1692093
    invoke-virtual {p0, v3, v0}, Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public setExtraWidth(I)V
    .locals 1

    .prologue
    .line 1692074
    iget v0, p0, Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;->b:I

    if-ne v0, p1, :cond_0

    .line 1692075
    :goto_0
    return-void

    .line 1692076
    :cond_0
    iput p1, p0, Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;->b:I

    .line 1692077
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;->requestLayout()V

    goto :goto_0
.end method

.method public setPercent(F)V
    .locals 1

    .prologue
    .line 1692070
    iget v0, p0, Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;->a:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_0

    .line 1692071
    :goto_0
    return-void

    .line 1692072
    :cond_0
    iput p1, p0, Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;->a:F

    .line 1692073
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;->requestLayout()V

    goto :goto_0
.end method
