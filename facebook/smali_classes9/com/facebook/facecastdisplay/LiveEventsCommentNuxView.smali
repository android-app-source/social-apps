.class public Lcom/facebook/facecastdisplay/LiveEventsCommentNuxView;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""


# instance fields
.field private a:Landroid/content/res/ColorStateList;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1692336
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/LiveEventsCommentNuxView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1692337
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1692338
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/LiveEventsCommentNuxView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1692339
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1692340
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1692341
    sget-object v0, LX/03r;->GlyphColorizer:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1692342
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1692343
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/facebook/facecastdisplay/LiveEventsCommentNuxView;->setGlyphColor(Landroid/content/res/ColorStateList;)V

    .line 1692344
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1692345
    return-void
.end method

.method private setGlyphColor(Landroid/content/res/ColorStateList;)V
    .locals 0

    .prologue
    .line 1692346
    iput-object p1, p0, Lcom/facebook/facecastdisplay/LiveEventsCommentNuxView;->a:Landroid/content/res/ColorStateList;

    .line 1692347
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/LiveEventsCommentNuxView;->refreshDrawableState()V

    .line 1692348
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/LiveEventsCommentNuxView;->invalidate()V

    .line 1692349
    return-void
.end method


# virtual methods
.method public final a(LX/Abz;Z)V
    .locals 2

    .prologue
    .line 1692350
    sget-object v0, LX/AcG;->a:[I

    invoke-virtual {p1}, LX/Abz;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1692351
    :goto_0
    return-void

    .line 1692352
    :pswitch_0
    const v0, 0x7f080c09

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/LiveEventsCommentNuxView;->setText(I)V

    goto :goto_0

    .line 1692353
    :pswitch_1
    const v0, 0x7f080c0a

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/LiveEventsCommentNuxView;->setText(I)V

    goto :goto_0

    .line 1692354
    :pswitch_2
    if-eqz p2, :cond_0

    const v0, 0x7f080c0b

    :goto_1
    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/LiveEventsCommentNuxView;->setText(I)V

    goto :goto_0

    :cond_0
    const v0, 0x7f080c0c

    goto :goto_1

    .line 1692355
    :pswitch_3
    const v0, 0x7f080c0d

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/LiveEventsCommentNuxView;->setText(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final drawableStateChanged()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1692356
    invoke-super {p0}, Lcom/facebook/resources/ui/FbTextView;->drawableStateChanged()V

    .line 1692357
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/LiveEventsCommentNuxView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1692358
    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 1692359
    if-eqz v4, :cond_0

    iget-object v5, p0, Lcom/facebook/facecastdisplay/LiveEventsCommentNuxView;->a:Landroid/content/res/ColorStateList;

    if-eqz v5, :cond_0

    .line 1692360
    iget-object v5, p0, Lcom/facebook/facecastdisplay/LiveEventsCommentNuxView;->a:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/LiveEventsCommentNuxView;->getDrawableState()[I

    move-result-object v6

    invoke-virtual {v5, v6, v1}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v5

    .line 1692361
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    new-instance v6, Landroid/graphics/PorterDuffColorFilter;

    sget-object v7, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v6, v5, v7}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v4, v6}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 1692362
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1692363
    :cond_1
    aget-object v0, v2, v1

    const/4 v1, 0x1

    aget-object v1, v2, v1

    const/4 v3, 0x2

    aget-object v3, v2, v3

    const/4 v4, 0x3

    aget-object v2, v2, v4

    invoke-virtual {p0, v0, v1, v3, v2}, Lcom/facebook/facecastdisplay/LiveEventsCommentNuxView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1692364
    return-void
.end method
