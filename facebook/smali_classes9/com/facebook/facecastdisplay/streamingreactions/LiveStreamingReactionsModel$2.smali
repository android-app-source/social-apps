.class public final Lcom/facebook/facecastdisplay/streamingreactions/LiveStreamingReactionsModel$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/AgH;


# direct methods
.method public constructor <init>(LX/AgH;)V
    .locals 0

    .prologue
    .line 1700443
    iput-object p1, p0, Lcom/facebook/facecastdisplay/streamingreactions/LiveStreamingReactionsModel$2;->a:LX/AgH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1700444
    iget-object v1, p0, Lcom/facebook/facecastdisplay/streamingreactions/LiveStreamingReactionsModel$2;->a:LX/AgH;

    monitor-enter v1

    .line 1700445
    :try_start_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/LiveStreamingReactionsModel$2;->a:LX/AgH;

    iget-object v2, p0, Lcom/facebook/facecastdisplay/streamingreactions/LiveStreamingReactionsModel$2;->a:LX/AgH;

    iget-object v2, v2, LX/AgH;->g:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    .line 1700446
    iput-wide v2, v0, LX/AgH;->w:J

    .line 1700447
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/LiveStreamingReactionsModel$2;->a:LX/AgH;

    iget-object v0, v0, LX/AgH;->q:LX/AgW;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/LiveStreamingReactionsModel$2;->a:LX/AgH;

    iget-boolean v0, v0, LX/AgH;->t:Z

    if-eqz v0, :cond_0

    .line 1700448
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/LiveStreamingReactionsModel$2;->a:LX/AgH;

    iget-object v0, v0, LX/AgH;->q:LX/AgW;

    iget-object v2, p0, Lcom/facebook/facecastdisplay/streamingreactions/LiveStreamingReactionsModel$2;->a:LX/AgH;

    iget-object v2, v2, LX/AgH;->o:Landroid/util/SparseArray;

    invoke-interface {v0, v2}, LX/AgW;->a(Landroid/util/SparseArray;)V

    .line 1700449
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/LiveStreamingReactionsModel$2;->a:LX/AgH;

    iget-object v0, v0, LX/AgH;->o:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 1700450
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/LiveStreamingReactionsModel$2;->a:LX/AgH;

    const/4 v2, 0x0

    .line 1700451
    iput-boolean v2, v0, LX/AgH;->y:Z

    .line 1700452
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
