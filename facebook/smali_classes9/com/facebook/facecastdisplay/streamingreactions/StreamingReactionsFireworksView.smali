.class public Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;
.super Landroid/view/View;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final d:Ljava/lang/String;

.field private static final e:Lcom/facebook/common/callercontext/CallerContext;

.field private static final f:Landroid/graphics/Rect;


# instance fields
.field public a:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/3Rb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/AgN;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final g:I

.field public final h:I

.field private final i:I

.field private final j:I

.field private final k:Landroid/view/animation/Interpolator;

.field private final l:Landroid/view/animation/Interpolator;

.field private final m:Landroid/view/animation/Interpolator;

.field private final n:LX/4Ac;

.field private final o:LX/1Uo;

.field private final p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/AgR;",
            ">;"
        }
    .end annotation
.end field

.field private final q:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/ref/SoftReference",
            "<",
            "LX/AgR;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1700883
    const-class v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->d:Ljava/lang/String;

    .line 1700884
    const-class v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 1700885
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->f:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1700881
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1700882
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1700879
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1700880
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 1700857
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1700858
    const-class v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;

    invoke-static {v0, p0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1700859
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0474

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->g:I

    .line 1700860
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0475

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->h:I

    .line 1700861
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0476

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->i:I

    .line 1700862
    iget v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->i:I

    iget v1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->h:I

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->j:I

    .line 1700863
    iget v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->j:I

    iget v1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->j:I

    invoke-virtual {p0, v0, v1, v2, v2}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->setPadding(IIII)V

    .line 1700864
    const v0, 0x3d99999a    # 0.075f

    const v1, 0x3f51eb85    # 0.82f

    const v2, 0x3e28f5c3    # 0.165f

    invoke-static {v0, v1, v2, v4}, LX/2pJ;->a(FFFF)Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->k:Landroid/view/animation/Interpolator;

    .line 1700865
    const v0, 0x3e2e147b    # 0.17f

    const v1, 0x3f63d70a    # 0.89f

    const v2, 0x3f07ae14    # 0.53f

    const v3, 0x3fc28f5c    # 1.52f

    invoke-static {v0, v1, v2, v3}, LX/2pJ;->a(FFFF)Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->l:Landroid/view/animation/Interpolator;

    .line 1700866
    const v0, 0x3e428f5c    # 0.19f

    const v1, 0x3e6147ae    # 0.22f

    invoke-static {v0, v4, v1, v4}, LX/2pJ;->a(FFFF)Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->m:Landroid/view/animation/Interpolator;

    .line 1700867
    new-instance v0, LX/4Ac;

    invoke-direct {v0}, LX/4Ac;-><init>()V

    iput-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->n:LX/4Ac;

    .line 1700868
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, LX/1Uo;->a(Landroid/content/res/Resources;)LX/1Uo;

    move-result-object v0

    invoke-static {}, LX/4Ab;->e()LX/4Ab;

    move-result-object v1

    .line 1700869
    iput-object v1, v0, LX/1Uo;->u:LX/4Ab;

    .line 1700870
    move-object v0, v0

    .line 1700871
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const v2, 0x7f0a00fb

    invoke-static {p1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 1700872
    iput-object v1, v0, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 1700873
    move-object v0, v0

    .line 1700874
    iput-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->o:LX/1Uo;

    .line 1700875
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->a:LX/1Ad;

    sget-object v1, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    .line 1700876
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->p:Ljava/util/List;

    .line 1700877
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->q:Ljava/util/Queue;

    .line 1700878
    return-void
.end method

.method private static a(Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;LX/1Ad;LX/3Rb;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;",
            "LX/1Ad;",
            "LX/3Rb;",
            "LX/0Or",
            "<",
            "LX/AgN;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1700856
    iput-object p1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->a:LX/1Ad;

    iput-object p2, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->b:LX/3Rb;

    iput-object p3, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->c:LX/0Or;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;

    invoke-static {v2}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-static {v2}, LX/3Rb;->a(LX/0QB;)LX/3Rb;

    move-result-object v1

    check-cast v1, LX/3Rb;

    const/16 v3, 0x1c1b

    invoke-static {v2, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->a(Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;LX/1Ad;LX/3Rb;LX/0Or;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;LX/AgR;)V
    .locals 2

    .prologue
    .line 1700761
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->p:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1700762
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->q:Ljava/util/Queue;

    new-instance v1, Ljava/lang/ref/SoftReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 1700763
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1700849
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AgR;

    .line 1700850
    iget-object v0, v0, LX/AgR;->c:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    goto :goto_0

    .line 1700851
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1700852
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->q:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 1700853
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->n:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->c()V

    .line 1700854
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->invalidate()V

    .line 1700855
    return-void
.end method

.method public final a(Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 1700844
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->getPaddingTop()I

    move-result v0

    iget v1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->h:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 1700845
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->h:I

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 1700846
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->getPaddingLeft()I

    move-result v0

    iget v1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->h:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 1700847
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->h:I

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 1700848
    return-void
.end method

.method public final a(Ljava/lang/String;IIIZJLX/AgS;)V
    .locals 12
    .param p8    # LX/AgS;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1700791
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->getHeight()I

    move-result v2

    iget v3, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->g:I

    div-int/lit8 v3, v3, 0x2

    add-int v4, v2, v3

    .line 1700792
    iget-object v2, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->q:Ljava/util/Queue;

    invoke-static {v2}, LX/AgX;->a(Ljava/util/Queue;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/AgR;

    .line 1700793
    if-eqz v2, :cond_0

    .line 1700794
    :goto_0
    iget-object v3, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->b:LX/3Rb;

    iget v5, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->h:I

    iget v6, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->h:I

    invoke-virtual {v3, p1, v5, v6}, LX/3Rb;->a(Ljava/lang/String;II)Landroid/net/Uri;

    move-result-object v3

    .line 1700795
    iget-object v5, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->a:LX/1Ad;

    invoke-virtual {v5, v3}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v5

    if-eqz p5, :cond_2

    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v5, v3}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    .line 1700796
    iget-object v3, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->a:LX/1Ad;

    invoke-virtual {v3}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v3

    .line 1700797
    iget-object v5, v2, LX/AgR;->b:LX/AgN;

    move/from16 v0, p4

    invoke-virtual {v5, v0}, LX/AgN;->a(I)V

    .line 1700798
    iget-object v5, v2, LX/AgR;->b:LX/AgN;

    iget v6, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->i:I

    mul-int/lit8 v6, v6, 0x2

    invoke-static {v5, p2, p3, v6}, LX/AgX;->a(Landroid/graphics/drawable/Drawable;III)V

    .line 1700799
    iget-object v5, v2, LX/AgR;->a:LX/1aX;

    invoke-virtual {v5}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iget v6, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->g:I

    invoke-static {v5, p2, p3, v6}, LX/AgX;->a(Landroid/graphics/drawable/Drawable;III)V

    .line 1700800
    move-object/from16 v0, p8

    invoke-virtual {v2, v0}, LX/AgR;->a(LX/AgS;)V

    .line 1700801
    invoke-virtual {v2}, LX/AgR;->d()V

    .line 1700802
    invoke-virtual {v2, v4}, LX/AgL;->setTranslationY(I)V

    .line 1700803
    iget-object v4, v2, LX/AgR;->a:LX/1aX;

    invoke-virtual {v4, v3}, LX/1aX;->a(LX/1aZ;)V

    .line 1700804
    if-eqz p5, :cond_3

    .line 1700805
    iget-object v3, v2, LX/AgR;->c:Landroid/animation/AnimatorSet;

    const-wide/16 v4, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    .line 1700806
    iget-object v3, v2, LX/AgR;->c:Landroid/animation/AnimatorSet;

    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->start()V

    .line 1700807
    :goto_2
    iget-object v3, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->p:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1700808
    :goto_3
    return-void

    .line 1700809
    :cond_0
    new-instance v5, LX/1aX;

    iget-object v2, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->o:LX/1Uo;

    invoke-virtual {v2}, LX/1Uo;->u()LX/1af;

    move-result-object v2

    invoke-direct {v5, v2}, LX/1aX;-><init>(LX/1aY;)V

    .line 1700810
    iget-object v2, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->n:LX/4Ac;

    invoke-virtual {v2, v5}, LX/4Ac;->a(LX/1aX;)V

    .line 1700811
    invoke-virtual {v5}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1700812
    if-nez v2, :cond_1

    .line 1700813
    sget-object v2, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->d:Ljava/lang/String;

    const-string v3, "Top level drawable was null when adding reaction"

    invoke-static {v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 1700814
    :cond_1
    invoke-virtual {v2, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1700815
    iget-object v2, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->c:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/AgN;

    .line 1700816
    invoke-virtual {v2, p0}, LX/AgN;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1700817
    new-instance v6, Landroid/animation/AnimatorSet;

    invoke-direct {v6}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1700818
    new-instance v3, LX/AgR;

    invoke-direct {v3, p0, v5, v2, v6}, LX/AgR;-><init>(Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;LX/1aX;LX/AgN;Landroid/animation/AnimatorSet;)V

    .line 1700819
    new-instance v2, Landroid/animation/ObjectAnimator;

    invoke-direct {v2}, Landroid/animation/ObjectAnimator;-><init>()V

    .line 1700820
    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 1700821
    const-string v5, "translationY"

    invoke-virtual {v2, v5}, Landroid/animation/ObjectAnimator;->setPropertyName(Ljava/lang/String;)V

    .line 1700822
    const/4 v5, 0x2

    new-array v5, v5, [I

    const/4 v7, 0x0

    aput v4, v5, v7

    const/4 v7, 0x1

    const/4 v8, 0x0

    aput v8, v5, v7

    invoke-virtual {v2, v5}, Landroid/animation/ObjectAnimator;->setIntValues([I)V

    .line 1700823
    const-wide/16 v8, 0x1f4

    invoke-virtual {v2, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1700824
    iget-object v5, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->k:Landroid/view/animation/Interpolator;

    invoke-virtual {v2, v5}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1700825
    new-instance v5, Landroid/animation/ObjectAnimator;

    invoke-direct {v5}, Landroid/animation/ObjectAnimator;-><init>()V

    .line 1700826
    invoke-virtual {v5, v3}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 1700827
    const-string v7, "size"

    invoke-virtual {v5, v7}, Landroid/animation/ObjectAnimator;->setPropertyName(Ljava/lang/String;)V

    .line 1700828
    const/4 v7, 0x2

    new-array v7, v7, [I

    const/4 v8, 0x0

    iget v9, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->g:I

    aput v9, v7, v8

    const/4 v8, 0x1

    iget v9, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->h:I

    aput v9, v7, v8

    invoke-virtual {v5, v7}, Landroid/animation/ObjectAnimator;->setIntValues([I)V

    .line 1700829
    const-wide/16 v8, 0x6e

    invoke-virtual {v5, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1700830
    const-wide/16 v8, 0x0

    invoke-virtual {v5, v8, v9}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 1700831
    iget-object v7, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->l:Landroid/view/animation/Interpolator;

    invoke-virtual {v5, v7}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1700832
    new-instance v7, LX/AgO;

    invoke-direct {v7, p0, v3}, LX/AgO;-><init>(Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;LX/AgR;)V

    invoke-virtual {v5, v7}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1700833
    new-instance v7, Landroid/animation/ObjectAnimator;

    invoke-direct {v7}, Landroid/animation/ObjectAnimator;-><init>()V

    .line 1700834
    invoke-virtual {v7, v3}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 1700835
    const-string v8, "size"

    invoke-virtual {v7, v8}, Landroid/animation/ObjectAnimator;->setPropertyName(Ljava/lang/String;)V

    .line 1700836
    const/4 v8, 0x2

    new-array v8, v8, [I

    const/4 v9, 0x0

    iget v10, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->h:I

    aput v10, v8, v9

    const/4 v9, 0x1

    const/4 v10, 0x0

    aput v10, v8, v9

    invoke-virtual {v7, v8}, Landroid/animation/ObjectAnimator;->setIntValues([I)V

    .line 1700837
    const-wide/16 v8, 0x12c

    invoke-virtual {v7, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1700838
    const-wide/16 v8, 0x320

    invoke-virtual {v7, v8, v9}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 1700839
    iget-object v8, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->m:Landroid/view/animation/Interpolator;

    invoke-virtual {v7, v8}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1700840
    new-instance v8, LX/AgP;

    invoke-direct {v8, p0, v3}, LX/AgP;-><init>(Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;LX/AgR;)V

    invoke-virtual {v7, v8}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1700841
    const/4 v8, 0x3

    new-array v8, v8, [Landroid/animation/Animator;

    const/4 v9, 0x0

    aput-object v2, v8, v9

    const/4 v2, 0x1

    aput-object v5, v8, v2

    const/4 v2, 0x2

    aput-object v7, v8, v2

    invoke-virtual {v6, v8}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    move-object v2, v3

    goto/16 :goto_0

    .line 1700842
    :cond_2
    iget-object v3, v2, LX/AgR;->d:LX/1Ai;

    goto/16 :goto_1

    .line 1700843
    :cond_3
    iget-object v3, v2, LX/AgR;->c:Landroid/animation/AnimatorSet;

    move-wide/from16 v0, p6

    invoke-virtual {v3, v0, v1}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    goto/16 :goto_2
.end method

.method public getAvatarSize()I
    .locals 1

    .prologue
    .line 1700790
    iget v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->h:I

    return v0
.end method

.method public getCurrentNumberOfFireworks()I
    .locals 1

    .prologue
    .line 1700789
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x4430d801

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1700786
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 1700787
    iget-object v1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->n:LX/4Ac;

    invoke-virtual {v1}, LX/4Ac;->a()V

    .line 1700788
    const/16 v1, 0x2d

    const v2, 0x61d4b3a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x490232d3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1700783
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 1700784
    iget-object v1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->n:LX/4Ac;

    invoke-virtual {v1}, LX/4Ac;->b()V

    .line 1700785
    const/16 v1, 0x2d

    const v2, 0x4c7ea2b8    # 6.67512E7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 1700778
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1700779
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AgR;

    .line 1700780
    iget-object v2, v0, LX/AgR;->b:LX/AgN;

    invoke-virtual {v2, p1}, LX/AgN;->draw(Landroid/graphics/Canvas;)V

    .line 1700781
    iget-object v0, v0, LX/AgR;->a:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 1700782
    :cond_0
    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 1700775
    invoke-super {p0}, Landroid/view/View;->onFinishTemporaryDetach()V

    .line 1700776
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->n:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->a()V

    .line 1700777
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 1700772
    invoke-super {p0}, Landroid/view/View;->onStartTemporaryDetach()V

    .line 1700773
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->n:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->b()V

    .line 1700774
    return-void
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1700764
    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1700765
    :goto_0
    return v0

    .line 1700766
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AgR;

    .line 1700767
    iget-object v3, v0, LX/AgR;->b:LX/AgN;

    if-ne v3, p1, :cond_2

    move v0, v1

    .line 1700768
    goto :goto_0

    .line 1700769
    :cond_2
    iget-object v0, v0, LX/AgR;->a:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-ne v0, p1, :cond_1

    move v0, v1

    .line 1700770
    goto :goto_0

    .line 1700771
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
