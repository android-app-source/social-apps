.class public Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public final a:I

.field public final b:I

.field private final c:I

.field private final d:Landroid/graphics/Paint;

.field public e:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1700988
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1700989
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1700986
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1700987
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 1700976
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1700977
    invoke-virtual {p0, v2}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->setOrientation(I)V

    .line 1700978
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b047a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->a:I

    .line 1700979
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b047b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->b:I

    .line 1700980
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b047c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->c:I

    .line 1700981
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->d:Landroid/graphics/Paint;

    .line 1700982
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1700983
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->d:Landroid/graphics/Paint;

    const v1, 0x7f0a00d2

    invoke-static {p1, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1700984
    invoke-virtual {p0, v2}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->setWillNotDraw(Z)V

    .line 1700985
    return-void
.end method


# virtual methods
.method public final bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 1700975
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public final generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;
    .locals 1

    .prologue
    .line 1700974
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedReaction()Landroid/view/View;
    .locals 1

    .prologue
    .line 1700973
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->e:Landroid/view/View;

    return-object v0
.end method

.method public getVisibleWidth()I
    .locals 4

    .prologue
    .line 1700965
    iget v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->a:I

    int-to-double v0, v0

    const-wide/high16 v2, 0x3ff8000000000000L    # 1.5

    mul-double/2addr v0, v2

    iget v2, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->b:I

    int-to-double v2, v2

    add-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 1700969
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 1700970
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->e:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1700971
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->e:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    add-float/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->c:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1700972
    :cond_0
    return-void
.end method

.method public setSelectedReaction(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1700966
    iput-object p1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->e:Landroid/view/View;

    .line 1700967
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->invalidate()V

    .line 1700968
    return-void
.end method
