.class public Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/AgJ;
.implements LX/AgW;


# static fields
.field private static final g:[Ljava/lang/String;


# instance fields
.field private final A:Landroid/view/animation/AccelerateDecelerateInterpolator;

.field private final B:Landroid/view/animation/LinearInterpolator;

.field private final C:Landroid/graphics/Rect;

.field public D:Z

.field private E:LX/AgG;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private F:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private G:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private H:Z

.field private I:I

.field public J:Z

.field public a:LX/1zf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AgH;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Agc;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/AgK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:F

.field private final n:F

.field private final o:I

.field private final p:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;

.field private final q:Landroid/graphics/Paint;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final r:I

.field private final s:I

.field private final t:I

.field private final u:Ljava/util/Random;

.field private final v:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1zt;",
            ">;"
        }
    .end annotation
.end field

.field private final w:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Aga;",
            ">;"
        }
    .end annotation
.end field

.field private final x:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/ref/SoftReference",
            "<",
            "LX/Aga;",
            ">;>;"
        }
    .end annotation
.end field

.field private final y:Landroid/view/animation/Interpolator;

.field private final z:Landroid/view/animation/AccelerateInterpolator;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1701144
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "1055790620"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "655436164"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "630379738"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "100001500408498"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "567878735"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "1940736"

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->g:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1701145
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1701146
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1701147
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1701148
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/16 v4, 0xa

    const/high16 v3, 0x3f800000    # 1.0f

    .line 1701149
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1701150
    const-class v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    invoke-static {v0, p0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1701151
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->e:LX/0ad;

    sget v1, LX/AgC;->g:I

    invoke-interface {v0, v1, v4}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->h:I

    .line 1701152
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->e:LX/0ad;

    sget v1, LX/AgC;->f:I

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->i:I

    .line 1701153
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->e:LX/0ad;

    sget v1, LX/AgC;->b:I

    const/16 v2, 0xf

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->j:I

    .line 1701154
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->e:LX/0ad;

    sget v1, LX/AgC;->a:I

    const/16 v2, 0x28

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->k:I

    .line 1701155
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->e:LX/0ad;

    sget v1, LX/AgC;->i:I

    const/16 v2, 0x64

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->l:I

    .line 1701156
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->e:LX/0ad;

    sget v1, LX/AgC;->j:F

    const v2, 0x3f333333    # 0.7f

    invoke-interface {v0, v1, v2}, LX/0ad;->a(FF)F

    move-result v0

    iput v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->m:F

    .line 1701157
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->e:LX/0ad;

    sget v1, LX/AgC;->k:F

    const/high16 v2, 0x3f000000    # 0.5f

    invoke-interface {v0, v1, v2}, LX/0ad;->a(FF)F

    move-result v0

    iput v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->n:F

    .line 1701158
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->e:LX/0ad;

    sget v1, LX/AgC;->d:I

    invoke-interface {v0, v1, v4}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->o:I

    .line 1701159
    const v0, 0x7f031402

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1701160
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0473

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->r:I

    .line 1701161
    iget v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->r:I

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->setMinimumHeight(I)V

    .line 1701162
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->setWillNotDraw(Z)V

    .line 1701163
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->q:Landroid/graphics/Paint;

    .line 1701164
    const v0, 0x7f0d2e04

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->p:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;

    .line 1701165
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->u:Ljava/util/Random;

    .line 1701166
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->a:LX/1zf;

    invoke-virtual {v0}, LX/1zf;->d()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->v:Ljava/util/List;

    .line 1701167
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0479

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->s:I

    .line 1701168
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0478

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->t:I

    .line 1701169
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->w:Ljava/util/List;

    .line 1701170
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->x:Ljava/util/Queue;

    .line 1701171
    const v0, 0x3e428f5c    # 0.19f

    const v1, 0x3e6147ae    # 0.22f

    invoke-static {v0, v3, v1, v3}, LX/2pJ;->a(FFFF)Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->y:Landroid/view/animation/Interpolator;

    .line 1701172
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    iput-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->z:Landroid/view/animation/AccelerateInterpolator;

    .line 1701173
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    iput-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->A:Landroid/view/animation/AccelerateDecelerateInterpolator;

    .line 1701174
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    iput-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->B:Landroid/view/animation/LinearInterpolator;

    .line 1701175
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->C:Landroid/graphics/Rect;

    .line 1701176
    return-void
.end method

.method private a(DD)D
    .locals 5

    .prologue
    .line 1701177
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->u:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextDouble()D

    move-result-wide v0

    sub-double v2, p3, p1

    mul-double/2addr v0, v2

    add-double/2addr v0, p1

    return-wide v0
.end method

.method private a(JJ)J
    .locals 7

    .prologue
    .line 1701097
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->u:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextDouble()D

    move-result-wide v0

    sub-long v2, p3, p1

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    long-to-double v2, v2

    mul-double/2addr v0, v2

    long-to-double v2, p1

    add-double/2addr v0, v2

    double-to-long v0, v0

    return-wide v0
.end method

.method private a(IIILandroid/graphics/Rect;)LX/Aga;
    .locals 24

    .prologue
    .line 1701178
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->E:LX/AgG;

    invoke-static {v4}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1701179
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->x:Ljava/util/Queue;

    invoke-static {v4}, LX/AgX;->a(Ljava/util/Queue;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Aga;

    .line 1701180
    if-eqz v4, :cond_0

    .line 1701181
    :goto_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->a:LX/1zf;

    move/from16 v0, p1

    invoke-virtual {v5, v0}, LX/1zf;->a(I)LX/1zt;

    move-result-object v5

    invoke-virtual {v5}, LX/1zt;->h()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iput-object v5, v4, LX/Aga;->j:Landroid/graphics/drawable/Drawable;

    .line 1701182
    iget-object v5, v4, LX/Aga;->j:Landroid/graphics/drawable/Drawable;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move/from16 v0, p2

    move/from16 v1, p2

    invoke-virtual {v5, v6, v7, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1701183
    const/4 v5, 0x0

    iput-boolean v5, v4, LX/Aga;->i:Z

    .line 1701184
    const/16 v5, 0xff

    invoke-virtual {v4, v5}, LX/AgL;->setAlpha(I)V

    .line 1701185
    invoke-virtual {v4}, LX/AgL;->ik_()V

    .line 1701186
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->E:LX/AgG;

    invoke-interface {v5}, LX/AgG;->c()D

    move-result-wide v6

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->l:I

    int-to-double v8, v5

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->min(DD)D

    move-result-wide v6

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->l:I

    int-to-double v8, v5

    div-double/2addr v6, v8

    .line 1701187
    move-object/from16 v0, p4

    iget v5, v0, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p4

    iget v8, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v8}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->b(II)I

    move-result v5

    int-to-double v8, v5

    .line 1701188
    move/from16 v0, p2

    neg-int v5, v0

    int-to-double v10, v5

    .line 1701189
    const-wide/16 v12, 0xb54

    const-wide/16 v14, 0xfa0

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13, v14, v15}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->a(JJ)J

    move-result-wide v12

    .line 1701190
    long-to-double v12, v12

    const-wide/high16 v14, 0x3ff0000000000000L    # 1.0

    const/high16 v5, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->m:F

    move/from16 v16, v0

    sub-float v5, v5, v16

    float-to-double v0, v5

    move-wide/from16 v16, v0

    mul-double v16, v16, v6

    sub-double v14, v14, v16

    mul-double/2addr v12, v14

    double-to-long v12, v12

    .line 1701191
    iget-object v5, v4, LX/Aga;->b:Landroid/animation/ObjectAnimator;

    const/4 v14, 0x2

    new-array v14, v14, [I

    const/4 v15, 0x0

    double-to-int v0, v8

    move/from16 v16, v0

    aput v16, v14, v15

    const/4 v15, 0x1

    double-to-int v10, v10

    aput v10, v14, v15

    invoke-virtual {v5, v14}, Landroid/animation/ObjectAnimator;->setIntValues([I)V

    .line 1701192
    iget-object v5, v4, LX/Aga;->b:Landroid/animation/ObjectAnimator;

    invoke-virtual {v5, v12, v13}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1701193
    double-to-int v5, v8

    invoke-virtual {v4, v5}, LX/AgL;->setTranslationX(I)V

    .line 1701194
    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->r:I

    int-to-double v8, v5

    const-wide v10, 0x3fa999999999999aL    # 0.05

    const-wide v14, 0x3fc999999999999aL    # 0.2

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11, v14, v15}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->a(DD)D

    move-result-wide v10

    mul-double/2addr v8, v10

    .line 1701195
    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    const/high16 v5, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v14, v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->n:F

    sub-float/2addr v5, v14

    float-to-double v14, v5

    mul-double/2addr v6, v14

    sub-double v6, v10, v6

    mul-double v14, v8, v6

    .line 1701196
    const-wide/16 v6, 0x5dc

    const-wide/16 v8, 0xbb8

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7, v8, v9}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->a(JJ)J

    move-result-wide v16

    .line 1701197
    move-object/from16 v0, p4

    iget v5, v0, Landroid/graphics/Rect;->top:I

    int-to-double v6, v5

    add-double/2addr v6, v14

    move-object/from16 v0, p4

    iget v5, v0, Landroid/graphics/Rect;->bottom:I

    int-to-double v8, v5

    sub-double/2addr v8, v14

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7, v8, v9}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->a(DD)D

    move-result-wide v6

    .line 1701198
    div-int/lit8 v5, p2, 0x2

    int-to-double v8, v5

    sub-double/2addr v6, v8

    .line 1701199
    add-double v8, v6, v14

    .line 1701200
    sub-double v10, v6, v14

    .line 1701201
    neg-double v0, v14

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-direct {v0, v1, v2, v14, v15}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->a(DD)D

    move-result-wide v18

    .line 1701202
    add-double v18, v18, v6

    .line 1701203
    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v6}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->b(II)I

    move-result v5

    if-nez v5, :cond_1

    const/4 v5, 0x1

    .line 1701204
    :goto_1
    if-eqz v5, :cond_2

    move-wide v6, v8

    .line 1701205
    :goto_2
    sub-double v20, v6, v18

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->abs(D)D

    move-result-wide v20

    const-wide/high16 v22, 0x4000000000000000L    # 2.0

    mul-double v14, v14, v22

    div-double v14, v20, v14

    .line 1701206
    move-wide/from16 v0, v16

    long-to-double v0, v0

    move-wide/from16 v20, v0

    const-wide/high16 v22, 0x3fe8000000000000L    # 0.75

    move-wide/from16 v0, v22

    invoke-static {v14, v15, v0, v1}, Ljava/lang/Math;->max(DD)D

    move-result-wide v14

    mul-double v14, v14, v20

    double-to-long v14, v14

    .line 1701207
    if-eqz v5, :cond_3

    .line 1701208
    :goto_3
    iget-object v5, v4, LX/Aga;->c:Landroid/animation/ObjectAnimator;

    const/4 v8, 0x2

    new-array v8, v8, [I

    const/4 v9, 0x0

    move-wide/from16 v0, v18

    double-to-int v0, v0

    move/from16 v20, v0

    aput v20, v8, v9

    const/4 v9, 0x1

    double-to-int v0, v6

    move/from16 v20, v0

    aput v20, v8, v9

    invoke-virtual {v5, v8}, Landroid/animation/ObjectAnimator;->setIntValues([I)V

    .line 1701209
    iget-object v5, v4, LX/Aga;->c:Landroid/animation/ObjectAnimator;

    invoke-virtual {v5, v14, v15}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1701210
    iget-object v5, v4, LX/Aga;->c:Landroid/animation/ObjectAnimator;

    long-to-double v8, v14

    const-wide v14, 0x3fc999999999999aL    # 0.2

    mul-double/2addr v8, v14

    double-to-long v8, v8

    invoke-virtual {v5, v8, v9}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 1701211
    move-wide/from16 v0, v18

    double-to-int v5, v0

    invoke-virtual {v4, v5}, LX/AgL;->setTranslationY(I)V

    .line 1701212
    iget-object v5, v4, LX/Aga;->d:Landroid/animation/ObjectAnimator;

    const/4 v8, 0x2

    new-array v8, v8, [I

    const/4 v9, 0x0

    double-to-int v6, v6

    aput v6, v8, v9

    const/4 v6, 0x1

    double-to-int v7, v10

    aput v7, v8, v6

    invoke-virtual {v5, v8}, Landroid/animation/ObjectAnimator;->setIntValues([I)V

    .line 1701213
    iget-object v5, v4, LX/Aga;->d:Landroid/animation/ObjectAnimator;

    move-wide/from16 v0, v16

    invoke-virtual {v5, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1701214
    long-to-double v6, v12

    const-wide v8, 0x3fc3333333333333L    # 0.15

    mul-double/2addr v6, v8

    double-to-long v6, v6

    .line 1701215
    sub-long v8, v12, v6

    .line 1701216
    move/from16 v0, p3

    int-to-float v5, v0

    move/from16 v0, p2

    int-to-float v10, v0

    div-float/2addr v5, v10

    .line 1701217
    float-to-double v10, v5

    const-wide v14, 0x3fd3333333333333L    # 0.3

    mul-double/2addr v10, v14

    double-to-float v10, v10

    .line 1701218
    const-string v11, "scale"

    const/4 v14, 0x2

    new-array v14, v14, [F

    const/4 v15, 0x0

    aput v5, v14, v15

    const/4 v15, 0x1

    aput v10, v14, v15

    invoke-static {v11, v14}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v10

    .line 1701219
    iget-object v11, v4, LX/Aga;->f:Landroid/animation/ObjectAnimator;

    invoke-virtual {v11, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1701220
    iget-object v6, v4, LX/Aga;->f:Landroid/animation/ObjectAnimator;

    invoke-virtual {v6, v8, v9}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 1701221
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->D:Z

    if-eqz v6, :cond_4

    .line 1701222
    const-string v6, "alpha"

    const/4 v7, 0x2

    new-array v7, v7, [I

    fill-array-data v7, :array_0

    invoke-static {v6, v7}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v6

    .line 1701223
    iget-object v7, v4, LX/Aga;->f:Landroid/animation/ObjectAnimator;

    const/4 v8, 0x2

    new-array v8, v8, [Landroid/animation/PropertyValuesHolder;

    const/4 v9, 0x0

    aput-object v10, v8, v9

    const/4 v9, 0x1

    aput-object v6, v8, v9

    invoke-virtual {v7, v8}, Landroid/animation/ObjectAnimator;->setValues([Landroid/animation/PropertyValuesHolder;)V

    .line 1701224
    :goto_4
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->E:LX/AgG;

    invoke-interface {v7}, LX/AgG;->d()I

    move-result v7

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->b(II)I

    move-result v6

    int-to-long v6, v6

    .line 1701225
    iget-object v8, v4, LX/Aga;->a:Landroid/animation/AnimatorSet;

    invoke-virtual {v8, v6, v7}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    .line 1701226
    long-to-double v6, v12

    const-wide v8, 0x3fd51eb851eb851fL    # 0.33

    mul-double/2addr v6, v8

    double-to-long v6, v6

    .line 1701227
    iget-object v8, v4, LX/Aga;->h:Landroid/animation/ObjectAnimator;

    const/4 v9, 0x2

    new-array v9, v9, [F

    const/4 v10, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    aput v11, v9, v10

    const/4 v10, 0x1

    aput v5, v9, v10

    invoke-virtual {v8, v9}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    .line 1701228
    iget-object v5, v4, LX/Aga;->h:Landroid/animation/ObjectAnimator;

    invoke-virtual {v5, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1701229
    return-object v4

    .line 1701230
    :cond_0
    new-instance v6, Landroid/animation/ObjectAnimator;

    invoke-direct {v6}, Landroid/animation/ObjectAnimator;-><init>()V

    .line 1701231
    const-string v4, "translationX"

    invoke-virtual {v6, v4}, Landroid/animation/ObjectAnimator;->setPropertyName(Ljava/lang/String;)V

    .line 1701232
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->z:Landroid/view/animation/AccelerateInterpolator;

    invoke-virtual {v6, v4}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1701233
    new-instance v7, Landroid/animation/ObjectAnimator;

    invoke-direct {v7}, Landroid/animation/ObjectAnimator;-><init>()V

    .line 1701234
    const-string v4, "translationY"

    invoke-virtual {v7, v4}, Landroid/animation/ObjectAnimator;->setPropertyName(Ljava/lang/String;)V

    .line 1701235
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->A:Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-virtual {v7, v4}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1701236
    new-instance v8, Landroid/animation/ObjectAnimator;

    invoke-direct {v8}, Landroid/animation/ObjectAnimator;-><init>()V

    .line 1701237
    const-string v4, "translationY"

    invoke-virtual {v8, v4}, Landroid/animation/ObjectAnimator;->setPropertyName(Ljava/lang/String;)V

    .line 1701238
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->A:Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-virtual {v8, v4}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1701239
    const/4 v4, 0x2

    invoke-virtual {v8, v4}, Landroid/animation/ObjectAnimator;->setRepeatMode(I)V

    .line 1701240
    const/4 v4, -0x1

    invoke-virtual {v8, v4}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    .line 1701241
    new-instance v9, Landroid/animation/AnimatorSet;

    invoke-direct {v9}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1701242
    const/4 v4, 0x2

    new-array v4, v4, [Landroid/animation/Animator;

    const/4 v5, 0x0

    aput-object v7, v4, v5

    const/4 v5, 0x1

    aput-object v8, v4, v5

    invoke-virtual {v9, v4}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    .line 1701243
    new-instance v10, Landroid/animation/ObjectAnimator;

    invoke-direct {v10}, Landroid/animation/ObjectAnimator;-><init>()V

    .line 1701244
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->B:Landroid/view/animation/LinearInterpolator;

    invoke-virtual {v10, v4}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1701245
    new-instance v4, Landroid/animation/AnimatorSet;

    invoke-direct {v4}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1701246
    const/4 v5, 0x3

    new-array v5, v5, [Landroid/animation/Animator;

    const/4 v11, 0x0

    aput-object v6, v5, v11

    const/4 v11, 0x1

    aput-object v9, v5, v11

    const/4 v11, 0x2

    aput-object v10, v5, v11

    invoke-virtual {v4, v5}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 1701247
    new-instance v11, Landroid/animation/AnimatorSet;

    invoke-direct {v11}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1701248
    invoke-virtual {v11, v4}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 1701249
    new-instance v14, Landroid/animation/ObjectAnimator;

    invoke-direct {v14}, Landroid/animation/ObjectAnimator;-><init>()V

    .line 1701250
    const-string v5, "scale"

    invoke-virtual {v14, v5}, Landroid/animation/ObjectAnimator;->setPropertyName(Ljava/lang/String;)V

    .line 1701251
    const/4 v5, 0x2

    new-array v5, v5, [F

    fill-array-data v5, :array_1

    invoke-virtual {v14, v5}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    .line 1701252
    const-wide/16 v12, 0x12c

    invoke-virtual {v14, v12, v13}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1701253
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->y:Landroid/view/animation/Interpolator;

    invoke-virtual {v14, v5}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1701254
    new-instance v13, Landroid/animation/ObjectAnimator;

    invoke-direct {v13}, Landroid/animation/ObjectAnimator;-><init>()V

    .line 1701255
    const-string v5, "scale"

    invoke-virtual {v13, v5}, Landroid/animation/ObjectAnimator;->setPropertyName(Ljava/lang/String;)V

    .line 1701256
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->B:Landroid/view/animation/LinearInterpolator;

    invoke-virtual {v13, v5}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1701257
    new-instance v5, Landroid/animation/AnimatorSet;

    invoke-direct {v5}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1701258
    const/4 v12, 0x2

    new-array v12, v12, [Landroid/animation/Animator;

    const/4 v15, 0x0

    aput-object v13, v12, v15

    const/4 v15, 0x1

    aput-object v4, v12, v15

    invoke-virtual {v5, v12}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 1701259
    const-wide/16 v16, 0x1f4

    move-wide/from16 v0, v16

    invoke-virtual {v5, v0, v1}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    .line 1701260
    new-instance v12, Landroid/animation/AnimatorSet;

    invoke-direct {v12}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1701261
    const/4 v4, 0x2

    new-array v4, v4, [Landroid/animation/Animator;

    const/4 v15, 0x0

    aput-object v14, v4, v15

    const/4 v15, 0x1

    aput-object v5, v4, v15

    invoke-virtual {v12, v4}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    .line 1701262
    new-instance v4, LX/Aga;

    move-object/from16 v5, p0

    invoke-direct/range {v4 .. v13}, LX/Aga;-><init>(Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;Landroid/animation/ObjectAnimator;Landroid/animation/ObjectAnimator;Landroid/animation/ObjectAnimator;Landroid/animation/AnimatorSet;Landroid/animation/ObjectAnimator;Landroid/animation/AnimatorSet;Landroid/animation/AnimatorSet;Landroid/animation/ObjectAnimator;)V

    .line 1701263
    invoke-virtual {v6, v4}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 1701264
    invoke-virtual {v7, v4}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 1701265
    invoke-virtual {v8, v4}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 1701266
    invoke-virtual {v10, v4}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 1701267
    invoke-virtual {v14, v4}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 1701268
    invoke-virtual {v13, v4}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 1701269
    new-instance v5, LX/AgZ;

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v4}, LX/AgZ;-><init>(Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;LX/Aga;)V

    invoke-virtual {v6, v5}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    goto/16 :goto_0

    .line 1701270
    :cond_1
    const/4 v5, 0x0

    goto/16 :goto_1

    :cond_2
    move-wide v6, v10

    .line 1701271
    goto/16 :goto_2

    :cond_3
    move-wide v10, v8

    .line 1701272
    goto/16 :goto_3

    .line 1701273
    :cond_4
    iget-object v6, v4, LX/Aga;->f:Landroid/animation/ObjectAnimator;

    const/4 v7, 0x1

    new-array v7, v7, [Landroid/animation/PropertyValuesHolder;

    const/4 v8, 0x0

    aput-object v10, v7, v8

    invoke-virtual {v6, v7}, Landroid/animation/ObjectAnimator;->setValues([Landroid/animation/PropertyValuesHolder;)V

    goto/16 :goto_4

    .line 1701274
    :array_0
    .array-data 4
        0xff
        0x0
    .end array-data

    .line 1701275
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private a(II)V
    .locals 6

    .prologue
    .line 1701276
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->getWidth()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->getHeight()I

    move-result v0

    if-nez v0, :cond_1

    .line 1701277
    :cond_0
    :goto_0
    return-void

    .line 1701278
    :cond_1
    iget v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->I:I

    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->I:I

    .line 1701279
    add-int/lit8 v0, p2, -0x1

    int-to-double v0, v0

    iget v2, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->I:I

    int-to-double v2, v2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v2, v4

    div-double/2addr v0, v2

    .line 1701280
    iget v2, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->t:I

    iget v3, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->s:I

    sub-int/2addr v2, v3

    int-to-double v2, v2

    mul-double/2addr v0, v2

    iget v2, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->s:I

    int-to-double v2, v2

    add-double/2addr v0, v2

    double-to-int v0, v0

    .line 1701281
    iget-object v1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->C:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->getHeight()I

    move-result v2

    iget v3, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->r:I

    sub-int/2addr v2, v3

    div-int/lit8 v3, v0, 0x2

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 1701282
    iget-object v1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->C:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->getHeight()I

    move-result v2

    div-int/lit8 v3, v0, 0x2

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 1701283
    iget-object v1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->C:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->getWidth()I

    move-result v2

    div-int/lit8 v3, v0, 0x2

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 1701284
    iget-object v1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->C:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->getWidth()I

    move-result v2

    div-int/lit8 v3, v0, 0x2

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 1701285
    iget-object v1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->C:Landroid/graphics/Rect;

    invoke-direct {p0, p1, v0, v0, v1}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->a(IIILandroid/graphics/Rect;)LX/Aga;

    move-result-object v0

    .line 1701286
    iget-object v1, v0, LX/Aga;->a:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->start()V

    .line 1701287
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/Aga;->i:Z

    .line 1701288
    iget-object v1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->w:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private a(ILjava/lang/String;Z)V
    .locals 9

    .prologue
    .line 1701310
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->E:LX/AgG;

    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1701311
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->getWidth()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->getHeight()I

    move-result v0

    if-nez v0, :cond_1

    .line 1701312
    :cond_0
    :goto_0
    return-void

    .line 1701313
    :cond_1
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->p:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;

    .line 1701314
    iget v1, v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->h:I

    move v0, v1

    .line 1701315
    iget-object v1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->p:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;

    iget-object v2, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->C:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->a(Landroid/graphics/Rect;)V

    .line 1701316
    iget-object v1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->C:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->p:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;

    invoke-virtual {v3}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->getLeft()I

    move-result v3

    div-int/lit8 v4, v0, 0x2

    sub-int/2addr v3, v4

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 1701317
    iget-object v1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->C:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->p:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;

    invoke-virtual {v3}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->getLeft()I

    move-result v3

    div-int/lit8 v4, v0, 0x2

    sub-int/2addr v3, v4

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 1701318
    iget-object v1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->C:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->p:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;

    invoke-virtual {v3}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->getTop()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 1701319
    iget-object v1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->C:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->bottom:I

    iget-object v3, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->p:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;

    invoke-virtual {v3}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->getTop()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 1701320
    iget v1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->s:I

    iget-object v2, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->C:Landroid/graphics/Rect;

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->a(IIILandroid/graphics/Rect;)LX/Aga;

    move-result-object v8

    .line 1701321
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->w:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1701322
    iget-object v0, v8, LX/Aga;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    .line 1701323
    iget-object v1, v8, LX/Aga;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    .line 1701324
    iget-object v2, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->p:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;

    invoke-virtual {v2}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->getLeft()I

    move-result v2

    sub-int v2, v0, v2

    .line 1701325
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->p:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->getTop()I

    move-result v0

    sub-int v3, v1, v0

    .line 1701326
    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->H:Z

    if-eqz v0, :cond_2

    const-wide/16 v6, 0x0

    .line 1701327
    :goto_1
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->p:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->a:LX/1zf;

    invoke-virtual {v1, p1}, LX/1zf;->a(I)LX/1zt;

    move-result-object v1

    .line 1701328
    iget v4, v1, LX/1zt;->g:I

    move v4, v4

    .line 1701329
    move-object v1, p2

    move v5, p3

    invoke-virtual/range {v0 .. v8}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->a(Ljava/lang/String;IIIZJLX/AgS;)V

    goto/16 :goto_0

    .line 1701330
    :cond_2
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->E:LX/AgG;

    invoke-interface {v1}, LX/AgG;->d()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->b(II)I

    move-result v0

    int-to-long v6, v0

    goto :goto_1
.end method

.method private static a(Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;LX/1zf;LX/0Ot;LX/0Ot;Ljava/lang/String;LX/0ad;LX/AgK;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;",
            "LX/1zf;",
            "LX/0Ot",
            "<",
            "LX/AgH;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Agc;",
            ">;",
            "Ljava/lang/String;",
            "LX/0ad;",
            "LX/AgK;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1701289
    iput-object p1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->a:LX/1zf;

    iput-object p2, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->b:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->c:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->d:Ljava/lang/String;

    iput-object p5, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->e:LX/0ad;

    iput-object p6, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->f:LX/AgK;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    invoke-static {v6}, LX/1zf;->a(LX/0QB;)LX/1zf;

    move-result-object v1

    check-cast v1, LX/1zf;

    const/16 v2, 0x1c19

    invoke-static {v6, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x1c1e

    invoke-static {v6, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {v6}, LX/1si;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v6}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {v6}, LX/AgK;->a(LX/0QB;)LX/AgK;

    move-result-object v6

    check-cast v6, LX/AgK;

    invoke-static/range {v0 .. v6}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->a(Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;LX/1zf;LX/0Ot;LX/0Ot;Ljava/lang/String;LX/0ad;LX/AgK;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;LX/Aga;)V
    .locals 2

    .prologue
    .line 1701290
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->w:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1701291
    iget-object v0, p1, LX/Aga;->e:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    .line 1701292
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->x:Ljava/util/Queue;

    new-instance v1, Ljava/lang/ref/SoftReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 1701293
    return-void
.end method

.method private b(II)I
    .locals 4

    .prologue
    .line 1701294
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->u:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextDouble()D

    move-result-wide v0

    sub-int v2, p2, p1

    add-int/lit8 v2, v2, 0x1

    int-to-double v2, v2

    mul-double/2addr v0, v2

    int-to-double v2, p1

    add-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method private getRandomReactionIndex()I
    .locals 2

    .prologue
    .line 1701295
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->u:Ljava/util/Random;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->v:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    .line 1701296
    iget-object v1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->v:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1zt;

    .line 1701297
    iget v1, v0, LX/1zt;->e:I

    move v0, v1

    .line 1701298
    return v0
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1701299
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Aga;

    .line 1701300
    const-string v2, "alpha"

    const/4 v3, 0x2

    new-array v3, v3, [I

    .line 1701301
    iget v4, v0, LX/AgL;->f:I

    move v4, v4

    .line 1701302
    aput v4, v3, v6

    const/4 v4, 0x1

    aput v6, v3, v4

    invoke-static {v0, v2, v3}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v2

    const-wide/16 v4, 0xc8

    invoke-virtual {v2, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 1701303
    new-instance v3, LX/AgY;

    invoke-direct {v3, p0, v0}, LX/AgY;-><init>(Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;LX/Aga;)V

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1701304
    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->start()V

    .line 1701305
    iget-object v2, v0, LX/Aga;->a:Landroid/animation/AnimatorSet;

    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->cancel()V

    .line 1701306
    iget-object v0, v0, LX/Aga;->g:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    goto :goto_0

    .line 1701307
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->x:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 1701308
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->p:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->a()V

    .line 1701309
    return-void
.end method

.method public final a(F)V
    .locals 0

    .prologue
    .line 1701139
    invoke-virtual {p0, p1}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->setAlpha(F)V

    .line 1701140
    return-void
.end method

.method public final a(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 1701141
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->p:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->getCurrentNumberOfFireworks()I

    move-result v0

    iget v1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->o:I

    if-ge v0, v1, :cond_0

    .line 1701142
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->a(ILjava/lang/String;Z)V

    .line 1701143
    :cond_0
    return-void
.end method

.method public final a(IZ)V
    .locals 2

    .prologue
    .line 1701026
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->d:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->a(ILjava/lang/String;Z)V

    .line 1701027
    if-eqz p2, :cond_0

    .line 1701028
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->E:LX/AgG;

    if-eqz v0, :cond_0

    .line 1701029
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->E:LX/AgG;

    invoke-interface {v0, p1}, LX/AgG;->a(I)V

    .line 1701030
    :cond_0
    return-void
.end method

.method public final a(Landroid/util/SparseArray;)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1701031
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->E:LX/AgG;

    invoke-static {v2}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1701032
    invoke-static/range {p1 .. p1}, LX/2y6;->a(Landroid/util/SparseArray;)[I

    move-result-object v7

    .line 1701033
    const/4 v3, 0x0

    .line 1701034
    array-length v5, v7

    const/4 v2, 0x0

    move v6, v3

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_0

    aget v2, v7, v3

    .line 1701035
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int v4, v6, v2

    .line 1701036
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v6, v4

    goto :goto_0

    .line 1701037
    :cond_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->k:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->w:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    sub-int/2addr v2, v3

    .line 1701038
    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->j:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->E:LX/AgG;

    invoke-interface {v4}, LX/AgG;->d()I

    move-result v4

    mul-int/2addr v3, v4

    int-to-double v4, v3

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v4, v8

    const-wide v8, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v8

    double-to-int v3, v4

    .line 1701039
    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1701040
    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->i:I

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v8

    .line 1701041
    if-gt v6, v8, :cond_2

    .line 1701042
    array-length v4, v7

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_5

    aget v5, v7, v3

    .line 1701043
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 1701044
    const/4 v2, 0x0

    :goto_2
    if-ge v2, v6, :cond_1

    .line 1701045
    const/4 v8, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v8}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->a(II)V

    .line 1701046
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1701047
    :cond_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 1701048
    :cond_2
    array-length v9, v7

    const/4 v2, 0x0

    move v5, v2

    :goto_3
    if-ge v5, v9, :cond_5

    aget v10, v7, v5

    .line 1701049
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 1701050
    int-to-double v2, v4

    int-to-double v12, v6

    div-double/2addr v2, v12

    int-to-double v12, v8

    mul-double/2addr v2, v12

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v11, v2

    .line 1701051
    if-eqz v11, :cond_4

    .line 1701052
    add-int/lit8 v2, v11, 0x1

    mul-int/2addr v2, v11

    div-int/lit8 v12, v2, 0x2

    .line 1701053
    const/4 v2, 0x1

    move v3, v4

    :goto_4
    if-ge v2, v11, :cond_3

    .line 1701054
    const-wide/high16 v14, 0x3ff0000000000000L    # 1.0

    int-to-double v0, v12

    move-wide/from16 v16, v0

    div-double v14, v14, v16

    int-to-double v0, v4

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    double-to-int v13, v14

    .line 1701055
    sub-int/2addr v3, v13

    .line 1701056
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v13}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->a(II)V

    .line 1701057
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 1701058
    :cond_3
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v3}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->a(II)V

    .line 1701059
    :cond_4
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_3

    .line 1701060
    :cond_5
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1701061
    iput-object p1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->F:Ljava/lang/String;

    .line 1701062
    iput-object p2, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->G:Ljava/lang/String;

    .line 1701063
    iput-boolean p3, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->H:Z

    .line 1701064
    if-eqz p3, :cond_0

    .line 1701065
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AgG;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->E:LX/AgG;

    .line 1701066
    :goto_0
    return-void

    .line 1701067
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AgG;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->E:LX/AgG;

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1701068
    iput-boolean p1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->J:Z

    .line 1701069
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1701070
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->E:LX/AgG;

    if-eqz v0, :cond_0

    .line 1701071
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->E:LX/AgG;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->F:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->G:Ljava/lang/String;

    invoke-interface {v0, p0, v1, v2}, LX/AgG;->a(LX/AgW;Ljava/lang/String;Ljava/lang/String;)V

    .line 1701072
    :cond_0
    iget v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->h:I

    iput v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->I:I

    .line 1701073
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1701074
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->E:LX/AgG;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->E:LX/AgG;

    invoke-interface {v0}, LX/AgG;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1701075
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->E:LX/AgG;

    invoke-interface {v0}, LX/AgG;->a()V

    .line 1701076
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 1701077
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->F:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->E:LX/AgG;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->E:LX/AgG;

    invoke-interface {v0}, LX/AgG;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1701078
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->b()V

    .line 1701079
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 1701080
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->E:LX/AgG;

    if-eqz v0, :cond_0

    .line 1701081
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->E:LX/AgG;

    invoke-interface {v0}, LX/AgG;->a()V

    .line 1701082
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Aga;

    .line 1701083
    iget-object v2, v0, LX/Aga;->a:Landroid/animation/AnimatorSet;

    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->cancel()V

    .line 1701084
    iget-object v0, v0, LX/Aga;->g:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    goto :goto_0

    .line 1701085
    :cond_1
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1701086
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->x:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 1701087
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->p:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->a()V

    .line 1701088
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->J:Z

    .line 1701089
    return-void
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 1701090
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->E:LX/AgG;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->E:LX/AgG;

    invoke-interface {v0}, LX/AgG;->b()Z

    move-result v0

    goto :goto_0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0xc80ea35

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1701091
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 1701092
    iget-object v1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->f:LX/AgK;

    invoke-virtual {v1, p0}, LX/AgK;->a(LX/AgJ;)V

    .line 1701093
    const/16 v1, 0x2d

    const v2, 0x78af3771

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x6afcf28a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1701094
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 1701095
    iget-object v1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->f:LX/AgK;

    invoke-virtual {v1, p0}, LX/AgK;->b(LX/AgJ;)V

    .line 1701096
    const/16 v1, 0x2d

    const v2, 0x79d7215e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 1701098
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->q:Landroid/graphics/Paint;

    if-eqz v0, :cond_0

    .line 1701099
    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    iget v2, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->r:I

    sub-int/2addr v0, v2

    int-to-float v2, v0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->q:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1701100
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Aga;

    .line 1701101
    iget-boolean v2, v0, LX/Aga;->i:Z

    if-eqz v2, :cond_1

    .line 1701102
    invoke-virtual {v0}, LX/Aga;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 1701103
    :cond_2
    return-void
.end method

.method public final onMeasure(II)V
    .locals 6

    .prologue
    .line 1701104
    const-string v0, "StreamingReactionsView.onMeasure"

    const v1, 0x26c5d647

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1701105
    :try_start_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 1701106
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->getMeasuredWidth()I

    move-result v2

    .line 1701107
    int-to-double v0, v2

    const-wide v4, 0x3fd6666666666666L    # 0.35

    mul-double/2addr v0, v4

    double-to-int v0, v0

    .line 1701108
    iget-object v1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->p:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->getPaddingLeft()I

    move-result v1

    add-int/2addr v1, v0

    iget-object v3, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->p:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;

    invoke-virtual {v3}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->getPaddingRight()I

    move-result v3

    add-int/2addr v1, v3

    .line 1701109
    iget-object v3, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->p:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;

    invoke-virtual {v3}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->getPaddingTop()I

    move-result v3

    add-int/2addr v0, v3

    iget-object v3, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->p:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;

    invoke-virtual {v3}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->getPaddingBottom()I

    move-result v3

    add-int/2addr v0, v3

    .line 1701110
    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 1701111
    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1701112
    iget-object v3, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->p:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;

    invoke-virtual {v3, v1, v0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->measure(II)V

    .line 1701113
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->p:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1701114
    iget-object v1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->p:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->getMeasuredHeight()I

    move-result v1

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, v1

    .line 1701115
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 1701116
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 1701117
    sparse-switch v3, :sswitch_data_0

    .line 1701118
    :goto_0
    :sswitch_0
    invoke-virtual {p0, v2, v0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->setMeasuredDimension(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1701119
    const v0, -0x76c85ecf

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1701120
    return-void

    .line 1701121
    :sswitch_1
    :try_start_1
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    :sswitch_2
    move v0, v1

    .line 1701122
    goto :goto_0

    .line 1701123
    :catchall_0
    move-exception v0

    const v1, 0x3a2207cd

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_0
        0x40000000 -> :sswitch_2
    .end sparse-switch
.end method

.method public setFadeOutAtEnd(Z)V
    .locals 0

    .prologue
    .line 1701124
    iput-boolean p1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->D:Z

    .line 1701125
    return-void
.end method

.method public setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .prologue
    .line 1701126
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1701127
    instance-of v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_0

    .line 1701128
    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1701129
    iget v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    neg-int v0, v0

    .line 1701130
    if-lez v0, :cond_0

    .line 1701131
    iget-object v1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->p:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->getPaddingBottom()I

    move-result v1

    sub-int v1, v0, v1

    .line 1701132
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->p:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1701133
    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 1701134
    iget-object v1, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->p:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;

    invoke-virtual {v1, v0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsFireworksView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1701135
    :cond_0
    return-void
.end method

.method public setVideoTime(I)V
    .locals 1

    .prologue
    .line 1701136
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->E:LX/AgG;

    if-eqz v0, :cond_0

    .line 1701137
    iget-object v0, p0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->E:LX/AgG;

    invoke-interface {v0, p1}, LX/AgG;->b(I)V

    .line 1701138
    :cond_0
    return-void
.end method
