.class public final Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;)V
    .locals 0

    .prologue
    .line 1692256
    iput-object p1, p0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment$1;->a:Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 1692257
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment$1;->a:Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;

    .line 1692258
    const v1, 0x7f0d1962

    invoke-virtual {v0, v1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 1692259
    const p0, 0x7f0309fd

    invoke-virtual {v1, p0}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 1692260
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    iput-object v1, v0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->v:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    .line 1692261
    iget-object v1, v0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->m:LX/8nK;

    iget-object p0, v0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->t:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v1, p0}, LX/8nK;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1692262
    iget-object v1, v0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->v:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    iget-object p0, v0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->m:LX/8nK;

    invoke-virtual {v1, p0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->a(LX/8nB;)V

    .line 1692263
    iget-object v1, v0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->v:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    const/4 p0, 0x1

    invoke-virtual {v1, p0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->setIncludeFriends(Z)V

    .line 1692264
    iget-object v1, v0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->o:Landroid/widget/EditText;

    iget-object p0, v0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->s:Landroid/text/TextWatcher;

    invoke-virtual {v1, p0}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1692265
    iget-object v1, v0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->o:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iget-object p0, v0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->o:Landroid/widget/EditText;

    invoke-virtual {v1, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1692266
    iget-object v1, v0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->v:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    iput-object v1, v0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->o:Landroid/widget/EditText;

    .line 1692267
    iget-object v1, v0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->o:Landroid/widget/EditText;

    iget-object p0, v0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->s:Landroid/text/TextWatcher;

    invoke-virtual {v1, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1692268
    return-void
.end method
