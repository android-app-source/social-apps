.class public Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# static fields
.field public static final f:Ljava/lang/String;


# instance fields
.field public a:LX/0xX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/39G;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final g:Lcom/facebook/facecastdisplay/LiveMetadataView;

.field private final h:Lcom/facebook/widget/text/BetterTextView;

.field public final i:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

.field public j:I

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1693004
    const-class v0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1693002
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1693003
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1693000
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1693001
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1692990
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1692991
    const-class v0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

    invoke-static {v0, p0}, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1692992
    const v0, 0x7f030bf1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1692993
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->setOrientation(I)V

    .line 1692994
    const v0, 0x7f0d1da1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/LiveMetadataView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->g:Lcom/facebook/facecastdisplay/LiveMetadataView;

    .line 1692995
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->g:Lcom/facebook/facecastdisplay/LiveMetadataView;

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00a3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveMetadataView;->setTextColor(I)V

    .line 1692996
    const v0, 0x7f0d1da2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->h:Lcom/facebook/widget/text/BetterTextView;

    .line 1692997
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->h:Lcom/facebook/widget/text/BetterTextView;

    new-instance v1, Landroid/text/method/ScrollingMovementMethod;

    invoke-direct {v1}, Landroid/text/method/ScrollingMovementMethod;-><init>()V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1692998
    const v0, 0x7f0d1da3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->i:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    .line 1692999
    return-void
.end method

.method private static a(Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;LX/0xX;LX/39G;LX/0tX;Ljava/util/concurrent/ExecutorService;LX/03V;)V
    .locals 0

    .prologue
    .line 1692989
    iput-object p1, p0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->a:LX/0xX;

    iput-object p2, p0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->b:LX/39G;

    iput-object p3, p0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->c:LX/0tX;

    iput-object p4, p0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->d:Ljava/util/concurrent/ExecutorService;

    iput-object p5, p0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->e:LX/03V;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

    invoke-static {v5}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v1

    check-cast v1, LX/0xX;

    invoke-static {v5}, LX/39G;->a(LX/0QB;)LX/39G;

    move-result-object v2

    check-cast v2, LX/39G;

    invoke-static {v5}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v5}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {v5}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static/range {v0 .. v5}, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->a(Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;LX/0xX;LX/39G;LX/0tX;Ljava/util/concurrent/ExecutorService;LX/03V;)V

    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 1692982
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->k:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1692983
    :cond_0
    :goto_0
    return-void

    .line 1692984
    :cond_1
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1692985
    :cond_2
    new-instance v0, LX/6SE;

    invoke-direct {v0}, LX/6SE;-><init>()V

    move-object v0, v0

    .line 1692986
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->c:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1692987
    const-string v1, "targetID"

    iget-object v2, p0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1692988
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/Acb;

    invoke-direct {v1, p0}, LX/Acb;-><init>(Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;)V

    iget-object v2, p0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method private setFollowVideosButton(Lcom/facebook/graphql/model/GraphQLMedia;)V
    .locals 4

    .prologue
    .line 1692950
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->aK()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    .line 1692951
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aB()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1692952
    :cond_0
    :goto_0
    return-void

    .line 1692953
    :cond_1
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->g:Lcom/facebook/facecastdisplay/LiveMetadataView;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/facebook/facecastdisplay/LiveMetadataView;->a(Lcom/facebook/graphql/model/GraphQLActor;ZLjava/lang/String;)V

    .line 1692954
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->g:Lcom/facebook/facecastdisplay/LiveMetadataView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/LiveMetadataView;->f()V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1692979
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->g:Lcom/facebook/facecastdisplay/LiveMetadataView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/LiveMetadataView;->d()V

    .line 1692980
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->c()V

    .line 1692981
    return-void
.end method

.method public final c(I)V
    .locals 2

    .prologue
    const/4 v0, 0x2

    .line 1692976
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->h:Lcom/facebook/widget/text/BetterTextView;

    if-ne p1, v0, :cond_0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setMaxLines(I)V

    .line 1692977
    return-void

    .line 1692978
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setMetadata(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1692955
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1692956
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1692957
    invoke-static {v0}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    .line 1692958
    iget-object v2, p0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->g:Lcom/facebook/facecastdisplay/LiveMetadataView;

    invoke-virtual {v2, p1}, Lcom/facebook/facecastdisplay/LiveMetadataView;->setStoryProps(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1692959
    iget-object v2, p0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->g:Lcom/facebook/facecastdisplay/LiveMetadataView;

    if-eqz v1, :cond_1

    invoke-static {v1}, LX/1xl;->b(Lcom/facebook/graphql/model/GraphQLActor;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {v1}, LX/1xl;->c(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Lcom/facebook/facecastdisplay/LiveMetadataView;->setProfilePicture(Ljava/lang/String;)V

    .line 1692960
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->h:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {v0}, LX/1z5;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1692961
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->i:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setVisibility(I)V

    .line 1692962
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->g:Lcom/facebook/facecastdisplay/LiveMetadataView;

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/LiveMetadataView;->e()V

    .line 1692963
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 1692964
    if-nez v1, :cond_2

    .line 1692965
    :cond_0
    :goto_1
    return-void

    .line 1692966
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 1692967
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 1692968
    if-eqz v1, :cond_0

    .line 1692969
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->k:Ljava/lang/String;

    .line 1692970
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->ao()Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->au()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1692971
    invoke-static {v0}, LX/0sa;->e(Lcom/facebook/graphql/model/GraphQLStory;)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->j:I

    .line 1692972
    :goto_2
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->a:LX/0xX;

    invoke-virtual {v0}, LX/0xX;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1692973
    invoke-direct {p0, v1}, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->setFollowVideosButton(Lcom/facebook/graphql/model/GraphQLMedia;)V

    .line 1692974
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->a()V

    goto :goto_1

    .line 1692975
    :cond_4
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->j:I

    goto :goto_2
.end method
