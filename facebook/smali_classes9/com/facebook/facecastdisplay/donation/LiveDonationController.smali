.class public Lcom/facebook/facecastdisplay/donation/LiveDonationController;
.super LX/AVi;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/Acx;
.implements LX/Ad1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/AVi",
        "<",
        "Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "Lcom/facebook/facecastdisplay/donation/LiveDonationCampaignQueryHelper$FacecastFundraiserUpdatedListener;",
        "LX/Acx;",
        "LX/Ad1;",
        "Lcom/facebook/facecastdisplay/donation/LiveDonationFragment$LiveDonationFragmentListener;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/1b4;

.field public final c:LX/Acy;

.field public final d:LX/03V;

.field public final e:LX/17W;

.field public final f:LX/Acr;

.field public g:Z

.field public h:Z

.field private final i:Landroid/os/Handler;

.field private j:LX/AcX;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final p:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1693515
    const-class v0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1b4;LX/Acy;LX/03V;LX/17W;LX/Acr;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1693548
    invoke-direct {p0}, LX/AVi;-><init>()V

    .line 1693549
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->i:Landroid/os/Handler;

    .line 1693550
    new-instance v0, Lcom/facebook/facecastdisplay/donation/LiveDonationController$1;

    invoke-direct {v0, p0}, Lcom/facebook/facecastdisplay/donation/LiveDonationController$1;-><init>(Lcom/facebook/facecastdisplay/donation/LiveDonationController;)V

    iput-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->p:Ljava/lang/Runnable;

    .line 1693551
    iput-object p1, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->b:LX/1b4;

    .line 1693552
    iput-object p2, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->c:LX/Acy;

    .line 1693553
    iput-object p3, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->d:LX/03V;

    .line 1693554
    iput-object p4, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->e:LX/17W;

    .line 1693555
    iput-object p5, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->f:LX/Acr;

    .line 1693556
    return-void
.end method

.method public static a(Lcom/facebook/facecastdisplay/donation/LiveDonationController;Z)V
    .locals 9

    .prologue
    .line 1693536
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    if-nez v0, :cond_1

    .line 1693537
    :cond_0
    :goto_0
    return-void

    .line 1693538
    :cond_1
    iget-object v8, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->f:LX/Acr;

    new-instance v0, LX/Acq;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->u()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v2}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->p()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v2}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->p()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;->j()Ljava/lang/String;

    move-result-object v2

    :goto_1
    iget-object v3, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->n:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->o:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v5}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->f()I

    move-result v5

    invoke-static {v5}, LX/38I;->a(I)Ljava/lang/String;

    move-result-object v5

    iget-boolean v6, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->h:Z

    move v7, p1

    invoke-direct/range {v0 .. v7}, LX/Acq;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 1693539
    iput-object v0, v8, LX/Acr;->b:LX/Acq;

    .line 1693540
    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->g:Z

    if-nez v0, :cond_0

    .line 1693541
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->f:LX/Acr;

    .line 1693542
    iget-object v1, v0, LX/Acr;->a:LX/0if;

    sget-object v2, LX/0ig;->D:LX/0ih;

    invoke-virtual {v1, v2}, LX/0if;->a(LX/0ih;)V

    .line 1693543
    iget-object v1, v0, LX/Acr;->a:LX/0if;

    sget-object v2, LX/0ig;->D:LX/0ih;

    const-string v3, "start_session"

    const/4 v4, 0x0

    .line 1693544
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v5

    const-string v6, "fundraiser_id"

    iget-object v7, v0, LX/Acr;->b:LX/Acq;

    iget-object v7, v7, LX/Acq;->a:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v5

    const-string v6, "charity_id"

    iget-object v7, v0, LX/Acr;->b:LX/Acq;

    iget-object v7, v7, LX/Acq;->b:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v5

    const-string v6, "video_id"

    iget-object v7, v0, LX/Acr;->b:LX/Acq;

    iget-object v7, v7, LX/Acq;->c:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v5

    const-string v6, "broadcaster_id"

    iget-object v7, v0, LX/Acr;->b:LX/Acq;

    iget-object v7, v7, LX/Acq;->d:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v5

    const-string v6, "fundraiser_type"

    iget-object v7, v0, LX/Acr;->b:LX/Acq;

    iget-object v7, v7, LX/Acq;->e:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v5

    const-string v6, "was_live"

    iget-object v7, v0, LX/Acr;->b:LX/Acq;

    iget-boolean v7, v7, LX/Acq;->f:Z

    invoke-virtual {v5, v6, v7}, LX/1rQ;->a(Ljava/lang/String;Z)LX/1rQ;

    move-result-object v5

    const-string v6, "can_donate"

    iget-object v7, v0, LX/Acr;->b:LX/Acq;

    iget-boolean v7, v7, LX/Acq;->g:Z

    invoke-virtual {v5, v6, v7}, LX/1rQ;->a(Ljava/lang/String;Z)LX/1rQ;

    move-result-object v5

    move-object v5, v5

    .line 1693545
    invoke-virtual {v1, v2, v3, v4, v5}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1693546
    goto/16 :goto_0

    .line 1693547
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static a(Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1693527
    if-nez p1, :cond_0

    .line 1693528
    :goto_0
    return-void

    .line 1693529
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->m()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1693530
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080c45

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->l()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->m()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1693531
    iget-object v1, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->c:Lcom/facebook/resources/ui/FbTextView;

    move-object v1, v1

    .line 1693532
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1693533
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->x()D

    move-result-wide v0

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Integer;

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, LX/0yq;->a(Ljava/lang/Comparable;[Ljava/lang/Comparable;)Ljava/lang/Comparable;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1693534
    iget-object v1, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->d:Landroid/widget/ProgressBar;

    move-object v1, v1

    .line 1693535
    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)Lcom/facebook/facecastdisplay/donation/LiveDonationController;
    .locals 6

    .prologue
    .line 1693525
    new-instance v0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;

    invoke-static {p0}, LX/1b4;->b(LX/0QB;)LX/1b4;

    move-result-object v1

    check-cast v1, LX/1b4;

    invoke-static {p0}, LX/Acy;->b(LX/0QB;)LX/Acy;

    move-result-object v2

    check-cast v2, LX/Acy;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {p0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v4

    check-cast v4, LX/17W;

    invoke-static {p0}, LX/Acr;->a(LX/0QB;)LX/Acr;

    move-result-object v5

    check-cast v5, LX/Acr;

    invoke-direct/range {v0 .. v5}, Lcom/facebook/facecastdisplay/donation/LiveDonationController;-><init>(LX/1b4;LX/Acy;LX/03V;LX/17W;LX/Acr;)V

    .line 1693526
    return-object v0
.end method

.method public static b(Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;)V
    .locals 2

    .prologue
    .line 1693516
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->s()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1693517
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->b:Lcom/facebook/resources/ui/FbTextView;

    move-object v0, v0

    .line 1693518
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1693519
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->k()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$AllDonationsSummaryTextModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1693520
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->c:Lcom/facebook/resources/ui/FbTextView;

    move-object v0, v0

    .line 1693521
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->k()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$AllDonationsSummaryTextModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$AllDonationsSummaryTextModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1693522
    :cond_0
    return-void

    .line 1693523
    :cond_1
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->b:Lcom/facebook/resources/ui/FbTextView;

    move-object v0, v0

    .line 1693524
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public static c(Lcom/facebook/facecastdisplay/donation/LiveDonationController;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1693376
    new-instance v0, LX/0ju;

    invoke-direct {v0, p1}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 1693377
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080c4f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 1693378
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080c50

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 1693379
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080c52

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/Acz;

    invoke-direct {v2, p0, p1}, LX/Acz;-><init>(Lcom/facebook/facecastdisplay/donation/LiveDonationController;Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1693380
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080c51

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/Ad0;

    invoke-direct {v2, p0}, LX/Ad0;-><init>(Lcom/facebook/facecastdisplay/donation/LiveDonationController;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1693381
    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 1693382
    return-void
.end method

.method private h()V
    .locals 6

    .prologue
    .line 1693491
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1693492
    :cond_0
    :goto_0
    return-void

    .line 1693493
    :cond_1
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 1693494
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->d:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_illegalCampaignType"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fetched campaign model returned illegal type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->f()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1693495
    :sswitch_0
    invoke-static {p0}, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->j(Lcom/facebook/facecastdisplay/donation/LiveDonationController;)V

    goto :goto_0

    .line 1693496
    :sswitch_1
    const/4 v2, 0x0

    .line 1693497
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1693498
    check-cast v0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;

    .line 1693499
    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    if-nez v1, :cond_3

    .line 1693500
    :cond_2
    :goto_1
    goto :goto_0

    .line 1693501
    :cond_3
    iget-boolean v1, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->g:Z

    move v1, v1

    .line 1693502
    if-nez v1, :cond_4

    .line 1693503
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->a()V

    .line 1693504
    :cond_4
    iget-object v1, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->w()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    const/4 v1, 0x1

    .line 1693505
    :goto_2
    invoke-static {p0, v1}, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->a(Lcom/facebook/facecastdisplay/donation/LiveDonationController;Z)V

    .line 1693506
    iget-object v3, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-static {v0, v3}, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->b(Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;)V

    .line 1693507
    iget-object v3, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->p()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;

    move-result-object v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->p()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 1693508
    iget-object v3, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v3, v3

    .line 1693509
    iget-object v4, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v4}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->p()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;->m()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const-class v5, Lcom/facebook/facecastdisplay/donation/LiveDonationController;

    invoke-static {v5}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1693510
    :cond_5
    iget-boolean v3, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->g:Z

    if-nez v3, :cond_6

    if-nez v1, :cond_7

    .line 1693511
    :cond_6
    iget-object v1, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->e:Lcom/facebook/fig/button/FigButton;

    move-object v1, v1

    .line 1693512
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 1693513
    :cond_7
    invoke-virtual {v0, v2}, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->setVisibility(I)V

    goto :goto_1

    :cond_8
    move v1, v2

    .line 1693514
    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        -0x4e6785e3 -> :sswitch_0
        0x291507f7 -> :sswitch_1
    .end sparse-switch
.end method

.method public static j(Lcom/facebook/facecastdisplay/donation/LiveDonationController;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1693468
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1693469
    check-cast v0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;

    .line 1693470
    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    if-nez v3, :cond_1

    .line 1693471
    :cond_0
    :goto_0
    return-void

    .line 1693472
    :cond_1
    iget-boolean v3, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->g:Z

    move v3, v3

    .line 1693473
    if-nez v3, :cond_2

    .line 1693474
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->b()V

    .line 1693475
    :cond_2
    iget-object v3, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->o()Z

    move-result v3

    invoke-static {p0, v3}, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->a(Lcom/facebook/facecastdisplay/donation/LiveDonationController;Z)V

    .line 1693476
    iget-object v3, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->b:Lcom/facebook/resources/ui/FbTextView;

    move-object v3, v3

    .line 1693477
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080c43

    new-array v6, v1, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v7}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->p()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;->k()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1693478
    iget-object v3, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->v()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 1693479
    iget-object v5, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v5, v5

    .line 1693480
    invoke-virtual {v4, v3, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const-class v4, Lcom/facebook/facecastdisplay/donation/LiveDonationController;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    invoke-virtual {v5, v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1693481
    iget-object v3, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-static {v0, v3}, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->a(Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;)V

    .line 1693482
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v1, :cond_6

    :goto_1
    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->setClickable(Z)V

    .line 1693483
    iget-boolean v1, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->g:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->o()Z

    move-result v1

    if-nez v1, :cond_4

    .line 1693484
    :cond_3
    iget-object v1, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->e:Lcom/facebook/fig/button/FigButton;

    move-object v1, v1

    .line 1693485
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 1693486
    invoke-virtual {v0, v2}, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->setClickable(Z)V

    .line 1693487
    :cond_4
    iget-boolean v1, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->h:Z

    if-nez v1, :cond_5

    .line 1693488
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->k()V

    .line 1693489
    :cond_5
    invoke-virtual {v0, v2}, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_6
    move v1, v2

    .line 1693490
    goto :goto_1
.end method

.method private k()V
    .locals 5

    .prologue
    .line 1693557
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->i:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->p:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1693558
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->i:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->p:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7530

    const v4, -0x1205183f

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1693559
    return-void
.end method


# virtual methods
.method public final a(LX/AcX;)V
    .locals 0

    .prologue
    .line 1693466
    iput-object p1, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->j:LX/AcX;

    .line 1693467
    return-void
.end method

.method public final a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1693463
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->f:LX/Acr;

    invoke-virtual {v0}, LX/Acr;->b()V

    .line 1693464
    invoke-static {p0, p1}, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->c(Lcom/facebook/facecastdisplay/donation/LiveDonationController;Landroid/content/Context;)V

    .line 1693465
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1693454
    check-cast p1, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;

    check-cast p2, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;

    .line 1693455
    const/4 v0, 0x0

    .line 1693456
    iput-object v0, p2, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->h:LX/Ad1;

    .line 1693457
    iput-object p0, p1, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->h:LX/Ad1;

    .line 1693458
    invoke-virtual {p2}, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->getVisibility()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->setVisibility(I)V

    .line 1693459
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->m:Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->m:Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1693460
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->m:Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1693461
    :cond_0
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->h()V

    .line 1693462
    return-void
.end method

.method public final a(Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;)V
    .locals 0
    .param p1    # Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1693451
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->k()V

    .line 1693452
    invoke-virtual {p0, p1}, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->b(Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;)V

    .line 1693453
    return-void
.end method

.method public final a(Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1693444
    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->j:LX/AcX;

    if-eqz v0, :cond_0

    .line 1693445
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->j:LX/AcX;

    .line 1693446
    new-instance p2, LX/AfV;

    invoke-direct {p2, p3, p1}, LX/AfV;-><init>(Ljava/lang/String;Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;)V

    invoke-virtual {v0, p2}, LX/AcX;->a(LX/AeO;)V

    .line 1693447
    :cond_0
    if-nez p1, :cond_1

    .line 1693448
    :goto_0
    return-void

    .line 1693449
    :cond_1
    iput-object p1, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    .line 1693450
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->h()V

    goto :goto_0
.end method

.method public final b(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1693441
    check-cast p1, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;

    .line 1693442
    iput-object p0, p1, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->h:LX/Ad1;

    .line 1693443
    return-void
.end method

.method public final b(Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;)V
    .locals 3
    .param p1    # Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1693423
    if-eqz p1, :cond_0

    .line 1693424
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1693425
    check-cast v0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 1693426
    :cond_0
    :goto_0
    return-void

    .line 1693427
    :cond_1
    iput-object p1, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    .line 1693428
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-nez v0, :cond_3

    .line 1693429
    :cond_2
    :goto_1
    goto :goto_0

    .line 1693430
    :cond_3
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 1693431
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->d:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_illegalCampaignType"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string p1, "Fetched campaign model returned illegal type: "

    invoke-direct {v2, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->f()I

    move-result p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1693432
    :sswitch_0
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1693433
    check-cast v0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-static {v0, v1}, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->a(Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;)V

    .line 1693434
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->m:Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->m:Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1693435
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->m:Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    .line 1693436
    iput-object v1, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->q:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    .line 1693437
    invoke-static {v0}, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->m(Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;)V

    .line 1693438
    :cond_4
    goto :goto_1

    .line 1693439
    :sswitch_1
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1693440
    check-cast v0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-static {v0, v1}, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->b(Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x4e6785e3 -> :sswitch_0
        0x291507f7 -> :sswitch_1
    .end sparse-switch
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 1693410
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1693411
    check-cast v0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;

    .line 1693412
    iget-boolean v1, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->g:Z

    move v0, v1

    .line 1693413
    if-eqz v0, :cond_0

    .line 1693414
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->f:LX/Acr;

    .line 1693415
    iget-object v1, v0, LX/Acr;->a:LX/0if;

    sget-object v2, LX/0ig;->D:LX/0ih;

    invoke-virtual {v1, v2}, LX/0if;->c(LX/0ih;)V

    .line 1693416
    :cond_0
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1693417
    check-cast v0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;

    const/4 v1, 0x0

    .line 1693418
    iput-object v1, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->h:LX/Ad1;

    .line 1693419
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->m:Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->m:Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1693420
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->m:Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1693421
    :cond_1
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->i:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->p:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1693422
    return-void
.end method

.method public final d()V
    .locals 5

    .prologue
    .line 1693383
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->f:LX/Acr;

    .line 1693384
    iget-object v1, v0, LX/Acr;->a:LX/0if;

    sget-object v2, LX/0ig;->D:LX/0ih;

    const-string v3, "click_banner"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1693385
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1693386
    check-cast v0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, LX/0ew;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ew;

    .line 1693387
    if-nez v0, :cond_1

    const/4 v0, 0x0

    move-object v2, v0

    .line 1693388
    :goto_0
    if-eqz v2, :cond_0

    const-string v0, "LIVE_DONATION_DIALOG"

    invoke-virtual {v2, v0}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1693389
    :cond_0
    :goto_1
    return-void

    .line 1693390
    :cond_1
    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v0

    move-object v2, v0

    goto :goto_0

    .line 1693391
    :cond_2
    const/4 v1, 0x0

    .line 1693392
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1693393
    check-cast v0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v3, Landroid/app/Activity;

    invoke-static {v0, v3}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1693394
    if-eqz v0, :cond_4

    .line 1693395
    const v3, 0x7f0d0fc9

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1693396
    if-eqz v0, :cond_4

    .line 1693397
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 1693398
    :goto_2
    iget-object v1, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->m:Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;

    if-nez v1, :cond_3

    .line 1693399
    new-instance v1, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;

    invoke-direct {v1}, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;-><init>()V

    .line 1693400
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1693401
    const-string v4, "square_view_height"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1693402
    invoke-virtual {v1, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1693403
    move-object v0, v1

    .line 1693404
    iput-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->m:Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;

    .line 1693405
    :cond_3
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->m:Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;

    .line 1693406
    iput-object p0, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->n:Lcom/facebook/facecastdisplay/donation/LiveDonationController;

    .line 1693407
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->m:Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    .line 1693408
    iput-object v1, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->q:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    .line 1693409
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->m:Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;

    invoke-virtual {v2}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const-string v2, "LIVE_DONATION_DIALOG"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/DialogFragment;->a(LX/0hH;Ljava/lang/String;Z)I

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2
.end method
