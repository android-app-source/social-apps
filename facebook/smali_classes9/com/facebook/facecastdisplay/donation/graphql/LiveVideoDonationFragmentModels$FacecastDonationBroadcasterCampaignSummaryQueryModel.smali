.class public final Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastDonationBroadcasterCampaignSummaryQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x655ea89e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastDonationBroadcasterCampaignSummaryQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastDonationBroadcasterCampaignSummaryQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1694266
    const-class v0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastDonationBroadcasterCampaignSummaryQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1694265
    const-class v0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastDonationBroadcasterCampaignSummaryQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1694263
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1694264
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1694261
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastDonationBroadcasterCampaignSummaryQueryModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastDonationBroadcasterCampaignSummaryQueryModel;->e:Ljava/lang/String;

    .line 1694262
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastDonationBroadcasterCampaignSummaryQueryModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1694254
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1694255
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastDonationBroadcasterCampaignSummaryQueryModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1694256
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1694257
    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1694258
    const/4 v0, 0x1

    iget v1, p0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastDonationBroadcasterCampaignSummaryQueryModel;->f:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1694259
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1694260
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1694267
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1694268
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1694269
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1694253
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastDonationBroadcasterCampaignSummaryQueryModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1694250
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1694251
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastDonationBroadcasterCampaignSummaryQueryModel;->f:I

    .line 1694252
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1694247
    new-instance v0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastDonationBroadcasterCampaignSummaryQueryModel;

    invoke-direct {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastDonationBroadcasterCampaignSummaryQueryModel;-><init>()V

    .line 1694248
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1694249
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1694246
    const v0, 0x672badbc

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1694245
    const v0, 0x4ed245b

    return v0
.end method

.method public final j()I
    .locals 2

    .prologue
    .line 1694243
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1694244
    iget v0, p0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastDonationBroadcasterCampaignSummaryQueryModel;->f:I

    return v0
.end method
