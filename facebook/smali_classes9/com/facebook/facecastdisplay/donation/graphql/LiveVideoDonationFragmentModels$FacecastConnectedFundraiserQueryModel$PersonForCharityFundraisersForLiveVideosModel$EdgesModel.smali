.class public final Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel$PersonForCharityFundraisersForLiveVideosModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3cb4b7c0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel$PersonForCharityFundraisersForLiveVideosModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel$PersonForCharityFundraisersForLiveVideosModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1694113
    const-class v0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel$PersonForCharityFundraisersForLiveVideosModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1694112
    const-class v0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel$PersonForCharityFundraisersForLiveVideosModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1694110
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1694111
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1694114
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1694115
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel$PersonForCharityFundraisersForLiveVideosModel$EdgesModel;->a()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1694116
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1694117
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1694118
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1694119
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1694102
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1694103
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel$PersonForCharityFundraisersForLiveVideosModel$EdgesModel;->a()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1694104
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel$PersonForCharityFundraisersForLiveVideosModel$EdgesModel;->a()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    .line 1694105
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel$PersonForCharityFundraisersForLiveVideosModel$EdgesModel;->a()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1694106
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel$PersonForCharityFundraisersForLiveVideosModel$EdgesModel;

    .line 1694107
    iput-object v0, v1, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel$PersonForCharityFundraisersForLiveVideosModel$EdgesModel;->e:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    .line 1694108
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1694109
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1694100
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel$PersonForCharityFundraisersForLiveVideosModel$EdgesModel;->e:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel$PersonForCharityFundraisersForLiveVideosModel$EdgesModel;->e:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    .line 1694101
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel$PersonForCharityFundraisersForLiveVideosModel$EdgesModel;->e:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1694097
    new-instance v0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel$PersonForCharityFundraisersForLiveVideosModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel$PersonForCharityFundraisersForLiveVideosModel$EdgesModel;-><init>()V

    .line 1694098
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1694099
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1694096
    const v0, 0x675dfe05

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1694095
    const v0, 0x693a5486

    return v0
.end method
