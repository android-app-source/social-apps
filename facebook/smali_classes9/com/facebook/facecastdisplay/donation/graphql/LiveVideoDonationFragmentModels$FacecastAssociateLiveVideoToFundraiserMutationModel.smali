.class public final Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastAssociateLiveVideoToFundraiserMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4edcfcfc
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastAssociateLiveVideoToFundraiserMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastAssociateLiveVideoToFundraiserMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastAssociateLiveVideoToFundraiserMutationModel$VideoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1693967
    const-class v0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastAssociateLiveVideoToFundraiserMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1693966
    const-class v0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastAssociateLiveVideoToFundraiserMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1693964
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1693965
    return-void
.end method

.method private a()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastAssociateLiveVideoToFundraiserMutationModel$VideoModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getVideo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1693962
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastAssociateLiveVideoToFundraiserMutationModel;->e:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastAssociateLiveVideoToFundraiserMutationModel$VideoModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastAssociateLiveVideoToFundraiserMutationModel$VideoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastAssociateLiveVideoToFundraiserMutationModel$VideoModel;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastAssociateLiveVideoToFundraiserMutationModel;->e:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastAssociateLiveVideoToFundraiserMutationModel$VideoModel;

    .line 1693963
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastAssociateLiveVideoToFundraiserMutationModel;->e:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastAssociateLiveVideoToFundraiserMutationModel$VideoModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1693968
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1693969
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastAssociateLiveVideoToFundraiserMutationModel;->a()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastAssociateLiveVideoToFundraiserMutationModel$VideoModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1693970
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1693971
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1693972
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1693973
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1693954
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1693955
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastAssociateLiveVideoToFundraiserMutationModel;->a()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastAssociateLiveVideoToFundraiserMutationModel$VideoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1693956
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastAssociateLiveVideoToFundraiserMutationModel;->a()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastAssociateLiveVideoToFundraiserMutationModel$VideoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastAssociateLiveVideoToFundraiserMutationModel$VideoModel;

    .line 1693957
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastAssociateLiveVideoToFundraiserMutationModel;->a()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastAssociateLiveVideoToFundraiserMutationModel$VideoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1693958
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastAssociateLiveVideoToFundraiserMutationModel;

    .line 1693959
    iput-object v0, v1, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastAssociateLiveVideoToFundraiserMutationModel;->e:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastAssociateLiveVideoToFundraiserMutationModel$VideoModel;

    .line 1693960
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1693961
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1693951
    new-instance v0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastAssociateLiveVideoToFundraiserMutationModel;

    invoke-direct {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastAssociateLiveVideoToFundraiserMutationModel;-><init>()V

    .line 1693952
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1693953
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1693950
    const v0, 0x5265223b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1693949
    const v0, 0x5b5c3af6

    return v0
.end method
