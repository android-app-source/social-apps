.class public final Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1693853
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1693854
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1693851
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1693852
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 1693827
    if-nez p1, :cond_0

    .line 1693828
    :goto_0
    return v0

    .line 1693829
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1693830
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1693831
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1693832
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1693833
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1693834
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1693835
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1693836
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1693837
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1693838
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1693839
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1693840
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1693841
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1693842
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1693843
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1693844
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1693845
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1693846
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1693847
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1693848
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1693849
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1693850
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x3ef477c8 -> :sswitch_3
        -0x367d7ebc -> :sswitch_1
        0x37af114e -> :sswitch_2
        0x4047edaa -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1693826
    new-instance v0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1693823
    sparse-switch p0, :sswitch_data_0

    .line 1693824
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1693825
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x3ef477c8 -> :sswitch_0
        -0x367d7ebc -> :sswitch_0
        0x37af114e -> :sswitch_0
        0x4047edaa -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1693822
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1693820
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$DraculaImplementation;->b(I)V

    .line 1693821
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1693855
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1693856
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1693857
    :cond_0
    iput-object p1, p0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$DraculaImplementation;->a:LX/15i;

    .line 1693858
    iput p2, p0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$DraculaImplementation;->b:I

    .line 1693859
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1693819
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1693818
    new-instance v0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1693815
    iget v0, p0, LX/1vt;->c:I

    .line 1693816
    move v0, v0

    .line 1693817
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1693812
    iget v0, p0, LX/1vt;->c:I

    .line 1693813
    move v0, v0

    .line 1693814
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1693809
    iget v0, p0, LX/1vt;->b:I

    .line 1693810
    move v0, v0

    .line 1693811
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1693806
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1693807
    move-object v0, v0

    .line 1693808
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1693797
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1693798
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1693799
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1693800
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1693801
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1693802
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1693803
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1693804
    invoke-static {v3, v9, v2}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1693805
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1693794
    iget v0, p0, LX/1vt;->c:I

    .line 1693795
    move v0, v0

    .line 1693796
    return v0
.end method
