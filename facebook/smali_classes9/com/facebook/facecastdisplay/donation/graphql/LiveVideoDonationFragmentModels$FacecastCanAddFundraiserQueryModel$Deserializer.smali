.class public final Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastCanAddFundraiserQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1693974
    const-class v0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastCanAddFundraiserQueryModel;

    new-instance v1, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastCanAddFundraiserQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastCanAddFundraiserQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1693975
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1693976
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1693977
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1693978
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1693979
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_5

    .line 1693980
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1693981
    :goto_0
    move v1, v2

    .line 1693982
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1693983
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1693984
    new-instance v1, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastCanAddFundraiserQueryModel;

    invoke-direct {v1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastCanAddFundraiserQueryModel;-><init>()V

    .line 1693985
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1693986
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1693987
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1693988
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1693989
    :cond_0
    return-object v1

    .line 1693990
    :cond_1
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, p0, :cond_3

    .line 1693991
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1693992
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1693993
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_1

    if-eqz v5, :cond_1

    .line 1693994
    const-string p0, "can_add_fundraiser_to_live_video"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1693995
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v4, v1

    move v1, v3

    goto :goto_1

    .line 1693996
    :cond_2
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1693997
    :cond_3
    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1693998
    if-eqz v1, :cond_4

    .line 1693999
    invoke-virtual {v0, v2, v4}, LX/186;->a(IZ)V

    .line 1694000
    :cond_4
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_5
    move v1, v2

    move v4, v2

    goto :goto_1
.end method
