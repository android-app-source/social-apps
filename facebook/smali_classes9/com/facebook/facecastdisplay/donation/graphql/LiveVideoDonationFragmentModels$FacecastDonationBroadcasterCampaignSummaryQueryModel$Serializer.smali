.class public final Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastDonationBroadcasterCampaignSummaryQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastDonationBroadcasterCampaignSummaryQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1694241
    const-class v0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastDonationBroadcasterCampaignSummaryQueryModel;

    new-instance v1, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastDonationBroadcasterCampaignSummaryQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastDonationBroadcasterCampaignSummaryQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1694242
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1694226
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastDonationBroadcasterCampaignSummaryQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1694228
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1694229
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x0

    .line 1694230
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1694231
    invoke-virtual {v1, v0, p2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1694232
    if-eqz v2, :cond_0

    .line 1694233
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1694234
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1694235
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2, p2}, LX/15i;->a(III)I

    move-result v2

    .line 1694236
    if-eqz v2, :cond_1

    .line 1694237
    const-string p0, "number_of_donation"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1694238
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1694239
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1694240
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1694227
    check-cast p1, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastDonationBroadcasterCampaignSummaryQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastDonationBroadcasterCampaignSummaryQueryModel$Serializer;->a(Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastDonationBroadcasterCampaignSummaryQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
