.class public final Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel$PageModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x46ac8a11
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel$PageModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel$PageModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1694736
    const-class v0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel$PageModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1694735
    const-class v0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel$PageModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1694733
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1694734
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1694731
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel$PageModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel$PageModel;->e:Ljava/lang/String;

    .line 1694732
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel$PageModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1694723
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1694724
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel$PageModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1694725
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel$PageModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1694726
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1694727
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1694728
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1694729
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1694730
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1694720
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1694721
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1694722
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1694737
    new-instance v0, LX/AdJ;

    invoke-direct {v0, p1}, LX/AdJ;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1694719
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel$PageModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1694717
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1694718
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1694716
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1694713
    new-instance v0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel$PageModel;

    invoke-direct {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel$PageModel;-><init>()V

    .line 1694714
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1694715
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1694712
    const v0, -0x26768eb2

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1694709
    const v0, 0x25d6af

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1694710
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel$PageModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel$PageModel;->f:Ljava/lang/String;

    .line 1694711
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel$PageModel;->f:Ljava/lang/String;

    return-object v0
.end method
