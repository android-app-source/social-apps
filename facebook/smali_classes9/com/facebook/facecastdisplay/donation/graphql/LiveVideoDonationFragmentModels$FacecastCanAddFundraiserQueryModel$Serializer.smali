.class public final Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastCanAddFundraiserQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastCanAddFundraiserQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1694001
    const-class v0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastCanAddFundraiserQueryModel;

    new-instance v1, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastCanAddFundraiserQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastCanAddFundraiserQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1694002
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1694013
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastCanAddFundraiserQueryModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1694004
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1694005
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1694006
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1694007
    const/4 p0, 0x0

    invoke-virtual {v1, v0, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 1694008
    if-eqz p0, :cond_0

    .line 1694009
    const-string p2, "can_add_fundraiser_to_live_video"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1694010
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 1694011
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1694012
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1694003
    check-cast p1, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastCanAddFundraiserQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastCanAddFundraiserQueryModel$Serializer;->a(Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastCanAddFundraiserQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
