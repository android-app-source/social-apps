.class public final Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonateEventSubscriptionModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonateEventSubscriptionModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1694433
    const-class v0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonateEventSubscriptionModel;

    new-instance v1, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonateEventSubscriptionModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonateEventSubscriptionModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1694434
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1694432
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonateEventSubscriptionModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1694435
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1694436
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1694437
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1694438
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1694439
    if-eqz v2, :cond_0

    .line 1694440
    const-string p0, "donation"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1694441
    invoke-static {v1, v2, p1, p2}, LX/AdP;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1694442
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1694443
    if-eqz v2, :cond_1

    .line 1694444
    const-string p0, "fundraiser_to_charity"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1694445
    invoke-static {v1, v2, p1, p2}, LX/AdX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1694446
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1694447
    if-eqz v2, :cond_2

    .line 1694448
    const-string p0, "video"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1694449
    invoke-static {v1, v2, p1}, LX/AdQ;->a(LX/15i;ILX/0nX;)V

    .line 1694450
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1694451
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1694431
    check-cast p1, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonateEventSubscriptionModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonateEventSubscriptionModel$Serializer;->a(Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonateEventSubscriptionModel;LX/0nX;LX/0my;)V

    return-void
.end method
