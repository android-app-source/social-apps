.class public Lcom/facebook/facecastdisplay/donation/LiveDonationHeaderView;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""


# instance fields
.field public final j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final k:Landroid/widget/TextView;

.field public final l:Landroid/widget/TextView;

.field public final m:Lcom/facebook/fbui/glyph/GlyphView;

.field public n:LX/Ad5;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1693660
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/donation/LiveDonationHeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1693661
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1693664
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/donation/LiveDonationHeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1693665
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    .line 1693666
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1693667
    const v0, 0x7f030a07

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1693668
    const v0, 0x7f0d1954

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationHeaderView;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1693669
    const v0, 0x7f0d1955

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationHeaderView;->k:Landroid/widget/TextView;

    .line 1693670
    const v0, 0x7f0d1956

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationHeaderView;->l:Landroid/widget/TextView;

    .line 1693671
    const v0, 0x7f0d1957

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationHeaderView;->m:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1693672
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationHeaderView;->m:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/Ad7;

    invoke-direct {v1, p0}, LX/Ad7;-><init>(Lcom/facebook/facecastdisplay/donation/LiveDonationHeaderView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1693673
    return-void
.end method


# virtual methods
.method public setLiveDonationHeaderViewListener(LX/Ad5;)V
    .locals 0

    .prologue
    .line 1693662
    iput-object p1, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationHeaderView;->n:LX/Ad5;

    .line 1693663
    return-void
.end method
