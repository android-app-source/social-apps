.class public Lcom/facebook/facecastdisplay/donation/LiveDonationView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public final a:Lcom/facebook/facecastdisplay/donation/LiveDonationHeaderView;

.field public final b:Lcom/facebook/resources/ui/FbTextView;

.field public final c:Landroid/widget/ProgressBar;

.field public final d:Lcom/facebook/resources/ui/FbTextView;

.field public final e:Lcom/facebook/resources/ui/FbButton;

.field public final f:Lcom/facebook/resources/ui/FbButton;

.field public g:LX/Ad6;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1693682
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/donation/LiveDonationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1693683
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1693684
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/donation/LiveDonationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1693685
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    .line 1693686
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1693687
    const v0, 0x7f030a08

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1693688
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/donation/LiveDonationView;->setOrientation(I)V

    .line 1693689
    const v0, 0x7f0d1959

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/donation/LiveDonationHeaderView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationView;->a:Lcom/facebook/facecastdisplay/donation/LiveDonationHeaderView;

    .line 1693690
    const v0, 0x7f0d195a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationView;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1693691
    const v0, 0x7f0d195b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationView;->c:Landroid/widget/ProgressBar;

    .line 1693692
    const v0, 0x7f0d195e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationView;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 1693693
    const v0, 0x7f0d195f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationView;->e:Lcom/facebook/resources/ui/FbButton;

    .line 1693694
    const v0, 0x7f0d1960

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationView;->f:Lcom/facebook/resources/ui/FbButton;

    .line 1693695
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationView;->e:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/Ad8;

    invoke-direct {v1, p0}, LX/Ad8;-><init>(Lcom/facebook/facecastdisplay/donation/LiveDonationView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1693696
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationView;->f:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/Ad9;

    invoke-direct {v1, p0}, LX/Ad9;-><init>(Lcom/facebook/facecastdisplay/donation/LiveDonationView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1693697
    return-void
.end method


# virtual methods
.method public setLiveDonationViewListener(LX/Ad6;)V
    .locals 0

    .prologue
    .line 1693698
    iput-object p1, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationView;->g:LX/Ad6;

    .line 1693699
    return-void
.end method
