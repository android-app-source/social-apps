.class public Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public b:Lcom/facebook/resources/ui/FbTextView;

.field public c:Lcom/facebook/resources/ui/FbTextView;

.field public d:Landroid/widget/ProgressBar;

.field public e:Lcom/facebook/fig/button/FigButton;

.field public f:Lcom/facebook/resources/ui/FbTextView;

.field public g:Z

.field public h:LX/Ad1;

.field public i:LX/AaE;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1693605
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1693606
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1693603
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1693604
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1693599
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1693600
    iput-boolean v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->g:Z

    .line 1693601
    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->setOrientation(I)V

    .line 1693602
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 1693592
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->g:Z

    .line 1693593
    const v0, 0x7f0d194a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1693594
    const v0, 0x7f0d194b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1693595
    const v0, 0x7f0d194d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 1693596
    const v0, 0x7f0d194f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->e:Lcom/facebook/fig/button/FigButton;

    .line 1693597
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->e:Lcom/facebook/fig/button/FigButton;

    new-instance v1, LX/Ad4;

    invoke-direct {v1, p0}, LX/Ad4;-><init>(Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1693598
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1693589
    const v0, 0x7f030a15

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1693590
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->d()V

    .line 1693591
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1693582
    const v0, 0x7f030a04

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1693583
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->d()V

    .line 1693584
    const v0, 0x7f0d194e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->d:Landroid/widget/ProgressBar;

    .line 1693585
    const v0, 0x7f0d194c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 1693586
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->f:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, LX/Ad2;

    invoke-direct {v1, p0}, LX/Ad2;-><init>(Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1693587
    new-instance v0, LX/Ad3;

    invoke-direct {v0, p0}, LX/Ad3;-><init>(Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1693588
    return-void
.end method

.method public getDonateButton()Lcom/facebook/fig/button/FigButton;
    .locals 1

    .prologue
    .line 1693581
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->e:Lcom/facebook/fig/button/FigButton;

    return-object v0
.end method

.method public getDonateProgressText()Lcom/facebook/resources/ui/FbTextView;
    .locals 1

    .prologue
    .line 1693572
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->c:Lcom/facebook/resources/ui/FbTextView;

    return-object v0
.end method

.method public getDonationCampaignEditButton()Lcom/facebook/resources/ui/FbTextView;
    .locals 1

    .prologue
    .line 1693580
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->f:Lcom/facebook/resources/ui/FbTextView;

    return-object v0
.end method

.method public getDonationCampaignTitle()Lcom/facebook/resources/ui/FbTextView;
    .locals 1

    .prologue
    .line 1693579
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->b:Lcom/facebook/resources/ui/FbTextView;

    return-object v0
.end method

.method public getDonationLogoImage()Lcom/facebook/drawee/fbpipeline/FbDraweeView;
    .locals 1

    .prologue
    .line 1693578
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    return-object v0
.end method

.method public getDonationProgressBar()Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 1693577
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->d:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method public setFacecastDonationBannerViewListener(LX/AaE;)V
    .locals 0

    .prologue
    .line 1693575
    iput-object p1, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->i:LX/AaE;

    .line 1693576
    return-void
.end method

.method public setLiveDonationEntryViewListener(LX/Ad1;)V
    .locals 0

    .prologue
    .line 1693573
    iput-object p1, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->h:LX/Ad1;

    .line 1693574
    return-void
.end method
