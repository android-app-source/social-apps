.class public Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/Ad5;
.implements LX/Ad6;


# instance fields
.field public m:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:Lcom/facebook/facecastdisplay/donation/LiveDonationController;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Landroid/view/View;

.field public p:Lcom/facebook/facecastdisplay/donation/LiveDonationView;

.field public q:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1693654
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 1693655
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;

    invoke-static {p0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object p0

    check-cast p0, LX/17W;

    iput-object p0, p1, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->m:LX/17W;

    return-void
.end method

.method public static m(Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1693647
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->q:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    if-nez v0, :cond_0

    .line 1693648
    :goto_0
    return-void

    .line 1693649
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->q:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->q:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->m()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1693650
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080c45

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->q:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->l()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->q:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->m()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1693651
    iget-object v1, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->p:Lcom/facebook/facecastdisplay/donation/LiveDonationView;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/donation/LiveDonationView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1693652
    :cond_1
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->q:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->x()D

    move-result-wide v0

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Integer;

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, LX/0yq;->a(Ljava/lang/Comparable;[Ljava/lang/Comparable;)Ljava/lang/Comparable;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1693653
    iget-object v1, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->p:Lcom/facebook/facecastdisplay/donation/LiveDonationView;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/donation/LiveDonationView;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1693642
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->n:Lcom/facebook/facecastdisplay/donation/LiveDonationController;

    if-eqz v0, :cond_0

    .line 1693643
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->n:Lcom/facebook/facecastdisplay/donation/LiveDonationController;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1693644
    iget-object p0, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->f:LX/Acr;

    invoke-virtual {p0}, LX/Acr;->b()V

    .line 1693645
    invoke-static {v0, v1}, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->c(Lcom/facebook/facecastdisplay/donation/LiveDonationController;Landroid/content/Context;)V

    .line 1693646
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 1693633
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->q:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->q:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->u()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1693634
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->q:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->u()Ljava/lang/String;

    move-result-object v0

    .line 1693635
    iget-object v1, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->m:LX/17W;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, LX/0ax;->gM:Ljava/lang/String;

    const-string v4, "fundraiser_live"

    invoke-static {v3, v0, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 1693636
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->n:Lcom/facebook/facecastdisplay/donation/LiveDonationController;

    if-eqz v0, :cond_0

    .line 1693637
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->n:Lcom/facebook/facecastdisplay/donation/LiveDonationController;

    .line 1693638
    iget-object v1, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->f:LX/Acr;

    .line 1693639
    iget-object v2, v1, LX/Acr;->a:LX/0if;

    sget-object v3, LX/0ig;->D:LX/0ih;

    const-string v0, "click_fundraiser_button"

    invoke-virtual {v2, v3, v0}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1693640
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1693641
    :cond_1
    return-void
.end method

.method public final k()V
    .locals 0

    .prologue
    .line 1693631
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1693632
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2aff30cb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1693627
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1693628
    const-class v1, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;

    invoke-static {v1, p0}, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1693629
    const v1, 0x7f0e0390

    invoke-virtual {p0, v3, v1}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 1693630
    const/16 v1, 0x2b

    const v2, 0x50e10d95

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x49231419

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1693626
    const v1, 0x7f030a06

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x3cfec744

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1693607
    invoke-super {p0, p1, p2}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1693608
    const v0, 0x7f0d1952

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->o:Landroid/view/View;

    .line 1693609
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1693610
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 1693611
    const-string v2, "square_view_height"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1693612
    const v0, 0x7f0d1953

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/donation/LiveDonationView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->p:Lcom/facebook/facecastdisplay/donation/LiveDonationView;

    .line 1693613
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->p:Lcom/facebook/facecastdisplay/donation/LiveDonationView;

    .line 1693614
    iput-object p0, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationView;->g:LX/Ad6;

    .line 1693615
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->p:Lcom/facebook/facecastdisplay/donation/LiveDonationView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationView;->a:Lcom/facebook/facecastdisplay/donation/LiveDonationHeaderView;

    .line 1693616
    iput-object p0, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationHeaderView;->n:LX/Ad5;

    .line 1693617
    const/4 p2, 0x0

    .line 1693618
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->q:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->q:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->o()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1693619
    :cond_0
    :goto_0
    return-void

    .line 1693620
    :cond_1
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->q:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->v()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1693621
    iget-object v2, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->p:Lcom/facebook/facecastdisplay/donation/LiveDonationView;

    iget-object v2, v2, Lcom/facebook/facecastdisplay/donation/LiveDonationView;->a:Lcom/facebook/facecastdisplay/donation/LiveDonationHeaderView;

    iget-object v2, v2, Lcom/facebook/facecastdisplay/donation/LiveDonationHeaderView;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0, p2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-class v1, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1693622
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->p:Lcom/facebook/facecastdisplay/donation/LiveDonationView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationView;->a:Lcom/facebook/facecastdisplay/donation/LiveDonationHeaderView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationHeaderView;->k:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->q:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1693623
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->p:Lcom/facebook/facecastdisplay/donation/LiveDonationView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationView;->a:Lcom/facebook/facecastdisplay/donation/LiveDonationHeaderView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationHeaderView;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080c43

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object p1, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->q:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->p()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;->k()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v3, p2

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1693624
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->p:Lcom/facebook/facecastdisplay/donation/LiveDonationView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationView;->d:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->q:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->r()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1693625
    invoke-static {p0}, Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;->m(Lcom/facebook/facecastdisplay/donation/LiveDonationFragment;)V

    goto :goto_0
.end method
