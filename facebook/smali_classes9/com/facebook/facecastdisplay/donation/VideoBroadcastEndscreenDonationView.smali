.class public Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private e:Lcom/facebook/resources/ui/FbTextView;

.field private f:Lcom/facebook/resources/ui/FbTextView;

.field private g:Lcom/facebook/resources/ui/FbTextView;

.field private h:Lcom/facebook/resources/ui/FbButton;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1693704
    const-class v0, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1693705
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1693706
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1693707
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1693708
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1693709
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1693710
    const-class v0, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;

    invoke-static {v0, p0}, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1693711
    const v0, 0x7f031581

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1693712
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;->setOrientation(I)V

    .line 1693713
    const v0, 0x7f0d194a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1693714
    const v0, 0x7f0d3065

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1693715
    const v0, 0x7f0d3067

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 1693716
    const v0, 0x7f0d3066

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 1693717
    const v0, 0x7f0d3068

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 1693718
    const v0, 0x7f0d3069

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;->h:Lcom/facebook/resources/ui/FbButton;

    .line 1693719
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v0

    check-cast v0, LX/17W;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;->a:LX/17W;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLActor;)V
    .locals 9
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/graphql/model/GraphQLActor;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1693720
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->v()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_0

    .line 1693721
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->v()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1693722
    iget-object v3, p0, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, v1, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1693723
    :cond_0
    iget-object v1, p0, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;->h:Lcom/facebook/resources/ui/FbButton;

    new-instance v2, LX/AdA;

    invoke-direct {v2, p0, p1}, LX/AdA;-><init>(Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;)V

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1693724
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->p()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->p()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;->m()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1693725
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->p()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1693726
    iget-object v2, p0, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v3, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v1, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1693727
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->p()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;

    move-result-object v1

    if-nez v1, :cond_2

    move-object v1, v0

    .line 1693728
    :goto_0
    if-eqz p2, :cond_3

    .line 1693729
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080c3f

    new-array v4, v8, [Ljava/lang/Object;

    aput-object p2, v4, v6

    aput-object v1, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1693730
    :goto_1
    return-void

    .line 1693731
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->p()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;->k()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1693732
    :cond_3
    iget-object v2, p0, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080c40

    new-array v5, v7, [Ljava/lang/Object;

    if-nez p3, :cond_4

    :goto_2
    aput-object v0, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1693733
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080c3c

    new-array v4, v8, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->n()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    aput-object v1, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1693734
    :cond_4
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1693735
    if-eqz p1, :cond_0

    .line 1693736
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1693737
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1693738
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;->h:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 1693739
    :goto_0
    return-void

    .line 1693740
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1693741
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1693742
    iget-object v0, p0, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;->h:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    goto :goto_0
.end method
