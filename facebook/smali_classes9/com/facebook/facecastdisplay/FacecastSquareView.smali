.class public Lcom/facebook/facecastdisplay/FacecastSquareView;
.super Landroid/view/View;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1692128
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/FacecastSquareView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1692129
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1692126
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/FacecastSquareView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1692127
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1692124
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1692125
    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 1692120
    const/4 v0, 0x0

    invoke-static {v0, p1}, Landroid/view/View;->getDefaultSize(II)I

    move-result v0

    .line 1692121
    invoke-static {v0, p2}, Landroid/view/View;->resolveSize(II)I

    move-result v1

    .line 1692122
    invoke-virtual {p0, v0, v1}, Lcom/facebook/facecastdisplay/FacecastSquareView;->setMeasuredDimension(II)V

    .line 1692123
    return-void
.end method
