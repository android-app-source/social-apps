.class public Lcom/facebook/facecastdisplay/LiveEventsPill;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private final a:Lcom/facebook/resources/ui/FbTextView;

.field private final b:Landroid/view/View;

.field private final c:Ljava/util/regex/Pattern;

.field private d:I

.field private e:F

.field private f:I

.field private g:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1692396
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/LiveEventsPill;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1692397
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1692394
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/LiveEventsPill;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1692395
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1692398
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1692399
    const v0, 0x7f030a0d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1692400
    const v0, 0x7f0d1968

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/LiveEventsPill;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPill;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 1692401
    const v0, 0x7f0d1967

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/LiveEventsPill;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPill;->b:Landroid/view/View;

    .line 1692402
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPill;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPill;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v1, v0

    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPill;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v1, v0

    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPill;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPill;->f:I

    .line 1692403
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/LiveEventsPill;->setOrientation(I)V

    .line 1692404
    const v0, 0x7f021105

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/LiveEventsPill;->setBackgroundResource(I)V

    .line 1692405
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPill;->e:F

    .line 1692406
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPill;->d:I

    .line 1692407
    const-string v0, "\\s\\S+$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPill;->c:Ljava/util/regex/Pattern;

    .line 1692408
    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/16 v6, 0x2026

    const/16 v5, 0x22

    const/4 v4, 0x0

    .line 1692381
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1692382
    const-string v0, "\"\""

    .line 1692383
    :cond_0
    return-object v0

    .line 1692384
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "\""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1692385
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPill;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    iget v2, p0, Lcom/facebook/facecastdisplay/LiveEventsPill;->e:F

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v1, v0, v2, v3}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1692386
    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1692387
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPill;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 1692388
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1692389
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v1

    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1692390
    :cond_2
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1692391
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPill;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    iget v2, p0, Lcom/facebook/facecastdisplay/LiveEventsPill;->e:F

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v1, v0, v2, v3}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1692392
    :cond_3
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x2

    if-le v1, v2, :cond_2

    .line 1692393
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v0, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1692369
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 1692370
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 1692371
    if-nez v1, :cond_0

    .line 1692372
    invoke-virtual {p0, v2, v2}, Lcom/facebook/facecastdisplay/LiveEventsPill;->setMeasuredDimension(II)V

    .line 1692373
    :goto_0
    return-void

    .line 1692374
    :cond_0
    iget v2, p0, Lcom/facebook/facecastdisplay/LiveEventsPill;->e:F

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_1

    const/high16 v2, 0x40000000    # 2.0f

    if-eq v0, v2, :cond_2

    iget v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPill;->d:I

    if-eq v1, v0, :cond_2

    .line 1692375
    :cond_1
    iput v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPill;->d:I

    .line 1692376
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;->onMeasure(II)V

    .line 1692377
    iget v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPill;->d:I

    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPill;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPill;->f:I

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/LiveEventsPill;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/LiveEventsPill;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iput v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPill;->e:F

    .line 1692378
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPill;->g:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1692379
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPill;->a:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventsPill;->g:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/facebook/facecastdisplay/LiveEventsPill;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1692380
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;->onMeasure(II)V

    goto :goto_0
.end method

.method public setPillText(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1692365
    iput-object p1, p0, Lcom/facebook/facecastdisplay/LiveEventsPill;->g:Ljava/lang/String;

    .line 1692366
    iget v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPill;->e:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 1692367
    :goto_0
    return-void

    .line 1692368
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsPill;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-direct {p0, p1}, Lcom/facebook/facecastdisplay/LiveEventsPill;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
