.class public final Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingDownloader$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/AhH;


# direct methods
.method public constructor <init>(LX/AhH;)V
    .locals 0

    .prologue
    .line 1702336
    iput-object p1, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingDownloader$1;->a:LX/AhH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 1702337
    iget-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingDownloader$1;->a:LX/AhH;

    .line 1702338
    iget-object v1, v0, LX/AhH;->a:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 1702339
    :cond_0
    :goto_0
    return-void

    .line 1702340
    :cond_1
    iget-object v1, v0, LX/AhH;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_2

    iget-object v1, v0, LX/AhH;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v1}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1702341
    :cond_2
    new-instance v1, LX/6S8;

    invoke-direct {v1}, LX/6S8;-><init>()V

    move-object v1, v1

    .line 1702342
    const-string v2, "targetID"

    iget-object p0, v0, LX/AhH;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1702343
    const-string v2, "includeIDs"

    iget-object p0, v0, LX/AhH;->h:Ljava/util/List;

    invoke-virtual {v1, v2, p0}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 1702344
    iget-object v2, v0, LX/AhH;->d:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    iput-object v1, v0, LX/AhH;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1702345
    iget-object v1, v0, LX/AhH;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, LX/AhF;

    invoke-direct {v2, v0}, LX/AhF;-><init>(LX/AhH;)V

    iget-object p0, v0, LX/AhH;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v2, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
