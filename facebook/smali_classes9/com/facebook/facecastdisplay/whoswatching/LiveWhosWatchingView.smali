.class public Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;
.super Landroid/support/v7/widget/RecyclerView;
.source ""

# interfaces
.implements LX/AhG;


# instance fields
.field public h:LX/AhD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/AhH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final k:LX/3wu;

.field private l:Z

.field public m:Z

.field private n:Z

.field private o:Z

.field private p:I

.field public q:LX/AhE;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1702469
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1702470
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1702385
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1702386
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 1702452
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1702453
    const-class v0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;

    invoke-static {v0, p0}, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1702454
    const/4 v0, 0x1

    .line 1702455
    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->v:Z

    .line 1702456
    const/4 v0, 0x4

    iput v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->p:I

    .line 1702457
    new-instance v0, LX/3wu;

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->p:I

    invoke-direct {v0, v1, v2}, LX/3wu;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->k:LX/3wu;

    .line 1702458
    iget-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->k:LX/3wu;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1702459
    iget-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->h:LX/AhD;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1702460
    iget-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->h:LX/AhD;

    iget v1, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->p:I

    .line 1702461
    iput v1, v0, LX/AhD;->e:I

    .line 1702462
    iget-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->j:LX/AhH;

    .line 1702463
    iput-object p0, v0, LX/AhH;->k:LX/AhG;

    .line 1702464
    iget-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->i:Landroid/content/res/Resources;

    const v1, 0x7f0b0531

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 1702465
    new-instance v1, LX/62Y;

    invoke-direct {v1, v0, v3}, LX/62Y;-><init>(IZ)V

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 1702466
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    move-object v0, v0

    .line 1702467
    iput-boolean v3, v0, LX/1Of;->g:Z

    .line 1702468
    return-void
.end method

.method private static a(Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;LX/AhD;Landroid/content/res/Resources;LX/AhH;)V
    .locals 0

    .prologue
    .line 1702451
    iput-object p1, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->h:LX/AhD;

    iput-object p2, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->i:Landroid/content/res/Resources;

    iput-object p3, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->j:LX/AhH;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 9

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;

    new-instance p1, LX/AhD;

    invoke-static {v2}, LX/3HT;->a(LX/0QB;)LX/3HT;

    move-result-object v0

    check-cast v0, LX/3HT;

    const-class v1, LX/AhK;

    invoke-interface {v2, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/AhK;

    invoke-direct {p1, v0, v1}, LX/AhD;-><init>(LX/3HT;LX/AhK;)V

    move-object v0, p1

    check-cast v0, LX/AhD;

    invoke-static {v2}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    new-instance v3, LX/AhH;

    invoke-static {v2}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v2}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {v2}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-static {v2}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v2}, LX/0kY;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v8

    check-cast v8, Landroid/os/Handler;

    invoke-direct/range {v3 .. v8}, LX/AhH;-><init>(LX/0ad;Ljava/util/concurrent/ExecutorService;LX/0tX;LX/03V;Landroid/os/Handler;)V

    move-object v2, v3

    check-cast v2, LX/AhH;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->a(Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;LX/AhD;Landroid/content/res/Resources;LX/AhH;)V

    return-void
.end method

.method private n()V
    .locals 2

    .prologue
    .line 1702446
    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->l:Z

    if-eqz v0, :cond_0

    .line 1702447
    iget-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->h:LX/AhD;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 1702448
    iget-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->q:LX/AhE;

    if-eqz v0, :cond_0

    .line 1702449
    iget-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->q:LX/AhE;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->h:LX/AhD;

    invoke-virtual {v1}, LX/AhD;->d()I

    move-result v1

    invoke-interface {v0, v1}, LX/AhE;->c(I)V

    .line 1702450
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoCurrentViewersModel$LiveVideoCurrentViewersModel$EdgesModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1702442
    iget-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->h:LX/AhD;

    .line 1702443
    iput-object p1, v0, LX/AhD;->d:LX/0Px;

    .line 1702444
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->n()V

    .line 1702445
    return-void
.end method

.method public final l()V
    .locals 2

    .prologue
    .line 1702438
    iget-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->j:LX/AhH;

    invoke-virtual {v0}, LX/AhH;->a()V

    .line 1702439
    iget-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->h:LX/AhD;

    .line 1702440
    iget-object v1, v0, LX/AhD;->b:LX/3HT;

    iget-object p0, v0, LX/AhD;->a:LX/AhB;

    invoke-virtual {v1, p0}, LX/0b4;->a(LX/0b2;)Z

    .line 1702441
    return-void
.end method

.method public final m()V
    .locals 2

    .prologue
    .line 1702429
    iget-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->j:LX/AhH;

    .line 1702430
    invoke-static {v0}, LX/AhH;->e(LX/AhH;)V

    .line 1702431
    const/4 v1, 0x0

    iput-object v1, v0, LX/AhH;->a:Ljava/lang/String;

    .line 1702432
    iget-object v1, v0, LX/AhH;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1702433
    iget-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->h:LX/AhD;

    .line 1702434
    const/4 v1, 0x0

    iput-object v1, v0, LX/AhD;->d:LX/0Px;

    .line 1702435
    const/4 v1, 0x0

    iput v1, v0, LX/AhD;->f:I

    .line 1702436
    iget-object v1, v0, LX/AhD;->b:LX/3HT;

    iget-object p0, v0, LX/AhD;->a:LX/AhB;

    invoke-virtual {v1, p0}, LX/0b4;->b(LX/0b2;)Z

    .line 1702437
    return-void
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    .line 1702419
    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->m:Z

    if-nez v0, :cond_0

    .line 1702420
    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->onMeasure(II)V

    .line 1702421
    :goto_0
    return-void

    .line 1702422
    :cond_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 1702423
    iget v1, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->p:I

    div-int/2addr v0, v1

    .line 1702424
    iget-object v1, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->h:LX/AhD;

    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v1

    iget v2, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->p:I

    if-le v1, v2, :cond_1

    iget-boolean v1, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->n:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->o:Z

    if-eqz v1, :cond_2

    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->i:Landroid/content/res/Resources;

    const v2, 0x7f0b0532

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 1702425
    invoke-static {v0, p2}, Landroid/view/View;->resolveSize(II)I

    move-result v0

    .line 1702426
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1702427
    invoke-super {p0, p1, v0}, Landroid/support/v7/widget/RecyclerView;->onMeasure(II)V

    goto :goto_0

    .line 1702428
    :cond_2
    mul-int/lit8 v0, v0, 0x2

    goto :goto_1
.end method

.method public setFullScreenMode(Z)V
    .locals 0

    .prologue
    .line 1702417
    iput-boolean p1, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->m:Z

    .line 1702418
    return-void
.end method

.method public setIsShowing(Z)V
    .locals 2

    .prologue
    .line 1702408
    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->l:Z

    if-ne v0, p1, :cond_0

    .line 1702409
    :goto_0
    return-void

    .line 1702410
    :cond_0
    iput-boolean p1, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->l:Z

    .line 1702411
    iget-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->j:LX/AhH;

    .line 1702412
    if-eqz p1, :cond_2

    const/16 v1, 0xbb8

    :goto_1
    iput v1, v0, LX/AhH;->l:I

    .line 1702413
    if-eqz p1, :cond_1

    .line 1702414
    invoke-virtual {v0}, LX/AhH;->a()V

    .line 1702415
    :cond_1
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->n()V

    goto :goto_0

    .line 1702416
    :cond_2
    iget v1, v0, LX/AhH;->i:I

    goto :goto_1
.end method

.method public setLandscape(Z)V
    .locals 2

    .prologue
    .line 1702401
    iput-boolean p1, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->o:Z

    .line 1702402
    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->o:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    iput v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->p:I

    .line 1702403
    iget-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->k:LX/3wu;

    iget v1, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->p:I

    invoke-virtual {v0, v1}, LX/3wu;->a(I)V

    .line 1702404
    iget-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->h:LX/AhD;

    iget v1, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->p:I

    .line 1702405
    iput v1, v0, LX/AhD;->e:I

    .line 1702406
    return-void

    .line 1702407
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public setListener(LX/AhE;)V
    .locals 0

    .prologue
    .line 1702399
    iput-object p1, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->q:LX/AhE;

    .line 1702400
    return-void
.end method

.method public setMinimizedMode(Z)V
    .locals 0

    .prologue
    .line 1702396
    iput-boolean p1, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->n:Z

    .line 1702397
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->requestLayout()V

    .line 1702398
    return-void
.end method

.method public setSuspended(Z)V
    .locals 1

    .prologue
    .line 1702391
    iget-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->j:LX/AhH;

    .line 1702392
    if-eqz p1, :cond_0

    .line 1702393
    invoke-static {v0}, LX/AhH;->e(LX/AhH;)V

    .line 1702394
    :goto_0
    return-void

    .line 1702395
    :cond_0
    invoke-virtual {v0}, LX/AhH;->a()V

    goto :goto_0
.end method

.method public setVideoId(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1702387
    iget-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->j:LX/AhH;

    .line 1702388
    iput-object p1, v0, LX/AhH;->a:Ljava/lang/String;

    .line 1702389
    iget p0, v0, LX/AhH;->i:I

    iput p0, v0, LX/AhH;->l:I

    .line 1702390
    return-void
.end method
