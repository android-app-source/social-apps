.class public Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/AhE;


# static fields
.field private static final b:LX/0Tn;


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;

.field private final d:Landroid/view/ViewStub;

.field private final e:Lcom/facebook/widget/text/BetterTextView;

.field private f:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1702335
    sget-object v0, LX/0Tm;->d:LX/0Tn;

    const-string v1, "whos_watching_nux_key"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->b:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1702285
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1702286
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1702287
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1702288
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1702289
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1702290
    const-class v0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;

    invoke-static {v0, p0}, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1702291
    const v0, 0x7f031621

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1702292
    const v0, 0x7f0d31bf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->c:Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;

    .line 1702293
    const v0, 0x7f0d31be

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->d:Landroid/view/ViewStub;

    .line 1702294
    const v0, 0x7f0d31bd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 1702295
    iget-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->b:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1702296
    iget-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->c:Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;

    .line 1702297
    iput-object p0, v0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->q:LX/AhE;

    .line 1702298
    iget-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->d:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->f:Landroid/view/View;

    .line 1702299
    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    return-void
.end method


# virtual methods
.method public final c(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1702300
    if-lez p1, :cond_0

    .line 1702301
    iget-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->f:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1702302
    iput-object v2, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->f:Landroid/view/View;

    .line 1702303
    iget-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->c:Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;

    .line 1702304
    iput-object v2, v0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->q:LX/AhE;

    .line 1702305
    iget-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->b:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1702306
    :cond_0
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 1702307
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomLinearLayout;->onLayout(ZIIII)V

    .line 1702308
    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->g:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    .line 1702309
    iget-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getMeasuredHeight()I

    move-result v0

    .line 1702310
    iget-object v1, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->f:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->f:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 1702311
    iget-object v1, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->f:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 1702312
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b0062

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    move v1, v2

    .line 1702313
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 1702314
    invoke-virtual {p0, v1}, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 1702315
    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v6

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v3

    sub-int v7, v3, v0

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->getChildCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v1, v3, :cond_1

    move v3, v2

    :goto_1
    sub-int/2addr v7, v3

    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v8

    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v9

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->getChildCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v1, v3, :cond_2

    move v3, v2

    :goto_2
    sub-int v3, v9, v3

    invoke-virtual {v5, v6, v7, v8, v3}, Landroid/view/View;->layout(IIII)V

    .line 1702316
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    move v3, v4

    .line 1702317
    goto :goto_1

    :cond_2
    add-int v3, v0, v4

    goto :goto_2

    .line 1702318
    :cond_3
    return-void
.end method

.method public setFullScreenMode(Z)V
    .locals 1

    .prologue
    .line 1702319
    iput-boolean p1, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->g:Z

    .line 1702320
    iget-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->c:Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;

    .line 1702321
    iput-boolean p1, v0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->m:Z

    .line 1702322
    return-void
.end method

.method public setIsAudioLive(Z)V
    .locals 2

    .prologue
    .line 1702323
    iget-object v1, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->e:Lcom/facebook/widget/text/BetterTextView;

    if-eqz p1, :cond_1

    const v0, 0x7f080c70

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 1702324
    iget-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1702325
    iget-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->f:Landroid/view/View;

    const v1, 0x7f0d31c2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    if-eqz p1, :cond_2

    const v1, 0x7f080c71

    :goto_1
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 1702326
    :cond_0
    return-void

    .line 1702327
    :cond_1
    const v0, 0x7f080c32

    goto :goto_0

    .line 1702328
    :cond_2
    const v1, 0x7f080c33

    goto :goto_1
.end method

.method public setTitleVisibility(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1702329
    iget-object v3, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->e:Lcom/facebook/widget/text/BetterTextView;

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1702330
    iget-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1702331
    iget-object v0, p0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->f:Landroid/view/View;

    if-eqz p1, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1702332
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 1702333
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1702334
    goto :goto_1
.end method
