.class public Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public final a:Lcom/facebook/fbui/glyph/GlyphView;

.field public final b:Lcom/facebook/resources/ui/FbTextView;

.field public final c:Lcom/facebook/resources/ui/FbTextView;

.field public final d:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;

.field public final e:Lcom/facebook/resources/ui/FbTextView;

.field public final f:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarBottomBarView;

.field public g:LX/Agr;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1701998
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1701999
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1702000
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1702001
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    .line 1702002
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1702003
    const v0, 0x7f030a1d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1702004
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;->setOrientation(I)V

    .line 1702005
    const v0, 0x7f0d1997

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1702006
    const v0, 0x7f0d1999

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1702007
    const v0, 0x7f0d199a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 1702008
    const v0, 0x7f0d199b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;->d:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;

    .line 1702009
    const v0, 0x7f0d199c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 1702010
    const v0, 0x7f0d199d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarBottomBarView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;->f:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarBottomBarView;

    .line 1702011
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/Agz;

    invoke-direct {v1, p0}, LX/Agz;-><init>(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1702012
    return-void
.end method


# virtual methods
.method public setLiveTipJarViewListener(LX/Agr;)V
    .locals 0

    .prologue
    .line 1702013
    iput-object p1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;->g:LX/Agr;

    .line 1702014
    return-void
.end method
