.class public Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# static fields
.field public static final a:[Ljava/lang/String;

.field public static final b:[Ljava/lang/String;

.field public static final c:[Ljava/lang/String;

.field public static final e:Ljava/lang/String;


# instance fields
.field public final d:[Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;

.field private final f:[LX/9Ug;

.field public final g:[LX/9Ug;

.field public final h:[LX/3K3;

.field public final i:[LX/3K3;

.field private final j:[LX/20u;

.field public k:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1701990
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "1"

    aput-object v1, v0, v2

    const-string v1, "3"

    aput-object v1, v0, v3

    const-string v1, "5"

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->a:[Ljava/lang/String;

    .line 1701991
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "option1a.json"

    aput-object v1, v0, v2

    const-string v1, "option2a.json"

    aput-object v1, v0, v3

    const-string v1, "option3a.json"

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->b:[Ljava/lang/String;

    .line 1701992
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "option1b.json"

    aput-object v1, v0, v2

    const-string v1, "option2b.json"

    aput-object v1, v0, v3

    const-string v1, "option3b.json"

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->c:[Ljava/lang/String;

    .line 1701993
    const-class v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1701903
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1701904
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1701988
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1701989
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 1701962
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1701963
    sget-object v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->a:[Ljava/lang/String;

    array-length v0, v0

    new-array v0, v0, [Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->d:[Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;

    .line 1701964
    sget-object v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->a:[Ljava/lang/String;

    array-length v0, v0

    new-array v0, v0, [LX/9Ug;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->f:[LX/9Ug;

    .line 1701965
    sget-object v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->a:[Ljava/lang/String;

    array-length v0, v0

    new-array v0, v0, [LX/9Ug;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->g:[LX/9Ug;

    .line 1701966
    sget-object v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->a:[Ljava/lang/String;

    array-length v0, v0

    new-array v0, v0, [LX/3K3;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->h:[LX/3K3;

    .line 1701967
    sget-object v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->a:[Ljava/lang/String;

    array-length v0, v0

    new-array v0, v0, [LX/3K3;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->i:[LX/3K3;

    .line 1701968
    sget-object v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->a:[Ljava/lang/String;

    array-length v0, v0

    new-array v0, v0, [LX/20u;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->j:[LX/20u;

    .line 1701969
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->setGravity(I)V

    .line 1701970
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->k:Landroid/content/res/Resources;

    .line 1701971
    const-string v0, "USD"

    invoke-static {v0}, LX/5fx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1701972
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    move v2, v3

    .line 1701973
    :goto_0
    sget-object v1, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->a:[Ljava/lang/String;

    array-length v1, v1

    if-ge v2, v1, :cond_0

    .line 1701974
    iget-object v5, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->d:[Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;

    const v1, 0x7f030a23

    invoke-virtual {v0, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;

    aput-object v1, v5, v2

    .line 1701975
    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->d:[Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;->b:Lcom/facebook/widget/text/BetterTextView;

    new-array v5, v8, [Ljava/lang/CharSequence;

    aput-object v4, v5, v3

    sget-object v6, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->a:[Ljava/lang/String;

    aget-object v6, v6, v2

    aput-object v6, v5, v7

    invoke-static {v5}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1701976
    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->d:[Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;

    aget-object v1, v1, v2

    invoke-virtual {p0, v1}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->addView(Landroid/view/View;)V

    .line 1701977
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1701978
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->d:[Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;

    aget-object v0, v0, v3

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;->a:Landroid/widget/ImageView;

    const v1, 0x7f021a29

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1701979
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->d:[Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;

    aget-object v0, v0, v7

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;->a:Landroid/widget/ImageView;

    const v1, 0x7f021a2a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1701980
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->d:[Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;

    aget-object v0, v0, v8

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;->a:Landroid/widget/ImageView;

    const v1, 0x7f021a2b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1701981
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_3

    move v0, v3

    .line 1701982
    :goto_1
    sget-object v1, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->a:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 1701983
    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->h:[LX/3K3;

    aget-object v1, v1, v0

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->i:[LX/3K3;

    aget-object v1, v1, v0

    if-nez v1, :cond_2

    .line 1701984
    :cond_1
    new-instance v1, LX/Agy;

    invoke-direct {v1, p0}, LX/Agy;-><init>(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;)V

    new-array v2, v7, [Ljava/lang/Integer;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, LX/Agy;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1701985
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1701986
    :cond_2
    invoke-static {p0, v0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->setKFImage(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;I)V

    goto :goto_2

    .line 1701987
    :cond_3
    return-void
.end method

.method private static a(LX/4mQ;)LX/4mQ;
    .locals 2

    .prologue
    .line 1701955
    const v0, 0x3f4ccccd    # 0.8f

    .line 1701956
    iput v0, p0, LX/4mQ;->d:F

    .line 1701957
    move-object v0, p0

    .line 1701958
    const/4 v1, 0x0

    .line 1701959
    iput v1, v0, LX/4mQ;->g:F

    .line 1701960
    move-object v0, v0

    .line 1701961
    return-object v0
.end method

.method public static setKFImage(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1701944
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->h:[LX/3K3;

    aget-object v0, v0, p1

    if-nez v0, :cond_0

    .line 1701945
    :goto_0
    return-void

    .line 1701946
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->d:[Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;

    aget-object v0, v0, p1

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;->a:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1701947
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->d:[Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;

    aget-object v0, v0, p1

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1701948
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->g:[LX/9Ug;

    aput-object v2, v0, p1

    .line 1701949
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->f:[LX/9Ug;

    aput-object v2, v0, p1

    .line 1701950
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->f:[LX/9Ug;

    new-instance v1, LX/9Uk;

    invoke-direct {v1}, LX/9Uk;-><init>()V

    iget-object v2, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->h:[LX/3K3;

    aget-object v2, v2, p1

    .line 1701951
    iput-object v2, v1, LX/9Uk;->a:LX/3K3;

    .line 1701952
    move-object v1, v1

    .line 1701953
    invoke-virtual {v1}, LX/9Uk;->a()LX/9Ug;

    move-result-object v1

    aput-object v1, v0, p1

    .line 1701954
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->d:[Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;

    aget-object v0, v0, p1

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;->a:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->f:[LX/9Ug;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method


# virtual methods
.method public final c(I)V
    .locals 2

    .prologue
    .line 1701940
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->j:[LX/20u;

    const/4 v1, 0x0

    aput-object v1, v0, p1

    .line 1701941
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->f:[LX/9Ug;

    aget-object v0, v0, p1

    if-eqz v0, :cond_0

    .line 1701942
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->f:[LX/9Ug;

    aget-object v0, v0, p1

    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->j:[LX/20u;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, LX/9Ug;->a(LX/20u;)V

    .line 1701943
    :cond_0
    return-void
.end method

.method public final d(I)V
    .locals 3

    .prologue
    .line 1701928
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->i:[LX/3K3;

    aget-object v0, v0, p1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->g:[LX/9Ug;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->g:[LX/9Ug;

    aget-object v0, v0, p1

    if-eqz v0, :cond_0

    .line 1701929
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->g:[LX/9Ug;

    aget-object v0, v0, p1

    .line 1701930
    iget-object v1, v0, LX/9Ug;->e:LX/9Uh;

    invoke-virtual {v1}, LX/9Uh;->d()V

    .line 1701931
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->g:[LX/9Ug;

    new-instance v1, LX/9Uk;

    invoke-direct {v1}, LX/9Uk;-><init>()V

    iget-object v2, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->i:[LX/3K3;

    aget-object v2, v2, p1

    .line 1701932
    iput-object v2, v1, LX/9Uk;->a:LX/3K3;

    .line 1701933
    move-object v1, v1

    .line 1701934
    invoke-virtual {v1}, LX/9Uk;->a()LX/9Ug;

    move-result-object v1

    aput-object v1, v0, p1

    .line 1701935
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->d:[Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;

    aget-object v0, v0, p1

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;->a:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->g:[LX/9Ug;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1701936
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->f:[LX/9Ug;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->f:[LX/9Ug;

    aget-object v0, v0, p1

    if-eqz v0, :cond_1

    .line 1701937
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->j:[LX/20u;

    const/4 v1, 0x0

    aput-object v1, v0, p1

    .line 1701938
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->f:[LX/9Ug;

    aget-object v0, v0, p1

    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->j:[LX/20u;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, LX/9Ug;->a(LX/20u;)V

    .line 1701939
    :cond_1
    return-void
.end method

.method public final e(I)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const v3, 0x3f666666    # 0.9f

    .line 1701915
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->h:[LX/3K3;

    aget-object v0, v0, p1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->i:[LX/3K3;

    aget-object v0, v0, p1

    if-eqz v0, :cond_0

    .line 1701916
    invoke-static {p0, p1}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->setKFImage(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;I)V

    .line 1701917
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->j:[LX/20u;

    new-instance v1, LX/Agx;

    invoke-direct {v1, p0, p1}, LX/Agx;-><init>(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;I)V

    aput-object v1, v0, p1

    .line 1701918
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->f:[LX/9Ug;

    aget-object v0, v0, p1

    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->j:[LX/20u;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, LX/9Ug;->a(LX/20u;)V

    .line 1701919
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->f:[LX/9Ug;

    aget-object v0, v0, p1

    invoke-virtual {v0}, LX/9Ug;->a()V

    .line 1701920
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->f:[LX/9Ug;

    aget-object v0, v0, p1

    invoke-virtual {v0}, LX/9Ug;->c()V

    .line 1701921
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->d:[Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;

    aget-object v0, v0, p1

    .line 1701922
    const-string v1, "scaleX"

    invoke-static {v0, v1, v3, v4}, LX/4mQ;->a(Ljava/lang/Object;Ljava/lang/String;FF)LX/4mQ;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->a(LX/4mQ;)LX/4mQ;

    move-result-object v1

    .line 1701923
    const-string v2, "scaleY"

    invoke-static {v0, v2, v3, v4}, LX/4mQ;->a(Ljava/lang/Object;Ljava/lang/String;FF)LX/4mQ;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->a(LX/4mQ;)LX/4mQ;

    move-result-object v0

    .line 1701924
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1701925
    const/4 v3, 0x2

    new-array v3, v3, [Landroid/animation/Animator;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v1, 0x1

    aput-object v0, v3, v1

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 1701926
    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    .line 1701927
    return-void
.end method

.method public final onMeasure(II)V
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 1701905
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;->onMeasure(II)V

    .line 1701906
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 1701907
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b0485

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1701908
    sget-object v2, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->a:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, 0x1

    mul-int/2addr v0, v2

    sub-int v0, v1, v0

    sget-object v2, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->a:[Ljava/lang/String;

    array-length v2, v2

    div-int v2, v0, v2

    .line 1701909
    const/4 v0, 0x0

    :goto_0
    sget-object v3, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->a:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 1701910
    iget-object v3, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->d:[Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;

    aget-object v3, v3, v0

    invoke-static {v2, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v2, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;->measure(II)V

    .line 1701911
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1701912
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b048e

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1701913
    mul-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v2

    invoke-virtual {p0, v1, v0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->setMeasuredDimension(II)V

    .line 1701914
    return-void
.end method
