.class public Lcom/facebook/facecastdisplay/tipjar/LiveTipJarBottomBarView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public final a:Lcom/facebook/resources/ui/FbTextView;

.field public final b:Lcom/facebook/resources/ui/FbTextView;

.field public final c:Lcom/facebook/resources/ui/FbButton;

.field public final d:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1701424
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarBottomBarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1701425
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1701426
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarBottomBarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1701427
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1701428
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1701429
    const v0, 0x7f030a1e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1701430
    invoke-virtual {p0, v2}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarBottomBarView;->setOrientation(I)V

    .line 1701431
    const v0, 0x7f0d199e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarBottomBarView;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 1701432
    const v0, 0x7f0d199f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarBottomBarView;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1701433
    const v0, 0x7f0d19a1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarBottomBarView;->c:Lcom/facebook/resources/ui/FbButton;

    .line 1701434
    const v0, 0x7f0d19a0    # 1.875542E38f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarBottomBarView;->d:Landroid/view/View;

    .line 1701435
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarBottomBarView;->a:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1701436
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarBottomBarView;->c:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 1701437
    return-void
.end method
