.class public Lcom/facebook/facecastdisplay/tipjar/LiveTipJarPaymentStatusView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field public final a:Lcom/facebook/fbui/glyph/GlyphView;

.field public final b:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1701823
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarPaymentStatusView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1701824
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1701825
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarPaymentStatusView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1701826
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1701827
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1701828
    const v0, 0x7f030a21

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1701829
    const v0, 0x7f0d1913

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarPaymentStatusView;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1701830
    const v0, 0x7f0d19ac

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarPaymentStatusView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1701831
    return-void
.end method

.method private setBackgroundForPaymentStatusView(Landroid/graphics/drawable/ColorDrawable;)V
    .locals 2

    .prologue
    .line 1701832
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 1701833
    invoke-virtual {p0, p1}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarPaymentStatusView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1701834
    :goto_0
    return-void

    .line 1701835
    :cond_0
    invoke-virtual {p0, p1}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarPaymentStatusView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method


# virtual methods
.method public setPaymentStatus(LX/Agv;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1701836
    sget-object v0, LX/Agu;->a:[I

    invoke-virtual {p1}, LX/Agv;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1701837
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarPaymentStatusView;->invalidate()V

    .line 1701838
    return-void

    .line 1701839
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarPaymentStatusView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1701840
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarPaymentStatusView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarPaymentStatusView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080bce

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1701841
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarPaymentStatusView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a0377

    invoke-static {v1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-direct {p0, v0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarPaymentStatusView;->setBackgroundForPaymentStatusView(Landroid/graphics/drawable/ColorDrawable;)V

    goto :goto_0

    .line 1701842
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarPaymentStatusView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1701843
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarPaymentStatusView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    const v1, 0x7f0207db

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 1701844
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarPaymentStatusView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarPaymentStatusView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080bcf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1701845
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarPaymentStatusView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a0378

    invoke-static {v1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-direct {p0, v0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarPaymentStatusView;->setBackgroundForPaymentStatusView(Landroid/graphics/drawable/ColorDrawable;)V

    goto :goto_0

    .line 1701846
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarPaymentStatusView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1701847
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarPaymentStatusView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    const v1, 0x7f02081d

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 1701848
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarPaymentStatusView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarPaymentStatusView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080bc7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1701849
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarPaymentStatusView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a0379

    invoke-static {v1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-direct {p0, v0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarPaymentStatusView;->setBackgroundForPaymentStatusView(Landroid/graphics/drawable/ColorDrawable;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
