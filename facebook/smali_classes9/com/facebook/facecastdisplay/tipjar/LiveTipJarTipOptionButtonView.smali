.class public Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public final a:Landroid/widget/ImageView;

.field public final b:Lcom/facebook/widget/text/BetterTextView;

.field public final c:Landroid/view/View;

.field private d:Landroid/graphics/Path;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1701854
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1701855
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1701864
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1701865
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1701866
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1701867
    const v0, 0x7f030a24

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1701868
    const v0, 0x7f0d19af

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;->a:Landroid/widget/ImageView;

    .line 1701869
    const v0, 0x7f0d19b0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 1701870
    const v0, 0x7f0d19b1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;->c:Landroid/view/View;

    .line 1701871
    return-void
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1701861
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;->d:Landroid/graphics/Path;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    .line 1701862
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 1701863
    return-void
.end method

.method public final onMeasure(II)V
    .locals 5

    .prologue
    const/high16 v4, 0x41600000    # 14.0f

    const/4 v3, 0x0

    .line 1701856
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 1701857
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;->d:Landroid/graphics/Path;

    .line 1701858
    new-instance v0, Landroid/graphics/RectF;

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    int-to-float v2, v2

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1701859
    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;->d:Landroid/graphics/Path;

    sget-object v2, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v1, v0, v4, v4, v2}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Path$Direction;)V

    .line 1701860
    return-void
.end method
