.class public Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;
.implements LX/Agr;
.implements LX/Ags;
.implements LX/6Ex;


# static fields
.field public static final n:LX/0Tn;


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field public C:F

.field public D:LX/2oj;

.field public E:LX/3Gu;

.field private F:Landroid/os/Handler;

.field private G:LX/03R;

.field private H:Lcom/facebook/payments/auth/model/NuxFollowUpAction;

.field public I:Z

.field public final J:LX/6qh;

.field public a:LX/Ah3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/6r9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/1b4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/Ah5;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/6nf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/6o6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/6o9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Agw;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/Ah7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private o:Landroid/view/View;

.field private p:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;

.field public q:Landroid/view/View;

.field public r:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarPaymentStatusView;

.field private s:Landroid/view/View;

.field public t:LX/Ah2;

.field public u:Lcom/facebook/payments/checkout/CheckoutParams;

.field public v:Lcom/facebook/payments/checkout/model/CheckoutData;

.field public w:LX/6qd;

.field public x:LX/6Dk;

.field public y:LX/6tv;

.field public z:LX/6Du;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1701717
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "payment_count_after_click_always_require"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->n:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1701808
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1701809
    new-instance v0, LX/Agh;

    invoke-direct {v0, p0}, LX/Agh;-><init>(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;)V

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->J:LX/6qh;

    return-void
.end method

.method private a(LX/Agv;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1701800
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->r:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarPaymentStatusView;

    invoke-virtual {v0, v2}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarPaymentStatusView;->setVisibility(I)V

    .line 1701801
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->r:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarPaymentStatusView;

    invoke-virtual {v0, p1}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarPaymentStatusView;->setPaymentStatus(LX/Agv;)V

    .line 1701802
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->o:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1701803
    sget-object v0, LX/Agv;->PROCESSING:LX/Agv;

    if-eq p1, v0, :cond_0

    .line 1701804
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->d()V

    .line 1701805
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->F:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment$11;

    invoke-direct {v1, p0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment$11;-><init>(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;)V

    const-wide/16 v2, 0x5dc

    const v4, -0x4570da4

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1701806
    :goto_0
    return-void

    .line 1701807
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->s:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;LX/Ah3;Ljava/util/concurrent/Executor;LX/6r9;Lcom/facebook/content/SecureContextHelper;LX/1Ck;LX/1b4;LX/Ah5;LX/6nf;LX/6o6;LX/6o9;LX/0Ot;LX/Ah7;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;",
            "LX/Ah3;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/facebook/payments/checkout/CheckoutManager;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/1Ck;",
            "LX/1b4;",
            "LX/Ah5;",
            "LX/6nf;",
            "LX/6o6;",
            "LX/6o9;",
            "LX/0Ot",
            "<",
            "LX/Agw;",
            ">;",
            "LX/Ah7;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1701799
    iput-object p1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->a:LX/Ah3;

    iput-object p2, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->b:Ljava/util/concurrent/Executor;

    iput-object p3, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->c:LX/6r9;

    iput-object p4, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->d:Lcom/facebook/content/SecureContextHelper;

    iput-object p5, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->e:LX/1Ck;

    iput-object p6, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->f:LX/1b4;

    iput-object p7, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->g:LX/Ah5;

    iput-object p8, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->h:LX/6nf;

    iput-object p9, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->i:LX/6o6;

    iput-object p10, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->j:LX/6o9;

    iput-object p11, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->k:LX/0Ot;

    iput-object p12, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->l:LX/Ah7;

    iput-object p13, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->m:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 15

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v13

    move-object v0, p0

    check-cast v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    const-class v1, LX/Ah3;

    invoke-interface {v13, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/Ah3;

    invoke-static {v13}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    invoke-static {v13}, LX/6r9;->a(LX/0QB;)LX/6r9;

    move-result-object v3

    check-cast v3, LX/6r9;

    invoke-static {v13}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v13}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    invoke-static {v13}, LX/1b4;->b(LX/0QB;)LX/1b4;

    move-result-object v6

    check-cast v6, LX/1b4;

    invoke-static {v13}, LX/Ah5;->a(LX/0QB;)LX/Ah5;

    move-result-object v7

    check-cast v7, LX/Ah5;

    const-class v8, LX/6nf;

    invoke-interface {v13, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/6nf;

    invoke-static {v13}, LX/6o6;->b(LX/0QB;)LX/6o6;

    move-result-object v9

    check-cast v9, LX/6o6;

    invoke-static {v13}, LX/6o9;->b(LX/0QB;)LX/6o9;

    move-result-object v10

    check-cast v10, LX/6o9;

    const/16 v11, 0x1c20

    invoke-static {v13, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static {v13}, LX/Ah7;->b(LX/0QB;)LX/Ah7;

    move-result-object v12

    check-cast v12, LX/Ah7;

    const/16 v14, 0xf9a

    invoke-static {v13, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static/range {v0 .. v13}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->a(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;LX/Ah3;Ljava/util/concurrent/Executor;LX/6r9;Lcom/facebook/content/SecureContextHelper;LX/1Ck;LX/1b4;LX/Ah5;LX/6nf;LX/6o6;LX/6o9;LX/0Ot;LX/Ah7;LX/0Ot;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;LX/Agq;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 2

    .prologue
    .line 1701797
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->e:LX/1Ck;

    new-instance v1, LX/Ago;

    invoke-direct {v1, p0}, LX/Ago;-><init>(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;)V

    invoke-virtual {v0, p1, p2, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1701798
    return-void
.end method

.method public static m(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 1701755
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v0

    .line 1701756
    sget-object v1, LX/6rp;->PAYMENT_METHOD:LX/6rp;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1701757
    sget-object v1, LX/6rp;->CONTACT_INFO:LX/6rp;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1701758
    iget-boolean v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->I:Z

    if-nez v1, :cond_0

    .line 1701759
    sget-object v1, LX/6rp;->AUTHENTICATION:LX/6rp;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1701760
    :cond_0
    sget-object v1, LX/6xY;->CHECKOUT:LX/6xY;

    invoke-static {v1}, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;->a(LX/6xY;)LX/6xd;

    move-result-object v1

    invoke-virtual {v1}, LX/6xd;->a()Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)LX/6qU;

    move-result-object v1

    invoke-virtual {v1}, LX/6qU;->a()Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    move-result-object v1

    .line 1701761
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v2

    .line 1701762
    sget-object v3, LX/6vb;->EMAIL:LX/6vb;

    invoke-virtual {v2, v3}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1701763
    new-instance v3, LX/0m9;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v4}, LX/0m9;-><init>(LX/0mC;)V

    .line 1701764
    const-string v4, "video_id"

    iget-object v5, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->B:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1701765
    const-string v4, "video_time_offset"

    iget v5, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->C:F

    invoke-virtual {v3, v4, v5}, LX/0m9;->a(Ljava/lang/String;F)LX/0m9;

    .line 1701766
    sget-object v4, LX/6qw;->TIP_JAR:LX/6qw;

    sget-object v5, LX/6xg;->NMOR_TIP_JAR:LX/6xg;

    invoke-virtual {v0}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    invoke-static {v4, v5, v0, v1}, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a(LX/6qw;LX/6xg;LX/0Rf;Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;)LX/6qZ;

    move-result-object v0

    .line 1701767
    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->t:LX/Ah2;

    invoke-virtual {v1}, LX/Ah2;->a()Ljava/lang/String;

    move-result-object v1

    .line 1701768
    if-eqz v1, :cond_1

    .line 1701769
    const-string v4, "tip amount"

    new-instance v5, Lcom/facebook/payments/currency/CurrencyAmount;

    const-string v6, "USD"

    new-instance v7, Ljava/math/BigDecimal;

    invoke-direct {v7, v1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-direct {v5, v6, v7}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;)V

    invoke-static {v4, v5}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a(Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;)Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 1701770
    iput-object v1, v0, LX/6qZ;->h:LX/0Px;

    .line 1701771
    :cond_1
    const/4 v1, 0x0

    .line 1701772
    iput-boolean v1, v0, LX/6qZ;->A:Z

    .line 1701773
    move-object v1, v0

    .line 1701774
    iput-boolean v8, v1, LX/6qZ;->v:Z

    .line 1701775
    move-object v1, v1

    .line 1701776
    invoke-virtual {v2}, LX/0cA;->b()LX/0Rf;

    move-result-object v2

    .line 1701777
    iput-object v2, v1, LX/6qZ;->t:LX/0Rf;

    .line 1701778
    move-object v1, v1

    .line 1701779
    iget-object v2, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->A:Ljava/lang/String;

    .line 1701780
    iput-object v2, v1, LX/6qZ;->r:Ljava/lang/String;

    .line 1701781
    move-object v1, v1

    .line 1701782
    invoke-static {}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->newBuilder()LX/6wu;

    move-result-object v2

    .line 1701783
    iput-boolean v8, v2, LX/6wu;->d:Z

    .line 1701784
    move-object v2, v2

    .line 1701785
    sget-object v4, LX/6ws;->SLIDE_RIGHT:LX/6ws;

    .line 1701786
    iput-object v4, v2, LX/6wu;->a:LX/6ws;

    .line 1701787
    move-object v2, v2

    .line 1701788
    sget-object v4, LX/73i;->PAYMENTS_WHITE:LX/73i;

    .line 1701789
    iput-object v4, v2, LX/6wu;->b:LX/73i;

    .line 1701790
    move-object v2, v2

    .line 1701791
    invoke-virtual {v2}, LX/6wu;->e()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v2

    .line 1701792
    iput-object v2, v1, LX/6qZ;->k:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1701793
    move-object v1, v1

    .line 1701794
    iput-object v3, v1, LX/6qZ;->B:LX/0m9;

    .line 1701795
    invoke-virtual {v0}, LX/6qZ;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->u:Lcom/facebook/payments/checkout/CheckoutParams;

    .line 1701796
    return-void
.end method

.method public static u(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1701722
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->y:LX/6tv;

    iget-object v3, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->v:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-virtual {v0, v3}, LX/6tv;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)V

    .line 1701723
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->y:LX/6tv;

    invoke-static {p0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->v(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;)LX/6ng;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/6tv;->a(LX/6ng;)V

    .line 1701724
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->y:LX/6tv;

    invoke-virtual {v0}, LX/6tv;->a()V

    .line 1701725
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->v:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0}, Lcom/facebook/payments/checkout/model/CheckoutData;->t()Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    move-result-object v0

    .line 1701726
    if-eqz v0, :cond_1

    .line 1701727
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->v:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0}, Lcom/facebook/payments/checkout/model/CheckoutData;->s()LX/0am;

    move-result-object v0

    invoke-static {v0}, LX/47j;->a(LX/0am;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 1701728
    :goto_0
    iget-object v3, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->G:LX/03R;

    sget-object v4, LX/03R;->NO:LX/03R;

    if-ne v3, v4, :cond_0

    if-eqz v0, :cond_0

    .line 1701729
    iget-object v3, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->v:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v3}, Lcom/facebook/payments/checkout/model/CheckoutData;->e()Lcom/facebook/payments/model/PaymentsPin;

    move-result-object v3

    invoke-interface {v3}, Lcom/facebook/payments/model/PaymentsPin;->a()LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->isPresent()Z

    move-result v4

    .line 1701730
    iget-object v3, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->i:LX/6o6;

    invoke-virtual {v3}, LX/6o6;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->j:LX/6o9;

    invoke-virtual {v3}, LX/6o9;->a()Z

    move-result v3

    if-nez v3, :cond_3

    move v3, v1

    .line 1701731
    :goto_1
    if-nez v3, :cond_4

    if-nez v4, :cond_4

    .line 1701732
    :goto_2
    invoke-static {}, Lcom/facebook/payments/auth/model/NuxFollowUpAction;->a()LX/5fs;

    move-result-object v5

    .line 1701733
    iput-boolean v3, v5, LX/5fs;->a:Z

    .line 1701734
    move-object v3, v5

    .line 1701735
    iput-boolean v4, v3, LX/5fs;->d:Z

    .line 1701736
    move-object v3, v3

    .line 1701737
    iput-boolean v1, v3, LX/5fs;->b:Z

    .line 1701738
    move-object v1, v3

    .line 1701739
    iput-boolean v2, v1, LX/5fs;->c:Z

    .line 1701740
    move-object v1, v1

    .line 1701741
    invoke-virtual {v1}, LX/5fs;->a()Lcom/facebook/payments/auth/model/NuxFollowUpAction;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->H:Lcom/facebook/payments/auth/model/NuxFollowUpAction;

    .line 1701742
    :cond_0
    if-eqz v0, :cond_5

    sget-object v1, LX/03R;->YES:LX/03R;

    :goto_3
    iput-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->G:LX/03R;

    .line 1701743
    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->c:LX/6r9;

    iget-object v2, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->v:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v2}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v1, v2}, LX/6r9;->e(LX/6qw;)LX/6E0;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->v:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1, v2}, LX/6E0;->h(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;

    move-result-object v2

    .line 1701744
    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->v:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1}, Lcom/facebook/payments/checkout/model/CheckoutData;->s()LX/0am;

    move-result-object v1

    invoke-static {v1}, LX/47j;->a(LX/0am;)Z

    move-result v1

    if-eqz v1, :cond_6

    const/16 v1, 0x65

    .line 1701745
    :goto_4
    new-instance v3, LX/Agp;

    invoke-direct {v3, p0, v0, v2, v1}, LX/Agp;-><init>(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;ZLcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;I)V

    .line 1701746
    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->v:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0}, Lcom/facebook/payments/checkout/model/CheckoutData;->s()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    .line 1701747
    :goto_5
    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->t:LX/Ah2;

    invoke-virtual {v1, v0, v3}, LX/Ah2;->a(Lcom/facebook/payments/paymentmethods/model/PaymentMethod;Landroid/view/View$OnClickListener;)V

    .line 1701748
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 1701749
    goto/16 :goto_0

    :cond_3
    move v3, v2

    .line 1701750
    goto :goto_1

    :cond_4
    move v1, v2

    .line 1701751
    goto :goto_2

    .line 1701752
    :cond_5
    sget-object v1, LX/03R;->NO:LX/03R;

    goto :goto_3

    .line 1701753
    :cond_6
    const/16 v1, 0x64

    goto :goto_4

    .line 1701754
    :cond_7
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public static v(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;)LX/6ng;
    .locals 1

    .prologue
    .line 1701718
    invoke-static {}, LX/6nh;->newBuilder()LX/6ng;

    move-result-object v0

    .line 1701719
    iput-object p0, v0, LX/6ng;->a:Lcom/facebook/base/fragment/FbFragment;

    .line 1701720
    move-object v0, v0

    .line 1701721
    return-object v0
.end method


# virtual methods
.method public final S_()Z
    .locals 1

    .prologue
    .line 1701715
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->x:LX/6Dk;

    invoke-interface {v0}, LX/6Dk;->a()V

    .line 1701716
    const/4 v0, 0x1

    return v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1701713
    sget-object v0, LX/Agv;->PROCESSING:LX/Agv;

    invoke-direct {p0, v0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->a(LX/Agv;)V

    .line 1701714
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 1701810
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->g:LX/Ah5;

    invoke-virtual {v0, p1}, LX/Ah5;->a(I)V

    .line 1701811
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1701546
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1701547
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1701548
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->F:Landroid/os/Handler;

    .line 1701549
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->G:LX/03R;

    .line 1701550
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->I:Z

    .line 1701551
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->H:Lcom/facebook/payments/auth/model/NuxFollowUpAction;

    .line 1701552
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;)V
    .locals 6

    .prologue
    .line 1701556
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->g:LX/Ah5;

    invoke-virtual {v0}, LX/Ah5;->c()V

    .line 1701557
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Agw;

    .line 1701558
    iget-object v1, v0, LX/Agw;->a:LX/3RX;

    const v2, 0x7f07008d

    iget-object v3, v0, LX/Agw;->b:LX/3RZ;

    invoke-virtual {v3}, LX/3RZ;->a()I

    move-result v3

    const p1, 0x3e19999a    # 0.15f

    invoke-virtual {v1, v2, v3, p1}, LX/3RX;->a(IIF)LX/7Cb;

    .line 1701559
    sget-object v0, LX/Agv;->SUCCEED:LX/Agv;

    invoke-direct {p0, v0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->a(LX/Agv;)V

    .line 1701560
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->H:Lcom/facebook/payments/auth/model/NuxFollowUpAction;

    if-eqz v0, :cond_1

    .line 1701561
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->h:LX/6nf;

    .line 1701562
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v1, v1

    .line 1701563
    new-instance v3, LX/6ne;

    .line 1701564
    new-instance p1, LX/6oZ;

    const-class v2, Landroid/content/Context;

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/6pC;->a(LX/0QB;)LX/6pC;

    move-result-object v5

    check-cast v5, LX/6pC;

    invoke-direct {p1, v2, v4, v5}, LX/6oZ;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/6pC;)V

    .line 1701565
    move-object v2, p1

    .line 1701566
    check-cast v2, LX/6oZ;

    invoke-direct {v3, v2, v1}, LX/6ne;-><init>(LX/6oZ;LX/0gc;)V

    .line 1701567
    move-object v0, v3

    .line 1701568
    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->H:Lcom/facebook/payments/auth/model/NuxFollowUpAction;

    .line 1701569
    iget-boolean v2, v1, Lcom/facebook/payments/auth/model/NuxFollowUpAction;->b:Z

    move v2, v2

    .line 1701570
    if-eqz v2, :cond_3

    iget-object v2, v0, LX/6ne;->b:LX/0gc;

    if-eqz v2, :cond_3

    .line 1701571
    iget-boolean v2, v1, Lcom/facebook/payments/auth/model/NuxFollowUpAction;->e:Z

    move v2, v2

    .line 1701572
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1701573
    const-string v4, "is_pin_present"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1701574
    new-instance v4, Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;

    invoke-direct {v4}, Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;-><init>()V

    .line 1701575
    invoke-virtual {v4, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1701576
    move-object v2, v4

    .line 1701577
    iget-object v3, v0, LX/6ne;->b:LX/0gc;

    const-string v4, "nux_fingerprint_dialog"

    invoke-virtual {v2, v3, v4}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1701578
    :cond_0
    :goto_0
    return-void

    .line 1701579
    :cond_1
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->f:LX/1b4;

    invoke-virtual {v0}, LX/1b4;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 1701580
    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->I:Z

    if-eqz v0, :cond_4

    .line 1701581
    :goto_1
    move v0, v1

    .line 1701582
    if-eqz v0, :cond_0

    .line 1701583
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1701584
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->v:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0}, Lcom/facebook/payments/checkout/model/CheckoutData;->e()Lcom/facebook/payments/model/PaymentsPin;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->v:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0}, Lcom/facebook/payments/checkout/model/CheckoutData;->e()Lcom/facebook/payments/model/PaymentsPin;

    move-result-object v0

    invoke-interface {v0}, Lcom/facebook/payments/model/PaymentsPin;->a()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1701585
    :cond_2
    :goto_2
    goto :goto_0

    .line 1701586
    :cond_3
    iget-boolean v2, v1, Lcom/facebook/payments/auth/model/NuxFollowUpAction;->c:Z

    move v2, v2

    .line 1701587
    if-eqz v2, :cond_0

    .line 1701588
    iget-object v2, v0, LX/6ne;->a:LX/6oZ;

    .line 1701589
    new-instance v3, LX/31Y;

    iget-object v4, v2, LX/6oZ;->a:Landroid/content/Context;

    invoke-direct {v3, v4}, LX/31Y;-><init>(Landroid/content/Context;)V

    const v4, 0x7f081dec

    invoke-virtual {v3, v4}, LX/0ju;->a(I)LX/0ju;

    move-result-object v3

    const v4, 0x7f081dee

    invoke-virtual {v3, v4}, LX/0ju;->b(I)LX/0ju;

    move-result-object v3

    const v4, 0x7f081ded

    new-instance v0, LX/6oY;

    invoke-direct {v0, v2}, LX/6oY;-><init>(LX/6oZ;)V

    invoke-virtual {v3, v4, v0}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v3

    const v4, 0x7f080022

    new-instance v0, LX/6oX;

    invoke-direct {v0, v2}, LX/6oX;-><init>(LX/6oZ;)V

    invoke-virtual {v3, v4, v0}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v3

    invoke-virtual {v3}, LX/0ju;->a()LX/2EJ;

    move-result-object v3

    move-object v2, v3

    .line 1701590
    invoke-virtual {v2}, LX/2EJ;->show()V

    goto :goto_0

    .line 1701591
    :cond_4
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->n:LX/0Tn;

    invoke-interface {v0, v2, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v2

    .line 1701592
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->f:LX/1b4;

    .line 1701593
    iget-object v3, v0, LX/1b4;->a:LX/0ad;

    sget v4, LX/1v6;->T:I

    const/16 v5, 0xa

    invoke-interface {v3, v4, v5}, LX/0ad;->a(II)I

    move-result v3

    move v0, v3

    .line 1701594
    rem-int v0, v2, v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    move v1, v0

    .line 1701595
    :cond_5
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v3, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->n:LX/0Tn;

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v0, v3, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto/16 :goto_1

    .line 1701596
    :cond_6
    new-instance v0, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v2

    .line 1701597
    const-string v0, "USD"

    invoke-static {v0}, LX/5fx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1701598
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "15"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1701599
    const v1, 0x7f080bd2

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {p0, v1, v3}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/2EJ;->setTitle(Ljava/lang/CharSequence;)V

    .line 1701600
    const v1, 0x7f080bd3

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {p0, v1, v3}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/2EJ;->a(Ljava/lang/CharSequence;)V

    .line 1701601
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030a22

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 1701602
    const v0, 0x7f0d19ad

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 1701603
    const v1, 0x7f0d19ae

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 1701604
    new-instance v4, LX/Agf;

    invoke-direct {v4, p0, v2}, LX/Agf;-><init>(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;LX/2EJ;)V

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1701605
    new-instance v0, LX/Agg;

    invoke-direct {v0, p0, v2}, LX/Agg;-><init>(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;LX/2EJ;)V

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1701606
    invoke-virtual {v2, v3}, LX/2EJ;->a(Landroid/view/View;)V

    .line 1701607
    invoke-virtual {v2}, LX/2EJ;->show()V

    goto/16 :goto_2
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1701608
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1701553
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->g:LX/Ah5;

    invoke-virtual {v0}, LX/Ah5;->d()V

    .line 1701554
    sget-object v0, LX/Agv;->FAILED:LX/Agv;

    invoke-direct {p0, v0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->a(LX/Agv;)V

    .line 1701555
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1701609
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->d()V

    .line 1701610
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 1701611
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->g:LX/Ah5;

    invoke-virtual {v0}, LX/Ah5;->b()V

    .line 1701612
    invoke-static {p0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->m(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;)V

    .line 1701613
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->w:LX/6qd;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->v:Lcom/facebook/payments/checkout/model/CheckoutData;

    iget-object v2, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->u:Lcom/facebook/payments/checkout/CheckoutParams;

    invoke-interface {v2}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/CheckoutCommonParams;)V

    .line 1701614
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->w:LX/6qd;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->v:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, v1}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1701615
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->w:LX/6qd;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->v:Lcom/facebook/payments/checkout/model/CheckoutData;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Z)V

    .line 1701616
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 1701617
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v0, v0

    .line 1701618
    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->f:LX/1b4;

    invoke-virtual {v1}, LX/1b4;->o()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1701619
    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->o:Landroid/view/View;

    const-string v2, "alpha"

    const/4 v3, 0x2

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 1701620
    const-wide/16 v2, 0xc8

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    .line 1701621
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f040068

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 1701622
    new-instance v2, LX/Agd;

    invoke-direct {v2, p0, v0}, LX/Agd;-><init>(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;LX/0gc;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1701623
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->q:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 1701624
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->q:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1701625
    :cond_1
    return-void

    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 1701626
    packed-switch p1, :pswitch_data_0

    .line 1701627
    :pswitch_0
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1701628
    :goto_0
    return-void

    .line 1701629
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->D:LX/2oj;

    new-instance v1, LX/2qa;

    sget-object v2, LX/04g;->BY_DIALOG:LX/04g;

    invoke-direct {v1, v2}, LX/2qa;-><init>(LX/04g;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 1701630
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->x:LX/6Dk;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->v:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, v1, p1, p2, p3}, LX/6Dk;->a(Lcom/facebook/payments/checkout/model/CheckoutData;IILandroid/content/Intent;)V

    goto :goto_0

    .line 1701631
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->D:LX/2oj;

    new-instance v1, LX/2qa;

    sget-object v2, LX/04g;->BY_DIALOG:LX/04g;

    invoke-direct {v1, v2}, LX/2qa;-><init>(LX/04g;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 1701632
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->y:LX/6tv;

    invoke-virtual {v0, p1, p2, p3}, LX/6tv;->a(IILandroid/content/Intent;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x636a7f12

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1701633
    const v1, 0x7f030a20

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x4e0d09bf    # 5.9155654E8f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x4dedb8d5    # 4.98539168E8f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1701634
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1701635
    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->g:LX/Ah5;

    invoke-virtual {v1}, LX/Ah5;->e()V

    .line 1701636
    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->e:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 1701637
    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->z:LX/6Du;

    invoke-interface {v1}, LX/6Du;->a()V

    .line 1701638
    const/16 v1, 0x2b

    const v2, -0x569f386c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2e76515b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1701639
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1701640
    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->t:LX/Ah2;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/Ah2;->b(Z)V

    .line 1701641
    const/16 v1, 0x2b

    const v2, 0x4a9d4daf    # 5154519.5f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1701642
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1701643
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1701644
    const-string v1, "recipient_id"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->A:Ljava/lang/String;

    .line 1701645
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1701646
    const-string v1, "video_id"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->B:Ljava/lang/String;

    .line 1701647
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->f:LX/1b4;

    invoke-virtual {v0}, LX/1b4;->o()Z

    move-result v1

    .line 1701648
    iget-object v2, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->g:LX/Ah5;

    new-instance v3, LX/Ah4;

    iget-object v4, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->B:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->A:Ljava/lang/String;

    if-nez v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-direct {v3, v4, v5, v0}, LX/Ah4;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1701649
    iput-object v3, v2, LX/Ah5;->c:LX/Ah4;

    .line 1701650
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->g:LX/Ah5;

    invoke-virtual {v0}, LX/Ah5;->a()V

    .line 1701651
    const v0, 0x7f0d19a9

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->p:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;

    .line 1701652
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->p:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;

    .line 1701653
    iput-object p0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;->g:LX/Agr;

    .line 1701654
    const v0, 0x7f0d19a6

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->o:Landroid/view/View;

    .line 1701655
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    if-eqz v1, :cond_3

    const v0, 0x106000d

    :goto_1
    invoke-static {v3, v0}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    invoke-direct {v2, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 1701656
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v0, v3, :cond_4

    .line 1701657
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->o:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1701658
    :goto_2
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->o:Landroid/view/View;

    new-instance v2, LX/Agi;

    invoke-direct {v2, p0}, LX/Agi;-><init>(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1701659
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->a:LX/Ah3;

    iget-object v2, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->p:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;

    invoke-virtual {v0, v2}, LX/Ah3;->a(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;)LX/Ah2;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->t:LX/Ah2;

    .line 1701660
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->t:LX/Ah2;

    invoke-virtual {v0, v1}, LX/Ah2;->a(Z)V

    .line 1701661
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->t:LX/Ah2;

    .line 1701662
    iput-object p0, v0, LX/Ah2;->c:LX/Ags;

    .line 1701663
    const v0, 0x7f0d19a7

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->q:Landroid/view/View;

    .line 1701664
    if-nez v1, :cond_0

    .line 1701665
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->o:Landroid/view/View;

    const-string v1, "alpha"

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1701666
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 1701667
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->q:Landroid/view/View;

    const v1, 0x7f0d19a8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1701668
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1701669
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f040067

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1701670
    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->q:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1701671
    const v0, 0x7f0d19ab

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarPaymentStatusView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->r:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarPaymentStatusView;

    .line 1701672
    const v0, 0x7f0d19aa

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->s:Landroid/view/View;

    .line 1701673
    invoke-static {p0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->m(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;)V

    .line 1701674
    invoke-static {}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->newBuilder()LX/6sR;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->u:Lcom/facebook/payments/checkout/CheckoutParams;

    .line 1701675
    iput-object v1, v0, LX/6sR;->a:Lcom/facebook/payments/checkout/CheckoutParams;

    .line 1701676
    move-object v0, v0

    .line 1701677
    sget-object v1, LX/6tr;->PREPARE_CHECKOUT:LX/6tr;

    .line 1701678
    iput-object v1, v0, LX/6sR;->p:LX/6tr;

    .line 1701679
    move-object v0, v0

    .line 1701680
    invoke-virtual {v0}, LX/6sR;->z()Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->v:Lcom/facebook/payments/checkout/model/CheckoutData;

    .line 1701681
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->c:LX/6r9;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->v:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v0, v1}, LX/6r9;->b(LX/6qw;)LX/6qd;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->w:LX/6qd;

    .line 1701682
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->w:LX/6qd;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->J:LX/6qh;

    invoke-interface {v0, v1}, LX/6qd;->a(LX/6qh;)V

    .line 1701683
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->w:LX/6qd;

    new-instance v1, LX/Agj;

    invoke-direct {v1, p0}, LX/Agj;-><init>(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;)V

    invoke-interface {v0, v1}, LX/6qd;->a(LX/6qc;)V

    .line 1701684
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->c:LX/6r9;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->v:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v0, v1}, LX/6r9;->d(LX/6qw;)LX/6Dk;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->x:LX/6Dk;

    .line 1701685
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->x:LX/6Dk;

    new-instance v1, LX/Agk;

    invoke-direct {v1, p0}, LX/Agk;-><init>(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;)V

    invoke-interface {v0, v1}, LX/6Dk;->a(LX/6qn;)V

    .line 1701686
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->c:LX/6r9;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->v:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v0, v1}, LX/6r9;->i(LX/6qw;)LX/6tv;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->y:LX/6tv;

    .line 1701687
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->y:LX/6tv;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->J:LX/6qh;

    .line 1701688
    iput-object v1, v0, LX/6tv;->g:LX/6qh;

    .line 1701689
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->y:LX/6tv;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->v:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-virtual {v0, v1}, LX/6tv;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)V

    .line 1701690
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->y:LX/6tv;

    new-instance v1, LX/Agl;

    invoke-direct {v1, p0}, LX/Agl;-><init>(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;)V

    invoke-virtual {v0, v1}, LX/6tv;->a(LX/6qp;)V

    .line 1701691
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->y:LX/6tv;

    invoke-static {p0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->v(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;)LX/6ng;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6tv;->a(LX/6ng;)V

    .line 1701692
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->c:LX/6r9;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->v:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1}, Lcom/facebook/payments/checkout/model/CheckoutData;->b()Lcom/facebook/payments/checkout/CheckoutParams;

    move-result-object v1

    invoke-interface {v1}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v0, v1}, LX/6r9;->g(LX/6qw;)LX/6Du;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->z:LX/6Du;

    .line 1701693
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->z:LX/6Du;

    invoke-interface {v0, p0}, LX/6Du;->a(LX/6Ex;)V

    .line 1701694
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->e:LX/1Ck;

    sget-object v1, LX/Agq;->CHECKOUT_LOADER:LX/Agq;

    invoke-virtual {v0, v1}, LX/1Ck;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1701695
    :goto_3
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->f:LX/1b4;

    invoke-virtual {v0}, LX/1b4;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1701696
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->l:LX/Ah7;

    .line 1701697
    new-instance v1, LX/6Tc;

    invoke-direct {v1}, LX/6Tc;-><init>()V

    move-object v1, v1

    .line 1701698
    const-string v2, "userID"

    iget-object v3, v0, LX/Ah7;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1701699
    iget-object v4, v3, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v3, v4

    .line 1701700
    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1701701
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    .line 1701702
    iget-object v2, v0, LX/Ah7;->b:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    .line 1701703
    new-instance v2, LX/Ah6;

    invoke-direct {v2, v0, p0}, LX/Ah6;-><init>(LX/Ah7;Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;)V

    iget-object v3, v0, LX/Ah7;->c:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1701704
    :cond_1
    return-void

    .line 1701705
    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1701706
    :cond_3
    const v0, 0x7f0a0380

    goto/16 :goto_1

    .line 1701707
    :cond_4
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->o:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    .line 1701708
    :cond_5
    iget-object v0, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->c:LX/6r9;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->v:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v0, v1}, LX/6r9;->a(LX/6qw;)LX/6qa;

    move-result-object v0

    .line 1701709
    new-instance v1, LX/Agm;

    invoke-direct {v1, p0}, LX/Agm;-><init>(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;)V

    invoke-interface {v0, v1}, LX/6qa;->a(LX/6qb;)V

    .line 1701710
    iget-object v1, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->v:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, v1}, LX/6qa;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1701711
    new-instance v1, LX/Agn;

    invoke-direct {v1, p0}, LX/Agn;-><init>(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;)V

    iget-object v2, p0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->b:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1701712
    sget-object v1, LX/Agq;->CHECKOUT_LOADER:LX/Agq;

    invoke-static {p0, v1, v0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->a$redex0(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;LX/Agq;Lcom/google/common/util/concurrent/ListenableFuture;)V

    goto :goto_3

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method
