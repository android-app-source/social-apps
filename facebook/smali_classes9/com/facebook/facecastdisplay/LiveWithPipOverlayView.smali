.class public Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xc
.end annotation


# instance fields
.field public a:Lcom/facebook/user/model/User;
    .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUser;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:Lcom/facebook/fbui/glyph/GlyphButton;

.field private final d:Lcom/facebook/fbui/glyph/GlyphButton;

.field public final e:Ljava/lang/Runnable;

.field public f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1693041
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1693042
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1693053
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1693054
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1693055
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1693056
    const-class v0, Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;

    invoke-static {v0, p0}, Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1693057
    const v0, 0x7f030a34

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1693058
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;->a()V

    .line 1693059
    const v0, 0x7f0d19cf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;->c:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 1693060
    const v0, 0x7f0d19d0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;->d:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 1693061
    const v0, 0x7f0d19d1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 1693062
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;->a:Lcom/facebook/user/model/User;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1693063
    new-instance v0, Lcom/facebook/facecastdisplay/LiveWithPipOverlayView$1;

    invoke-direct {v0, p0}, Lcom/facebook/facecastdisplay/LiveWithPipOverlayView$1;-><init>(Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;)V

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;->e:Ljava/lang/Runnable;

    .line 1693064
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;->setAlpha(F)V

    .line 1693065
    new-instance v0, LX/Acg;

    invoke-direct {v0, p0}, LX/Acg;-><init>(Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1693066
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1693046
    new-instance v0, LX/Ach;

    invoke-direct {v0, p0}, LX/Ach;-><init>(Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;)V

    .line 1693047
    new-instance v1, Landroid/graphics/drawable/PaintDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/PaintDrawable;-><init>()V

    .line 1693048
    new-instance v2, Landroid/graphics/drawable/shapes/RectShape;

    invoke-direct {v2}, Landroid/graphics/drawable/shapes/RectShape;-><init>()V

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/PaintDrawable;->setShape(Landroid/graphics/drawable/shapes/Shape;)V

    .line 1693049
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/PaintDrawable;->setShaderFactory(Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;)V

    .line 1693050
    invoke-virtual {p0, v1}, Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1693051
    return-void
.end method

.method private static a(Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;Lcom/facebook/user/model/User;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 1693052
    iput-object p1, p0, Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;->a:Lcom/facebook/user/model/User;

    iput-object p2, p0, Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;->b:Landroid/os/Handler;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;

    invoke-static {v1}, LX/41r;->b(LX/0QB;)Lcom/facebook/user/model/User;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-static {v1}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v1

    check-cast v1, Landroid/os/Handler;

    invoke-static {p0, v0, v1}, Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;->a(Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;Lcom/facebook/user/model/User;Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1693043
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;->c:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1693044
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;->d:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, p2}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1693045
    return-void
.end method
