.class public Lcom/facebook/facecastdisplay/quietmode/QuietModeView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:LX/27l;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1700382
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/quietmode/QuietModeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1700383
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1700384
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/quietmode/QuietModeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1700385
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1700386
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1700387
    const-class v0, Lcom/facebook/facecastdisplay/quietmode/QuietModeView;

    invoke-static {v0, p0}, Lcom/facebook/facecastdisplay/quietmode/QuietModeView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1700388
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/facecastdisplay/quietmode/QuietModeView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/facecastdisplay/quietmode/QuietModeView;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v0

    check-cast v0, LX/0kL;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/quietmode/QuietModeView;->a:LX/0kL;

    return-void
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 1700389
    iget-object v0, p0, Lcom/facebook/facecastdisplay/quietmode/QuietModeView;->b:LX/27l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecastdisplay/quietmode/QuietModeView;->b:LX/27l;

    invoke-virtual {v0}, LX/27l;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public setIsQuietModeSelected(Z)V
    .locals 3

    .prologue
    .line 1700390
    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/facebook/facecastdisplay/quietmode/QuietModeView;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1700391
    iget-object v0, p0, Lcom/facebook/facecastdisplay/quietmode/QuietModeView;->a:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f080c0d

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/quietmode/QuietModeView;->b:LX/27l;

    .line 1700392
    :cond_0
    return-void
.end method
