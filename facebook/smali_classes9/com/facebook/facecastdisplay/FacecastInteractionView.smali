.class public Lcom/facebook/facecastdisplay/FacecastInteractionView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/Ac2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final b:Lcom/facebook/widget/CustomViewPager;

.field public final c:Landroid/view/View;

.field public final d:Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;

.field public final e:Lcom/facebook/facecastdisplay/LiveEventsTickerView;

.field public final f:Landroid/graphics/drawable/Drawable;

.field public g:Lcom/facebook/facecastdisplay/quietmode/QuietModeView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final j:Landroid/view/LayoutInflater;

.field private final k:F

.field public l:LX/Abx;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1692017
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/FacecastInteractionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1692018
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1692038
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/FacecastInteractionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1692039
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1692019
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1692020
    const-class v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    invoke-static {v0, p0}, Lcom/facebook/facecastdisplay/FacecastInteractionView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1692021
    const v0, 0x7f0305e1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1692022
    const v0, 0x7f0d1033

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomViewPager;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->b:Lcom/facebook/widget/CustomViewPager;

    .line 1692023
    iget-object v0, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->b:Lcom/facebook/widget/CustomViewPager;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->a:LX/Ac2;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 1692024
    iget-object v0, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->b:Lcom/facebook/widget/CustomViewPager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 1692025
    const v0, 0x7f0d1032

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->c:Landroid/view/View;

    .line 1692026
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/FacecastInteractionView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->j:Landroid/view/LayoutInflater;

    .line 1692027
    iget-object v0, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->j:Landroid/view/LayoutInflater;

    const v1, 0x7f030a10

    iget-object v2, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->b:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->d:Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;

    .line 1692028
    iget-object v0, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->d:Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;

    const v1, 0x7f0d1972

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->e:Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    .line 1692029
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, LX/03r;->FacecastInteraction:[I

    invoke-virtual {v0, p2, v1, v3, p3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1692030
    const/16 v1, 0x0

    const/high16 v2, -0x40800000    # -1.0f

    invoke-virtual {v0, v1, v4, v4, v2}, Landroid/content/res/TypedArray;->getFraction(IIIF)F

    move-result v1

    iput v1, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->k:F

    .line 1692031
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1692032
    iget v0, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->k:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 1692033
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/FacecastInteractionView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0206c9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->f:Landroid/graphics/drawable/Drawable;

    .line 1692034
    iget-object v0, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1692035
    :goto_0
    invoke-virtual {p0, v3}, Lcom/facebook/facecastdisplay/FacecastInteractionView;->setClipChildren(Z)V

    .line 1692036
    return-void

    .line 1692037
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->f:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/facecastdisplay/FacecastInteractionView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    new-instance p1, LX/Ac2;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-direct {p1, v1}, LX/Ac2;-><init>(LX/0Sh;)V

    move-object v0, p1

    check-cast v0, LX/Ac2;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->a:LX/Ac2;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1692013
    iget-object v0, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->g:Lcom/facebook/facecastdisplay/quietmode/QuietModeView;

    if-nez v0, :cond_0

    .line 1692014
    iget-object v0, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->j:Landroid/view/LayoutInflater;

    const v1, 0x7f0310e4

    iget-object v2, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->b:Lcom/facebook/widget/CustomViewPager;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/quietmode/QuietModeView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->g:Lcom/facebook/facecastdisplay/quietmode/QuietModeView;

    .line 1692015
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->a:LX/Ac2;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->g:Lcom/facebook/facecastdisplay/quietmode/QuietModeView;

    invoke-virtual {v0, v1}, LX/Ac2;->a(Landroid/view/View;)V

    .line 1692016
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1692040
    iget-object v0, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->a:LX/Ac2;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->d:Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;

    invoke-virtual {v0, v1}, LX/Ac2;->a(Landroid/view/View;)V

    .line 1692041
    return-void
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1692009
    iget-object v0, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->f:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1692010
    iget-object v0, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1692011
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1692012
    return-void
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 1692004
    iget-object v0, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->h:Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;

    if-nez v0, :cond_0

    .line 1692005
    iget-object v0, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->j:Landroid/view/LayoutInflater;

    const v1, 0x7f031620

    iget-object v2, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->b:Lcom/facebook/widget/CustomViewPager;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->h:Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;

    .line 1692006
    iget-object v0, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->h:Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;

    const v1, 0x7f0d31bf

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->i:Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;

    .line 1692007
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->a:LX/Ac2;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->h:Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;

    invoke-virtual {v0, v1}, LX/Ac2;->a(Landroid/view/View;)V

    .line 1692008
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1691969
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomFrameLayout;->onLayout(ZIIII)V

    .line 1691970
    iget-object v0, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 1691971
    iget-object v0, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->b:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v0}, Lcom/facebook/widget/CustomViewPager;->getTop()I

    move-result v0

    .line 1691972
    iget-object v1, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->c:Landroid/view/View;

    iget-object v2, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    sub-int v2, v0, v2

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/FacecastInteractionView;->getWidth()I

    move-result v3

    invoke-virtual {v1, v5, v2, v3, v0}, Landroid/view/View;->layout(IIII)V

    .line 1691973
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->f:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 1691974
    sub-int v0, p4, p2

    .line 1691975
    sub-int v1, p5, p3

    .line 1691976
    iget-object v2, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->f:Landroid/graphics/drawable/Drawable;

    int-to-float v3, v1

    iget v4, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->k:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {v2, v5, v3, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1691977
    :cond_1
    return-void
.end method

.method public final onMeasure(II)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 1691983
    const-string v1, "FacecastInteractionView.onMeasure"

    const v2, -0x555adce2

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1691984
    :try_start_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 1691985
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 1691986
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->l:LX/Abx;

    if-nez v3, :cond_1

    .line 1691987
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1691988
    const v0, 0x327f35b3

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1691989
    :goto_0
    return-void

    .line 1691990
    :cond_1
    :try_start_1
    invoke-virtual {p0, v1, v2}, Lcom/facebook/facecastdisplay/FacecastInteractionView;->setMeasuredDimension(II)V

    .line 1691991
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/FacecastInteractionView;->getChildCount()I

    move-result v7

    move v6, v0

    .line 1691992
    :goto_1
    if-ge v6, v7, :cond_3

    .line 1691993
    invoke-virtual {p0, v6}, Lcom/facebook/facecastdisplay/FacecastInteractionView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1691994
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1691995
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_2

    iget v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-eqz v2, :cond_2

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-eqz v0, :cond_2

    .line 1691996
    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move v2, p1

    move v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/facecastdisplay/FacecastInteractionView;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 1691997
    :cond_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    .line 1691998
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/FacecastInteractionView;->getMeasuredWidth()I

    move-result v0

    .line 1691999
    iget-object v1, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->l:LX/Abx;

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/FacecastInteractionView;->getMeasuredHeight()I

    move-result v2

    invoke-interface {v1, v0, v2}, LX/Abx;->a(II)I

    move-result v1

    .line 1692000
    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1692001
    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 1692002
    iget-object v2, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->b:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v2, v0, v1}, Lcom/facebook/widget/CustomViewPager;->measure(II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1692003
    const v0, 0xda26a2e    # 1.0009583E-30f

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, -0x75e7540

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public setListener(LX/Abx;)V
    .locals 0

    .prologue
    .line 1691981
    iput-object p1, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->l:LX/Abx;

    .line 1691982
    return-void
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 1691978
    iget-object v0, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->f:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->f:Landroid/graphics/drawable/Drawable;

    if-ne v0, p1, :cond_0

    .line 1691979
    const/4 v0, 0x1

    .line 1691980
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    goto :goto_0
.end method
