.class public Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:LX/8nK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/Ac6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:Landroid/widget/EditText;

.field private p:Lcom/facebook/fbui/glyph/GlyphView;

.field private q:Landroid/view/View;

.field private r:Landroid/view/View;

.field public s:Landroid/text/TextWatcher;

.field public t:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public u:LX/AYb;

.field public v:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1692283
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 1692284
    return-void
.end method

.method public static k(Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;)V
    .locals 2

    .prologue
    .line 1692329
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->o:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 1692330
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 1692331
    :goto_0
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->p:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setEnabled(Z)V

    .line 1692332
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->q:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 1692333
    return-void

    .line 1692334
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x542940ea

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1692325
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1692326
    const v1, 0x7f0e0390

    invoke-virtual {p0, v3, v1}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 1692327
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;

    invoke-static {v1}, LX/8nK;->b(LX/0QB;)LX/8nK;

    move-result-object p1

    check-cast p1, LX/8nK;

    invoke-static {v1}, LX/Ac6;->b(LX/0QB;)LX/Ac6;

    move-result-object v1

    check-cast v1, LX/Ac6;

    iput-object p1, p0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->m:LX/8nK;

    iput-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->n:LX/Ac6;

    .line 1692328
    const/16 v1, 0x2b

    const v2, -0x42eb1c0b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x572bd040

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1692324
    const v1, 0x7f030a0a

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x6c5f63a5

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1ac89a65

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1692319
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDestroy()V

    .line 1692320
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->p:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v1, v4}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1692321
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->o:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->s:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1692322
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->r:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1692323
    const/16 v1, 0x2b

    const v2, -0x6ac37572

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 1692315
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 1692316
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->u:LX/AYb;

    if-eqz v0, :cond_0

    .line 1692317
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->u:LX/AYb;

    invoke-interface {v0}, LX/AYb;->a()V

    .line 1692318
    :cond_0
    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x39c97254

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1692310
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onResume()V

    .line 1692311
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->o:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1692312
    iget-object v1, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v1, v1

    .line 1692313
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x15

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 1692314
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x75760dc6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1692306
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1692307
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->o:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1692308
    const-string v1, "facecast_comment_draft_saved_tag"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1692309
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1692290
    invoke-super {p0, p1, p2}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    move-object v0, p1

    .line 1692291
    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {v0}, LX/AVI;->a(Landroid/view/ViewGroup;)LX/AVI;

    .line 1692292
    const v0, 0x7f0d1941

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->o:Landroid/widget/EditText;

    .line 1692293
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->n:LX/Ac6;

    .line 1692294
    iget-object v1, v0, LX/Ac6;->a:LX/0Uh;

    const/16 v2, 0x355

    const/4 p2, 0x0

    invoke-virtual {v1, v2, p2}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 1692295
    if-eqz v0, :cond_0

    .line 1692296
    new-instance v0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment$1;

    invoke-direct {v0, p0}, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment$1;-><init>(Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 1692297
    :cond_0
    const v0, 0x7f0d1963

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->p:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1692298
    const v0, 0x7f0d1964

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->q:Landroid/view/View;

    .line 1692299
    const v0, 0x7f0d1961

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->r:Landroid/view/View;

    .line 1692300
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->p:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/AcD;

    invoke-direct {v1, p0}, LX/AcD;-><init>(Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1692301
    new-instance v0, LX/AcE;

    invoke-direct {v0, p0}, LX/AcE;-><init>(Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;)V

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->s:Landroid/text/TextWatcher;

    .line 1692302
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->o:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->s:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1692303
    invoke-static {p0}, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->k(Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;)V

    .line 1692304
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->r:Landroid/view/View;

    new-instance v1, LX/AcF;

    invoke-direct {v1, p0}, LX/AcF;-><init>(Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1692305
    return-void
.end method

.method public final onViewStateRestored(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4a59db71

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1692285
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onViewStateRestored(Landroid/os/Bundle;)V

    .line 1692286
    if-eqz p1, :cond_0

    .line 1692287
    iget-object v1, p0, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->o:Landroid/widget/EditText;

    const-string v2, "facecast_comment_draft_saved_tag"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1692288
    invoke-static {p0}, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->k(Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;)V

    .line 1692289
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x3d4e9dc

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
