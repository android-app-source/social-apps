.class public Lcom/facebook/facecastdisplay/LiveEventsTickerView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field public final a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

.field public final b:Landroid/view/View;

.field public final c:Landroid/view/View;

.field public final d:Landroid/view/ViewStub;

.field public final e:Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;

.field public final f:Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;

.field public final g:LX/AcH;

.field public final h:Landroid/view/View;

.field public i:LX/AcI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/AcW;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1692863
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/LiveEventsTickerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1692864
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1692861
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/LiveEventsTickerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1692862
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1692846
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1692847
    const-class v0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    invoke-static {v0, p0}, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1692848
    const v0, 0x7f030a0f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1692849
    const v0, 0x7f0d1969

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    .line 1692850
    const v0, 0x7f0d1970

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->b:Landroid/view/View;

    .line 1692851
    const v0, 0x7f0d196e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->c:Landroid/view/View;

    .line 1692852
    const v0, 0x7f0d1966

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->d:Landroid/view/ViewStub;

    .line 1692853
    const v0, 0x7f0d196c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->e:Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;

    .line 1692854
    const v0, 0x7f0d196b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->f:Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;

    .line 1692855
    const v0, 0x7f0d196d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->h:Landroid/view/View;

    .line 1692856
    new-instance v1, LX/0zw;

    const v0, 0x7f0d1971

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    new-instance v2, LX/AcZ;

    invoke-direct {v2, p0}, LX/AcZ;-><init>(Lcom/facebook/facecastdisplay/LiveEventsTickerView;)V

    invoke-direct {v1, v0, v2}, LX/0zw;-><init>(Landroid/view/ViewStub;LX/0zy;)V

    .line 1692857
    iget-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->i:LX/AcI;

    invoke-virtual {v0, v1}, LX/AcI;->a(LX/0zw;)LX/AcH;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->g:LX/AcH;

    .line 1692858
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    const-class v1, LX/AcI;

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/AcI;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->i:LX/AcI;

    return-void
.end method


# virtual methods
.method public setListener(LX/AcW;)V
    .locals 0

    .prologue
    .line 1692859
    iput-object p1, p0, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->j:LX/AcW;

    .line 1692860
    return-void
.end method
