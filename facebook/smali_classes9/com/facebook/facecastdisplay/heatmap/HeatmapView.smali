.class public Lcom/facebook/facecastdisplay/heatmap/HeatmapView;
.super LX/AeE;
.source ""

# interfaces
.implements LX/Acj;


# instance fields
.field public a:LX/Ack;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1697292
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/heatmap/HeatmapView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1697293
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1697290
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/heatmap/HeatmapView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1697291
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1697287
    invoke-direct {p0, p1, p2, p3}, LX/AeE;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1697288
    const-class v0, Lcom/facebook/facecastdisplay/heatmap/HeatmapView;

    invoke-static {v0, p0}, Lcom/facebook/facecastdisplay/heatmap/HeatmapView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1697289
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/facecastdisplay/heatmap/HeatmapView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/facecastdisplay/heatmap/HeatmapView;

    new-instance p1, LX/Ack;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-direct {p1, v1, v2, v3}, LX/Ack;-><init>(Ljava/util/concurrent/ExecutorService;LX/0tX;LX/03V;)V

    move-object v0, p1

    check-cast v0, LX/Ack;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/heatmap/HeatmapView;->a:LX/Ack;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1697284
    iget-object v0, p0, Lcom/facebook/facecastdisplay/heatmap/HeatmapView;->a:LX/Ack;

    invoke-virtual {v0}, LX/Ack;->b()V

    .line 1697285
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/AeE;->a(Z)V

    .line 1697286
    return-void
.end method

.method public final a(LX/0Px;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1697261
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1697262
    :goto_0
    return-void

    .line 1697263
    :cond_0
    new-instance v0, LX/AeG;

    invoke-direct {v0}, LX/AeG;-><init>()V

    .line 1697264
    iget-object v1, v0, LX/AeG;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1697265
    const/4 v1, 0x0

    iput v1, v0, LX/AeG;->b:F

    .line 1697266
    const/4 v2, 0x0

    .line 1697267
    invoke-virtual {v0}, LX/AeG;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v2

    .line 1697268
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v3, v1

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    .line 1697269
    invoke-virtual {v1}, Ljava/lang/Number;->floatValue()F

    move-result v4

    .line 1697270
    invoke-virtual {v1}, Ljava/lang/Number;->floatValue()F

    move-result v1

    cmpg-float v1, v1, v2

    if-gez v1, :cond_4

    .line 1697271
    :goto_3
    new-instance v4, LX/AeF;

    invoke-direct {v4, v3}, LX/AeF;-><init>(F)V

    .line 1697272
    iget v1, v4, LX/AeF;->a:F

    move v1, v1

    .line 1697273
    iget v3, v0, LX/AeG;->b:F

    cmpl-float v1, v1, v3

    if-lez v1, :cond_1

    .line 1697274
    iget v1, v4, LX/AeF;->a:F

    move v1, v1

    .line 1697275
    iput v1, v0, LX/AeG;->b:F

    .line 1697276
    :cond_1
    iget v1, v4, LX/AeF;->a:F

    move v1, v1

    .line 1697277
    iget-object v3, v0, LX/AeG;->a:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v3, v1

    .line 1697278
    goto :goto_2

    .line 1697279
    :cond_2
    iget-object v1, v0, LX/AeG;->a:Ljava/util/ArrayList;

    iget-object v3, v0, LX/AeG;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/AeF;

    .line 1697280
    iget v3, v1, LX/AeF;->a:F

    move v1, v3

    .line 1697281
    goto :goto_1

    .line 1697282
    :cond_3
    iput-object v0, p0, LX/AeE;->a:LX/AeG;

    .line 1697283
    goto :goto_0

    :cond_4
    move v3, v4

    goto :goto_3
.end method

.method public setVideoId(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1697247
    iget-object v0, p0, Lcom/facebook/facecastdisplay/heatmap/HeatmapView;->a:LX/Ack;

    .line 1697248
    iput-object p1, v0, LX/Ack;->f:Ljava/lang/String;

    .line 1697249
    iget-object v0, p0, Lcom/facebook/facecastdisplay/heatmap/HeatmapView;->a:LX/Ack;

    .line 1697250
    iput-object p0, v0, LX/Ack;->e:LX/Acj;

    .line 1697251
    iget-object v0, p0, Lcom/facebook/facecastdisplay/heatmap/HeatmapView;->a:LX/Ack;

    .line 1697252
    invoke-virtual {v0}, LX/Ack;->b()V

    .line 1697253
    iget-object v1, v0, LX/Ack;->f:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1697254
    iget-object v1, v0, LX/Ack;->d:LX/03V;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/Ack;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_startFetching"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Tried to fetch without a story id."

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1697255
    :goto_0
    return-void

    .line 1697256
    :cond_0
    new-instance v1, LX/6TN;

    invoke-direct {v1}, LX/6TN;-><init>()V

    move-object v1, v1

    .line 1697257
    const-string v2, "targetID"

    iget-object v3, v0, LX/Ack;->f:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1697258
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    sget-object v2, LX/0zS;->a:LX/0zS;

    invoke-virtual {v1, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    const-wide/16 v3, 0x258

    invoke-virtual {v1, v3, v4}, LX/0zO;->a(J)LX/0zO;

    move-result-object v1

    .line 1697259
    iget-object v2, v0, LX/Ack;->b:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    iput-object v1, v0, LX/Ack;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1697260
    iget-object v1, v0, LX/Ack;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, LX/Aci;

    invoke-direct {v2, v0}, LX/Aci;-><init>(LX/Ack;)V

    iget-object v3, v0, LX/Ack;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
