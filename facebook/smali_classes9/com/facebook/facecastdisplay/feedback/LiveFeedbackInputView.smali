.class public Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;
.super Landroid/widget/HorizontalScrollView;
.source ""


# instance fields
.field public final a:Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;

.field private final b:Landroid/view/VelocityTracker;

.field public final c:LX/Ac3;

.field public final d:I

.field public e:Z

.field public f:LX/Ae4;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1696166
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1696167
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1696164
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1696165
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1696117
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1696118
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1696119
    const v1, 0x7f030a12

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1696120
    const v0, 0x7f0d1977

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->a:Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;

    .line 1696121
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->b:Landroid/view/VelocityTracker;

    .line 1696122
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, LX/03r;->FacecastBottomBarToolbar:[I

    const v2, 0x7f01041f

    invoke-virtual {v0, p2, v1, v2, p3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1696123
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 1696124
    const/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    .line 1696125
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1696126
    new-instance v0, LX/Ac3;

    invoke-direct {v0, v1, v2}, LX/Ac3;-><init>(II)V

    iput-object v0, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->c:LX/Ac3;

    .line 1696127
    iget-object v0, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->c:LX/Ac3;

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1696128
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, LX/03r;->LiveEventsTickerView:[I

    const v2, 0x7f01041f

    invoke-virtual {v0, p2, v1, v2, p3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1696129
    const/16 v1, 0x6

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->d:I

    .line 1696130
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1696131
    return-void
.end method


# virtual methods
.method public getFeedbackInputViewContainer()Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;
    .locals 1

    .prologue
    .line 1696163
    iget-object v0, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->a:Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;

    return-object v0
.end method

.method public getToolbarBackgroundColorContextMode()I
    .locals 1

    .prologue
    .line 1696162
    iget v0, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->d:I

    return v0
.end method

.method public getToolbarDrawableBackground()LX/Ac3;
    .locals 1

    .prologue
    .line 1696161
    iget-object v0, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->c:LX/Ac3;

    return-object v0
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 1696154
    const-string v0, "LiveFeedbackInputView.onMeasure"

    const v1, 0x35e916fb

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1696155
    :try_start_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->a:Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 1696156
    iput v1, v0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->h:I

    .line 1696157
    invoke-super {p0, p1, p2}, Landroid/widget/HorizontalScrollView;->onMeasure(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1696158
    const v0, 0x65f3cfd4

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1696159
    return-void

    .line 1696160
    :catchall_0
    move-exception v0

    const v1, 0x41f0bad8

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x2

    const v1, 0x67cd9d8

    invoke-static {v0, v4, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1696136
    invoke-super {p0, p1}, Landroid/widget/HorizontalScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1696137
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    .line 1696138
    packed-switch v1, :pswitch_data_0

    .line 1696139
    :cond_0
    :goto_0
    const v1, 0x4f5bc4d8

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return v4

    .line 1696140
    :pswitch_0
    iget-object v1, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->b:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->clear()V

    .line 1696141
    iget-object v1, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->b:Landroid/view/VelocityTracker;

    invoke-virtual {v1, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 1696142
    :pswitch_1
    iget-object v1, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->b:Landroid/view/VelocityTracker;

    invoke-virtual {v1, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 1696143
    iget-object v1, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->b:Landroid/view/VelocityTracker;

    const/16 v2, 0x3e8

    invoke-virtual {v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    goto :goto_0

    .line 1696144
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v1

    .line 1696145
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    .line 1696146
    const/high16 v2, -0x40800000    # -1.0f

    iget-object v3, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->b:Landroid/view/VelocityTracker;

    invoke-static {v3, v1}, LX/2uG;->a(Landroid/view/VelocityTracker;I)F

    move-result v1

    mul-float/2addr v1, v2

    .line 1696147
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->getScrollX()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    .line 1696148
    const/16 v1, 0x42

    invoke-virtual {p0, v1}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->fullScroll(I)Z

    .line 1696149
    iput-boolean v4, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->e:Z

    .line 1696150
    iget-object v1, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->f:LX/Ae4;

    if-eqz v1, :cond_0

    .line 1696151
    iget-object v1, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->f:LX/Ae4;

    invoke-interface {v1}, LX/Ae4;->d()V

    goto :goto_0

    .line 1696152
    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->e:Z

    .line 1696153
    const/16 v1, 0x11

    invoke-virtual {p0, v1}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->fullScroll(I)Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public setListener(LX/Ae4;)V
    .locals 0

    .prologue
    .line 1696134
    iput-object p1, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->f:LX/Ae4;

    .line 1696135
    return-void
.end method

.method public setSwipeToReactions(Z)V
    .locals 1

    .prologue
    .line 1696132
    new-instance v0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView$1;

    invoke-direct {v0, p0, p1}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView$1;-><init>(Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;Z)V

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;->post(Ljava/lang/Runnable;)Z

    .line 1696133
    return-void
.end method
