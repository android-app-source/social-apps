.class public Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public final a:Lcom/facebook/fbui/glyph/GlyphView;

.field public final b:Lcom/facebook/resources/ui/FbTextView;

.field public final c:Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;

.field public final d:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;

.field public final e:I

.field public final f:I

.field public g:LX/Ae1;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1696172
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1696173
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1696226
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1696227
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 1696174
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1696175
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, LX/03r;->FacecastBottomBarToolbar:[I

    const v2, 0x7f01041f

    invoke-virtual {v0, p2, v1, v2, p3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1696176
    const/16 v1, 0x3

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->e:I

    .line 1696177
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1696178
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const v1, 0x7f0e037c

    sget-object v2, LX/03r;->FacecastBottomBarToolbar:[I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1696179
    const/16 v1, 0x3

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->f:I

    .line 1696180
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1696181
    const v0, 0x7f030a13

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1696182
    invoke-virtual {p0, v3}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->setOrientation(I)V

    .line 1696183
    const v0, 0x7f0d1979

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->a:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1696184
    const v0, 0x7f0d1978

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1696185
    const v0, 0x7f0d197a

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->c:Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;

    .line 1696186
    const v0, 0x7f0d197b

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->d:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;

    .line 1696187
    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 1696188
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;->onMeasure(II)V

    .line 1696189
    iget-object v0, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->g:LX/Ae1;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->g:LX/Ae1;

    invoke-interface {v0}, LX/Ae1;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1696190
    :cond_0
    :goto_0
    return-void

    .line 1696191
    :cond_1
    iget-object v6, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1696192
    if-eqz v6, :cond_8

    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object v3, v0

    .line 1696193
    :goto_1
    iget-object v0, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0}, Lcom/facebook/fbui/glyph/GlyphView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1696194
    iget-object v1, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->c:Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1696195
    iget-object v2, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->d:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;

    invoke-virtual {v2}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1696196
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->getPaddingLeft()I

    move-result v4

    iget v5, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v4, v5

    .line 1696197
    iget v5, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->h:I

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->getPaddingRight()I

    move-result v7

    sub-int/2addr v5, v7

    iget v7, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int/2addr v5, v7

    .line 1696198
    if-eqz v6, :cond_2

    .line 1696199
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    iget v8, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v7, v8

    iget v8, v3, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v7, v8

    add-int/2addr v4, v7

    .line 1696200
    :cond_2
    iget-object v7, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v7}, Lcom/facebook/fbui/glyph/GlyphView;->getVisibility()I

    move-result v7

    if-nez v7, :cond_3

    .line 1696201
    iget-object v7, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v7}, Lcom/facebook/fbui/glyph/GlyphView;->getMeasuredWidth()I

    move-result v7

    iget v8, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v7, v8

    iget v8, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v7, v8

    add-int/2addr v4, v7

    .line 1696202
    :cond_3
    iget-object v7, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->g:LX/Ae1;

    invoke-interface {v7}, LX/Ae1;->f()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1696203
    iget-object v7, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->d:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;

    invoke-virtual {v7}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->getVisibleWidth()I

    move-result v7

    iget v8, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v7, v8

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->getPaddingRight()I

    move-result v8

    sub-int/2addr v7, v8

    sub-int/2addr v5, v7

    .line 1696204
    :cond_4
    sub-int v4, v5, v4

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 1696205
    invoke-static {v9, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 1696206
    iget-object v7, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->c:Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;

    invoke-virtual {v7, v4, v5}, Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;->measure(II)V

    .line 1696207
    iget-object v4, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->c:Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;

    invoke-virtual {v4}, Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;->getMeasuredWidth()I

    move-result v4

    iget v5, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v4, v5

    iget v1, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v1, v4

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->getPaddingLeft()I

    move-result v4

    add-int/2addr v1, v4

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->getPaddingRight()I

    move-result v4

    add-int/2addr v1, v4

    .line 1696208
    if-eqz v6, :cond_5

    .line 1696209
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    iget v5, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v4, v5

    iget v3, v3, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v3, v4

    add-int/2addr v1, v3

    .line 1696210
    :cond_5
    iget-object v3, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v3}, Lcom/facebook/fbui/glyph/GlyphView;->getVisibility()I

    move-result v3

    if-nez v3, :cond_6

    .line 1696211
    iget-object v3, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v3}, Lcom/facebook/fbui/glyph/GlyphView;->getMeasuredWidth()I

    move-result v3

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v3, v4

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 1696212
    :cond_6
    iget-object v0, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->g:LX/Ae1;

    invoke-interface {v0}, LX/Ae1;->f()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1696213
    iget-object v0, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->d:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->getMeasuredWidth()I

    move-result v0

    iget v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v0, v3

    iget v2, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 1696214
    :cond_7
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 1696215
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 1696216
    sparse-switch v2, :sswitch_data_0

    .line 1696217
    :goto_2
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 1696218
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 1696219
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->getMeasuredHeight()I

    move-result v2

    iget-object v4, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->c:Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;

    invoke-virtual {v4}, Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;->getMeasuredHeight()I

    move-result v4

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 1696220
    sparse-switch v3, :sswitch_data_1

    move v0, v2

    .line 1696221
    :goto_3
    :sswitch_0
    invoke-virtual {p0, v1, v0}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->setMeasuredDimension(II)V

    goto/16 :goto_0

    .line 1696222
    :cond_8
    const/4 v0, 0x0

    move-object v3, v0

    goto/16 :goto_1

    :sswitch_1
    move v1, v0

    .line 1696223
    goto :goto_2

    .line 1696224
    :sswitch_2
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    goto :goto_2

    .line 1696225
    :sswitch_3
    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_3

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_2
        0x40000000 -> :sswitch_1
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        -0x80000000 -> :sswitch_3
        0x40000000 -> :sswitch_0
    .end sparse-switch
.end method

.method public setListener(LX/Ae1;)V
    .locals 0

    .prologue
    .line 1696168
    iput-object p1, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->g:LX/Ae1;

    .line 1696169
    return-void
.end method

.method public setParentViewWidthSize(I)V
    .locals 0

    .prologue
    .line 1696170
    iput p1, p0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->h:I

    .line 1696171
    return-void
.end method
