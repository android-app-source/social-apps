.class public Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/widget/text/BetterEditTextView;

.field private final b:Landroid/app/Activity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1695451
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1695452
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1695453
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1695454
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1695455
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1695456
    const v0, 0x7f030a09

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1695457
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;->setOrientation(I)V

    .line 1695458
    const v0, 0x7f0d1941

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;->a:Lcom/facebook/widget/text/BetterEditTextView;

    .line 1695459
    const-class v0, Landroid/app/Activity;

    invoke-static {p1, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;->b:Landroid/app/Activity;

    .line 1695460
    return-void
.end method


# virtual methods
.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x5a1c9c8b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1695461
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onAttachedToWindow()V

    .line 1695462
    iget-object v1, p0, Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;->b:Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 1695463
    iget-object v1, p0, Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;->b:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 1695464
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    iput v2, p0, Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;->c:I

    .line 1695465
    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 1695466
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x3ead8cf

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x6576abfb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1695467
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onDetachedFromWindow()V

    .line 1695468
    iget-object v1, p0, Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;->b:Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 1695469
    iget-object v1, p0, Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;->b:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    iget v2, p0, Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;->c:I

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 1695470
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x6ea88d6a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
