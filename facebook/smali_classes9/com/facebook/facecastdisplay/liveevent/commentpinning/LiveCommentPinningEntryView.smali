.class public Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# static fields
.field private static final e:[I

.field private static final f:[I


# instance fields
.field public a:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/215;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/AaZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1b4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final g:I

.field private final h:I

.field private final i:I

.field private final j:Landroid/text/Spannable;

.field private final k:Landroid/text/Spannable;

.field private final l:Ljava/lang/CharSequence;

.field private m:Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;

.field private n:Lcom/facebook/resources/ui/FbTextView;

.field private o:Lcom/facebook/widget/springbutton/SpringScaleButton;

.field private p:Lcom/facebook/fbui/glyph/GlyphView;

.field private q:Lcom/facebook/user/tiles/UserTileView;

.field public r:LX/AfI;

.field public s:Z

.field public t:LX/Aeu;

.field private u:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1699177
    new-array v0, v3, [I

    const v1, 0x10100a0

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->e:[I

    .line 1699178
    new-array v0, v3, [I

    const v1, 0x101009f

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->f:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1699175
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1699176
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1699087
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1699088
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1699161
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1699162
    const-class v0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;

    invoke-static {v0, p0}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1699163
    invoke-virtual {p0, v2}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->setOrientation(I)V

    .line 1699164
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, LX/03r;->LiveCommentPinningEntryView:[I

    invoke-virtual {v0, p2, v1, p3, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1699165
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->g:I

    .line 1699166
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->h:I

    .line 1699167
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->i:I

    .line 1699168
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1699169
    iput-boolean v2, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->s:Z

    .line 1699170
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->c()Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->j:Landroid/text/Spannable;

    .line 1699171
    invoke-direct {p0}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->d()Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->k:Landroid/text/Spannable;

    .line 1699172
    iget-object v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->d:LX/1b4;

    invoke-virtual {v0}, LX/1b4;->F()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->getStyledTranslatedSuffix()Ljava/lang/CharSequence;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->l:Ljava/lang/CharSequence;

    .line 1699173
    return-void

    .line 1699174
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private static a(Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;LX/0wM;LX/215;LX/AaZ;LX/1b4;)V
    .locals 0

    .prologue
    .line 1699160
    iput-object p1, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->a:LX/0wM;

    iput-object p2, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->b:LX/215;

    iput-object p3, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->c:LX/AaZ;

    iput-object p4, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->d:LX/1b4;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;

    invoke-static {v3}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    invoke-static {v3}, LX/215;->b(LX/0QB;)LX/215;

    move-result-object v1

    check-cast v1, LX/215;

    invoke-static {v3}, LX/AaZ;->b(LX/0QB;)LX/AaZ;

    move-result-object v2

    check-cast v2, LX/AaZ;

    invoke-static {v3}, LX/1b4;->b(LX/0QB;)LX/1b4;

    move-result-object v3

    check-cast v3, LX/1b4;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->a(Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;LX/0wM;LX/215;LX/AaZ;LX/1b4;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1699154
    if-eqz p1, :cond_0

    .line 1699155
    iget-object v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->o:Lcom/facebook/widget/springbutton/SpringScaleButton;

    sget-object v1, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->e:[I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setImageState([IZ)V

    .line 1699156
    iget-object v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->o:Lcom/facebook/widget/springbutton/SpringScaleButton;

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080c2e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1699157
    :goto_0
    return-void

    .line 1699158
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->o:Lcom/facebook/widget/springbutton/SpringScaleButton;

    sget-object v1, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->f:[I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setImageState([IZ)V

    .line 1699159
    iget-object v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->o:Lcom/facebook/widget/springbutton/SpringScaleButton;

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080c2d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private b(LX/Aeu;LX/AeU;)V
    .locals 4

    .prologue
    .line 1699146
    iget-object v0, p1, LX/AeP;->a:LX/AcC;

    iget-object v0, v0, LX/AcC;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v1

    .line 1699147
    sget-object v0, LX/8ue;->NONE:LX/8ue;

    .line 1699148
    iget-object v2, p1, LX/AeP;->a:LX/AcC;

    iget-object v2, v2, LX/AcC;->b:Ljava/lang/String;

    iget-object v3, p2, LX/AeU;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1699149
    sget-object v0, LX/8ue;->BROADCASTER:LX/8ue;

    .line 1699150
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->q:Lcom/facebook/user/tiles/UserTileView;

    invoke-static {v1, v0}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;LX/8ue;)LX/8t9;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 1699151
    return-void

    .line 1699152
    :cond_1
    iget-object v2, p1, LX/AeP;->a:LX/AcC;

    iget-boolean v2, v2, LX/AcC;->d:Z

    if-eqz v2, :cond_0

    .line 1699153
    sget-object v0, LX/8ue;->VERIFIED:LX/8ue;

    goto :goto_0
.end method

.method private c()Landroid/text/SpannableStringBuilder;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 1699132
    new-instance v6, Landroid/text/SpannableStringBuilder;

    invoke-direct {v6}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1699133
    iget-object v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->a:LX/0wM;

    const v1, 0x7f020997

    iget v2, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->h:I

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1699134
    if-nez v1, :cond_0

    move-object v0, v6

    .line 1699135
    :goto_0
    return-object v0

    .line 1699136
    :cond_0
    const-string v0, "\u2060"

    invoke-virtual {v6, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1699137
    invoke-virtual {v6}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    .line 1699138
    const-string v0, "[badge]"

    invoke-virtual {v6, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1699139
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1699140
    const v2, 0x7f0b04bb

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 1699141
    const v3, 0x7f0b04bc

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 1699142
    new-instance v0, Landroid/graphics/drawable/InsetDrawable;

    neg-int v3, v5

    move v4, v2

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    .line 1699143
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    add-int/2addr v3, v2

    add-int/2addr v2, v3

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-virtual {v0, v8, v8, v2, v1}, Landroid/graphics/drawable/InsetDrawable;->setBounds(IIII)V

    .line 1699144
    new-instance v1, Landroid/text/style/ImageSpan;

    const/4 v2, 0x1

    invoke-direct {v1, v0, v2}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    invoke-virtual {v6}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    const/16 v2, 0x21

    invoke-virtual {v6, v1, v7, v0, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    move-object v0, v6

    .line 1699145
    goto :goto_0
.end method

.method private d()Landroid/text/SpannableStringBuilder;
    .locals 5

    .prologue
    .line 1699127
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080c46

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1699128
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1699129
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    iget v2, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->h:I

    invoke-direct {v0, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 1699130
    const/4 v2, 0x0

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    const/16 v4, 0x21

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1699131
    return-object v1
.end method

.method private getStyledTranslatedSuffix()Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 1699125
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080c05

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1699126
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    const-string v3, "\n"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v0, v3}, LX/3Hk;->b(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-static {v1}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/Aeu;LX/AeU;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/16 v6, 0x8

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1699105
    iput-object p1, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->t:LX/Aeu;

    .line 1699106
    invoke-direct {p0, p1, p2}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->b(LX/Aeu;LX/AeU;)V

    .line 1699107
    new-instance v0, Landroid/text/SpannableStringBuilder;

    iget-object v1, p1, LX/AeP;->a:LX/AcC;

    iget-object v1, v1, LX/AcC;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1699108
    new-instance v1, Landroid/text/style/StyleSpan;

    invoke-direct {v1, v5}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    const/16 v3, 0x21

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1699109
    iget-object v1, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->n:Lcom/facebook/resources/ui/FbTextView;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/CharSequence;

    aput-object v0, v2, v4

    iget-object v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->j:Landroid/text/Spannable;

    aput-object v0, v2, v5

    const/4 v0, 0x2

    iget-object v3, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->k:Landroid/text/Spannable;

    aput-object v3, v2, v0

    invoke-static {v2}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1699110
    iget-object v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->c:LX/AaZ;

    invoke-virtual {v0}, LX/AaZ;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, LX/Aeu;->j:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1699111
    iget-object v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->m:Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;

    iget-object v1, p1, LX/Aeu;->j:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->l:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->a(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 1699112
    :goto_0
    invoke-virtual {p2}, LX/AeU;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1699113
    iget-object v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->p:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v6}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1699114
    iget-object v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->p:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v7}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1699115
    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->u:Z

    if-eqz v0, :cond_0

    .line 1699116
    iget-object v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->o:Lcom/facebook/widget/springbutton/SpringScaleButton;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setVisibility(I)V

    .line 1699117
    iget-boolean v0, p1, LX/Aeu;->g:Z

    invoke-static {p0, v0}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->a$redex0(Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;Z)V

    .line 1699118
    iget-object v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->o:Lcom/facebook/widget/springbutton/SpringScaleButton;

    new-instance v1, LX/AfK;

    invoke-direct {v1, p0}, LX/AfK;-><init>(Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1699119
    :cond_0
    :goto_1
    return-void

    .line 1699120
    :cond_1
    iget-object v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->m:Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;

    iget-object v1, p1, LX/Aeu;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->a(Ljava/lang/String;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1699121
    :cond_2
    iget-object v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->o:Lcom/facebook/widget/springbutton/SpringScaleButton;

    invoke-virtual {v0, v6}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setVisibility(I)V

    .line 1699122
    iget-object v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->o:Lcom/facebook/widget/springbutton/SpringScaleButton;

    invoke-virtual {v0, v7}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1699123
    iget-object v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->p:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v4}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1699124
    iget-object v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->p:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/AfL;

    invoke-direct {v1, p0}, LX/AfL;-><init>(Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1699089
    const v0, 0x7f0309fe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1699090
    iput-boolean v1, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->s:Z

    .line 1699091
    const v0, 0x7f0d1942

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->q:Lcom/facebook/user/tiles/UserTileView;

    .line 1699092
    const v0, 0x7f0d1944

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->m:Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;

    .line 1699093
    const v0, 0x7f0d1943

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->n:Lcom/facebook/resources/ui/FbTextView;

    .line 1699094
    const v0, 0x7f0d1946

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->p:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1699095
    const v0, 0x7f0d1945

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/springbutton/SpringScaleButton;

    iput-object v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->o:Lcom/facebook/widget/springbutton/SpringScaleButton;

    .line 1699096
    iget-object v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->o:Lcom/facebook/widget/springbutton/SpringScaleButton;

    iget-object v2, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->b:LX/215;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/springbutton/SpringScaleButton;->a(LX/215;)V

    .line 1699097
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 1699098
    iget-object v2, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->o:Lcom/facebook/widget/springbutton/SpringScaleButton;

    invoke-virtual {v2}, Lcom/facebook/widget/springbutton/SpringScaleButton;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const v3, 0x7f01041c

    invoke-virtual {v2, v3, v0, v1}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 1699099
    iget v0, v0, Landroid/util/TypedValue;->data:I

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->u:Z

    .line 1699100
    iget-object v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->n:Lcom/facebook/resources/ui/FbTextView;

    iget v1, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->g:I

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1699101
    iget-object v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->m:Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;

    iget v1, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->g:I

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->setTextColor(I)V

    .line 1699102
    iget-object v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->p:Lcom/facebook/fbui/glyph/GlyphView;

    iget v1, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->i:I

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1699103
    return-void

    .line 1699104
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getModel()LX/Aeu;
    .locals 1

    .prologue
    .line 1699086
    iget-object v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->t:LX/Aeu;

    return-object v0
.end method

.method public setLiveCommentPinningEntryViewListener(LX/AfI;)V
    .locals 0

    .prologue
    .line 1699084
    iput-object p1, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningEntryView;->r:LX/AfI;

    .line 1699085
    return-void
.end method
