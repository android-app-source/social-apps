.class public Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningScrollView;
.super Lcom/facebook/widget/FbScrollView;
.source ""


# instance fields
.field private final a:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1699179
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1699180
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1699181
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1699182
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1699183
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/FbScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1699184
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, LX/03r;->LiveCommentPinning:[I

    invoke-virtual {v0, p2, v1, v2, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1699185
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningScrollView;->a:I

    .line 1699186
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1699187
    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 1699188
    iget v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningScrollView;->a:I

    const/high16 v1, -0x80000000

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1699189
    invoke-super {p0, p1, v0}, Lcom/facebook/widget/FbScrollView;->onMeasure(II)V

    .line 1699190
    return-void
.end method
