.class public Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""


# instance fields
.field public a:Z

.field private final b:Ljava/lang/CharSequence;

.field private final c:Ljava/lang/CharSequence;

.field private d:Ljava/lang/CharSequence;

.field private e:Ljava/lang/CharSequence;

.field private f:Ljava/lang/CharSequence;

.field private g:Ljava/lang/CharSequence;

.field private h:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1699286
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1699287
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1699284
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1699285
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1699279
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1699280
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080c04

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->a(Ljava/lang/String;)Landroid/text/Spannable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->c:Ljava/lang/CharSequence;

    .line 1699281
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080c03

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->a(Ljava/lang/String;)Landroid/text/Spannable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->b:Ljava/lang/CharSequence;

    .line 1699282
    new-instance v0, LX/AfP;

    invoke-direct {v0, p0}, LX/AfP;-><init>(Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1699283
    return-void
.end method

.method private static a(Ljava/lang/String;)Landroid/text/Spannable;
    .locals 5

    .prologue
    .line 1699275
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, p0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1699276
    new-instance v1, Landroid/text/style/StyleSpan;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 1699277
    const/4 v2, 0x0

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    const/16 v4, 0x21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1699278
    return-object v0
.end method

.method private a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 1699274
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/CharSequence;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    const-string v2, "  "

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->c:Ljava/lang/CharSequence;

    aput-object v2, v0, v1

    invoke-static {v0}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;)V
    .locals 1

    .prologue
    .line 1699270
    iget-boolean v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->a:Z

    if-eqz v0, :cond_0

    .line 1699271
    iget-object v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->f:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1699272
    :goto_0
    return-void

    .line 1699273
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->e:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 1699236
    invoke-direct {p0, p1}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->c(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1699237
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const/4 v0, 0x1

    const-string v2, "\u2026 "

    aput-object v2, v1, v0

    const/4 v0, 0x2

    iget-object v2, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->b:Ljava/lang/CharSequence;

    aput-object v2, v1, v0

    invoke-static {v1}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1699238
    return-object v0
.end method

.method private c(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1699264
    invoke-direct {p0, p1}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->d(Ljava/lang/CharSequence;)Landroid/text/Layout;

    move-result-object v0

    .line 1699265
    invoke-virtual {v0}, Landroid/text/Layout;->getLineCount()I

    move-result v1

    if-le v1, v2, :cond_0

    .line 1699266
    invoke-virtual {v0, v2}, Landroid/text/Layout;->getLineEnd(I)I

    move-result v0

    .line 1699267
    const/4 v1, 0x0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v1, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1699268
    invoke-direct {p0, v0}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->c(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 1699269
    :cond_0
    return-object p1
.end method

.method private d(Ljava/lang/CharSequence;)Landroid/text/Layout;
    .locals 8

    .prologue
    .line 1699263
    new-instance v0, Landroid/text/StaticLayout;

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->getMeasuredWidth()I

    move-result v3

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 1699254
    iput-object p2, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->g:Ljava/lang/CharSequence;

    .line 1699255
    iput-object p1, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->d:Ljava/lang/CharSequence;

    .line 1699256
    iput-object v1, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->e:Ljava/lang/CharSequence;

    .line 1699257
    iput-object v1, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->f:Ljava/lang/CharSequence;

    .line 1699258
    iput-boolean v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->a:Z

    .line 1699259
    iput v0, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->h:I

    .line 1699260
    invoke-virtual {p0, p1}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1699261
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->requestLayout()V

    .line 1699262
    return-void
.end method

.method public final onMeasure(II)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x1fbef7d1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1699239
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;->onMeasure(II)V

    .line 1699240
    iget-object v1, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->e:Ljava/lang/CharSequence;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->getMeasuredWidth()I

    move-result v1

    iget v2, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->h:I

    if-eq v1, v2, :cond_2

    .line 1699241
    :cond_0
    iget-object v1, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->d:Ljava/lang/CharSequence;

    invoke-direct {p0, v1}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->d(Ljava/lang/CharSequence;)Landroid/text/Layout;

    move-result-object v1

    .line 1699242
    invoke-virtual {v1}, Landroid/text/Layout;->getLineCount()I

    move-result v1

    if-le v1, v3, :cond_3

    .line 1699243
    iget-object v1, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->d:Ljava/lang/CharSequence;

    invoke-direct {p0, v1}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->e:Ljava/lang/CharSequence;

    .line 1699244
    iget-object v1, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->d:Ljava/lang/CharSequence;

    invoke-direct {p0, v1}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->f:Ljava/lang/CharSequence;

    .line 1699245
    :goto_0
    iget-object v1, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->g:Ljava/lang/CharSequence;

    if-eqz v1, :cond_1

    .line 1699246
    new-array v1, v3, [Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->e:Ljava/lang/CharSequence;

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->g:Ljava/lang/CharSequence;

    aput-object v2, v1, v5

    invoke-static {v1}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->e:Ljava/lang/CharSequence;

    .line 1699247
    new-array v1, v3, [Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->f:Ljava/lang/CharSequence;

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->g:Ljava/lang/CharSequence;

    aput-object v2, v1, v5

    invoke-static {v1}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->f:Ljava/lang/CharSequence;

    .line 1699248
    :cond_1
    invoke-static {p0}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->a(Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;)V

    .line 1699249
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->getMeasuredWidth()I

    move-result v1

    iput v1, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->h:I

    .line 1699250
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;->onMeasure(II)V

    .line 1699251
    :cond_2
    const v1, -0x7a3355d

    invoke-static {v1, v0}, LX/02F;->g(II)V

    return-void

    .line 1699252
    :cond_3
    iget-object v1, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->d:Ljava/lang/CharSequence;

    iput-object v1, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->e:Ljava/lang/CharSequence;

    .line 1699253
    iget-object v1, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->d:Ljava/lang/CharSequence;

    iput-object v1, p0, Lcom/facebook/facecastdisplay/liveevent/commentpinning/LiveCommentPinningTextView;->f:Ljava/lang/CharSequence;

    goto :goto_0
.end method
