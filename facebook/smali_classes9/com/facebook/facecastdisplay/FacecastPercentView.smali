.class public Lcom/facebook/facecastdisplay/FacecastPercentView;
.super Landroid/view/View;
.source ""


# instance fields
.field private a:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1692101
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecastdisplay/FacecastPercentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1692102
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1692103
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecastdisplay/FacecastPercentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1692104
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1692105
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1692106
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/facebook/facecastdisplay/FacecastPercentView;->a:F

    .line 1692107
    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1692108
    iget v0, p0, Lcom/facebook/facecastdisplay/FacecastPercentView;->a:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 1692109
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 1692110
    :goto_0
    return-void

    .line 1692111
    :cond_0
    invoke-static {v2, p1}, Landroid/view/View;->getDefaultSize(II)I

    move-result v0

    .line 1692112
    invoke-static {v2, p2}, Landroid/view/View;->getDefaultSize(II)I

    move-result v1

    .line 1692113
    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/facecastdisplay/FacecastPercentView;->a:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 1692114
    invoke-static {v1, p2}, Landroid/view/View;->resolveSize(II)I

    move-result v1

    .line 1692115
    invoke-virtual {p0, v0, v1}, Lcom/facebook/facecastdisplay/FacecastPercentView;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public setPercent(F)V
    .locals 1

    .prologue
    .line 1692116
    iget v0, p0, Lcom/facebook/facecastdisplay/FacecastPercentView;->a:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_0

    .line 1692117
    :goto_0
    return-void

    .line 1692118
    :cond_0
    iput p1, p0, Lcom/facebook/facecastdisplay/FacecastPercentView;->a:F

    .line 1692119
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/FacecastPercentView;->requestLayout()V

    goto :goto_0
.end method
