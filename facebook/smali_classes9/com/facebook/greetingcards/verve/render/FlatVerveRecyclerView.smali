.class public Lcom/facebook/greetingcards/verve/render/FlatVerveRecyclerView;
.super Landroid/support/v7/widget/RecyclerView;
.source ""


# instance fields
.field private final h:LX/CG8;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1864906
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/greetingcards/verve/render/FlatVerveRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1864907
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1864887
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1864888
    iput-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->v:Z

    .line 1864889
    new-instance v0, LX/1P1;

    const/4 v1, 0x1

    invoke-direct {v0, v1, v2}, LX/1P1;-><init>(IZ)V

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1864890
    new-instance v0, LX/CG8;

    invoke-direct {v0, p0}, LX/CG8;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    iput-object v0, p0, Lcom/facebook/greetingcards/verve/render/FlatVerveRecyclerView;->h:LX/CG8;

    .line 1864891
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/greetingcards/verve/model/VMDeck;LX/CEl;LX/CEO;LX/CEU;)V
    .locals 6

    .prologue
    .line 1864892
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/render/FlatVerveRecyclerView;->h:LX/CG8;

    .line 1864893
    iget-object v1, v0, LX/CG8;->d:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->clear()V

    .line 1864894
    iget-object v1, v0, LX/CG8;->e:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->clear()V

    .line 1864895
    iget-object v1, v0, LX/CG8;->f:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->clear()V

    .line 1864896
    iput-object p3, v0, LX/CG8;->b:LX/CEO;

    .line 1864897
    iget-object v1, v0, LX/CG8;->b:LX/CEO;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1864898
    iget-object v1, v0, LX/CG8;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, LX/CG7;

    invoke-direct {v2, v0}, LX/CG7;-><init>(LX/CG8;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1864899
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMDeck;->slides:LX/0Px;

    iget-object v1, p1, Lcom/facebook/greetingcards/verve/model/VMDeck;->initialSlide:Ljava/lang/String;

    invoke-static {p1, v1}, LX/CGA;->a(Lcom/facebook/greetingcards/verve/model/VMDeck;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/verve/model/VMSlide;

    .line 1864900
    invoke-virtual {p0}, Lcom/facebook/greetingcards/verve/render/FlatVerveRecyclerView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0}, Lcom/facebook/greetingcards/verve/model/VMSlide;->b()F

    move-result v0

    div-float v5, v1, v0

    .line 1864901
    new-instance v0, LX/CFp;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, LX/CFp;-><init>(Lcom/facebook/greetingcards/verve/model/VMDeck;LX/CEl;LX/CEO;LX/CEU;F)V

    .line 1864902
    new-instance v1, LX/CG4;

    invoke-direct {v1, v0}, LX/CG4;-><init>(LX/CFp;)V

    .line 1864903
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1864904
    return-void

    .line 1864905
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
