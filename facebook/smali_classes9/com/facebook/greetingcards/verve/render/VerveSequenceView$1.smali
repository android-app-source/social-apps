.class public final Lcom/facebook/greetingcards/verve/render/VerveSequenceView$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/CGJ;


# direct methods
.method public constructor <init>(LX/CGJ;)V
    .locals 0

    .prologue
    .line 1865289
    iput-object p1, p0, Lcom/facebook/greetingcards/verve/render/VerveSequenceView$1;->a:LX/CGJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    .prologue
    .line 1865290
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/render/VerveSequenceView$1;->a:LX/CGJ;

    .line 1865291
    iget-object v1, v0, LX/CGJ;->d:Lcom/facebook/greetingcards/verve/model/VMView;

    iget-object v1, v1, Lcom/facebook/greetingcards/verve/model/VMView;->subviews:LX/0Px;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/CGJ;->d:Lcom/facebook/greetingcards/verve/model/VMView;

    iget-object v1, v1, Lcom/facebook/greetingcards/verve/model/VMView;->subviews:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1865292
    :cond_0
    :goto_0
    return-void

    .line 1865293
    :cond_1
    iget v1, v0, LX/CGJ;->g:I

    invoke-static {v0, v1}, LX/CGJ;->a(LX/CGJ;I)Landroid/view/View;

    move-result-object v1

    .line 1865294
    iget v2, v0, LX/CGJ;->g:I

    add-int/lit8 v2, v2, 0x1

    iget-object v3, v0, LX/CGJ;->d:Lcom/facebook/greetingcards/verve/model/VMView;

    iget-object v3, v3, Lcom/facebook/greetingcards/verve/model/VMView;->subviews:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    rem-int/2addr v2, v3

    iput v2, v0, LX/CGJ;->g:I

    .line 1865295
    iget v2, v0, LX/CGJ;->g:I

    invoke-static {v0, v2}, LX/CGJ;->a(LX/CGJ;I)Landroid/view/View;

    move-result-object v2

    .line 1865296
    invoke-virtual {v0, v2}, LX/CGJ;->addView(Landroid/view/View;)V

    .line 1865297
    new-instance v4, LX/CGI;

    invoke-direct {v4, v0}, LX/CGI;-><init>(LX/CGJ;)V

    .line 1865298
    const/16 v5, 0x8

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 1865299
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 1865300
    if-eqz v4, :cond_2

    .line 1865301
    iget-object v6, v4, LX/CGI;->a:LX/CGJ;

    invoke-virtual {v6, v1}, LX/CGJ;->removeView(Landroid/view/View;)V

    .line 1865302
    iget-object v6, v4, LX/CGI;->a:LX/CGJ;

    iget-object v6, v6, LX/CGJ;->c:Landroid/os/Handler;

    iget-object v7, v4, LX/CGI;->a:LX/CGJ;

    iget-object v7, v7, LX/CGJ;->h:Ljava/lang/Runnable;

    iget-object v8, v4, LX/CGI;->a:LX/CGJ;

    iget v8, v8, LX/CGJ;->e:I

    int-to-long v8, v8

    const v10, 0x4ea455eb

    invoke-static {v6, v7, v8, v9, v10}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1865303
    :cond_2
    goto :goto_0
.end method
