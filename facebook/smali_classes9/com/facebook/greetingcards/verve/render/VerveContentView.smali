.class public Lcom/facebook/greetingcards/verve/render/VerveContentView;
.super Landroid/widget/FrameLayout;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:LX/CG2;

.field public c:LX/CFp;

.field public d:I

.field public e:I

.field private f:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/CGC;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1865254
    const-class v0, Lcom/facebook/greetingcards/verve/render/VerveContentView;

    sput-object v0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1865255
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 1865256
    iput v0, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->d:I

    .line 1865257
    iput v0, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->e:I

    .line 1865258
    invoke-direct {p0}, Lcom/facebook/greetingcards/verve/render/VerveContentView;->a()V

    .line 1865259
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1865265
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1865266
    iput v0, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->d:I

    .line 1865267
    iput v0, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->e:I

    .line 1865268
    invoke-direct {p0}, Lcom/facebook/greetingcards/verve/render/VerveContentView;->a()V

    .line 1865269
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1865260
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1865261
    iput v0, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->d:I

    .line 1865262
    iput v0, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->e:I

    .line 1865263
    invoke-direct {p0}, Lcom/facebook/greetingcards/verve/render/VerveContentView;->a()V

    .line 1865264
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1865270
    new-instance v0, LX/CGC;

    invoke-direct {v0}, LX/CGC;-><init>()V

    iput-object v0, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->h:LX/CGC;

    .line 1865271
    invoke-static {}, LX/0RA;->b()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->g:Ljava/util/Set;

    .line 1865272
    new-instance v0, LX/CG2;

    new-instance v1, LX/1wz;

    invoke-virtual {p0}, Lcom/facebook/greetingcards/verve/render/VerveContentView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1wz;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1, p0}, LX/CG2;-><init>(LX/1wz;Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->b:LX/CG2;

    .line 1865273
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->b:LX/CG2;

    new-instance v1, LX/CGF;

    invoke-direct {v1, p0}, LX/CGF;-><init>(Lcom/facebook/greetingcards/verve/render/VerveContentView;)V

    .line 1865274
    iput-object v1, v0, LX/CG2;->f:LX/CGF;

    .line 1865275
    return-void
.end method

.method public static a(Lcom/facebook/greetingcards/verve/render/VerveContentView;I)V
    .locals 8

    .prologue
    .line 1865232
    iget v0, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->d:I

    iget-object v1, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->c:LX/CFp;

    .line 1865233
    invoke-virtual {v1, v0}, LX/CFp;->b(I)LX/0Px;

    move-result-object v2

    invoke-virtual {v1, p1}, LX/CFp;->b(I)LX/0Px;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v0, p1, v2, v3, v4}, LX/CFu;->a(IILX/0Px;LX/0Px;Z)LX/CFt;

    move-result-object v2

    move-object v3, v2

    .line 1865234
    iget-object v4, v3, LX/CFt;->a:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_1

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CFr;

    .line 1865235
    const/4 v1, 0x0

    .line 1865236
    sget-object v6, LX/CGG;->a:[I

    iget-object v7, v0, LX/CFr;->a:LX/CFs;

    invoke-virtual {v7}, LX/CFs;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 1865237
    :goto_1
    instance-of v6, v1, LX/CGH;

    if-eqz v6, :cond_0

    .line 1865238
    check-cast v1, LX/CGH;

    .line 1865239
    iget-object v6, v1, LX/CGH;->a:LX/CGE;

    move-object v1, v6

    .line 1865240
    iget v6, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->d:I

    iget-object v7, v0, LX/CFr;->b:Lcom/facebook/greetingcards/verve/model/VMView;

    iget-object v0, v0, LX/CFr;->c:Lcom/facebook/greetingcards/verve/model/VMView;

    invoke-virtual {v1, v6, p1, v7, v0}, LX/CGE;->a(IILcom/facebook/greetingcards/verve/model/VMView;Lcom/facebook/greetingcards/verve/model/VMView;)V

    .line 1865241
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1865242
    :pswitch_0
    iget-object v1, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->f:LX/0YU;

    iget-object v6, v0, LX/CFr;->c:Lcom/facebook/greetingcards/verve/model/VMView;

    iget v6, v6, Lcom/facebook/greetingcards/verve/model/VMView;->a:I

    invoke-virtual {v1, v6}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    iget-object v6, v0, LX/CFr;->b:Lcom/facebook/greetingcards/verve/model/VMView;

    invoke-static {v1, v6}, LX/CGS;->b(Landroid/view/View;Lcom/facebook/greetingcards/verve/model/VMView;)V

    .line 1865243
    iget-object v1, v0, LX/CFr;->c:Lcom/facebook/greetingcards/verve/model/VMView;

    iget v1, v1, Lcom/facebook/greetingcards/verve/model/VMView;->a:I

    invoke-direct {p0, v1}, Lcom/facebook/greetingcards/verve/render/VerveContentView;->b(I)V

    .line 1865244
    iget-object v1, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->f:LX/0YU;

    iget-object v6, v0, LX/CFr;->c:Lcom/facebook/greetingcards/verve/model/VMView;

    iget v6, v6, Lcom/facebook/greetingcards/verve/model/VMView;->a:I

    invoke-virtual {v1, v6}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    goto :goto_1

    .line 1865245
    :pswitch_1
    iget-object v1, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->f:LX/0YU;

    iget-object v6, v0, LX/CFr;->b:Lcom/facebook/greetingcards/verve/model/VMView;

    iget v6, v6, Lcom/facebook/greetingcards/verve/model/VMView;->a:I

    invoke-virtual {v1, v6}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/facebook/greetingcards/verve/render/VerveContentView;->bringChildToFront(Landroid/view/View;)V

    .line 1865246
    iget-object v1, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->f:LX/0YU;

    iget-object v6, v0, LX/CFr;->b:Lcom/facebook/greetingcards/verve/model/VMView;

    iget v6, v6, Lcom/facebook/greetingcards/verve/model/VMView;->a:I

    invoke-virtual {v1, v6}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    goto :goto_1

    .line 1865247
    :cond_1
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->h:LX/CGC;

    .line 1865248
    iput-object v3, v0, LX/CGC;->b:LX/CFt;

    .line 1865249
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 1865229
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->g:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1865230
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->f:LX/0YU;

    invoke-virtual {v0, p1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/facebook/greetingcards/verve/render/VerveContentView;->addView(Landroid/view/View;)V

    .line 1865231
    return-void
.end method

.method private c(I)V
    .locals 2

    .prologue
    .line 1865250
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->g:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1865251
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->f:LX/0YU;

    invoke-virtual {v0, p1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1865252
    invoke-virtual {p0, v0}, Lcom/facebook/greetingcards/verve/render/VerveContentView;->removeView(Landroid/view/View;)V

    .line 1865253
    return-void
.end method

.method private d(I)V
    .locals 6

    .prologue
    .line 1865124
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->c:LX/CFp;

    invoke-virtual {v0, p1}, LX/CFp;->b(I)LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/verve/model/VMView;

    .line 1865125
    iget-object v1, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->f:LX/0YU;

    iget v5, v0, Lcom/facebook/greetingcards/verve/model/VMView;->a:I

    invoke-virtual {v1, v5}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-static {v1, v0}, LX/CGS;->a(Landroid/view/View;Lcom/facebook/greetingcards/verve/model/VMView;)V

    .line 1865126
    iget v1, v0, Lcom/facebook/greetingcards/verve/model/VMView;->a:I

    invoke-direct {p0, v1}, Lcom/facebook/greetingcards/verve/render/VerveContentView;->b(I)V

    .line 1865127
    iget-object v1, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->f:LX/0YU;

    iget v5, v0, Lcom/facebook/greetingcards/verve/model/VMView;->a:I

    invoke-virtual {v1, v5}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v0}, Lcom/facebook/greetingcards/verve/model/VMView;->b()F

    move-result v5

    invoke-virtual {v1, v5}, Landroid/view/View;->setAlpha(F)V

    .line 1865128
    invoke-virtual {v0}, Lcom/facebook/greetingcards/verve/model/VMView;->o()LX/CFo;

    move-result-object v1

    sget-object v5, LX/CFo;->GROUP:LX/CFo;

    if-ne v1, v5, :cond_0

    .line 1865129
    iget-object v1, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->f:LX/0YU;

    iget v0, v0, Lcom/facebook/greetingcards/verve/model/VMView;->a:I

    invoke-virtual {v1, v0}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CGH;

    .line 1865130
    iget-object v1, v0, LX/CGH;->a:LX/CGE;

    move-object v0, v1

    .line 1865131
    invoke-virtual {v0}, LX/CGE;->a()V

    .line 1865132
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1865133
    :cond_1
    invoke-static {p0, p1}, Lcom/facebook/greetingcards/verve/render/VerveContentView;->e(Lcom/facebook/greetingcards/verve/render/VerveContentView;I)V

    .line 1865134
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->b:LX/CG2;

    iget-object v1, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->c:LX/CFp;

    .line 1865135
    iget-object v2, v1, LX/CFp;->a:Lcom/facebook/greetingcards/verve/model/VMDeck;

    move-object v1, v2

    .line 1865136
    invoke-static {v1, p1}, LX/CGA;->a(Lcom/facebook/greetingcards/verve/model/VMDeck;I)I

    move-result v1

    .line 1865137
    iput v1, v0, LX/CG2;->i:I

    .line 1865138
    iget-object v2, v0, LX/CG2;->c:LX/1wz;

    .line 1865139
    iput v1, v2, LX/1wz;->p:I

    .line 1865140
    return-void
.end method

.method public static e(Lcom/facebook/greetingcards/verve/render/VerveContentView;I)V
    .locals 11

    .prologue
    .line 1865141
    iput p1, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->d:I

    .line 1865142
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->c:LX/CFp;

    .line 1865143
    iget-object v1, v0, LX/CFp;->h:LX/0YU;

    invoke-virtual {v1, p1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/greetingcards/verve/model/VMColor;

    .line 1865144
    if-nez v1, :cond_0

    .line 1865145
    iget-object v1, v0, LX/CFp;->a:Lcom/facebook/greetingcards/verve/model/VMDeck;

    invoke-virtual {v1}, Lcom/facebook/greetingcards/verve/model/VMDeck;->a()Lcom/facebook/greetingcards/verve/model/VMColor;

    move-result-object v1

    .line 1865146
    :cond_0
    move-object v0, v1

    .line 1865147
    invoke-static {v0}, LX/CFw;->a(Lcom/facebook/greetingcards/verve/model/VMColor;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/greetingcards/verve/render/VerveContentView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1865148
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v3

    .line 1865149
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1865150
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1865151
    iget-object v2, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->c:LX/CFp;

    .line 1865152
    iget-object v4, v2, LX/CFp;->i:LX/0YU;

    invoke-virtual {v4, v0}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/greetingcards/verve/model/VMView;

    move-object v2, v4

    .line 1865153
    invoke-virtual {v2}, Lcom/facebook/greetingcards/verve/model/VMView;->o()LX/CFo;

    move-result-object v2

    sget-object v4, LX/CFo;->GROUP:LX/CFo;

    if-ne v2, v4, :cond_1

    .line 1865154
    iget-object v2, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->f:LX/0YU;

    invoke-virtual {v2, v0}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CGH;

    .line 1865155
    iget-object v2, v0, LX/CGH;->a:LX/CGE;

    move-object v0, v2

    .line 1865156
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v7

    .line 1865157
    iget-object v2, v0, LX/CGE;->d:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 1865158
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v7, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1865159
    :cond_2
    iget-object v2, v0, LX/CGE;->e:LX/0YU;

    invoke-virtual {v2, p1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Px;

    .line 1865160
    if-eqz v2, :cond_4

    .line 1865161
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v8

    const/4 v4, 0x0

    move v6, v4

    :goto_2
    if-ge v6, v8, :cond_4

    invoke-virtual {v2, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/greetingcards/verve/model/VMView;

    .line 1865162
    iget-object v5, v0, LX/CGE;->d:Ljava/util/Set;

    iget v9, v4, Lcom/facebook/greetingcards/verve/model/VMView;->a:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v5, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1865163
    iget-object v9, v0, LX/CGE;->a:Landroid/widget/FrameLayout;

    iget-object v5, v0, LX/CGE;->g:LX/0YU;

    iget v10, v4, Lcom/facebook/greetingcards/verve/model/VMView;->a:I

    invoke-virtual {v5, v10}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    invoke-virtual {v9, v5}, Landroid/widget/FrameLayout;->bringChildToFront(Landroid/view/View;)V

    .line 1865164
    :goto_3
    iget v4, v4, Lcom/facebook/greetingcards/verve/model/VMView;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v7, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1865165
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_2

    .line 1865166
    :cond_3
    iget v5, v4, Lcom/facebook/greetingcards/verve/model/VMView;->a:I

    invoke-static {v0, v5}, LX/CGE;->b(LX/CGE;I)V

    goto :goto_3

    .line 1865167
    :cond_4
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 1865168
    iget-object v5, v0, LX/CGE;->d:Ljava/util/Set;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1865169
    iget-object v5, v0, LX/CGE;->g:LX/0YU;

    invoke-virtual {v5, v2}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    .line 1865170
    iget-object v6, v0, LX/CGE;->f:LX/0YU;

    invoke-virtual {v6, v2}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/greetingcards/verve/model/VMView;

    invoke-static {v5, v6}, LX/CGS;->a(Landroid/view/View;Lcom/facebook/greetingcards/verve/model/VMView;)V

    .line 1865171
    iget-object v6, v0, LX/CGE;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v6, v5}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 1865172
    goto :goto_4

    .line 1865173
    :cond_5
    goto/16 :goto_0

    .line 1865174
    :cond_6
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->c:LX/CFp;

    iget v1, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->d:I

    invoke-virtual {v0, v1}, LX/CFp;->b(I)LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_5
    if-ge v2, v5, :cond_9

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/verve/model/VMView;

    .line 1865175
    iget-object v1, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->g:Ljava/util/Set;

    iget v6, v0, Lcom/facebook/greetingcards/verve/model/VMView;->a:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1865176
    iget-object v1, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->f:LX/0YU;

    iget v6, v0, Lcom/facebook/greetingcards/verve/model/VMView;->a:I

    invoke-virtual {v1, v6}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/facebook/greetingcards/verve/render/VerveContentView;->bringChildToFront(Landroid/view/View;)V

    .line 1865177
    :cond_7
    :goto_6
    iget v0, v0, Lcom/facebook/greetingcards/verve/model/VMView;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1865178
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    .line 1865179
    :cond_8
    iget-object v1, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->f:LX/0YU;

    iget v6, v0, Lcom/facebook/greetingcards/verve/model/VMView;->a:I

    invoke-virtual {v1, v6}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-static {v1, v0}, LX/CGS;->a(Landroid/view/View;Lcom/facebook/greetingcards/verve/model/VMView;)V

    .line 1865180
    iget v1, v0, Lcom/facebook/greetingcards/verve/model/VMView;->a:I

    invoke-direct {p0, v1}, Lcom/facebook/greetingcards/verve/render/VerveContentView;->b(I)V

    .line 1865181
    invoke-virtual {v0}, Lcom/facebook/greetingcards/verve/model/VMView;->o()LX/CFo;

    move-result-object v1

    sget-object v6, LX/CFo;->GROUP:LX/CFo;

    if-ne v1, v6, :cond_7

    .line 1865182
    iget-object v1, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->f:LX/0YU;

    iget v6, v0, Lcom/facebook/greetingcards/verve/model/VMView;->a:I

    invoke-virtual {v1, v6}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CGH;

    .line 1865183
    iget v6, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->d:I

    invoke-virtual {v1, v0, v6}, LX/CGH;->a(Lcom/facebook/greetingcards/verve/model/VMView;I)V

    .line 1865184
    iget-object v6, v1, LX/CGH;->a:LX/CGE;

    move-object v1, v6

    .line 1865185
    invoke-virtual {v1}, LX/CGE;->a()V

    goto :goto_6

    .line 1865186
    :cond_9
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1865187
    invoke-direct {p0, v0}, Lcom/facebook/greetingcards/verve/render/VerveContentView;->c(I)V

    goto :goto_7

    .line 1865188
    :cond_a
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/greetingcards/verve/model/VMDeck;LX/CEl;LX/CEO;LX/CEU;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1865189
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->c:LX/CFp;

    if-eqz v0, :cond_0

    .line 1865190
    invoke-virtual {p0}, Lcom/facebook/greetingcards/verve/render/VerveContentView;->removeAllViews()V

    .line 1865191
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->f:LX/0YU;

    .line 1865192
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->c:LX/CFp;

    .line 1865193
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1865194
    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->d:I

    .line 1865195
    new-instance v0, LX/CFp;

    const/high16 v5, 0x3f800000    # 1.0f

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, LX/CFp;-><init>(Lcom/facebook/greetingcards/verve/model/VMDeck;LX/CEl;LX/CEO;LX/CEU;F)V

    iput-object v0, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->c:LX/CFp;

    .line 1865196
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->c:LX/CFp;

    invoke-virtual {p0}, Lcom/facebook/greetingcards/verve/render/VerveContentView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1865197
    new-instance v4, LX/0YU;

    invoke-direct {v4}, LX/0YU;-><init>()V

    .line 1865198
    const/4 v2, 0x0

    move v3, v2

    .line 1865199
    :goto_0
    iget-object v2, v0, LX/CFp;->a:Lcom/facebook/greetingcards/verve/model/VMDeck;

    move-object v2, v2

    .line 1865200
    iget-object v2, v2, Lcom/facebook/greetingcards/verve/model/VMDeck;->slides:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_2

    .line 1865201
    iget-object v2, v0, LX/CFp;->a:Lcom/facebook/greetingcards/verve/model/VMDeck;

    move-object v2, v2

    .line 1865202
    iget-object v2, v2, Lcom/facebook/greetingcards/verve/model/VMDeck;->slides:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/greetingcards/verve/model/VMSlide;

    .line 1865203
    iget-boolean v2, v2, Lcom/facebook/greetingcards/verve/model/VMSlide;->master:Z

    if-nez v2, :cond_1

    .line 1865204
    invoke-virtual {v0, v3}, LX/CFp;->b(I)LX/0Px;

    move-result-object v2

    invoke-static {v2, v3, v0, v1, v4}, LX/CGN;->a(LX/0Px;ILX/CFp;Landroid/content/Context;LX/0YU;)V

    .line 1865205
    :cond_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 1865206
    :cond_2
    move-object v0, v4

    .line 1865207
    iput-object v0, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->f:LX/0YU;

    .line 1865208
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->h:LX/CGC;

    iget-object v1, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->f:LX/0YU;

    .line 1865209
    iput-object v1, v0, LX/CGC;->a:LX/0YU;

    .line 1865210
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMDeck;->initialSlide:Ljava/lang/String;

    invoke-static {p1, v0}, LX/CGA;->a(Lcom/facebook/greetingcards/verve/model/VMDeck;Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/greetingcards/verve/render/VerveContentView;->d(I)V

    .line 1865211
    return-void
.end method

.method public final a(LX/31M;)Z
    .locals 9

    .prologue
    .line 1865212
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->b:LX/CG2;

    const-wide v7, 0x3fd3333333333333L    # 0.3

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1865213
    iget v1, v0, LX/CG2;->i:I

    sget-object v4, LX/31M;->UP:LX/31M;

    invoke-virtual {v4}, LX/31M;->flag()I

    move-result v4

    if-ne v1, v4, :cond_2

    sget-object v1, LX/31M;->DOWN:LX/31M;

    if-ne p1, v1, :cond_2

    iget-object v1, v0, LX/CG2;->h:LX/CG1;

    sget-object v4, LX/CG1;->IDLE:LX/CG1;

    if-eq v1, v4, :cond_0

    iget-object v1, v0, LX/CG2;->h:LX/CG1;

    sget-object v4, LX/CG1;->DRAGGING:LX/CG1;

    if-ne v1, v4, :cond_2

    invoke-static {v0}, LX/CG2;->d(LX/CG2;)D

    move-result-wide v5

    cmpg-double v1, v5, v7

    if-gez v1, :cond_2

    :cond_0
    move v1, v2

    .line 1865214
    :goto_0
    iget v4, v0, LX/CG2;->i:I

    sget-object v5, LX/31M;->DOWN:LX/31M;

    invoke-virtual {v5}, LX/31M;->flag()I

    move-result v5

    if-ne v4, v5, :cond_3

    sget-object v4, LX/31M;->UP:LX/31M;

    if-ne p1, v4, :cond_3

    iget-object v4, v0, LX/CG2;->h:LX/CG1;

    sget-object v5, LX/CG1;->IDLE:LX/CG1;

    if-eq v4, v5, :cond_1

    iget-object v4, v0, LX/CG2;->h:LX/CG1;

    sget-object v5, LX/CG1;->DRAGGING:LX/CG1;

    if-ne v4, v5, :cond_3

    invoke-static {v0}, LX/CG2;->d(LX/CG2;)D

    move-result-wide v5

    cmpg-double v4, v5, v7

    if-gez v4, :cond_3

    :cond_1
    move v4, v2

    .line 1865215
    :goto_1
    if-nez v1, :cond_4

    if-nez v4, :cond_4

    :goto_2
    move v0, v2

    .line 1865216
    return v0

    :cond_2
    move v1, v3

    .line 1865217
    goto :goto_0

    :cond_3
    move v4, v3

    .line 1865218
    goto :goto_1

    :cond_4
    move v2, v3

    .line 1865219
    goto :goto_2
.end method

.method public getCurrentSlide()Lcom/facebook/greetingcards/verve/model/VMSlide;
    .locals 2

    .prologue
    .line 1865220
    iget v0, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->d:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->c:LX/CFp;

    iget v1, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->d:I

    .line 1865221
    iget-object p0, v0, LX/CFp;->a:Lcom/facebook/greetingcards/verve/model/VMDeck;

    iget-object p0, p0, Lcom/facebook/greetingcards/verve/model/VMDeck;->slides:LX/0Px;

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/greetingcards/verve/model/VMSlide;

    move-object v0, p0

    .line 1865222
    goto :goto_0
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1865223
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->b:LX/CG2;

    .line 1865224
    iget-object p0, v0, LX/CG2;->c:LX/1wz;

    invoke-virtual {p0, p1}, LX/1wz;->a(Landroid/view/MotionEvent;)Z

    move-result p0

    move v0, p0

    .line 1865225
    return v0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x51a6f887

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1865226
    iget-object v1, p0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->b:LX/CG2;

    .line 1865227
    iget-object v2, v1, LX/CG2;->c:LX/1wz;

    invoke-virtual {v2, p1}, LX/1wz;->b(Landroid/view/MotionEvent;)Z

    move-result v2

    move v1, v2

    .line 1865228
    const v2, 0x8b33e03

    invoke-static {v3, v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method
