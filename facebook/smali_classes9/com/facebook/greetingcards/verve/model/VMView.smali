.class public Lcom/facebook/greetingcards/verve/model/VMView;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/greetingcards/verve/model/VMViewDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private static final f:Lcom/facebook/greetingcards/verve/model/VMColor;

.field private static final g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private static final h:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field public final a:I

.field public final actionsMap:LX/0P1;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "actions"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/greetingcards/verve/model/VMAction;",
            ">;"
        }
    .end annotation
.end field

.field public final alignment:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "alignment"
    .end annotation
.end field

.field public final anchor:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "anchor"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Landroid/graphics/PointF;

.field public final buttonSize:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "button-size"
    .end annotation
.end field

.field public final buttonStyle:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "button-style"
    .end annotation
.end field

.field public final duration:Ljava/lang/Float;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "duration"
    .end annotation
.end field

.field public final fillColor:Lcom/facebook/greetingcards/verve/model/VMColor;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "fill-color"
    .end annotation
.end field

.field public final font:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "font"
    .end annotation
.end field

.field public final fontSize:Ljava/lang/Float;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "font-size"
    .end annotation
.end field

.field public final i:F

.field private j:LX/CFi;

.field private k:LX/CFo;

.field public final lineBreak:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "line-break"
    .end annotation
.end field

.field public final mType:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "type"
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation
.end field

.field public final opacity:Ljava/lang/Float;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "opacity"
    .end annotation
.end field

.field public final placeholderColor:Lcom/facebook/greetingcards/verve/model/VMColor;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "placeholder-color"
    .end annotation
.end field

.field public final placeholderSrc:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "placeholder-src"
    .end annotation
.end field

.field public final position:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "position"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public final resize:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "resize"
    .end annotation
.end field

.field public final rotation:Ljava/lang/Float;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "rotation"
    .end annotation
.end field

.field public final shadowColor:Lcom/facebook/greetingcards/verve/model/VMColor;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "shadow-color"
    .end annotation
.end field

.field public final shadowOffset:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "shadow-offset"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public final shadowRadius:Ljava/lang/Integer;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "shadow-radius"
    .end annotation
.end field

.field public final size:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "size"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public final src:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "src"
    .end annotation
.end field

.field public final statesMap:LX/0P1;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "states"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            ">;"
        }
    .end annotation
.end field

.field public final styleName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "style"
    .end annotation
.end field

.field public final subviews:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "subviews"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            ">;"
        }
    .end annotation
.end field

.field public final text:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text"
    .end annotation
.end field

.field public final title:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "title"
    .end annotation
.end field

.field public final vAlignment:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "v-alignment"
    .end annotation
.end field

.field public final valueName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "value-name"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1864252
    const-class v0, Lcom/facebook/greetingcards/verve/model/VMViewDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/high16 v2, 0x3f000000    # 0.5f

    const/4 v4, 0x0

    .line 1864253
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, Lcom/facebook/greetingcards/verve/model/VMView;->c:LX/0Px;

    .line 1864254
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, Lcom/facebook/greetingcards/verve/model/VMView;->d:LX/0Px;

    .line 1864255
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, Lcom/facebook/greetingcards/verve/model/VMView;->e:LX/0Px;

    .line 1864256
    new-instance v0, Lcom/facebook/greetingcards/verve/model/VMColor;

    const-string v1, "solid"

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-static {v2, v5, v6}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/facebook/greetingcards/verve/model/VMColor;-><init>(Ljava/lang/String;LX/0Px;LX/0Px;FLX/0Px;)V

    sput-object v0, Lcom/facebook/greetingcards/verve/model/VMView;->f:Lcom/facebook/greetingcards/verve/model/VMColor;

    .line 1864257
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    const/high16 v1, 0x40400000    # 3.0f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, Lcom/facebook/greetingcards/verve/model/VMView;->g:LX/0Px;

    .line 1864258
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Lcom/facebook/greetingcards/verve/model/VMView;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1864259
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1864260
    sget-object v0, Lcom/facebook/greetingcards/verve/model/VMView;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    iput v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->a:I

    .line 1864261
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->i:F

    .line 1864262
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->name:Ljava/lang/String;

    .line 1864263
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->size:LX/0Px;

    .line 1864264
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->position:LX/0Px;

    .line 1864265
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->anchor:LX/0Px;

    .line 1864266
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->mType:Ljava/lang/String;

    .line 1864267
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->rotation:Ljava/lang/Float;

    .line 1864268
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->opacity:Ljava/lang/Float;

    .line 1864269
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->src:Ljava/lang/String;

    .line 1864270
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->placeholderSrc:Ljava/lang/String;

    .line 1864271
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->resize:Ljava/lang/String;

    .line 1864272
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->fillColor:Lcom/facebook/greetingcards/verve/model/VMColor;

    .line 1864273
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->subviews:LX/0Px;

    .line 1864274
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->statesMap:LX/0P1;

    .line 1864275
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->text:Ljava/lang/String;

    .line 1864276
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->fontSize:Ljava/lang/Float;

    .line 1864277
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->font:Ljava/lang/String;

    .line 1864278
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->shadowColor:Lcom/facebook/greetingcards/verve/model/VMColor;

    .line 1864279
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->shadowRadius:Ljava/lang/Integer;

    .line 1864280
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->shadowOffset:LX/0Px;

    .line 1864281
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->alignment:Ljava/lang/String;

    .line 1864282
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->vAlignment:Ljava/lang/String;

    .line 1864283
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->valueName:Ljava/lang/String;

    .line 1864284
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->styleName:Ljava/lang/String;

    .line 1864285
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->title:Ljava/lang/String;

    .line 1864286
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->buttonStyle:Ljava/lang/String;

    .line 1864287
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->buttonSize:Ljava/lang/String;

    .line 1864288
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->actionsMap:LX/0P1;

    .line 1864289
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->duration:Ljava/lang/Float;

    .line 1864290
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->b:Landroid/graphics/PointF;

    .line 1864291
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->placeholderColor:Lcom/facebook/greetingcards/verve/model/VMColor;

    .line 1864292
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->lineBreak:Ljava/lang/String;

    .line 1864293
    return-void
.end method

.method private constructor <init>(FLjava/lang/String;LX/0Px;LX/0Px;LX/0Px;Ljava/lang/String;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/greetingcards/verve/model/VMColor;LX/0Px;LX/0P1;Ljava/lang/String;Ljava/lang/Float;Ljava/lang/String;Lcom/facebook/greetingcards/verve/model/VMColor;Ljava/lang/Integer;LX/0Px;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0P1;Ljava/lang/Float;Landroid/graphics/PointF;Lcom/facebook/greetingcards/verve/model/VMColor;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/greetingcards/verve/model/VMColor;",
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            ">;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            "Ljava/lang/String;",
            "Lcom/facebook/greetingcards/verve/model/VMColor;",
            "Ljava/lang/Integer;",
            "LX/0Px",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/greetingcards/verve/model/VMAction;",
            ">;",
            "Ljava/lang/Float;",
            "Landroid/graphics/PointF;",
            "Lcom/facebook/greetingcards/verve/model/VMColor;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1864294
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1864295
    iput p1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->i:F

    .line 1864296
    sget-object v1, Lcom/facebook/greetingcards/verve/model/VMView;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v1

    iput v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->a:I

    .line 1864297
    iput-object p2, p0, Lcom/facebook/greetingcards/verve/model/VMView;->name:Ljava/lang/String;

    .line 1864298
    iput-object p3, p0, Lcom/facebook/greetingcards/verve/model/VMView;->size:LX/0Px;

    .line 1864299
    iput-object p4, p0, Lcom/facebook/greetingcards/verve/model/VMView;->position:LX/0Px;

    .line 1864300
    iput-object p5, p0, Lcom/facebook/greetingcards/verve/model/VMView;->anchor:LX/0Px;

    .line 1864301
    iput-object p6, p0, Lcom/facebook/greetingcards/verve/model/VMView;->mType:Ljava/lang/String;

    .line 1864302
    iput-object p7, p0, Lcom/facebook/greetingcards/verve/model/VMView;->rotation:Ljava/lang/Float;

    .line 1864303
    iput-object p8, p0, Lcom/facebook/greetingcards/verve/model/VMView;->opacity:Ljava/lang/Float;

    .line 1864304
    iput-object p9, p0, Lcom/facebook/greetingcards/verve/model/VMView;->src:Ljava/lang/String;

    .line 1864305
    iput-object p10, p0, Lcom/facebook/greetingcards/verve/model/VMView;->placeholderSrc:Ljava/lang/String;

    .line 1864306
    iput-object p11, p0, Lcom/facebook/greetingcards/verve/model/VMView;->resize:Ljava/lang/String;

    .line 1864307
    iput-object p12, p0, Lcom/facebook/greetingcards/verve/model/VMView;->fillColor:Lcom/facebook/greetingcards/verve/model/VMColor;

    .line 1864308
    iput-object p13, p0, Lcom/facebook/greetingcards/verve/model/VMView;->subviews:LX/0Px;

    .line 1864309
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->statesMap:LX/0P1;

    .line 1864310
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->text:Ljava/lang/String;

    .line 1864311
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->fontSize:Ljava/lang/Float;

    .line 1864312
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->font:Ljava/lang/String;

    .line 1864313
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->shadowColor:Lcom/facebook/greetingcards/verve/model/VMColor;

    .line 1864314
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->shadowRadius:Ljava/lang/Integer;

    .line 1864315
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->shadowOffset:LX/0Px;

    .line 1864316
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->alignment:Ljava/lang/String;

    .line 1864317
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->vAlignment:Ljava/lang/String;

    .line 1864318
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->valueName:Ljava/lang/String;

    .line 1864319
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->styleName:Ljava/lang/String;

    .line 1864320
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->title:Ljava/lang/String;

    .line 1864321
    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->buttonStyle:Ljava/lang/String;

    .line 1864322
    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->buttonSize:Ljava/lang/String;

    .line 1864323
    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->actionsMap:LX/0P1;

    .line 1864324
    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->duration:Ljava/lang/Float;

    .line 1864325
    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->b:Landroid/graphics/PointF;

    .line 1864326
    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->placeholderColor:Lcom/facebook/greetingcards/verve/model/VMColor;

    .line 1864327
    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->lineBreak:Ljava/lang/String;

    .line 1864328
    return-void
.end method

.method public synthetic constructor <init>(FLjava/lang/String;LX/0Px;LX/0Px;LX/0Px;Ljava/lang/String;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/greetingcards/verve/model/VMColor;LX/0Px;LX/0P1;Ljava/lang/String;Ljava/lang/Float;Ljava/lang/String;Lcom/facebook/greetingcards/verve/model/VMColor;Ljava/lang/Integer;LX/0Px;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0P1;Ljava/lang/Float;Landroid/graphics/PointF;Lcom/facebook/greetingcards/verve/model/VMColor;Ljava/lang/String;B)V
    .locals 0

    .prologue
    .line 1864329
    invoke-direct/range {p0 .. p32}, Lcom/facebook/greetingcards/verve/model/VMView;-><init>(FLjava/lang/String;LX/0Px;LX/0Px;LX/0Px;Ljava/lang/String;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/greetingcards/verve/model/VMColor;LX/0Px;LX/0P1;Ljava/lang/String;Ljava/lang/Float;Ljava/lang/String;Lcom/facebook/greetingcards/verve/model/VMColor;Ljava/lang/Integer;LX/0Px;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0P1;Ljava/lang/Float;Landroid/graphics/PointF;Lcom/facebook/greetingcards/verve/model/VMColor;Ljava/lang/String;)V

    return-void
.end method

.method public static p()LX/CFn;
    .locals 2

    .prologue
    .line 1864247
    new-instance v0, LX/CFn;

    invoke-direct {v0}, LX/CFn;-><init>()V

    return-object v0
.end method

.method private q()LX/CFi;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1864330
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->j:LX/CFi;

    if-nez v0, :cond_0

    .line 1864331
    new-instance v1, LX/CFi;

    invoke-direct {p0}, Lcom/facebook/greetingcards/verve/model/VMView;->s()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-direct {p0}, Lcom/facebook/greetingcards/verve/model/VMView;->t()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v3

    invoke-direct {p0}, Lcom/facebook/greetingcards/verve/model/VMView;->r()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    mul-float/2addr v0, v3

    sub-float/2addr v2, v0

    invoke-direct {p0}, Lcom/facebook/greetingcards/verve/model/VMView;->s()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v3

    invoke-direct {p0}, Lcom/facebook/greetingcards/verve/model/VMView;->t()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-direct {p0}, Lcom/facebook/greetingcards/verve/model/VMView;->r()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    mul-float/2addr v0, v4

    sub-float/2addr v3, v0

    invoke-direct {p0}, Lcom/facebook/greetingcards/verve/model/VMView;->r()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-direct {p0}, Lcom/facebook/greetingcards/verve/model/VMView;->r()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-direct {v1, v2, v3, v4, v0}, LX/CFi;-><init>(FFFF)V

    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->j:LX/CFi;

    .line 1864332
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->j:LX/CFi;

    iget v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->i:F

    .line 1864333
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v1, v2

    if-nez v2, :cond_1

    .line 1864334
    :goto_0
    move-object v0, v0

    .line 1864335
    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->j:LX/CFi;

    .line 1864336
    :cond_0
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->j:LX/CFi;

    return-object v0

    :cond_1
    new-instance v2, LX/CFi;

    iget v3, v0, LX/CFi;->a:F

    mul-float/2addr v3, v1

    iget v4, v0, LX/CFi;->b:F

    mul-float/2addr v4, v1

    iget v5, v0, LX/CFi;->c:F

    mul-float/2addr v5, v1

    iget v6, v0, LX/CFi;->d:F

    mul-float/2addr v6, v1

    invoke-direct {v2, v3, v4, v5, v6}, LX/CFi;-><init>(FFFF)V

    move-object v0, v2

    goto :goto_0
.end method

.method private r()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1864337
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->size:LX/0Px;

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/greetingcards/verve/model/VMView;->c:LX/0Px;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->size:LX/0Px;

    goto :goto_0
.end method

.method private s()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1864338
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->position:LX/0Px;

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/greetingcards/verve/model/VMView;->d:LX/0Px;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->position:LX/0Px;

    goto :goto_0
.end method

.method private t()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1864339
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->anchor:LX/0Px;

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/greetingcards/verve/model/VMView;->e:LX/0Px;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->anchor:LX/0Px;

    goto :goto_0
.end method


# virtual methods
.method public final a()F
    .locals 1

    .prologue
    .line 1864340
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->rotation:Ljava/lang/Float;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->rotation:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_0
.end method

.method public final b()F
    .locals 1

    .prologue
    .line 1864248
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->opacity:Ljava/lang/Float;

    if-nez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->opacity:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_0
.end method

.method public final c()F
    .locals 2

    .prologue
    .line 1864249
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->fontSize:Ljava/lang/Float;

    if-nez v0, :cond_0

    const/high16 v0, 0x41900000    # 18.0f

    .line 1864250
    :goto_0
    iget v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->i:F

    mul-float/2addr v0, v1

    return v0

    .line 1864251
    :cond_0
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->fontSize:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_0
.end method

.method public final d()Lcom/facebook/greetingcards/verve/model/VMColor;
    .locals 1

    .prologue
    .line 1864238
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->fillColor:Lcom/facebook/greetingcards/verve/model/VMColor;

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/greetingcards/verve/model/VMView;->f:Lcom/facebook/greetingcards/verve/model/VMColor;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->fillColor:Lcom/facebook/greetingcards/verve/model/VMColor;

    goto :goto_0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 1864233
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->shadowRadius:Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/4 v0, 0x3

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->shadowRadius:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public final f()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1864234
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->shadowOffset:LX/0Px;

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/greetingcards/verve/model/VMView;->g:LX/0Px;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->shadowOffset:LX/0Px;

    goto :goto_0
.end method

.method public final g()F
    .locals 1

    .prologue
    .line 1864235
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->duration:Ljava/lang/Float;

    if-nez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->duration:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_0
.end method

.method public final h()F
    .locals 1

    .prologue
    .line 1864236
    invoke-direct {p0}, Lcom/facebook/greetingcards/verve/model/VMView;->q()LX/CFi;

    move-result-object v0

    iget v0, v0, LX/CFi;->c:F

    return v0
.end method

.method public final i()F
    .locals 1

    .prologue
    .line 1864237
    invoke-direct {p0}, Lcom/facebook/greetingcards/verve/model/VMView;->q()LX/CFi;

    move-result-object v0

    iget v0, v0, LX/CFi;->d:F

    return v0
.end method

.method public final j()F
    .locals 1

    .prologue
    .line 1864239
    invoke-direct {p0}, Lcom/facebook/greetingcards/verve/model/VMView;->q()LX/CFi;

    move-result-object v0

    iget v0, v0, LX/CFi;->a:F

    return v0
.end method

.method public final k()F
    .locals 1

    .prologue
    .line 1864240
    invoke-direct {p0}, Lcom/facebook/greetingcards/verve/model/VMView;->q()LX/CFi;

    move-result-object v0

    iget v0, v0, LX/CFi;->b:F

    return v0
.end method

.method public final l()F
    .locals 2

    .prologue
    .line 1864241
    invoke-direct {p0}, Lcom/facebook/greetingcards/verve/model/VMView;->t()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/greetingcards/verve/model/VMView;->h()F

    move-result v1

    mul-float/2addr v0, v1

    return v0
.end method

.method public final m()F
    .locals 2

    .prologue
    .line 1864242
    invoke-direct {p0}, Lcom/facebook/greetingcards/verve/model/VMView;->t()LX/0Px;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/greetingcards/verve/model/VMView;->i()F

    move-result v1

    mul-float/2addr v0, v1

    return v0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1864243
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->lineBreak:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "wrap"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->lineBreak:Ljava/lang/String;

    goto :goto_0
.end method

.method public final o()LX/CFo;
    .locals 1

    .prologue
    .line 1864244
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->k:LX/CFo;

    if-nez v0, :cond_0

    .line 1864245
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->mType:Ljava/lang/String;

    invoke-static {v0}, LX/CFo;->fromString(Ljava/lang/String;)LX/CFo;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->k:LX/CFo;

    .line 1864246
    :cond_0
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->k:LX/CFo;

    return-object v0
.end method
