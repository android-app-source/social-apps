.class public Lcom/facebook/greetingcards/verve/model/VMSlideValue;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/greetingcards/verve/model/VMSlideValueDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Landroid/graphics/PointF;

.field private b:LX/CFo;

.field public final duration:Ljava/lang/Float;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "duration"
    .end annotation
.end field

.field private final mType:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "type"
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation
.end field

.field public final src:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "src"
    .end annotation
.end field

.field public final subviews:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "subviews"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            ">;"
        }
    .end annotation
.end field

.field public final text:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text"
    .end annotation
.end field

.field public final title:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "title"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1864137
    const-class v0, Lcom/facebook/greetingcards/verve/model/VMSlideValueDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1864112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1864113
    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->name:Ljava/lang/String;

    .line 1864114
    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->mType:Ljava/lang/String;

    .line 1864115
    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->src:Ljava/lang/String;

    .line 1864116
    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->text:Ljava/lang/String;

    .line 1864117
    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->title:Ljava/lang/String;

    .line 1864118
    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->subviews:LX/0Px;

    .line 1864119
    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->duration:Ljava/lang/Float;

    .line 1864120
    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->a:Landroid/graphics/PointF;

    .line 1864121
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/Float;Landroid/graphics/PointF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            ">;",
            "Ljava/lang/Float;",
            "Landroid/graphics/PointF;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1864127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1864128
    iput-object p1, p0, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->name:Ljava/lang/String;

    .line 1864129
    iput-object p2, p0, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->mType:Ljava/lang/String;

    .line 1864130
    iput-object p3, p0, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->src:Ljava/lang/String;

    .line 1864131
    iput-object p4, p0, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->text:Ljava/lang/String;

    .line 1864132
    iput-object p5, p0, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->title:Ljava/lang/String;

    .line 1864133
    iput-object p6, p0, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->subviews:LX/0Px;

    .line 1864134
    iput-object p7, p0, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->duration:Ljava/lang/Float;

    .line 1864135
    iput-object p8, p0, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->a:Landroid/graphics/PointF;

    .line 1864136
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/greetingcards/verve/model/VMSlideValue;
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 1864138
    new-instance v0, Lcom/facebook/greetingcards/verve/model/VMSlideValue;

    sget-object v1, LX/CFo;->LABEL:LX/CFo;

    invoke-virtual {v1}, LX/CFo;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v1, p0

    move-object v4, p1

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    move-object v8, v3

    invoke-direct/range {v0 .. v8}, Lcom/facebook/greetingcards/verve/model/VMSlideValue;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/Float;Landroid/graphics/PointF;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/PointF;)Lcom/facebook/greetingcards/verve/model/VMSlideValue;
    .locals 9
    .param p2    # Landroid/graphics/PointF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 1864126
    new-instance v0, Lcom/facebook/greetingcards/verve/model/VMSlideValue;

    sget-object v1, LX/CFo;->MEDIA:LX/CFo;

    invoke-virtual {v1}, LX/CFo;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v1, p0

    move-object v3, p1

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    move-object v8, p2

    invoke-direct/range {v0 .. v8}, Lcom/facebook/greetingcards/verve/model/VMSlideValue;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/Float;Landroid/graphics/PointF;)V

    return-object v0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/greetingcards/verve/model/VMSlideValue;
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 1864125
    new-instance v0, Lcom/facebook/greetingcards/verve/model/VMSlideValue;

    sget-object v1, LX/CFo;->BUTTON:LX/CFo;

    invoke-virtual {v1}, LX/CFo;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v1, p0

    move-object v4, v3

    move-object v5, p1

    move-object v6, v3

    move-object v7, v3

    move-object v8, v3

    invoke-direct/range {v0 .. v8}, Lcom/facebook/greetingcards/verve/model/VMSlideValue;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/Float;Landroid/graphics/PointF;)V

    return-object v0
.end method


# virtual methods
.method public final a()LX/CFo;
    .locals 1

    .prologue
    .line 1864122
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->b:LX/CFo;

    if-nez v0, :cond_0

    .line 1864123
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->mType:Ljava/lang/String;

    invoke-static {v0}, LX/CFo;->fromString(Ljava/lang/String;)LX/CFo;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->b:LX/CFo;

    .line 1864124
    :cond_0
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->b:LX/CFo;

    return-object v0
.end method
