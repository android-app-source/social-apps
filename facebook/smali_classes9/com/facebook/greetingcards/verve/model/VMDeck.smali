.class public Lcom/facebook/greetingcards/verve/model/VMDeck;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/greetingcards/verve/model/VMDeckDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final a:Lcom/facebook/greetingcards/verve/model/VMColor;


# instance fields
.field public final bgColor:Lcom/facebook/greetingcards/verve/model/VMColor;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "bg-color"
    .end annotation
.end field

.field public final initialSlide:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial-slide"
    .end annotation
.end field

.field public final resources:LX/0P1;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "resources"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final size:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "size"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public final slides:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "slides"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMSlide;",
            ">;"
        }
    .end annotation
.end field

.field public final styles:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "styles"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            ">;"
        }
    .end annotation
.end field

.field public final theme:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "theme"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1863992
    const-class v0, Lcom/facebook/greetingcards/verve/model/VMDeckDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    .line 1863993
    new-instance v0, Lcom/facebook/greetingcards/verve/model/VMColor;

    const-string v1, "solid"

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-static {v2, v4, v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    const/4 v4, 0x0

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/facebook/greetingcards/verve/model/VMColor;-><init>(Ljava/lang/String;LX/0Px;LX/0Px;FLX/0Px;)V

    sput-object v0, Lcom/facebook/greetingcards/verve/model/VMDeck;->a:Lcom/facebook/greetingcards/verve/model/VMColor;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1863994
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1863995
    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMDeck;->size:LX/0Px;

    .line 1863996
    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMDeck;->slides:LX/0Px;

    .line 1863997
    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMDeck;->resources:LX/0P1;

    .line 1863998
    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMDeck;->initialSlide:Ljava/lang/String;

    .line 1863999
    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMDeck;->styles:LX/0Px;

    .line 1864000
    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMDeck;->theme:Ljava/lang/String;

    .line 1864001
    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMDeck;->bgColor:Lcom/facebook/greetingcards/verve/model/VMColor;

    .line 1864002
    return-void
.end method

.method public constructor <init>(LX/0Px;LX/0Px;LX/0P1;Ljava/lang/String;LX/0Px;Ljava/lang/String;Lcom/facebook/greetingcards/verve/model/VMColor;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMSlide;",
            ">;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/facebook/greetingcards/verve/model/VMColor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1864003
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1864004
    iput-object p1, p0, Lcom/facebook/greetingcards/verve/model/VMDeck;->size:LX/0Px;

    .line 1864005
    iput-object p2, p0, Lcom/facebook/greetingcards/verve/model/VMDeck;->slides:LX/0Px;

    .line 1864006
    iput-object p3, p0, Lcom/facebook/greetingcards/verve/model/VMDeck;->resources:LX/0P1;

    .line 1864007
    iput-object p4, p0, Lcom/facebook/greetingcards/verve/model/VMDeck;->initialSlide:Ljava/lang/String;

    .line 1864008
    iput-object p5, p0, Lcom/facebook/greetingcards/verve/model/VMDeck;->styles:LX/0Px;

    .line 1864009
    iput-object p6, p0, Lcom/facebook/greetingcards/verve/model/VMDeck;->theme:Ljava/lang/String;

    .line 1864010
    iput-object p7, p0, Lcom/facebook/greetingcards/verve/model/VMDeck;->bgColor:Lcom/facebook/greetingcards/verve/model/VMColor;

    .line 1864011
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/greetingcards/verve/model/VMColor;
    .locals 1

    .prologue
    .line 1864012
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMDeck;->bgColor:Lcom/facebook/greetingcards/verve/model/VMColor;

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/greetingcards/verve/model/VMDeck;->a:Lcom/facebook/greetingcards/verve/model/VMColor;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMDeck;->bgColor:Lcom/facebook/greetingcards/verve/model/VMColor;

    goto :goto_0
.end method
