.class public Lcom/facebook/greetingcards/verve/model/VMAction;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/greetingcards/verve/model/VMActionDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final arg:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "arg"
    .end annotation
.end field

.field public final type:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "type"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1863918
    const-class v0, Lcom/facebook/greetingcards/verve/model/VMActionDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1863919
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1863920
    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMAction;->type:Ljava/lang/String;

    .line 1863921
    iput-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMAction;->arg:Ljava/lang/String;

    .line 1863922
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1863923
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1863924
    iput-object p1, p0, Lcom/facebook/greetingcards/verve/model/VMAction;->type:Ljava/lang/String;

    .line 1863925
    iput-object p2, p0, Lcom/facebook/greetingcards/verve/model/VMAction;->arg:Ljava/lang/String;

    .line 1863926
    return-void
.end method
