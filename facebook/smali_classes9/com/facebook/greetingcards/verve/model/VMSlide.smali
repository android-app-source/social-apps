.class public Lcom/facebook/greetingcards/verve/model/VMSlide;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/greetingcards/verve/model/VMSlideDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMSlideValue;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final actionsMap:LX/0P1;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "actions"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/greetingcards/verve/model/VMAction;",
            ">;"
        }
    .end annotation
.end field

.field public final bgColor:Lcom/facebook/greetingcards/verve/model/VMColor;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "bg-color"
    .end annotation
.end field

.field public final className:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "class"
    .end annotation
.end field

.field public final master:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "master"
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation
.end field

.field public final size:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "size"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public final values:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "values"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMSlideValue;",
            ">;"
        }
    .end annotation
.end field

.field public final views:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "views"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1864058
    const-class v0, Lcom/facebook/greetingcards/verve/model/VMSlideDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1864059
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1864060
    sput-object v0, Lcom/facebook/greetingcards/verve/model/VMSlide;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1864061
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1864062
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMSlide;->name:Ljava/lang/String;

    .line 1864063
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMSlide;->size:LX/0Px;

    .line 1864064
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMSlide;->views:LX/0Px;

    .line 1864065
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMSlide;->actionsMap:LX/0P1;

    .line 1864066
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/greetingcards/verve/model/VMSlide;->master:Z

    .line 1864067
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMSlide;->className:Ljava/lang/String;

    .line 1864068
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMSlide;->values:LX/0Px;

    .line 1864069
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMSlide;->bgColor:Lcom/facebook/greetingcards/verve/model/VMColor;

    .line 1864070
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0Px;LX/0Px;LX/0P1;ZLjava/lang/String;LX/0Px;Lcom/facebook/greetingcards/verve/model/VMColor;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            ">;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/greetingcards/verve/model/VMAction;",
            ">;Z",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMSlideValue;",
            ">;",
            "Lcom/facebook/greetingcards/verve/model/VMColor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1864071
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1864072
    iput-object p1, p0, Lcom/facebook/greetingcards/verve/model/VMSlide;->name:Ljava/lang/String;

    .line 1864073
    iput-object p2, p0, Lcom/facebook/greetingcards/verve/model/VMSlide;->size:LX/0Px;

    .line 1864074
    iput-object p3, p0, Lcom/facebook/greetingcards/verve/model/VMSlide;->views:LX/0Px;

    .line 1864075
    iput-object p4, p0, Lcom/facebook/greetingcards/verve/model/VMSlide;->actionsMap:LX/0P1;

    .line 1864076
    iput-boolean p5, p0, Lcom/facebook/greetingcards/verve/model/VMSlide;->master:Z

    .line 1864077
    iput-object p6, p0, Lcom/facebook/greetingcards/verve/model/VMSlide;->className:Ljava/lang/String;

    .line 1864078
    iput-object p7, p0, Lcom/facebook/greetingcards/verve/model/VMSlide;->values:LX/0Px;

    .line 1864079
    iput-object p8, p0, Lcom/facebook/greetingcards/verve/model/VMSlide;->bgColor:Lcom/facebook/greetingcards/verve/model/VMColor;

    .line 1864080
    return-void
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMSlideValue;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1864081
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMSlide;->values:LX/0Px;

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/greetingcards/verve/model/VMSlide;->a:LX/0Px;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMSlide;->values:LX/0Px;

    goto :goto_0
.end method

.method public final b()F
    .locals 2

    .prologue
    .line 1864082
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMSlide;->size:LX/0Px;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    return v0
.end method

.method public final c()F
    .locals 2

    .prologue
    .line 1864083
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMSlide;->size:LX/0Px;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    return v0
.end method
