.class public Lcom/facebook/greetingcards/verve/model/VMColor;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/greetingcards/verve/model/VMColorDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final angle:F
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "angle"
    .end annotation
.end field

.field public final color:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "color"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public final colors:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "colors"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0Px",
            "<",
            "Ljava/lang/Float;",
            ">;>;"
        }
    .end annotation
.end field

.field public final locations:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "locations"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public final type:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "type"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1863949
    const-class v0, Lcom/facebook/greetingcards/verve/model/VMColorDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1863950
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1863951
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMColor;->type:Ljava/lang/String;

    .line 1863952
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMColor;->color:LX/0Px;

    .line 1863953
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMColor;->colors:LX/0Px;

    .line 1863954
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/greetingcards/verve/model/VMColor;->angle:F

    .line 1863955
    iput-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMColor;->locations:LX/0Px;

    .line 1863956
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0Px;LX/0Px;FLX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "LX/0Px",
            "<",
            "LX/0Px",
            "<",
            "Ljava/lang/Float;",
            ">;>;F",
            "LX/0Px",
            "<",
            "Ljava/lang/Float;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1863957
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1863958
    iput-object p1, p0, Lcom/facebook/greetingcards/verve/model/VMColor;->type:Ljava/lang/String;

    .line 1863959
    iput-object p2, p0, Lcom/facebook/greetingcards/verve/model/VMColor;->color:LX/0Px;

    .line 1863960
    iput-object p3, p0, Lcom/facebook/greetingcards/verve/model/VMColor;->colors:LX/0Px;

    .line 1863961
    iput p4, p0, Lcom/facebook/greetingcards/verve/model/VMColor;->angle:F

    .line 1863962
    iput-object p5, p0, Lcom/facebook/greetingcards/verve/model/VMColor;->locations:LX/0Px;

    .line 1863963
    return-void
.end method
