.class public Lcom/facebook/greetingcards/vervecontrols/InlineVerveVideoView;
.super LX/2oW;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/CEe;


# static fields
.field private static final m:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public n:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1861672
    const-class v0, Lcom/facebook/greetingcards/vervecontrols/InlineVerveVideoView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/greetingcards/vervecontrols/InlineVerveVideoView;->m:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1861666
    invoke-direct {p0, p1, p2, p3}, LX/2oW;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1861667
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setShouldCropToFit(Z)V

    .line 1861668
    sget-object v0, LX/04D;->VERVE:LX/04D;

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerOrigin(LX/04D;)V

    .line 1861669
    sget-object v0, LX/04G;->INLINE_PLAYER:LX/04G;

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerType(LX/04G;)V

    .line 1861670
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "LX/0Px",
            "<+",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1861671
    new-instance v0, Lcom/facebook/video/player/plugins/CoverImagePlugin;

    sget-object v1, Lcom/facebook/greetingcards/vervecontrols/InlineVerveVideoView;->m:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v0, p1, v1}, Lcom/facebook/video/player/plugins/CoverImagePlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    new-instance v1, LX/2pM;

    invoke-direct {v1, p1}, LX/2pM;-><init>(Landroid/content/Context;)V

    new-instance v2, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-direct {v2, p1}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;-><init>(Landroid/content/Context;)V

    new-instance v3, LX/7NY;

    invoke-direct {v3, p1}, LX/7NY;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1, v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final jj_()V
    .locals 1

    .prologue
    .line 1861664
    sget-object v0, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 1861665
    return-void
.end method

.method public setVideoListener(LX/CGK;)V
    .locals 1

    .prologue
    .line 1861661
    new-instance v0, LX/CGT;

    invoke-direct {v0, p0, p1}, LX/CGT;-><init>(Lcom/facebook/greetingcards/vervecontrols/InlineVerveVideoView;LX/CGK;)V

    .line 1861662
    iput-object v0, p0, Lcom/facebook/video/player/RichVideoPlayer;->F:LX/3It;

    .line 1861663
    return-void
.end method
