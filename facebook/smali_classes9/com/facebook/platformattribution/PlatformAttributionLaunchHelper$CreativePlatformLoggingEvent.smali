.class public final Lcom/facebook/platformattribution/PlatformAttributionLaunchHelper$CreativePlatformLoggingEvent;
.super Lcom/facebook/analytics/logger/HoneyClientEvent;
.source ""


# instance fields
.field public final synthetic c:LX/BM1;


# direct methods
.method public constructor <init>(LX/BM1;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1776700
    iput-object p1, p0, Lcom/facebook/platformattribution/PlatformAttributionLaunchHelper$CreativePlatformLoggingEvent;->c:LX/BM1;

    .line 1776701
    invoke-direct {p0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1776702
    const-string v0, "creative_platform"

    .line 1776703
    iput-object v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1776704
    const-string v0, "SOURCE"

    invoke-virtual {p0, v0, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1776705
    const-string v1, "IS_APP_INSTALLED"

    invoke-virtual {p1, p5}, LX/BM1;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "YES"

    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1776706
    const-string v0, "APP_ID"

    invoke-virtual {p0, v0, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1776707
    const-string v0, "APP_PACKAGE_NAME"

    invoke-virtual {p0, v0, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1776708
    const-string v0, "COMPOSER_SESSION_ID"

    invoke-virtual {p0, v0, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1776709
    return-void

    .line 1776710
    :cond_0
    const-string v0, "NO"

    goto :goto_0
.end method
