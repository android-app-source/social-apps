.class public Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final p:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public m:LX/BM1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/4bD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public r:Landroid/widget/ProgressBar;

.field private s:Landroid/widget/Button;

.field private t:Landroid/widget/ImageButton;

.field public u:LX/1j2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1j2",
            "<",
            "Ljava/net/URI;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1776821
    const-class v0, Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;

    const-class v1, Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;->p:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1776820
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;

    invoke-static {p0}, LX/BM1;->a(LX/0QB;)LX/BM1;

    move-result-object v1

    check-cast v1, LX/BM1;

    invoke-static {p0}, LX/4bD;->a(LX/0QB;)LX/4bD;

    move-result-object v2

    check-cast v2, LX/4bD;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object p0

    check-cast p0, Ljava/util/concurrent/ExecutorService;

    iput-object v1, p1, Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;->m:LX/BM1;

    iput-object v2, p1, Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;->n:LX/4bD;

    iput-object p0, p1, Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;->o:Ljava/util/concurrent/ExecutorService;

    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/16 v0, 0x2a

    const v1, 0x6d0bfad6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1776788
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1776789
    const-class v1, Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;

    invoke-static {v1, p0}, Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1776790
    const/4 v1, 0x0

    invoke-virtual {p0, v2, v1}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 1776791
    iput-boolean v2, p0, Landroid/support/v4/app/DialogFragment;->d:Z

    .line 1776792
    invoke-virtual {p0, v2}, Landroid/support/v4/app/DialogFragment;->d_(Z)V

    .line 1776793
    iget-object v1, p0, Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;->n:LX/4bD;

    const-string v2, "https://www.facebook.com/friendsharing/instagram_layout/nux/"

    invoke-static {v2}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v2

    .line 1776794
    iget-object v4, v1, LX/4bD;->a:Lcom/facebook/http/common/FbHttpRequestProcessor;

    .line 1776795
    invoke-static {}, LX/15D;->newBuilder()LX/15E;

    move-result-object v5

    new-instance p1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {p1, v2}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    .line 1776796
    iput-object p1, v5, LX/15E;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 1776797
    move-object v5, v5

    .line 1776798
    sget-object p1, LX/4bD;->b:Lorg/apache/http/client/RedirectHandler;

    .line 1776799
    iput-object p1, v5, LX/15E;->h:Lorg/apache/http/client/RedirectHandler;

    .line 1776800
    move-object v5, v5

    .line 1776801
    sget-object p1, LX/4bD;->c:Lorg/apache/http/client/ResponseHandler;

    .line 1776802
    iput-object p1, v5, LX/15E;->g:Lorg/apache/http/client/ResponseHandler;

    .line 1776803
    move-object v5, v5

    .line 1776804
    const-string p1, "HttpRedirectFetcher"

    .line 1776805
    iput-object p1, v5, LX/15E;->c:Ljava/lang/String;

    .line 1776806
    move-object v5, v5

    .line 1776807
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-static {p1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object p1

    .line 1776808
    iput-object p1, v5, LX/15E;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 1776809
    move-object v5, v5

    .line 1776810
    sget-object p1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    .line 1776811
    iput-object p1, v5, LX/15E;->k:Lcom/facebook/http/interfaces/RequestPriority;

    .line 1776812
    move-object v5, v5

    .line 1776813
    invoke-virtual {v5}, LX/15E;->a()LX/15D;

    move-result-object v5

    move-object v5, v5

    .line 1776814
    invoke-virtual {v4, v5}, Lcom/facebook/http/common/FbHttpRequestProcessor;->b(LX/15D;)LX/1j2;

    move-result-object v4

    move-object v1, v4

    .line 1776815
    iput-object v1, p0, Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;->u:LX/1j2;

    .line 1776816
    iget-object v1, p0, Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;->u:LX/1j2;

    .line 1776817
    iget-object v2, v1, LX/1j2;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v1, v2

    .line 1776818
    new-instance v2, LX/BM4;

    invoke-direct {v2, p0}, LX/BM4;-><init>(Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;)V

    iget-object v4, p0, Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;->o:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v2, v4}, LX/1v3;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1776819
    const/16 v1, 0x2b

    const v2, -0x5f1469ba    # -3.990999E-19f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/16 v0, 0x2a

    const v1, 0x2bea6da9

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1776779
    const v0, 0x7f0309ba

    invoke-virtual {p1, v0, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 1776780
    const v0, 0x7f0d18c0

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1776781
    const v0, 0x7f0d18c1

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;->r:Landroid/widget/ProgressBar;

    .line 1776782
    const v0, 0x7f0d18c2

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;->s:Landroid/widget/Button;

    .line 1776783
    iget-object v0, p0, Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;->s:Landroid/widget/Button;

    new-instance v3, LX/BM2;

    invoke-direct {v3, p0}, LX/BM2;-><init>(Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1776784
    const v0, 0x7f0d05f0

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;->t:Landroid/widget/ImageButton;

    .line 1776785
    iget-object v0, p0, Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;->t:Landroid/widget/ImageButton;

    new-instance v3, LX/BM3;

    invoke-direct {v3, p0}, LX/BM3;-><init>(Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1776786
    iget-object v0, p0, Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;->m:LX/BM1;

    const-string v3, "sprout_nux"

    const-string v4, "881555691867714"

    const-string v5, "com.instagram.layout"

    invoke-virtual {v0, v3, v4, v5}, LX/BM1;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1776787
    const/16 v0, 0x2b

    const v3, -0x5ee1e79e

    invoke-static {v6, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x97efbdc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1776775
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDestroyView()V

    .line 1776776
    iget-object v1, p0, Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;->u:LX/1j2;

    if-eqz v1, :cond_0

    .line 1776777
    iget-object v1, p0, Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;->u:LX/1j2;

    invoke-virtual {v1}, LX/1j2;->b()V

    .line 1776778
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x72949de2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
