.class public Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;
.super Landroid/widget/FrameLayout;
.source ""


# instance fields
.field public a:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1766047
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1766048
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1766051
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1766052
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1766049
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1766050
    return-void
.end method

.method private a(Landroid/view/View;II)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, -0x1

    .line 1766042
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1766043
    iget v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-ne v2, v3, :cond_0

    :goto_0
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-ne v0, v3, :cond_1

    :goto_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;->measureChild(Landroid/view/View;II)V

    .line 1766044
    return-void

    :cond_0
    move p2, v1

    .line 1766045
    goto :goto_0

    :cond_1
    move p3, v1

    goto :goto_1
.end method

.method private a()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1766046
    invoke-virtual {p0}, Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "accelerometer_rotation"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private static b(Landroid/view/View;II)V
    .locals 2

    .prologue
    const/high16 v1, 0x40000000    # 2.0f

    .line 1766040
    invoke-static {p1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {p2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/view/View;->measure(II)V

    .line 1766041
    return-void
.end method


# virtual methods
.method public final onLayout(ZIIII)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1766028
    invoke-direct {p0}, Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 1766029
    :cond_0
    const v0, 0x7f0d0ba4

    invoke-virtual {p0, v0}, Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1766030
    const v0, 0x7f0d0ba3

    invoke-virtual {p0, v0}, Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1766031
    iget v2, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;->a:F

    sget-object v3, LX/89U;->SQUARE:LX/89U;

    invoke-virtual {v3}, LX/89U;->getValue()F

    move-result v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_1

    .line 1766032
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1766033
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 1766034
    :cond_1
    :goto_0
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 1766035
    return-void

    .line 1766036
    :cond_2
    const v0, 0x7f0d0ba4

    invoke-virtual {p0, v0}, Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1766037
    const v1, 0x7f0d0753

    invoke-virtual {p0, v1}, Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1766038
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {v0, v4, v4, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 1766039
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v0, v3

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {v1, v2, v4, v0, v3}, Landroid/view/View;->layout(IIII)V

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 9

    .prologue
    .line 1765997
    iget v0, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;->a:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1765998
    invoke-direct {p0}, Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 1765999
    :cond_0
    const v0, 0x7f0d0ba4

    invoke-virtual {p0, v0}, Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1766000
    const v1, 0x7f0d0ba3

    invoke-virtual {p0, v1}, Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1766001
    const v1, 0x7f0d0753

    invoke-virtual {p0, v1}, Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 1766002
    invoke-direct {p0, v0, p1, p2}, Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;->a(Landroid/view/View;II)V

    .line 1766003
    invoke-direct {p0, v3, p1, p2}, Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;->a(Landroid/view/View;II)V

    .line 1766004
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 1766005
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    .line 1766006
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    .line 1766007
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    .line 1766008
    int-to-float v0, v5

    iget v7, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;->a:F

    div-float/2addr v0, v7

    float-to-int v7, v0

    .line 1766009
    iget v0, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;->a:F

    sget-object v8, LX/89U;->SQUARE:LX/89U;

    invoke-virtual {v8}, LX/89U;->getValue()F

    move-result v8

    cmpl-float v0, v0, v8

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v6

    add-int/2addr v0, v7

    .line 1766010
    if-gt v0, v4, :cond_2

    .line 1766011
    sub-int v0, v4, v0

    .line 1766012
    invoke-static {v2, v5, v7}, Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;->b(Landroid/view/View;II)V

    .line 1766013
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v0, v2

    invoke-static {v3, v1, v0}, Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;->b(Landroid/view/View;II)V

    .line 1766014
    :goto_1
    invoke-virtual {p0, p1, p2}, Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;->setMeasuredDimension(II)V

    .line 1766015
    return-void

    .line 1766016
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1766017
    :cond_2
    sub-int v0, v4, v1

    sub-int/2addr v0, v6

    .line 1766018
    int-to-float v1, v0

    iget v3, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;->a:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    .line 1766019
    invoke-static {v2, v1, v0}, Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;->b(Landroid/view/View;II)V

    goto :goto_1

    .line 1766020
    :cond_3
    const v0, 0x7f0d0ba4

    invoke-virtual {p0, v0}, Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1766021
    const v1, 0x7f0d0753

    invoke-virtual {p0, v1}, Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1766022
    invoke-direct {p0, v0, p1, p2}, Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;->a(Landroid/view/View;II)V

    .line 1766023
    invoke-direct {p0, v1, p1, p2}, Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;->a(Landroid/view/View;II)V

    .line 1766024
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 1766025
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    .line 1766026
    sub-int/2addr v2, v3

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    invoke-static {v0, v2, v4}, Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;->b(Landroid/view/View;II)V

    .line 1766027
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-static {v1, v3, v0}, Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;->b(Landroid/view/View;II)V

    goto :goto_1
.end method

.method public setAspectRatio(F)V
    .locals 0

    .prologue
    .line 1765995
    iput p1, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamLayout;->a:F

    .line 1765996
    return-void
.end method
