.class public Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:LX/BG9;

.field public final c:LX/Jve;

.field public final d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final e:Landroid/content/Context;

.field public final f:LX/1Ad;

.field public final g:Z

.field public final h:LX/1Ai;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1766773
    const-class v0, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/Jve;Lcom/facebook/drawee/fbpipeline/FbDraweeView;ZLandroid/content/Context;LX/1Ad;)V
    .locals 2
    .param p1    # LX/Jve;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/drawee/fbpipeline/FbDraweeView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1766764
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1766765
    new-instance v0, LX/BG8;

    invoke-direct {v0, p0}, LX/BG8;-><init>(Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;)V

    iput-object v0, p0, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;->h:LX/1Ai;

    .line 1766766
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Jve;

    iput-object v0, p0, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;->c:LX/Jve;

    .line 1766767
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1766768
    iput-object p4, p0, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;->e:Landroid/content/Context;

    .line 1766769
    iput-object p5, p0, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;->f:LX/1Ad;

    .line 1766770
    iput-boolean p3, p0, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;->g:Z

    .line 1766771
    new-instance v0, LX/BG9;

    invoke-direct {v0}, LX/BG9;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;->b:LX/BG9;

    .line 1766772
    return-void
.end method
