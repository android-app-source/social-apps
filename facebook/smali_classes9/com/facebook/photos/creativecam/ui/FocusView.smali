.class public Lcom/facebook/photos/creativecam/ui/FocusView;
.super Landroid/view/View;
.source ""


# instance fields
.field private final a:Landroid/graphics/PointF;

.field private final b:Ljava/lang/Runnable;

.field public c:Landroid/graphics/Paint;

.field private d:Landroid/graphics/Paint;

.field public e:F

.field private f:F

.field public g:Z

.field public h:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1766391
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1766392
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/creativecam/ui/FocusView;->a:Landroid/graphics/PointF;

    .line 1766393
    new-instance v0, Lcom/facebook/photos/creativecam/ui/FocusView$1;

    invoke-direct {v0, p0}, Lcom/facebook/photos/creativecam/ui/FocusView$1;-><init>(Lcom/facebook/photos/creativecam/ui/FocusView;)V

    iput-object v0, p0, Lcom/facebook/photos/creativecam/ui/FocusView;->b:Ljava/lang/Runnable;

    .line 1766394
    invoke-direct {p0}, Lcom/facebook/photos/creativecam/ui/FocusView;->d()V

    .line 1766395
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1766386
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1766387
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/creativecam/ui/FocusView;->a:Landroid/graphics/PointF;

    .line 1766388
    new-instance v0, Lcom/facebook/photos/creativecam/ui/FocusView$1;

    invoke-direct {v0, p0}, Lcom/facebook/photos/creativecam/ui/FocusView$1;-><init>(Lcom/facebook/photos/creativecam/ui/FocusView;)V

    iput-object v0, p0, Lcom/facebook/photos/creativecam/ui/FocusView;->b:Ljava/lang/Runnable;

    .line 1766389
    invoke-direct {p0}, Lcom/facebook/photos/creativecam/ui/FocusView;->d()V

    .line 1766390
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1766381
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1766382
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/creativecam/ui/FocusView;->a:Landroid/graphics/PointF;

    .line 1766383
    new-instance v0, Lcom/facebook/photos/creativecam/ui/FocusView$1;

    invoke-direct {v0, p0}, Lcom/facebook/photos/creativecam/ui/FocusView$1;-><init>(Lcom/facebook/photos/creativecam/ui/FocusView;)V

    iput-object v0, p0, Lcom/facebook/photos/creativecam/ui/FocusView;->b:Ljava/lang/Runnable;

    .line 1766384
    invoke-direct {p0}, Lcom/facebook/photos/creativecam/ui/FocusView;->d()V

    .line 1766385
    return-void
.end method

.method private d()V
    .locals 5

    .prologue
    const/high16 v4, 0x41700000    # 15.0f

    const/4 v3, 0x1

    .line 1766370
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/photos/creativecam/ui/FocusView;->setWillNotDraw(Z)V

    .line 1766371
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/photos/creativecam/ui/FocusView;->c:Landroid/graphics/Paint;

    .line 1766372
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/FocusView;->c:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1766373
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/FocusView;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1766374
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/FocusView;->c:Landroid/graphics/Paint;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {p0}, Lcom/facebook/photos/creativecam/ui/FocusView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v3, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1766375
    new-instance v0, Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/facebook/photos/creativecam/ui/FocusView;->c:Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v0, p0, Lcom/facebook/photos/creativecam/ui/FocusView;->d:Landroid/graphics/Paint;

    .line 1766376
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/FocusView;->d:Landroid/graphics/Paint;

    const v1, -0x333334

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1766377
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/FocusView;->d:Landroid/graphics/Paint;

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1766378
    invoke-virtual {p0}, Lcom/facebook/photos/creativecam/ui/FocusView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-static {v3, v4, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativecam/ui/FocusView;->e:F

    .line 1766379
    invoke-virtual {p0}, Lcom/facebook/photos/creativecam/ui/FocusView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-static {v3, v4, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativecam/ui/FocusView;->f:F

    .line 1766380
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1766367
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/photos/creativecam/ui/FocusView;->g:Z

    .line 1766368
    invoke-virtual {p0}, Lcom/facebook/photos/creativecam/ui/FocusView;->invalidate()V

    .line 1766369
    return-void
.end method

.method public final a(II)V
    .locals 4

    .prologue
    .line 1766356
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/FocusView;->a:Landroid/graphics/PointF;

    int-to-float v1, p1

    int-to-float v2, p2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 1766357
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/creativecam/ui/FocusView;->g:Z

    .line 1766358
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 1766359
    new-instance v1, LX/BFs;

    invoke-direct {v1, p0}, LX/BFs;-><init>(Lcom/facebook/photos/creativecam/ui/FocusView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1766360
    const-wide/16 v2, 0x1c2

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1766361
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 1766362
    invoke-virtual {p0}, Lcom/facebook/photos/creativecam/ui/FocusView;->invalidate()V

    .line 1766363
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/FocusView;->b:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/facebook/photos/creativecam/ui/FocusView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1766364
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/FocusView;->b:Ljava/lang/Runnable;

    const-wide/16 v2, 0xbb8

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/photos/creativecam/ui/FocusView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1766365
    return-void

    .line 1766366
    :array_0
    .array-data 4
        0x40400000    # 3.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1766352
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/FocusView;->d:Landroid/graphics/Paint;

    const/16 v1, -0x100

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1766353
    invoke-virtual {p0}, Lcom/facebook/photos/creativecam/ui/FocusView;->invalidate()V

    .line 1766354
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/FocusView;->b:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/photos/creativecam/ui/FocusView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1766355
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 1766348
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/FocusView;->d:Landroid/graphics/Paint;

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1766349
    invoke-virtual {p0}, Lcom/facebook/photos/creativecam/ui/FocusView;->invalidate()V

    .line 1766350
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/FocusView;->b:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/photos/creativecam/ui/FocusView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1766351
    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 1766343
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1766344
    iget-boolean v0, p0, Lcom/facebook/photos/creativecam/ui/FocusView;->g:Z

    if-eqz v0, :cond_0

    .line 1766345
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/FocusView;->a:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lcom/facebook/photos/creativecam/ui/FocusView;->a:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget v2, p0, Lcom/facebook/photos/creativecam/ui/FocusView;->f:F

    iget v3, p0, Lcom/facebook/photos/creativecam/ui/FocusView;->h:F

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/facebook/photos/creativecam/ui/FocusView;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1766346
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/FocusView;->a:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lcom/facebook/photos/creativecam/ui/FocusView;->a:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget v2, p0, Lcom/facebook/photos/creativecam/ui/FocusView;->h:F

    iget-object v3, p0, Lcom/facebook/photos/creativecam/ui/FocusView;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1766347
    :cond_0
    return-void
.end method
