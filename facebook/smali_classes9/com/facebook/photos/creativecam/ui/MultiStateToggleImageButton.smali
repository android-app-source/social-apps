.class public Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;
.super Landroid/widget/ImageView;
.source ""


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1766396
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1766397
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;->a:I

    .line 1766398
    invoke-direct {p0}, Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;->a()V

    .line 1766399
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1766400
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1766401
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;->a:I

    .line 1766402
    invoke-direct {p0}, Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;->a()V

    .line 1766403
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1766404
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1766405
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;->a:I

    .line 1766406
    invoke-direct {p0}, Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;->a()V

    .line 1766407
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1766408
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;->setClickable(Z)V

    .line 1766409
    return-void
.end method

.method private setCurrentStateIndex(I)V
    .locals 2

    .prologue
    .line 1766410
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;->b:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1766411
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "States not initialised"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1766412
    :cond_0
    if-ltz p1, :cond_1

    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_2

    .line 1766413
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "index must be in range of states"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1766414
    :cond_2
    iput p1, p0, Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;->a:I

    .line 1766415
    invoke-virtual {p0}, Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1766416
    invoke-virtual {p0, v0}, Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1766417
    return-void
.end method


# virtual methods
.method public getCurrentStateId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1766418
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;->b:Ljava/util/List;

    iget v1, p0, Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;->a:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final performClick()Z
    .locals 2

    .prologue
    .line 1766419
    iget v0, p0, Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;->a:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    rem-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;->setCurrentStateIndex(I)V

    .line 1766420
    invoke-super {p0}, Landroid/widget/ImageView;->performClick()Z

    move-result v0

    return v0
.end method

.method public setCurrentState(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1766421
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1766422
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1766423
    invoke-direct {p0, v1}, Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;->setCurrentStateIndex(I)V

    .line 1766424
    :cond_0
    return-void

    .line 1766425
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public setStates(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1766426
    iput-object p1, p0, Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;->b:Ljava/util/List;

    .line 1766427
    invoke-virtual {p0}, Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;->invalidate()V

    .line 1766428
    return-void
.end method
