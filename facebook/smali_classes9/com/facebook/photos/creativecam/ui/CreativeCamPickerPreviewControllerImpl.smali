.class public Lcom/facebook/photos/creativecam/ui/CreativeCamPickerPreviewControllerImpl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/89c;


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final c:LX/1Ck;

.field public final d:LX/8I2;

.field public final e:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1766134
    const-class v0, Lcom/facebook/photos/creativecam/ui/CreativeCamPickerPreviewControllerImpl;

    sput-object v0, Lcom/facebook/photos/creativecam/ui/CreativeCamPickerPreviewControllerImpl;->a:Ljava/lang/Class;

    .line 1766135
    const-class v0, Lcom/facebook/photos/creativecam/ui/CreativeCamPickerPreviewControllerImpl;

    const-class v1, Lcom/facebook/photos/creativecam/ui/CreativeCamPickerPreviewControllerImpl;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/creativecam/ui/CreativeCamPickerPreviewControllerImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1Ck;LX/8I2;LX/0TD;)V
    .locals 0
    .param p3    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1766136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1766137
    iput-object p1, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamPickerPreviewControllerImpl;->c:LX/1Ck;

    .line 1766138
    iput-object p2, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamPickerPreviewControllerImpl;->d:LX/8I2;

    .line 1766139
    iput-object p3, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamPickerPreviewControllerImpl;->e:LX/0TD;

    .line 1766140
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;I)V
    .locals 4

    .prologue
    .line 1766141
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamPickerPreviewControllerImpl;->c:LX/1Ck;

    const-string v1, "picker_task_key"

    new-instance v2, LX/BFd;

    invoke-direct {v2, p0}, LX/BFd;-><init>(Lcom/facebook/photos/creativecam/ui/CreativeCamPickerPreviewControllerImpl;)V

    new-instance v3, LX/BFe;

    invoke-direct {v3, p0, p1}, LX/BFe;-><init>(Lcom/facebook/photos/creativecam/ui/CreativeCamPickerPreviewControllerImpl;Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1766142
    return-void
.end method

.method public final a(Ljava/lang/String;I)Z
    .locals 1

    .prologue
    .line 1766143
    const/4 v0, 0x0

    return v0
.end method
