.class public Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final b:Ljava/lang/String;

.field private static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final a:LX/0TF;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/Jvd;

.field public final e:Lcom/facebook/optic/CameraPreviewView;

.field private final f:Lcom/facebook/bitmaps/NativeImageProcessor;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1FZ;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1HI;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0So;

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Er;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Sh;

.field public final l:LX/0kL;

.field public final m:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
    .end annotation
.end field

.field public final n:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field

.field public final o:LX/03V;

.field private final p:LX/89Z;

.field public final q:Lcom/facebook/photos/creativecam/ui/FocusView;

.field public final r:LX/5fM;

.field public s:Z

.field public t:Z

.field private u:LX/9c7;

.field public v:Ljava/io/File;

.field public w:I

.field public x:Z

.field public y:Landroid/net/Uri;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1766670
    const-class v0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->b:Ljava/lang/String;

    .line 1766671
    const-class v0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/Jvd;Lcom/facebook/optic/CameraPreviewView;Lcom/facebook/photos/creativecam/ui/FocusView;LX/5fM;LX/9c7;LX/89Z;Lcom/facebook/bitmaps/NativeImageProcessor;LX/0Ot;LX/0Ot;LX/0Ot;LX/0So;LX/0Sh;LX/0kL;LX/0TD;Ljava/util/concurrent/ExecutorService;LX/03V;)V
    .locals 2
    .param p1    # LX/Jvd;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/optic/CameraPreviewView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/photos/creativecam/ui/FocusView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/5fM;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/9c7;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/89Z;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p14    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p15    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController$Delegate;",
            "Lcom/facebook/optic/CameraPreviewView;",
            "Lcom/facebook/photos/creativecam/ui/FocusView;",
            "LX/5fM;",
            "LX/9c7;",
            "LX/89Z;",
            "Lcom/facebook/bitmaps/NativeImageProcessor;",
            "LX/0Ot",
            "<",
            "LX/1FZ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1HI;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Er;",
            ">;",
            "LX/0So;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0kL;",
            "LX/0TD;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1766683
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1766684
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->x:Z

    .line 1766685
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->y:Landroid/net/Uri;

    .line 1766686
    new-instance v1, LX/BG5;

    invoke-direct {v1, p0}, LX/BG5;-><init>(Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;)V

    iput-object v1, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->a:LX/0TF;

    .line 1766687
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Jvd;

    iput-object v1, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->d:LX/Jvd;

    .line 1766688
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/optic/CameraPreviewView;

    iput-object v1, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->e:Lcom/facebook/optic/CameraPreviewView;

    .line 1766689
    iput-object p7, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->f:Lcom/facebook/bitmaps/NativeImageProcessor;

    .line 1766690
    iput-object p8, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->g:LX/0Ot;

    .line 1766691
    iput-object p9, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->h:LX/0Ot;

    .line 1766692
    iput-object p10, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->j:LX/0Ot;

    .line 1766693
    iput-object p11, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->i:LX/0So;

    .line 1766694
    iput-object p12, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->k:LX/0Sh;

    .line 1766695
    iput-object p13, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->l:LX/0kL;

    .line 1766696
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->m:LX/0TD;

    .line 1766697
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->n:Ljava/util/concurrent/ExecutorService;

    .line 1766698
    iput-object p5, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->u:LX/9c7;

    .line 1766699
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->o:LX/03V;

    .line 1766700
    iput-object p6, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->p:LX/89Z;

    .line 1766701
    iput-object p3, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->q:Lcom/facebook/photos/creativecam/ui/FocusView;

    .line 1766702
    iput-object p4, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->r:LX/5fM;

    .line 1766703
    return-void
.end method

.method private static a(LX/5fk;ILjava/io/File;Ljava/io/File;)V
    .locals 7

    .prologue
    .line 1766672
    invoke-virtual {p0, p1}, LX/5fk;->a(I)Landroid/graphics/Rect;

    move-result-object v0

    .line 1766673
    iget-object v1, p0, LX/5fk;->a:Landroid/graphics/Rect;

    .line 1766674
    new-instance v2, Landroid/graphics/RectF;

    iget v3, v0, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    iget v4, v0, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v4, v5

    iget v5, v0, Landroid/graphics/Rect;->right:I

    int-to-float v5, v5

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-direct {v2, v3, v4, v5, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1766675
    :try_start_0
    invoke-virtual {p2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v2, v1}, Lcom/facebook/bitmaps/NativeImageProcessor;->a(Ljava/lang/String;ILandroid/graphics/RectF;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/facebook/bitmaps/ImageResizer$ImageResizingInputFileException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1766676
    :goto_0
    return-void

    .line 1766677
    :catch_0
    move-exception v0

    .line 1766678
    invoke-virtual {p3}, Ljava/io/File;->delete()Z

    .line 1766679
    sget-object v1, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->b:Ljava/lang/String;

    const-string v2, "Image crop failed"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1766680
    :catch_1
    move-exception v0

    .line 1766681
    invoke-virtual {p3}, Ljava/io/File;->delete()Z

    .line 1766682
    sget-object v1, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->b:Ljava/lang/String;

    const-string v2, "IO Exception while writing to file"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static a(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 1766661
    if-eqz p0, :cond_0

    .line 1766662
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/net/Uri;->getEncodedPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1766663
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1766664
    :cond_0
    return-void
.end method

.method private static a(Ljava/io/Closeable;)V
    .locals 3

    .prologue
    .line 1766665
    if-nez p0, :cond_0

    .line 1766666
    :goto_0
    return-void

    .line 1766667
    :cond_0
    const/4 v0, 0x0

    :try_start_0
    invoke-static {p0, v0}, LX/1md;->a(Ljava/io/Closeable;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1766668
    :catch_0
    move-exception v0

    .line 1766669
    sget-object v1, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->b:Ljava/lang/String;

    const-string v2, "IO Exception, couldnt close stream"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;[BLX/5fk;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B",
            "LX/5fk;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1766628
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1766629
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->k:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 1766630
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Er;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "creativecam_intermediate_image_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->i:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".jpg"

    sget-object v3, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    invoke-virtual {v0, v1, v2, v3}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v3

    .line 1766631
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Er;

    const-string v1, "FB_IMG_"

    const-string v2, ".jpg"

    sget-object v4, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    invoke-virtual {v0, v1, v2, v4}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v4

    .line 1766632
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1766633
    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1766634
    const/4 v2, 0x0

    .line 1766635
    :try_start_0
    new-instance v1, Ljava/io/BufferedOutputStream;

    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v0}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1766636
    :try_start_1
    invoke-virtual {v1, p1}, Ljava/io/BufferedOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1766637
    invoke-static {v1}, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->a(Ljava/io/Closeable;)V

    .line 1766638
    :goto_0
    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/2Qx;->b(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->w:I

    .line 1766639
    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 1766640
    iget-object v1, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->e:Lcom/facebook/optic/CameraPreviewView;

    new-instance v2, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController$5;

    invoke-direct {v2, p0, v0, p2}, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController$5;-><init>(Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;Landroid/net/Uri;LX/5fk;)V

    invoke-virtual {v1, v2}, Lcom/facebook/optic/CameraPreviewView;->post(Ljava/lang/Runnable;)Z

    .line 1766641
    sget-object v0, LX/5fM;->FRONT:LX/5fM;

    iget-object v1, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->e:Lcom/facebook/optic/CameraPreviewView;

    invoke-virtual {v1}, Lcom/facebook/optic/CameraPreviewView;->getCameraFacing()LX/5fM;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/5fM;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1766642
    iget v0, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->w:I

    invoke-static {p2, v0, v3, v4}, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->a(LX/5fk;ILjava/io/File;Ljava/io/File;)V

    .line 1766643
    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1766644
    :goto_1
    return-object v0

    .line 1766645
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 1766646
    :goto_2
    :try_start_2
    sget-object v2, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->b:Ljava/lang/String;

    const-string v5, "File not found while storing"

    invoke-static {v2, v5, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1766647
    invoke-static {v1}, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->a(Ljava/io/Closeable;)V

    goto :goto_0

    .line 1766648
    :catch_1
    move-exception v0

    .line 1766649
    :goto_3
    :try_start_3
    sget-object v1, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->b:Ljava/lang/String;

    const-string v5, "IO Exception while writing to file"

    invoke-static {v1, v5, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1766650
    invoke-static {v2}, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :goto_4
    invoke-static {v2}, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->a(Ljava/io/Closeable;)V

    throw v0

    .line 1766651
    :cond_0
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v1

    .line 1766652
    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v2

    .line 1766653
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1HI;

    sget-object v3, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2, v3}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    .line 1766654
    new-instance v2, LX/BG2;

    invoke-direct {v2, p0, p2, v4, v1}, LX/BG2;-><init>(Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;LX/5fk;Ljava/io/File;Lcom/google/common/util/concurrent/SettableFuture;)V

    .line 1766655
    sget-object v3, LX/131;->INSTANCE:LX/131;

    move-object v3, v3

    .line 1766656
    invoke-interface {v0, v2, v3}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    move-object v0, v1

    .line 1766657
    goto :goto_1

    .line 1766658
    :catchall_1
    move-exception v0

    move-object v2, v1

    goto :goto_4

    .line 1766659
    :catch_2
    move-exception v0

    move-object v2, v1

    goto :goto_3

    .line 1766660
    :catch_3
    move-exception v0

    goto :goto_2
.end method

.method public static a$redex0(Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;LX/5fk;Landroid/graphics/Bitmap;ILjava/io/File;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5fk;",
            "Landroid/graphics/Bitmap;",
            "I",
            "Ljava/io/File;",
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1766593
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Er;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "creativecam_working_image_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->i:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, ".jpg"

    sget-object v4, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    invoke-virtual {v0, v2, v3, v4}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v2

    .line 1766594
    :try_start_0
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    .line 1766595
    const/high16 v0, -0x40800000    # -1.0f

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v0, v4}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 1766596
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1766597
    if-lez p3, :cond_0

    .line 1766598
    mul-int/lit8 v0, p3, -0x1

    int-to-float v0, v0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    invoke-virtual {v3, v0, v4, v5}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 1766599
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FZ;

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-virtual {v0, v4, v5}, LX/1FZ;->a(II)LX/1FJ;

    move-result-object v1

    .line 1766600
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4}, Landroid/graphics/Canvas;-><init>()V

    .line 1766601
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 1766602
    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 1766603
    invoke-virtual {v1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v4, v0}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 1766604
    invoke-virtual {v4, p2, v3, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 1766605
    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 1766606
    invoke-virtual {v1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    invoke-static {v0, v3, v4, v2}, LX/2Qx;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;ILjava/io/File;)V

    .line 1766607
    invoke-static {p1, p3, v2, p4}, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->a(LX/5fk;ILjava/io/File;Ljava/io/File;)V

    .line 1766608
    invoke-static {p4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    const v3, 0xccffba

    invoke-static {p5, v0, v3}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z
    :try_end_0
    .catch LX/42w; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/1FM; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1766609
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1766610
    if-eqz v1, :cond_1

    .line 1766611
    invoke-virtual {v1}, LX/1FJ;->close()V

    .line 1766612
    :cond_1
    :goto_0
    return-void

    .line 1766613
    :catch_0
    move-exception v0

    .line 1766614
    :try_start_1
    sget-object v3, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->b:Ljava/lang/String;

    const-string v4, "Bitmap exception while storing"

    invoke-static {v3, v4, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1766615
    invoke-virtual {p5, v0}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1766616
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1766617
    if-eqz v1, :cond_1

    .line 1766618
    invoke-virtual {v1}, LX/1FJ;->close()V

    goto :goto_0

    .line 1766619
    :catch_1
    move-exception v0

    .line 1766620
    :try_start_2
    sget-object v3, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->b:Ljava/lang/String;

    const-string v4, "Bitmap pool is full, preventing allocation for the mirrored bitmap"

    invoke-static {v3, v4, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1766621
    invoke-virtual {p5, v0}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1766622
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1766623
    if-eqz v1, :cond_1

    .line 1766624
    invoke-virtual {v1}, LX/1FJ;->close()V

    goto :goto_0

    .line 1766625
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1766626
    if-eqz v1, :cond_2

    .line 1766627
    invoke-virtual {v1}, LX/1FJ;->close()V

    :cond_2
    throw v0
.end method

.method public static m(Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;)V
    .locals 8

    .prologue
    .line 1766585
    iget-boolean v0, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->x:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->y:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 1766586
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->d:LX/Jvd;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/Jvd;->a(Z)V

    .line 1766587
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->d:LX/Jvd;

    iget-object v1, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->y:Landroid/net/Uri;

    .line 1766588
    iget-object v2, v0, LX/Jvd;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v3, v2, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->O:LX/89S;

    new-instance v4, LX/89R;

    iget-object v2, v0, LX/Jvd;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v2, v2, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->w:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    invoke-virtual {v2}, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->e()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, LX/89W;->CREATIVE_CAM_FRONT:LX/89W;

    :goto_0
    iget-object v5, v0, LX/Jvd;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v5, v5, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->q:LX/0Px;

    iget-object v6, v0, LX/Jvd;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v6, v6, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->A:LX/89e;

    invoke-interface {v6}, LX/89e;->a()Ljava/lang/String;

    move-result-object v6

    iget-object v7, v0, LX/Jvd;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v7, v7, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->A:LX/89e;

    invoke-interface {v7}, LX/89e;->b()LX/0Px;

    move-result-object v7

    invoke-direct {v4, v2, v5, v6, v7}, LX/89R;-><init>(LX/89W;LX/0Px;Ljava/lang/String;LX/0Px;)V

    invoke-interface {v3, v1, v4}, LX/89S;->b(Landroid/net/Uri;LX/89R;)V

    .line 1766589
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->x:Z

    .line 1766590
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->y:Landroid/net/Uri;

    .line 1766591
    :cond_0
    return-void

    .line 1766592
    :cond_1
    sget-object v2, LX/89W;->CREATIVE_CAM_BACK:LX/89W;

    goto :goto_0
.end method


# virtual methods
.method public final e()Z
    .locals 2

    .prologue
    .line 1766584
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->e:Lcom/facebook/optic/CameraPreviewView;

    invoke-virtual {v0}, Lcom/facebook/optic/CameraPreviewView;->getCameraFacing()LX/5fM;

    move-result-object v0

    sget-object v1, LX/5fM;->FRONT:LX/5fM;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()V
    .locals 3

    .prologue
    .line 1766581
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->x:Z

    .line 1766582
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->e:Lcom/facebook/optic/CameraPreviewView;

    new-instance v1, LX/BG6;

    invoke-direct {v1, p0}, LX/BG6;-><init>(Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;)V

    new-instance v2, LX/BFt;

    invoke-direct {v2, p0}, LX/BFt;-><init>(Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/optic/CameraPreviewView;->a(LX/5f5;LX/5f5;)V

    .line 1766583
    return-void
.end method
