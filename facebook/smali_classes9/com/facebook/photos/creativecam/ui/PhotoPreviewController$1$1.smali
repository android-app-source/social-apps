.class public final Lcom/facebook/photos/creativecam/ui/PhotoPreviewController$1$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/BG8;


# direct methods
.method public constructor <init>(LX/BG8;)V
    .locals 0

    .prologue
    .line 1766708
    iput-object p1, p0, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController$1$1;->a:LX/BG8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 1766709
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 1766710
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController$1$1;->a:LX/BG8;

    iget-object v0, v0, LX/BG8;->a:Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {v0, v1}, LX/1af;->a(Landroid/graphics/RectF;)V

    .line 1766711
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController$1$1;->a:LX/BG8;

    iget-object v0, v0, LX/BG8;->a:Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;->c:LX/Jve;

    iget-object v1, p0, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController$1$1;->a:LX/BG8;

    iget-object v1, v1, LX/BG8;->a:Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;

    iget-object v1, v1, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController$1$1;->a:LX/BG8;

    iget-object v2, v2, LX/BG8;->a:Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;

    iget-object v2, v2, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getMeasuredWidth()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController$1$1;->a:LX/BG8;

    iget-object v3, v3, LX/BG8;->a:Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;

    iget-object v3, v3, Lcom/facebook/photos/creativecam/ui/PhotoPreviewController;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1766712
    iget p0, v3, Lcom/facebook/drawee/view/DraweeView;->b:F

    move v3, p0

    .line 1766713
    div-float/2addr v2, v3

    float-to-int v2, v2

    const/4 v4, 0x0

    .line 1766714
    iget-object v3, v0, LX/Jve;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 1766715
    iget-object v5, v0, LX/Jve;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v5, v5, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->n:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v5

    sub-int v5, v3, v5

    iget-object v3, v0, LX/Jve;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v3, v3, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->F:LX/0am;

    invoke-virtual {v3}, LX/0am;->isPresent()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v0, LX/Jve;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v3, v3, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->F:LX/0am;

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;

    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->getMeasuredHeight()I

    move-result v3

    :goto_0
    sub-int v3, v5, v3

    .line 1766716
    if-le v2, v3, :cond_1

    .line 1766717
    iget-object v5, v0, LX/Jve;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v5, v5, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->A:LX/89e;

    iget-object p0, v0, LX/Jve;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object p0, p0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->o:Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

    .line 1766718
    iget-boolean v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->c:Z

    move p0, v0

    .line 1766719
    invoke-interface {v5, v1, v3, v4, p0}, LX/89e;->a(IIIZ)V

    .line 1766720
    :goto_1
    return-void

    :cond_0
    move v3, v4

    .line 1766721
    goto :goto_0

    .line 1766722
    :cond_1
    iget-object v3, v0, LX/Jve;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v3, v3, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->A:LX/89e;

    iget-object v5, v0, LX/Jve;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v5, v5, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->o:Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

    .line 1766723
    iget-boolean p0, v5, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->c:Z

    move v5, p0

    .line 1766724
    invoke-interface {v3, v1, v2, v4, v5}, LX/89e;->a(IIIZ)V

    goto :goto_1
.end method
