.class public Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:Landroid/view/View;

.field private b:I

.field private c:I

.field private d:Lcom/facebook/fbui/glyph/GlyphView;

.field private e:Lcom/facebook/fbui/glyph/GlyphView;

.field private final f:I

.field private final g:I

.field public h:LX/BFc;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1766114
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1766115
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1766112
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1766113
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1766103
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1766104
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->c:I

    .line 1766105
    invoke-virtual {p0}, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->f:I

    .line 1766106
    invoke-virtual {p0}, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00e5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->g:I

    .line 1766107
    const v0, 0x7f03022b

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1766108
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0bfb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 1766109
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0bfc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 1766110
    sub-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->b:I

    .line 1766111
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1766096
    iget v0, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->c:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->f:I

    .line 1766097
    :goto_0
    iget v1, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->c:I

    if-nez v1, :cond_1

    iget v1, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->g:I

    .line 1766098
    :goto_1
    iget-object v2, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->d:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v2, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1766099
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->e:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1766100
    return-void

    .line 1766101
    :cond_0
    iget v0, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->g:I

    goto :goto_0

    .line 1766102
    :cond_1
    iget v1, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->f:I

    goto :goto_1
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1766061
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    int-to-float v0, v0

    .line 1766062
    iget v1, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->c:I

    if-nez v1, :cond_0

    neg-float v0, v0

    .line 1766063
    :goto_0
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    invoke-direct {v1, v2, v2, v2, v0}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 1766064
    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 1766065
    new-instance v0, LX/BFb;

    invoke-direct {v0, p0}, LX/BFb;-><init>(Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;)V

    invoke-virtual {v1, v0}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1766066
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1766067
    return-void

    .line 1766068
    :cond_0
    iget v1, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->b:I

    int-to-float v1, v1

    sub-float v0, v1, v0

    goto :goto_0
.end method

.method public static e(Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;)V
    .locals 2

    .prologue
    .line 1766090
    iget v0, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->c:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    move v1, v0

    .line 1766091
    :goto_0
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1766092
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 1766093
    iget-object v1, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1766094
    return-void

    .line 1766095
    :cond_0
    iget v0, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->b:I

    move v1, v0

    goto :goto_0
.end method

.method public static f(Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;)V
    .locals 2

    .prologue
    .line 1766082
    iget v0, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->c:I

    if-nez v0, :cond_1

    .line 1766083
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->c:I

    .line 1766084
    :goto_0
    invoke-direct {p0}, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->a()V

    .line 1766085
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->h:LX/BFc;

    if-eqz v0, :cond_0

    .line 1766086
    iget-object v0, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->h:LX/BFc;

    iget v1, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->c:I

    invoke-interface {v0, v1}, LX/BFc;->a(I)V

    .line 1766087
    :cond_0
    invoke-direct {p0}, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->b()V

    .line 1766088
    return-void

    .line 1766089
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->c:I

    goto :goto_0
.end method


# virtual methods
.method public final onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x698d7683

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1766075
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onFinishInflate()V

    .line 1766076
    const v0, 0x7f0d087e

    invoke-virtual {p0, v0}, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->a:Landroid/view/View;

    .line 1766077
    const v0, 0x7f0d087f

    invoke-virtual {p0, v0}, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->d:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1766078
    const v0, 0x7f0d0880

    invoke-virtual {p0, v0}, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->e:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1766079
    invoke-direct {p0}, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->a()V

    .line 1766080
    new-instance v0, LX/BFa;

    invoke-direct {v0, p0}, LX/BFa;-><init>(Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;)V

    invoke-virtual {p0, v0}, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1766081
    const/16 v0, 0x2d

    const v2, 0x61a1c8ca

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setSelectedPosition(I)V
    .locals 0

    .prologue
    .line 1766071
    iput p1, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->c:I

    .line 1766072
    invoke-direct {p0}, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->a()V

    .line 1766073
    invoke-static {p0}, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->e(Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;)V

    .line 1766074
    return-void
.end method

.method public setToggleListener(LX/BFc;)V
    .locals 0

    .prologue
    .line 1766069
    iput-object p1, p0, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->h:LX/BFc;

    .line 1766070
    return-void
.end method
