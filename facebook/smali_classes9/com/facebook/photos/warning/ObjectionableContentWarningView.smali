.class public Lcom/facebook/photos/warning/ObjectionableContentWarningView;
.super Landroid/view/ViewGroup;
.source ""


# instance fields
.field private final a:Lcom/facebook/resources/ui/FbTextView;

.field private final b:Lcom/facebook/resources/ui/FbButton;

.field private final c:Landroid/widget/ImageView;

.field private d:LX/BJ5;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1771578
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/photos/warning/ObjectionableContentWarningView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1771579
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1771571
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/photos/warning/ObjectionableContentWarningView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1771572
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1771580
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1771581
    sget-object v0, LX/BJ5;->SMALL:LX/BJ5;

    iput-object v0, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->d:LX/BJ5;

    .line 1771582
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030c4d

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 1771583
    const v0, 0x7f0d1e0b

    invoke-virtual {p0, v0}, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 1771584
    const v0, 0x7f0d1e0c

    invoke-virtual {p0, v0}, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->b:Lcom/facebook/resources/ui/FbButton;

    .line 1771585
    const v0, 0x7f0d1e0a

    invoke-virtual {p0, v0}, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->c:Landroid/widget/ImageView;

    .line 1771586
    iget-object v0, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1771587
    return-void
.end method

.method private static a(Landroid/util/DisplayMetrics;I)I
    .locals 2

    .prologue
    .line 1771573
    const/4 v0, 0x1

    int-to-float v1, p1

    invoke-static {v0, v1, p0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method private a(I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1771574
    const/high16 v0, -0x80000000

    invoke-static {p1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1771575
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 1771576
    iget-object v2, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v0, v1}, Lcom/facebook/resources/ui/FbTextView;->measure(II)V

    .line 1771577
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1771543
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1771544
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 8

    .prologue
    const/16 v7, 0x10

    const/4 v2, 0x0

    .line 1771526
    invoke-virtual {p0}, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 1771527
    invoke-virtual {p0}, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->getMeasuredWidth()I

    move-result v4

    .line 1771528
    const/16 v0, 0x20

    invoke-static {v3, v0}, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->a(Landroid/util/DisplayMetrics;I)I

    move-result v1

    .line 1771529
    invoke-static {v3, v7}, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->a(Landroid/util/DisplayMetrics;I)I

    move-result v0

    .line 1771530
    iget-object v5, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->d:LX/BJ5;

    sget-object v6, LX/BJ5;->FULL:LX/BJ5;

    if-ne v5, v6, :cond_0

    .line 1771531
    iget-object v3, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->c:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1771532
    iget-object v3, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->c:Landroid/widget/ImageView;

    div-int/lit8 v5, v4, 0x2

    iget-object v6, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->c:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v5, v6

    div-int/lit8 v6, v4, 0x2

    iget-object v7, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->c:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v6, v7

    iget-object v7, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->c:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v1

    invoke-virtual {v3, v5, v1, v6, v7}, Landroid/widget/ImageView;->layout(IIII)V

    .line 1771533
    :goto_0
    iget-object v3, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->d:LX/BJ5;

    sget-object v5, LX/BJ5;->FULL:LX/BJ5;

    if-eq v3, v5, :cond_3

    :goto_1
    add-int/2addr v1, v2

    .line 1771534
    iget-object v2, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->a:Lcom/facebook/resources/ui/FbTextView;

    div-int/lit8 v3, v4, 0x2

    iget-object v5, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v5}, Lcom/facebook/resources/ui/FbTextView;->getMeasuredWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v3, v5

    div-int/lit8 v5, v4, 0x2

    iget-object v6, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v6}, Lcom/facebook/resources/ui/FbTextView;->getMeasuredWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v6}, Lcom/facebook/resources/ui/FbTextView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v1

    invoke-virtual {v2, v3, v1, v5, v6}, Lcom/facebook/resources/ui/FbTextView;->layout(IIII)V

    .line 1771535
    iget-object v2, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2}, Lcom/facebook/resources/ui/FbTextView;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    .line 1771536
    iget-object v1, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->b:Lcom/facebook/resources/ui/FbButton;

    div-int/lit8 v2, v4, 0x2

    iget-object v3, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->b:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v3}, Lcom/facebook/resources/ui/FbButton;->getMeasuredWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    div-int/lit8 v3, v4, 0x2

    iget-object v4, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->b:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v4}, Lcom/facebook/resources/ui/FbButton;->getMeasuredWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->b:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v4}, Lcom/facebook/resources/ui/FbButton;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/facebook/resources/ui/FbButton;->layout(IIII)V

    .line 1771537
    return-void

    .line 1771538
    :cond_0
    iget-object v5, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->d:LX/BJ5;

    sget-object v6, LX/BJ5;->MINI:LX/BJ5;

    if-eq v5, v6, :cond_1

    iget-object v5, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->d:LX/BJ5;

    sget-object v6, LX/BJ5;->SMALL:LX/BJ5;

    if-ne v5, v6, :cond_2

    .line 1771539
    :cond_1
    invoke-static {v3, v7}, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->a(Landroid/util/DisplayMetrics;I)I

    move-result v1

    .line 1771540
    const/16 v0, 0xc

    invoke-static {v3, v0}, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->a(Landroid/util/DisplayMetrics;I)I

    move-result v0

    .line 1771541
    :cond_2
    iget-object v3, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->c:Landroid/widget/ImageView;

    const/16 v5, 0x8

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 1771542
    :cond_3
    iget-object v2, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->c:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v2, v0

    goto :goto_1
.end method

.method public final onMeasure(II)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 1771545
    invoke-virtual {p0}, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 1771546
    const/16 v1, 0x30

    invoke-static {v0, v1}, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->a(Landroid/util/DisplayMetrics;I)I

    move-result v1

    .line 1771547
    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 1771548
    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 1771549
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 1771550
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    .line 1771551
    const/16 v6, 0x20

    invoke-static {v0, v6}, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->a(Landroid/util/DisplayMetrics;I)I

    move-result v6

    mul-int/lit8 v6, v6, 0x2

    sub-int v6, v4, v6

    invoke-direct {p0, v6}, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->a(I)V

    .line 1771552
    iget-object v6, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->b:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v6, v2, v3}, Lcom/facebook/resources/ui/FbButton;->measure(II)V

    .line 1771553
    iget-object v6, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v6}, Lcom/facebook/resources/ui/FbTextView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v1, v6

    iget-object v6, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->b:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v6}, Lcom/facebook/resources/ui/FbButton;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v1, v6

    const/16 v6, 0x60

    invoke-static {v0, v6}, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->a(Landroid/util/DisplayMetrics;I)I

    move-result v6

    add-int/2addr v1, v6

    .line 1771554
    iget-object v6, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v6}, Lcom/facebook/resources/ui/FbTextView;->getMeasuredHeight()I

    move-result v6

    iget-object v7, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->b:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v7}, Lcom/facebook/resources/ui/FbButton;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v6, v7

    const/16 v7, 0x50

    invoke-static {v0, v7}, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->a(Landroid/util/DisplayMetrics;I)I

    move-result v7

    add-int/2addr v6, v7

    .line 1771555
    iget-object v7, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v7}, Lcom/facebook/resources/ui/FbTextView;->getMeasuredHeight()I

    move-result v7

    iget-object v8, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->b:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v8}, Lcom/facebook/resources/ui/FbButton;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v7, v8

    const/16 v8, 0x2e

    invoke-static {v0, v8}, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->a(Landroid/util/DisplayMetrics;I)I

    move-result v0

    add-int/2addr v0, v7

    .line 1771556
    invoke-virtual {p0}, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->a()V

    .line 1771557
    if-gt v1, v5, :cond_0

    .line 1771558
    iget-object v0, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v2, v3}, Landroid/widget/ImageView;->measure(II)V

    .line 1771559
    invoke-virtual {p0, v4, v1}, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->setMeasuredDimension(II)V

    .line 1771560
    sget-object v0, LX/BJ5;->FULL:LX/BJ5;

    iput-object v0, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->d:LX/BJ5;

    .line 1771561
    :goto_0
    return-void

    .line 1771562
    :cond_0
    if-gt v6, v5, :cond_1

    .line 1771563
    invoke-virtual {p0, v4, v6}, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->setMeasuredDimension(II)V

    .line 1771564
    sget-object v0, LX/BJ5;->MEDIUM:LX/BJ5;

    iput-object v0, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->d:LX/BJ5;

    goto :goto_0

    .line 1771565
    :cond_1
    if-gt v0, v5, :cond_2

    .line 1771566
    invoke-virtual {p0, v4, v0}, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->setMeasuredDimension(II)V

    .line 1771567
    sget-object v0, LX/BJ5;->MINI:LX/BJ5;

    iput-object v0, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->d:LX/BJ5;

    goto :goto_0

    .line 1771568
    :cond_2
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a00f7

    invoke-static {v2, v3}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p0, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1771569
    invoke-virtual {p0, v4, v0}, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->setMeasuredDimension(II)V

    .line 1771570
    sget-object v0, LX/BJ5;->SMALL:LX/BJ5;

    iput-object v0, p0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->d:LX/BJ5;

    goto :goto_0
.end method
