.class public Lcom/facebook/photos/warning/rows/ObjectionableContentImagePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pp;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/CcZ;",
        "LX/BJ4;",
        "TE;",
        "LX/3VN;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field public final b:LX/1WN;

.field private final c:LX/1WM;

.field private final d:Lcom/facebook/multirow/parts/ViewDimensionsPartDefinition;

.field public final e:LX/1Ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1921340
    const-class v0, Lcom/facebook/photos/warning/rows/ObjectionableContentImagePartDefinition;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/warning/rows/ObjectionableContentImagePartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1WN;LX/1WM;Lcom/facebook/multirow/parts/ViewDimensionsPartDefinition;LX/1Ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1921341
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1921342
    iput-object p1, p0, Lcom/facebook/photos/warning/rows/ObjectionableContentImagePartDefinition;->b:LX/1WN;

    .line 1921343
    iput-object p2, p0, Lcom/facebook/photos/warning/rows/ObjectionableContentImagePartDefinition;->c:LX/1WM;

    .line 1921344
    iput-object p3, p0, Lcom/facebook/photos/warning/rows/ObjectionableContentImagePartDefinition;->d:Lcom/facebook/multirow/parts/ViewDimensionsPartDefinition;

    .line 1921345
    iput-object p4, p0, Lcom/facebook/photos/warning/rows/ObjectionableContentImagePartDefinition;->e:LX/1Ad;

    .line 1921346
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/photos/warning/rows/ObjectionableContentImagePartDefinition;
    .locals 7

    .prologue
    .line 1921347
    const-class v1, Lcom/facebook/photos/warning/rows/ObjectionableContentImagePartDefinition;

    monitor-enter v1

    .line 1921348
    :try_start_0
    sget-object v0, Lcom/facebook/photos/warning/rows/ObjectionableContentImagePartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1921349
    sput-object v2, Lcom/facebook/photos/warning/rows/ObjectionableContentImagePartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1921350
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1921351
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1921352
    new-instance p0, Lcom/facebook/photos/warning/rows/ObjectionableContentImagePartDefinition;

    invoke-static {v0}, LX/1WN;->b(LX/0QB;)LX/1WN;

    move-result-object v3

    check-cast v3, LX/1WN;

    invoke-static {v0}, LX/1WM;->a(LX/0QB;)LX/1WM;

    move-result-object v4

    check-cast v4, LX/1WM;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ViewDimensionsPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ViewDimensionsPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ViewDimensionsPartDefinition;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v6

    check-cast v6, LX/1Ad;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/photos/warning/rows/ObjectionableContentImagePartDefinition;-><init>(LX/1WN;LX/1WM;Lcom/facebook/multirow/parts/ViewDimensionsPartDefinition;LX/1Ad;)V

    .line 1921353
    move-object v0, p0

    .line 1921354
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1921355
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/photos/warning/rows/ObjectionableContentImagePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1921356
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1921357
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1921358
    check-cast p2, LX/CcZ;

    check-cast p3, LX/1Pn;

    .line 1921359
    iget-object v0, p2, LX/CcZ;->a:LX/24k;

    if-eqz v0, :cond_0

    .line 1921360
    const v0, 0x7f0d1e07

    iget-object v1, p0, Lcom/facebook/photos/warning/rows/ObjectionableContentImagePartDefinition;->d:Lcom/facebook/multirow/parts/ViewDimensionsPartDefinition;

    iget-object v2, p2, LX/CcZ;->a:LX/24k;

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1921361
    const v0, 0x7f0d1e08

    iget-object v1, p0, Lcom/facebook/photos/warning/rows/ObjectionableContentImagePartDefinition;->d:Lcom/facebook/multirow/parts/ViewDimensionsPartDefinition;

    iget-object v2, p2, LX/CcZ;->a:LX/24k;

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1921362
    :cond_0
    iget-object v0, p2, LX/CcZ;->c:LX/1bf;

    invoke-static {v0}, LX/1bX;->a(LX/1bf;)LX/1bX;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/photos/warning/rows/ObjectionableContentImagePartDefinition;->c:LX/1WM;

    invoke-virtual {v1}, LX/1WM;->a()LX/33B;

    move-result-object v1

    .line 1921363
    iput-object v1, v0, LX/1bX;->j:LX/33B;

    .line 1921364
    move-object v0, v0

    .line 1921365
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v2

    move-object v0, p3

    .line 1921366
    check-cast v0, LX/1Pt;

    sget-object v1, Lcom/facebook/photos/warning/rows/ObjectionableContentImagePartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {v0, v2, v1}, LX/1Pt;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1921367
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1921368
    iget-object v0, p0, Lcom/facebook/photos/warning/rows/ObjectionableContentImagePartDefinition;->e:LX/1Ad;

    sget-object v1, Lcom/facebook/photos/warning/rows/ObjectionableContentImagePartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/1Ae;->a(Z)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, v2}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->p()LX/1Ad;

    move-result-object v0

    .line 1921369
    iput-boolean v3, v0, LX/1Ad;->h:Z

    .line 1921370
    move-object v1, v0

    .line 1921371
    move-object v0, v1

    .line 1921372
    invoke-virtual {v0, v3}, LX/1Ae;->b(Z)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, v4}, LX/1Ae;->d(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    .line 1921373
    iput-object v4, v0, LX/1Ad;->i:Landroid/graphics/drawable/Drawable;

    .line 1921374
    move-object v1, v0

    .line 1921375
    move-object v0, v1

    .line 1921376
    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    move-object v3, v0

    .line 1921377
    iget-object v0, p2, LX/CcZ;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 1921378
    if-nez v0, :cond_7

    .line 1921379
    :cond_1
    :goto_0
    move-object v1, v1

    .line 1921380
    move-object v0, p3

    .line 1921381
    check-cast v0, LX/1Pp;

    if-nez v1, :cond_2

    iget-object v1, p2, LX/CcZ;->e:Ljava/lang/String;

    :cond_2
    sget-object v4, Lcom/facebook/photos/warning/rows/ObjectionableContentImagePartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {v0, v3, v1, v2, v4}, LX/1Pp;->a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1921382
    new-instance v0, LX/BJ4;

    iget-object v1, p0, Lcom/facebook/photos/warning/rows/ObjectionableContentImagePartDefinition;->c:LX/1WM;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 1921383
    const/4 v4, 0x0

    .line 1921384
    iget-object p0, v1, LX/1WM;->b:LX/0oz;

    invoke-virtual {p0}, LX/0oz;->c()LX/0p3;

    move-result-object p0

    .line 1921385
    iget-object p1, v1, LX/1WM;->d:LX/0ad;

    sget-short p2, LX/0fe;->aC:S

    invoke-interface {p1, p2, v4}, LX/0ad;->a(SZ)Z

    move-result p1

    if-eqz p1, :cond_4

    sget-object p1, LX/0p3;->MODERATE:LX/0p3;

    if-eq p0, p1, :cond_3

    sget-object p1, LX/0p3;->POOR:LX/0p3;

    if-ne p0, p1, :cond_4

    :cond_3
    const/4 v4, 0x1

    :cond_4
    move v4, v4

    .line 1921386
    if-nez v4, :cond_6

    const/4 v4, 0x0

    .line 1921387
    iget-object p0, v1, LX/1WM;->d:LX/0ad;

    sget-short p1, LX/0fe;->aB:S

    invoke-interface {p0, p1, v4}, LX/0ad;->a(SZ)Z

    move-result p0

    if-eqz p0, :cond_5

    invoke-static {v2}, LX/1sT;->a(Landroid/content/Context;)I

    move-result p0

    const/16 p1, 0x7db

    if-gt p0, p1, :cond_5

    const/4 v4, 0x1

    :cond_5
    move v4, v4

    .line 1921388
    if-eqz v4, :cond_8

    :cond_6
    const/4 v4, 0x1

    :goto_1
    move v1, v4

    .line 1921389
    invoke-direct {v0, v3, v1}, LX/BJ4;-><init>(LX/1aZ;Z)V

    return-object v0

    .line 1921390
    :cond_7
    invoke-static {v0}, LX/1WF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    .line 1921391
    if-eqz v4, :cond_1

    .line 1921392
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_8
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x785f0f24

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1921393
    check-cast p1, LX/CcZ;

    check-cast p2, LX/BJ4;

    check-cast p4, LX/3VN;

    .line 1921394
    invoke-virtual {p4, p2}, LX/3VN;->a(LX/BJ4;)V

    .line 1921395
    iget-object v1, p0, Lcom/facebook/photos/warning/rows/ObjectionableContentImagePartDefinition;->b:LX/1WN;

    const-string v2, "warning_screen_shown"

    iget-boolean v4, p1, LX/CcZ;->d:Z

    iget-object p3, p1, LX/CcZ;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v4, p3}, LX/1WN;->a(Ljava/lang/String;ZLjava/lang/String;)V

    .line 1921396
    const/16 v1, 0x1f

    const v2, -0x5694fb81

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1921397
    check-cast p4, LX/3VN;

    .line 1921398
    invoke-virtual {p4}, LX/3VN;->a()V

    .line 1921399
    return-void
.end method
