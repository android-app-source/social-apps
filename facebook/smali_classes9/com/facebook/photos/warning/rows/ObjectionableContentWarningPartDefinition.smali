.class public Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/Ccm;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field public final a:LX/1WN;

.field public final b:LX/1WM;

.field private final c:Lcom/facebook/multirow/parts/ViewDimensionsPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final e:Lcom/facebook/content/SecureContextHelper;

.field private final f:Lcom/facebook/multirow/parts/TextPartDefinition;

.field public final g:LX/17Y;


# direct methods
.method public constructor <init>(LX/1WN;LX/1WM;Lcom/facebook/multirow/parts/ViewDimensionsPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/multirow/parts/TextPartDefinition;LX/17Y;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1921626
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1921627
    iput-object p1, p0, Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;->a:LX/1WN;

    .line 1921628
    iput-object p2, p0, Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;->b:LX/1WM;

    .line 1921629
    iput-object p3, p0, Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;->c:Lcom/facebook/multirow/parts/ViewDimensionsPartDefinition;

    .line 1921630
    iput-object p4, p0, Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 1921631
    iput-object p5, p0, Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;->e:Lcom/facebook/content/SecureContextHelper;

    .line 1921632
    iput-object p6, p0, Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;->f:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 1921633
    iput-object p7, p0, Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;->g:LX/17Y;

    .line 1921634
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;
    .locals 11

    .prologue
    .line 1921635
    const-class v1, Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;

    monitor-enter v1

    .line 1921636
    :try_start_0
    sget-object v0, Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1921637
    sput-object v2, Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1921638
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1921639
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1921640
    new-instance v3, Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;

    invoke-static {v0}, LX/1WN;->b(LX/0QB;)LX/1WN;

    move-result-object v4

    check-cast v4, LX/1WN;

    invoke-static {v0}, LX/1WM;->a(LX/0QB;)LX/1WM;

    move-result-object v5

    check-cast v5, LX/1WM;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ViewDimensionsPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ViewDimensionsPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ViewDimensionsPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v8

    check-cast v8, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v10

    check-cast v10, LX/17Y;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;-><init>(LX/1WN;LX/1WM;Lcom/facebook/multirow/parts/ViewDimensionsPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/multirow/parts/TextPartDefinition;LX/17Y;)V

    .line 1921641
    move-object v0, v3

    .line 1921642
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1921643
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1921644
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1921645
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 1921646
    check-cast p2, LX/Ccm;

    check-cast p3, LX/1Pq;

    .line 1921647
    const v1, 0x7f0d1e0b

    iget-object v2, p0, Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;->f:Lcom/facebook/multirow/parts/TextPartDefinition;

    move-object v0, p3

    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-boolean v3, p2, LX/Ccm;->e:Z

    invoke-static {v3}, LX/Cce;->c(Z)I

    move-result v3

    .line 1921648
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1921649
    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1921650
    const v6, 0x7f0826fd

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1921651
    new-instance v7, LX/47x;

    invoke-direct {v7, v4}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v7, v5}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v4

    const-string v5, "link_change_content_filter"

    .line 1921652
    new-instance v7, LX/Ccl;

    invoke-direct {v7, p0, v0, p2}, LX/Ccl;-><init>(Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;Landroid/content/Context;LX/Ccm;)V

    move-object v7, v7

    .line 1921653
    const/16 v8, 0x21

    invoke-virtual {v4, v5, v6, v7, v8}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    move-result-object v4

    .line 1921654
    invoke-virtual {v4}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v4

    move-object v0, v4

    .line 1921655
    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1921656
    const v0, 0x7f0d1e0c

    iget-object v1, p0, Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v2, LX/Cck;

    invoke-direct {v2, p0, p2, p3}, LX/Cck;-><init>(Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;LX/Ccm;LX/1Pq;)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1921657
    const v0, 0x7f0d1e0c

    iget-object v1, p0, Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;->f:Lcom/facebook/multirow/parts/TextPartDefinition;

    check-cast p3, LX/1Pn;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-boolean v3, p2, LX/Ccm;->e:Z

    invoke-static {v3}, LX/Cce;->d(Z)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1921658
    iget-object v0, p2, LX/Ccm;->a:LX/24k;

    if-eqz v0, :cond_0

    .line 1921659
    const v0, 0x7f0d1e06

    iget-object v1, p0, Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;->c:Lcom/facebook/multirow/parts/ViewDimensionsPartDefinition;

    iget-object v2, p2, LX/Ccm;->a:LX/24k;

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1921660
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method
