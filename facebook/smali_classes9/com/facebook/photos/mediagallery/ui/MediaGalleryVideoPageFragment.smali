.class public Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/CaH;
.implements LX/CaJ;


# static fields
.field private static final h:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/1m0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/CaK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/23g;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/CcX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/video/player/IsVideoSpecDisplayEnabled;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/19m;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0W3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:Lcom/facebook/video/player/RichVideoPlayer;

.field public j:Ljava/lang/String;

.field private k:LX/2pa;

.field public l:Z

.field private m:Z

.field private n:Z

.field private o:LX/Cae;

.field private p:LX/Caf;

.field private q:LX/Cai;

.field public r:LX/7N9;

.field private final s:Landroid/graphics/Rect;

.field private t:LX/Caj;

.field private u:LX/Caj;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1918530
    const-class v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->h:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1918524
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1918525
    iput-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->m:Z

    .line 1918526
    iput-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->n:Z

    .line 1918527
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->s:Landroid/graphics/Rect;

    .line 1918528
    sget-object v0, LX/Caj;->NONE:LX/Caj;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->t:LX/Caj;

    .line 1918529
    return-void
.end method

.method private a(LX/2pa;)V
    .locals 3

    .prologue
    .line 1918515
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->i:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04D;->MEDIA_GALLERY:LX/04D;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerOrigin(LX/04D;)V

    .line 1918516
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->i:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1918517
    new-instance v1, LX/Cah;

    invoke-direct {v1, p0}, LX/Cah;-><init>(Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;)V

    move-object v1, v1

    .line 1918518
    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1918519
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->u:LX/Caj;

    sget-object v1, LX/Caj;->SPHERICAL:LX/Caj;

    if-ne v0, v1, :cond_0

    .line 1918520
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->i:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/2pa;)V

    .line 1918521
    :goto_0
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->i:Lcom/facebook/video/player/RichVideoPlayer;

    const/4 v1, 0x0

    sget-object v2, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 1918522
    return-void

    .line 1918523
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->i:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    goto :goto_0
.end method

.method private a(Lcom/facebook/video/player/RichVideoPlayer;)V
    .locals 3

    .prologue
    .line 1918488
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->t:LX/Caj;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->u:LX/Caj;

    if-ne v0, v1, :cond_0

    .line 1918489
    :goto_0
    return-void

    .line 1918490
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->t:LX/Caj;

    sget-object v1, LX/Caj;->NONE:LX/Caj;

    if-eq v0, v1, :cond_1

    .line 1918491
    invoke-virtual {p1}, Lcom/facebook/video/player/RichVideoPlayer;->n()Ljava/util/List;

    .line 1918492
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1918493
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->u:LX/Caj;

    iput-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->t:LX/Caj;

    .line 1918494
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1918495
    new-instance v1, LX/7MZ;

    invoke-direct {v1, v0}, LX/7MZ;-><init>(Landroid/content/Context;)V

    .line 1918496
    invoke-static {p1, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1918497
    :cond_2
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->t:LX/Caj;

    sget-object v2, LX/Caj;->SPHERICAL:LX/Caj;

    if-ne v1, v2, :cond_3

    .line 1918498
    new-instance v1, Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-direct {v1, v0}, Lcom/facebook/video/player/plugins/Video360Plugin;-><init>(Landroid/content/Context;)V

    .line 1918499
    invoke-static {p1, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1918500
    new-instance v1, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;

    invoke-direct {v1, v0}, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;-><init>(Landroid/content/Context;)V

    .line 1918501
    invoke-static {p1, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1918502
    new-instance v1, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;

    invoke-direct {v1, v0}, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;-><init>(Landroid/content/Context;)V

    .line 1918503
    invoke-static {p1, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1918504
    :goto_1
    new-instance v1, LX/7N9;

    invoke-direct {v1, v0}, LX/7N9;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->r:LX/7N9;

    .line 1918505
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->r:LX/7N9;

    .line 1918506
    invoke-static {p1, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1918507
    new-instance v1, Lcom/facebook/video/player/plugins/CoverImagePlugin;

    sget-object v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->h:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v1, v0, v2}, Lcom/facebook/video/player/plugins/CoverImagePlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1918508
    invoke-static {p1, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1918509
    new-instance v1, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-direct {v1, v0}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;-><init>(Landroid/content/Context;)V

    .line 1918510
    invoke-static {p1, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1918511
    goto :goto_0

    .line 1918512
    :cond_3
    new-instance v1, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-direct {v1, v0}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    .line 1918513
    invoke-static {p1, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1918514
    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1918487
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->j:Ljava/lang/String;

    invoke-static {p1, v0}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static b(Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;)Z
    .locals 2

    .prologue
    .line 1918486
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->i:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->s:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method

.method public static e(Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;)V
    .locals 2

    .prologue
    .line 1918314
    invoke-static {p0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->b(Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->k(Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1918315
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->i:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 1918316
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->d:LX/CcX;

    .line 1918317
    iget-object v1, v0, LX/CcX;->a:LX/CaP;

    move-object v0, v1

    .line 1918318
    if-eqz v0, :cond_0

    .line 1918319
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->d:LX/CcX;

    .line 1918320
    iget-object v1, v0, LX/CcX;->a:LX/CaP;

    move-object v0, v1

    .line 1918321
    invoke-virtual {v0}, LX/CaP;->a()V

    .line 1918322
    :cond_0
    return-void
.end method

.method public static k(Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;)Z
    .locals 2

    .prologue
    .line 1918475
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->i:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1918476
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v0, v1

    .line 1918477
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->i:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1918478
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v0, v1

    .line 1918479
    iget-object v1, v0, LX/2pb;->y:LX/2qV;

    move-object v0, v1

    .line 1918480
    if-eqz v0, :cond_0

    .line 1918481
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->i:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1918482
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v0, v1

    .line 1918483
    iget-object v1, v0, LX/2pb;->y:LX/2qV;

    move-object v0, v1

    .line 1918484
    invoke-virtual {v0}, LX/2qV;->isPlayingState()Z

    move-result v0

    .line 1918485
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1918531
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/5kD;)V
    .locals 14

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1918383
    if-nez p1, :cond_1

    .line 1918384
    :cond_0
    :goto_0
    return-void

    .line 1918385
    :cond_1
    invoke-interface {p1}, LX/5kD;->T()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->m:Z

    if-eqz v0, :cond_2

    move v6, v1

    .line 1918386
    :goto_1
    if-eqz v6, :cond_3

    sget-object v0, LX/Caj;->SPHERICAL:LX/Caj;

    :goto_2
    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->u:LX/Caj;

    .line 1918387
    invoke-interface {p1}, LX/5kD;->T()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->m:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->n:Z

    if-eqz v0, :cond_4

    move v3, v1

    .line 1918388
    :goto_3
    if-eqz v6, :cond_5

    invoke-interface {p1}, LX/5kD;->al()Ljava/lang/String;

    move-result-object v0

    :goto_4
    invoke-static {v0}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 1918389
    if-eqz v6, :cond_6

    invoke-interface {p1}, LX/5kD;->ak()Ljava/lang/String;

    move-result-object v0

    :goto_5
    invoke-static {v0}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 1918390
    if-eqz v7, :cond_7

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->l:Z

    .line 1918391
    invoke-interface {p1}, LX/5kD;->e()LX/1Fb;

    move-result-object v0

    .line 1918392
    if-eqz v4, :cond_0

    if-eqz v0, :cond_0

    .line 1918393
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->a:LX/1m0;

    invoke-interface {p1}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5, v2}, LX/1m0;->a(Landroid/net/Uri;Ljava/lang/String;Z)Landroid/net/Uri;

    move-result-object v8

    .line 1918394
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->c:LX/23g;

    invoke-interface {p1}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/23g;->c(Ljava/lang/String;)V

    .line 1918395
    invoke-interface {p1}, LX/5kD;->aj_()LX/1Fb;

    move-result-object v1

    .line 1918396
    if-eqz v1, :cond_8

    invoke-interface {v1}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    :goto_7
    invoke-static {v0}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 1918397
    invoke-interface {p1}, LX/5kD;->e()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->c()I

    move-result v4

    .line 1918398
    invoke-interface {p1}, LX/5kD;->e()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->a()I

    move-result v5

    .line 1918399
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 1918400
    if-lez v5, :cond_d

    .line 1918401
    int-to-double v0, v4

    int-to-double v4, v5

    div-double/2addr v0, v4

    move-wide v4, v0

    .line 1918402
    :goto_8
    invoke-static {}, Lcom/facebook/video/engine/VideoDataSource;->newBuilder()LX/2oE;

    move-result-object v0

    .line 1918403
    iput-object v8, v0, LX/2oE;->a:Landroid/net/Uri;

    .line 1918404
    move-object v0, v0

    .line 1918405
    iput-object v7, v0, LX/2oE;->b:Landroid/net/Uri;

    .line 1918406
    move-object v0, v0

    .line 1918407
    sget-object v1, LX/097;->FROM_STREAM:LX/097;

    .line 1918408
    iput-object v1, v0, LX/2oE;->e:LX/097;

    .line 1918409
    move-object v0, v0

    .line 1918410
    invoke-virtual {v0}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v7

    .line 1918411
    const/4 v0, 0x0

    .line 1918412
    if-eqz v6, :cond_a

    .line 1918413
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1918414
    invoke-interface {p1}, LX/5kD;->I()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel$GuidedTourModel;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-interface {p1}, LX/5kD;->I()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel$GuidedTourModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel$GuidedTourModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 1918415
    invoke-interface {p1}, LX/5kD;->I()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel$GuidedTourModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel$GuidedTourModel;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v10

    move v1, v2

    :goto_9
    if-ge v1, v10, :cond_9

    invoke-virtual {v8, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel$GuidedTourModel$KeyframesModel;

    .line 1918416
    new-instance v11, LX/7DB;

    invoke-direct {v11}, LX/7DB;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel$GuidedTourModel$KeyframesModel;->b()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, LX/7DB;->a(J)LX/7DB;

    move-result-object v11

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel$GuidedTourModel$KeyframesModel;->a()I

    move-result v12

    .line 1918417
    iput v12, v11, LX/7DB;->b:I

    .line 1918418
    move-object v11, v11

    .line 1918419
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel$GuidedTourModel$KeyframesModel;->c()I

    move-result v0

    .line 1918420
    iput v0, v11, LX/7DB;->c:I

    .line 1918421
    move-object v0, v11

    .line 1918422
    invoke-virtual {v0}, LX/7DB;->a()Lcom/facebook/spherical/model/KeyframeParams;

    move-result-object v0

    .line 1918423
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1918424
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_9

    :cond_2
    move v6, v2

    .line 1918425
    goto/16 :goto_1

    .line 1918426
    :cond_3
    sget-object v0, LX/Caj;->REGULAR:LX/Caj;

    goto/16 :goto_2

    :cond_4
    move v3, v2

    .line 1918427
    goto/16 :goto_3

    .line 1918428
    :cond_5
    invoke-interface {p1}, LX/5kD;->ad()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 1918429
    :cond_6
    invoke-interface {p1}, LX/5kD;->K()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    :cond_7
    move v0, v2

    .line 1918430
    goto/16 :goto_6

    .line 1918431
    :cond_8
    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_7

    .line 1918432
    :cond_9
    new-instance v0, LX/7DI;

    invoke-direct {v0}, LX/7DI;-><init>()V

    invoke-interface {p1}, LX/5kD;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/19o;->fromString(Ljava/lang/String;)LX/19o;

    move-result-object v1

    .line 1918433
    iput-object v1, v0, LX/7DI;->a:LX/19o;

    .line 1918434
    move-object v0, v0

    .line 1918435
    invoke-interface {p1}, LX/5kD;->N()I

    move-result v1

    .line 1918436
    iput v1, v0, LX/7DI;->c:I

    .line 1918437
    move-object v0, v0

    .line 1918438
    invoke-interface {p1}, LX/5kD;->O()I

    move-result v1

    neg-int v1, v1

    .line 1918439
    iput v1, v0, LX/7DI;->d:I

    .line 1918440
    move-object v0, v0

    .line 1918441
    invoke-interface {p1}, LX/5kD;->P()I

    move-result v1

    .line 1918442
    iput v1, v0, LX/7DI;->e:I

    .line 1918443
    move-object v0, v0

    .line 1918444
    invoke-interface {p1}, LX/5kD;->am()I

    move-result v1

    .line 1918445
    iput v1, v0, LX/7DI;->f:I

    .line 1918446
    move-object v0, v0

    .line 1918447
    invoke-interface {p1}, LX/5kD;->aj()D

    move-result-wide v10

    invoke-virtual {v0, v10, v11}, LX/7DI;->a(D)LX/7DI;

    move-result-object v0

    invoke-interface {p1}, LX/5kD;->ai()D

    move-result-wide v10

    invoke-virtual {v0, v10, v11}, LX/7DI;->b(D)LX/7DI;

    move-result-object v0

    new-instance v1, LX/7D9;

    invoke-direct {v1}, LX/7D9;-><init>()V

    invoke-virtual {v1, v6}, LX/7D9;->a(Ljava/util/List;)LX/7D9;

    move-result-object v1

    invoke-virtual {v1}, LX/7D9;->a()Lcom/facebook/spherical/model/GuidedTourParams;

    move-result-object v1

    .line 1918448
    iput-object v1, v0, LX/7DI;->i:Lcom/facebook/spherical/model/GuidedTourParams;

    .line 1918449
    move-object v0, v0

    .line 1918450
    invoke-interface {p1}, LX/5kD;->D()Z

    move-result v1

    .line 1918451
    iput-boolean v1, v0, LX/7DI;->j:Z

    .line 1918452
    move-object v0, v0

    .line 1918453
    invoke-interface {p1}, LX/5kD;->Y()D

    move-result-wide v10

    invoke-virtual {v0, v10, v11}, LX/7DI;->c(D)LX/7DI;

    move-result-object v0

    invoke-interface {p1}, LX/5kD;->H()D

    move-result-wide v10

    invoke-virtual {v0, v10, v11}, LX/7DI;->d(D)LX/7DI;

    move-result-object v0

    invoke-virtual {v0}, LX/7DI;->a()Lcom/facebook/spherical/model/SphericalVideoParams;

    move-result-object v0

    .line 1918454
    :cond_a
    invoke-static {}, Lcom/facebook/video/engine/VideoPlayerParams;->newBuilder()LX/2oH;

    move-result-object v1

    invoke-virtual {v1, v7}, LX/2oH;->a(Lcom/facebook/video/engine/VideoDataSource;)LX/2oH;

    move-result-object v1

    invoke-interface {p1}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v6

    .line 1918455
    iput-object v6, v1, LX/2oH;->b:Ljava/lang/String;

    .line 1918456
    move-object v1, v1

    .line 1918457
    iput-boolean v2, v1, LX/2oH;->g:Z

    .line 1918458
    move-object v1, v1

    .line 1918459
    iput-object v0, v1, LX/2oH;->m:Lcom/facebook/spherical/model/SphericalVideoParams;

    .line 1918460
    move-object v0, v1

    .line 1918461
    iput-boolean v3, v0, LX/2oH;->y:Z

    .line 1918462
    move-object v0, v0

    .line 1918463
    new-instance v1, LX/2pZ;

    invoke-direct {v1}, LX/2pZ;-><init>()V

    invoke-virtual {v0}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v0

    .line 1918464
    iput-object v0, v1, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1918465
    move-object v0, v1

    .line 1918466
    invoke-virtual {v0, v4, v5}, LX/2pZ;->a(D)LX/2pZ;

    move-result-object v0

    const-string v1, "CoverImageParamsKey"

    invoke-static {v9}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v2

    invoke-virtual {v2}, LX/1bX;->n()LX/1bf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/2pZ;->a(Ljava/lang/String;Ljava/lang/Object;)LX/2pZ;

    move-result-object v0

    const-string v1, "InvisibleSeekBarListenerKey"

    .line 1918467
    new-instance v2, LX/Cag;

    invoke-direct {v2, p0}, LX/Cag;-><init>(Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;)V

    move-object v2, v2

    .line 1918468
    invoke-virtual {v0, v1, v2}, LX/2pZ;->a(Ljava/lang/String;Ljava/lang/Object;)LX/2pZ;

    move-result-object v0

    invoke-virtual {v0}, LX/2pZ;->b()LX/2pa;

    move-result-object v0

    .line 1918469
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->i:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v1, :cond_c

    .line 1918470
    iget-boolean v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->m:Z

    if-eqz v1, :cond_b

    .line 1918471
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->i:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-direct {p0, v1}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->a(Lcom/facebook/video/player/RichVideoPlayer;)V

    .line 1918472
    :cond_b
    invoke-direct {p0, v0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->a(LX/2pa;)V

    .line 1918473
    :goto_a
    invoke-interface {p1}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->j:Ljava/lang/String;

    goto/16 :goto_0

    .line 1918474
    :cond_c
    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->k:LX/2pa;

    goto :goto_a

    :cond_d
    move-wide v4, v0

    goto/16 :goto_8
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 1918378
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1918379
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    invoke-static {v0}, LX/1m0;->a(LX/0QB;)LX/1m0;

    move-result-object v3

    check-cast v3, LX/1m0;

    invoke-static {v0}, LX/CaK;->a(LX/0QB;)LX/CaK;

    move-result-object v4

    check-cast v4, LX/CaK;

    invoke-static {v0}, LX/23g;->a(LX/0QB;)LX/23g;

    move-result-object v5

    check-cast v5, LX/23g;

    invoke-static {v0}, LX/CcX;->a(LX/0QB;)LX/CcX;

    move-result-object v6

    check-cast v6, LX/CcX;

    invoke-static {v0}, LX/3Gf;->b(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v7

    check-cast v7, Ljava/lang/Boolean;

    invoke-static {v0}, LX/19m;->a(LX/0QB;)LX/19m;

    move-result-object v8

    check-cast v8, LX/19m;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v0

    check-cast v0, LX/0W3;

    iput-object v3, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->a:LX/1m0;

    iput-object v4, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->b:LX/CaK;

    iput-object v5, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->c:LX/23g;

    iput-object v6, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->d:LX/CcX;

    iput-object v7, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->e:Ljava/lang/Boolean;

    iput-object v8, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->f:LX/19m;

    iput-object v0, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->g:LX/0W3;

    .line 1918380
    if-eqz p1, :cond_0

    .line 1918381
    const-string v0, "EXTRA_MEDIA_ID"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->j:Ljava/lang/String;

    .line 1918382
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0xd82b7d4

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1918377
    const v1, 0x7f030a91

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0xa8a45bf

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2616bfeb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1918372
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->b:LX/CaK;

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->j:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/CaK;->a(Ljava/lang/String;)V

    .line 1918373
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->i:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->q:LX/Cai;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/2oa;)V

    .line 1918374
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->i:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 1918375
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 1918376
    const/16 v1, 0x2b

    const v2, 0x1dd49134

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1c3107a2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1918364
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1918365
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->d:LX/CcX;

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->o:LX/Cae;

    .line 1918366
    if-eqz v2, :cond_0

    .line 1918367
    iget-object v4, v1, LX/CcX;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v4, v2}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1918368
    :cond_0
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->d:LX/CcX;

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->p:LX/Caf;

    .line 1918369
    if-eqz v2, :cond_1

    .line 1918370
    iget-object v4, v1, LX/CcX;->c:Ljava/util/WeakHashMap;

    invoke-virtual {v4, v2}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1918371
    :cond_1
    const/16 v1, 0x2b

    const v2, -0x5acbaab5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x57314f13

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1918344
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1918345
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->o:LX/Cae;

    if-nez v1, :cond_0

    .line 1918346
    new-instance v1, LX/Cae;

    invoke-direct {v1, p0}, LX/Cae;-><init>(Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;)V

    move-object v1, v1

    .line 1918347
    iput-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->o:LX/Cae;

    .line 1918348
    :cond_0
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->p:LX/Caf;

    if-nez v1, :cond_1

    .line 1918349
    new-instance v1, LX/Caf;

    invoke-direct {v1, p0}, LX/Caf;-><init>(Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;)V

    move-object v1, v1

    .line 1918350
    iput-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->p:LX/Caf;

    .line 1918351
    :cond_1
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->d:LX/CcX;

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->o:LX/Cae;

    .line 1918352
    if-eqz v2, :cond_2

    .line 1918353
    iget-object v4, v1, LX/CcX;->b:Ljava/util/WeakHashMap;

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v2, v5}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1918354
    :cond_2
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->d:LX/CcX;

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->p:LX/Caf;

    .line 1918355
    if-eqz v2, :cond_3

    .line 1918356
    iget-object v4, v1, LX/CcX;->c:Ljava/util/WeakHashMap;

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v2, v5}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1918357
    :cond_3
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->k:LX/2pa;

    if-eqz v1, :cond_5

    .line 1918358
    iget-boolean v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->m:Z

    if-eqz v1, :cond_4

    .line 1918359
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->i:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-direct {p0, v1}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->a(Lcom/facebook/video/player/RichVideoPlayer;)V

    .line 1918360
    :cond_4
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->k:LX/2pa;

    invoke-direct {p0, v1}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->a(LX/2pa;)V

    .line 1918361
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->k:LX/2pa;

    .line 1918362
    :cond_5
    invoke-static {p0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->e(Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;)V

    .line 1918363
    const/16 v1, 0x2b

    const v2, -0x1b81356e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1918341
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1918342
    const-string v0, "EXTRA_MEDIA_ID"

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1918343
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1918323
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1918324
    new-instance v0, LX/Cai;

    invoke-direct {v0, p0}, LX/Cai;-><init>(Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->q:LX/Cai;

    .line 1918325
    const v0, 0x7f0d1b06

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->i:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1918326
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->i:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v3, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->q:LX/Cai;

    invoke-virtual {v0, v3}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/2oa;)V

    .line 1918327
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->g:LX/0W3;

    sget-wide v4, LX/0X5;->gk:J

    invoke-interface {v0, v4, v5}, LX/0W4;->a(J)Z

    move-result v3

    .line 1918328
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->f:LX/19m;

    .line 1918329
    iget-boolean v4, v0, LX/19m;->d:Z

    move v0, v4

    .line 1918330
    if-eqz v0, :cond_1

    if-eqz v3, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->m:Z

    .line 1918331
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->f:LX/19m;

    .line 1918332
    iget-boolean v4, v0, LX/19m;->i:Z

    move v0, v4

    .line 1918333
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    :goto_1
    iput-boolean v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->n:Z

    .line 1918334
    iget-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->m:Z

    if-nez v0, :cond_0

    .line 1918335
    sget-object v0, LX/Caj;->REGULAR:LX/Caj;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->u:LX/Caj;

    .line 1918336
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->i:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-direct {p0, v0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->a(Lcom/facebook/video/player/RichVideoPlayer;)V

    .line 1918337
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->b:LX/CaK;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, LX/CaK;->a(Ljava/lang/String;LX/CaJ;)V

    .line 1918338
    return-void

    :cond_1
    move v0, v2

    .line 1918339
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1918340
    goto :goto_1
.end method
