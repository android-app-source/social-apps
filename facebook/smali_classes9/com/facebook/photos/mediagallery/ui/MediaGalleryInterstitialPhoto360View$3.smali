.class public final Lcom/facebook/photos/mediagallery/ui/MediaGalleryInterstitialPhoto360View$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/CaW;


# direct methods
.method public constructor <init>(LX/CaW;)V
    .locals 0

    .prologue
    .line 1917711
    iput-object p1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryInterstitialPhoto360View$3;->a:LX/CaW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1917712
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryInterstitialPhoto360View$3;->a:LX/CaW;

    invoke-virtual {v0}, LX/CaW;->getHeight()I

    move-result v0

    if-lez v0, :cond_0

    .line 1917713
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryInterstitialPhoto360View$3;->a:LX/CaW;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryInterstitialPhoto360View$3;->a:LX/CaW;

    invoke-static {v1}, LX/CaW;->getBlindHeight(LX/CaW;)I

    move-result v1

    invoke-static {v0, v1}, LX/CaW;->setBlindHeight(LX/CaW;I)V

    .line 1917714
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryInterstitialPhoto360View$3;->a:LX/CaW;

    invoke-virtual {v0}, LX/CaW;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryInterstitialPhoto360View$3;->a:LX/CaW;

    invoke-virtual {v1}, LX/CaW;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 1917715
    const/high16 v1, 0x42b40000    # 90.0f

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryInterstitialPhoto360View$3;->a:LX/CaW;

    iget-object v2, v2, LX/8wv;->h:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    invoke-virtual {v2}, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->b()F

    move-result v2

    div-float v0, v2, v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 1917716
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryInterstitialPhoto360View$3;->a:LX/CaW;

    iget-object v1, v1, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v1, v0}, LX/2qW;->setPreferredVerticalFOVOnZoom(F)V

    .line 1917717
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryInterstitialPhoto360View$3;->a:LX/CaW;

    iget-object v0, v0, LX/8wv;->k:Lcom/facebook/drawee/view/GenericDraweeView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryInterstitialPhoto360View$3;->a:LX/CaW;

    invoke-virtual {v2}, LX/CaW;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryInterstitialPhoto360View$3;->a:LX/CaW;

    invoke-virtual {v3}, LX/CaW;->getWidth()I

    move-result v3

    const/16 v4, 0x11

    invoke-direct {v1, v2, v3, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/GenericDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1917718
    :cond_0
    return-void
.end method
