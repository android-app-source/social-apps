.class public Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/CaH;
.implements LX/CaJ;


# static fields
.field private static final g:Ljava/lang/String;

.field private static final h:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/Cbe;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/CaK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1HI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/CcS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Dh;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/CaV;

.field private j:Ljava/lang/String;

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:LX/Caa;

.field public final n:Landroid/graphics/Rect;

.field public o:Z

.field private p:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1918190
    const-class v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->g:Ljava/lang/String;

    .line 1918191
    const-class v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;

    sget-object v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->g:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->h:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1918187
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1918188
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->n:Landroid/graphics/Rect;

    .line 1918189
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->o:Z

    return-void
.end method

.method public static a(LX/5kD;Ljava/lang/String;Landroid/net/Uri;Z)Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1918180
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1918181
    new-instance v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;

    invoke-direct {v0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;-><init>()V

    .line 1918182
    invoke-interface {p0}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->j:Ljava/lang/String;

    .line 1918183
    iput-object p1, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->k:Ljava/lang/String;

    .line 1918184
    iput-object p2, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->l:Landroid/net/Uri;

    .line 1918185
    iput-boolean p3, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->p:Z

    .line 1918186
    return-object v0
.end method

.method public static a$redex0(Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1918179
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->j:Ljava/lang/String;

    invoke-static {p1, v0}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1918178
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/5kD;)V
    .locals 11

    .prologue
    .line 1918152
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->k:Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-interface {p1}, LX/1U8;->d()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1918153
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->l:Landroid/net/Uri;

    .line 1918154
    :cond_0
    :goto_0
    move-object v0, v0

    .line 1918155
    if-eqz v0, :cond_1

    .line 1918156
    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 1918157
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->d:LX/1Ad;

    sget-object v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->h:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1918158
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->i:LX/CaV;

    invoke-virtual {v1, v0}, LX/8wv;->setPreviewPhotoDraweeController(LX/1aZ;)V

    .line 1918159
    :cond_1
    invoke-static {p1}, LX/5k9;->a(LX/5kD;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v10

    .line 1918160
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLMedia;->aN()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1918161
    const/4 v5, 0x0

    .line 1918162
    :goto_1
    move-object v0, v5

    .line 1918163
    if-eqz v0, :cond_2

    .line 1918164
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->i:LX/CaV;

    sget-object v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->h:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {p1}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/7Dj;->OTHER:LX/7Dj;

    invoke-virtual {v1, v0, v2, v3, v4}, LX/8wv;->a(Lcom/facebook/spherical/photo/model/SphericalPhotoParams;Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;LX/7Dj;)V

    .line 1918165
    :cond_2
    invoke-interface {p1}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->j:Ljava/lang/String;

    .line 1918166
    return-void

    .line 1918167
    :cond_3
    invoke-interface {p1}, LX/1U8;->aj_()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1918168
    invoke-interface {p1}, LX/1U8;->aj_()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 1918169
    :cond_4
    invoke-interface {p1}, LX/1U8;->j()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1918170
    invoke-interface {p1}, LX/1U8;->j()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1918171
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->c:LX/1HI;

    invoke-virtual {v1, v0}, LX/1HI;->b(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1918172
    :cond_5
    invoke-interface {p1}, LX/1U8;->ai_()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1918173
    invoke-interface {p1}, LX/1U8;->ai_()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1918174
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->c:LX/1HI;

    invoke-virtual {v1, v0}, LX/1HI;->b(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1918175
    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1918176
    :cond_7
    invoke-static {v10}, LX/7Dx;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Ljava/util/List;

    move-result-object v5

    .line 1918177
    iget-object v6, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->f:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/7Dh;

    invoke-virtual {v6}, LX/7Dh;->e()Z

    move-result v6

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, LX/1sT;->a(Landroid/content/Context;)I

    move-result v7

    const-string v8, ""

    const-string v9, ""

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v10

    invoke-static/range {v5 .. v10}, LX/7E3;->a(Ljava/util/List;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    move-result-object v5

    goto/16 :goto_1
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 1918192
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1918193
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v8

    move-object v2, p0

    check-cast v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;

    invoke-static {v8}, LX/Cbe;->a(LX/0QB;)LX/Cbe;

    move-result-object v3

    check-cast v3, LX/Cbe;

    invoke-static {v8}, LX/CaK;->a(LX/0QB;)LX/CaK;

    move-result-object v4

    check-cast v4, LX/CaK;

    invoke-static {v8}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v5

    check-cast v5, LX/1HI;

    invoke-static {v8}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v6

    check-cast v6, LX/1Ad;

    invoke-static {v8}, LX/CcS;->a(LX/0QB;)LX/CcS;

    move-result-object v7

    check-cast v7, LX/CcS;

    const/16 v0, 0x3572

    invoke-static {v8, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    iput-object v3, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->a:LX/Cbe;

    iput-object v4, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->b:LX/CaK;

    iput-object v5, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->c:LX/1HI;

    iput-object v6, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->d:LX/1Ad;

    iput-object v7, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->e:LX/CcS;

    iput-object v8, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->f:LX/0Ot;

    .line 1918194
    if-eqz p1, :cond_0

    .line 1918195
    const-string v0, "EXTRA_MEDIA_ID"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->j:Ljava/lang/String;

    .line 1918196
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x64ce91f3

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1918148
    iget-boolean v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->p:Z

    if-eqz v1, :cond_0

    .line 1918149
    new-instance v1, LX/CaW;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/CaW;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->i:LX/CaV;

    .line 1918150
    :goto_0
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->i:LX/CaV;

    const v2, -0x34977935    # -1.5238859E7f

    invoke-static {v2, v0}, LX/02F;->f(II)V

    return-object v1

    .line 1918151
    :cond_0
    new-instance v1, LX/CaV;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/CaV;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->i:LX/CaV;

    goto :goto_0
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2d5c7bbd

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1918141
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->b:LX/CaK;

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->j:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/CaK;->a(Ljava/lang/String;)V

    .line 1918142
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->i:LX/CaV;

    invoke-virtual {v1}, LX/CaV;->q()V

    .line 1918143
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->i:LX/CaV;

    .line 1918144
    iput-object v4, v1, LX/CaV;->D:LX/7US;

    .line 1918145
    iput-object v4, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->i:LX/CaV;

    .line 1918146
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 1918147
    const/16 v1, 0x2b

    const v2, 0x1cced8ae

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x1f69039

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1918110
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1918111
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->e:LX/CcS;

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->m:LX/Caa;

    .line 1918112
    if-eqz v2, :cond_0

    .line 1918113
    iget-object v4, v1, LX/CcS;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v4, v2}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1918114
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->o:Z

    .line 1918115
    const/16 v1, 0x2b

    const v2, -0x2ca14280

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2d5fe85a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1918129
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1918130
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->m:LX/Caa;

    if-nez v1, :cond_0

    .line 1918131
    new-instance v1, LX/Caa;

    invoke-direct {v1, p0}, LX/Caa;-><init>(Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;)V

    move-object v1, v1

    .line 1918132
    iput-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->m:LX/Caa;

    .line 1918133
    :cond_0
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->e:LX/CcS;

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->m:LX/Caa;

    .line 1918134
    if-eqz v2, :cond_1

    .line 1918135
    iget-object v4, v1, LX/CcS;->a:Ljava/util/WeakHashMap;

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v2, v5}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1918136
    :cond_1
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->i:LX/CaV;

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->n:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, LX/CaV;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v1

    move v1, v1

    .line 1918137
    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->o:Z

    if-nez v1, :cond_2

    .line 1918138
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->o:Z

    .line 1918139
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->i:LX/CaV;

    invoke-virtual {v1}, LX/CaV;->r()V

    .line 1918140
    :cond_2
    const/16 v1, 0x2b

    const v2, 0x6f1277af    # 4.53295E28f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1918126
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1918127
    const-string v0, "EXTRA_MEDIA_ID"

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1918128
    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x721ecb38

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1918123
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 1918124
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->i:LX/CaV;

    invoke-virtual {v1}, LX/CaV;->q()V

    .line 1918125
    const/16 v1, 0x2b

    const v2, 0x392783d4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1918116
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1918117
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->i:LX/CaV;

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->a:LX/Cbe;

    .line 1918118
    iget-object p1, v0, LX/Cbe;->a:LX/7US;

    move-object v0, p1

    .line 1918119
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7US;

    .line 1918120
    iput-object v0, v1, LX/CaV;->D:LX/7US;

    .line 1918121
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->b:LX/CaK;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, LX/CaK;->a(Ljava/lang/String;LX/CaJ;)V

    .line 1918122
    return-void
.end method
