.class public Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f2;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final s:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public p:LX/23T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/23g;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/9hF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1917343
    const-class v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;

    const-string v1, "photo_viewer"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;->s:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1917396
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;LX/23T;LX/23g;LX/9hF;)V
    .locals 0

    .prologue
    .line 1917395
    iput-object p1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;->p:LX/23T;

    iput-object p2, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;->q:LX/23g;

    iput-object p3, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;->r:LX/9hF;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;

    invoke-static {v2}, LX/23T;->b(LX/0QB;)LX/23T;

    move-result-object v0

    check-cast v0, LX/23T;

    invoke-static {v2}, LX/23g;->a(LX/0QB;)LX/23g;

    move-result-object v1

    check-cast v1, LX/23g;

    invoke-static {v2}, LX/9hF;->a(LX/0QB;)LX/9hF;

    move-result-object v2

    check-cast v2, LX/9hF;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;->a(Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;LX/23T;LX/23g;LX/9hF;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1917397
    sget-object v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;->s:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1917392
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/os/Bundle;)V

    .line 1917393
    const v0, 0x7f04006d

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;->overridePendingTransition(II)V

    .line 1917394
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1917388
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d03b0

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    .line 1917389
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1917390
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 1917391
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 1917347
    invoke-virtual {p0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1917348
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1917349
    invoke-static {p0, p0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1917350
    const v0, 0x7f030a7f

    invoke-virtual {p0, v0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;->setContentView(I)V

    .line 1917351
    invoke-virtual {p0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "photo_fbid"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 1917352
    invoke-virtual {p0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "photoset_token"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1917353
    invoke-virtual {p0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "extra_show_attribution"

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 1917354
    invoke-virtual {p0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v5, "fullscreen_gallery_source"

    invoke-virtual {v0, v5}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v5, "fullscreen_gallery_source"

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/74S;->valueOf(Ljava/lang/String;)LX/74S;

    move-result-object v0

    .line 1917355
    :goto_0
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1917356
    invoke-static {v3}, LX/9hF;->b(Ljava/lang/String;)LX/9hE;

    move-result-object v1

    .line 1917357
    :goto_1
    invoke-virtual {v1, v2}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    move-result-object v1

    .line 1917358
    iput-boolean v4, v1, LX/9hD;->m:Z

    .line 1917359
    move-object v1, v1

    .line 1917360
    invoke-virtual {v1, v0}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v0

    invoke-virtual {v0}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v1

    .line 1917361
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    check-cast v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;

    .line 1917362
    if-nez v0, :cond_0

    .line 1917363
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;->p:LX/23T;

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;->q:LX/23g;

    sget-object v3, Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;->s:Lcom/facebook/common/callercontext/CallerContext;

    const/4 v12, 0x0

    .line 1917364
    move-object v8, v1

    move-object v9, v0

    move-object v10, v2

    move-object v11, v3

    move-object v13, v12

    invoke-static/range {v8 .. v13}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->a(Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/23T;LX/23g;Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/graphql/model/GraphQLStory;LX/9hK;)Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    move-result-object v8

    move-object v0, v8

    .line 1917365
    new-instance v2, LX/CaI;

    invoke-direct {v2, p0}, LX/CaI;-><init>(Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;)V

    .line 1917366
    new-instance v3, LX/9eM;

    invoke-direct {v3, v1}, LX/9eM;-><init>(Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;)V

    sget-object v1, LX/31M;->UP:LX/31M;

    invoke-virtual {v3, v1}, LX/9eM;->a(LX/31M;)LX/9eM;

    move-result-object v1

    sget-object v3, LX/31M;->UP:LX/31M;

    invoke-virtual {v3}, LX/31M;->flag()I

    move-result v3

    sget-object v4, LX/31M;->DOWN:LX/31M;

    invoke-virtual {v4}, LX/31M;->flag()I

    move-result v4

    or-int/2addr v3, v4

    .line 1917367
    iput v3, v1, LX/9eM;->f:I

    .line 1917368
    move-object v1, v1

    .line 1917369
    const/high16 v3, -0x1000000

    .line 1917370
    iput v3, v1, LX/9eM;->g:I

    .line 1917371
    move-object v1, v1

    .line 1917372
    invoke-virtual {v1}, LX/9eM;->a()Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;

    move-result-object v1

    .line 1917373
    const/4 v3, 0x0

    invoke-static {p0, v0, v1, v3, v2}, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->a(Landroid/content/Context;Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;Lcom/facebook/photos/dialog/PhotoAnimationDialogLaunchParams;LX/9hN;Landroid/content/DialogInterface$OnDismissListener;)Z

    move-result v1

    .line 1917374
    if-nez v1, :cond_0

    .line 1917375
    invoke-virtual {v0}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->c()V

    .line 1917376
    invoke-virtual {p0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;->finish()V

    .line 1917377
    :cond_0
    return-void

    .line 1917378
    :cond_1
    sget-object v0, LX/74S;->INTENT:LX/74S;

    goto :goto_0

    .line 1917379
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v5, "extra_photo_set_fb_id_array"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v3

    .line 1917380
    if-eqz v3, :cond_4

    .line 1917381
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 1917382
    :goto_2
    array-length v6, v3

    if-ge v1, v6, :cond_3

    .line 1917383
    aget-wide v6, v3, v1

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1917384
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1917385
    :cond_3
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1917386
    :goto_3
    invoke-static {v1}, LX/9hF;->f(LX/0Px;)LX/9hE;

    move-result-object v1

    goto/16 :goto_1

    .line 1917387
    :cond_4
    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    goto :goto_3
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 1917344
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 1917345
    const/4 v0, 0x0

    const v1, 0x7f04006e

    invoke-virtual {p0, v0, v1}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryActivity;->overridePendingTransition(II)V

    .line 1917346
    return-void
.end method
