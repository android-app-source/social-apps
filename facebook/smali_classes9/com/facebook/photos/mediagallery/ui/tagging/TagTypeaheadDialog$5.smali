.class public final Lcom/facebook/photos/mediagallery/ui/tagging/TagTypeaheadDialog$5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/Cb5;


# direct methods
.method public constructor <init>(LX/Cb5;)V
    .locals 0

    .prologue
    .line 1918961
    iput-object p1, p0, Lcom/facebook/photos/mediagallery/ui/tagging/TagTypeaheadDialog$5;->a:LX/Cb5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    .line 1918962
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/tagging/TagTypeaheadDialog$5;->a:LX/Cb5;

    const/4 v2, 0x1

    .line 1918963
    iget-object v1, v0, LX/Cb5;->e:LX/CbL;

    invoke-virtual {v1}, LX/CbL;->c()LX/CbN;

    move-result-object v6

    .line 1918964
    iget-object v1, v0, LX/Cb5;->l:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->setVisibility(I)V

    .line 1918965
    iget-object v1, v0, LX/Cb5;->l:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    iget-object v3, v0, LX/Cb5;->h:LX/8JW;

    .line 1918966
    iget-object v4, v6, LX/CbN;->e:LX/0Px;

    move-object v4, v4

    .line 1918967
    new-instance v5, Landroid/graphics/PointF;

    iget-object v7, v6, LX/CbN;->c:Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->x:F

    iget-object v8, v6, LX/CbN;->c:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->y:F

    invoke-direct {v5, v7, v8}, Landroid/graphics/PointF;-><init>(FF)V

    move-object v5, v5

    .line 1918968
    invoke-virtual {v6}, LX/CbN;->c()Landroid/graphics/PointF;

    move-result-object v6

    move v7, v2

    invoke-virtual/range {v1 .. v7}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->a(ZLX/8JW;Ljava/util/List;Landroid/graphics/PointF;Landroid/graphics/PointF;Z)V

    .line 1918969
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    invoke-static {v0, v1}, LX/Cb5;->a(LX/Cb5;LX/0am;)V

    .line 1918970
    iget-object v1, v0, LX/Cb5;->i:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1918971
    iget-object v1, v0, LX/Cb5;->i:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1918972
    iget-object v1, v0, LX/Cb5;->i:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8Jd;

    iget-object v2, v0, LX/Cb5;->e:LX/CbL;

    .line 1918973
    iget v3, v2, LX/CbL;->e:F

    move v2, v3

    .line 1918974
    invoke-virtual {v1, v2}, LX/8Jd;->setRadius(F)V

    .line 1918975
    iget-object v1, v0, LX/Cb5;->i:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8Jd;

    iget-object v2, v0, LX/Cb5;->e:LX/CbL;

    .line 1918976
    iget-object v3, v2, LX/CbL;->d:Landroid/graphics/PointF;

    move-object v2, v3

    .line 1918977
    invoke-virtual {v1, v2}, LX/8Jd;->setPosition(Landroid/graphics/PointF;)V

    .line 1918978
    iget-object v1, v0, LX/Cb5;->i:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8Jd;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/8Jd;->setVisibility(I)V

    .line 1918979
    :cond_0
    return-void
.end method
