.class public final Lcom/facebook/photos/mediagallery/ui/tagging/TagTypeaheadDialog$4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/Cb5;


# direct methods
.method public constructor <init>(LX/Cb5;)V
    .locals 0

    .prologue
    .line 1918953
    iput-object p1, p0, Lcom/facebook/photos/mediagallery/ui/tagging/TagTypeaheadDialog$4;->a:LX/Cb5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 12

    .prologue
    .line 1918954
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/tagging/TagTypeaheadDialog$4;->a:LX/Cb5;

    iget-object v0, v0, LX/Cb5;->e:LX/CbL;

    invoke-virtual {v0}, LX/CbL;->c()LX/CbN;

    move-result-object v0

    .line 1918955
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/tagging/TagTypeaheadDialog$4;->a:LX/Cb5;

    iget-object v1, v1, LX/Cb5;->l:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    invoke-virtual {v0}, LX/CbN;->c()Landroid/graphics/PointF;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->a(Landroid/graphics/PointF;F)V

    .line 1918956
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/tagging/TagTypeaheadDialog$4;->a:LX/Cb5;

    iget-object v0, v0, LX/Cb5;->l:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    invoke-virtual {v0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->b()V

    .line 1918957
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/tagging/TagTypeaheadDialog$4;->a:LX/Cb5;

    iget-object v0, v0, LX/Cb5;->e:LX/CbL;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/tagging/TagTypeaheadDialog$4;->a:LX/Cb5;

    iget-object v1, v1, LX/Cb5;->o:Ljava/lang/Runnable;

    .line 1918958
    iget-object v3, v0, LX/CbL;->p:Landroid/graphics/PointF;

    iget-object v4, v0, LX/CbL;->g:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    iget-object v5, v0, LX/CbL;->g:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->centerY()F

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/PointF;->set(FF)V

    .line 1918959
    iget-object v3, v0, LX/CbL;->o:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

    invoke-virtual {v3}, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->getZoomableController()LX/5ue;

    move-result-object v4

    iget-object v3, v0, LX/CbL;->b:LX/8Jb;

    iget v5, v3, LX/8Jb;->a:F

    iget-object v6, v0, LX/CbL;->p:Landroid/graphics/PointF;

    iget-object v7, v0, LX/CbL;->i:Landroid/graphics/PointF;

    const/4 v8, 0x5

    const-wide/16 v9, 0x12c

    move-object v11, v1

    invoke-virtual/range {v4 .. v11}, LX/5ub;->a(FLandroid/graphics/PointF;Landroid/graphics/PointF;IJLjava/lang/Runnable;)V

    .line 1918960
    return-void
.end method
