.class public final Lcom/facebook/photos/mediagallery/ui/tagging/TagView$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/CbA;


# direct methods
.method public constructor <init>(LX/CbA;)V
    .locals 0

    .prologue
    .line 1919161
    iput-object p1, p0, Lcom/facebook/photos/mediagallery/ui/tagging/TagView$1;->a:LX/CbA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x96

    .line 1919162
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/tagging/TagView$1;->a:LX/CbA;

    iget-object v0, v0, LX/CbA;->a:Lcom/facebook/photos/tagging/shared/BubbleLayout;

    invoke-virtual {v0}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/tagging/TagView$1;->a:LX/CbA;

    iget v1, v1, LX/CbA;->q:I

    add-int v5, v0, v1

    .line 1919163
    iget-object v6, p0, Lcom/facebook/photos/mediagallery/ui/tagging/TagView$1;->a:LX/CbA;

    new-instance v0, LX/8Hm;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/tagging/TagView$1;->a:LX/CbA;

    iget-object v1, v1, LX/CbA;->a:Lcom/facebook/photos/tagging/shared/BubbleLayout;

    sget-object v2, LX/8Hk;->WIDTH:LX/8Hk;

    sget-object v3, LX/8Hl;->EXPAND:LX/8Hl;

    iget-object v4, p0, Lcom/facebook/photos/mediagallery/ui/tagging/TagView$1;->a:LX/CbA;

    iget-object v4, v4, LX/CbA;->a:Lcom/facebook/photos/tagging/shared/BubbleLayout;

    invoke-virtual {v4}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->getWidth()I

    move-result v4

    invoke-direct/range {v0 .. v5}, LX/8Hm;-><init>(Landroid/view/View;LX/8Hk;LX/8Hl;II)V

    .line 1919164
    iput-object v0, v6, LX/CbA;->n:LX/8Hm;

    .line 1919165
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/tagging/TagView$1;->a:LX/CbA;

    iget-object v0, v0, LX/CbA;->n:LX/8Hm;

    invoke-virtual {v0, v8, v9}, LX/8Hm;->setDuration(J)V

    .line 1919166
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/tagging/TagView$1;->a:LX/CbA;

    iget-object v0, v0, LX/CbA;->n:LX/8Hm;

    new-instance v1, LX/Cb6;

    invoke-direct {v1, p0}, LX/Cb6;-><init>(Lcom/facebook/photos/mediagallery/ui/tagging/TagView$1;)V

    invoke-virtual {v0, v1}, LX/8Hm;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1919167
    iget-object v6, p0, Lcom/facebook/photos/mediagallery/ui/tagging/TagView$1;->a:LX/CbA;

    new-instance v0, LX/8Hm;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/tagging/TagView$1;->a:LX/CbA;

    iget-object v1, v1, LX/CbA;->a:Lcom/facebook/photos/tagging/shared/BubbleLayout;

    sget-object v2, LX/8Hk;->WIDTH:LX/8Hk;

    sget-object v3, LX/8Hl;->COLLAPSE:LX/8Hl;

    iget-object v4, p0, Lcom/facebook/photos/mediagallery/ui/tagging/TagView$1;->a:LX/CbA;

    iget-object v4, v4, LX/CbA;->a:Lcom/facebook/photos/tagging/shared/BubbleLayout;

    invoke-virtual {v4}, Lcom/facebook/photos/tagging/shared/BubbleLayout;->getWidth()I

    move-result v4

    invoke-direct/range {v0 .. v5}, LX/8Hm;-><init>(Landroid/view/View;LX/8Hk;LX/8Hl;II)V

    .line 1919168
    iput-object v0, v6, LX/CbA;->o:LX/8Hm;

    .line 1919169
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/tagging/TagView$1;->a:LX/CbA;

    iget-object v0, v0, LX/CbA;->o:LX/8Hm;

    invoke-virtual {v0, v8, v9}, LX/8Hm;->setDuration(J)V

    .line 1919170
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/tagging/TagView$1;->a:LX/CbA;

    iget-object v0, v0, LX/CbA;->o:LX/8Hm;

    new-instance v1, LX/Cb7;

    invoke-direct {v1, p0}, LX/Cb7;-><init>(Lcom/facebook/photos/mediagallery/ui/tagging/TagView$1;)V

    invoke-virtual {v0, v1}, LX/8Hm;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1919171
    return-void
.end method
