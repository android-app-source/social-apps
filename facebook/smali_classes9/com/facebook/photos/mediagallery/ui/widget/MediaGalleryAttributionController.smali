.class public Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryAttributionController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field


# instance fields
.field private final b:Lcom/facebook/resources/ui/FbTextView;

.field private final c:Lcom/facebook/resources/ui/FbTextView;

.field private final d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final e:LX/11S;

.field private final f:LX/9iB;

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1919677
    const-class v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryAttributionController;

    const-string v1, "photo_gallery"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryAttributionController;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/resources/ui/FbTextView;Lcom/facebook/resources/ui/FbTextView;Lcom/facebook/drawee/fbpipeline/FbDraweeView;LX/11S;LX/9iB;LX/0Or;)V
    .locals 0
    .param p1    # Lcom/facebook/resources/ui/FbTextView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/resources/ui/FbTextView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/drawee/fbpipeline/FbDraweeView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/resources/ui/FbTextView;",
            "Lcom/facebook/resources/ui/FbTextView;",
            "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
            "LX/11S;",
            "LX/9iB;",
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1919678
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1919679
    iput-object p1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryAttributionController;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1919680
    iput-object p2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryAttributionController;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 1919681
    iput-object p3, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryAttributionController;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1919682
    iput-object p4, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryAttributionController;->e:LX/11S;

    .line 1919683
    iput-object p5, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryAttributionController;->f:LX/9iB;

    .line 1919684
    iput-object p6, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryAttributionController;->g:LX/0Or;

    .line 1919685
    return-void
.end method

.method private static a(LX/5kD;)Ljava/lang/String;
    .locals 2
    .param p0    # LX/5kD;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1919686
    if-nez p0, :cond_1

    .line 1919687
    :cond_0
    :goto_0
    return-object v0

    .line 1919688
    :cond_1
    invoke-interface {p0}, LX/5kD;->Z()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;

    move-result-object v1

    .line 1919689
    if-eqz v1, :cond_0

    .line 1919690
    invoke-static {v1}, LX/5k9;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-static {v0}, LX/1xl;->a(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/5kD;LX/5kD;)V
    .locals 10
    .param p1    # LX/5kD;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1919691
    if-nez p1, :cond_7

    const/4 v0, 0x1

    .line 1919692
    :goto_0
    invoke-static {p1}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryAttributionController;->a(LX/5kD;)Ljava/lang/String;

    move-result-object v2

    .line 1919693
    invoke-static {p2}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryAttributionController;->a(LX/5kD;)Ljava/lang/String;

    move-result-object v3

    .line 1919694
    if-nez v0, :cond_0

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1919695
    :cond_0
    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryAttributionController;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1919696
    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryAttributionController;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1919697
    if-eqz p2, :cond_1

    invoke-interface {p2}, LX/5kD;->Z()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;

    move-result-object v3

    if-nez v3, :cond_a

    .line 1919698
    :cond_1
    const/4 v3, 0x0

    .line 1919699
    :goto_1
    move-object v3, v3

    .line 1919700
    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1919701
    :cond_2
    if-nez v0, :cond_3

    invoke-interface {p1}, LX/5kD;->B()J

    move-result-wide v2

    invoke-interface {p2}, LX/5kD;->B()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    .line 1919702
    :cond_3
    iget-object v3, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryAttributionController;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-interface {p2}, LX/5kD;->B()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-lez v2, :cond_8

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryAttributionController;->e:LX/11S;

    sget-object v4, LX/1lB;->STREAM_RELATIVE_STYLE:LX/1lB;

    invoke-interface {p2}, LX/5kD;->B()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    invoke-interface {v2, v4, v6, v7}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    :goto_2
    invoke-virtual {v3, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1919703
    :cond_4
    invoke-static {p1}, LX/9iB;->b(LX/5kD;)Ljava/lang/String;

    move-result-object v2

    .line 1919704
    invoke-static {p2}, LX/9iB;->b(LX/5kD;)Ljava/lang/String;

    move-result-object v3

    .line 1919705
    if-nez v0, :cond_5

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1919706
    :cond_5
    if-nez v3, :cond_9

    .line 1919707
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryAttributionController;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    sget-object v2, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryAttributionController;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1919708
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryAttributionController;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1919709
    :cond_6
    :goto_3
    return-void

    :cond_7
    move v0, v1

    .line 1919710
    goto :goto_0

    .line 1919711
    :cond_8
    const-string v2, ""

    goto :goto_2

    .line 1919712
    :cond_9
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryAttributionController;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryAttributionController;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1919713
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryAttributionController;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_3

    :cond_a
    new-instance v3, LX/Cbc;

    invoke-direct {v3, p0, p2}, LX/Cbc;-><init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryAttributionController;LX/5kD;)V

    goto :goto_1
.end method
