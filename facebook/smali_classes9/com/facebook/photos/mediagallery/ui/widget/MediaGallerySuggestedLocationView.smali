.class public Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private c:Lcom/facebook/widget/text/BetterTextView;

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;

.field private f:Landroid/widget/ImageView;

.field private g:Landroid/view/View;

.field private h:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1921212
    const-class v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1921209
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1921210
    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;->a()V

    .line 1921211
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1921206
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1921207
    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;->a()V

    .line 1921208
    return-void
.end method

.method private final a()V
    .locals 2

    .prologue
    .line 1921198
    const v0, 0x7f030a8e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1921199
    const v0, 0x7f0d1afa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1921200
    const v0, 0x7f0d1afb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 1921201
    const v0, 0x7f0d1af9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;->d:Landroid/view/View;

    .line 1921202
    const v0, 0x7f0d1afc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;->e:Landroid/view/View;

    .line 1921203
    const v0, 0x7f0d1af8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;->f:Landroid/widget/ImageView;

    .line 1921204
    invoke-virtual {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b187b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;->h:I

    .line 1921205
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;)V
    .locals 6

    .prologue
    .line 1921194
    invoke-virtual {p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;->a()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;

    move-result-object v0

    .line 1921195
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;->e()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel$PlaceProfilePictureModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel$PlaceProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1921196
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0823ca

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PlaceInfoModel;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1921197
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1921177
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomLinearLayout;->onLayout(ZIIII)V

    .line 1921178
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;->g:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1921179
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 1921180
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;->g:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1921181
    aget v0, v0, v4

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;->g:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 1921182
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;->f:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int v1, v0, v1

    iget v2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;->h:I

    sub-int/2addr v1, v2

    .line 1921183
    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;->f:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    iget v2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;->h:I

    sub-int/2addr v0, v2

    .line 1921184
    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;->f:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getHeight()I

    move-result v2

    .line 1921185
    iget-object v3, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;->f:Landroid/widget/ImageView;

    invoke-virtual {v3, v1, v4, v0, v2}, Landroid/widget/ImageView;->layout(IIII)V

    .line 1921186
    :cond_0
    return-void
.end method

.method public setAcceptSuggestedLocationListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1921192
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;->d:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1921193
    return-void
.end method

.method public setAnchor(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1921189
    iput-object p1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;->g:Landroid/view/View;

    .line 1921190
    invoke-virtual {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;->requestLayout()V

    .line 1921191
    return-void
.end method

.method public setRejectSuggestedLocationListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1921187
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;->e:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1921188
    return-void
.end method
