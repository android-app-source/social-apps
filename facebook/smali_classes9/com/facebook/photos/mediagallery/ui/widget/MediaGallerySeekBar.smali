.class public Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/CcX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:Landroid/widget/SeekBar;

.field private final c:Lcom/facebook/resources/ui/FbTextView;

.field private final d:Lcom/facebook/resources/ui/FbTextView;

.field private final e:Lcom/facebook/resources/ui/FbTextView;

.field private final f:I

.field private final g:I

.field public h:Ljava/lang/String;

.field public i:Z

.field private j:I

.field private k:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1921143
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1921144
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1921141
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1921142
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 1921128
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1921129
    const-class v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;

    invoke-static {v0, p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1921130
    invoke-virtual {p0, v2}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->setOrientation(I)V

    .line 1921131
    const v0, 0x7f030a92

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1921132
    const v0, 0x7f0d1b08

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->b:Landroid/widget/SeekBar;

    .line 1921133
    const v0, 0x7f0d1b07

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 1921134
    const v0, 0x7f0d1b09

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 1921135
    const v0, 0x7f0d0908

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 1921136
    invoke-virtual {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a004b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->f:I

    .line 1921137
    invoke-virtual {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a03ce

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->g:I

    .line 1921138
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->b:Landroid/widget/SeekBar;

    new-instance v1, LX/CcR;

    invoke-direct {v1, p0}, LX/CcR;-><init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 1921139
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->e:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, LX/CcQ;

    invoke-direct {v1, p0}, LX/CcQ;-><init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1921140
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;

    invoke-static {v0}, LX/CcX;->a(LX/0QB;)LX/CcX;

    move-result-object v0

    check-cast v0, LX/CcX;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->a:LX/CcX;

    return-void
.end method


# virtual methods
.method public final a(IILjava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1921119
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1921120
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1921121
    iget v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->j:I

    if-eq v0, p2, :cond_0

    .line 1921122
    iput p2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->j:I

    .line 1921123
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->b:Landroid/widget/SeekBar;

    invoke-virtual {v0, p2}, Landroid/widget/SeekBar;->setMax(I)V

    .line 1921124
    :cond_0
    iget v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->k:I

    if-eq v0, p1, :cond_1

    .line 1921125
    iput p1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->k:I

    .line 1921126
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->b:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1921127
    :cond_1
    return-void
.end method

.method public final a(ZZ)V
    .locals 2

    .prologue
    .line 1921112
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->e:Lcom/facebook/resources/ui/FbTextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1921113
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->e:Lcom/facebook/resources/ui/FbTextView;

    if-eqz p2, :cond_1

    iget v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->f:I

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1921114
    return-void

    .line 1921115
    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    .line 1921116
    :cond_1
    iget v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->g:I

    goto :goto_1
.end method

.method public setMediaId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1921117
    iput-object p1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->h:Ljava/lang/String;

    .line 1921118
    return-void
.end method
