.class public Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;
.super Landroid/widget/FrameLayout;
.source ""

# interfaces
.implements LX/9hC;


# static fields
.field private static final P:Ljava/lang/String;

.field private static final Q:I

.field private static final R:I

.field private static final S:I


# instance fields
.field public A:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/1Wo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/20r;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/20s;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public H:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public I:LX/9iA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public J:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1VD;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public K:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CcV;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public L:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/B9y;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public M:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public N:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public O:LX/0W3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private T:Lcom/facebook/base/fragment/FbFragment;

.field private U:Landroid/view/View;

.field private V:Lcom/facebook/components/ComponentView;

.field public W:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

.field public a:LX/3iG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private aA:Z

.field private aB:Z

.field public aC:Z

.field private aD:LX/Cay;

.field public aE:LX/74S;

.field public aF:LX/745;

.field private aG:LX/8z9;

.field private aH:Landroid/view/View$OnClickListener;

.field private aI:LX/3iK;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aJ:LX/20Z;

.field private aK:LX/21M;

.field private aL:LX/20z;

.field private aM:LX/21T;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aN:LX/0wd;

.field public aO:Z

.field private aP:Z

.field public aQ:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aR:LX/9hK;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aS:Z

.field private aa:Lcom/facebook/fbui/glyph/GlyphView;

.field private ab:Lcom/facebook/fbui/glyph/GlyphView;

.field public ac:Lcom/facebook/fbui/glyph/GlyphView;

.field private ad:Landroid/widget/ImageView;

.field private ae:Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;

.field private af:Landroid/view/View;

.field private ag:Lcom/facebook/resources/ui/FbTextView;

.field private ah:Lcom/facebook/composer/metatext/TagsTextViewContainer;

.field private ai:Lcom/facebook/resources/ui/FbTextView;

.field private aj:Landroid/widget/LinearLayout;

.field private ak:Lcom/facebook/resources/ui/FbTextView;

.field private al:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private am:Landroid/view/View;

.field private an:Lcom/facebook/components/ComponentView;

.field private ao:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

.field private ap:Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;

.field private aq:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryPlayButton;

.field private ar:LX/1De;

.field private as:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryAttributionController;

.field public at:LX/5kD;

.field public au:LX/21D;

.field public av:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aw:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ax:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ay:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private az:LX/162;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nI;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Cbd;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/1EP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/1nK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/CbH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/459;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/8tu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Or;
    .annotation runtime Lcom/facebook/photos/mediagallery/IsBillingEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0jU;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/1Kf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/215;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0wW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/9hh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/9iE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0iA;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/15W;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/39G;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/1zf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/20h;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/20w;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1920332
    const-class v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->P:Ljava/lang/String;

    .line 1920333
    const v0, 0x7f020740

    sput v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->Q:I

    .line 1920334
    const v0, 0x7f0209f0

    sput v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->R:I

    .line 1920335
    const v0, 0x7f020966

    sput v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->S:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1920325
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 1920326
    iput-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aA:Z

    .line 1920327
    iput-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aB:Z

    .line 1920328
    iput-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aO:Z

    .line 1920329
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aP:Z

    .line 1920330
    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->f()V

    .line 1920331
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1920318
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1920319
    iput-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aA:Z

    .line 1920320
    iput-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aB:Z

    .line 1920321
    iput-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aO:Z

    .line 1920322
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aP:Z

    .line 1920323
    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->f()V

    .line 1920324
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1920311
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1920312
    iput-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aA:Z

    .line 1920313
    iput-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aB:Z

    .line 1920314
    iput-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aO:Z

    .line 1920315
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aP:Z

    .line 1920316
    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->f()V

    .line 1920317
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/20z;
    .locals 4

    .prologue
    .line 1920304
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aL:LX/20z;

    if-eqz v0, :cond_0

    .line 1920305
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aL:LX/20z;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v1

    .line 1920306
    iput-object v1, v0, LX/20z;->g:Ljava/lang/String;

    .line 1920307
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aL:LX/20z;

    .line 1920308
    :goto_0
    return-object v0

    .line 1920309
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->z:LX/20w;

    invoke-virtual {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->getStoryId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v2

    const-string v3, "photo_gallery"

    invoke-virtual {v0, v1, v2, v3}, LX/20w;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/20z;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aL:LX/20z;

    .line 1920310
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aL:LX/20z;

    goto :goto_0
.end method

.method private a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1920303
    new-instance v0, LX/Cbg;

    invoke-direct {v0, p0, p1}, LX/Cbg;-><init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;)V

    return-object v0
.end method

.method private a(LX/1zt;)V
    .locals 3

    .prologue
    .line 1920300
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aK:LX/21M;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ao:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    const/4 v2, 0x0

    invoke-interface {v0, v1, p1, v2}, LX/21M;->a(Landroid/view/View;LX/1zt;LX/0Ve;)V

    .line 1920301
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ao:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    invoke-virtual {v0, p1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->setReaction(LX/1zt;)V

    .line 1920302
    return-void
.end method

.method private a(LX/5kD;)V
    .locals 13

    .prologue
    const/4 v10, 0x1

    .line 1920276
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->h:LX/CbH;

    .line 1920277
    iget-boolean v1, v0, LX/CbH;->g:Z

    move v0, v1

    .line 1920278
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->h:LX/CbH;

    .line 1920279
    iget-boolean v1, v0, LX/CbH;->c:Z

    move v0, v1

    .line 1920280
    if-eqz v0, :cond_0

    .line 1920281
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->h:LX/CbH;

    .line 1920282
    iget-boolean v1, v0, LX/CbH;->g:Z

    move v0, v1

    .line 1920283
    invoke-static {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->setTaggingMode(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;Z)V

    .line 1920284
    :cond_0
    invoke-static {p0, p1}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->j(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;LX/5kD;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 1920285
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->r_()Z

    move-result v11

    .line 1920286
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->c()Z

    move-result v12

    .line 1920287
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aQ:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aQ:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/214;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    move v9, v0

    .line 1920288
    :goto_0
    new-instance v0, Ljava/util/EnumMap;

    const-class v2, LX/20X;

    invoke-direct {v0, v2}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    .line 1920289
    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->o:LX/0Or;

    invoke-static {v11, v12, v9, v0, v2}, LX/212;->a(ZZZLjava/util/EnumMap;LX/0Or;)V

    .line 1920290
    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ao:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    iget-object v3, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aJ:LX/20Z;

    invoke-static {v2, v0, v1, v3}, LX/212;->a(LX/1wK;Ljava/util/EnumMap;Lcom/facebook/graphql/model/GraphQLFeedback;LX/20Z;)V

    .line 1920291
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ao:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aK:LX/21M;

    iget-object v3, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aN:LX/0wd;

    invoke-direct {p0, v1}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/20z;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->x:LX/1zf;

    sget-object v6, LX/20I;->DARK:LX/20I;

    iget-object v7, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->x:LX/1zf;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->M()LX/0Px;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/1zf;->a(Ljava/util/List;)LX/0Px;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, LX/21N;->a(Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;Lcom/facebook/graphql/model/GraphQLFeedback;LX/21M;LX/0wd;LX/20z;LX/1zf;LX/20I;LX/0Px;LX/21H;)V

    .line 1920292
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aM:LX/21T;

    invoke-virtual {v0, v11, v12, v9}, LX/21T;->a(ZZZ)LX/21Y;

    move-result-object v0

    .line 1920293
    sget-object v1, LX/21c;->ICONS_EQUAL_WIDTH:LX/21c;

    .line 1920294
    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ao:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    invoke-virtual {v0, v1}, LX/21Y;->a(LX/21c;)[F

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->setButtonWeights([F)V

    .line 1920295
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ao:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    invoke-virtual {v0, v10}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->setShowIcons(Z)V

    .line 1920296
    if-nez v11, :cond_1

    if-nez v12, :cond_1

    if-eqz v9, :cond_3

    :cond_1
    move v0, v10

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aS:Z

    .line 1920297
    return-void

    .line 1920298
    :cond_2
    invoke-direct {p0, p1}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->g(LX/5kD;)Z

    move-result v0

    move v9, v0

    goto :goto_0

    .line 1920299
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static a(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;LX/3iG;LX/0Ot;LX/0Ot;LX/Cbd;LX/0Zb;LX/1EP;LX/1nK;LX/CbH;LX/459;LX/8tu;LX/0Or;LX/0Ot;LX/0Ot;LX/1Kf;LX/0Or;LX/0wW;LX/0Ot;LX/0Ot;LX/9hh;LX/9iE;LX/0Ot;LX/0Ot;LX/39G;LX/1zf;LX/20h;LX/20w;LX/0if;LX/0Ot;LX/1Wo;LX/20r;LX/20s;LX/0Ot;LX/0Uh;LX/0Ot;LX/9iA;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0W3;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;",
            "LX/3iG;",
            "LX/0Ot",
            "<",
            "LX/1nI;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;",
            ">;",
            "LX/Cbd;",
            "LX/0Zb;",
            "LX/1EP;",
            "LX/1nK;",
            "LX/CbH;",
            "LX/459;",
            "LX/8tu;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0jU;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/1Kf;",
            "LX/0Or",
            "<",
            "LX/215;",
            ">;",
            "LX/0wW;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/9hh;",
            "LX/9iE;",
            "LX/0Ot",
            "<",
            "LX/0iA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/15W;",
            ">;",
            "LX/39G;",
            "LX/1zf;",
            "LX/20h;",
            "LX/20w;",
            "Lcom/facebook/funnellogger/FunnelLogger;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;",
            ">;",
            "LX/1Wo;",
            "LX/20r;",
            "LX/20s;",
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;",
            "LX/9iA;",
            "LX/0Ot",
            "<",
            "LX/1VD;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CcV;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/B9y;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1920075
    iput-object p1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->a:LX/3iG;

    iput-object p2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->b:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->c:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->d:LX/Cbd;

    iput-object p5, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->e:LX/0Zb;

    iput-object p6, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->f:LX/1EP;

    iput-object p7, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->g:LX/1nK;

    iput-object p8, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->h:LX/CbH;

    iput-object p9, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->i:LX/459;

    iput-object p10, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->j:LX/8tu;

    iput-object p11, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->k:LX/0Or;

    iput-object p12, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->l:LX/0Ot;

    iput-object p13, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->m:LX/0Ot;

    iput-object p14, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->n:LX/1Kf;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->o:LX/0Or;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->p:LX/0wW;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->q:LX/0Ot;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->r:LX/0Ot;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->s:LX/9hh;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->t:LX/9iE;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->u:LX/0Ot;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->v:LX/0Ot;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->w:LX/39G;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->x:LX/1zf;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->y:LX/20h;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->z:LX/20w;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->A:LX/0if;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->B:LX/0Ot;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->C:LX/1Wo;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->D:LX/20r;

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->E:LX/20s;

    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->F:LX/0Ot;

    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->G:LX/0Uh;

    move-object/from16 v0, p34

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->H:LX/0Ot;

    move-object/from16 v0, p35

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->I:LX/9iA;

    move-object/from16 v0, p36

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->J:LX/0Ot;

    move-object/from16 v0, p37

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->K:LX/0Ot;

    move-object/from16 v0, p38

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->L:LX/0Ot;

    move-object/from16 v0, p39

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->M:LX/0Ot;

    move-object/from16 v0, p40

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->N:LX/0Ot;

    move-object/from16 v0, p41

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->O:LX/0W3;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 44

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v43

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    const-class v3, LX/3iG;

    move-object/from16 v0, v43

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/3iG;

    const/16 v4, 0x7bc

    move-object/from16 v0, v43

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2e7c

    move-object/from16 v0, v43

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const-class v6, LX/Cbd;

    move-object/from16 v0, v43

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/Cbd;

    invoke-static/range {v43 .. v43}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    invoke-static/range {v43 .. v43}, LX/1EP;->a(LX/0QB;)LX/1EP;

    move-result-object v8

    check-cast v8, LX/1EP;

    invoke-static/range {v43 .. v43}, LX/1nK;->a(LX/0QB;)LX/1nK;

    move-result-object v9

    check-cast v9, LX/1nK;

    invoke-static/range {v43 .. v43}, LX/CbH;->a(LX/0QB;)LX/CbH;

    move-result-object v10

    check-cast v10, LX/CbH;

    invoke-static/range {v43 .. v43}, LX/459;->a(LX/0QB;)LX/459;

    move-result-object v11

    check-cast v11, LX/459;

    invoke-static/range {v43 .. v43}, LX/8tu;->a(LX/0QB;)LX/8tu;

    move-result-object v12

    check-cast v12, LX/8tu;

    const/16 v13, 0x154c

    move-object/from16 v0, v43

    invoke-static {v0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    const/16 v14, 0xfc

    move-object/from16 v0, v43

    invoke-static {v0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0xbc6

    move-object/from16 v0, v43

    invoke-static {v0, v15}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v15

    invoke-static/range {v43 .. v43}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v16

    check-cast v16, LX/1Kf;

    const/16 v17, 0x13a4

    move-object/from16 v0, v43

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v17

    invoke-static/range {v43 .. v43}, LX/0wW;->a(LX/0QB;)LX/0wW;

    move-result-object v18

    check-cast v18, LX/0wW;

    const/16 v19, 0x97

    move-object/from16 v0, v43

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v19

    const/16 v20, 0x259

    move-object/from16 v0, v43

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v20

    invoke-static/range {v43 .. v43}, LX/9hh;->a(LX/0QB;)LX/9hh;

    move-result-object v21

    check-cast v21, LX/9hh;

    invoke-static/range {v43 .. v43}, LX/9iE;->a(LX/0QB;)LX/9iE;

    move-result-object v22

    check-cast v22, LX/9iE;

    const/16 v23, 0xbd2

    move-object/from16 v0, v43

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v23

    const/16 v24, 0xbca

    move-object/from16 v0, v43

    move/from16 v1, v24

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v24

    invoke-static/range {v43 .. v43}, LX/39G;->a(LX/0QB;)LX/39G;

    move-result-object v25

    check-cast v25, LX/39G;

    invoke-static/range {v43 .. v43}, LX/1zf;->a(LX/0QB;)LX/1zf;

    move-result-object v26

    check-cast v26, LX/1zf;

    invoke-static/range {v43 .. v43}, LX/20h;->a(LX/0QB;)LX/20h;

    move-result-object v27

    check-cast v27, LX/20h;

    const-class v28, LX/20w;

    move-object/from16 v0, v43

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v28

    check-cast v28, LX/20w;

    invoke-static/range {v43 .. v43}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v29

    check-cast v29, LX/0if;

    const/16 v30, 0x2e79

    move-object/from16 v0, v43

    move/from16 v1, v30

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v30

    const-class v31, LX/1Wo;

    move-object/from16 v0, v43

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v31

    check-cast v31, LX/1Wo;

    invoke-static/range {v43 .. v43}, LX/20r;->a(LX/0QB;)LX/20r;

    move-result-object v32

    check-cast v32, LX/20r;

    invoke-static/range {v43 .. v43}, LX/20s;->a(LX/0QB;)LX/20s;

    move-result-object v33

    check-cast v33, LX/20s;

    const/16 v34, 0x2ba

    move-object/from16 v0, v43

    move/from16 v1, v34

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v34

    invoke-static/range {v43 .. v43}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v35

    check-cast v35, LX/0Uh;

    const/16 v36, 0x3567

    move-object/from16 v0, v43

    move/from16 v1, v36

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v36

    invoke-static/range {v43 .. v43}, LX/9iA;->a(LX/0QB;)LX/9iA;

    move-result-object v37

    check-cast v37, LX/9iA;

    const/16 v38, 0x71f

    move-object/from16 v0, v43

    move/from16 v1, v38

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v38

    const/16 v39, 0x2e7e

    move-object/from16 v0, v43

    move/from16 v1, v39

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v39

    const/16 v40, 0x2978

    move-object/from16 v0, v43

    move/from16 v1, v40

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v40

    const/16 v41, 0xc49

    move-object/from16 v0, v43

    move/from16 v1, v41

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v41

    const/16 v42, 0x455

    move-object/from16 v0, v43

    move/from16 v1, v42

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v42

    invoke-static/range {v43 .. v43}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v43

    check-cast v43, LX/0W3;

    invoke-static/range {v2 .. v43}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->a(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;LX/3iG;LX/0Ot;LX/0Ot;LX/Cbd;LX/0Zb;LX/1EP;LX/1nK;LX/CbH;LX/459;LX/8tu;LX/0Or;LX/0Ot;LX/0Ot;LX/1Kf;LX/0Or;LX/0wW;LX/0Ot;LX/0Ot;LX/9hh;LX/9iE;LX/0Ot;LX/0Ot;LX/39G;LX/1zf;LX/20h;LX/20w;LX/0if;LX/0Ot;LX/1Wo;LX/20r;LX/20s;LX/0Ot;LX/0Uh;LX/0Ot;LX/9iA;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0W3;)V

    return-void
.end method

.method private b(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1920257
    new-instance v0, LX/Cbh;

    invoke-direct {v0, p0, p1}, LX/Cbh;-><init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;)V

    return-object v0
.end method

.method private b(LX/5kD;)V
    .locals 6

    .prologue
    .line 1920219
    invoke-static {p0, p1}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->j(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;LX/5kD;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 1920220
    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->getComponentContext()LX/1De;

    move-result-object v2

    .line 1920221
    const/4 v0, 0x0

    .line 1920222
    new-instance v3, LX/CEC;

    invoke-direct {v3}, LX/CEC;-><init>()V

    .line 1920223
    sget-object v4, LX/CED;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/CEB;

    .line 1920224
    if-nez v4, :cond_0

    .line 1920225
    new-instance v4, LX/CEB;

    invoke-direct {v4}, LX/CEB;-><init>()V

    .line 1920226
    :cond_0
    invoke-static {v4, v2, v0, v0, v3}, LX/CEB;->a$redex0(LX/CEB;LX/1De;IILX/CEC;)V

    .line 1920227
    move-object v3, v4

    .line 1920228
    move-object v0, v3

    .line 1920229
    move-object v3, v0

    .line 1920230
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->K:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CcV;

    const/4 v4, 0x0

    .line 1920231
    new-instance v5, LX/CcU;

    invoke-direct {v5, v0}, LX/CcU;-><init>(LX/CcV;)V

    .line 1920232
    sget-object p1, LX/CcV;->a:LX/0Zi;

    invoke-virtual {p1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/CcT;

    .line 1920233
    if-nez p1, :cond_1

    .line 1920234
    new-instance p1, LX/CcT;

    invoke-direct {p1}, LX/CcT;-><init>()V

    .line 1920235
    :cond_1
    invoke-static {p1, v2, v4, v4, v5}, LX/CcT;->a$redex0(LX/CcT;LX/1De;IILX/CcU;)V

    .line 1920236
    move-object v5, p1

    .line 1920237
    move-object v4, v5

    .line 1920238
    move-object v0, v4

    .line 1920239
    new-instance v4, LX/23u;

    invoke-direct {v4}, LX/23u;-><init>()V

    .line 1920240
    iput-object v1, v4, LX/23u;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1920241
    move-object v1, v4

    .line 1920242
    invoke-virtual {v1}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 1920243
    iget-object v4, v0, LX/CcT;->a:LX/CcU;

    iput-object v1, v4, LX/CcU;->a:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1920244
    iget-object v4, v0, LX/CcT;->d:Ljava/util/BitSet;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 1920245
    move-object v0, v0

    .line 1920246
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 1920247
    iget-object v1, v3, LX/CEB;->a:LX/CEC;

    iput-object v0, v1, LX/CEC;->a:LX/1X1;

    .line 1920248
    iget-object v1, v3, LX/CEB;->d:Ljava/util/BitSet;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Ljava/util/BitSet;->set(I)V

    .line 1920249
    move-object v0, v3

    .line 1920250
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aH:Landroid/view/View$OnClickListener;

    .line 1920251
    iget-object v3, v0, LX/CEB;->a:LX/CEC;

    iput-object v1, v3, LX/CEC;->b:Landroid/view/View$OnClickListener;

    .line 1920252
    iget-object v3, v0, LX/CEB;->d:Ljava/util/BitSet;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 1920253
    move-object v0, v0

    .line 1920254
    invoke-static {v2, v0}, LX/1dV;->a(LX/1De;LX/1X5;)LX/1me;

    move-result-object v0

    invoke-virtual {v0}, LX/1me;->b()LX/1dV;

    move-result-object v0

    .line 1920255
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->an:Lcom/facebook/components/ComponentView;

    invoke-virtual {v1, v0}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 1920256
    return-void
.end method

.method private c(LX/5kD;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/16 v1, 0x8

    .line 1920159
    iget-boolean v2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aP:Z

    if-nez v2, :cond_0

    .line 1920160
    :goto_0
    return-void

    .line 1920161
    :cond_0
    new-instance v2, LX/8zC;

    invoke-direct {v2}, LX/8zC;-><init>()V

    iget-object v3, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->I:LX/9iA;

    invoke-virtual {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->getContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aQ:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3, p1, v4, v5, v6}, LX/9iA;->a(LX/5kD;Landroid/content/Context;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    .line 1920162
    iput-object v3, v2, LX/8zC;->a:Ljava/lang/CharSequence;

    .line 1920163
    move-object v2, v2

    .line 1920164
    iget-object v3, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aQ:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v3, :cond_1

    .line 1920165
    iget-object v3, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aQ:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1920166
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->ai()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v4

    if-eqz v4, :cond_8

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->ai()Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a(Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;)Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v4

    :goto_1
    move-object v3, v4

    .line 1920167
    iput-object v3, v2, LX/8zC;->b:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1920168
    iget-object v3, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aQ:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v3}, LX/9iA;->c(Lcom/facebook/graphql/model/GraphQLStory;)LX/0Px;

    move-result-object v3

    .line 1920169
    iput-object v3, v2, LX/8zC;->c:LX/0Px;

    .line 1920170
    iget-object v3, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aQ:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1920171
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->X()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v4

    .line 1920172
    if-eqz v4, :cond_9

    new-instance v5, LX/5m9;

    invoke-direct {v5}, LX/5m9;-><init>()V

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPlace;->w()Ljava/lang/String;

    move-result-object v6

    .line 1920173
    iput-object v6, v5, LX/5m9;->f:Ljava/lang/String;

    .line 1920174
    move-object v5, v5

    .line 1920175
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPlace;->A()Ljava/lang/String;

    move-result-object v4

    .line 1920176
    iput-object v4, v5, LX/5m9;->h:Ljava/lang/String;

    .line 1920177
    move-object v4, v5

    .line 1920178
    invoke-virtual {v4}, LX/5m9;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v4

    :goto_2
    move-object v3, v4

    .line 1920179
    iput-object v3, v2, LX/8zC;->d:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1920180
    :goto_3
    invoke-virtual {v2}, LX/8zC;->a()LX/8zD;

    move-result-object v2

    .line 1920181
    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->r()V

    .line 1920182
    invoke-interface {p1}, LX/5kD;->S()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1920183
    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ah:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    invoke-virtual {v2, v1}, Lcom/facebook/composer/metatext/TagsTextViewContainer;->setVisibility(I)V

    .line 1920184
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ap:Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;

    invoke-interface {p1}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v2

    .line 1920185
    iput-object v2, v1, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->h:Ljava/lang/String;

    .line 1920186
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ap:Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;

    invoke-virtual {v1, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->setVisibility(I)V

    .line 1920187
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aq:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryPlayButton;

    invoke-interface {p1}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v2

    .line 1920188
    iput-object v2, v1, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryPlayButton;->b:Ljava/lang/String;

    .line 1920189
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aq:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryPlayButton;

    invoke-virtual {v1, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryPlayButton;->setVisibility(I)V

    .line 1920190
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->setVideoControlAlpha(F)V

    goto/16 :goto_0

    .line 1920191
    :cond_1
    invoke-interface {p1}, LX/5kD;->Q()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataInlineActivitiesModel;

    move-result-object v3

    if-eqz v3, :cond_a

    invoke-interface {p1}, LX/5kD;->Q()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataInlineActivitiesModel;

    move-result-object v3

    invoke-static {v3}, LX/5k9;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataInlineActivitiesModel;)Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a(Lcom/facebook/graphql/model/GraphQLInlineActivitiesConnection;)Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v3

    :goto_4
    move-object v3, v3

    .line 1920192
    iput-object v3, v2, LX/8zC;->b:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1920193
    iget-object v3, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->I:LX/9iA;

    invoke-virtual {v3, p1}, LX/9iA;->d(LX/5kD;)LX/0Px;

    move-result-object v3

    .line 1920194
    iput-object v3, v2, LX/8zC;->c:LX/0Px;

    .line 1920195
    invoke-interface {p1}, LX/5kD;->E()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    move-result-object v3

    .line 1920196
    if-eqz v3, :cond_b

    new-instance v4, LX/5m9;

    invoke-direct {v4}, LX/5m9;-><init>()V

    invoke-virtual {v3}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;->c()Ljava/lang/String;

    move-result-object v5

    .line 1920197
    iput-object v5, v4, LX/5m9;->f:Ljava/lang/String;

    .line 1920198
    move-object v4, v4

    .line 1920199
    invoke-virtual {v3}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;->d()Ljava/lang/String;

    move-result-object v3

    .line 1920200
    iput-object v3, v4, LX/5m9;->h:Ljava/lang/String;

    .line 1920201
    move-object v3, v4

    .line 1920202
    invoke-virtual {v3}, LX/5m9;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v3

    :goto_5
    move-object v3, v3

    .line 1920203
    iput-object v3, v2, LX/8zC;->d:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1920204
    goto :goto_3

    .line 1920205
    :cond_2
    iget-object v3, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ah:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    iget-object v4, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aG:LX/8z9;

    iget-boolean v5, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aB:Z

    invoke-virtual {v3, v2, v4, v5}, Lcom/facebook/composer/metatext/TagsTextViewContainer;->a(LX/8zD;LX/8z9;Z)V

    .line 1920206
    iget-object v3, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ah:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    iget-object v4, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->j:LX/8tu;

    invoke-virtual {v3, v4}, Lcom/facebook/composer/metatext/TagsTextViewContainer;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1920207
    iget-object v3, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ah:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    invoke-static {p1}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->f(LX/5kD;)Ljava/lang/String;

    move-result-object v4

    .line 1920208
    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1920209
    iget-object v5, v2, LX/8zD;->d:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    if-nez v5, :cond_d

    iget-object v5, v2, LX/8zD;->b:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    if-nez v5, :cond_d

    iget-object v5, v2, LX/8zD;->c:LX/0Px;

    if-eqz v5, :cond_3

    iget-object v5, v2, LX/8zD;->c:LX/0Px;

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_d

    :cond_3
    iget-object v5, v2, LX/8zD;->a:Ljava/lang/CharSequence;

    if-eqz v5, :cond_4

    iget-object v5, v2, LX/8zD;->a:Ljava/lang/CharSequence;

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-nez v5, :cond_d

    :cond_4
    const/4 v5, 0x1

    :goto_6
    move v5, v5

    .line 1920210
    if-nez v5, :cond_c

    :cond_5
    const/4 v5, 0x1

    :goto_7
    move v2, v5

    .line 1920211
    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->h:LX/CbH;

    .line 1920212
    iget-boolean v4, v2, LX/CbH;->c:Z

    move v2, v4

    .line 1920213
    if-nez v2, :cond_7

    :goto_8
    invoke-virtual {v3, v0}, Lcom/facebook/composer/metatext/TagsTextViewContainer;->setVisibility(I)V

    .line 1920214
    iget-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aB:Z

    if-eqz v0, :cond_6

    .line 1920215
    invoke-static {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->q(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;)V

    .line 1920216
    :cond_6
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aq:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryPlayButton;

    invoke-virtual {v0, v1}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryPlayButton;->setVisibility(I)V

    .line 1920217
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ap:Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;

    invoke-virtual {v0, v1}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->setVisibility(I)V

    goto/16 :goto_0

    :cond_7
    move v0, v1

    .line 1920218
    goto :goto_8

    :cond_8
    const/4 v4, 0x0

    goto/16 :goto_1

    :cond_9
    const/4 v4, 0x0

    goto/16 :goto_2

    :cond_a
    const/4 v3, 0x0

    goto/16 :goto_4

    :cond_b
    const/4 v3, 0x0

    goto :goto_5

    :cond_c
    const/4 v5, 0x0

    goto :goto_7

    :cond_d
    const/4 v5, 0x0

    goto :goto_6
.end method

.method private d(LX/5kD;)V
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    const/16 v8, 0x8

    .line 1920111
    iget-boolean v3, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aO:Z

    if-eqz v3, :cond_1

    .line 1920112
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->W:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v0, v8}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setVisibility(I)V

    .line 1920113
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aa:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v8}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1920114
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ab:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v8}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1920115
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ac:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v8}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1920116
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ae:Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;

    if-eqz v0, :cond_0

    .line 1920117
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ae:Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;

    invoke-virtual {v0, v8}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;->setVisibility(I)V

    .line 1920118
    :cond_0
    :goto_0
    return-void

    .line 1920119
    :cond_1
    iget-object v3, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    if-nez v3, :cond_f

    move v3, v0

    .line 1920120
    :goto_1
    if-eqz v3, :cond_2

    .line 1920121
    iget-object v4, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->O:LX/0W3;

    sget-wide v6, LX/0X5;->er:J

    invoke-interface {v4, v6, v7}, LX/0W4;->a(J)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 1920122
    iget-object v4, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aa:Lcom/facebook/fbui/glyph/GlyphView;

    sget v5, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->Q:I

    invoke-virtual {v4, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 1920123
    iget-object v4, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aa:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v4, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1920124
    iget-object v4, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aa:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v4, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1920125
    :cond_2
    :goto_2
    if-nez v3, :cond_3

    iget-object v4, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    invoke-interface {v4}, LX/5kD;->o()Z

    move-result v4

    invoke-interface {p1}, LX/5kD;->o()Z

    move-result v5

    if-eq v4, v5, :cond_4

    .line 1920126
    :cond_3
    invoke-interface {p1}, LX/5kD;->o()Z

    move-result v4

    if-eqz v4, :cond_11

    .line 1920127
    iget-object v4, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ab:Lcom/facebook/fbui/glyph/GlyphView;

    sget v5, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->R:I

    invoke-virtual {v4, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 1920128
    iget-object v4, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ab:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v4, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1920129
    :cond_4
    :goto_3
    if-nez v3, :cond_5

    iget-object v4, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    invoke-interface {v4}, LX/5kD;->r()Z

    move-result v4

    invoke-interface {p1}, LX/5kD;->r()Z

    move-result v5

    if-ne v4, v5, :cond_5

    iget-object v4, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    invoke-interface {v4}, LX/5kD;->y()Z

    move-result v4

    invoke-interface {p1}, LX/5kD;->y()Z

    move-result v5

    if-ne v4, v5, :cond_5

    iget-object v4, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    invoke-interface {v4}, LX/5kD;->aa()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;

    move-result-object v4

    invoke-interface {p1}, LX/5kD;->aa()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;

    move-result-object v5

    if-ne v4, v5, :cond_5

    iget-object v4, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    invoke-interface {v4}, LX/5kD;->E()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    move-result-object v4

    invoke-interface {p1}, LX/5kD;->E()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    move-result-object v5

    if-eq v4, v5, :cond_7

    .line 1920130
    :cond_5
    invoke-interface {p1}, LX/5kD;->y()Z

    move-result v4

    if-eqz v4, :cond_12

    invoke-interface {p1}, LX/5kD;->r()Z

    move-result v4

    if-nez v4, :cond_12

    invoke-interface {p1}, LX/5kD;->E()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    move-result-object v4

    if-nez v4, :cond_12

    invoke-interface {p1}, LX/5kD;->aa()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;

    move-result-object v4

    if-nez v4, :cond_12

    .line 1920131
    :goto_4
    invoke-interface {p1}, LX/5kD;->r()Z

    move-result v4

    if-nez v4, :cond_6

    if-eqz v0, :cond_14

    .line 1920132
    :cond_6
    iget-object v4, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ac:Lcom/facebook/fbui/glyph/GlyphView;

    sget v5, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->S:I

    invoke-virtual {v4, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 1920133
    iget-object v4, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ac:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v4, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1920134
    iget-object v4, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ac:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-interface {p1}, LX/5kD;->E()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    move-result-object v1

    if-eqz v1, :cond_13

    const v1, -0xa76f01

    :goto_5
    invoke-virtual {v4, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1920135
    :goto_6
    if-eqz v0, :cond_7

    .line 1920136
    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->t()V

    .line 1920137
    :cond_7
    if-nez v3, :cond_8

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    invoke-interface {v0}, LX/5kD;->W()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;

    move-result-object v0

    invoke-interface {p1}, LX/5kD;->W()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;

    move-result-object v1

    if-eq v0, v1, :cond_9

    .line 1920138
    :cond_8
    invoke-direct {p0, p1}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->i(LX/5kD;)V

    .line 1920139
    :cond_9
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->h:LX/CbH;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    const/4 v3, 0x0

    .line 1920140
    iget-object v2, v0, LX/CbH;->d:Landroid/view/View;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1920141
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1920142
    if-eqz v1, :cond_b

    invoke-interface {p1}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_a

    invoke-interface {p1}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    :cond_a
    invoke-interface {v1}, LX/5kD;->z()Z

    move-result v2

    invoke-interface {p1}, LX/5kD;->z()Z

    move-result v4

    if-ne v2, v4, :cond_b

    invoke-interface {v1}, LX/5kD;->o()Z

    move-result v2

    invoke-interface {p1}, LX/5kD;->o()Z

    move-result v4

    if-eq v2, v4, :cond_16

    :cond_b
    const/4 v2, 0x1

    :goto_7
    move v2, v2

    .line 1920143
    if-eqz v2, :cond_e

    .line 1920144
    iget-object v2, v0, LX/CbH;->a:LX/03R;

    sget-object v4, LX/03R;->YES:LX/03R;

    if-ne v2, v4, :cond_17

    if-eqz p1, :cond_17

    invoke-interface {p1}, LX/5kD;->o()Z

    move-result v2

    if-nez v2, :cond_c

    invoke-interface {p1}, LX/5kD;->z()Z

    move-result v2

    if-eqz v2, :cond_17

    :cond_c
    const/4 v2, 0x1

    :goto_8
    move v4, v2

    .line 1920145
    iget-object v5, v0, LX/CbH;->d:Landroid/view/View;

    if-eqz v4, :cond_15

    move v2, v3

    :goto_9
    invoke-virtual {v5, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1920146
    if-nez v4, :cond_d

    iget-boolean v2, v0, LX/CbH;->c:Z

    if-eqz v2, :cond_d

    .line 1920147
    invoke-virtual {v0}, LX/CbH;->d()V

    .line 1920148
    :cond_d
    iget-boolean v2, v0, LX/CbH;->f:Z

    if-eqz v2, :cond_e

    iget-boolean v2, v0, LX/CbH;->c:Z

    if-nez v2, :cond_e

    if-eqz v4, :cond_e

    .line 1920149
    iput-boolean v3, v0, LX/CbH;->f:Z

    .line 1920150
    invoke-virtual {v0}, LX/CbH;->d()V

    .line 1920151
    :cond_e
    goto/16 :goto_0

    :cond_f
    move v3, v1

    .line 1920152
    goto/16 :goto_1

    .line 1920153
    :cond_10
    iget-object v4, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aa:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v4, v8}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto/16 :goto_2

    .line 1920154
    :cond_11
    iget-object v4, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ab:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v4, v8}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto/16 :goto_3

    :cond_12
    move v0, v1

    .line 1920155
    goto/16 :goto_4

    :cond_13
    move v1, v2

    .line 1920156
    goto/16 :goto_5

    .line 1920157
    :cond_14
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ac:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v1, v8}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto/16 :goto_6

    .line 1920158
    :cond_15
    const/16 v2, 0x8

    goto :goto_9

    :cond_16
    const/4 v2, 0x0

    goto :goto_7

    :cond_17
    const/4 v2, 0x0

    goto :goto_8
.end method

.method private static e(LX/5kD;)Z
    .locals 1

    .prologue
    .line 1920110
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5kD;

    invoke-interface {v0}, LX/5kD;->G()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static f(LX/5kD;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1920109
    invoke-interface {p0}, LX/5kD;->X()LX/175;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/5kD;->X()LX/175;

    move-result-object v0

    invoke-interface {v0}, LX/175;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()V
    .locals 5

    .prologue
    .line 1920076
    const-class v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    invoke-static {v0, p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1920077
    invoke-virtual {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030a81

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1920078
    const v0, 0x7f0d1ac9

    invoke-virtual {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->W:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 1920079
    const v0, 0x7f0d1ac2

    invoke-virtual {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->U:Landroid/view/View;

    .line 1920080
    const v0, 0x7f0d1ac8

    invoke-virtual {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/components/ComponentView;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->V:Lcom/facebook/components/ComponentView;

    .line 1920081
    const v0, 0x7f0d1acb

    invoke-virtual {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->af:Landroid/view/View;

    .line 1920082
    const v0, 0x7f0d1ad5

    invoke-virtual {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ao:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    .line 1920083
    const v0, 0x7f0d1ad0

    invoke-virtual {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/components/ComponentView;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->an:Lcom/facebook/components/ComponentView;

    .line 1920084
    const v0, 0x7f0d1acd

    invoke-virtual {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/metatext/TagsTextViewContainer;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ah:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    .line 1920085
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ah:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->j:LX/8tu;

    invoke-virtual {v0, v1}, Lcom/facebook/composer/metatext/TagsTextViewContainer;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1920086
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ah:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/composer/metatext/TagsTextViewContainer;->setHighlightColor(I)V

    .line 1920087
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ah:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    new-instance v1, LX/8z8;

    invoke-virtual {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LX/8z8;-><init>(Landroid/content/res/Resources;)V

    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0048

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 1920088
    iput-object v2, v1, LX/8z8;->a:Landroid/text/style/CharacterStyle;

    .line 1920089
    move-object v1, v1

    .line 1920090
    new-instance v2, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0e0728

    invoke-direct {v2, v3, v4}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    .line 1920091
    iput-object v2, v1, LX/8z8;->b:Landroid/text/style/CharacterStyle;

    .line 1920092
    move-object v1, v1

    .line 1920093
    invoke-virtual {v1}, LX/8z8;->a()LX/8z7;

    move-result-object v1

    .line 1920094
    iput-object v1, v0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->i:LX/8z7;

    .line 1920095
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ah:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->s()LX/8zL;

    move-result-object v1

    .line 1920096
    iput-object v1, v0, Lcom/facebook/composer/metatext/TagsTextViewContainer;->l:LX/8zL;

    .line 1920097
    const v0, 0x7f0d1ace

    invoke-virtual {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aj:Landroid/widget/LinearLayout;

    .line 1920098
    const v0, 0x7f0d1ad4

    invoke-virtual {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->am:Landroid/view/View;

    .line 1920099
    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->g()V

    .line 1920100
    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->h()V

    .line 1920101
    const v0, 0x7f0d1acf

    invoke-virtual {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ai:Lcom/facebook/resources/ui/FbTextView;

    .line 1920102
    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->l()LX/Cay;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aD:LX/Cay;

    .line 1920103
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->h:LX/CbH;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aD:LX/Cay;

    invoke-virtual {v0, v1}, LX/CbH;->a(LX/Cay;)V

    .line 1920104
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->I:LX/9iA;

    invoke-virtual {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1920105
    new-instance v2, LX/9i9;

    invoke-direct {v2, v0, p0, v1}, LX/9i9;-><init>(LX/9iA;LX/9hC;Landroid/content/Context;)V

    move-object v0, v2

    .line 1920106
    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aG:LX/8z9;

    .line 1920107
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->setVisibility(I)V

    .line 1920108
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 1920423
    const v0, 0x7f0d1ac7

    invoke-virtual {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ad:Landroid/widget/ImageView;

    .line 1920424
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ad:Landroid/widget/ImageView;

    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->p()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1920425
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->i:LX/459;

    invoke-virtual {v0}, LX/459;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1920426
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ad:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1920427
    :cond_0
    const v0, 0x7f0d1ac4

    invoke-virtual {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aa:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1920428
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aa:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/Cbj;

    invoke-direct {v1, p0}, LX/Cbj;-><init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1920429
    const v0, 0x7f0d1ac5

    invoke-virtual {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ab:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1920430
    const v0, 0x7f0d1ac6

    invoke-virtual {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ac:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1920431
    return-void
.end method

.method private g(LX/5kD;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1920341
    invoke-interface {p1}, LX/5kD;->S()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1920342
    invoke-interface {p1}, LX/5kD;->w()Z

    move-result v0

    .line 1920343
    :cond_0
    :goto_0
    return v0

    .line 1920344
    :cond_1
    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->G:LX/0Uh;

    const/16 v3, 0x45c

    invoke-virtual {v2, v3, v1}, LX/0Uh;->a(IZ)Z

    move-result v2

    .line 1920345
    if-eqz v2, :cond_3

    .line 1920346
    invoke-interface {p1}, LX/5kD;->w()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aE:LX/74S;

    sget-object v3, LX/74S;->PHOTO_COMMENT:LX/74S;

    if-ne v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 1920347
    :cond_3
    invoke-interface {p1}, LX/5kD;->s()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aE:LX/74S;

    sget-object v3, LX/74S;->PHOTO_COMMENT:LX/74S;

    if-ne v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method private getAttributionController()Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryAttributionController;
    .locals 13

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1920406
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->as:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryAttributionController;

    if-nez v0, :cond_0

    .line 1920407
    const v0, 0x7f0d1acc

    invoke-virtual {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ag:Lcom/facebook/resources/ui/FbTextView;

    .line 1920408
    const v0, 0x7f0d1ad1

    invoke-virtual {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ak:Lcom/facebook/resources/ui/FbTextView;

    .line 1920409
    const v0, 0x7f0d1ad2

    invoke-virtual {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->al:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1920410
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->d:LX/Cbd;

    iget-object v3, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ag:Lcom/facebook/resources/ui/FbTextView;

    iget-object v4, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ak:Lcom/facebook/resources/ui/FbTextView;

    iget-object v5, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->al:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1920411
    new-instance v6, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryAttributionController;

    invoke-static {v0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v10

    check-cast v10, LX/11S;

    invoke-static {v0}, LX/9iB;->a(LX/0QB;)LX/9iB;

    move-result-object v11

    check-cast v11, LX/9iB;

    const/16 v7, 0xbc6

    invoke-static {v0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    move-object v7, v3

    move-object v8, v4

    move-object v9, v5

    invoke-direct/range {v6 .. v12}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryAttributionController;-><init>(Lcom/facebook/resources/ui/FbTextView;Lcom/facebook/resources/ui/FbTextView;Lcom/facebook/drawee/fbpipeline/FbDraweeView;LX/11S;LX/9iB;LX/0Or;)V

    .line 1920412
    move-object v0, v6

    .line 1920413
    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->as:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryAttributionController;

    .line 1920414
    iget-object v3, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ag:Lcom/facebook/resources/ui/FbTextView;

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->h:LX/CbH;

    .line 1920415
    iget-boolean v4, v0, LX/CbH;->c:Z

    move v0, v4

    .line 1920416
    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1920417
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aj:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->h:LX/CbH;

    .line 1920418
    iget-boolean v4, v3, LX/CbH;->c:Z

    move v3, v4

    .line 1920419
    if-nez v3, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1920420
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->as:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryAttributionController;

    return-object v0

    :cond_1
    move v0, v2

    .line 1920421
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1920422
    goto :goto_1
.end method

.method private getBaseContext()Landroid/content/Context;
    .locals 2

    .prologue
    .line 1920404
    invoke-virtual {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1920405
    invoke-static {v0}, LX/31Z;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    check-cast v0, LX/31Z;

    invoke-virtual {v0}, LX/31Z;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private getComponentContext()LX/1De;
    .locals 2

    .prologue
    .line 1920401
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ar:LX/1De;

    if-nez v0, :cond_0

    .line 1920402
    new-instance v0, LX/1De;

    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1De;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ar:LX/1De;

    .line 1920403
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ar:LX/1De;

    return-object v0
.end method

.method private getFeedbackController()LX/3iK;
    .locals 2

    .prologue
    .line 1920398
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aI:LX/3iK;

    if-nez v0, :cond_0

    .line 1920399
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->a:LX/3iG;

    sget-object v1, Lcom/facebook/feedback/ui/FeedbackControllerParams;->a:Lcom/facebook/feedback/ui/FeedbackControllerParams;

    invoke-virtual {v0, v1}, LX/3iG;->a(Lcom/facebook/feedback/ui/FeedbackControllerParams;)LX/3iK;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aI:LX/3iK;

    .line 1920400
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aI:LX/3iK;

    return-object v0
.end method

.method private getStoryProps()Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1920392
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->av:Ljava/lang/String;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 1920393
    :goto_0
    return-object v0

    .line 1920394
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jU;

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->av:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/0jU;->d(Ljava/lang/String;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 1920395
    instance-of v2, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_1

    .line 1920396
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 1920397
    goto :goto_0
.end method

.method private h()V
    .locals 6

    .prologue
    .line 1920381
    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->k()LX/20Z;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aJ:LX/20Z;

    .line 1920382
    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->n()LX/21M;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aK:LX/21M;

    .line 1920383
    new-instance v0, LX/Cbk;

    invoke-direct {v0, p0}, LX/Cbk;-><init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;)V

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aH:Landroid/view/View$OnClickListener;

    .line 1920384
    const v0, 0x7f0d1ad3

    invoke-virtual {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ap:Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;

    .line 1920385
    const v0, 0x7f0d1ad6

    invoke-virtual {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryPlayButton;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aq:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryPlayButton;

    .line 1920386
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->C:LX/1Wo;

    sget-object v1, LX/21S;->INLINE:LX/21S;

    invoke-virtual {v0, v1}, LX/1Wo;->a(LX/21S;)LX/21T;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aM:LX/21T;

    .line 1920387
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->p:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    const-wide/high16 v2, 0x4026000000000000L    # 11.0

    const-wide/16 v4, 0x0

    invoke-static {v2, v3, v4, v5}, LX/0wT;->b(DD)LX/0wT;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    const/4 v1, 0x1

    .line 1920388
    iput-boolean v1, v0, LX/0wd;->c:Z

    .line 1920389
    move-object v0, v0

    .line 1920390
    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aN:LX/0wd;

    .line 1920391
    return-void
.end method

.method private static h(LX/5kD;)Z
    .locals 1

    .prologue
    .line 1920380
    invoke-interface {p0}, LX/5kD;->Q()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataInlineActivitiesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/5kD;->Q()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataInlineActivitiesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataInlineActivitiesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i(LX/5kD;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 1920432
    invoke-interface {p1}, LX/5kD;->W()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;

    move-result-object v1

    .line 1920433
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ac:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0}, Lcom/facebook/fbui/glyph/GlyphView;->getVisibility()I

    move-result v0

    if-eq v0, v2, :cond_0

    invoke-interface {p1}, LX/5kD;->E()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1920434
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ae:Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;

    if-eqz v0, :cond_1

    .line 1920435
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ae:Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;

    invoke-virtual {v0, v2}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;->setVisibility(I)V

    .line 1920436
    :cond_1
    :goto_0
    return-void

    .line 1920437
    :cond_2
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->t:LX/9iE;

    .line 1920438
    const-string v2, "photo_location_suggestion_effective"

    invoke-static {v0, v2, v1, p1}, LX/9iE;->a(LX/9iE;Ljava/lang/String;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;LX/5kD;)V

    .line 1920439
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ae:Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;

    if-nez v0, :cond_3

    .line 1920440
    const v0, 0x7f0d1aca

    invoke-virtual {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1920441
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ae:Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;

    .line 1920442
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ae:Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ac:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;->setAnchor(Landroid/view/View;)V

    .line 1920443
    :cond_3
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ae:Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;

    invoke-direct {p0, v1}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;->setAcceptSuggestedLocationListener(Landroid/view/View$OnClickListener;)V

    .line 1920444
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ae:Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;

    invoke-direct {p0, v1}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->b(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;->setRejectSuggestedLocationListener(Landroid/view/View$OnClickListener;)V

    .line 1920445
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ae:Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;

    invoke-virtual {v0, v1}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;)V

    .line 1920446
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ae:Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySuggestedLocationView;->setVisibility(I)V

    .line 1920447
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->t:LX/9iE;

    .line 1920448
    const-string v2, "photo_location_suggestion_appears"

    invoke-static {v0, v2, v1, p1}, LX/9iE;->a(LX/9iE;Ljava/lang/String;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;LX/5kD;)V

    .line 1920449
    goto :goto_0
.end method

.method private i()Z
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 1920375
    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1920376
    iget-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aC:Z

    .line 1920377
    :goto_0
    return v0

    .line 1920378
    :cond_0
    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->getStoryProps()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1920379
    if-eqz v0, :cond_1

    invoke-static {v0}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static j(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;LX/5kD;)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 1

    .prologue
    .line 1920373
    invoke-interface {p1}, LX/5kD;->G()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1920374
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aQ:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aQ:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aQ:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p1}, LX/5kD;->G()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    move-result-object v0

    invoke-static {v0}, LX/5k9;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    goto :goto_0
.end method

.method private j()Z
    .locals 3

    .prologue
    .line 1920372
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->G:LX/0Uh;

    const/16 v1, 0x3fe

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method private k()LX/20Z;
    .locals 1

    .prologue
    .line 1920371
    new-instance v0, LX/Cbl;

    invoke-direct {v0, p0}, LX/Cbl;-><init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;)V

    return-object v0
.end method

.method private l()LX/Cay;
    .locals 1

    .prologue
    .line 1920370
    new-instance v0, LX/Cbm;

    invoke-direct {v0, p0}, LX/Cbm;-><init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;)V

    return-object v0
.end method

.method private m()V
    .locals 3

    .prologue
    .line 1920365
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->am:Landroid/view/View;

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->h:LX/CbH;

    .line 1920366
    iget-boolean v2, v0, LX/CbH;->c:Z

    move v0, v2

    .line 1920367
    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aS:Z

    if-nez v0, :cond_1

    :cond_0
    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1920368
    return-void

    .line 1920369
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private n()LX/21M;
    .locals 1

    .prologue
    .line 1920364
    new-instance v0, LX/Cbn;

    invoke-direct {v0, p0}, LX/Cbn;-><init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;)V

    return-object v0
.end method

.method public static o(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;)V
    .locals 4

    .prologue
    .line 1920349
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1920350
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    invoke-static {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->j(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;LX/5kD;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->W()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->x:LX/1zf;

    invoke-virtual {v0}, LX/1zf;->e()LX/1zt;

    move-result-object v0

    move-object v1, v0

    .line 1920351
    :goto_0
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->x:LX/1zf;

    invoke-virtual {v0}, LX/1zf;->e()LX/1zt;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1920352
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->H:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3RX;

    const-string v2, "like_main"

    invoke-virtual {v0, v2}, LX/3RX;->a(Ljava/lang/String;)V

    .line 1920353
    :cond_0
    invoke-direct {p0, v1}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->a(LX/1zt;)V

    .line 1920354
    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->e:LX/0Zb;

    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v0, LX/1zt;->c:LX/1zt;

    if-ne v1, v0, :cond_2

    sget-object v0, LX/CZw;->c:Ljava/lang/String;

    :goto_1
    invoke-direct {v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    invoke-static {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->j(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;LX/5kD;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    .line 1920355
    iput-object v0, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->e:Ljava/lang/String;

    .line 1920356
    move-object v0, v3

    .line 1920357
    const-string v1, "fbobj"

    .line 1920358
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->d:Ljava/lang/String;

    .line 1920359
    move-object v0, v0

    .line 1920360
    invoke-interface {v2, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1920361
    return-void

    .line 1920362
    :cond_1
    sget-object v0, LX/1zt;->c:LX/1zt;

    move-object v1, v0

    goto :goto_0

    .line 1920363
    :cond_2
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->x:LX/1zf;

    invoke-virtual {v0}, LX/1zf;->e()LX/1zt;

    move-result-object v0

    if-ne v1, v0, :cond_3

    sget-object v0, LX/CZw;->a:Ljava/lang/String;

    goto :goto_1

    :cond_3
    sget-object v0, LX/CZw;->b:Ljava/lang/String;

    goto :goto_1
.end method

.method private p()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1920348
    new-instance v0, LX/Cbq;

    invoke-direct {v0, p0}, LX/Cbq;-><init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;)V

    return-object v0
.end method

.method public static q(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;)V
    .locals 3

    .prologue
    .line 1920258
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aB:Z

    .line 1920259
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1920260
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->U:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1920261
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 1920262
    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aj:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 1920263
    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aj:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1920264
    :goto_0
    iget v1, v1, Landroid/graphics/Rect;->top:I

    .line 1920265
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    sub-int v0, v1, v0

    .line 1920266
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ah:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    invoke-virtual {v1}, Lcom/facebook/composer/metatext/TagsTextViewContainer;->getLayoutHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1920267
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ah:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    invoke-virtual {v1}, Lcom/facebook/composer/metatext/TagsTextViewContainer;->getHeight()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 1920268
    :goto_1
    return-void

    .line 1920269
    :cond_0
    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->an:Lcom/facebook/components/ComponentView;

    invoke-virtual {v2, v1}, Lcom/facebook/components/ComponentView;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    goto :goto_0

    .line 1920270
    :cond_1
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ah:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    invoke-virtual {v1, v0}, Lcom/facebook/composer/metatext/TagsTextViewContainer;->setMaxHeight(I)V

    .line 1920271
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->af:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1920272
    const/4 v1, -0x2

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 1920273
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->af:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1920274
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 1920275
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ah:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    invoke-virtual {v1, v0}, Lcom/facebook/composer/metatext/TagsTextViewContainer;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method private r()V
    .locals 3

    .prologue
    .line 1920336
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ah:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/composer/metatext/TagsTextViewContainer;->setMaxLines(I)V

    .line 1920337
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->af:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1920338
    invoke-virtual {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1878

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 1920339
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->af:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1920340
    return-void
.end method

.method private s()LX/8zL;
    .locals 1

    .prologue
    .line 1919826
    new-instance v0, LX/Cbr;

    invoke-direct {v0, p0}, LX/Cbr;-><init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;)V

    return-object v0
.end method

.method public static setTaggingMode(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;Z)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1919980
    iget-object v4, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ab:Lcom/facebook/fbui/glyph/GlyphView;

    if-eqz p1, :cond_6

    const v0, -0xa76f01

    :goto_0
    invoke-virtual {v4, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1919981
    if-nez p1, :cond_7

    move v0, v1

    :goto_1
    invoke-direct {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->setUfiElementsEnabled(Z)V

    .line 1919982
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->h:LX/CbH;

    .line 1919983
    iget-boolean v4, v0, LX/CbH;->g:Z

    move v0, v4

    .line 1919984
    if-eqz v0, :cond_0

    .line 1919985
    iget-object v4, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ao:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    if-eqz p1, :cond_8

    const/4 v0, 0x4

    :goto_2
    invoke-virtual {v4, v0}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->setVisibility(I)V

    .line 1919986
    :cond_0
    iget-object v4, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ai:Lcom/facebook/resources/ui/FbTextView;

    if-eqz p1, :cond_9

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    invoke-interface {v0}, LX/5kD;->r()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->h:LX/CbH;

    .line 1919987
    iget-boolean v5, v0, LX/CbH;->g:Z

    move v0, v5

    .line 1919988
    if-nez v0, :cond_9

    :cond_2
    move v0, v2

    :goto_3
    invoke-virtual {v4, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1919989
    iget-object v4, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aj:Landroid/widget/LinearLayout;

    iget-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aA:Z

    if-eqz v0, :cond_3

    if-eqz p1, :cond_a

    :cond_3
    move v0, v3

    :goto_4
    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1919990
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    invoke-static {v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->f(LX/5kD;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    invoke-static {v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->h(LX/5kD;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    invoke-static {v0}, LX/9iA;->a(LX/5kD;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1919991
    :cond_4
    :goto_5
    iget-object v4, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ah:Lcom/facebook/composer/metatext/TagsTextViewContainer;

    iget-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aP:Z

    if-eqz v0, :cond_c

    if-eqz v1, :cond_c

    if-nez p1, :cond_c

    move v0, v2

    :goto_6
    invoke-virtual {v4, v0}, Lcom/facebook/composer/metatext/TagsTextViewContainer;->setVisibility(I)V

    .line 1919992
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ag:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_5

    .line 1919993
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ag:Lcom/facebook/resources/ui/FbTextView;

    if-eqz p1, :cond_d

    move v0, v3

    :goto_7
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1919994
    :cond_5
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->an:Lcom/facebook/components/ComponentView;

    if-eqz p1, :cond_e

    :goto_8
    invoke-virtual {v0, v3}, Lcom/facebook/components/ComponentView;->setVisibility(I)V

    .line 1919995
    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->m()V

    .line 1919996
    return-void

    .line 1919997
    :cond_6
    const/4 v0, -0x1

    goto/16 :goto_0

    :cond_7
    move v0, v2

    .line 1919998
    goto/16 :goto_1

    :cond_8
    move v0, v2

    .line 1919999
    goto :goto_2

    :cond_9
    move v0, v3

    .line 1920000
    goto :goto_3

    :cond_a
    move v0, v2

    .line 1920001
    goto :goto_4

    :cond_b
    move v1, v2

    .line 1920002
    goto :goto_5

    :cond_c
    move v0, v3

    .line 1920003
    goto :goto_6

    :cond_d
    move v0, v2

    .line 1920004
    goto :goto_7

    :cond_e
    move v3, v2

    .line 1920005
    goto :goto_8
.end method

.method private setUfiElementsEnabled(Z)V
    .locals 1

    .prologue
    .line 1919977
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ao:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    invoke-virtual {v0, p1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->setEnabled(Z)V

    .line 1919978
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->an:Lcom/facebook/components/ComponentView;

    invoke-virtual {v0, p1}, Lcom/facebook/components/ComponentView;->setEnabled(Z)V

    .line 1919979
    return-void
.end method

.method private t()V
    .locals 4

    .prologue
    .line 1919966
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iA;

    const-string v1, "4003"

    const-class v2, LX/CcY;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Ljava/lang/String;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/CcY;

    .line 1919967
    if-nez v0, :cond_0

    .line 1919968
    :goto_0
    return-void

    .line 1919969
    :cond_0
    new-instance v1, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MEDIA_GALLERY_OPENED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    .line 1919970
    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ac:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1919971
    iput-object v2, v0, LX/CcY;->c:Landroid/view/View;

    .line 1919972
    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->u()LX/5Od;

    move-result-object v2

    .line 1919973
    iput-object v2, v0, LX/CcY;->b:LX/5Od;

    .line 1919974
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15W;

    invoke-virtual {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 1919975
    const-class v3, LX/0i1;

    const/4 p0, 0x0

    invoke-virtual {v0, v2, v1, v3, p0}, LX/15W;->a(Landroid/content/Context;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 1919976
    goto :goto_0
.end method

.method private u()LX/5Od;
    .locals 1

    .prologue
    .line 1919965
    new-instance v0, LX/Cbf;

    invoke-direct {v0, p0}, LX/Cbf;-><init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;)V

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1919956
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aR:LX/9hK;

    if-nez v0, :cond_0

    .line 1919957
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->P:Ljava/lang/String;

    const-string v2, "Attempting to setup media gallery header with a null MediaGalleryEnvironment"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1919958
    :goto_0
    return-void

    .line 1919959
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aO:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aQ:Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v0, :cond_2

    .line 1919960
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->V:Lcom/facebook/components/ComponentView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/components/ComponentView;->setVisibility(I)V

    goto :goto_0

    .line 1919961
    :cond_2
    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->getComponentContext()LX/1De;

    move-result-object v1

    .line 1919962
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->J:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1VD;

    invoke-virtual {v0, v1}, LX/1VD;->c(LX/1De;)LX/1X4;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aQ:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1X4;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X4;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aR:LX/9hK;

    invoke-virtual {v0, v2}, LX/1X4;->a(LX/1Pb;)LX/1X4;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/1X4;->c(Z)LX/1X4;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/1X4;->g(Z)LX/1X4;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/1X4;->d(Z)LX/1X4;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/1X4;->e(Z)LX/1X4;

    move-result-object v0

    const v2, 0x7f0e0a02

    invoke-virtual {v0, v2}, LX/1X4;->n(I)LX/1X4;

    move-result-object v0

    invoke-static {v1, v0}, LX/1dV;->a(LX/1De;LX/1X5;)LX/1me;

    move-result-object v0

    invoke-virtual {v0}, LX/1me;->b()LX/1dV;

    move-result-object v0

    .line 1919963
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->V:Lcom/facebook/components/ComponentView;

    invoke-virtual {v1, v0}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 1919964
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->V:Lcom/facebook/components/ComponentView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/components/ComponentView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(IILjava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1919954
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ap:Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->a(IILjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 1919955
    return-void
.end method

.method public final a(LX/0am;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 1919940
    new-instance v1, LX/6WS;

    invoke-virtual {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0, v4}, LX/6WS;-><init>(Landroid/content/Context;I)V

    .line 1919941
    invoke-virtual {v1}, LX/5OM;->c()LX/5OG;

    move-result-object v0

    .line 1919942
    const v2, 0x7f081095

    invoke-virtual {v0, v2}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v2

    .line 1919943
    const v3, 0x7f02080f

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 1919944
    new-instance v3, LX/Cbo;

    invoke-direct {v3, p0}, LX/Cbo;-><init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;)V

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1919945
    const v2, 0x7f081096

    invoke-virtual {v0, v2}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v0

    .line 1919946
    sget v2, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->Q:I

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 1919947
    new-instance v2, LX/Cbp;

    invoke-direct {v2, p0}, LX/Cbp;-><init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;)V

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1919948
    invoke-virtual {p1}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1919949
    invoke-virtual {v1, v4}, LX/0ht;->c(Z)V

    .line 1919950
    invoke-virtual {p1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v1, v0}, LX/0ht;->f(Landroid/view/View;)V

    .line 1919951
    :goto_0
    return-void

    .line 1919952
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, LX/0ht;->c(Z)V

    .line 1919953
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ao:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    invoke-virtual {v1, v0}, LX/0ht;->f(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final a(LX/74P;LX/0am;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/74P;",
            "LX/0am",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 1919920
    new-instance v7, LX/6WS;

    invoke-virtual {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v7, v0, v8}, LX/6WS;-><init>(Landroid/content/Context;I)V

    .line 1919921
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->T:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v7}, LX/5OM;->c()LX/5OG;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    iget-object v4, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->av:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ax:Ljava/lang/String;

    iget-object v6, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->B:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->a(Lcom/facebook/base/fragment/FbFragment;Landroid/view/Menu;LX/5kD;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;)V

    .line 1919922
    invoke-virtual {p2}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1919923
    invoke-virtual {v7, v8}, LX/0ht;->c(Z)V

    .line 1919924
    invoke-virtual {p2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v7, v0}, LX/0ht;->f(Landroid/view/View;)V

    .line 1919925
    :goto_0
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aF:LX/745;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    if-eqz v0, :cond_3

    .line 1919926
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aF:LX/745;

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    invoke-interface {v0}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    invoke-interface {v0}, LX/5kD;->Z()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    invoke-interface {v0}, LX/5kD;->Z()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;->c()Ljava/lang/String;

    move-result-object v0

    .line 1919927
    :goto_1
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919928
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919929
    invoke-static {v1}, LX/745;->d(LX/745;)Ljava/util/HashMap;

    move-result-object v3

    .line 1919930
    const-string v4, "content_id"

    invoke-virtual {v3, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919931
    const-string v4, "action"

    iget-object v5, p1, LX/74P;->value:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919932
    if-eqz v0, :cond_0

    .line 1919933
    const-string v4, "owner_id"

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919934
    :cond_0
    sget-object v4, LX/74R;->PHOTO_GALLERY_MENU_SHOWN:LX/74R;

    invoke-static {v1, v4, v3, v2}, LX/745;->a(LX/745;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1919935
    :goto_2
    return-void

    .line 1919936
    :cond_1
    invoke-virtual {v7, v9}, LX/0ht;->c(Z)V

    .line 1919937
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ao:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    invoke-virtual {v7, v0}, LX/0ht;->f(Landroid/view/View;)V

    goto :goto_0

    .line 1919938
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 1919939
    :cond_3
    sget-object v1, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->P:Ljava/lang/String;

    const-string v2, "MediaLogger or CurrentMedia is not available when opening overflow menu: %s : source %s"

    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/Object;

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    if-eqz v0, :cond_4

    const-string v0, "media exists"

    :goto_3
    aput-object v0, v3, v9

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aE:LX/74S;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aE:LX/74S;

    invoke-virtual {v0}, LX/74S;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_4
    aput-object v0, v3, v8

    invoke-static {v1, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    :cond_4
    const-string v0, "media is null"

    goto :goto_3

    :cond_5
    const-string v0, "unknown"

    goto :goto_4
.end method

.method public final a(Lcom/facebook/base/fragment/FbFragment;LX/5kD;LX/21D;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1919901
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919902
    invoke-static {p2}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->e(LX/5kD;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1919903
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->setVisibility(I)V

    .line 1919904
    :goto_0
    return-void

    .line 1919905
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 1919906
    invoke-virtual {p0, v2}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->setVisibility(I)V

    .line 1919907
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    invoke-interface {v0}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1919908
    iput-boolean v2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aB:Z

    .line 1919909
    :cond_2
    invoke-interface {p2}, LX/5kD;->G()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919910
    invoke-direct {p0, p2}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->a(LX/5kD;)V

    .line 1919911
    invoke-direct {p0, p2}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->b(LX/5kD;)V

    .line 1919912
    invoke-direct {p0, p2}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->c(LX/5kD;)V

    .line 1919913
    invoke-direct {p0, p2}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->d(LX/5kD;)V

    .line 1919914
    iget-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aA:Z

    if-eqz v0, :cond_3

    .line 1919915
    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->getAttributionController()Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryAttributionController;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    invoke-virtual {v0, v1, p2}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryAttributionController;->a(LX/5kD;LX/5kD;)V

    .line 1919916
    :cond_3
    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->m()V

    .line 1919917
    iput-object p2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    .line 1919918
    iput-object p1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->T:Lcom/facebook/base/fragment/FbFragment;

    .line 1919919
    iput-object p3, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->au:LX/21D;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 1
    .param p1    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1919895
    iput-object p1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aQ:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1919896
    invoke-virtual {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->a()V

    .line 1919897
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    invoke-direct {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->a(LX/5kD;)V

    .line 1919898
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    invoke-direct {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->b(LX/5kD;)V

    .line 1919899
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    invoke-direct {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->c(LX/5kD;)V

    .line 1919900
    return-void
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    .line 1919879
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->h:LX/CbH;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ab:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1919880
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919881
    iget-object v2, v0, LX/CbH;->a:LX/03R;

    sget-object v3, LX/03R;->UNSET:LX/03R;

    if-eq v2, v3, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 1919882
    iput-object v1, v0, LX/CbH;->d:Landroid/view/View;

    .line 1919883
    iget-object v2, v0, LX/CbH;->a:LX/03R;

    sget-object v3, LX/03R;->YES:LX/03R;

    if-ne v2, v3, :cond_1

    .line 1919884
    iget-object v2, v0, LX/CbH;->d:Landroid/view/View;

    .line 1919885
    new-instance v3, LX/CbG;

    invoke-direct {v3, v0}, LX/CbG;-><init>(LX/CbH;)V

    move-object v3, v3

    .line 1919886
    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1919887
    iput-boolean p1, v0, LX/CbH;->c:Z

    .line 1919888
    iput-boolean p1, v0, LX/CbH;->f:Z

    .line 1919889
    :goto_1
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->h:LX/CbH;

    .line 1919890
    iget-boolean v1, v0, LX/CbH;->c:Z

    move v0, v1

    .line 1919891
    invoke-static {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->setTaggingMode(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;Z)V

    .line 1919892
    return-void

    .line 1919893
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 1919894
    :cond_1
    iget-object v2, v0, LX/CbH;->d:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public final a(ZLX/2oi;)V
    .locals 2

    .prologue
    .line 1919876
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ap:Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;

    sget-object v0, LX/2oi;->HIGH_DEFINITION:LX/2oi;

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, p1, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->a(ZZ)V

    .line 1919877
    return-void

    .line 1919878
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1920006
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aI:LX/3iK;

    if-eqz v0, :cond_0

    .line 1920007
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aI:LX/3iK;

    invoke-virtual {v0}, LX/3iK;->a()V

    .line 1920008
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->h:LX/CbH;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aD:LX/Cay;

    invoke-virtual {v0, v1}, LX/CbH;->b(LX/Cay;)V

    .line 1920009
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ac:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1920010
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    .line 1920011
    iget-object v1, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->g:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 1920012
    iget-object v1, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->E:Landroid/view/Menu;

    if-eqz v1, :cond_1

    .line 1920013
    iget-object v1, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->E:Landroid/view/Menu;

    invoke-interface {v1}, Landroid/view/Menu;->clear()V

    .line 1920014
    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->setVisibility(I)V

    .line 1920015
    iput-object v2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    .line 1920016
    iput-object v2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->T:Lcom/facebook/base/fragment/FbFragment;

    .line 1920017
    return-void
.end method

.method public final b(Z)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 1919835
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    invoke-interface {v0}, LX/5kD;->G()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1919836
    :cond_0
    :goto_0
    return-void

    .line 1919837
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    invoke-static {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->j(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;LX/5kD;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    .line 1919838
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->g:LX/1nK;

    .line 1919839
    iget-object v1, v0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0x39000b

    const-string v5, "Photos_FlyoutLoadCached"

    invoke-interface {v1, v2, v5}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 1919840
    iget-object v1, v0, LX/1nK;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0x39000c

    const-string v5, "Photos_FlyoutLoadNetwork"

    invoke-interface {v1, v2, v5}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 1919841
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    const-string v1, "tap_photo_footer_comment"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 1919842
    new-instance v0, LX/8qL;

    invoke-direct {v0}, LX/8qL;-><init>()V

    .line 1919843
    iput-object v4, v0, LX/8qL;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1919844
    move-object v0, v0

    .line 1919845
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v1

    .line 1919846
    iput-object v1, v0, LX/8qL;->d:Ljava/lang/String;

    .line 1919847
    move-object v0, v0

    .line 1919848
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v1

    .line 1919849
    iput-object v1, v0, LX/8qL;->e:Ljava/lang/String;

    .line 1919850
    move-object v0, v0

    .line 1919851
    invoke-static {v4}, LX/16z;->b(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v1

    .line 1919852
    iput-boolean v1, v0, LX/8qL;->p:Z

    .line 1919853
    move-object v0, v0

    .line 1919854
    new-instance v1, LX/21A;

    invoke-direct {v1}, LX/21A;-><init>()V

    const-string v2, "photo_gallery"

    .line 1919855
    iput-object v2, v1, LX/21A;->c:Ljava/lang/String;

    .line 1919856
    move-object v1, v1

    .line 1919857
    invoke-virtual {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->getTrackingCodes()LX/162;

    move-result-object v2

    .line 1919858
    iput-object v2, v1, LX/21A;->a:LX/162;

    .line 1919859
    move-object v1, v1

    .line 1919860
    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->au:LX/21D;

    .line 1919861
    iput-object v2, v1, LX/21A;->i:LX/21D;

    .line 1919862
    move-object v1, v1

    .line 1919863
    invoke-virtual {v1}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v1

    .line 1919864
    iput-object v1, v0, LX/8qL;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1919865
    move-object v0, v0

    .line 1919866
    iput-boolean p1, v0, LX/8qL;->i:Z

    .line 1919867
    move-object v1, v0

    .line 1919868
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aQ:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aQ:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1919869
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aQ:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1919870
    iput-object v0, v1, LX/8qL;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1919871
    :cond_2
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ay:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1919872
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ay:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/8qL;->a(Ljava/lang/Long;)LX/8qL;

    .line 1919873
    :cond_3
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->H:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3RX;

    const-string v2, "comment"

    invoke-virtual {v0, v2}, LX/3RX;->a(Ljava/lang/String;)V

    .line 1919874
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1nI;

    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1}, LX/8qL;->a()Lcom/facebook/ufiservices/flyout/FeedbackParams;

    move-result-object v1

    invoke-interface {v0, v2, v1}, LX/1nI;->a(Landroid/content/Context;Lcom/facebook/ufiservices/flyout/FeedbackParams;)V

    .line 1919875
    iget-object v6, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->e:LX/0Zb;

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_1
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    invoke-interface {v1}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v1

    :goto_2
    if-eqz v4, :cond_7

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v2

    :goto_3
    if-eqz v4, :cond_4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v3

    :cond_4
    invoke-virtual {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->getTrackingCodes()LX/162;

    move-result-object v4

    const-string v5, "photo_gallery"

    invoke-static/range {v0 .. v5}, LX/1EP;->a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0lF;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-interface {v6, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto/16 :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    :cond_6
    move-object v1, v3

    goto :goto_2

    :cond_7
    move-object v2, v3

    goto :goto_3
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 1919831
    if-eqz p1, :cond_0

    .line 1919832
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aq:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryPlayButton;

    invoke-virtual {v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryPlayButton;->b()V

    .line 1919833
    :goto_0
    return-void

    .line 1919834
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aq:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryPlayButton;

    invoke-virtual {v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryPlayButton;->a()V

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1919830
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    invoke-static {v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->e(LX/5kD;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 6

    .prologue
    .line 1919827
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->H:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3RX;

    const-string v1, "share"

    invoke-virtual {v0, v1}, LX/3RX;->a(Ljava/lang/String;)V

    .line 1919828
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->n:LX/1Kf;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v3, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    invoke-interface {v3}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->au:LX/21D;

    const-string v5, "mediaGalleryFooter"

    invoke-interface {v0, v3, v4, v5}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Ljava/lang/String;LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v1, v2, v0, v3}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    .line 1919829
    return-void
.end method

.method public final e()V
    .locals 8

    .prologue
    .line 1919820
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->L:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B9y;

    invoke-virtual {v0}, LX/B9y;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1919821
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->L:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B9y;

    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    invoke-interface {v2}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    iget-object v4, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    invoke-interface {v4}, LX/5kD;->aj_()LX/1Fb;

    move-result-object v4

    invoke-interface {v4}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    const-string v6, ""

    const-string v7, "mediaGalleryFooter"

    invoke-virtual/range {v0 .. v7}, LX/B9y;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1919822
    :cond_0
    :goto_0
    return-void

    .line 1919823
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->M:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/0ax;->aj:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 1919824
    if-eqz v1, :cond_0

    .line 1919825
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->N:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public getCurrentMedia()LX/5kD;
    .locals 1

    .prologue
    .line 1920051
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    return-object v0
.end method

.method public getFeedbackCustomPressStateButton()Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1920074
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->W:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    return-object v0
.end method

.method public getStoryId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1920067
    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1920068
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aw:Ljava/lang/String;

    .line 1920069
    :goto_0
    return-object v0

    .line 1920070
    :cond_0
    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->getStoryProps()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1920071
    if-eqz v0, :cond_1

    .line 1920072
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 1920073
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTrackingCodes()LX/162;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1920062
    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1920063
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->az:LX/162;

    .line 1920064
    :goto_0
    return-object v0

    .line 1920065
    :cond_0
    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->getStoryProps()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1920066
    if-eqz v0, :cond_1

    invoke-static {v0}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setGallerySource(LX/74S;)V
    .locals 0

    .prologue
    .line 1920060
    iput-object p1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aE:LX/74S;

    .line 1920061
    return-void
.end method

.method public setGroupId(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1920058
    iput-object p1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ay:Ljava/lang/String;

    .line 1920059
    return-void
.end method

.method public setInSponsoredContext(Z)V
    .locals 0

    .prologue
    .line 1920056
    iput-boolean p1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aC:Z

    .line 1920057
    return-void
.end method

.method public setLegacyApiStoryId(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1920054
    iput-object p1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ax:Ljava/lang/String;

    .line 1920055
    return-void
.end method

.method public setLocationButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1920052
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ac:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1920053
    return-void
.end method

.method public setMediaLogger(LX/745;)V
    .locals 0

    .prologue
    .line 1920018
    iput-object p1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aF:LX/745;

    .line 1920019
    return-void
.end method

.method public setShouldShowStoryCaption(Z)V
    .locals 4

    .prologue
    .line 1920042
    iput-boolean p1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aP:Z

    .line 1920043
    iget-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aP:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b188a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    move v1, v0

    .line 1920044
    :goto_0
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->af:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1920045
    if-nez v0, :cond_1

    .line 1920046
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    const/16 v3, 0x50

    invoke-direct {v0, v2, v1, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 1920047
    :goto_1
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->af:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1920048
    return-void

    .line 1920049
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b188b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    move v1, v0

    goto :goto_0

    .line 1920050
    :cond_1
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    goto :goto_1
.end method

.method public setShouldShowStoryHeader(Z)V
    .locals 0

    .prologue
    .line 1920040
    iput-boolean p1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aO:Z

    .line 1920041
    return-void
.end method

.method public setShowAttribution(Z)V
    .locals 1

    .prologue
    .line 1920036
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1920037
    iput-boolean p1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aA:Z

    .line 1920038
    return-void

    .line 1920039
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setStoryCacheId(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1920034
    iput-object p1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->av:Ljava/lang/String;

    .line 1920035
    return-void
.end method

.method public setStoryId(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1920032
    iput-object p1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aw:Ljava/lang/String;

    .line 1920033
    return-void
.end method

.method public setTargetEnvironment(LX/9hK;)V
    .locals 0
    .param p1    # LX/9hK;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1920030
    iput-object p1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aR:LX/9hK;

    .line 1920031
    return-void
.end method

.method public setTargetStory(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1920028
    iput-object p1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aQ:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1920029
    return-void
.end method

.method public setTrackingCodes(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1920023
    if-nez p1, :cond_0

    .line 1920024
    :goto_0
    return-void

    .line 1920025
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->F:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lC;

    invoke-virtual {v0, p1}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    check-cast v0, LX/162;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->az:LX/162;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1920026
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1920027
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-class v2, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "error while parsing tracking codes "

    invoke-virtual {v0, v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public setVideoControlAlpha(F)V
    .locals 1

    .prologue
    .line 1920020
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->aq:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryPlayButton;

    invoke-virtual {v0, p1}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryPlayButton;->setAlpha(F)V

    .line 1920021
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->ap:Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;

    invoke-virtual {v0, p1}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->setAlpha(F)V

    .line 1920022
    return-void
.end method
