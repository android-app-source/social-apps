.class public Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;
.super LX/5ui;
.source ""


# instance fields
.field public a:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/imagepipeline/abtest/IsExperimentalZoomableDraweeTouchHandlingEnabled;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/7US;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/1ff;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1919615
    invoke-direct {p0, p1}, LX/5ui;-><init>(Landroid/content/Context;)V

    .line 1919616
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->c:Ljava/util/List;

    .line 1919617
    new-instance v0, LX/1ff;

    invoke-direct {v0}, LX/1ff;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->d:LX/1ff;

    .line 1919618
    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->c()V

    .line 1919619
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1919610
    invoke-direct {p0, p1, p2}, LX/5ui;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1919611
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->c:Ljava/util/List;

    .line 1919612
    new-instance v0, LX/1ff;

    invoke-direct {v0}, LX/1ff;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->d:LX/1ff;

    .line 1919613
    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->c()V

    .line 1919614
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1919605
    invoke-direct {p0, p1, p2, p3}, LX/5ui;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1919606
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->c:Ljava/util/List;

    .line 1919607
    new-instance v0, LX/1ff;

    invoke-direct {v0}, LX/1ff;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->d:LX/1ff;

    .line 1919608
    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->c()V

    .line 1919609
    return-void
.end method

.method private static a(Landroid/net/Uri;)LX/1bf;
    .locals 2
    .param p0    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1919601
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    sget-object v1, LX/1bY;->DISK_CACHE:LX/1bY;

    .line 1919602
    iput-object v1, v0, LX/1bX;->b:LX/1bY;

    .line 1919603
    move-object v0, v0

    .line 1919604
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;LX/1Ad;Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 1919586
    iput-object p1, p0, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->a:LX/1Ad;

    iput-object p2, p0, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->b:Ljava/lang/Boolean;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

    invoke-static {v1}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-static {v1}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object p1

    check-cast p1, LX/0Uh;

    invoke-static {p1}, LX/1Aq;->d(LX/0Uh;)Ljava/lang/Boolean;

    move-result-object p1

    move-object v1, p1

    check-cast v1, Ljava/lang/Boolean;

    invoke-static {p0, v0, v1}, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->a(Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;LX/1Ad;Ljava/lang/Boolean;)V

    return-void
.end method

.method private c()V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    .line 1919596
    const-class v0, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

    invoke-static {v0, p0}, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1919597
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1919598
    iput-boolean v0, p0, LX/5ui;->b:Z

    .line 1919599
    new-instance v0, LX/CbT;

    invoke-direct {v0, p0}, LX/CbT;-><init>(Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;)V

    invoke-super {p0, v0}, LX/5ui;->setTapListener(Landroid/view/GestureDetector$SimpleOnGestureListener;)V

    .line 1919600
    return-void
.end method


# virtual methods
.method public final a(LX/1Ai;)V
    .locals 1

    .prologue
    .line 1919620
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->d:LX/1ff;

    invoke-virtual {v0, p1}, LX/1ff;->a(LX/1Ai;)V

    .line 1919621
    return-void
.end method

.method public final a(LX/7US;)V
    .locals 1

    .prologue
    .line 1919594
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1919595
    return-void
.end method

.method public final a(Landroid/graphics/Matrix;)V
    .locals 2

    .prologue
    .line 1919590
    invoke-super {p0, p1}, LX/5ui;->a(Landroid/graphics/Matrix;)V

    .line 1919591
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7US;

    .line 1919592
    invoke-interface {v0, p1}, LX/7US;->a(Landroid/graphics/Matrix;)V

    goto :goto_0

    .line 1919593
    :cond_0
    return-void
.end method

.method public final a(Landroid/net/Uri;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 2
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1919587
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->a:LX/1Ad;

    invoke-static {p1}, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ae;->d(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, p2}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, p3}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->d:LX/1ff;

    invoke-virtual {v0, v1}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/1Ae;->b(Z)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1919588
    invoke-virtual {p0, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1919589
    return-void
.end method

.method public final b(LX/1Ai;)V
    .locals 1

    .prologue
    .line 1919584
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->d:LX/1ff;

    invoke-virtual {v0, p1}, LX/1ff;->b(LX/1Ai;)V

    .line 1919585
    return-void
.end method

.method public final b(LX/7US;)V
    .locals 1

    .prologue
    .line 1919582
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1919583
    return-void
.end method

.method public bridge synthetic getZoomableController()LX/5ua;
    .locals 1

    .prologue
    .line 1919581
    invoke-virtual {p0}, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->getZoomableController()LX/5ue;

    move-result-object v0

    return-object v0
.end method

.method public getZoomableController()LX/5ue;
    .locals 1

    .prologue
    .line 1919580
    invoke-super {p0}, LX/5ui;->getZoomableController()LX/5ua;

    move-result-object v0

    check-cast v0, LX/5ue;

    return-object v0
.end method
