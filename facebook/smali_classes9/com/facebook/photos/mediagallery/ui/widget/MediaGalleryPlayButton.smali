.class public Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryPlayButton;
.super Landroid/widget/ImageView;
.source ""


# instance fields
.field public a:LX/CcX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1921071
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryPlayButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1921072
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1921073
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryPlayButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1921074
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1921075
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1921076
    const-class v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryPlayButton;

    invoke-static {v0, p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryPlayButton;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1921077
    new-instance v0, LX/CcP;

    invoke-direct {v0, p0}, LX/CcP;-><init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryPlayButton;)V

    invoke-virtual {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryPlayButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1921078
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryPlayButton;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryPlayButton;

    invoke-static {v0}, LX/CcX;->a(LX/0QB;)LX/CcX;

    move-result-object v0

    check-cast v0, LX/CcX;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryPlayButton;->a:LX/CcX;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1921079
    const v0, 0x7f0214b3

    invoke-virtual {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryPlayButton;->setImageResource(I)V

    .line 1921080
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1921081
    const v0, 0x7f0213fa

    invoke-virtual {p0, v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryPlayButton;->setImageResource(I)V

    .line 1921082
    return-void
.end method

.method public setMediaId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1921083
    iput-object p1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryPlayButton;->b:Ljava/lang/String;

    .line 1921084
    return-void
.end method
