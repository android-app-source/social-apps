.class public Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final a:LX/1HI;

.field private final c:LX/9iU;

.field public final d:Ljava/util/concurrent/ExecutorService;

.field public final e:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1919647
    const-class v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;

    const-string v1, "set_cover_photo"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1HI;LX/9iU;Ljava/util/concurrent/ExecutorService;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1919658
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1919659
    iput-object p1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;->a:LX/1HI;

    .line 1919660
    iput-object p2, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;->c:LX/9iU;

    .line 1919661
    iput-object p3, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;->d:Ljava/util/concurrent/ExecutorService;

    .line 1919662
    iput-object p4, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;->e:Lcom/facebook/content/SecureContextHelper;

    .line 1919663
    return-void
.end method

.method public static a(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;LX/5kD;)LX/1ca;
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5kD;",
            ")",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1919655
    invoke-interface {p1}, LX/5kD;->aj_()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    .line 1919656
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;->a:LX/1HI;

    sget-object v2, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, LX/1HI;->c(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    .line 1919657
    return-object v0
.end method

.method public static b(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;LX/5kD;)LX/1ca;
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5kD;",
            ")",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1919652
    invoke-interface {p1}, LX/5kD;->aj_()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    .line 1919653
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;->a:LX/1HI;

    sget-object v2, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    .line 1919654
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;Ljava/io/File;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1919650
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919651
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;->c:LX/9iU;

    invoke-virtual {v0, p1, p2}, LX/9iU;->a(Landroid/graphics/Bitmap;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/io/InputStream;Ljava/io/File;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1919648
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919649
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;->c:LX/9iU;

    invoke-virtual {v0, p1, p2}, LX/9iU;->a(Ljava/io/InputStream;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
