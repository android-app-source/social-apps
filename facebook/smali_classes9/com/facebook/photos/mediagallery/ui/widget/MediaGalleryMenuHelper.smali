.class public Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field public static final c:Ljava/lang/String;


# instance fields
.field public final A:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation
.end field

.field public final B:LX/9fy;

.field public final C:LX/9yI;

.field public final D:LX/0Uh;

.field public E:Landroid/view/Menu;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/17Y;

.field public final f:Lcom/facebook/content/SecureContextHelper;

.field public final g:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/9hh;

.field public final j:LX/0kL;

.field public final k:LX/03V;

.field private final l:LX/9hH;

.field public final m:LX/8Q3;

.field public final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1L1;",
            ">;"
        }
    .end annotation
.end field

.field public final o:LX/9iB;

.field public final p:LX/745;

.field public final q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final r:LX/0i5;

.field private final s:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/01T;",
            ">;"
        }
    .end annotation
.end field

.field public final t:Landroid/support/v4/app/FragmentActivity;

.field public final u:LX/Cbs;

.field public final v:LX/9hJ;

.field public final w:Ljava/util/concurrent/ExecutorService;

.field public final x:Landroid/content/Context;

.field public final y:LX/0ad;

.field public final z:LX/CbR;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1920916
    const-class v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    const-string v1, "set_cover_photo"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1920917
    const-class v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 1920918
    const-class v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Or;LX/17Y;Lcom/facebook/content/SecureContextHelper;LX/1Ck;LX/1Ck;LX/9hh;LX/0kL;LX/03V;LX/8Q3;LX/0Ot;LX/9iB;LX/745;LX/0Ot;Landroid/support/v4/app/FragmentActivity;LX/0i4;LX/0Or;LX/Cbs;LX/9hJ;Ljava/util/concurrent/ExecutorService;Landroid/content/Context;LX/0ad;LX/CbR;LX/0Or;LX/9fy;LX/9yI;LX/0Uh;)V
    .locals 2
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .param p20    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/ipc/media/MediaGalleryMenuOptions;",
            ">;",
            "LX/17Y;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/1Ck;",
            "LX/1Ck;",
            "LX/9hh;",
            "LX/0kL;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/8Q3;",
            "LX/0Ot",
            "<",
            "LX/1L1;",
            ">;",
            "LX/9iB;",
            "LX/745;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;",
            ">;",
            "Landroid/support/v4/app/FragmentActivity;",
            "LX/0i4;",
            "LX/0Or",
            "<",
            "LX/01T;",
            ">;",
            "LX/Cbs;",
            "LX/9hJ;",
            "Ljava/util/concurrent/ExecutorService;",
            "Landroid/content/Context;",
            "LX/0ad;",
            "LX/CbR;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;",
            "LX/9fy;",
            "LX/9yI;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1920919
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1920920
    iput-object p1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->d:LX/0Or;

    .line 1920921
    iput-object p3, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->e:LX/17Y;

    .line 1920922
    iput-object p4, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->f:Lcom/facebook/content/SecureContextHelper;

    .line 1920923
    iput-object p5, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->g:LX/1Ck;

    .line 1920924
    iput-object p6, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->h:LX/1Ck;

    .line 1920925
    iput-object p7, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->i:LX/9hh;

    .line 1920926
    iput-object p8, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->j:LX/0kL;

    .line 1920927
    iput-object p9, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->k:LX/03V;

    .line 1920928
    invoke-interface {p2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9hH;

    iput-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->l:LX/9hH;

    .line 1920929
    iput-object p10, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->m:LX/8Q3;

    .line 1920930
    iput-object p11, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->n:LX/0Ot;

    .line 1920931
    iput-object p12, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->o:LX/9iB;

    .line 1920932
    iput-object p13, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->p:LX/745;

    .line 1920933
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->q:LX/0Ot;

    .line 1920934
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->t:Landroid/support/v4/app/FragmentActivity;

    .line 1920935
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->s:LX/0Or;

    .line 1920936
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->t:Landroid/support/v4/app/FragmentActivity;

    move-object/from16 v0, p16

    invoke-virtual {v0, v1}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->r:LX/0i5;

    .line 1920937
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->u:LX/Cbs;

    .line 1920938
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->v:LX/9hJ;

    .line 1920939
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->w:Ljava/util/concurrent/ExecutorService;

    .line 1920940
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->x:Landroid/content/Context;

    .line 1920941
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->y:LX/0ad;

    .line 1920942
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->z:LX/CbR;

    .line 1920943
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->A:LX/0Or;

    .line 1920944
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->B:LX/9fy;

    .line 1920945
    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->C:LX/9yI;

    .line 1920946
    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->D:LX/0Uh;

    .line 1920947
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;
    .locals 30

    .prologue
    .line 1920948
    new-instance v2, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    const/16 v3, 0x15e8

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x2585

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static/range {p0 .. p0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v5

    check-cast v5, LX/17Y;

    invoke-static/range {p0 .. p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {p0 .. p0}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v7

    check-cast v7, LX/1Ck;

    invoke-static/range {p0 .. p0}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v8

    check-cast v8, LX/1Ck;

    invoke-static/range {p0 .. p0}, LX/9hh;->a(LX/0QB;)LX/9hh;

    move-result-object v9

    check-cast v9, LX/9hh;

    invoke-static/range {p0 .. p0}, LX/0kL;->a(LX/0QB;)LX/0kL;

    move-result-object v10

    check-cast v10, LX/0kL;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v11

    check-cast v11, LX/03V;

    invoke-static/range {p0 .. p0}, LX/8Q3;->a(LX/0QB;)LX/8Q3;

    move-result-object v12

    check-cast v12, LX/8Q3;

    const/16 v13, 0xf2b

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static/range {p0 .. p0}, LX/9iB;->a(LX/0QB;)LX/9iB;

    move-result-object v14

    check-cast v14, LX/9iB;

    invoke-static/range {p0 .. p0}, LX/745;->a(LX/0QB;)LX/745;

    move-result-object v15

    check-cast v15, LX/745;

    const/16 v16, 0x2dff

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v16

    invoke-static/range {p0 .. p0}, LX/1No;->a(LX/0QB;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v17

    check-cast v17, Landroid/support/v4/app/FragmentActivity;

    const-class v18, LX/0i4;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v18

    check-cast v18, LX/0i4;

    const/16 v19, 0x3df

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v19

    invoke-static/range {p0 .. p0}, LX/Cbs;->a(LX/0QB;)LX/Cbs;

    move-result-object v20

    check-cast v20, LX/Cbs;

    invoke-static/range {p0 .. p0}, LX/9hJ;->a(LX/0QB;)LX/9hJ;

    move-result-object v21

    check-cast v21, LX/9hJ;

    invoke-static/range {p0 .. p0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v22

    check-cast v22, Ljava/util/concurrent/ExecutorService;

    const-class v23, Landroid/content/Context;

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v24

    check-cast v24, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/CbR;->a(LX/0QB;)LX/CbR;

    move-result-object v25

    check-cast v25, LX/CbR;

    const/16 v26, 0x122d

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v26

    invoke-static/range {p0 .. p0}, LX/9fy;->a(LX/0QB;)LX/9fy;

    move-result-object v27

    check-cast v27, LX/9fy;

    invoke-static/range {p0 .. p0}, LX/9yI;->a(LX/0QB;)LX/9yI;

    move-result-object v28

    check-cast v28, LX/9yI;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v29

    check-cast v29, LX/0Uh;

    invoke-direct/range {v2 .. v29}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;-><init>(LX/0Or;LX/0Or;LX/17Y;Lcom/facebook/content/SecureContextHelper;LX/1Ck;LX/1Ck;LX/9hh;LX/0kL;LX/03V;LX/8Q3;LX/0Ot;LX/9iB;LX/745;LX/0Ot;Landroid/support/v4/app/FragmentActivity;LX/0i4;LX/0Or;LX/Cbs;LX/9hJ;Ljava/util/concurrent/ExecutorService;Landroid/content/Context;LX/0ad;LX/CbR;LX/0Or;LX/9fy;LX/9yI;LX/0Uh;)V

    .line 1920949
    return-object v2
.end method


# virtual methods
.method public final a(Lcom/facebook/base/fragment/FbFragment;Landroid/view/Menu;LX/5kD;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;)V
    .locals 5
    .param p3    # LX/5kD;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1920950
    if-nez p3, :cond_0

    .line 1920951
    invoke-interface {p2}, Landroid/view/Menu;->clear()V

    .line 1920952
    :goto_0
    return-void

    .line 1920953
    :cond_0
    new-instance v0, LX/CcO;

    invoke-direct {v0, p0}, LX/CcO;-><init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;)V

    .line 1920954
    iput-object p1, v0, LX/CcO;->d:Lcom/facebook/base/fragment/FbFragment;

    .line 1920955
    move-object v0, v0

    .line 1920956
    iput-object p2, v0, LX/CcO;->c:Landroid/view/Menu;

    .line 1920957
    move-object v0, v0

    .line 1920958
    iput-object p3, v0, LX/CcO;->e:LX/5kD;

    .line 1920959
    move-object v0, v0

    .line 1920960
    iput-object p4, v0, LX/CcO;->f:Ljava/lang/String;

    .line 1920961
    move-object v0, v0

    .line 1920962
    iput-object p5, v0, LX/CcO;->g:Ljava/lang/String;

    .line 1920963
    move-object v0, v0

    .line 1920964
    iput-object p6, v0, LX/CcO;->h:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;

    .line 1920965
    move-object v2, v0

    .line 1920966
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->l:LX/9hH;

    invoke-virtual {v0}, LX/9hH;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1920967
    iget-object p1, v2, LX/CcO;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 1920968
    iget-object p1, v2, LX/CcO;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1920969
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1920970
    :cond_2
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->E:Landroid/view/Menu;

    if-eqz v0, :cond_3

    .line 1920971
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->E:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->clear()V

    .line 1920972
    :cond_3
    iget-object v0, v2, LX/CcO;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1920973
    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_2

    .line 1920974
    :pswitch_1
    const/16 p2, 0x7d0

    .line 1920975
    iget-object v0, v2, LX/CcO;->c:Landroid/view/Menu;

    invoke-interface {v0, p2}, Landroid/view/Menu;->removeItem(I)V

    .line 1920976
    iget-object v0, v2, LX/CcO;->e:LX/5kD;

    invoke-interface {v0}, LX/5kD;->v()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, v2, LX/CcO;->e:LX/5kD;

    invoke-interface {v0}, LX/5kD;->p()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1920977
    :cond_4
    :goto_3
    goto :goto_2

    .line 1920978
    :pswitch_2
    const/16 p2, 0x7d1

    .line 1920979
    iget-object v0, v2, LX/CcO;->c:Landroid/view/Menu;

    invoke-interface {v0, p2}, Landroid/view/Menu;->removeItem(I)V

    .line 1920980
    iget-object v0, v2, LX/CcO;->e:LX/5kD;

    invoke-interface {v0}, LX/5kD;->u()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1920981
    iget-object v0, v2, LX/CcO;->e:LX/5kD;

    invoke-static {v0}, LX/9hJ;->a(LX/5kD;)Z

    move-result v0

    if-eqz v0, :cond_b

    const/4 v0, 0x1

    .line 1920982
    :goto_4
    if-nez v0, :cond_c

    .line 1920983
    :goto_5
    goto :goto_2

    .line 1920984
    :pswitch_3
    const/16 p2, 0x7d2

    .line 1920985
    iget-object v0, v2, LX/CcO;->c:Landroid/view/Menu;

    invoke-interface {v0, p2}, Landroid/view/Menu;->removeItem(I)V

    .line 1920986
    iget-object v0, v2, LX/CcO;->e:LX/5kD;

    invoke-interface {v0}, LX/5kD;->r()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, v2, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->v:LX/9hJ;

    iget-object v3, v2, LX/CcO;->e:LX/5kD;

    .line 1920987
    invoke-interface {v3}, LX/5kD;->Z()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;

    move-result-object v4

    if-eqz v4, :cond_f

    invoke-interface {v3}, LX/5kD;->Z()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_f

    invoke-interface {v3}, LX/5kD;->Z()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;->c()Ljava/lang/String;

    move-result-object v4

    iget-object p1, v0, LX/9hJ;->a:LX/0Or;

    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p1

    invoke-static {v4, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    const/4 v4, 0x1

    :goto_6
    move v0, v4

    .line 1920988
    if-eqz v0, :cond_d

    const/4 v0, 0x1

    .line 1920989
    :goto_7
    if-nez v0, :cond_e

    .line 1920990
    :goto_8
    goto/16 :goto_2

    .line 1920991
    :pswitch_4
    iget-object v0, v2, LX/CcO;->e:LX/5kD;

    const/16 p5, 0x7d3

    .line 1920992
    iget-object v3, v2, LX/CcO;->c:Landroid/view/Menu;

    invoke-interface {v3, p5}, Landroid/view/Menu;->removeItem(I)V

    .line 1920993
    iget-object v3, v2, LX/CcO;->e:LX/5kD;

    invoke-interface {v3}, LX/5kD;->p()Z

    move-result v3

    if-nez v3, :cond_10

    .line 1920994
    :goto_9
    goto/16 :goto_2

    .line 1920995
    :pswitch_5
    const/16 p2, 0x7d4

    .line 1920996
    iget-object v0, v2, LX/CcO;->c:Landroid/view/Menu;

    invoke-interface {v0, p2}, Landroid/view/Menu;->removeItem(I)V

    .line 1920997
    iget-object v0, v2, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->D:LX/0Uh;

    const/16 v3, 0x45b

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 1920998
    if-eqz v0, :cond_13

    .line 1920999
    iget-object v0, v2, LX/CcO;->e:LX/5kD;

    invoke-interface {v0}, LX/5kD;->x()Z

    move-result v0

    if-nez v0, :cond_14

    .line 1921000
    :cond_5
    :goto_a
    goto/16 :goto_2

    .line 1921001
    :pswitch_6
    const/16 p2, 0x7d5

    .line 1921002
    iget-object v0, v2, LX/CcO;->c:Landroid/view/Menu;

    invoke-interface {v0, p2}, Landroid/view/Menu;->removeItem(I)V

    .line 1921003
    iget-object v0, v2, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->D:LX/0Uh;

    const/16 v3, 0x45a

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 1921004
    if-eqz v0, :cond_15

    .line 1921005
    iget-object v0, v2, LX/CcO;->e:LX/5kD;

    invoke-interface {v0}, LX/5kD;->q()Z

    move-result v0

    if-nez v0, :cond_16

    .line 1921006
    :cond_6
    :goto_b
    goto/16 :goto_2

    .line 1921007
    :pswitch_7
    const/16 p2, 0x7d6

    .line 1921008
    iget-object v0, v2, LX/CcO;->c:Landroid/view/Menu;

    invoke-interface {v0, p2}, Landroid/view/Menu;->removeItem(I)V

    .line 1921009
    iget-object v0, v2, LX/CcO;->e:LX/5kD;

    invoke-interface {v0}, LX/5kD;->t()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 1921010
    iget-object v0, v2, LX/CcO;->e:LX/5kD;

    invoke-static {v0}, LX/9hJ;->a(LX/5kD;)Z

    move-result v0

    if-eqz v0, :cond_17

    const/4 v0, 0x1

    .line 1921011
    :goto_c
    if-nez v0, :cond_18

    .line 1921012
    :goto_d
    goto/16 :goto_2

    .line 1921013
    :pswitch_8
    const/16 p2, 0x7d7

    .line 1921014
    iget-object v0, v2, LX/CcO;->c:Landroid/view/Menu;

    invoke-interface {v0, p2}, Landroid/view/Menu;->removeItem(I)V

    .line 1921015
    iget-object v0, v2, LX/CcO;->e:LX/5kD;

    invoke-interface {v0}, LX/5kD;->z()Z

    move-result v0

    if-eqz v0, :cond_19

    iget-object v0, v2, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->v:LX/9hJ;

    iget-object v3, v2, LX/CcO;->e:LX/5kD;

    const/4 p1, 0x0

    .line 1921016
    invoke-interface {v3}, LX/5kD;->an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v4

    .line 1921017
    if-eqz v4, :cond_7

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;->a()LX/0Px;

    move-result-object p3

    if-nez p3, :cond_1b

    :cond_7
    move v4, p1

    .line 1921018
    :goto_e
    move v0, v4

    .line 1921019
    if-eqz v0, :cond_19

    const/4 v0, 0x1

    .line 1921020
    :goto_f
    if-nez v0, :cond_1a

    .line 1921021
    :goto_10
    goto/16 :goto_2

    .line 1921022
    :pswitch_9
    const/16 p2, 0x7d9

    .line 1921023
    iget-object v0, v2, LX/CcO;->c:Landroid/view/Menu;

    invoke-interface {v0, p2}, Landroid/view/Menu;->removeItem(I)V

    .line 1921024
    iget-object v0, v2, LX/CcO;->e:LX/5kD;

    const/4 v3, 0x1

    .line 1921025
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1921026
    invoke-interface {v0}, LX/5kD;->ae()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$PrivacyScopeModel;

    move-result-object v4

    if-eqz v4, :cond_1f

    invoke-interface {v0}, LX/5kD;->ae()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$PrivacyScopeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$PrivacyScopeModel;->a()Z

    move-result v4

    if-eqz v4, :cond_1f

    .line 1921027
    :cond_8
    :goto_11
    move v0, v3

    .line 1921028
    if-nez v0, :cond_1e

    .line 1921029
    :goto_12
    goto/16 :goto_2

    .line 1921030
    :cond_9
    iget-object v0, v2, LX/CcO;->c:Landroid/view/Menu;

    move-object v0, v0

    .line 1921031
    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->E:Landroid/view/Menu;

    goto/16 :goto_0

    .line 1921032
    :cond_a
    const v0, 0x7f0819ea

    const v3, 0x7f0209ae

    iget-object v4, v2, LX/CcO;->d:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object p1, v2, LX/CcO;->e:LX/5kD;

    invoke-static {v2, v4, p1}, LX/CcO;->a(LX/CcO;Landroid/content/Context;LX/5kD;)Landroid/view/MenuItem$OnMenuItemClickListener;

    move-result-object v4

    iget-object p1, v2, LX/CcO;->c:Landroid/view/Menu;

    invoke-static {p2, v0, v3, v4, p1}, LX/CcO;->a(IIILandroid/view/MenuItem$OnMenuItemClickListener;Landroid/view/Menu;)V

    goto/16 :goto_3

    .line 1921033
    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_4

    .line 1921034
    :cond_c
    const v0, 0x7f0819e7    # 1.809095E38f

    const v3, 0x7f02098f

    iget-object v4, v2, LX/CcO;->d:Lcom/facebook/base/fragment/FbFragment;

    iget-object p1, v2, LX/CcO;->e:LX/5kD;

    invoke-static {v2, v4, p1}, LX/CcO;->a(LX/CcO;Lcom/facebook/base/fragment/FbFragment;LX/5kD;)Landroid/view/MenuItem$OnMenuItemClickListener;

    move-result-object v4

    iget-object p1, v2, LX/CcO;->c:Landroid/view/Menu;

    invoke-static {p2, v0, v3, v4, p1}, LX/CcO;->a(IIILandroid/view/MenuItem$OnMenuItemClickListener;Landroid/view/Menu;)V

    goto/16 :goto_5

    .line 1921035
    :cond_d
    const/4 v0, 0x0

    goto/16 :goto_7

    .line 1921036
    :cond_e
    const v0, 0x7f0819e1

    const v3, 0x7f020952

    iget-object v4, v2, LX/CcO;->d:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v4}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v4

    iget-object p1, v2, LX/CcO;->e:LX/5kD;

    invoke-static {v2, v4, p1}, LX/CcO;->b(LX/CcO;Landroid/content/Context;LX/5kD;)Landroid/view/MenuItem$OnMenuItemClickListener;

    move-result-object v4

    iget-object p1, v2, LX/CcO;->c:Landroid/view/Menu;

    invoke-static {p2, v0, v3, v4, p1}, LX/CcO;->a(IIILandroid/view/MenuItem$OnMenuItemClickListener;Landroid/view/Menu;)V

    goto/16 :goto_8

    :cond_f
    const/4 v4, 0x0

    goto/16 :goto_6

    .line 1921037
    :cond_10
    invoke-interface {v0}, LX/5kD;->S()Z

    move-result v4

    .line 1921038
    if-eqz v4, :cond_11

    const v3, 0x7f0819e5

    :goto_13
    const p1, 0x7f020a0b

    if-eqz v4, :cond_12

    iget-object v4, v2, LX/CcO;->d:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object p2, v2, LX/CcO;->e:LX/5kD;

    iget-object p3, v2, LX/CcO;->f:Ljava/lang/String;

    iget-object p4, v2, LX/CcO;->g:Ljava/lang/String;

    invoke-static {v2, v4, p2, p3, p4}, LX/CcO;->b(LX/CcO;Landroid/content/Context;LX/5kD;Ljava/lang/String;Ljava/lang/String;)Landroid/view/MenuItem$OnMenuItemClickListener;

    move-result-object v4

    :goto_14
    iget-object p2, v2, LX/CcO;->c:Landroid/view/Menu;

    invoke-static {p5, v3, p1, v4, p2}, LX/CcO;->a(IIILandroid/view/MenuItem$OnMenuItemClickListener;Landroid/view/Menu;)V

    goto/16 :goto_9

    :cond_11
    const v3, 0x7f0819e4

    goto :goto_13

    :cond_12
    iget-object v4, v2, LX/CcO;->d:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object p2, v2, LX/CcO;->e:LX/5kD;

    iget-object p3, v2, LX/CcO;->f:Ljava/lang/String;

    iget-object p4, v2, LX/CcO;->g:Ljava/lang/String;

    invoke-static {v2, v4, p2, p3, p4}, LX/CcO;->a(LX/CcO;Landroid/content/Context;LX/5kD;Ljava/lang/String;Ljava/lang/String;)Landroid/view/MenuItem$OnMenuItemClickListener;

    move-result-object v4

    goto :goto_14

    .line 1921039
    :cond_13
    iget-object v0, v2, LX/CcO;->e:LX/5kD;

    invoke-interface {v0}, LX/5kD;->s()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1921040
    :cond_14
    const v0, 0x7f0819e0

    const v3, 0x7f0209c5

    iget-object v4, v2, LX/CcO;->d:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    iget-object v4, v2, LX/CcO;->e:LX/5kD;

    invoke-static {v2, v4}, LX/CcO;->d(LX/CcO;LX/5kD;)Landroid/view/MenuItem$OnMenuItemClickListener;

    move-result-object v4

    iget-object p1, v2, LX/CcO;->c:Landroid/view/Menu;

    invoke-static {p2, v0, v3, v4, p1}, LX/CcO;->a(IIILandroid/view/MenuItem$OnMenuItemClickListener;Landroid/view/Menu;)V

    goto/16 :goto_a

    .line 1921041
    :cond_15
    iget-object v0, v2, LX/CcO;->e:LX/5kD;

    invoke-interface {v0}, LX/5kD;->s()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1921042
    :cond_16
    const v0, 0x7f0819e9

    const v3, 0x7f020841

    iget-object v4, v2, LX/CcO;->e:LX/5kD;

    invoke-static {v2, v4}, LX/CcO;->c(LX/CcO;LX/5kD;)Landroid/view/MenuItem$OnMenuItemClickListener;

    move-result-object v4

    iget-object p1, v2, LX/CcO;->c:Landroid/view/Menu;

    invoke-static {p2, v0, v3, v4, p1}, LX/CcO;->a(IIILandroid/view/MenuItem$OnMenuItemClickListener;Landroid/view/Menu;)V

    goto/16 :goto_b

    .line 1921043
    :cond_17
    const/4 v0, 0x0

    goto/16 :goto_c

    .line 1921044
    :cond_18
    const v0, 0x7f0819e8

    const v3, 0x7f02095e

    iget-object v4, v2, LX/CcO;->d:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object p1, v2, LX/CcO;->e:LX/5kD;

    invoke-static {v2, v4, p1}, LX/CcO;->c(LX/CcO;Landroid/content/Context;LX/5kD;)Landroid/view/MenuItem$OnMenuItemClickListener;

    move-result-object v4

    iget-object p1, v2, LX/CcO;->c:Landroid/view/Menu;

    invoke-static {p2, v0, v3, v4, p1}, LX/CcO;->a(IIILandroid/view/MenuItem$OnMenuItemClickListener;Landroid/view/Menu;)V

    goto/16 :goto_d

    .line 1921045
    :cond_19
    const/4 v0, 0x0

    goto/16 :goto_f

    .line 1921046
    :cond_1a
    const v0, 0x7f0819eb

    const v3, 0x7f020d17

    iget-object v4, v2, LX/CcO;->d:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object p1, v2, LX/CcO;->e:LX/5kD;

    invoke-static {v2, v4, p1}, LX/CcO;->d(LX/CcO;Landroid/content/Context;LX/5kD;)Landroid/view/MenuItem$OnMenuItemClickListener;

    move-result-object v4

    iget-object p1, v2, LX/CcO;->c:Landroid/view/Menu;

    invoke-static {p2, v0, v3, v4, p1}, LX/CcO;->a(IIILandroid/view/MenuItem$OnMenuItemClickListener;Landroid/view/Menu;)V

    goto/16 :goto_10

    .line 1921047
    :cond_1b
    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;->a()LX/0Px;

    move-result-object p4

    invoke-virtual {p4}, LX/0Px;->size()I

    move-result p5

    move p3, p1

    :goto_15
    if-ge p3, p5, :cond_1d

    invoke-virtual {p4, p3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;

    .line 1921048
    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v4

    .line 1921049
    if-eqz v4, :cond_1c

    .line 1921050
    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v4

    iget-object p6, v0, LX/9hJ;->a:LX/0Or;

    invoke-interface {p6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p6

    invoke-static {v4, p6}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1c

    .line 1921051
    const/4 v4, 0x1

    goto/16 :goto_e

    .line 1921052
    :cond_1c
    add-int/lit8 v4, p3, 0x1

    move p3, v4

    goto :goto_15

    :cond_1d
    move v4, p1

    .line 1921053
    goto/16 :goto_e

    .line 1921054
    :cond_1e
    iget-object v0, v2, LX/CcO;->e:LX/5kD;

    .line 1921055
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1921056
    invoke-interface {v0}, LX/5kD;->ae()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$PrivacyScopeModel;

    move-result-object v3

    if-eqz v3, :cond_21

    invoke-interface {v0}, LX/5kD;->ae()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$PrivacyScopeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$PrivacyScopeModel;->a()Z

    move-result v3

    if-eqz v3, :cond_21

    .line 1921057
    const v3, 0x7f0819e2

    .line 1921058
    :goto_16
    move v0, v3

    .line 1921059
    const v3, 0x7f02098b

    iget-object v4, v2, LX/CcO;->d:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object p1, v2, LX/CcO;->e:LX/5kD;

    invoke-static {v2, v4, p1}, LX/CcO;->e(LX/CcO;Landroid/content/Context;LX/5kD;)Landroid/view/MenuItem$OnMenuItemClickListener;

    move-result-object v4

    iget-object p1, v2, LX/CcO;->c:Landroid/view/Menu;

    invoke-static {p2, v0, v3, v4, p1}, LX/CcO;->a(IIILandroid/view/MenuItem$OnMenuItemClickListener;Landroid/view/Menu;)V

    goto/16 :goto_12

    .line 1921060
    :cond_1f
    invoke-interface {v0}, LX/5kD;->A()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;

    move-result-object v4

    if-eqz v4, :cond_20

    invoke-interface {v0}, LX/5kD;->A()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;->e()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel;

    move-result-object v4

    if-eqz v4, :cond_20

    invoke-interface {v0}, LX/5kD;->A()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;->e()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel;->a()Z

    move-result v4

    if-nez v4, :cond_8

    .line 1921061
    :cond_20
    const/4 v3, 0x0

    goto/16 :goto_11

    .line 1921062
    :cond_21
    invoke-interface {v0}, LX/5kD;->A()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;

    move-result-object v3

    if-eqz v3, :cond_22

    invoke-interface {v0}, LX/5kD;->A()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;->e()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel;

    move-result-object v3

    if-eqz v3, :cond_22

    invoke-interface {v0}, LX/5kD;->A()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;->e()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel;->a()Z

    move-result v3

    if-eqz v3, :cond_22

    .line 1921063
    const v3, 0x7f0819e3

    goto :goto_16

    .line 1921064
    :cond_22
    const v3, 0x7f0819e2

    goto :goto_16

    nop

    :pswitch_data_0
    .packed-switch 0x7d0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_9
    .end packed-switch
.end method
