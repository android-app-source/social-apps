.class public Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/CaH;
.implements LX/CaJ;


# static fields
.field private static final g:Ljava/lang/String;

.field private static final h:Lcom/facebook/common/callercontext/CallerContext;

.field public static final i:LX/1Up;


# instance fields
.field public a:LX/Cb0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Cbe;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/CaK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/9eS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1HI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/23g;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:LX/Caz;

.field private o:LX/9eR;

.field private p:LX/CaY;

.field public q:I

.field public r:Ljava/lang/String;

.field private s:Z

.field public t:Ljava/lang/String;

.field public u:LX/1Up;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/graphql/model/GraphQLStory;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1917905
    const-class v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->g:Ljava/lang/String;

    .line 1917906
    const-class v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;

    sget-object v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->g:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->h:Lcom/facebook/common/callercontext/CallerContext;

    .line 1917907
    sget-object v0, LX/1Up;->c:LX/1Up;

    sput-object v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->i:LX/1Up;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1917956
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1917957
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1918043
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/5kD;)V
    .locals 10

    .prologue
    .line 1917958
    invoke-interface {p1}, LX/1U8;->d()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1917959
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->m:Landroid/net/Uri;

    .line 1917960
    :cond_0
    :goto_0
    move-object v2, v0

    .line 1917961
    invoke-interface {p1}, LX/5kD;->aj_()LX/1Fb;

    move-result-object v0

    if-nez v0, :cond_b

    move-object v1, v2

    .line 1917962
    :goto_1
    if-eqz v1, :cond_1

    iget-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->s:Z

    if-nez v0, :cond_2

    if-nez v2, :cond_2

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->e:LX/1HI;

    invoke-virtual {v0, v1}, LX/1HI;->b(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1917963
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->j:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02145d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    sget-object v4, LX/1Up;->f:LX/1Up;

    invoke-virtual {v0, v3, v4}, LX/1af;->a(Landroid/graphics/drawable/Drawable;LX/1Up;)V

    .line 1917964
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->j:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02145d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    sget-object v4, LX/1Up;->f:LX/1Up;

    invoke-virtual {v0, v3, v4}, LX/1af;->b(Landroid/graphics/drawable/Drawable;LX/1Up;)V

    .line 1917965
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->s:Z

    .line 1917966
    :cond_2
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->j:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

    sget-object v3, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->h:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2, v1, v3}, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->a(Landroid/net/Uri;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1917967
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->n:LX/Caz;

    if-eqz v0, :cond_a

    .line 1917968
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->n:LX/Caz;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1917969
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1917970
    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 1917971
    iget-object v1, v0, LX/Caz;->q:LX/5kD;

    if-nez v1, :cond_10

    move v6, v7

    .line 1917972
    :cond_3
    :goto_2
    move v1, v6

    .line 1917973
    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 1917974
    iget-object v4, v0, LX/Caz;->q:LX/5kD;

    if-nez v4, :cond_16

    move v6, v7

    .line 1917975
    :cond_4
    :goto_3
    move v4, v6

    .line 1917976
    invoke-static {v0, p1}, LX/Caz;->e(LX/Caz;LX/5kD;)Z

    move-result v5

    .line 1917977
    iput-object p1, v0, LX/Caz;->q:LX/5kD;

    .line 1917978
    if-nez v4, :cond_5

    if-nez v1, :cond_5

    if-eqz v5, :cond_a

    .line 1917979
    :cond_5
    const/4 v4, 0x0

    .line 1917980
    if-eqz p1, :cond_6

    invoke-interface {p1}, LX/5kD;->an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-interface {p1}, LX/5kD;->an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;->a()LX/0Px;

    move-result-object v1

    if-nez v1, :cond_1c

    :cond_6
    move v1, v4

    .line 1917981
    :goto_4
    move v1, v1

    .line 1917982
    if-eqz v1, :cond_f

    iget-object v1, v0, LX/Caz;->x:LX/0ad;

    sget-short v4, LX/1xL;->c:S

    invoke-interface {v1, v4, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_f

    move v1, v2

    .line 1917983
    :goto_5
    iget-object v4, v0, LX/Caz;->c:LX/Cb1;

    if-nez v1, :cond_7

    move v3, v2

    :cond_7
    invoke-virtual {v4, p1, v3}, LX/Cb1;->a(LX/5kD;Z)V

    .line 1917984
    iget-object v3, v0, LX/Caz;->e:LX/CbH;

    .line 1917985
    iput-boolean v1, v3, LX/CbH;->g:Z

    .line 1917986
    iget-object v3, v0, LX/Caz;->s:LX/Cb5;

    if-eqz v3, :cond_8

    .line 1917987
    iget-object v3, v0, LX/Caz;->s:LX/Cb5;

    .line 1917988
    iget-object v4, v3, LX/Cb5;->e:LX/CbL;

    invoke-virtual {v4}, LX/CbL;->c()LX/CbN;

    move-result-object v6

    .line 1917989
    iget-object v4, v3, LX/Cb5;->l:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    invoke-virtual {v4}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->isShown()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1917990
    iget-boolean v4, v6, LX/CbN;->a:Z

    move v4, v4

    .line 1917991
    if-nez v4, :cond_1f

    .line 1917992
    :cond_8
    if-eqz v1, :cond_9

    invoke-interface {p1}, LX/5kD;->r()Z

    move-result v1

    if-nez v1, :cond_9

    .line 1917993
    iget-object v1, v0, LX/Caz;->e:LX/CbH;

    invoke-virtual {v1}, LX/CbH;->d()V

    .line 1917994
    :cond_9
    iget-object v1, v0, LX/Caz;->e:LX/CbH;

    .line 1917995
    iget-boolean v3, v1, LX/CbH;->c:Z

    move v1, v3

    .line 1917996
    if-eqz v1, :cond_a

    .line 1917997
    invoke-static {v0}, LX/Caz;->m$redex0(LX/Caz;)V

    .line 1917998
    invoke-static {v0, v2}, LX/Caz;->b$redex0(LX/Caz;Z)V

    .line 1917999
    :cond_a
    invoke-interface {p1}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->k:Ljava/lang/String;

    .line 1918000
    return-void

    .line 1918001
    :cond_b
    invoke-interface {p1}, LX/5kD;->aj_()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_1

    .line 1918002
    :cond_c
    invoke-interface {p1}, LX/1U8;->j()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 1918003
    invoke-interface {p1}, LX/1U8;->j()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1918004
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->e:LX/1HI;

    invoke-virtual {v1, v0}, LX/1HI;->b(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1918005
    :cond_d
    invoke-interface {p1}, LX/1U8;->ai_()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 1918006
    invoke-interface {p1}, LX/1U8;->ai_()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1918007
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->e:LX/1HI;

    invoke-virtual {v1, v0}, LX/1HI;->b(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1918008
    :cond_e
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_f
    move v1, v3

    .line 1918009
    goto/16 :goto_5

    .line 1918010
    :cond_10
    iget-object v1, v0, LX/Caz;->q:LX/5kD;

    invoke-interface {v1}, LX/5kD;->F()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    move-result-object v1

    if-nez v1, :cond_11

    invoke-interface {p1}, LX/5kD;->F()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1918011
    :cond_11
    iget-object v1, v0, LX/Caz;->q:LX/5kD;

    invoke-interface {v1}, LX/5kD;->F()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    move-result-object v1

    if-eqz v1, :cond_12

    invoke-interface {p1}, LX/5kD;->F()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    move-result-object v1

    if-nez v1, :cond_13

    :cond_12
    move v6, v7

    .line 1918012
    goto/16 :goto_2

    .line 1918013
    :cond_13
    iget-object v1, v0, LX/Caz;->q:LX/5kD;

    invoke-interface {v1}, LX/5kD;->F()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    invoke-interface {p1}, LX/5kD;->F()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    if-eq v1, v4, :cond_14

    move v6, v7

    .line 1918014
    goto/16 :goto_2

    :cond_14
    move v5, v6

    .line 1918015
    :goto_6
    iget-object v1, v0, LX/Caz;->q:LX/5kD;

    invoke-interface {v1}, LX/5kD;->F()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v5, v1, :cond_3

    .line 1918016
    iget-object v1, v0, LX/Caz;->q:LX/5kD;

    invoke-interface {v1}, LX/5kD;->F()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;

    .line 1918017
    invoke-interface {p1}, LX/5kD;->F()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;

    .line 1918018
    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_15

    move v6, v7

    .line 1918019
    goto/16 :goto_2

    .line 1918020
    :cond_15
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_6

    .line 1918021
    :cond_16
    iget-object v4, v0, LX/Caz;->q:LX/5kD;

    invoke-interface {v4}, LX/5kD;->an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v4

    if-nez v4, :cond_17

    invoke-interface {p1}, LX/5kD;->an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 1918022
    :cond_17
    iget-object v4, v0, LX/Caz;->q:LX/5kD;

    invoke-interface {v4}, LX/5kD;->an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v4

    if-eqz v4, :cond_18

    invoke-interface {p1}, LX/5kD;->an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v4

    if-nez v4, :cond_19

    :cond_18
    move v6, v7

    .line 1918023
    goto/16 :goto_3

    .line 1918024
    :cond_19
    iget-object v4, v0, LX/Caz;->q:LX/5kD;

    invoke-interface {v4}, LX/5kD;->an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    invoke-interface {p1}, LX/5kD;->an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    if-eq v4, v5, :cond_1a

    move v6, v7

    .line 1918025
    goto/16 :goto_3

    :cond_1a
    move v5, v6

    .line 1918026
    :goto_7
    iget-object v4, v0, LX/Caz;->q:LX/5kD;

    invoke-interface {v4}, LX/5kD;->an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    if-ge v5, v4, :cond_4

    .line 1918027
    iget-object v4, v0, LX/Caz;->q:LX/5kD;

    invoke-interface {v4}, LX/5kD;->an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v8

    invoke-interface {p1}, LX/5kD;->an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1b

    move v6, v7

    .line 1918028
    goto/16 :goto_3

    .line 1918029
    :cond_1b
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_7

    .line 1918030
    :cond_1c
    invoke-interface {p1}, LX/5kD;->an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v6

    move v5, v4

    .line 1918031
    :goto_8
    if-ge v5, v6, :cond_1e

    .line 1918032
    invoke-interface {p1}, LX/5kD;->an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;

    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    .line 1918033
    if-eqz v1, :cond_1d

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v7, 0xa7c5482

    if-ne v1, v7, :cond_1d

    .line 1918034
    const/4 v1, 0x1

    goto/16 :goto_4

    .line 1918035
    :cond_1d
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_8

    :cond_1e
    move v1, v4

    .line 1918036
    goto/16 :goto_4

    .line 1918037
    :cond_1f
    invoke-interface {p1}, LX/5kD;->F()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    const/4 v4, 0x0

    move v5, v4

    :goto_9
    if-ge v5, v8, :cond_8

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;

    .line 1918038
    iget-object v9, v6, LX/CbN;->b:LX/0am;

    move-object v9, v9

    .line 1918039
    invoke-virtual {v9}, LX/0am;->isPresent()Z

    move-result v9

    if-eqz v9, :cond_20

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;->d()Ljava/lang/String;

    move-result-object v4

    .line 1918040
    iget-object v9, v6, LX/CbN;->b:LX/0am;

    move-object v9, v9

    .line 1918041
    invoke-virtual {v9}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 1918042
    :cond_20
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_9
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 1917945
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1917946
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;

    const-class v3, LX/Cb0;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/Cb0;

    invoke-static {v0}, LX/Cbe;->a(LX/0QB;)LX/Cbe;

    move-result-object v4

    check-cast v4, LX/Cbe;

    invoke-static {v0}, LX/CaK;->a(LX/0QB;)LX/CaK;

    move-result-object v5

    check-cast v5, LX/CaK;

    invoke-static {v0}, LX/9eS;->a(LX/0QB;)LX/9eS;

    move-result-object v6

    check-cast v6, LX/9eS;

    invoke-static {v0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v7

    check-cast v7, LX/1HI;

    invoke-static {v0}, LX/23g;->a(LX/0QB;)LX/23g;

    move-result-object v0

    check-cast v0, LX/23g;

    iput-object v3, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->a:LX/Cb0;

    iput-object v4, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->b:LX/Cbe;

    iput-object v5, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->c:LX/CaK;

    iput-object v6, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->d:LX/9eS;

    iput-object v7, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->e:LX/1HI;

    iput-object v0, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->f:LX/23g;

    .line 1917947
    if-eqz p1, :cond_0

    .line 1917948
    const-string v0, "EXTRA_MEDIA_ID"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->k:Ljava/lang/String;

    .line 1917949
    :cond_0
    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 1917950
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1917951
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->n:LX/Caz;

    if-eqz v0, :cond_0

    .line 1917952
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->n:LX/Caz;

    .line 1917953
    iget-object p0, v0, LX/Caz;->s:LX/Cb5;

    if-eqz p0, :cond_0

    iget-object p0, v0, LX/Caz;->s:LX/Cb5;

    invoke-virtual {p0}, LX/Cb5;->isShowing()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1917954
    iget-object p0, v0, LX/Caz;->s:LX/Cb5;

    invoke-virtual {p0}, LX/Cb5;->dismiss()V

    .line 1917955
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x60ae4e04

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1917944
    const v1, 0x7f030a89

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x47656a06

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x12e58915

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1917933
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->c:LX/CaK;

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/CaK;->a(Ljava/lang/String;)V

    .line 1917934
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->n:LX/Caz;

    .line 1917935
    iget-object v2, v1, LX/Caz;->e:LX/CbH;

    invoke-virtual {v2, v1}, LX/CbH;->b(LX/Cay;)V

    .line 1917936
    iget-object v2, v1, LX/Caz;->i:LX/1Ck;

    invoke-virtual {v2}, LX/1Ck;->c()V

    .line 1917937
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->j:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->b:LX/Cbe;

    .line 1917938
    iget-object v4, v2, LX/Cbe;->a:LX/7US;

    move-object v2, v4

    .line 1917939
    invoke-virtual {v1, v2}, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->b(LX/7US;)V

    .line 1917940
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->j:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->p:LX/CaY;

    invoke-virtual {v1, v2}, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->b(LX/1Ai;)V

    .line 1917941
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->d:LX/9eS;

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->o:LX/9eR;

    invoke-virtual {v1, v2}, LX/9eS;->b(LX/9eR;)V

    .line 1917942
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 1917943
    const/16 v1, 0x2b

    const v2, 0x2d89b536

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1917930
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1917931
    const-string v0, "EXTRA_MEDIA_ID"

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1917932
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1917914
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1917915
    const v0, 0x7f0d1af3

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->j:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

    .line 1917916
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1917917
    new-instance v1, LX/1Uo;

    invoke-direct {v1, v0}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->u:LX/1Up;

    invoke-virtual {v1, v0}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v0

    .line 1917918
    iput-boolean v2, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->s:Z

    .line 1917919
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->j:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 1917920
    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->j:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->b:LX/Cbe;

    .line 1917921
    iget-object v2, v0, LX/Cbe;->a:LX/7US;

    move-object v0, v2

    .line 1917922
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7US;

    invoke-virtual {v1, v0}, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->a(LX/7US;)V

    .line 1917923
    new-instance v0, LX/CaY;

    invoke-direct {v0, p0}, LX/CaY;-><init>(Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->p:LX/CaY;

    .line 1917924
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->j:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->p:LX/CaY;

    invoke-virtual {v0, v1}, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->a(LX/1Ai;)V

    .line 1917925
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->a:LX/Cb0;

    move-object v1, p1

    check-cast v1, Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->j:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

    iget v3, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->q:I

    iget-object v4, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->r:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->t:Ljava/lang/String;

    iget-object v6, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->v:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual/range {v0 .. v6}, LX/Cb0;->a(Landroid/widget/FrameLayout;Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;ILjava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)LX/Caz;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->n:LX/Caz;

    .line 1917926
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->c:LX/CaK;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, LX/CaK;->a(Ljava/lang/String;LX/CaJ;)V

    .line 1917927
    new-instance v0, LX/CaX;

    invoke-direct {v0, p0}, LX/CaX;-><init>(Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->o:LX/9eR;

    .line 1917928
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->d:LX/9eS;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->o:LX/9eR;

    invoke-virtual {v0, v1}, LX/9eS;->a(LX/9eR;)V

    .line 1917929
    return-void
.end method

.method public final setUserVisibleHint(Z)V
    .locals 3

    .prologue
    .line 1917908
    :try_start_0
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->setUserVisibleHint(Z)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1917909
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->j:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

    if-eqz v0, :cond_0

    .line 1917910
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->j:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

    invoke-virtual {v0}, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->getZoomableController()LX/5ue;

    move-result-object v0

    invoke-virtual {v0}, LX/5ua;->a()V

    .line 1917911
    :cond_0
    :goto_0
    return-void

    .line 1917912
    :catch_0
    move-exception v0

    .line 1917913
    sget-object v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->g:Ljava/lang/String;

    const-string v2, "setUserVisibleHint failed FragmentManager is null"

    invoke-static {v1, v2, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
