.class public final Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment$5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)V
    .locals 0

    .prologue
    .line 1917472
    iput-object p1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment$5;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 1917473
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment$5;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-boolean v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->ab:Z

    if-eqz v0, :cond_1

    .line 1917474
    :cond_0
    :goto_0
    return-void

    .line 1917475
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment$5;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8JL;

    invoke-virtual {v0}, LX/8JL;->b()V

    .line 1917476
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment$5;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cao;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment$5;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->z:Ljava/lang/String;

    .line 1917477
    iput-object v1, v0, LX/Cao;->f:Ljava/lang/String;

    .line 1917478
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment$5;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cao;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment$5;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->H:Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;->y:Ljava/lang/String;

    .line 1917479
    iput-object v1, v0, LX/Cao;->g:Ljava/lang/String;

    .line 1917480
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment$5;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cao;

    invoke-virtual {v0}, LX/Cao;->b()V

    .line 1917481
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment$5;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->O:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment$5;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->P:LX/CaZ;

    invoke-virtual {v1}, LX/0gG;->b()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1917482
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment$5;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-static {v0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->y(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1917483
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment$5;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->k:LX/CcX;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment$5;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->P:LX/CaZ;

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment$5;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v2, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->O:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    invoke-virtual {v1, v2}, LX/CaZ;->e(I)LX/5kD;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->c(LX/5kD;)Ljava/lang/String;

    .line 1917484
    iget-object v2, v0, LX/CcX;->c:Ljava/util/WeakHashMap;

    invoke-virtual {v2}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Caf;

    .line 1917485
    if-eqz v2, :cond_2

    .line 1917486
    iget-object v0, v2, LX/Caf;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    invoke-static {v0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->e(Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;)V

    .line 1917487
    goto :goto_1

    .line 1917488
    :cond_3
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment$5;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-static {v0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->z(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1917489
    iget-object v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment$5;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->l:LX/CcS;

    iget-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment$5;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->P:LX/CaZ;

    iget-object v2, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment$5;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v2, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->O:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    invoke-virtual {v1, v2}, LX/CaZ;->e(I)LX/5kD;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->c(LX/5kD;)Ljava/lang/String;

    move-result-object v1

    .line 1917490
    iget-object v2, v0, LX/CcS;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v2}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Caa;

    .line 1917491
    if-eqz v2, :cond_4

    .line 1917492
    iget-object p0, v2, LX/Caa;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;

    invoke-static {p0, v1}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->a$redex0(Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_5

    iget-object p0, v2, LX/Caa;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;

    iget-boolean p0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->o:Z

    if-nez p0, :cond_5

    .line 1917493
    iget-object p0, v2, LX/Caa;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;

    const/4 v0, 0x1

    .line 1917494
    iput-boolean v0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->o:Z

    .line 1917495
    iget-object p0, v2, LX/Caa;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;

    iget-object p0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->i:LX/CaV;

    invoke-virtual {p0}, LX/CaV;->r()V

    .line 1917496
    :cond_5
    goto :goto_2

    .line 1917497
    :cond_6
    goto/16 :goto_0
.end method
