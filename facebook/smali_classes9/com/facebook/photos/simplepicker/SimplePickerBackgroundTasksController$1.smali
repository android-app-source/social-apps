.class public final Lcom/facebook/photos/simplepicker/SimplePickerBackgroundTasksController$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/BGb;


# direct methods
.method public constructor <init>(LX/BGb;)V
    .locals 0

    .prologue
    .line 1767254
    iput-object p1, p0, Lcom/facebook/photos/simplepicker/SimplePickerBackgroundTasksController$1;->a:LX/BGb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    .line 1767255
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerBackgroundTasksController$1;->a:LX/BGb;

    iget-object v0, v0, LX/BGb;->e:LX/75S;

    .line 1767256
    const-string v2, "%s <= %s AND %s > 0"

    const-string v3, "image_hash"

    iget-object v1, v0, LX/75S;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v5

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v7, 0x7

    sget-object v4, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v7, v8, v4}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v7

    sub-long/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v4, "image_hash"

    invoke-static {v2, v3, v1, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1767257
    iget-object v1, v0, LX/75S;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6Z0;

    iget-object v3, v1, LX/6Z0;->d:Landroid/net/Uri;

    .line 1767258
    iget-object v1, v0, LX/75S;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/ContentResolver;

    invoke-virtual {v1, v3}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v4

    .line 1767259
    if-eqz v4, :cond_0

    .line 1767260
    iget-object v1, v0, LX/75S;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/ContentResolver;

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v2, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1767261
    invoke-virtual {v4}, Landroid/content/ContentProviderClient;->release()Z

    .line 1767262
    :cond_0
    return-void
.end method
