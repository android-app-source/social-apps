.class public final Lcom/facebook/photos/simplepicker/SimplePickerBackgroundTasksController$5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:J

.field public final synthetic b:LX/BGb;


# direct methods
.method public constructor <init>(LX/BGb;J)V
    .locals 0

    .prologue
    .line 1767294
    iput-object p1, p0, Lcom/facebook/photos/simplepicker/SimplePickerBackgroundTasksController$5;->b:LX/BGb;

    iput-wide p2, p0, Lcom/facebook/photos/simplepicker/SimplePickerBackgroundTasksController$5;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 1767279
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerBackgroundTasksController$5;->b:LX/BGb;

    iget-object v0, v0, LX/BGb;->g:LX/BHQ;

    const/4 v4, 0x0

    .line 1767280
    iget-object v1, v0, LX/BHQ;->d:LX/8I2;

    sget-object v2, LX/4gI;->VIDEO_ONLY:LX/4gI;

    invoke-virtual {v1, v2, v4}, LX/8I2;->a(LX/4gI;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1767281
    iget-object v2, v0, LX/BHQ;->d:LX/8I2;

    sget-object v3, LX/4gI;->ALL:LX/4gI;

    invoke-virtual {v2, v3, v4}, LX/8I2;->a(LX/4gI;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 1767282
    iget-object v3, v0, LX/BHQ;->i:LX/BHs;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v5

    .line 1767283
    iget-boolean v6, v3, LX/BHs;->c:Z

    if-eqz v6, :cond_0

    .line 1767284
    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v7, "composer_asset_count"

    invoke-direct {v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1767285
    const-string v7, "reachability_status"

    iget-object v0, v3, LX/BHs;->b:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v7, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1767286
    const-string v7, "num_video_assets"

    invoke-virtual {v6, v7, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1767287
    const-string v7, "num_assets"

    invoke-virtual {v6, v7, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1767288
    iget-object v7, v3, LX/BHs;->a:LX/0Zb;

    invoke-interface {v7, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1767289
    const/4 v6, 0x0

    iput-boolean v6, v3, LX/BHs;->c:Z

    .line 1767290
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1767291
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1767292
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerBackgroundTasksController$5;->b:LX/BGb;

    iget-object v0, v0, LX/BGb;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/1Ip;->i:LX/0Tn;

    iget-wide v2, p0, Lcom/facebook/photos/simplepicker/SimplePickerBackgroundTasksController$5;->a:J

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1767293
    return-void
.end method
