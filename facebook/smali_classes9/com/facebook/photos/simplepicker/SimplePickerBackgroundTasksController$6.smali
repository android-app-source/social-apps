.class public final Lcom/facebook/photos/simplepicker/SimplePickerBackgroundTasksController$6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/BGb;


# direct methods
.method public constructor <init>(LX/BGb;)V
    .locals 0

    .prologue
    .line 1767298
    iput-object p1, p0, Lcom/facebook/photos/simplepicker/SimplePickerBackgroundTasksController$6;->a:LX/BGb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 12

    .prologue
    .line 1767299
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerBackgroundTasksController$6;->a:LX/BGb;

    iget-object v0, v0, LX/BGb;->l:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, LX/1lQ;->m(J)J

    move-result-wide v0

    .line 1767300
    iget-object v2, p0, Lcom/facebook/photos/simplepicker/SimplePickerBackgroundTasksController$6;->a:LX/BGb;

    iget-object v2, v2, LX/BGb;->j:LX/8I4;

    .line 1767301
    const-wide/16 v10, -0x1

    .line 1767302
    :try_start_0
    const-string v9, "_id desc"

    .line 1767303
    iget-object v4, v2, LX/8I4;->b:Landroid/content/ContentResolver;

    sget-object v5, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "date_added"

    aput-object v8, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1767304
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1767305
    const/4 v4, 0x0

    invoke-interface {v6, v4}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    .line 1767306
    :goto_0
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1767307
    :goto_1
    move-wide v2, v4

    .line 1767308
    sub-long/2addr v0, v2

    const-wide/32 v2, 0x3f480

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 1767309
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerBackgroundTasksController$6;->a:LX/BGb;

    iget-object v0, v0, LX/BGb;->d:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/photos/simplepicker/SimplePickerBackgroundTasksController$6$1;

    invoke-direct {v1, p0}, Lcom/facebook/photos/simplepicker/SimplePickerBackgroundTasksController$6$1;-><init>(Lcom/facebook/photos/simplepicker/SimplePickerBackgroundTasksController$6;)V

    const v2, 0x6331e3eb

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1767310
    :cond_0
    return-void

    .line 1767311
    :catch_0
    move-exception v4

    move-object v6, v4

    move-wide v4, v10

    .line 1767312
    :goto_2
    iget-object v7, v2, LX/8I4;->c:LX/03V;

    const-string v8, "MediaCursorUtil: fetchLatestVideoAddedTime failed"

    const-string v9, "error creating cursor"

    invoke-virtual {v7, v8, v9, v6}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1767313
    :catch_1
    move-exception v6

    goto :goto_2

    :cond_1
    move-wide v4, v10

    goto :goto_0
.end method
