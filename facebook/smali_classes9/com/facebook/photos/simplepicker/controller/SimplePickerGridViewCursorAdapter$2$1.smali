.class public final Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/ipc/media/MediaItem;

.field public final synthetic b:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2;Lcom/facebook/ipc/media/MediaItem;)V
    .locals 0

    .prologue
    .line 1769043
    iput-object p1, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2$1;->b:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2;

    iput-object p2, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2$1;->a:Lcom/facebook/ipc/media/MediaItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 1769044
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2$1;->b:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2;

    iget-object v0, v0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2;->e:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;

    iget-object v0, v0, LX/3tK;->c:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2$1;->b:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2;

    iget-object v0, v0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2;->e:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;

    iget-object v0, v0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->m:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2$1;->b:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2;

    iget-object v0, v0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2;->d:LX/BIG;

    const v1, 0x7f0d00d2

    invoke-virtual {v0, v1}, LX/BIG;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2$1;->b:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2;

    iget v1, v1, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2;->c:I

    if-ne v0, v1, :cond_0

    .line 1769045
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2$1;->b:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2;

    iget-object v0, v0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2;->e:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;

    iget-object v1, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2$1;->b:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2;

    iget-object v1, v1, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2;->d:LX/BIG;

    iget-object v2, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2$1;->a:Lcom/facebook/ipc/media/MediaItem;

    iget-object v3, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2$1;->b:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2;

    iget v3, v3, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2;->c:I

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->a$redex0(Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;LX/BIG;Lcom/facebook/ipc/media/MediaItem;I)V

    .line 1769046
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2$1;->b:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2;

    iget-object v0, v0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2;->e:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;

    iget-object v0, v0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->H:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1769047
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2$1;->b:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2;

    iget-object v0, v0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2;->e:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;

    iget-object v0, v0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->F:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1769048
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2$1;->b:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2;

    iget-object v0, v0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2;->e:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;

    iget-object v0, v0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->F:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BHW;

    .line 1769049
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2$1;->b:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2;

    iget-object v1, v1, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2;->e:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;

    iget-wide v2, v0, LX/BHW;->a:J

    iget-object v4, v0, LX/BHW;->b:Landroid/database/Cursor;

    iget v5, v0, LX/BHW;->c:I

    iget-object v6, v0, LX/BHW;->d:LX/BIG;

    .line 1769050
    invoke-static/range {v1 .. v6}, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->a$redex0(Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;JLandroid/database/Cursor;ILX/BIG;)V

    .line 1769051
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2$1;->b:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2;

    iget-object v1, v1, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2;->e:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;

    iget-object v1, v1, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->G:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 1769052
    :cond_1
    return-void
.end method
