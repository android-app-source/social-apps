.class public Lcom/facebook/photos/simplepicker/controller/RecognitionManager;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final h:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:Lcom/facebook/objectrec/manager/ObjectRecManager;

.field private final c:LX/1HI;

.field private final d:Ljava/util/concurrent/ExecutorService;

.field public final e:LX/03V;

.field public final f:LX/0wL;

.field public final g:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1769000
    const-class v0, LX/BHy;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/simplepicker/controller/RecognitionManager;->h:Lcom/facebook/common/callercontext/CallerContext;

    .line 1769001
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/facebook/photos/simplepicker/controller/RecognitionManager;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/objectrec/manager/ObjectRecManager;LX/1HI;Ljava/util/concurrent/ExecutorService;LX/03V;LX/0wL;Landroid/content/Context;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1768992
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1768993
    iput-object p2, p0, Lcom/facebook/photos/simplepicker/controller/RecognitionManager;->c:LX/1HI;

    .line 1768994
    iput-object p3, p0, Lcom/facebook/photos/simplepicker/controller/RecognitionManager;->d:Ljava/util/concurrent/ExecutorService;

    .line 1768995
    iput-object p1, p0, Lcom/facebook/photos/simplepicker/controller/RecognitionManager;->b:Lcom/facebook/objectrec/manager/ObjectRecManager;

    .line 1768996
    iput-object p4, p0, Lcom/facebook/photos/simplepicker/controller/RecognitionManager;->e:LX/03V;

    .line 1768997
    iput-object p5, p0, Lcom/facebook/photos/simplepicker/controller/RecognitionManager;->f:LX/0wL;

    .line 1768998
    iput-object p6, p0, Lcom/facebook/photos/simplepicker/controller/RecognitionManager;->g:Landroid/content/Context;

    .line 1768999
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/photos/simplepicker/controller/RecognitionManager;
    .locals 1

    .prologue
    .line 1768964
    invoke-static {p0}, Lcom/facebook/photos/simplepicker/controller/RecognitionManager;->b(LX/0QB;)Lcom/facebook/photos/simplepicker/controller/RecognitionManager;

    move-result-object v0

    return-object v0
.end method

.method public static a(J)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1768989
    sget-object v0, Lcom/facebook/photos/simplepicker/controller/RecognitionManager;->a:Ljava/util/Map;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1768990
    if-eqz v0, :cond_0

    .line 1768991
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)Lcom/facebook/photos/simplepicker/controller/RecognitionManager;
    .locals 7

    .prologue
    .line 1768980
    new-instance v0, Lcom/facebook/photos/simplepicker/controller/RecognitionManager;

    .line 1768981
    new-instance v2, Lcom/facebook/objectrec/manager/ObjectRecManager;

    .line 1768982
    new-instance v1, LX/BEB;

    invoke-direct {v1}, LX/BEB;-><init>()V

    .line 1768983
    move-object v1, v1

    .line 1768984
    move-object v1, v1

    .line 1768985
    check-cast v1, LX/BEB;

    const/16 v3, 0xb83

    invoke-static {p0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Lcom/facebook/objectrec/manager/ObjectRecManager;-><init>(LX/BEB;LX/0Or;)V

    .line 1768986
    move-object v1, v2

    .line 1768987
    check-cast v1, Lcom/facebook/objectrec/manager/ObjectRecManager;

    invoke-static {p0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v2

    check-cast v2, LX/1HI;

    invoke-static {p0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {p0}, LX/0wL;->a(LX/0QB;)LX/0wL;

    move-result-object v5

    check-cast v5, LX/0wL;

    const-class v6, Landroid/content/Context;

    invoke-interface {p0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-direct/range {v0 .. v6}, Lcom/facebook/photos/simplepicker/controller/RecognitionManager;-><init>(Lcom/facebook/objectrec/manager/ObjectRecManager;LX/1HI;Ljava/util/concurrent/ExecutorService;LX/03V;LX/0wL;Landroid/content/Context;)V

    .line 1768988
    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a(LX/BIG;)V
    .locals 6

    .prologue
    .line 1768965
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, LX/BIG;->getItemType()LX/BHH;

    move-result-object v0

    sget-object v1, LX/BHH;->PHOTO:LX/BHH;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v0, v1, :cond_0

    .line 1768966
    :goto_0
    monitor-exit p0

    return-void

    .line 1768967
    :cond_0
    :try_start_1
    invoke-virtual {p1}, LX/BIG;->getMediaItem()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    .line 1768968
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->c()J

    move-result-wide v0

    .line 1768969
    invoke-virtual {p1}, LX/BIG;->getMediaItem()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    .line 1768970
    iget-object v3, v2, Lcom/facebook/ipc/media/MediaItem;->c:Lcom/facebook/ipc/media/data/LocalMediaData;

    move-object v2, v3

    .line 1768971
    invoke-virtual {v2}, Lcom/facebook/ipc/media/data/LocalMediaData;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v2

    .line 1768972
    iget-object v3, v2, Lcom/facebook/ipc/media/data/MediaData;->mUri:Landroid/net/Uri;

    move-object v2, v3

    .line 1768973
    invoke-static {v2}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v2

    new-instance v3, LX/1o9;

    const/16 v4, 0xe0

    const/16 v5, 0xe0

    invoke-direct {v3, v4, v5}, LX/1o9;-><init>(II)V

    .line 1768974
    iput-object v3, v2, LX/1bX;->c:LX/1o9;

    .line 1768975
    move-object v2, v2

    .line 1768976
    invoke-virtual {v2}, LX/1bX;->n()LX/1bf;

    move-result-object v2

    .line 1768977
    iget-object v3, p0, Lcom/facebook/photos/simplepicker/controller/RecognitionManager;->c:LX/1HI;

    sget-object v4, Lcom/facebook/photos/simplepicker/controller/RecognitionManager;->h:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v2, v4}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v2

    .line 1768978
    new-instance v3, LX/BHM;

    invoke-direct {v3, p0, p1, v0, v1}, LX/BHM;-><init>(Lcom/facebook/photos/simplepicker/controller/RecognitionManager;LX/BIG;J)V

    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/RecognitionManager;->d:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v2, v3, v0}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1768979
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
