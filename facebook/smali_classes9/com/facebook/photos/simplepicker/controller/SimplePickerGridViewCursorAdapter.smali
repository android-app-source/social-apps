.class public Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;
.super LX/3tK;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final j:Ljava/lang/String;

.field private static final k:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final A:LX/7Dh;

.field private final B:Z

.field private final C:Z

.field private final D:LX/BHE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/BHE",
            "<",
            "LX/BIP;",
            ">;"
        }
    .end annotation
.end field

.field public final E:Lcom/facebook/photos/simplepicker/controller/RecognitionManager;

.field public F:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/BHW;",
            ">;"
        }
    .end annotation
.end field

.field public G:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/BHW;",
            ">;"
        }
    .end annotation
.end field

.field public H:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public I:LX/BGf;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public J:LX/BIM;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public K:LX/4gI;

.field private final L:LX/0sX;

.field private final M:LX/BHv;

.field public final l:LX/8I2;

.field public final m:LX/1FJ;

.field private final n:LX/BH5;

.field private final o:Landroid/view/View$OnClickListener;

.field public final p:LX/BHJ;

.field private final q:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;",
            ">;"
        }
    .end annotation
.end field

.field public final r:Z

.field private final s:LX/BID;

.field private final t:LX/11o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/11o",
            "<",
            "LX/BHw;",
            ">;"
        }
    .end annotation
.end field

.field private final u:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final v:I

.field private final w:Ljava/util/concurrent/ExecutorService;

.field public final x:LX/0Sh;

.field private final y:LX/1Ad;

.field private final z:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1769315
    const-class v0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->j:Ljava/lang/String;

    .line 1769316
    const-class v0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;

    const-string v1, "simple_picker"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->k:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;LX/BH5;LX/BHJ;LX/0am;ZZZLandroid/content/Context;LX/8I2;LX/BID;LX/11i;LX/0Ot;LX/BHY;Ljava/util/concurrent/ExecutorService;LX/0Sh;LX/1Ad;LX/BHE;LX/0ad;LX/7Dh;Lcom/facebook/photos/simplepicker/controller/RecognitionManager;LX/0sX;LX/BHv;)V
    .locals 4
    .param p1    # Landroid/database/Cursor;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/BH5;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/BHJ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/0am;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .param p14    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Lcom/facebook/photos/simplepicker/SimplePickerFragment$BitmapRenderedCallback;",
            "LX/BHJ;",
            "LX/0am",
            "<",
            "Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;",
            ">;ZZZ",
            "Landroid/content/Context;",
            "Lcom/facebook/photos/local/LocalMediaCursor;",
            "LX/BID;",
            "LX/11i;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/BHY;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/1Ad;",
            "LX/BHE;",
            "LX/0ad;",
            "LX/7Dh;",
            "Lcom/facebook/photos/simplepicker/controller/RecognitionManager;",
            "LX/0sX;",
            "LX/BHv;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1769284
    const/4 v1, 0x0

    invoke-direct {p0, p8, p1, v1}, LX/3tK;-><init>(Landroid/content/Context;Landroid/database/Cursor;Z)V

    .line 1769285
    iput-object p2, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->n:LX/BH5;

    .line 1769286
    iput-object p3, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->p:LX/BHJ;

    .line 1769287
    iput-object p4, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->q:LX/0am;

    .line 1769288
    iput-boolean p5, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->r:Z

    .line 1769289
    iput-boolean p6, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->B:Z

    .line 1769290
    iput-boolean p7, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->C:Z

    .line 1769291
    iput-object p8, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->d:Landroid/content/Context;

    .line 1769292
    iput-object p9, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->l:LX/8I2;

    .line 1769293
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->l:LX/8I2;

    invoke-virtual {v1}, LX/8I2;->a()LX/1FJ;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->m:LX/1FJ;

    .line 1769294
    iput-object p10, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->s:LX/BID;

    .line 1769295
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->u:LX/0Ot;

    .line 1769296
    invoke-virtual/range {p13 .. p13}, LX/BHY;->a()I

    move-result v1

    mul-int/lit8 v1, v1, 0x5

    iput v1, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->v:I

    .line 1769297
    new-instance v1, LX/BHR;

    invoke-direct {v1, p0, p3}, LX/BHR;-><init>(Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;LX/BHJ;)V

    iput-object v1, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->o:Landroid/view/View$OnClickListener;

    .line 1769298
    sget-object v1, LX/BHx;->a:LX/BHw;

    invoke-interface {p11, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->t:LX/11o;

    .line 1769299
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->w:Ljava/util/concurrent/ExecutorService;

    .line 1769300
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->x:LX/0Sh;

    .line 1769301
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->t:LX/11o;

    if-eqz v1, :cond_0

    .line 1769302
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->t:LX/11o;

    const-string v2, "LoadMediaItems"

    const v3, -0x7b19d5e0

    invoke-static {v1, v2, v3}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1769303
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->t:LX/11o;

    const-string v2, "RenderThumbnails"

    const v3, -0x7d0cdc50

    invoke-static {v1, v2, v3}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1769304
    :cond_0
    invoke-static {}, LX/0UK;->a()Ljava/util/ArrayDeque;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->F:Ljava/util/Queue;

    .line 1769305
    invoke-static {}, LX/0UK;->a()Ljava/util/ArrayDeque;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->G:Ljava/util/Queue;

    .line 1769306
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v1, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->H:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1769307
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->y:LX/1Ad;

    .line 1769308
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->D:LX/BHE;

    .line 1769309
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->z:LX/0ad;

    .line 1769310
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->A:LX/7Dh;

    .line 1769311
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->E:Lcom/facebook/photos/simplepicker/controller/RecognitionManager;

    .line 1769312
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->L:LX/0sX;

    .line 1769313
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->M:LX/BHv;

    .line 1769314
    return-void
.end method

.method private a(Lcom/facebook/ipc/media/MediaItem;)Z
    .locals 1

    .prologue
    .line 1769317
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->p:LX/BHJ;

    .line 1769318
    iget-object p0, v0, LX/BHJ;->z:LX/BHZ;

    move-object v0, p0

    .line 1769319
    invoke-virtual {v0, p1}, LX/BHZ;->b(Lcom/facebook/ipc/media/MediaItem;)Z

    move-result v0

    return v0
.end method

.method public static declared-synchronized a$redex0(Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;JLandroid/database/Cursor;ILX/BIG;)V
    .locals 9

    .prologue
    .line 1769090
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->H:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1769091
    const v0, 0x7f0d00d2

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p5, v0, v1}, LX/BIG;->setTag(ILjava/lang/Object;)V

    .line 1769092
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->m:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p4}, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->getItemViewType(I)I

    move-result v0

    sget-object v1, LX/BHH;->LIVE_CAMERA:LX/BHH;

    invoke-virtual {v1}, LX/BHH;->ordinal()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-ne v0, v1, :cond_1

    .line 1769093
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1769094
    :cond_1
    :try_start_1
    iget-object v7, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->w:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2;

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter$2;-><init>(Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;JLandroid/database/Cursor;ILX/BIG;)V

    const v1, -0x4bfb2ba0

    invoke-static {v7, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1769095
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;LX/BIG;Lcom/facebook/ipc/media/MediaItem;I)V
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 1769190
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    .line 1769191
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v2, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->j:Ljava/lang/String;

    if-nez p2, :cond_2

    const-string v1, "mediaItem is null"

    :goto_0
    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1769192
    invoke-virtual {p1, v5}, LX/BIG;->setController(LX/1bp;)V

    .line 1769193
    :cond_1
    :goto_1
    return-void

    .line 1769194
    :cond_2
    const-string v1, "mediaItem path is null"

    goto :goto_0

    .line 1769195
    :cond_3
    invoke-virtual {p0, p3}, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->getItemViewType(I)I

    move-result v0

    invoke-static {v0}, LX/BHH;->fromOrdinal(I)LX/BHH;

    move-result-object v1

    .line 1769196
    if-nez v1, :cond_4

    .line 1769197
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->j:Ljava/lang/String;

    const-string v2, "unknown grid item type"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1769198
    invoke-virtual {p1, v5}, LX/BIG;->setController(LX/1bp;)V

    goto :goto_1

    .line 1769199
    :cond_4
    sget-object v0, LX/BHV;->a:[I

    invoke-virtual {v1}, LX/BHH;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 1769200
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->j:Ljava/lang/String;

    const-string v2, "unknown grid item type"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1769201
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->p:LX/BHJ;

    .line 1769202
    iget-object v2, v0, LX/BHJ;->z:LX/BHZ;

    move-object v0, v2

    .line 1769203
    iget-boolean v2, v0, LX/BHZ;->d:Z

    move v0, v2

    .line 1769204
    if-nez v0, :cond_e

    .line 1769205
    :cond_5
    :goto_2
    invoke-direct {p0, p2}, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->a(Lcom/facebook/ipc/media/MediaItem;)Z

    move-result v0

    if-eqz v0, :cond_c

    move-object v0, p1

    .line 1769206
    check-cast v0, LX/BIE;

    const/high16 v2, 0x3f400000    # 0.75f

    invoke-interface {v0, v2}, LX/BIE;->a(F)V

    .line 1769207
    :goto_3
    iput p3, p1, LX/BIG;->i:I

    .line 1769208
    iput-object p2, p1, LX/BIG;->c:Lcom/facebook/ipc/media/MediaItem;

    .line 1769209
    invoke-virtual {p2}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v0

    .line 1769210
    iget-object v2, p0, LX/3tK;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0bbb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 1769211
    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/1bX;->a(Z)LX/1bX;

    move-result-object v0

    new-instance v3, LX/1o9;

    invoke-direct {v3, v2, v2}, LX/1o9;-><init>(II)V

    .line 1769212
    iput-object v3, v0, LX/1bX;->c:LX/1o9;

    .line 1769213
    move-object v2, v0

    .line 1769214
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->M:LX/BHv;

    .line 1769215
    iget-boolean v6, v0, LX/BHv;->a:Z

    if-nez v6, :cond_6

    iget-boolean v6, v0, LX/BHv;->b:Z

    if-eqz v6, :cond_11

    :cond_6
    const/4 v6, 0x1

    .line 1769216
    :goto_4
    if-eqz v6, :cond_12

    new-instance v6, LX/BHu;

    iget-wide v8, v0, LX/BHv;->f:J

    invoke-direct {v6, v0, v8, v9}, LX/BHu;-><init>(LX/1BP;J)V

    :goto_5
    move-object v0, v6

    .line 1769217
    if-eqz v0, :cond_7

    .line 1769218
    iput-object v0, p1, LX/BIG;->m:LX/BHu;

    .line 1769219
    new-instance v3, LX/4CP;

    invoke-direct {v3, v0}, LX/4CP;-><init>(LX/2xn;)V

    .line 1769220
    iput-object v3, v2, LX/1bX;->l:LX/1BU;

    .line 1769221
    :cond_7
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->y:LX/1Ad;

    sget-object v3, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->k:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v3}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {p1}, LX/BIG;->getController()LX/1aZ;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v2}, LX/1bX;->n()LX/1bf;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1769222
    iget-object v2, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->n:LX/BH5;

    invoke-virtual {p1, v0, v2}, LX/BIG;->a(LX/1bp;LX/BH5;)V

    .line 1769223
    sget-object v0, LX/BHH;->VIDEO:LX/BHH;

    if-ne v1, v0, :cond_8

    instance-of v0, p1, LX/BIP;

    if-eqz v0, :cond_8

    .line 1769224
    invoke-virtual {p2}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v2

    .line 1769225
    iget-object v3, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->D:LX/BHE;

    move-object v0, p1

    check-cast v0, LX/BIP;

    new-instance v4, LX/BHS;

    invoke-direct {v4, p0, v2}, LX/BHS;-><init>(Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;Ljava/lang/String;)V

    .line 1769226
    iget-object v2, v3, LX/BHE;->a:LX/1Aa;

    invoke-virtual {v2, v0, v4}, LX/1Aa;->a(Landroid/view/View;LX/2oV;)V

    .line 1769227
    :cond_8
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->o:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, LX/BIG;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1769228
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->q:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1769229
    invoke-static {p2}, LX/74s;->a(Lcom/facebook/ipc/media/MediaItem;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1769230
    invoke-virtual {p1, v5}, LX/BIG;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1769231
    :cond_9
    :goto_6
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->p:LX/BHJ;

    .line 1769232
    invoke-interface {p1}, LX/BIF;->getMediaItem()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    .line 1769233
    iget-object v3, v0, LX/BHJ;->a:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 1769234
    iget-object v3, v0, LX/BHJ;->a:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 1769235
    iget-boolean v3, v0, LX/BHJ;->l:Z

    move v3, v3

    .line 1769236
    invoke-interface {p1, v2, v3}, LX/BIF;->a(IZ)V

    .line 1769237
    :cond_a
    :goto_7
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->L:LX/0sX;

    const/4 v2, 0x0

    .line 1769238
    invoke-virtual {v0}, LX/0sX;->a()Z

    move-result v3

    if-eqz v3, :cond_b

    iget-object v3, v0, LX/0sX;->b:LX/0Uh;

    const/16 v4, 0xa

    invoke-virtual {v3, v4, v2}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_b

    const/4 v2, 0x1

    :cond_b
    move v0, v2

    .line 1769239
    if-eqz v0, :cond_1

    sget-object v0, LX/BHH;->PHOTO:LX/BHH;

    if-ne v1, v0, :cond_1

    .line 1769240
    invoke-virtual {p1, v5}, LX/BIG;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1769241
    new-instance v0, LX/BHT;

    invoke-direct {v0, p0, p2, p1}, LX/BHT;-><init>(Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;Lcom/facebook/ipc/media/MediaItem;LX/BIG;)V

    invoke-virtual {p1, v0}, LX/BIG;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1769242
    new-instance v0, LX/BHU;

    invoke-direct {v0, p0}, LX/BHU;-><init>(Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;)V

    invoke-static {p1, v0}, LX/0vv;->a(Landroid/view/View;LX/0vn;)V

    goto/16 :goto_1

    :cond_c
    move-object v0, p1

    .line 1769243
    check-cast v0, LX/BIE;

    invoke-interface {v0}, LX/BIE;->a()V

    goto/16 :goto_3

    .line 1769244
    :cond_d
    iget-object v2, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->s:LX/BID;

    iget-object v3, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->p:LX/BHJ;

    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->q:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;

    .line 1769245
    new-instance v4, LX/BIC;

    const/16 v6, 0x11fa

    invoke-static {v2, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-direct {v4, v3, v0, p1, v6}, LX/BIC;-><init>(LX/BHJ;Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;LX/BIF;LX/0Ot;)V

    .line 1769246
    move-object v0, v4

    .line 1769247
    invoke-virtual {p1, v0}, LX/BIG;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_6

    .line 1769248
    :cond_e
    const/4 v0, 0x0

    .line 1769249
    iget-object v2, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->p:LX/BHJ;

    .line 1769250
    iget-object v3, v2, LX/BHJ;->i:LX/BHi;

    move-object v2, v3

    .line 1769251
    invoke-virtual {v2}, LX/BHi;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BIF;

    .line 1769252
    invoke-interface {v0}, LX/BIF;->getMediaItem()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v4

    if-eqz v4, :cond_10

    iget-object v4, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->p:LX/BHJ;

    .line 1769253
    iget-object v6, v4, LX/BHJ;->z:LX/BHZ;

    move-object v4, v6

    .line 1769254
    iget-object v6, v4, LX/BHZ;->h:Ljava/util/ArrayList;

    move-object v4, v6

    .line 1769255
    invoke-interface {v0}, LX/BIF;->getMediaItem()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1769256
    add-int/lit8 v2, v2, 0x1

    move v0, v2

    :goto_9
    move v2, v0

    .line 1769257
    goto :goto_8

    .line 1769258
    :cond_f
    if-nez v2, :cond_5

    .line 1769259
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->p:LX/BHJ;

    .line 1769260
    iget-object v2, v0, LX/BHJ;->z:LX/BHZ;

    move-object v0, v2

    .line 1769261
    iget-object v2, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->p:LX/BHJ;

    .line 1769262
    iget-object v3, v2, LX/BHJ;->i:LX/BHi;

    move-object v2, v3

    .line 1769263
    invoke-virtual {v0, v2}, LX/BHZ;->a(LX/BHi;)V

    goto/16 :goto_2

    :cond_10
    move v0, v2

    goto :goto_9

    .line 1769264
    :cond_11
    const/4 v6, 0x0

    goto/16 :goto_4

    .line 1769265
    :cond_12
    const/4 v6, 0x0

    goto/16 :goto_5

    .line 1769266
    :cond_13
    invoke-interface {p1}, LX/BIF;->isSelected()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1769267
    invoke-interface {p1}, LX/BIF;->f()V

    goto/16 :goto_7

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 1769268
    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    .line 1769269
    invoke-virtual {p0, v2}, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->getItemViewType(I)I

    move-result v0

    invoke-static {v0}, LX/BHH;->fromOrdinal(I)LX/BHH;

    move-result-object v0

    .line 1769270
    if-nez v0, :cond_0

    .line 1769271
    sget-object v0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->j:Ljava/lang/String;

    const-string v1, "Unknown item type"

    invoke-static {v0, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1769272
    new-instance v1, LX/BIH;

    invoke-direct {v1, p1}, LX/BIH;-><init>(Landroid/content/Context;)V

    .line 1769273
    :goto_0
    return-object v1

    .line 1769274
    :cond_0
    sget-object v1, LX/BHV;->a:[I

    invoke-virtual {v0}, LX/BHH;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1769275
    new-instance v1, LX/BIH;

    invoke-direct {v1, p1}, LX/BIH;-><init>(Landroid/content/Context;)V

    move-object v0, v1

    .line 1769276
    check-cast v0, LX/BIH;

    iget-boolean v3, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->B:Z

    .line 1769277
    iput-boolean v3, v0, LX/BIH;->e:Z

    .line 1769278
    :goto_1
    invoke-interface {p2, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    goto :goto_0

    .line 1769279
    :pswitch_0
    new-instance v1, LX/BIP;

    invoke-direct {v1, p1}, LX/BIP;-><init>(Landroid/content/Context;)V

    goto :goto_1

    .line 1769280
    :pswitch_1
    iget-boolean v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->C:Z

    if-eqz v0, :cond_1

    .line 1769281
    new-instance v1, LX/BII;

    invoke-direct {v1, p1}, LX/BII;-><init>(Landroid/content/Context;)V

    goto :goto_1

    .line 1769282
    :cond_1
    new-instance v1, LX/BIH;

    invoke-direct {v1, p1}, LX/BIH;-><init>(Landroid/content/Context;)V

    goto :goto_1

    .line 1769283
    :pswitch_2
    new-instance v1, LX/BIM;

    invoke-direct {v1, p1}, LX/BIM;-><init>(Landroid/content/Context;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Landroid/view/View;Landroid/database/Cursor;)V
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 1769142
    move-object v6, p1

    check-cast v6, LX/BIG;

    .line 1769143
    :try_start_0
    const-string v0, "camera_entry"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 1769144
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 1769145
    const/4 v0, 0x0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 1769146
    :goto_0
    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    .line 1769147
    invoke-virtual {p0, v5}, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->getItemViewType(I)I

    move-result v0

    invoke-static {v0}, LX/BHH;->fromOrdinal(I)LX/BHH;

    move-result-object v0

    .line 1769148
    check-cast p1, LX/BIG;

    iget-object v1, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->K:LX/4gI;

    .line 1769149
    sget-object v4, LX/BHG;->a:Ljava/util/Map;

    invoke-interface {v4, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    sget-object v4, LX/BHG;->a:Ljava/util/Map;

    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    const/4 v4, 0x1

    :goto_1
    move v1, v4

    .line 1769150
    iput-boolean v1, p1, LX/BIG;->k:Z

    .line 1769151
    iget-boolean v4, p1, LX/BIG;->k:Z

    if-eqz v4, :cond_a

    const/high16 v4, 0x3f800000    # 1.0f

    :goto_2
    invoke-virtual {p1, v4}, LX/BIG;->setAlpha(F)V

    .line 1769152
    invoke-static {p1}, LX/BIG;->getXRayDescription(LX/BIG;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/BIG;->setAccessibilityTalkback(Ljava/lang/String;)V

    .line 1769153
    sget-object v1, LX/BHH;->LIVE_CAMERA:LX/BHH;

    if-ne v0, v1, :cond_4

    .line 1769154
    check-cast v6, LX/BIM;

    iput-object v6, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->J:LX/BIM;

    .line 1769155
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->J:LX/BIM;

    iget-object v1, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->I:LX/BGf;

    .line 1769156
    iput-object v1, v0, LX/BIM;->j:LX/BGf;

    .line 1769157
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->J:LX/BIM;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/BIG;->setController(LX/1bp;)V

    .line 1769158
    iget-boolean v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->r:Z

    if-eqz v0, :cond_0

    .line 1769159
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->J:LX/BIM;

    sget-object v1, LX/5fM;->FRONT:LX/5fM;

    .line 1769160
    const p0, 0x7f0d0861

    invoke-static {v0, p0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Lcom/facebook/optic/CameraPreviewView;

    .line 1769161
    iput-object v1, p0, Lcom/facebook/optic/CameraPreviewView;->p:LX/5fM;

    .line 1769162
    :cond_0
    :goto_3
    return-void

    .line 1769163
    :cond_1
    const-wide/16 v2, -0x1

    goto :goto_0

    .line 1769164
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1769165
    instance-of v0, v1, Landroid/database/CursorIndexOutOfBoundsException;

    if-nez v0, :cond_2

    instance-of v0, v1, Ljava/lang/IllegalStateException;

    if-eqz v0, :cond_3

    .line 1769166
    :cond_2
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v2, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->j:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1769167
    const/4 v0, 0x0

    invoke-virtual {v6, v0}, LX/BIG;->setController(LX/1bp;)V

    goto :goto_3

    .line 1769168
    :cond_3
    throw v1

    .line 1769169
    :cond_4
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->l:LX/8I2;

    invoke-virtual {v0, v2, v3}, LX/8I2;->a(J)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    .line 1769170
    if-eqz v0, :cond_5

    .line 1769171
    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    invoke-static {p0, v6, v0, v1}, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->a$redex0(Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;LX/BIG;Lcom/facebook/ipc/media/MediaItem;I)V

    goto :goto_3

    .line 1769172
    :cond_5
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->F:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->H:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_6

    move-object v1, p0

    move-object v4, p2

    .line 1769173
    invoke-static/range {v1 .. v6}, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->a$redex0(Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;JLandroid/database/Cursor;ILX/BIG;)V

    goto :goto_3

    .line 1769174
    :cond_6
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->F:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    const/16 v1, 0x28

    if-lt v0, v1, :cond_7

    .line 1769175
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->F:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/BHW;

    move-object v4, p2

    .line 1769176
    iput-wide v2, v1, LX/BHW;->a:J

    .line 1769177
    iput-object v4, v1, LX/BHW;->b:Landroid/database/Cursor;

    .line 1769178
    iput v5, v1, LX/BHW;->c:I

    .line 1769179
    iput-object v6, v1, LX/BHW;->d:LX/BIG;

    .line 1769180
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->F:Ljava/util/Queue;

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1769181
    :cond_7
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->G:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1769182
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->F:Ljava/util/Queue;

    new-instance v1, LX/BHW;

    move-object v4, p2

    invoke-direct/range {v1 .. v7}, LX/BHW;-><init>(JLandroid/database/Cursor;ILX/BIG;B)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 1769183
    :cond_8
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->G:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/BHW;

    move-object v4, p2

    .line 1769184
    iput-wide v2, v1, LX/BHW;->a:J

    .line 1769185
    iput-object v4, v1, LX/BHW;->b:Landroid/database/Cursor;

    .line 1769186
    iput v5, v1, LX/BHW;->c:I

    .line 1769187
    iput-object v6, v1, LX/BHW;->d:LX/BIG;

    .line 1769188
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->F:Ljava/util/Queue;

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    :cond_9
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 1769189
    :cond_a
    const v4, 0x3e4ccccd    # 0.2f

    goto/16 :goto_2
.end method

.method public final getItemId(I)J
    .locals 9

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1769125
    :try_start_0
    invoke-super {p0, p1}, LX/3tK;->getItemId(I)J
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-wide v0

    .line 1769126
    :goto_0
    return-wide v0

    .line 1769127
    :catch_0
    move-exception v1

    .line 1769128
    const-string v4, "Exception = %s, position = %d, cursor count = %s"

    new-array v5, v0, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v7

    iget-object v0, p0, LX/3tK;->c:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3tK;->c:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    aput-object v0, v5, v8

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1769129
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v5, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->j:Ljava/lang/String;

    invoke-virtual {v0, v5, v4, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-wide v0, v2

    .line 1769130
    goto :goto_0

    .line 1769131
    :cond_0
    const-string v0, "null"

    goto :goto_1

    .line 1769132
    :catch_1
    move-exception v1

    .line 1769133
    const-string v4, "Cursor Exception = %s, position = %d, cursor length = %s"

    new-array v5, v0, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v7

    iget-object v0, p0, LX/3tK;->c:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/3tK;->c:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    :goto_2
    aput-object v0, v5, v8

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1769134
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v5, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->j:Ljava/lang/String;

    invoke-virtual {v0, v5, v4, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-wide v0, v2

    .line 1769135
    goto :goto_0

    .line 1769136
    :cond_1
    const-string v0, "null"

    goto :goto_2

    .line 1769137
    :catch_2
    move-exception v1

    .line 1769138
    const-string v4, "Cursor Exception = %s, position = %d, cursor length = %s"

    new-array v5, v0, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v7

    iget-object v0, p0, LX/3tK;->c:Landroid/database/Cursor;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/3tK;->c:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    :goto_3
    aput-object v0, v5, v8

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1769139
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v5, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->j:Ljava/lang/String;

    invoke-virtual {v0, v5, v4, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-wide v0, v2

    .line 1769140
    goto/16 :goto_0

    .line 1769141
    :cond_2
    const-string v0, "null"

    goto :goto_3
.end method

.method public final getItemViewType(I)I
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v1, -0x1

    .line 1769097
    iget-object v0, p0, LX/3tK;->c:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3tK;->c:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 1769098
    :goto_0
    return v0

    .line 1769099
    :cond_1
    :try_start_0
    iget-object v0, p0, LX/3tK;->c:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1769100
    const-string v2, "Trying to access invalid index of position = %d, cursor length = %s"

    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v4, 0x1

    iget-object v0, p0, LX/3tK;->c:Landroid/database/Cursor;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/3tK;->c:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1769101
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v3, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->j:Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 1769102
    goto :goto_0

    .line 1769103
    :cond_2
    const-string v0, "null"

    goto :goto_1

    .line 1769104
    :cond_3
    iget-object v0, p0, LX/3tK;->c:Landroid/database/Cursor;

    const-string v2, "camera_entry"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v1, :cond_4

    iget-object v0, p0, LX/3tK;->c:Landroid/database/Cursor;

    iget-object v2, p0, LX/3tK;->c:Landroid/database/Cursor;

    const-string v3, "camera_entry"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v4, v0, :cond_4

    .line 1769105
    sget-object v0, LX/BHH;->LIVE_CAMERA:LX/BHH;

    invoke-virtual {v0}, LX/BHH;->ordinal()I

    move-result v0

    goto :goto_0

    .line 1769106
    :cond_4
    iget-object v0, p0, LX/3tK;->c:Landroid/database/Cursor;

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_5

    const-wide/16 v2, 0x0

    .line 1769107
    :goto_2
    const-wide/16 v4, 0x3

    cmp-long v0, v2, v4

    if-nez v0, :cond_6

    .line 1769108
    sget-object v0, LX/BHH;->VIDEO:LX/BHH;

    invoke-virtual {v0}, LX/BHH;->ordinal()I

    move-result v0

    goto :goto_0

    .line 1769109
    :cond_5
    iget-object v0, p0, LX/3tK;->c:Landroid/database/Cursor;

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    goto :goto_2

    .line 1769110
    :cond_6
    iget-object v0, p0, LX/3tK;->c:Landroid/database/Cursor;

    const/4 v2, 0x2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x0

    .line 1769111
    :goto_3
    if-eqz v0, :cond_8

    sget-object v2, Lcom/facebook/ipc/media/data/MimeType;->d:Lcom/facebook/ipc/media/data/MimeType;

    invoke-virtual {v2}, Lcom/facebook/ipc/media/data/MimeType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1769112
    sget-object v0, LX/BHH;->GIF:LX/BHH;

    invoke-virtual {v0}, LX/BHH;->ordinal()I

    move-result v0

    goto/16 :goto_0

    .line 1769113
    :cond_7
    iget-object v0, p0, LX/3tK;->c:Landroid/database/Cursor;

    const/4 v2, 0x2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 1769114
    :cond_8
    sget-object v0, LX/BHH;->PHOTO:LX/BHH;

    invoke-virtual {v0}, LX/BHH;->ordinal()I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    goto/16 :goto_0

    .line 1769115
    :catch_0
    move-exception v0

    move-object v2, v0

    .line 1769116
    iget-object v0, p0, LX/3tK;->c:Landroid/database/Cursor;

    instance-of v0, v0, Landroid/database/MergeCursor;

    if-eqz v0, :cond_9

    .line 1769117
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v3, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->j:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 1769118
    goto/16 :goto_0

    .line 1769119
    :cond_9
    throw v2

    .line 1769120
    :catch_1
    move-exception v0

    move-object v2, v0

    .line 1769121
    instance-of v0, v2, Landroid/database/CursorIndexOutOfBoundsException;

    if-nez v0, :cond_a

    instance-of v0, v2, Ljava/lang/IllegalStateException;

    if-eqz v0, :cond_b

    .line 1769122
    :cond_a
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v3, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->j:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 1769123
    goto/16 :goto_0

    .line 1769124
    :cond_b
    throw v2
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 1769096
    invoke-static {}, LX/BHH;->values()[LX/BHH;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
