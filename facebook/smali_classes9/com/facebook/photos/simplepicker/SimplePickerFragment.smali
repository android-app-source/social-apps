.class public Lcom/facebook/photos/simplepicker/SimplePickerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field public static final s:Ljava/lang/String;

.field private static final t:[Ljava/lang/String;

.field private static final u:[Ljava/lang/String;


# instance fields
.field private A:LX/74q;

.field private B:LX/9iZ;

.field public C:LX/1b0;

.field private D:LX/23P;

.field public E:LX/BHh;

.field public F:LX/BHq;

.field private G:LX/0wL;

.field public H:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public I:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9iY;",
            ">;"
        }
    .end annotation
.end field

.field private J:LX/BHK;

.field public K:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BHL;",
            ">;"
        }
    .end annotation
.end field

.field private L:LX/BGc;

.field public M:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation
.end field

.field public N:LX/75S;

.field private O:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private P:LX/0SG;

.field private Q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Axt;",
            ">;"
        }
    .end annotation
.end field

.field private R:LX/0i5;

.field public S:LX/BGa;

.field private T:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/5SF;",
            ">;"
        }
    .end annotation
.end field

.field private U:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8OU;",
            ">;"
        }
    .end annotation
.end field

.field public V:Z

.field public W:Ljava/lang/String;

.field private X:Z

.field public Y:LX/0TD;

.field public Z:Ljava/lang/String;

.field public a:LX/BHj;

.field public aA:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field

.field public aB:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation
.end field

.field public aC:LX/74n;

.field private aa:Z

.field private ab:Z

.field public ac:Z

.field public ad:LX/0kL;

.field public ae:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

.field public af:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

.field public ag:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

.field public ah:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

.field public ai:Landroid/support/v7/widget/Toolbar;

.field public aj:Lcom/facebook/resources/ui/FbTextView;

.field public ak:Lcom/facebook/resources/ui/FbTextView;

.field public al:LX/107;

.field public am:LX/107;

.field public an:LX/107;

.field public ao:LX/107;

.field public ap:Z

.field public aq:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public ar:LX/3l1;

.field public as:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gd;",
            ">;"
        }
    .end annotation
.end field

.field private at:LX/BI7;

.field private au:LX/BI6;

.field public av:LX/0ad;

.field private aw:LX/1Kf;

.field private ax:LX/75Q;

.field private ay:LX/75F;

.field private az:Z

.field public b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

.field public c:LX/BHJ;

.field public d:LX/BHQ;

.field public e:LX/BHl;

.field public f:I

.field public g:Z

.field public h:Z

.field public i:Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;

.field public j:Z

.field public k:LX/11i;

.field public l:LX/0id;

.field public m:LX/11o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/11o",
            "<",
            "LX/BHw;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;",
            ">;"
        }
    .end annotation
.end field

.field public o:Z

.field public p:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public r:Landroid/database/Cursor;

.field private v:LX/BHn;

.field private w:LX/BHm;

.field public x:LX/BHA;

.field public y:Landroid/view/View$OnClickListener;

.field public z:LX/BGe;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1768445
    const-class v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->s:Ljava/lang/String;

    .line 1768446
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "android.permission.READ_EXTERNAL_STORAGE"

    aput-object v1, v0, v2

    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v1, v0, v3

    sput-object v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->t:[Ljava/lang/String;

    .line 1768447
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "android.permission.CAMERA"

    aput-object v1, v0, v2

    const-string v1, "android.permission.READ_EXTERNAL_STORAGE"

    aput-object v1, v0, v3

    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->u:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1768448
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1768449
    iput-boolean v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->V:Z

    .line 1768450
    iput-boolean v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->g:Z

    .line 1768451
    iput-boolean v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->h:Z

    .line 1768452
    iput-boolean v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->X:Z

    .line 1768453
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->n:LX/0Px;

    .line 1768454
    return-void
.end method

.method public static A(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)Ljava/util/concurrent/Callable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1768455
    new-instance v0, LX/BGs;

    invoke-direct {v0, p0}, LX/BGs;-><init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V

    return-object v0
.end method

.method public static B(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V
    .locals 2

    .prologue
    .line 1768456
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 1768457
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 1768458
    return-void
.end method

.method public static C(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 1

    .prologue
    .line 1768459
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1768460
    iget-object p0, v0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-object v0, p0

    .line 1768461
    invoke-static {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/ipc/media/MediaItem;Landroid/net/Uri;)Lcom/facebook/ipc/media/MediaItem;
    .locals 8

    .prologue
    .line 1768429
    new-instance v0, LX/4gN;

    invoke-direct {v0}, LX/4gN;-><init>()V

    .line 1768430
    iget-object v1, p0, Lcom/facebook/ipc/media/MediaItem;->c:Lcom/facebook/ipc/media/data/LocalMediaData;

    move-object v1, v1

    .line 1768431
    iget-wide v6, v1, Lcom/facebook/ipc/media/data/LocalMediaData;->mMediaStoreId:J

    move-wide v2, v6

    .line 1768432
    iput-wide v2, v0, LX/4gN;->d:J

    .line 1768433
    move-object v0, v0

    .line 1768434
    new-instance v1, LX/4gP;

    invoke-direct {v1}, LX/4gP;-><init>()V

    new-instance v2, Lcom/facebook/ipc/media/MediaIdKey;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 1768435
    iget-object v4, p0, Lcom/facebook/ipc/media/MediaItem;->c:Lcom/facebook/ipc/media/data/LocalMediaData;

    move-object v4, v4

    .line 1768436
    iget-wide v6, v4, Lcom/facebook/ipc/media/data/LocalMediaData;->mMediaStoreId:J

    move-wide v4, v6

    .line 1768437
    invoke-direct {v2, v3, v4, v5}, Lcom/facebook/ipc/media/MediaIdKey;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaIdKey;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4gP;->a(Ljava/lang/String;)LX/4gP;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/4gP;->a(Landroid/net/Uri;)LX/4gP;

    move-result-object v1

    sget-object v2, Lcom/facebook/ipc/media/data/MimeType;->b:Lcom/facebook/ipc/media/data/MimeType;

    invoke-virtual {v1, v2}, LX/4gP;->a(Lcom/facebook/ipc/media/data/MimeType;)LX/4gP;

    move-result-object v1

    sget-object v2, LX/4gQ;->Video:LX/4gQ;

    invoke-virtual {v1, v2}, LX/4gP;->a(LX/4gQ;)LX/4gP;

    move-result-object v1

    invoke-virtual {v1}, LX/4gP;->a()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4gN;->a(Lcom/facebook/ipc/media/data/MediaData;)LX/4gN;

    move-result-object v0

    invoke-virtual {v0}, LX/4gN;->a()Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v0

    .line 1768438
    new-instance v1, LX/74m;

    invoke-direct {v1}, LX/74m;-><init>()V

    .line 1768439
    iput-object v0, v1, LX/74m;->e:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 1768440
    move-object v0, v1

    .line 1768441
    invoke-virtual {v0}, LX/74m;->a()Lcom/facebook/photos/base/media/VideoItem;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/os/Bundle;Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;Ljava/lang/String;)Lcom/facebook/photos/simplepicker/SimplePickerFragment;
    .locals 2
    .param p0    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1768462
    new-instance v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    invoke-direct {v0}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;-><init>()V

    .line 1768463
    if-nez p0, :cond_0

    .line 1768464
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 1768465
    :cond_0
    const-string v1, "extra_simple_picker_launcher_waterfall_id"

    invoke-virtual {p0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1768466
    const-string v1, "extra_simple_picker_launcher_settings"

    invoke-virtual {p0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1768467
    invoke-virtual {v0, p0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1768468
    return-object v0
.end method

.method public static a(Lcom/facebook/photos/simplepicker/SimplePickerFragment;I)V
    .locals 2

    .prologue
    .line 1768469
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 1768470
    const v1, 0x7f0d04e7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1768471
    if-eqz v0, :cond_0

    .line 1768472
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->G:LX/0wL;

    invoke-virtual {v1, v0, p1}, LX/0wL;->a(Landroid/view/View;I)V

    .line 1768473
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 1768474
    const v1, 0x7f0d00bc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1768475
    if-eqz v0, :cond_1

    .line 1768476
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->G:LX/0wL;

    invoke-virtual {v1, v0, p1}, LX/0wL;->a(Landroid/view/View;I)V

    .line 1768477
    :cond_1
    return-void
.end method

.method private static a(Lcom/facebook/photos/simplepicker/SimplePickerFragment;LX/74q;LX/BGe;LX/11i;LX/0id;LX/0Ot;LX/0Ot;LX/9iZ;LX/0Or;LX/BHn;LX/BHK;LX/0Ot;LX/0Ot;LX/0Ot;LX/BGc;LX/75S;LX/BHQ;LX/0TD;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/0Ot;LX/BHm;LX/0kL;LX/1Kf;LX/75Q;LX/75F;LX/0i4;LX/BI7;LX/0ad;LX/23P;LX/1b0;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;LX/74n;LX/3l1;LX/0Ot;LX/0wL;LX/BGa;LX/0Ot;LX/0Ot;)V
    .locals 3
    .param p0    # Lcom/facebook/photos/simplepicker/SimplePickerFragment;
        .annotation runtime Lcom/facebook/photos/simplepicker/SimplePickerItemsOperator;
        .end annotation
    .end param
    .param p6    # LX/0Ot;
        .annotation runtime Lcom/facebook/photos/simplepicker/SimplePickerAutoTaggingActivator;
        .end annotation
    .end param
    .param p7    # LX/9iZ;
        .annotation runtime Lcom/facebook/photos/gating/SimplePickerShouldLogDetailedInfo;
        .end annotation
    .end param
    .param p16    # LX/BHQ;
        .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
        .end annotation
    .end param
    .param p30    # LX/1b0;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p31    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/74q;",
            "LX/BGe;",
            "LX/11i;",
            "LX/0id;",
            "LX/0Ot",
            "<",
            "LX/9iY;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "Lcom/facebook/photos/base/tagging/PrefilledTaggingActivator;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/BHn;",
            "LX/BHK;",
            "LX/0Ot",
            "<",
            "LX/BHL;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/BGc;",
            "LX/75S;",
            "LX/BHQ;",
            "LX/0TD;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0SG;",
            "LX/0Ot",
            "<",
            "LX/Axt;",
            ">;",
            "LX/BHm;",
            "LX/0kL;",
            "LX/1Kf;",
            "LX/75Q;",
            "LX/75F;",
            "LX/0i4;",
            "LX/BI7;",
            "LX/0ad;",
            "LX/23P;",
            "LX/1b0;",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/74n;",
            "LX/3l1;",
            "LX/0Ot",
            "<",
            "LX/0gd;",
            ">;",
            "LX/0wL;",
            "LX/BGa;",
            "LX/0Ot",
            "<",
            "LX/5SF;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8OU;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1768478
    iput-object p1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->A:LX/74q;

    .line 1768479
    iput-object p2, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->z:LX/BGe;

    .line 1768480
    iput-object p3, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->k:LX/11i;

    .line 1768481
    iput-object p4, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->l:LX/0id;

    .line 1768482
    sget-object v1, LX/BHx;->a:LX/BHw;

    invoke-interface {p3, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->m:LX/11o;

    .line 1768483
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->m:LX/11o;

    if-nez v1, :cond_0

    .line 1768484
    new-instance v1, LX/11n;

    invoke-direct {v1}, LX/11n;-><init>()V

    iput-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->m:LX/11o;

    .line 1768485
    :cond_0
    iput-object p5, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->I:LX/0Ot;

    .line 1768486
    iput-object p6, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->H:LX/0Ot;

    .line 1768487
    iput-object p7, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->B:LX/9iZ;

    .line 1768488
    invoke-interface {p8}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    sget-object v2, LX/03R;->YES:LX/03R;

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ap:Z

    .line 1768489
    iput-object p10, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->J:LX/BHK;

    .line 1768490
    iput-object p11, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->K:LX/0Ot;

    .line 1768491
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->L:LX/BGc;

    .line 1768492
    iput-object p12, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->M:LX/0Ot;

    .line 1768493
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->aq:LX/0Ot;

    .line 1768494
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->N:LX/75S;

    .line 1768495
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->d:LX/BHQ;

    .line 1768496
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->Y:LX/0TD;

    .line 1768497
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->O:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1768498
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->P:LX/0SG;

    .line 1768499
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->Q:LX/0Ot;

    .line 1768500
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    move-object/from16 v0, p26

    invoke-virtual {v0, v1}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->R:LX/0i5;

    .line 1768501
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ad:LX/0kL;

    .line 1768502
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->aw:LX/1Kf;

    .line 1768503
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ax:LX/75Q;

    .line 1768504
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ay:LX/75F;

    .line 1768505
    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->at:LX/BI7;

    .line 1768506
    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->av:LX/0ad;

    .line 1768507
    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->D:LX/23P;

    .line 1768508
    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->C:LX/1b0;

    .line 1768509
    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->aA:Ljava/util/concurrent/ExecutorService;

    .line 1768510
    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->aB:Ljava/util/concurrent/ExecutorService;

    .line 1768511
    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->aC:LX/74n;

    .line 1768512
    move-object/from16 v0, p34

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ar:LX/3l1;

    .line 1768513
    move-object/from16 v0, p35

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->as:LX/0Ot;

    .line 1768514
    iput-object p9, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->v:LX/BHn;

    .line 1768515
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->w:LX/BHm;

    .line 1768516
    move-object/from16 v0, p36

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->G:LX/0wL;

    .line 1768517
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0bbb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->f:I

    .line 1768518
    move-object/from16 v0, p37

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->S:LX/BGa;

    .line 1768519
    move-object/from16 v0, p38

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->T:LX/0Ot;

    .line 1768520
    move-object/from16 v0, p39

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->U:LX/0Ot;

    .line 1768521
    return-void

    .line 1768522
    :cond_1
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method public static a(Lcom/facebook/photos/simplepicker/SimplePickerFragment;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)V
    .locals 3

    .prologue
    .line 1768523
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->aw:LX/1Kf;

    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->W:Ljava/lang/String;

    const/16 v2, 0x4d8

    invoke-interface {v0, v1, p1, v2, p0}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/support/v4/app/Fragment;)V

    .line 1768524
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 43

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v41

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    invoke-static/range {v41 .. v41}, LX/9ib;->a(LX/0QB;)LX/9ib;

    move-result-object v3

    check-cast v3, LX/74q;

    invoke-static/range {v41 .. v41}, LX/BGe;->a(LX/0QB;)LX/BGe;

    move-result-object v4

    check-cast v4, LX/BGe;

    invoke-static/range {v41 .. v41}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v5

    check-cast v5, LX/11i;

    invoke-static/range {v41 .. v41}, LX/0id;->a(LX/0QB;)LX/0id;

    move-result-object v6

    check-cast v6, LX/0id;

    const/16 v7, 0x2eb5

    move-object/from16 v0, v41

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x455

    move-object/from16 v0, v41

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static/range {v41 .. v41}, LX/9iZ;->a(LX/0QB;)LX/9iZ;

    move-result-object v9

    check-cast v9, LX/9iZ;

    const/16 v10, 0x341

    move-object/from16 v0, v41

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const-class v11, LX/BHn;

    move-object/from16 v0, v41

    invoke-interface {v0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/BHn;

    const-class v12, LX/BHK;

    move-object/from16 v0, v41

    invoke-interface {v0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/BHK;

    const/16 v13, 0x2eb9

    move-object/from16 v0, v41

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x12b1

    move-object/from16 v0, v41

    invoke-static {v0, v14}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0x259

    move-object/from16 v0, v41

    invoke-static {v0, v15}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const-class v16, LX/BGc;

    move-object/from16 v0, v41

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v16

    check-cast v16, LX/BGc;

    invoke-static/range {v41 .. v41}, LX/75S;->a(LX/0QB;)LX/75S;

    move-result-object v17

    check-cast v17, LX/75S;

    invoke-static/range {v41 .. v41}, LX/BHQ;->a(LX/0QB;)LX/BHQ;

    move-result-object v18

    check-cast v18, LX/BHQ;

    invoke-static/range {v41 .. v41}, LX/0tb;->a(LX/0QB;)LX/0TD;

    move-result-object v19

    check-cast v19, LX/0TD;

    invoke-static/range {v41 .. v41}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v20

    check-cast v20, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {v41 .. v41}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v21

    check-cast v21, LX/0SG;

    const/16 v22, 0x22c0

    move-object/from16 v0, v41

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v22

    const-class v23, LX/BHm;

    move-object/from16 v0, v41

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v23

    check-cast v23, LX/BHm;

    invoke-static/range {v41 .. v41}, LX/0kL;->a(LX/0QB;)LX/0kL;

    move-result-object v24

    check-cast v24, LX/0kL;

    invoke-static/range {v41 .. v41}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v25

    check-cast v25, LX/1Kf;

    invoke-static/range {v41 .. v41}, LX/75Q;->a(LX/0QB;)LX/75Q;

    move-result-object v26

    check-cast v26, LX/75Q;

    invoke-static/range {v41 .. v41}, LX/75F;->a(LX/0QB;)LX/75F;

    move-result-object v27

    check-cast v27, LX/75F;

    const-class v28, LX/0i4;

    move-object/from16 v0, v41

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v28

    check-cast v28, LX/0i4;

    const-class v29, LX/BI7;

    move-object/from16 v0, v41

    move-object/from16 v1, v29

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v29

    check-cast v29, LX/BI7;

    invoke-static/range {v41 .. v41}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v30

    check-cast v30, LX/0ad;

    invoke-static/range {v41 .. v41}, LX/23P;->a(LX/0QB;)LX/23P;

    move-result-object v31

    check-cast v31, LX/23P;

    invoke-static/range {v41 .. v41}, LX/1az;->a(LX/0QB;)LX/1az;

    move-result-object v32

    check-cast v32, LX/1b0;

    invoke-static/range {v41 .. v41}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v33

    check-cast v33, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {v41 .. v41}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v34

    check-cast v34, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {v41 .. v41}, LX/74n;->a(LX/0QB;)LX/74n;

    move-result-object v35

    check-cast v35, LX/74n;

    invoke-static/range {v41 .. v41}, LX/3l1;->a(LX/0QB;)LX/3l1;

    move-result-object v36

    check-cast v36, LX/3l1;

    const/16 v37, 0x3bf

    move-object/from16 v0, v41

    move/from16 v1, v37

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v37

    invoke-static/range {v41 .. v41}, LX/0wL;->a(LX/0QB;)LX/0wL;

    move-result-object v38

    check-cast v38, LX/0wL;

    invoke-static/range {v41 .. v41}, LX/BGa;->a(LX/0QB;)LX/BGa;

    move-result-object v39

    check-cast v39, LX/BGa;

    const/16 v40, 0x36f4

    move-object/from16 v0, v41

    move/from16 v1, v40

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v40

    const/16 v42, 0x2f09

    invoke-static/range {v41 .. v42}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v41

    invoke-static/range {v2 .. v41}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->a(Lcom/facebook/photos/simplepicker/SimplePickerFragment;LX/74q;LX/BGe;LX/11i;LX/0id;LX/0Ot;LX/0Ot;LX/9iZ;LX/0Or;LX/BHn;LX/BHK;LX/0Ot;LX/0Ot;LX/0Ot;LX/BGc;LX/75S;LX/BHQ;LX/0TD;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/0Ot;LX/BHm;LX/0kL;LX/1Kf;LX/75Q;LX/75F;LX/0i4;LX/BI7;LX/0ad;LX/23P;LX/1b0;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;LX/74n;LX/3l1;LX/0Ot;LX/0wL;LX/BGa;LX/0Ot;LX/0Ot;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/photos/simplepicker/SimplePickerFragment;LX/0Px;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 1768525
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1768526
    const-string v0, "extra_media_items"

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1768527
    const-string v0, "are_media_items_modified"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1768528
    iget-boolean v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->o:Z

    if-eqz v0, :cond_0

    .line 1768529
    const-string v0, "extra_slideshow_data"

    iget-object v2, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->p:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1768530
    :cond_0
    invoke-static {p1}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->g(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1768531
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 1768532
    new-instance v2, Ljava/io/File;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1768533
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1768534
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1768535
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 1768536
    :goto_0
    return-void

    .line 1768537
    :cond_2
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->aq:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->s:Ljava/lang/String;

    const-string v2, "Hosting Activity is null"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/photos/simplepicker/SimplePickerFragment;LX/9iX;Z)V
    .locals 4

    .prologue
    .line 1768538
    sget-object v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->u:[Ljava/lang/String;

    .line 1768539
    sget-object v1, LX/9iX;->VIDEO:LX/9iX;

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->av:LX/0ad;

    sget-short v2, LX/BHD;->b:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1768540
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1768541
    const-string v0, "android.permission.RECORD_AUDIO"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1768542
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 1768543
    :cond_0
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->R:LX/0i5;

    new-instance v2, LX/BGk;

    invoke-direct {v2, p0, p1, p2}, LX/BGk;-><init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;LX/9iX;Z)V

    invoke-virtual {v1, v0, v2}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    .line 1768544
    return-void
.end method

.method public static a$redex0(Lcom/facebook/photos/simplepicker/SimplePickerFragment;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 3
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1768545
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1768546
    const-string v1, "extra_staging_ground_photo_uri"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1768547
    const-string v1, "extra_staging_ground_selected_frame_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1768548
    const-string v1, "extra_is_from_simple_picker"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1768549
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1768550
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1768551
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 1768552
    :goto_0
    return-void

    .line 1768553
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->aq:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->s:Ljava/lang/String;

    const-string v2, "Cannot get hosting activity"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/photos/simplepicker/SimplePickerFragment;Lcom/facebook/ipc/media/MediaItem;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1768554
    new-instance v0, LX/5Rw;

    invoke-direct {v0}, LX/5Rw;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/media/MediaIdKey;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/5Rw;->a(Landroid/net/Uri;Ljava/lang/String;)LX/5Rw;

    move-result-object v0

    sget-object v3, LX/5Rr;->CROP:LX/5Rr;

    invoke-virtual {v0, v3}, LX/5Rw;->a(LX/5Rr;)LX/5Rw;

    move-result-object v0

    sget-object v3, LX/5Rq;->ZOOM_CROP:LX/5Rq;

    invoke-virtual {v0, v3}, LX/5Rw;->a(LX/5Rq;)LX/5Rw;

    move-result-object v0

    .line 1768555
    iput-boolean v1, v0, LX/5Rw;->f:Z

    .line 1768556
    move-object v0, v0

    .line 1768557
    iput-boolean v2, v0, LX/5Rw;->i:Z

    .line 1768558
    move-object v0, v0

    .line 1768559
    iget-object v3, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->D:LX/23P;

    const v4, 0x7f0813d1    # 1.808779E38f

    invoke-virtual {p0, v4}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1768560
    iput-object v3, v0, LX/5Rw;->j:Ljava/lang/String;

    .line 1768561
    move-object v0, v0

    .line 1768562
    invoke-virtual {v0}, LX/5Rw;->a()Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    move-result-object v3

    .line 1768563
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1768564
    const-string v4, "extra_photo_tab_mode_params"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    .line 1768565
    if-eqz v0, :cond_0

    .line 1768566
    iget-object v4, v0, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->c:Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;

    move-object v4, v4

    .line 1768567
    if-eqz v4, :cond_0

    .line 1768568
    new-instance v1, LX/5SJ;

    .line 1768569
    iget-object v2, v0, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->c:Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;

    move-object v0, v2

    .line 1768570
    invoke-direct {v1, v0}, LX/5SJ;-><init>(Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;)V

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaIdKey;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/5SJ;->a(Landroid/net/Uri;Ljava/lang/String;)LX/5SJ;

    move-result-object v0

    move-object v1, v0

    .line 1768571
    :goto_0
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->T:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5SF;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1}, LX/5SJ;->b()Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;

    move-result-object v1

    invoke-interface {v0, v2, v1, v3}, LX/5SF;->a(Landroid/content/Context;Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;)Landroid/content/Intent;

    move-result-object v1

    .line 1768572
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->H:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x4da

    invoke-interface {v0, v1, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1768573
    return-void

    .line 1768574
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->av:LX/0ad;

    sget-short v4, LX/0wf;->c:S

    invoke-interface {v0, v4, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 1768575
    iget-object v4, v0, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    move-object v0, v4

    .line 1768576
    sget-object v4, LX/4gQ;->Photo:LX/4gQ;

    if-ne v0, v4, :cond_3

    move v0, v1

    .line 1768577
    :goto_1
    new-instance v1, LX/5SJ;

    invoke-direct {v1}, LX/5SJ;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/ipc/media/MediaIdKey;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, LX/5SJ;->a(Landroid/net/Uri;Ljava/lang/String;)LX/5SJ;

    move-result-object v1

    sget-object v4, LX/5Rz;->PROFILE:LX/5Rz;

    invoke-virtual {v4}, LX/5Rz;->name()Ljava/lang/String;

    move-result-object v4

    .line 1768578
    iput-object v4, v1, LX/5SJ;->c:Ljava/lang/String;

    .line 1768579
    move-object v1, v1

    .line 1768580
    iget-object v4, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1768581
    iget-wide v6, v4, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->c:J

    move-wide v4, v6

    .line 1768582
    iput-wide v4, v1, LX/5SJ;->e:J

    .line 1768583
    move-object v1, v1

    .line 1768584
    iget-object v4, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1768585
    iget-boolean v5, v4, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->g:Z

    move v4, v5

    .line 1768586
    iput-boolean v4, v1, LX/5SJ;->o:Z

    .line 1768587
    move-object v1, v1

    .line 1768588
    if-eqz v0, :cond_1

    const/4 v2, 0x2

    .line 1768589
    :cond_1
    iput v2, v1, LX/5SJ;->q:I

    .line 1768590
    move-object v0, v1

    .line 1768591
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1768592
    iget-boolean v2, v1, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->h:Z

    move v1, v2

    .line 1768593
    if-nez v1, :cond_2

    .line 1768594
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/5SJ;->s:Z

    .line 1768595
    :cond_2
    move-object v1, v0

    goto :goto_0

    :cond_3
    move v0, v2

    .line 1768596
    goto :goto_1
.end method

.method public static g(LX/0Px;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1768597
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v2

    if-ne v2, v0, :cond_0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lcom/facebook/photos/base/media/VideoItem;

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static h(Lcom/facebook/photos/simplepicker/SimplePickerFragment;LX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1768442
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-static {}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->newBuilder()Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;->setMediaItems(LX/0Px;)Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;

    move-result-object v1

    sget-object v2, LX/B66;->SIMPLE_PICKER:LX/B66;

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;->setSource(LX/B66;)Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->W:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;->setSessionId(Ljava/lang/String;)Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;->a()Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;

    move-result-object v1

    invoke-static {v0, v1}, LX/B67;->a(Landroid/content/Context;Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;)Landroid/content/Intent;

    move-result-object v1

    .line 1768443
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->H:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x4dc

    invoke-interface {v0, v1, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1768444
    return-void
.end method

.method private q()LX/4gI;
    .locals 1

    .prologue
    .line 1767916
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->a:LX/BHj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->a:LX/BHj;

    .line 1767917
    iget-object p0, v0, LX/BHj;->f:LX/4gI;

    move-object v0, p0

    .line 1767918
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->r(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)LX/4gI;

    move-result-object v0

    goto :goto_0
.end method

.method public static r(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)LX/4gI;
    .locals 2

    .prologue
    .line 1767919
    iget-boolean v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->o:Z

    if-eqz v0, :cond_1

    sget-object v0, LX/4gI;->PHOTO_ONLY_EXCLUDING_GIFS:LX/4gI;

    .line 1767920
    :goto_0
    sget-object v1, LX/4gI;->PHOTO_ONLY:LX/4gI;

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1767921
    iget-object p0, v1, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    move-object v1, p0

    .line 1767922
    iget-boolean p0, v1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->r:Z

    move v1, p0

    .line 1767923
    if-nez v1, :cond_0

    .line 1767924
    sget-object v0, LX/4gI;->PHOTO_ONLY_EXCLUDING_GIFS:LX/4gI;

    .line 1767925
    :cond_0
    return-object v0

    .line 1767926
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1767927
    iget-object v1, v0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    move-object v0, v1

    .line 1767928
    iget-object v1, v0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->b:LX/4gI;

    move-object v0, v1

    .line 1767929
    goto :goto_0
.end method

.method private t()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1767930
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->c:LX/BHJ;

    invoke-virtual {v0}, LX/BHJ;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static w(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 1767931
    invoke-direct {p0}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->t()LX/0Px;

    move-result-object v6

    .line 1767932
    invoke-virtual {v6}, LX/0Px;->isEmpty()Z

    move-result v7

    .line 1767933
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1767934
    iget-object v2, v0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    move-object v8, v2

    .line 1767935
    invoke-direct {p0}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->q()LX/4gI;

    move-result-object v2

    .line 1767936
    iget-boolean v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->o:Z

    if-nez v0, :cond_0

    .line 1767937
    iget v0, v8, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->u:I

    move v0, v0

    .line 1767938
    if-le v0, v3, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->o:Z

    if-eqz v0, :cond_6

    .line 1767939
    iget v0, v8, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->x:I

    move v0, v0

    .line 1767940
    if-gt v0, v3, :cond_6

    :cond_1
    move v0, v3

    .line 1767941
    :goto_0
    sget-object v4, LX/4gI;->VIDEO_ONLY:LX/4gI;

    if-eq v2, v4, :cond_7

    if-eqz v7, :cond_7

    if-eqz v0, :cond_7

    .line 1767942
    iget-boolean v0, v8, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->l:Z

    move v0, v0

    .line 1767943
    if-eqz v0, :cond_7

    move v4, v3

    .line 1767944
    :goto_1
    sget-object v0, LX/4gI;->PHOTO_ONLY:LX/4gI;

    if-eq v2, v0, :cond_8

    sget-object v0, LX/4gI;->PHOTO_ONLY_EXCLUDING_GIFS:LX/4gI;

    if-eq v2, v0, :cond_8

    if-eqz v7, :cond_8

    .line 1767945
    iget-boolean v0, v8, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->l:Z

    move v0, v0

    .line 1767946
    if-eqz v0, :cond_8

    move v5, v3

    .line 1767947
    :goto_2
    if-eqz v7, :cond_2

    .line 1767948
    iget-boolean v0, v8, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->l:Z

    move v0, v0

    .line 1767949
    if-nez v0, :cond_9

    :cond_2
    move v2, v3

    .line 1767950
    :goto_3
    iget-boolean v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->o:Z

    if-eqz v0, :cond_a

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v0

    .line 1767951
    iget v9, v8, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->x:I

    move v9, v9

    .line 1767952
    if-lt v0, v9, :cond_a

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v0

    .line 1767953
    iget v9, v8, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->y:I

    move v9, v9

    .line 1767954
    if-gt v0, v9, :cond_a

    move v0, v3

    .line 1767955
    :goto_4
    iget-boolean v9, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->o:Z

    if-nez v9, :cond_b

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    .line 1767956
    iget v9, v8, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->u:I

    move v9, v9

    .line 1767957
    if-lt v6, v9, :cond_b

    move v6, v3

    .line 1767958
    :goto_5
    if-eqz v7, :cond_3

    invoke-virtual {v8}, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->k()Z

    move-result v7

    if-eqz v7, :cond_c

    :cond_3
    if-nez v6, :cond_4

    if-eqz v0, :cond_c

    .line 1767959
    :cond_4
    :goto_6
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ah:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    move-object v0, p0

    .line 1767960
    if-nez v1, :cond_d

    .line 1767961
    :cond_5
    :goto_7
    return-void

    :cond_6
    move v0, v1

    .line 1767962
    goto :goto_0

    :cond_7
    move v4, v1

    .line 1767963
    goto :goto_1

    :cond_8
    move v5, v1

    .line 1767964
    goto :goto_2

    :cond_9
    move v2, v1

    .line 1767965
    goto :goto_3

    :cond_a
    move v0, v1

    .line 1767966
    goto :goto_4

    :cond_b
    move v6, v1

    .line 1767967
    goto :goto_5

    :cond_c
    move v3, v1

    .line 1767968
    goto :goto_6

    .line 1767969
    :cond_d
    iput-boolean v4, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->j:Z

    .line 1767970
    iget-object v6, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1767971
    iget-object v7, v6, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    move-object v6, v7

    .line 1767972
    iget-boolean v7, v6, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->h:Z

    move v6, v7

    .line 1767973
    if-eqz v6, :cond_e

    iget-object v6, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->av:LX/0ad;

    sget-short v7, LX/5lr;->c:S

    const/4 v8, 0x0

    invoke-interface {v6, v7, v8}, LX/0ad;->a(SZ)Z

    move-result v6

    if-nez v6, :cond_f

    :cond_e
    iget-boolean v6, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->o:Z

    if-nez v6, :cond_f

    iget-object v6, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1767974
    iget-boolean v7, v6, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->f:Z

    move v6, v7

    .line 1767975
    if-eqz v6, :cond_10

    .line 1767976
    :cond_f
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v6

    const v7, 0x7f081350

    invoke-virtual {v0, v7}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1767977
    iput-object v7, v6, LX/108;->g:Ljava/lang/String;

    .line 1767978
    move-object v6, v6

    .line 1767979
    invoke-virtual {v6}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v6

    iput-object v6, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ae:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 1767980
    :goto_8
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v6

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f021839

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 1767981
    iput-object v7, v6, LX/108;->b:Landroid/graphics/drawable/Drawable;

    .line 1767982
    move-object v6, v6

    .line 1767983
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f081369

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1767984
    iput-object v7, v6, LX/108;->j:Ljava/lang/String;

    .line 1767985
    move-object v6, v6

    .line 1767986
    invoke-virtual {v6}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v6

    iput-object v6, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->af:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 1767987
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v6

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f02183c

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 1767988
    iput-object v7, v6, LX/108;->b:Landroid/graphics/drawable/Drawable;

    .line 1767989
    move-object v6, v6

    .line 1767990
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f08136a

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1767991
    iput-object v7, v6, LX/108;->j:Ljava/lang/String;

    .line 1767992
    move-object v6, v6

    .line 1767993
    invoke-virtual {v6}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v6

    iput-object v6, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ag:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 1767994
    new-instance v6, LX/BGi;

    invoke-direct {v6, v0}, LX/BGi;-><init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V

    iput-object v6, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->an:LX/107;

    .line 1767995
    new-instance v6, LX/BGj;

    invoke-direct {v6, v0}, LX/BGj;-><init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V

    iput-object v6, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ao:LX/107;

    .line 1767996
    if-eqz v2, :cond_12

    .line 1767997
    if-eqz v3, :cond_11

    iget-object v6, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->al:LX/107;

    :goto_9
    invoke-virtual {v1, v6}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setActionButtonOnClickListener(LX/107;)V

    .line 1767998
    iget-object v6, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ae:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 1767999
    iput-boolean v3, v6, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->u:Z

    .line 1768000
    iget-object v6, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ae:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    invoke-virtual {v1, v6}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setPrimaryButton(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    goto/16 :goto_7

    .line 1768001
    :cond_10
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v6

    const v7, 0x7f08134f

    invoke-virtual {v0, v7}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1768002
    iput-object v7, v6, LX/108;->g:Ljava/lang/String;

    .line 1768003
    move-object v6, v6

    .line 1768004
    invoke-virtual {v6}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v6

    iput-object v6, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ae:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    goto/16 :goto_8

    .line 1768005
    :cond_11
    iget-object v6, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->am:LX/107;

    goto :goto_9

    .line 1768006
    :cond_12
    if-eqz v4, :cond_13

    .line 1768007
    iget-object v6, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->an:LX/107;

    invoke-virtual {v1, v6}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setActionButtonOnClickListener(LX/107;)V

    .line 1768008
    iget-object v6, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->af:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    invoke-virtual {v1, v6}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setPrimaryButton(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 1768009
    :cond_13
    if-eqz v5, :cond_5

    .line 1768010
    iget-object v6, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ao:LX/107;

    invoke-virtual {v1, v6}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setSecondaryActionButtonOnClickListener(LX/107;)V

    .line 1768011
    iget-object v6, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ag:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    invoke-virtual {v1, v6}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setSecondaryButton(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    goto/16 :goto_7
.end method

.method public static z(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1768012
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1768013
    iget-object v2, v1, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    move-object v1, v2

    .line 1768014
    if-nez v1, :cond_1

    .line 1768015
    :cond_0
    :goto_0
    return v0

    .line 1768016
    :cond_1
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1768017
    iget-object v2, v1, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    move-object v1, v2

    .line 1768018
    iget-object v2, v1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->e:LX/8AB;

    move-object v1, v2

    .line 1768019
    sget-object v2, LX/8AB;->PROFILEPIC:LX/8AB;

    if-eq v1, v2, :cond_2

    sget-object v2, LX/8AB;->COVERPHOTO:LX/8AB;

    if-eq v1, v2, :cond_2

    sget-object v2, LX/8AB;->HOLIDAY_CARDS:LX/8AB;

    if-ne v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1768020
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1768021
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1768022
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->m:LX/11o;

    const-string v1, "LaunchFragmentAndDi"

    const v2, 0x30ec43de

    invoke-static {v0, v1, v2}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1768023
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->R:LX/0i5;

    sget-object v1, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->t:[Ljava/lang/String;

    new-instance v2, LX/BGp;

    invoke-direct {v2, p0}, LX/BGp;-><init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    .line 1768024
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ax:LX/75Q;

    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->W:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/75Q;->a(Ljava/lang/String;)V

    .line 1768025
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ay:LX/75F;

    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->W:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/75F;->a(Ljava/lang/String;)V

    .line 1768026
    iput-boolean v3, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->az:Z

    .line 1768027
    if-eqz p1, :cond_1

    .line 1768028
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->I:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9iY;

    .line 1768029
    const-string v1, "fb_captured_media_uri"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 1768030
    if-eqz v1, :cond_0

    .line 1768031
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1768032
    iput-object v1, v0, LX/9iY;->g:Landroid/net/Uri;

    .line 1768033
    :cond_0
    const-string v0, "fb_simple_picker_fragment_conf"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1768034
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    if-nez v0, :cond_2

    .line 1768035
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1768036
    if-eqz v0, :cond_2

    .line 1768037
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1768038
    const-string v1, "extra_simple_picker_launcher_settings"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1768039
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1768040
    const-string v1, "extra_simple_picker_launcher_settings"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1768041
    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1768042
    if-eqz v0, :cond_3

    const-string v1, "extra_simple_picker_launcher_waterfall_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1768043
    const-string v1, "extra_should_merge_camera_roll"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->aa:Z

    .line 1768044
    const-string v1, "extra_disable_camera_cell"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ab:Z

    .line 1768045
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    const-string v2, "extra_simple_picker_launcher_waterfall_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, LX/BGw;

    invoke-direct {v2, p0}, LX/BGw;-><init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V

    .line 1768046
    iput-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1768047
    iget-object v3, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1768048
    iget-object p1, v3, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    move-object v3, p1

    .line 1768049
    iget-boolean p1, v3, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->z:Z

    move v3, p1

    .line 1768050
    iput-boolean v3, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->o:Z

    .line 1768051
    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->W:Ljava/lang/String;

    .line 1768052
    iput-object v2, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->y:Landroid/view/View$OnClickListener;

    .line 1768053
    new-instance v3, LX/BHA;

    invoke-direct {v3, p0}, LX/BHA;-><init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V

    iput-object v3, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->x:LX/BHA;

    .line 1768054
    iget-object v3, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ar:LX/3l1;

    iget-object p1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->W:Ljava/lang/String;

    .line 1768055
    iput-object p1, v3, LX/3l1;->b:Ljava/lang/String;

    .line 1768056
    :cond_3
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1768057
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->d:LX/BHQ;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    invoke-static {p0}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->r(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)LX/4gI;

    move-result-object v1

    iget-boolean v2, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->aa:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ab:Z

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    iget-object v3, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->a:LX/BHj;

    iget-object v4, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->a:LX/BHj;

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, LX/BHQ;->a(LX/4gI;ZLX/BGy;LX/BHj;Ljava/lang/String;)V

    .line 1768058
    return-void

    .line 1768059
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 14

    .prologue
    .line 1768060
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->h:Z

    .line 1768061
    const/4 v0, 0x0

    .line 1768062
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->q:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 1768063
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1768064
    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    .line 1768065
    goto :goto_0

    .line 1768066
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->m:LX/11o;

    const-string v2, "RenderThumbnails"

    const/4 v3, 0x0

    const-string v4, "NumMemoryCachedMedia"

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v1

    const v4, 0x2f7eb1a0

    invoke-static {v0, v2, v3, v1, v4}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 1768067
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->k:LX/11i;

    sget-object v1, LX/BHx;->a:LX/BHw;

    invoke-interface {v0, v1}, LX/11i;->b(LX/0Pq;)V

    .line 1768068
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->l:LX/0id;

    const-string v1, "OpenMediaPicker"

    invoke-virtual {v0, v1}, LX/0id;->b(Ljava/lang/String;)V

    .line 1768069
    invoke-virtual {p0}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->c()V

    .line 1768070
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->L:LX/BGc;

    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->au:LX/BI6;

    invoke-virtual {v0, v1}, LX/BGc;->a(LX/BI6;)LX/BGb;

    move-result-object v0

    const/4 v13, 0x0

    const-wide/16 v11, -0x1

    .line 1768071
    iget-object v5, v0, LX/BGb;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v6, Lcom/facebook/photos/simplepicker/SimplePickerBackgroundTasksController$1;

    invoke-direct {v6, v0}, Lcom/facebook/photos/simplepicker/SimplePickerBackgroundTasksController$1;-><init>(LX/BGb;)V

    const v7, -0x2a1975c

    invoke-static {v5, v6, v7}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1768072
    iget-object v5, v0, LX/BGb;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v6, Lcom/facebook/photos/simplepicker/SimplePickerBackgroundTasksController$2;

    invoke-direct {v6, v0}, Lcom/facebook/photos/simplepicker/SimplePickerBackgroundTasksController$2;-><init>(LX/BGb;)V

    const v7, 0x1758f960

    invoke-static {v5, v6, v7}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1768073
    iget-object v5, v0, LX/BGb;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v6, Lcom/facebook/photos/simplepicker/SimplePickerBackgroundTasksController$3;

    invoke-direct {v6, v0}, Lcom/facebook/photos/simplepicker/SimplePickerBackgroundTasksController$3;-><init>(LX/BGb;)V

    const v7, 0x75c14846

    invoke-static {v5, v6, v7}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1768074
    iget-object v5, v0, LX/BGb;->h:LX/0Uh;

    const/16 v6, 0x267

    invoke-virtual {v5, v6, v13}, LX/0Uh;->a(IZ)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1768075
    iget-object v5, v0, LX/BGb;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v6, Lcom/facebook/photos/simplepicker/SimplePickerBackgroundTasksController$4;

    invoke-direct {v6, v0}, Lcom/facebook/photos/simplepicker/SimplePickerBackgroundTasksController$4;-><init>(LX/BGb;)V

    const v7, 0x5bdd9e98

    invoke-static {v5, v6, v7}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1768076
    :cond_1
    iget-object v5, v0, LX/BGb;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v6, LX/1Ip;->i:LX/0Tn;

    invoke-interface {v5, v6, v11, v12}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v5

    .line 1768077
    iget-object v7, v0, LX/BGb;->l:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v7

    .line 1768078
    cmp-long v9, v5, v11

    if-eqz v9, :cond_2

    sub-long v5, v7, v5

    const-wide/32 v9, 0x5265c00

    cmp-long v5, v5, v9

    if-lez v5, :cond_3

    :cond_2
    iget-object v5, v0, LX/BGb;->h:LX/0Uh;

    if-eqz v5, :cond_3

    iget-object v5, v0, LX/BGb;->h:LX/0Uh;

    const/16 v6, 0x36c

    invoke-virtual {v5, v6, v13}, LX/0Uh;->a(IZ)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1768079
    iget-object v5, v0, LX/BGb;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v6, Lcom/facebook/photos/simplepicker/SimplePickerBackgroundTasksController$5;

    invoke-direct {v6, v0, v7, v8}, Lcom/facebook/photos/simplepicker/SimplePickerBackgroundTasksController$5;-><init>(LX/BGb;J)V

    const v7, -0x6a93695e

    invoke-static {v5, v6, v7}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1768080
    :cond_3
    iget-object v5, v0, LX/BGb;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v6, LX/0dp;->s:LX/0Tn;

    invoke-interface {v5, v6, v11, v12}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v5

    .line 1768081
    cmp-long v5, v5, v11

    if-nez v5, :cond_4

    .line 1768082
    iget-object v5, v0, LX/BGb;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v6, Lcom/facebook/photos/simplepicker/SimplePickerBackgroundTasksController$6;

    invoke-direct {v6, v0}, Lcom/facebook/photos/simplepicker/SimplePickerBackgroundTasksController$6;-><init>(LX/BGb;)V

    const v7, 0x1fc2b202

    invoke-static {v5, v6, v7}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1768083
    :cond_4
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->O:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/1Ip;->b:LX/0Tn;

    iget-object v2, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->P:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1768084
    return-void

    :cond_5
    move v0, v1

    goto/16 :goto_1
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1768085
    iget-boolean v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->X:Z

    if-nez v0, :cond_0

    .line 1768086
    goto :goto_1

    .line 1768087
    :cond_0
    :goto_0
    return-void

    .line 1768088
    :goto_1
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1768089
    iget-object v1, v0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    move-object v0, v1

    .line 1768090
    iget-boolean v1, v0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->f:Z

    move v0, v1

    .line 1768091
    if-eqz v0, :cond_0

    .line 1768092
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->X:Z

    .line 1768093
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->B:LX/9iZ;

    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->W:Ljava/lang/String;

    .line 1768094
    invoke-virtual {v0}, LX/9iZ;->b()V

    .line 1768095
    iget-object p0, v0, LX/9iZ;->c:LX/747;

    .line 1768096
    iput-object v1, p0, LX/747;->b:Ljava/lang/String;

    .line 1768097
    goto :goto_0
.end method

.method public final e()V
    .locals 5

    .prologue
    .line 1768098
    invoke-direct {p0}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->t()LX/0Px;

    move-result-object v1

    .line 1768099
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->M:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    sget-object v2, LX/BHC;->FETCH_TAGGING_DATA:LX/BHC;

    invoke-static {p0}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->A(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)Ljava/util/concurrent/Callable;

    move-result-object v3

    new-instance v4, LX/BGm;

    invoke-direct {v4, p0, v1}, LX/BGm;-><init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;LX/0Px;)V

    invoke-virtual {v0, v2, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1768100
    return-void
.end method

.method public final k()V
    .locals 10

    .prologue
    const/16 v3, 0x8

    const/4 v6, 0x0

    .line 1768101
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->av:LX/0ad;

    sget-short v1, LX/8Jz;->D:S

    invoke-interface {v0, v1, v6}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1768102
    iget-object v1, v0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    move-object v0, v1

    .line 1768103
    if-nez v0, :cond_1

    .line 1768104
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ai:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/Toolbar;->setVisibility(I)V

    .line 1768105
    :goto_0
    return-void

    .line 1768106
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->av:LX/0ad;

    sget-char v1, LX/8Jz;->C:C

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1768107
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1768108
    iget-object v2, v1, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    move-object v1, v2

    .line 1768109
    iget-object v2, v1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->e:LX/8AB;

    move-object v1, v2

    .line 1768110
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1}, LX/8AB;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1768111
    :cond_2
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ai:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/Toolbar;->setVisibility(I)V

    goto :goto_0

    .line 1768112
    :cond_3
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->c:LX/BHJ;

    invoke-virtual {v0}, LX/BHJ;->b()LX/0Px;

    move-result-object v1

    .line 1768113
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->U:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8OU;

    invoke-virtual {v0, v1}, LX/8OU;->a(Ljava/util/List;)LX/0am;

    move-result-object v0

    .line 1768114
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v2

    if-nez v2, :cond_4

    .line 1768115
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ai:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/Toolbar;->setVisibility(I)V

    goto :goto_0

    .line 1768116
    :cond_4
    iget-object v2, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ai:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v2, v6}, Landroid/support/v7/widget/Toolbar;->setVisibility(I)V

    .line 1768117
    iget-object v2, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->aj:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08135f

    const v5, 0x7f081361

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    invoke-static {v3, v4, v5, v1}, LX/1z0;->a(Landroid/content/res/Resources;III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1768118
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ak:Lcom/facebook/resources/ui/FbTextView;

    const-string v2, "%s: %d%s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const v4, 0x7f081393

    invoke-virtual {p0, v4}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x1

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    long-to-double v6, v6

    const-wide/high16 v8, 0x4090000000000000L    # 1024.0

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-int v0, v6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x2

    const-string v4, "kb"

    aput-object v4, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 1768119
    if-eq p2, v2, :cond_0

    invoke-static {p0}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->z(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1768120
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->c:LX/BHJ;

    .line 1768121
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1768122
    invoke-virtual {v0, v1}, LX/BHJ;->a(LX/0Px;)V

    .line 1768123
    invoke-static {p0}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->w(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V

    .line 1768124
    :cond_0
    sparse-switch p1, :sswitch_data_0

    .line 1768125
    :cond_1
    :goto_0
    return-void

    .line 1768126
    :sswitch_0
    invoke-static {p1}, LX/9iY;->a(I)LX/9iX;

    move-result-object v1

    .line 1768127
    const/4 v0, -0x1

    if-ne p2, v0, :cond_a

    .line 1768128
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->z:LX/BGe;

    invoke-virtual {v1}, LX/9iX;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1768129
    sget-object p1, LX/BGd;->CAMERA_MEDIA_CAPTURED:LX/BGd;

    invoke-static {p1}, LX/BGe;->a(LX/BGd;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p2, "media_type"

    invoke-virtual {p1, p2, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-static {v0, p1}, LX/BGe;->a(LX/BGe;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1768130
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->I:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9iY;

    iget-object v2, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->x:LX/BHA;

    invoke-virtual {v0, v1, p3, v2}, LX/9iY;->a(LX/9iX;Landroid/content/Intent;LX/9Tq;)V

    .line 1768131
    :goto_1
    goto :goto_0

    .line 1768132
    :sswitch_1
    if-ne p2, v2, :cond_1

    .line 1768133
    const-string v0, "extra_video_item"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 1768134
    iget-boolean v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ap:Z

    if-eqz v1, :cond_2

    .line 1768135
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->z:LX/BGe;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->l()LX/4gF;

    move-result-object v2

    invoke-virtual {v2}, LX/4gF;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/BGe;->b(Ljava/lang/String;)V

    .line 1768136
    :cond_2
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->c:LX/BHJ;

    invoke-virtual {v1, v0}, LX/BHJ;->a(Lcom/facebook/ipc/media/MediaItem;)Z

    goto :goto_0

    .line 1768137
    :sswitch_2
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1768138
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 1768139
    if-nez p2, :cond_1

    .line 1768140
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->z:LX/BGe;

    if-eqz v0, :cond_1

    .line 1768141
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->z:LX/BGe;

    .line 1768142
    sget-object v1, LX/BGd;->RETURN_TO_COMPOSER:LX/BGd;

    invoke-static {v1}, LX/BGe;->a(LX/BGd;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-static {v0, v1}, LX/BGe;->a(LX/BGe;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1768143
    goto :goto_0

    .line 1768144
    :sswitch_3
    if-ne p2, v2, :cond_1

    .line 1768145
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1768146
    iget-object v1, v0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    move-object v0, v1

    .line 1768147
    iget-object v1, v0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->e:LX/8AB;

    move-object v0, v1

    .line 1768148
    sget-object v1, LX/8AB;->FUNDRAISER_CREATION:LX/8AB;

    if-ne v0, v1, :cond_3

    invoke-direct {p0}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->t()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1768149
    const-string v1, "original_image_item"

    invoke-direct {p0}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->t()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1768150
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v2, p3}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1768151
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    .line 1768152
    :sswitch_4
    if-ne p2, v2, :cond_1

    .line 1768153
    const-string v0, "profile_photo_method_extra"

    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->Z:Ljava/lang/String;

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1768154
    const-string v0, "camera_roll"

    invoke-virtual {p3, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1768155
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v2, p3}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1768156
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    .line 1768157
    :sswitch_5
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1768158
    iget-object v1, v0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    move-object v0, v1

    .line 1768159
    iget-object v1, v0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->e:LX/8AB;

    move-object v0, v1

    .line 1768160
    if-ne p2, v2, :cond_5

    sget-object v1, LX/8AB;->PROFILEPIC:LX/8AB;

    invoke-virtual {v1, v0}, LX/8AB;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1768161
    const-string v0, "creative_cam_result_extra"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/creativecam/CreativeCamResult;

    .line 1768162
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    if-nez v1, :cond_b

    .line 1768163
    :cond_4
    :goto_2
    goto/16 :goto_0

    .line 1768164
    :cond_5
    if-ne p2, v2, :cond_7

    sget-object v1, LX/8AB;->COMPOSER_ADD_MORE_MEDIA:LX/8AB;

    invoke-virtual {v1, v0}, LX/8AB;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1768165
    const-string v0, "creative_cam_result_extra"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/creativecam/CreativeCamResult;

    .line 1768166
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1768167
    invoke-static {p0}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->C(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    .line 1768168
    if-eqz v0, :cond_6

    .line 1768169
    iget-object v2, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->aC:LX/74n;

    .line 1768170
    iget-object v3, v0, Lcom/facebook/ipc/creativecam/CreativeCamResult;->b:Landroid/net/Uri;

    move-object v3, v3

    .line 1768171
    sget-object v4, LX/74j;->CREATIVECAM_MEDIA:LX/74j;

    invoke-virtual {v2, v3, v4}, LX/74n;->a(Landroid/net/Uri;LX/74j;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    .line 1768172
    invoke-static {v2}, LX/7kv;->a(Lcom/facebook/ipc/media/MediaItem;)LX/7kv;

    move-result-object v2

    .line 1768173
    iget-object v3, v0, Lcom/facebook/ipc/creativecam/CreativeCamResult;->c:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v3, v3

    .line 1768174
    iput-object v3, v2, LX/7kv;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1768175
    move-object v2, v2

    .line 1768176
    invoke-virtual {v2}, LX/7kv;->a()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v2

    .line 1768177
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialAttachments(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setUseOptimisticPosting(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 1768178
    :cond_6
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->a(Lcom/facebook/photos/simplepicker/SimplePickerFragment;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)V

    .line 1768179
    goto/16 :goto_0

    .line 1768180
    :cond_7
    if-eqz p2, :cond_1

    .line 1768181
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1768182
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    .line 1768183
    :sswitch_6
    if-ne p2, v2, :cond_8

    .line 1768184
    const-string v0, "inspiration_camera_uri"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 1768185
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1768186
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->aC:LX/74n;

    sget-object v2, LX/74j;->CREATIVECAM_MEDIA:LX/74j;

    invoke-virtual {v1, v0, v2}, LX/74n;->a(Landroid/net/Uri;LX/74j;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    .line 1768187
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->a$redex0(Lcom/facebook/photos/simplepicker/SimplePickerFragment;LX/0Px;Z)V

    .line 1768188
    goto/16 :goto_0

    .line 1768189
    :cond_8
    if-eqz p2, :cond_1

    .line 1768190
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1768191
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    .line 1768192
    :sswitch_7
    if-ne p2, v2, :cond_1

    .line 1768193
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1768194
    iget-object v1, v0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->b:LX/8A9;

    move-object v0, v1

    .line 1768195
    sget-object v1, LX/8A9;->LAUNCH_COMPOSER:LX/8A9;

    if-eq v0, v1, :cond_9

    const-string v0, "extra_media_items"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1768196
    const-string v0, "extra_slideshow_data"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->p:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    .line 1768197
    const-string v0, "extra_media_items"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1768198
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    invoke-static {p0, v0, v4}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->a$redex0(Lcom/facebook/photos/simplepicker/SimplePickerFragment;LX/0Px;Z)V

    goto/16 :goto_0

    .line 1768199
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1768200
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    .line 1768201
    :sswitch_8
    if-ne p2, v2, :cond_1

    .line 1768202
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->aC:LX/74n;

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    sget-object v2, LX/74j;->SINGLE_SHOT_CAMERA:LX/74j;

    invoke-virtual {v0, v1, v2}, LX/74n;->a(Landroid/net/Uri;LX/74j;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    .line 1768203
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->x:LX/BHA;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0, v3}, LX/BHA;->a(LX/0Px;Z)V

    goto/16 :goto_0

    .line 1768204
    :cond_a
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->z:LX/BGe;

    invoke-virtual {v1}, LX/9iX;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1768205
    sget-object v2, LX/BGd;->CAMERA_CANCELLED:LX/BGd;

    invoke-static {v2}, LX/BGe;->a(LX/BGd;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string p0, "media_type"

    invoke-virtual {v2, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-static {v0, v2}, LX/BGe;->a(LX/BGe;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1768206
    goto/16 :goto_1

    .line 1768207
    :cond_b
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1768208
    iget-object v2, v1, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->b:LX/8A9;

    move-object v1, v2

    .line 1768209
    sget-object v2, LX/8A9;->RETURN_TO_STAGING_GROUND:LX/8A9;

    if-ne v1, v2, :cond_c

    .line 1768210
    iget-object v1, v0, Lcom/facebook/ipc/creativecam/CreativeCamResult;->b:Landroid/net/Uri;

    move-object v2, v1

    .line 1768211
    iget-object v1, v0, Lcom/facebook/ipc/creativecam/CreativeCamResult;->c:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v1, v1

    .line 1768212
    if-nez v1, :cond_d

    const/4 v1, 0x0

    :goto_3
    invoke-static {p0, v2, v1}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->a$redex0(Lcom/facebook/photos/simplepicker/SimplePickerFragment;Landroid/net/Uri;Ljava/lang/String;)V

    .line 1768213
    :cond_c
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1768214
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->aC:LX/74n;

    .line 1768215
    iget-object v2, v0, Lcom/facebook/ipc/creativecam/CreativeCamResult;->b:Landroid/net/Uri;

    move-object v2, v2

    .line 1768216
    sget-object v3, LX/74j;->CREATIVECAM_MEDIA:LX/74j;

    invoke-virtual {v1, v2, v3}, LX/74n;->a(Landroid/net/Uri;LX/74j;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    .line 1768217
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1768218
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 1768219
    iget-object v2, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->x:LX/BHA;

    if-eqz v2, :cond_4

    .line 1768220
    iget-object v2, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->x:LX/BHA;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, LX/BHA;->a(LX/0Px;Z)V

    goto/16 :goto_2

    .line 1768221
    :cond_d
    iget-object v1, v0, Lcom/facebook/ipc/creativecam/CreativeCamResult;->c:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v1, v1

    .line 1768222
    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFilterName()Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    :sswitch_data_0
    .sparse-switch
        0x3e8 -> :sswitch_1
        0x4d8 -> :sswitch_2
        0x4d9 -> :sswitch_3
        0x4da -> :sswitch_4
        0x4db -> :sswitch_5
        0x4dc -> :sswitch_7
        0x4dd -> :sswitch_6
        0x4e8 -> :sswitch_8
        0x7d2 -> :sswitch_0
        0x7d3 -> :sswitch_0
    .end sparse-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12

    .prologue
    const/4 v9, 0x1

    const/4 v7, 0x0

    const/4 v10, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x6a3aa132

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v11

    .line 1768223
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->m:LX/11o;

    const-string v1, "InflateViews"

    const v2, -0x48f9bd7d

    invoke-static {v0, v1, v2}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1768224
    const v0, 0x7f031332

    invoke-virtual {p1, v0, p2, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1768225
    const v0, 0x7f0d04e7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Landroid/support/v7/widget/RecyclerView;

    .line 1768226
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1768227
    iget-object v2, v0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    move-object v4, v2

    .line 1768228
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->Q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Axt;

    invoke-virtual {v0}, LX/Axt;->a()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->n:LX/0Px;

    .line 1768229
    new-instance v0, LX/BHB;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2, v9}, LX/BHB;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v8, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1768230
    new-instance v0, LX/BH6;

    invoke-direct {v0}, LX/BH6;-><init>()V

    invoke-virtual {v8, v0}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 1768231
    invoke-virtual {v8, v7}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(LX/1Of;)V

    .line 1768232
    invoke-virtual {v8}, Landroid/support/v7/widget/RecyclerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1768233
    iget-boolean v2, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->aa:Z

    if-eqz v2, :cond_0

    .line 1768234
    iput v10, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 1768235
    invoke-virtual {v8, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1768236
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->z:LX/BGe;

    iget-object v2, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->W:Ljava/lang/String;

    .line 1768237
    iput-object v2, v0, LX/BGe;->a:Ljava/lang/String;

    .line 1768238
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1768239
    iget-object v2, v0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->b:LX/8A9;

    move-object v0, v2

    .line 1768240
    sget-object v2, LX/8A9;->LAUNCH_COMPOSER:LX/8A9;

    if-ne v0, v2, :cond_1

    .line 1768241
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1768242
    iget-object v2, v0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-object v0, v2

    .line 1768243
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1768244
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->as:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gd;

    iget-object v2, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->W:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1768245
    iget-object v5, v3, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-object v3, v5

    .line 1768246
    invoke-virtual {v0, v2, v3}, LX/0gd;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)V

    .line 1768247
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->J:LX/BHK;

    .line 1768248
    new-instance v2, LX/BH4;

    invoke-direct {v2, p0}, LX/BH4;-><init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V

    move-object v2, v2

    .line 1768249
    iget-object v3, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->n:LX/0Px;

    invoke-virtual {v0, v4, v2, v3}, LX/BHK;->a(Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;LX/BH4;LX/0Px;)LX/BHJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->c:LX/BHJ;

    .line 1768250
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->c:LX/BHJ;

    iget-object v2, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->A:LX/74q;

    invoke-virtual {v0, v2}, LX/BHJ;->a(LX/74q;)V

    .line 1768251
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->c:LX/BHJ;

    .line 1768252
    new-instance v2, LX/BGo;

    invoke-direct {v2, p0}, LX/BGo;-><init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V

    move-object v2, v2

    .line 1768253
    invoke-virtual {v0, v2}, LX/BHJ;->a(LX/74q;)V

    .line 1768254
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->c:LX/BHJ;

    .line 1768255
    new-instance v2, LX/BGr;

    invoke-direct {v2, p0}, LX/BGr;-><init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V

    move-object v2, v2

    .line 1768256
    invoke-virtual {v0, v2}, LX/BHJ;->a(LX/74q;)V

    .line 1768257
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->c:LX/BHJ;

    .line 1768258
    new-instance v2, LX/BGq;

    invoke-direct {v2, p0}, LX/BGq;-><init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V

    move-object v2, v2

    .line 1768259
    invoke-virtual {v0, v2}, LX/BHJ;->a(LX/74q;)V

    .line 1768260
    iget-boolean v0, v4, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->w:Z

    move v0, v0

    .line 1768261
    if-eqz v0, :cond_5

    .line 1768262
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->av:LX/0ad;

    sget-object v2, LX/0c0;->Live:LX/0c0;

    sget-short v3, LX/1EB;->P:S

    invoke-interface {v0, v2, v3, v10}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1768263
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->c:LX/BHJ;

    .line 1768264
    iget-object v2, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->E:LX/BHh;

    if-nez v2, :cond_2

    .line 1768265
    new-instance v2, LX/BHh;

    iget-object v3, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1768266
    iget-object v5, v3, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    move-object v3, v5

    .line 1768267
    new-instance v5, LX/BH1;

    invoke-direct {v5, p0}, LX/BH1;-><init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V

    invoke-direct {v2, v3, v1, v8, v5}, LX/BHh;-><init>(Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;Landroid/view/View;Landroid/view/View;LX/BH0;)V

    iput-object v2, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->E:LX/BHh;

    .line 1768268
    :cond_2
    new-instance v2, LX/BH2;

    invoke-direct {v2, p0}, LX/BH2;-><init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V

    move-object v2, v2

    .line 1768269
    invoke-virtual {v0, v2}, LX/BHJ;->a(LX/74q;)V

    .line 1768270
    :cond_3
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->c:LX/BHJ;

    .line 1768271
    iget-object v2, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->F:LX/BHq;

    if-nez v2, :cond_4

    .line 1768272
    new-instance v2, LX/BHq;

    iget-object v3, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1768273
    iget-object v5, v3, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    move-object v3, v5

    .line 1768274
    invoke-direct {v2, v1, v3}, LX/BHq;-><init>(Landroid/view/View;Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;)V

    iput-object v2, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->F:LX/BHq;

    .line 1768275
    :cond_4
    new-instance v2, LX/BH3;

    invoke-direct {v2, p0}, LX/BH3;-><init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V

    move-object v2, v2

    .line 1768276
    invoke-virtual {v0, v2}, LX/BHJ;->a(LX/74q;)V

    .line 1768277
    :cond_5
    iget-boolean v0, v4, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->s:Z

    move v0, v0

    .line 1768278
    iput-boolean v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ac:Z

    .line 1768279
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1768280
    const v0, 0x7f0d00bc

    invoke-static {v1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ah:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 1768281
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1768282
    iget-object v2, v0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->i:Ljava/lang/String;

    move-object v0, v2

    .line 1768283
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1768284
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ah:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    const v2, 0x7f08134c

    invoke-virtual {v0, v2}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(I)V

    .line 1768285
    :goto_0
    const v0, 0x7f0d2c7f

    invoke-static {v1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ai:Landroid/support/v7/widget/Toolbar;

    .line 1768286
    const v0, 0x7f0d2c80

    invoke-static {v1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->aj:Lcom/facebook/resources/ui/FbTextView;

    .line 1768287
    const v0, 0x7f0d2c81

    invoke-static {v1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ak:Lcom/facebook/resources/ui/FbTextView;

    .line 1768288
    invoke-virtual {p0}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->k()V

    .line 1768289
    const v0, 0x7f0d04de

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->i:Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;

    .line 1768290
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->i:Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;

    new-instance v2, LX/BH9;

    invoke-direct {v2, p0}, LX/BH9;-><init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;->a(LX/0xi;)V

    .line 1768291
    new-instance v0, LX/BGg;

    invoke-direct {v0, p0}, LX/BGg;-><init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->al:LX/107;

    .line 1768292
    new-instance v0, LX/BGh;

    invoke-direct {v0, p0}, LX/BGh;-><init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->am:LX/107;

    .line 1768293
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ah:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iget-object v2, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->y:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 1768294
    invoke-static {p0}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->w(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V

    .line 1768295
    if-eqz p3, :cond_a

    .line 1768296
    const-string v0, "selected_media_items"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1768297
    iget-object v2, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->c:LX/BHJ;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/BHJ;->a(LX/0Px;)V

    .line 1768298
    :goto_1
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->F:LX/BHq;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->c:LX/BHJ;

    invoke-virtual {v0}, LX/BHJ;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1768299
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->F:LX/BHq;

    iget-boolean v2, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->o:Z

    iget-object v3, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->c:LX/BHJ;

    invoke-virtual {v3}, LX/BHJ;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/BHq;->a(ZLX/0Px;)V

    .line 1768300
    :cond_6
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->at:LX/BI7;

    .line 1768301
    iget-boolean v2, v4, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->i:Z

    move v2, v2

    .line 1768302
    iget-boolean v3, v4, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->w:Z

    move v3, v3

    .line 1768303
    iget v5, v4, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->x:I

    move v4, v5

    .line 1768304
    iget-object v5, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->E:LX/BHh;

    iget-object v6, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->W:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, LX/BI7;->a(Landroid/view/View;ZZILX/BHh;Ljava/lang/String;)LX/BI6;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->au:LX/BI6;

    .line 1768305
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->c:LX/BHJ;

    iget-object v2, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->au:LX/BI6;

    .line 1768306
    iput-object v2, v0, LX/BHJ;->A:LX/BI6;

    .line 1768307
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->m:LX/11o;

    const-string v2, "InflateViews"

    const v3, -0xe7674da

    invoke-static {v0, v2, v3}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1768308
    iget-boolean v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->aa:Z

    if-eqz v0, :cond_7

    .line 1768309
    const v0, 0x7f0d00bc

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1768310
    :cond_7
    new-instance v0, LX/BH5;

    invoke-direct {v0, p0}, LX/BH5;-><init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V

    move-object v0, v0

    .line 1768311
    iget-object v2, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->w:LX/BHm;

    iget-object v3, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1768312
    iget-object v4, v3, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    move-object v3, v4

    .line 1768313
    iget-boolean v4, v3, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->p:Z

    move v3, v4

    .line 1768314
    iget-object v4, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1768315
    iget-object v5, v4, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    move-object v4, v5

    .line 1768316
    iget-boolean v5, v4, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->r:Z

    move v4, v5

    .line 1768317
    invoke-virtual {v2, v8, v0, v3, v4}, LX/BHm;->a(Landroid/support/v7/widget/RecyclerView;LX/BH5;ZZ)LX/BHl;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->e:LX/BHl;

    .line 1768318
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->e:LX/BHl;

    .line 1768319
    if-eqz p3, :cond_8

    .line 1768320
    const-string v2, "souvenir_pager_indicator_index"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v0, LX/BHl;->h:I

    .line 1768321
    :cond_8
    iget-object v2, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->c:LX/BHJ;

    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1768322
    iget-object v3, v0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    move-object v0, v3

    .line 1768323
    iget-boolean v3, v0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->k:Z

    move v0, v3

    .line 1768324
    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->i:Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;

    :goto_2
    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iget-object v3, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->e:LX/BHl;

    iget-object v4, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1768325
    iget-object v5, v4, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    move-object v4, v5

    .line 1768326
    iget-boolean v5, v4, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->o:Z

    move v4, v5

    .line 1768327
    invoke-static {v1, v2, v0, v3, v4}, LX/BHn;->a(Landroid/view/View;LX/BHJ;LX/0am;LX/BHl;Z)LX/BHj;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->a:LX/BHj;

    .line 1768328
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->a:LX/BHj;

    new-instance v2, LX/BGx;

    invoke-direct {v2, p0}, LX/BGx;-><init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V

    .line 1768329
    iput-object v2, v0, LX/BHj;->h:LX/BGx;

    .line 1768330
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1768331
    if-eqz v0, :cond_9

    const-string v2, "extra_simple_picker_launcher_waterfall_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1768332
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->q:Ljava/util/Map;

    .line 1768333
    iget-object v2, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->d:LX/BHQ;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    invoke-static {p0}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->r(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)LX/4gI;

    move-result-object v3

    iget-boolean v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->aa:Z

    if-eqz v0, :cond_c

    iget-boolean v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ab:Z

    if-nez v0, :cond_c

    move v4, v9

    :goto_3
    new-instance v5, LX/BGz;

    invoke-direct {v5, p0}, LX/BGz;-><init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V

    iget-object v6, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->a:LX/BHj;

    invoke-virtual/range {v2 .. v7}, LX/BHQ;->a(LX/4gI;ZLX/BGy;LX/BHj;Ljava/lang/String;)V

    .line 1768334
    :cond_9
    const v0, 0x76547dad

    invoke-static {v0, v11}, LX/02F;->f(II)V

    return-object v1

    .line 1768335
    :cond_a
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->c:LX/BHJ;

    .line 1768336
    iget-object v2, v4, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->d:LX/0Px;

    move-object v2, v2

    .line 1768337
    invoke-virtual {v0, v2}, LX/BHJ;->a(LX/0Px;)V

    goto/16 :goto_1

    :cond_b
    move-object v0, v7

    .line 1768338
    goto :goto_2

    :cond_c
    move v4, v10

    .line 1768339
    goto :goto_3

    .line 1768340
    :cond_d
    iget-object v2, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ah:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-virtual {v2, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3480ce9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1768341
    iget-boolean v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->az:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1768342
    iget-object v2, v1, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    move-object v1, v2

    .line 1768343
    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1768344
    iget-object v2, v1, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    move-object v1, v2

    .line 1768345
    iget-object v2, v1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->e:LX/8AB;

    move-object v1, v2

    .line 1768346
    sget-object v2, LX/8AB;->COMPOSER_ADD_MORE_MEDIA:LX/8AB;

    if-eq v1, v2, :cond_0

    .line 1768347
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ax:LX/75Q;

    iget-object v2, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->W:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/75Q;->b(Ljava/lang/String;)V

    .line 1768348
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ay:LX/75F;

    iget-object v2, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->W:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/75F;->b(Ljava/lang/String;)V

    .line 1768349
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1768350
    const/16 v1, 0x2b

    const v2, 0x48690abf

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1ee606c2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1768351
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1768352
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->a:LX/BHj;

    .line 1768353
    iget-object v2, v0, LX/BHj;->b:LX/BHl;

    if-eqz v2, :cond_0

    .line 1768354
    iget-object v2, v0, LX/BHj;->b:LX/BHl;

    .line 1768355
    iget-object v0, v2, LX/BHl;->f:LX/BHd;

    if-eqz v0, :cond_0

    .line 1768356
    iget-object v0, v2, LX/BHl;->f:LX/BHd;

    .line 1768357
    iget-object v2, v0, LX/BHd;->e:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;

    .line 1768358
    iget-object v0, v2, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->m:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->close()V

    .line 1768359
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->g:Z

    .line 1768360
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->M:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 1768361
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->d:LX/BHQ;

    .line 1768362
    iget-object v2, v0, LX/BHQ;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Ck;

    invoke-virtual {v2}, LX/1Ck;->c()V

    .line 1768363
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->N:LX/75S;

    .line 1768364
    const/4 v2, 0x0

    iput-object v2, v0, LX/75S;->e:Ljava/util/Map;

    .line 1768365
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1768366
    iget-object v2, v0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    move-object v0, v2

    .line 1768367
    iget-object v2, v0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->e:LX/8AB;

    move-object v0, v2

    .line 1768368
    sget-object v2, LX/8AB;->COMPOSER_ADD_MORE_MEDIA:LX/8AB;

    if-eq v0, v2, :cond_1

    .line 1768369
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->A:LX/74q;

    invoke-interface {v0}, LX/74q;->a()V

    .line 1768370
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->a:LX/BHj;

    .line 1768371
    iget-object v2, v0, LX/BHj;->b:LX/BHl;

    if-eqz v2, :cond_3

    .line 1768372
    iget-object v2, v0, LX/BHj;->b:LX/BHl;

    .line 1768373
    iget-object v4, v2, LX/BHl;->f:LX/BHd;

    if-eqz v4, :cond_3

    .line 1768374
    iget-object v4, v2, LX/BHl;->f:LX/BHd;

    .line 1768375
    iget-object v0, v4, LX/BHd;->e:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;

    .line 1768376
    iget-object v4, v0, LX/3tK;->c:Landroid/database/Cursor;

    if-eqz v4, :cond_2

    .line 1768377
    iget-object v4, v0, LX/3tK;->c:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 1768378
    :cond_2
    iget-object v4, v2, LX/BHl;->f:LX/BHd;

    invoke-virtual {v4}, LX/1OM;->notifyDataSetChanged()V

    .line 1768379
    :cond_3
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->au:LX/BI6;

    .line 1768380
    invoke-static {}, LX/BI3;->values()[LX/BI3;

    move-result-object v5

    array-length v6, v5

    const/4 v2, 0x0

    move v4, v2

    :goto_0
    if-ge v4, v6, :cond_4

    aget-object v2, v5, v4

    .line 1768381
    iget-object v7, v0, LX/BI6;->e:LX/0iA;

    iget-object v2, v2, LX/BI3;->interstitialId:Ljava/lang/String;

    const-class p0, LX/3ko;

    invoke-virtual {v7, v2, p0}, LX/0iA;->a(Ljava/lang/String;Ljava/lang/Class;)LX/0i1;

    move-result-object v2

    check-cast v2, LX/3ko;

    .line 1768382
    invoke-virtual {v2}, LX/3ko;->d()V

    .line 1768383
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0

    .line 1768384
    :cond_4
    const/16 v0, 0x2b

    const v2, -0xb4977b2

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x90c5c5c

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1768385
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1768386
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->m:LX/11o;

    const-string v2, "LoadMediaItems"

    const v3, 0x2de4d07e

    invoke-static {v1, v2, v3}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1768387
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->a:LX/BHj;

    invoke-static {p0}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->r(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)LX/4gI;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/BHj;->a(LX/4gI;)V

    .line 1768388
    iget-boolean v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->g:Z

    if-eqz v1, :cond_6

    .line 1768389
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->a(Ljava/lang/String;)V

    .line 1768390
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1768391
    iget-object v2, v1, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    move-object v1, v2

    .line 1768392
    iget-object v2, v1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->e:LX/8AB;

    move-object v1, v2

    .line 1768393
    sget-object v2, LX/8AB;->COMPOSER_ADD_MORE_MEDIA:LX/8AB;

    invoke-virtual {v1, v2}, LX/8AB;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1768394
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->A:LX/74q;

    invoke-interface {v1}, LX/74q;->b()V

    .line 1768395
    :cond_1
    iget-boolean v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->g:Z

    if-eqz v1, :cond_3

    .line 1768396
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->c:LX/BHJ;

    invoke-virtual {v1}, LX/BHJ;->b()LX/0Px;

    move-result-object v2

    .line 1768397
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->K:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/BHL;

    invoke-virtual {v1, v2}, LX/BHL;->a(LX/0Px;)LX/0Px;

    move-result-object v1

    .line 1768398
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    if-eq v2, v3, :cond_2

    .line 1768399
    iget-object v2, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->c:LX/BHJ;

    invoke-virtual {v2, v1}, LX/BHJ;->a(LX/0Px;)V

    .line 1768400
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1768401
    invoke-static {p0}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->w(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V

    .line 1768402
    :cond_2
    iput-boolean v4, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->g:Z

    .line 1768403
    :cond_3
    iput-boolean v4, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->V:Z

    .line 1768404
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->au:LX/BI6;

    invoke-virtual {v1}, LX/BI6;->a()V

    .line 1768405
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->a:LX/BHj;

    invoke-virtual {v1}, LX/BHj;->a()Z

    move-result v1

    if-nez v1, :cond_4

    iget-boolean v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->aa:Z

    if-eqz v1, :cond_5

    iget-boolean v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ab:Z

    if-nez v1, :cond_5

    .line 1768406
    :cond_4
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->e:LX/BHl;

    new-instance v2, LX/BGf;

    invoke-direct {v2, p0}, LX/BGf;-><init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V

    invoke-virtual {v1, v2}, LX/BHl;->a(LX/BGf;)V

    .line 1768407
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->k()V

    .line 1768408
    const v1, 0x5e5ac065

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void

    .line 1768409
    :cond_6
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->r:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    .line 1768410
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->a:LX/BHj;

    iget-object v2, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->r:Landroid/database/Cursor;

    invoke-virtual {v1, v2}, LX/BHj;->a(Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1768411
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    .line 1768412
    const-string v0, "GALLERY_FRAGMENT"

    invoke-virtual {v1, v0}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    .line 1768413
    if-eqz v0, :cond_0

    .line 1768414
    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->c()I

    .line 1768415
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1768416
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->I:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9iY;

    .line 1768417
    iget-object v1, v0, LX/9iY;->g:Landroid/net/Uri;

    move-object v1, v1

    .line 1768418
    if-eqz v1, :cond_1

    .line 1768419
    const-string v2, "fb_captured_media_uri"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1768420
    :cond_1
    const-string v0, "fb_simple_picker_fragment_conf"

    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1768421
    const-string v0, "selected_media_items"

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {p0}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->t()LX/0Px;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1768422
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->e:LX/BHl;

    .line 1768423
    iget-object v1, v0, LX/BHl;->f:LX/BHd;

    if-eqz v1, :cond_2

    .line 1768424
    const-string v1, "souvenir_pager_indicator_index"

    iget-object v2, v0, LX/BHl;->f:LX/BHd;

    .line 1768425
    iget v0, v2, LX/BHd;->f:I

    move v2, v0

    .line 1768426
    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1768427
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->az:Z

    .line 1768428
    return-void
.end method
