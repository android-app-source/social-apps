.class public final Lcom/facebook/photos/simplepicker/SimplePickerFragment$17;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:LX/4BY;

.field public final synthetic c:Lcom/facebook/photos/simplepicker/SimplePickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;LX/0Px;LX/4BY;)V
    .locals 0

    .prologue
    .line 1767487
    iput-object p1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment$17;->c:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iput-object p2, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment$17;->a:LX/0Px;

    iput-object p3, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment$17;->b:LX/4BY;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 13

    .prologue
    .line 1767488
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment$17;->c:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment$17;->a:LX/0Px;

    .line 1767489
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 1767490
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v6

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_1

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/ipc/media/MediaItem;

    .line 1767491
    instance-of v7, v3, Lcom/facebook/photos/base/media/PhotoItem;

    if-eqz v7, :cond_0

    .line 1767492
    iget-object v7, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->S:LX/BGa;

    .line 1767493
    iget-object v8, v3, Lcom/facebook/ipc/media/MediaItem;->c:Lcom/facebook/ipc/media/data/LocalMediaData;

    move-object v8, v8

    .line 1767494
    iget-wide v11, v8, Lcom/facebook/ipc/media/data/LocalMediaData;->mMediaStoreId:J

    move-wide v9, v11

    .line 1767495
    invoke-static {v9, v10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, LX/BGa;->a(Ljava/lang/String;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v7

    .line 1767496
    if-eqz v7, :cond_0

    .line 1767497
    invoke-static {v3, v7}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->a(Lcom/facebook/ipc/media/MediaItem;Landroid/net/Uri;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v3

    invoke-virtual {v5, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1767498
    :goto_1
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    .line 1767499
    :cond_0
    invoke-virtual {v5, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1767500
    :cond_1
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    move-object v0, v3

    .line 1767501
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment$17;->c:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v1, v1, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->aA:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/photos/simplepicker/SimplePickerFragment$17$1;

    invoke-direct {v2, p0, v0}, Lcom/facebook/photos/simplepicker/SimplePickerFragment$17$1;-><init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment$17;LX/0Px;)V

    const v0, -0x61edcac2

    invoke-static {v1, v2, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1767502
    return-void
.end method
