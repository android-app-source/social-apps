.class public Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f2;
.implements LX/0l6;


# static fields
.field private static final p:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private q:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

.field private r:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

.field private s:LX/BGe;

.field private t:Ljava/lang/String;

.field private u:LX/11i;

.field private v:LX/0id;

.field private w:LX/11o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/11o",
            "<",
            "LX/BHw;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1769790
    const-class v0, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;

    sput-object v0, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->p:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1769789
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a(LX/BGe;LX/11i;LX/0id;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1769785
    iput-object p1, p0, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->s:LX/BGe;

    .line 1769786
    iput-object p2, p0, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->u:LX/11i;

    .line 1769787
    iput-object p3, p0, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->v:LX/0id;

    .line 1769788
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;

    invoke-static {v2}, LX/BGe;->b(LX/0QB;)LX/BGe;

    move-result-object v0

    check-cast v0, LX/BGe;

    invoke-static {v2}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v1

    check-cast v1, LX/11i;

    invoke-static {v2}, LX/0id;->a(LX/0QB;)LX/0id;

    move-result-object v2

    check-cast v2, LX/0id;

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->a(LX/BGe;LX/11i;LX/0id;)V

    return-void
.end method

.method private b()Lcom/facebook/photos/simplepicker/SimplePickerFragment;
    .locals 3

    .prologue
    .line 1769778
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->w:LX/11o;

    const-string v1, "LaunchFragmentAndDi"

    const v2, 0x71f7bec4

    invoke-static {v0, v1, v2}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1769779
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d002f

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    .line 1769780
    if-eqz v0, :cond_0

    .line 1769781
    :goto_0
    return-object v0

    .line 1769782
    :cond_0
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->r:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    iget-object v2, p0, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->t:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->a(Landroid/os/Bundle;Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;Ljava/lang/String;)Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    move-result-object v0

    .line 1769783
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    invoke-virtual {v1, v2, v0}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v1

    invoke-virtual {v1}, LX/0hH;->b()I

    .line 1769784
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->b()Z

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1769703
    const-string v0, "simple_picker"

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 1769751
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 1769752
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1769753
    invoke-static {p0, p0}, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1769754
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->u:LX/11i;

    sget-object v1, LX/BHx;->a:LX/BHw;

    invoke-interface {v0, v1}, LX/11i;->a(LX/0Pq;)LX/11o;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->w:LX/11o;

    .line 1769755
    const v0, 0x7f031333

    invoke-virtual {p0, v0}, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->setContentView(I)V

    .line 1769756
    invoke-virtual {p0}, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 1769757
    const-string v0, "extra_simple_picker_launcher_settings"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->r:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1769758
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->r:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1769759
    if-eqz p1, :cond_0

    .line 1769760
    const-string v0, "extra_simple_picker_launcher_waterfall_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, p0

    .line 1769761
    :goto_0
    iput-object v0, v1, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->t:Ljava/lang/String;

    .line 1769762
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->s:LX/BGe;

    iget-object v1, p0, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->t:Ljava/lang/String;

    .line 1769763
    iput-object v1, v0, LX/BGe;->a:Ljava/lang/String;

    .line 1769764
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->r:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1769765
    iget-object v1, v0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-object v0, v1

    .line 1769766
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->r:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1769767
    iget-object v1, v0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-object v0, v1

    .line 1769768
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    iget-wide v0, v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    .line 1769769
    :goto_1
    iget-object v4, p0, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->s:LX/BGe;

    iget-object v5, p0, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->r:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1769770
    iget-object p1, v5, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    move-object v5, p1

    .line 1769771
    iget-object p1, v5, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->e:LX/8AB;

    move-object v5, p1

    .line 1769772
    sget-object v6, LX/BGd;->LAUNCHED:LX/BGd;

    invoke-static {v6}, LX/BGe;->a(LX/BGd;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "source"

    invoke-virtual {v5}, LX/8AB;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "target_id"

    invoke-virtual {v6, v7, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    invoke-static {v4, v6}, LX/BGe;->a(LX/BGe;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1769773
    invoke-direct {p0}, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->b()Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->q:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    .line 1769774
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->v:LX/0id;

    sget-object v1, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->p:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v3}, LX/0id;->b(Ljava/lang/String;J)V

    .line 1769775
    return-void

    .line 1769776
    :cond_0
    const-string v0, "extra_simple_picker_launcher_waterfall_id"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "extra_simple_picker_launcher_waterfall_id"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, p0

    goto :goto_0

    :cond_1
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, p0

    goto :goto_0

    .line 1769777
    :cond_2
    const-wide/16 v0, -0x1

    goto :goto_1
.end method

.method public final onBackPressed()V
    .locals 4

    .prologue
    .line 1769723
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->q:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    .line 1769724
    iget-boolean v1, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->o:Z

    if-eqz v1, :cond_2

    .line 1769725
    invoke-static {v0}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->B(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V

    .line 1769726
    const/4 v1, 0x1

    .line 1769727
    :goto_0
    move v0, v1

    .line 1769728
    if-eqz v0, :cond_0

    .line 1769729
    :goto_1
    return-void

    .line 1769730
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->r:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1769731
    iget-boolean v1, v0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->e:Z

    move v0, v1

    .line 1769732
    if-eqz v0, :cond_1

    .line 1769733
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->q:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    invoke-virtual {v0}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->e()V

    goto :goto_1

    .line 1769734
    :cond_1
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 1769735
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->s:LX/BGe;

    .line 1769736
    sget-object v1, LX/BGd;->CANCELLED:LX/BGd;

    invoke-static {v1}, LX/BGe;->a(LX/BGd;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-static {v0, v1}, LX/BGe;->a(LX/BGe;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1769737
    goto :goto_1

    .line 1769738
    :cond_2
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    const-string v2, "GALLERY_FRAGMENT"

    invoke-virtual {v1, v2}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 1769739
    if-nez v1, :cond_4

    .line 1769740
    iget-object v1, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1769741
    iget-object v2, v1, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->b:LX/8A9;

    move-object v1, v2

    .line 1769742
    sget-object v2, LX/8A9;->LAUNCH_COMPOSER:LX/8A9;

    if-ne v1, v2, :cond_3

    .line 1769743
    iget-object v1, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1769744
    iget-object v2, v1, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-object v1, v2

    .line 1769745
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1769746
    iget-object v1, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->as:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0gd;

    iget-object v2, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->W:Ljava/lang/String;

    iget-object v3, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1769747
    iget-object v0, v3, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-object v3, v0

    .line 1769748
    invoke-virtual {v1, v2, v3}, LX/0gd;->b(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)V

    .line 1769749
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 1769750
    :cond_4
    check-cast v1, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-virtual {v1}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->b()Z

    move-result v1

    goto :goto_0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x79312569

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1769719
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 1769720
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->u:LX/11i;

    sget-object v2, LX/BHx;->a:LX/BHw;

    invoke-interface {v1, v2}, LX/11i;->d(LX/0Pq;)V

    .line 1769721
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->v:LX/0id;

    invoke-virtual {v1}, LX/0id;->a()V

    .line 1769722
    const/16 v1, 0x23

    const v2, -0x405239c7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0xc715b20

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1769715
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->v:LX/0id;

    sget-object v2, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->p:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0id;->e(Ljava/lang/String;)V

    .line 1769716
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 1769717
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->v:LX/0id;

    sget-object v2, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->p:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0id;->f(Ljava/lang/String;)V

    .line 1769718
    const/16 v1, 0x23

    const v2, -0x61fdf4c9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1769712
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1769713
    const-string v0, "extra_simple_picker_launcher_waterfall_id"

    iget-object v1, p0, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->t:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1769714
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x6f48cde

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1769708
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->v:LX/0id;

    sget-object v2, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->p:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0id;->c(Ljava/lang/String;)V

    .line 1769709
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStart()V

    .line 1769710
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->v:LX/0id;

    sget-object v2, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->p:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0id;->d(Ljava/lang/String;)V

    .line 1769711
    const/16 v1, 0x23

    const v2, -0x113e9cd2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onWindowFocusChanged(Z)V
    .locals 1

    .prologue
    .line 1769704
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onWindowFocusChanged(Z)V

    .line 1769705
    if-eqz p1, :cond_0

    .line 1769706
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;->v:LX/0id;

    invoke-virtual {v0, p0}, LX/0id;->a(Landroid/app/Activity;)V

    .line 1769707
    :cond_0
    return-void
.end method
