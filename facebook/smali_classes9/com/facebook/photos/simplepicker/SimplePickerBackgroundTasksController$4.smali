.class public final Lcom/facebook/photos/simplepicker/SimplePickerBackgroundTasksController$4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/BGb;


# direct methods
.method public constructor <init>(LX/BGb;)V
    .locals 0

    .prologue
    .line 1767269
    iput-object p1, p0, Lcom/facebook/photos/simplepicker/SimplePickerBackgroundTasksController$4;->a:LX/BGb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    .prologue
    .line 1767270
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/SimplePickerBackgroundTasksController$4;->a:LX/BGb;

    iget-object v0, v0, LX/BGb;->m:LX/BGa;

    .line 1767271
    iget-object v1, v0, LX/BGa;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/BGa;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1767272
    :cond_0
    return-void

    .line 1767273
    :cond_1
    iget-object v1, v0, LX/BGa;->c:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v3

    .line 1767274
    iget-object v1, v0, LX/BGa;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v5, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v5, :cond_0

    aget-object v6, v2, v1

    .line 1767275
    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, "motion_photo_"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v6}, Ljava/io/File;->lastModified()J

    move-result-wide v7

    sub-long v7, v3, v7

    const-wide/32 v9, 0xa4cb800

    cmp-long v7, v7, v9

    if-lez v7, :cond_2

    .line 1767276
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    move-result v6

    if-nez v6, :cond_2

    .line 1767277
    sget-object v6, LX/BGa;->a:Ljava/lang/String;

    const-string v7, "Couldn\'t delete motion photo video"

    invoke-static {v6, v7}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1767278
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
