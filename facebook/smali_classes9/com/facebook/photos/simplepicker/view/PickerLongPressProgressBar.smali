.class public Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;
.super Landroid/view/View;
.source ""


# static fields
.field private static final a:LX/0wT;


# instance fields
.field public b:LX/0wW;

.field private c:LX/0wd;

.field private d:Landroid/graphics/Paint;

.field public e:Lcom/facebook/ipc/media/MediaItem;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1770421
    const-wide/high16 v0, 0x403e000000000000L    # 30.0

    const-wide/high16 v2, 0x4020000000000000L    # 8.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;->a:LX/0wT;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1770418
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1770419
    invoke-direct {p0}, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;->b()V

    .line 1770420
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1770415
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1770416
    invoke-direct {p0}, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;->b()V

    .line 1770417
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;

    invoke-static {v0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v0

    check-cast v0, LX/0wW;

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;->b:LX/0wW;

    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    .line 1770405
    const-class v0, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;

    invoke-static {v0, p0}, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1770406
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;->d:Landroid/graphics/Paint;

    .line 1770407
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;->d:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a04e1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1770408
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1770409
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;->b:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v1, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;->a:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    const/4 v1, 0x1

    .line 1770410
    iput-boolean v1, v0, LX/0wd;->c:Z

    .line 1770411
    move-object v0, v0

    .line 1770412
    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;->c:LX/0wd;

    .line 1770413
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;->c:LX/0wd;

    new-instance v1, LX/BIQ;

    invoke-direct {v1, p0}, LX/BIQ;-><init>(Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 1770414
    return-void
.end method


# virtual methods
.method public final a(LX/0xi;)V
    .locals 1

    .prologue
    .line 1770403
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;->c:LX/0wd;

    invoke-virtual {v0, p1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 1770404
    return-void
.end method

.method public final a()Z
    .locals 4

    .prologue
    .line 1770402
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;->c:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->d()D

    move-result-wide v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/ipc/media/MediaItem;)Z
    .locals 4
    .param p1    # Lcom/facebook/ipc/media/MediaItem;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1770396
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;->e:Lcom/facebook/ipc/media/MediaItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;->e:Lcom/facebook/ipc/media/MediaItem;

    if-eq v0, p1, :cond_0

    .line 1770397
    const/4 v0, 0x0

    .line 1770398
    :goto_0
    return v0

    .line 1770399
    :cond_0
    iput-object p1, p0, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;->e:Lcom/facebook/ipc/media/MediaItem;

    .line 1770400
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;->c:LX/0wd;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1770401
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Lcom/facebook/ipc/media/MediaItem;)Z
    .locals 4
    .param p1    # Lcom/facebook/ipc/media/MediaItem;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1770390
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;->e:Lcom/facebook/ipc/media/MediaItem;

    if-eq v0, p1, :cond_0

    .line 1770391
    const/4 v0, 0x0

    .line 1770392
    :goto_0
    return v0

    .line 1770393
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;->e:Lcom/facebook/ipc/media/MediaItem;

    .line 1770394
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;->c:LX/0wd;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1770395
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getMediaItem()Lcom/facebook/ipc/media/MediaItem;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1770389
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;->e:Lcom/facebook/ipc/media/MediaItem;

    return-object v0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1770383
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1770384
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;->c:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v0, v0

    .line 1770385
    cmpl-float v1, v0, v2

    if-lez v1, :cond_0

    .line 1770386
    invoke-virtual {p0}, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;->getWidth()I

    move-result v1

    int-to-float v1, v1

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float v0, v3, v0

    mul-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v0, v1

    .line 1770387
    invoke-virtual {p0}, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;->getWidth()I

    move-result v0

    int-to-float v0, v0

    sub-float v3, v0, v1

    invoke-virtual {p0}, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;->getHeight()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;->d:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1770388
    :cond_0
    return-void
.end method
