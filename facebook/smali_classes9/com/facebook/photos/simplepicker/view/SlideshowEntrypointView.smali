.class public Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:LX/1Uo;

.field private final d:LX/4Ac;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4Ac",
            "<",
            "LX/1af;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/fbui/facepile/FacepileGridView;

.field private final f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final g:Landroid/view/View;

.field private final h:Landroid/view/View;

.field private final i:Landroid/widget/TextView;

.field private final j:Landroid/widget/TextView;

.field private final k:Landroid/widget/RadioButton;

.field private final l:Landroid/widget/RadioButton;

.field public final m:Landroid/os/Handler;

.field private final n:LX/1o9;

.field private o:LX/BH0;

.field private p:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private q:Ljava/util/Timer;

.field public r:LX/1ap;

.field private s:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1770548
    const-class v0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1770546
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1770547
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1770544
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1770545
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x1

    .line 1770522
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1770523
    const-class v0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;

    invoke-static {v0, p0}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1770524
    new-instance v0, LX/1Uo;

    invoke-virtual {p0}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->c:LX/1Uo;

    .line 1770525
    new-instance v0, LX/4Ac;

    invoke-direct {v0}, LX/4Ac;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->d:LX/4Ac;

    .line 1770526
    const v0, 0x7f031347

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1770527
    const v0, 0x7f0d2ca8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileGridView;

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->e:Lcom/facebook/fbui/facepile/FacepileGridView;

    .line 1770528
    const v0, 0x7f0d2cac

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1770529
    const v0, 0x7f0d2ca7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->g:Landroid/view/View;

    .line 1770530
    const v0, 0x7f0d2cab

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->h:Landroid/view/View;

    .line 1770531
    const v0, 0x7f0d2ca9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->i:Landroid/widget/TextView;

    .line 1770532
    const v0, 0x7f0d2cad

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->j:Landroid/widget/TextView;

    .line 1770533
    const v0, 0x7f0d2caa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->k:Landroid/widget/RadioButton;

    .line 1770534
    const v0, 0x7f0d2cae

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->l:Landroid/widget/RadioButton;

    .line 1770535
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->k:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setClickable(Z)V

    .line 1770536
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->l:Landroid/widget/RadioButton;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setClickable(Z)V

    .line 1770537
    invoke-virtual {p0}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1770538
    const v1, 0x7f0b0bd4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    .line 1770539
    iget-object v2, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->e:Lcom/facebook/fbui/facepile/FacepileGridView;

    new-instance v3, LX/6UY;

    new-instance v4, LX/4AW;

    const v5, 0x7f0a00a3

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-direct {v4, v1, v5}, LX/4AW;-><init>(FI)V

    const/4 v5, 0x2

    invoke-direct {v3, v4, v8, v5}, LX/6UY;-><init>(Landroid/graphics/drawable/Drawable;II)V

    new-instance v4, LX/6UY;

    new-instance v5, LX/4AW;

    const v6, 0x7f0a00a1

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-direct {v5, v1, v6}, LX/4AW;-><init>(FI)V

    invoke-direct {v4, v5, v8, v8}, LX/6UY;-><init>(Landroid/graphics/drawable/Drawable;II)V

    new-instance v5, LX/6UY;

    new-instance v6, LX/4AW;

    const v7, 0x7f0a00a7

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-direct {v6, v1, v7}, LX/4AW;-><init>(FI)V

    invoke-direct {v5, v6, v8, v8}, LX/6UY;-><init>(Landroid/graphics/drawable/Drawable;II)V

    invoke-static {v3, v4, v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/facepile/FacepileGridView;->setFaces(Ljava/util/List;)V

    .line 1770540
    const v1, 0x7f0b0bd6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 1770541
    new-instance v1, LX/1o9;

    invoke-direct {v1, v0, v0}, LX/1o9;-><init>(II)V

    iput-object v1, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->n:LX/1o9;

    .line 1770542
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->m:Landroid/os/Handler;

    .line 1770543
    return-void
.end method

.method private a(LX/4Ab;)LX/1aX;
    .locals 2

    .prologue
    .line 1770518
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->c:LX/1Uo;

    .line 1770519
    iput-object p1, v0, LX/1Uo;->u:LX/4Ab;

    .line 1770520
    move-object v0, v0

    .line 1770521
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/1aX;Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 1770510
    invoke-static {p2}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->n:LX/1o9;

    .line 1770511
    iput-object v1, v0, LX/1bX;->c:LX/1o9;

    .line 1770512
    move-object v0, v0

    .line 1770513
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    .line 1770514
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->a:LX/1Ad;

    .line 1770515
    iget-object v2, p1, LX/1aX;->f:LX/1aZ;

    move-object v2, v2

    .line 1770516
    invoke-virtual {v0, v2}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, v1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/1aX;->a(LX/1aZ;)V

    .line 1770517
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v0

    check-cast v0, LX/1Ad;

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->a:LX/1Ad;

    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    .line 1770434
    invoke-virtual {p0}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00a8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 1770435
    invoke-virtual {p0}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a00d2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 1770436
    iget-object v3, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->i:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1770437
    iget-object v3, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->k:Landroid/widget/RadioButton;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 1770438
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->j:Landroid/widget/TextView;

    if-eqz p1, :cond_2

    :goto_2
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1770439
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->l:Landroid/widget/RadioButton;

    invoke-virtual {v0, p1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 1770440
    return-void

    :cond_0
    move v0, v2

    .line 1770441
    goto :goto_0

    .line 1770442
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    move v2, v1

    .line 1770443
    goto :goto_2
.end method

.method private b()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 1770496
    invoke-virtual {p0}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1770497
    const v1, 0x7f0b0bd4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    .line 1770498
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->a:LX/1Ad;

    sget-object v2, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    .line 1770499
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->c:LX/1Uo;

    sget-object v2, LX/1Up;->g:LX/1Up;

    invoke-virtual {v1, v2}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    .line 1770500
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->d:LX/4Ac;

    invoke-virtual {v1}, LX/4Ac;->c()V

    .line 1770501
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->d:LX/4Ac;

    invoke-static {v0, v3, v3, v0}, LX/4Ab;->b(FFFF)LX/4Ab;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->a(LX/4Ab;)LX/1aX;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4Ac;->a(LX/1aX;)V

    .line 1770502
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->d:LX/4Ac;

    invoke-static {v3, v0, v3, v3}, LX/4Ab;->b(FFFF)LX/4Ab;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->a(LX/4Ab;)LX/1aX;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4Ac;->a(LX/1aX;)V

    .line 1770503
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->d:LX/4Ac;

    invoke-static {v3, v3, v0, v3}, LX/4Ab;->b(FFFF)LX/4Ab;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->a(LX/4Ab;)LX/1aX;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4Ac;->a(LX/1aX;)V

    .line 1770504
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->d:LX/4Ac;

    invoke-static {v0}, LX/4Ab;->b(F)LX/4Ab;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->a(LX/4Ab;)LX/1aX;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4Ac;->a(LX/1aX;)V

    .line 1770505
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->d:LX/4Ac;

    invoke-static {v0}, LX/4Ab;->b(F)LX/4Ab;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->a(LX/4Ab;)LX/1aX;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4Ac;->a(LX/1aX;)V

    .line 1770506
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->d:LX/4Ac;

    invoke-static {v0}, LX/4Ab;->b(F)LX/4Ab;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->a(LX/4Ab;)LX/1aX;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/4Ac;->a(LX/1aX;)V

    .line 1770507
    new-instance v0, LX/1ap;

    new-array v1, v4, [Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->d:LX/4Ac;

    invoke-virtual {v3, v4}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v3

    invoke-virtual {v3}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->d:LX/4Ac;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v3

    invoke-virtual {v3}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->d:LX/4Ac;

    const/4 v4, 0x5

    invoke-virtual {v3, v4}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v3

    invoke-virtual {v3}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, LX/1ap;-><init>([Landroid/graphics/drawable/Drawable;)V

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->r:LX/1ap;

    .line 1770508
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->r:LX/1ap;

    const/16 v1, 0x1f4

    invoke-virtual {v0, v1}, LX/1ap;->c(I)V

    .line 1770509
    return-void
.end method

.method public static synthetic c(Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;)I
    .locals 1

    .prologue
    .line 1770495
    iget v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->s:I

    return v0
.end method

.method public static c(Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1770490
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->p:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->o:LX/BH0;

    if-nez v0, :cond_1

    .line 1770491
    :cond_0
    :goto_0
    return-void

    .line 1770492
    :cond_1
    invoke-direct {p0, v1}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->a(Z)V

    .line 1770493
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->o:LX/BH0;

    invoke-interface {v0, v1}, LX/BH0;->a(Z)V

    .line 1770494
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->p:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0
.end method

.method public static d(Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1770485
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->p:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->o:LX/BH0;

    if-nez v0, :cond_1

    .line 1770486
    :cond_0
    :goto_0
    return-void

    .line 1770487
    :cond_1
    invoke-direct {p0, v1}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->a(Z)V

    .line 1770488
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->o:LX/BH0;

    invoke-interface {v0, v1}, LX/BH0;->a(Z)V

    .line 1770489
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->p:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 1770465
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->p:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->a(Z)V

    .line 1770466
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->g:Landroid/view/View;

    new-instance v1, LX/BIR;

    invoke-direct {v1, p0}, LX/BIR;-><init>(Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1770467
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->h:Landroid/view/View;

    new-instance v1, LX/BIS;

    invoke-direct {v1, p0}, LX/BIS;-><init>(Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1770468
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->d:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->d()I

    move-result v0

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    .line 1770469
    invoke-direct {p0}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->b()V

    .line 1770470
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->d:LX/4Ac;

    invoke-virtual {v0, v3}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->a(LX/1aX;Landroid/net/Uri;)V

    .line 1770471
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->d:LX/4Ac;

    invoke-virtual {v0, v5}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->a(LX/1aX;Landroid/net/Uri;)V

    .line 1770472
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->d:LX/4Ac;

    invoke-virtual {v0, v6}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->a(LX/1aX;Landroid/net/Uri;)V

    .line 1770473
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->e:Lcom/facebook/fbui/facepile/FacepileGridView;

    new-instance v1, LX/6UY;

    iget-object v2, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->d:LX/4Ac;

    invoke-virtual {v2, v3}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v2

    invoke-virtual {v2}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-direct {v1, v2, v5, v6}, LX/6UY;-><init>(Landroid/graphics/drawable/Drawable;II)V

    new-instance v2, LX/6UY;

    iget-object v3, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->d:LX/4Ac;

    invoke-virtual {v3, v5}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v3

    invoke-virtual {v3}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-direct {v2, v3, v5, v5}, LX/6UY;-><init>(Landroid/graphics/drawable/Drawable;II)V

    new-instance v3, LX/6UY;

    iget-object v4, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->d:LX/4Ac;

    invoke-virtual {v4, v6}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v4

    invoke-virtual {v4}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-direct {v3, v4, v5, v5}, LX/6UY;-><init>(Landroid/graphics/drawable/Drawable;II)V

    invoke-static {v1, v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/facepile/FacepileGridView;->setFaces(Ljava/util/List;)V

    .line 1770474
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->d:LX/4Ac;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->a(LX/1aX;Landroid/net/Uri;)V

    .line 1770475
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->d:LX/4Ac;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->a(LX/1aX;Landroid/net/Uri;)V

    .line 1770476
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->d:LX/4Ac;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->a(LX/1aX;Landroid/net/Uri;)V

    .line 1770477
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->r:LX/1ap;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1770478
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->q:Ljava/util/Timer;

    if-eqz v0, :cond_1

    .line 1770479
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->q:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 1770480
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->q:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 1770481
    :cond_1
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->q:Ljava/util/Timer;

    .line 1770482
    iput v6, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->s:I

    .line 1770483
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->q:Ljava/util/Timer;

    new-instance v1, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView$3;

    invoke-direct {v1, p0}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView$3;-><init>(Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x5dc

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 1770484
    return-void
.end method

.method public final a(ZLX/BH0;)V
    .locals 2

    .prologue
    .line 1770461
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->i:Landroid/widget/TextView;

    const v1, 0x7f08137b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1770462
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->p:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1770463
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BH0;

    iput-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->o:LX/BH0;

    .line 1770464
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1770460
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->p:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x3d667bf6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1770457
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onAttachedToWindow()V

    .line 1770458
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->d:LX/4Ac;

    invoke-virtual {v1}, LX/4Ac;->a()V

    .line 1770459
    const/16 v1, 0x2d

    const v2, 0x74fdbeab

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x530d5b21

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1770450
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onDetachedFromWindow()V

    .line 1770451
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->d:LX/4Ac;

    invoke-virtual {v1}, LX/4Ac;->b()V

    .line 1770452
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->q:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 1770453
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->q:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 1770454
    iget-object v1, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->q:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->purge()I

    .line 1770455
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->q:Ljava/util/Timer;

    .line 1770456
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x5275b2a4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 1770447
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onFinishTemporaryDetach()V

    .line 1770448
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->d:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->a()V

    .line 1770449
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 1770444
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onStartTemporaryDetach()V

    .line 1770445
    iget-object v0, p0, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->d:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->b()V

    .line 1770446
    return-void
.end method
