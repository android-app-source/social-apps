.class public Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/BGQ;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:LX/1Ad;

.field private final c:LX/8GN;

.field public final d:I

.field public final e:LX/BMK;

.field public final f:LX/BMP;

.field public final g:LX/1RN;

.field private final h:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

.field private final i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/B5l;

.field public final k:LX/1E1;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1767079
    const-class v0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;

    const-class v1, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;LX/1RN;LX/1Ad;LX/8GN;LX/BMK;LX/BMP;LX/B5l;LX/1E1;)V
    .locals 4
    .param p1    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1RN;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1767042
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1767043
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;->d:I

    .line 1767044
    iput-object p2, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;->g:LX/1RN;

    .line 1767045
    iput-object p3, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;->b:LX/1Ad;

    .line 1767046
    iput-object p4, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;->c:LX/8GN;

    .line 1767047
    iput-object p5, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;->e:LX/BMK;

    .line 1767048
    iput-object p6, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;->f:LX/BMP;

    .line 1767049
    iput-object p7, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;->j:LX/B5l;

    .line 1767050
    iput-object p8, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;->k:LX/1E1;

    .line 1767051
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;->g:LX/1RN;

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    check-cast v0, LX/1kW;

    .line 1767052
    iget-object v1, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, v1

    .line 1767053
    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->o()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;->h:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    .line 1767054
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;->c:LX/8GN;

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;->h:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    iget v2, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;->d:I

    iget v3, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;->d:I

    invoke-virtual {v0, v1, v2, v3}, LX/8GN;->a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;II)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;->i:LX/0Px;

    .line 1767055
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 1767075
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0306c7

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1767076
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v2, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;->d:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1767077
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v2, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;->d:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1767078
    new-instance v1, LX/BGQ;

    invoke-direct {v1, p0, v0}, LX/BGQ;-><init>(Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 7

    .prologue
    .line 1767057
    check-cast p1, LX/BGQ;

    .line 1767058
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;->i:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1767059
    iput-object v0, p1, LX/BGQ;->o:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1767060
    new-instance v1, LX/5iG;

    const/4 v2, 0x0

    const-string v3, ""

    const-string v4, ""

    invoke-direct {v1, v2, v3, v4}, LX/5iG;-><init>(LX/1aX;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p1, LX/BGQ;->n:LX/5iG;

    .line 1767061
    iget-object v1, p1, LX/BGQ;->o:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/creativeediting/model/StickerParams;

    .line 1767062
    iget-object v5, p1, LX/BGQ;->n:LX/5iG;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/StickerParams;->d()Landroid/net/Uri;

    move-result-object v6

    .line 1767063
    new-instance p0, LX/1Uo;

    iget-object p2, p1, LX/BGQ;->m:Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;

    invoke-virtual {p2}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-direct {p0, p2}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    sget-object p2, LX/1Up;->c:LX/1Up;

    invoke-virtual {p0, p2}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object p0

    const/4 p2, 0x0

    .line 1767064
    iput p2, p0, LX/1Uo;->d:I

    .line 1767065
    move-object p2, p0

    .line 1767066
    iget-object p0, p1, LX/BGQ;->l:Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;

    iget-object p0, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;->b:LX/1Ad;

    invoke-virtual {p0}, LX/1Ad;->o()LX/1Ad;

    .line 1767067
    iget-object p0, p1, LX/BGQ;->l:Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;

    iget-object p0, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;->b:LX/1Ad;

    invoke-virtual {p0, v6}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object p0

    sget-object v0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p0, v0}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object p0

    new-instance v0, LX/BGP;

    invoke-direct {v0, p1}, LX/BGP;-><init>(LX/BGQ;)V

    invoke-virtual {p0, v0}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object p0

    check-cast p0, LX/1Ad;

    invoke-virtual {p0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object p0

    .line 1767068
    invoke-virtual {p2}, LX/1Uo;->u()LX/1af;

    move-result-object p2

    iget-object v0, p1, LX/BGQ;->m:Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p2, v0}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object p2

    .line 1767069
    invoke-virtual {p2, p0}, LX/1aX;->a(LX/1aZ;)V

    .line 1767070
    move-object v6, p2

    .line 1767071
    invoke-virtual {v5, v1, v6}, LX/5iG;->a(Lcom/facebook/photos/creativeediting/model/StickerParams;LX/1aX;)V

    .line 1767072
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1767073
    :cond_0
    iget-object v1, p1, LX/BGQ;->m:Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;

    iget-object v2, p1, LX/BGQ;->n:LX/5iG;

    invoke-virtual {v1, v2}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;->setSwipeableItem(LX/5iG;)V

    .line 1767074
    return-void
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1767056
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;->h:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
