.class public final Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final synthetic e:Ljava/util/concurrent/atomic/AtomicInteger;

.field public final synthetic f:LX/Ave;

.field public final synthetic g:Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/atomic/AtomicBoolean;Ljava/util/concurrent/atomic/AtomicInteger;LX/Ave;)V
    .locals 0

    .prologue
    .line 1766868
    iput-object p1, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader$2;->g:Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;

    iput-object p2, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader$2;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader$2;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader$2;->c:Ljava/lang/String;

    iput-object p5, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader$2;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p6, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader$2;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    iput-object p7, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader$2;->f:LX/Ave;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 1766869
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader$2;->g:Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader$2;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader$2;->b:Ljava/lang/String;

    .line 1766870
    invoke-static {v2}, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->e(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-static {v0, v3}, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->f(Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    .line 1766871
    :goto_0
    if-eqz v4, :cond_4

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    move v4, v0

    .line 1766872
    move v0, v4

    .line 1766873
    if-nez v0, :cond_0

    .line 1766874
    const/4 v2, 0x0

    .line 1766875
    :try_start_0
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader$2;->g:Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;

    iget-object v3, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader$2;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader$2;->a:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader$2;->b:Ljava/lang/String;

    invoke-static {v0, v3, v4, v5}, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->a$redex0(Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1766876
    :goto_2
    if-nez v0, :cond_0

    .line 1766877
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader$2;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1766878
    sget-object v0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->a:Ljava/lang/String;

    const-string v2, "Downloaded file is null"

    invoke-static {v0, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1766879
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader$2;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_1

    .line 1766880
    iget-object v2, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader$2;->f:LX/Ave;

    iget-object v0, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader$2;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_3
    invoke-virtual {v2, v0}, LX/Ave;->a(Z)V

    .line 1766881
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader$2;->g:Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;

    iget-object v0, v0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1766882
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader$2;->g:Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;

    invoke-static {v0}, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->b$redex0(Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;)V

    .line 1766883
    return-void

    .line 1766884
    :catch_0
    move-exception v0

    .line 1766885
    iget-object v3, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader$2;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v3, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1766886
    sget-object v3, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->a:Ljava/lang/String;

    const-string v4, "Exception"

    invoke-static {v3, v4, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v2

    goto :goto_2

    .line 1766887
    :cond_2
    const/4 v0, 0x0

    goto :goto_3

    .line 1766888
    :cond_3
    invoke-static {v0, v2}, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->f(Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method
