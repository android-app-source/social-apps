.class public Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile i:Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;


# instance fields
.field private final b:Lcom/facebook/common/callercontext/CallerContext;

.field private final c:LX/3AP;

.field private final d:Ljava/util/concurrent/ExecutorService;

.field public final e:Ljava/util/concurrent/ExecutorService;

.field public final f:LX/AwR;

.field public final g:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final h:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1766950
    const-class v0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/1Gm;LX/0Zb;LX/1Gk;LX/1Gl;LX/1Go;LX/13t;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;LX/AwR;)V
    .locals 11
    .param p9    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p10    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .param p11    # LX/AwR;
        .annotation runtime Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdCache;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1766941
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1766942
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "asset_download"

    invoke-static {v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 1766943
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v1, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1766944
    new-instance v1, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v1, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->h:Ljava/util/Queue;

    .line 1766945
    new-instance v1, LX/3AP;

    const-string v4, "msqrd"

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    move-object v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    invoke-direct/range {v1 .. v10}, LX/3AP;-><init>(Landroid/content/Context;Lcom/facebook/http/common/FbHttpRequestProcessor;Ljava/lang/String;LX/1Gm;LX/0Zb;LX/1Gk;LX/1Gl;LX/1Go;LX/13t;)V

    iput-object v1, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->c:LX/3AP;

    .line 1766946
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->d:Ljava/util/concurrent/ExecutorService;

    .line 1766947
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->e:Ljava/util/concurrent/ExecutorService;

    .line 1766948
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->f:LX/AwR;

    .line 1766949
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;
    .locals 15

    .prologue
    .line 1766928
    sget-object v0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->i:Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;

    if-nez v0, :cond_1

    .line 1766929
    const-class v1, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;

    monitor-enter v1

    .line 1766930
    :try_start_0
    sget-object v0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->i:Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1766931
    if-eqz v2, :cond_0

    .line 1766932
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1766933
    new-instance v3, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v5

    check-cast v5, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-static {v0}, LX/1Gm;->a(LX/0QB;)LX/1Gm;

    move-result-object v6

    check-cast v6, LX/1Gm;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    invoke-static {v0}, LX/1Gk;->a(LX/0QB;)LX/1Gk;

    move-result-object v8

    check-cast v8, LX/1Gk;

    invoke-static {v0}, LX/1Gl;->a(LX/0QB;)LX/1Gl;

    move-result-object v9

    check-cast v9, LX/1Gl;

    invoke-static {v0}, LX/1Gn;->a(LX/0QB;)LX/1Gn;

    move-result-object v10

    check-cast v10, LX/1Go;

    invoke-static {v0}, LX/13t;->a(LX/0QB;)LX/13t;

    move-result-object v11

    check-cast v11, LX/13t;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v12

    check-cast v12, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v13

    check-cast v13, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/BGF;->a(LX/0QB;)LX/AwR;

    move-result-object v14

    check-cast v14, LX/AwR;

    invoke-direct/range {v3 .. v14}, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;-><init>(Landroid/content/Context;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/1Gm;LX/0Zb;LX/1Gk;LX/1Gl;LX/1Go;LX/13t;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;LX/AwR;)V

    .line 1766934
    move-object v0, v3

    .line 1766935
    sput-object v0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->i:Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1766936
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1766937
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1766938
    :cond_1
    sget-object v0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->i:Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;

    return-object v0

    .line 1766939
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1766940
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;LX/Ave;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/atomic/AtomicInteger;Ljava/util/concurrent/atomic/AtomicBoolean;)Ljava/lang/Runnable;
    .locals 8

    .prologue
    .line 1766896
    new-instance v0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader$2;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p4

    move-object v4, p2

    move-object v5, p6

    move-object v6, p5

    move-object v7, p1

    invoke-direct/range {v0 .. v7}, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader$2;-><init>(Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/atomic/AtomicBoolean;Ljava/util/concurrent/atomic/AtomicInteger;LX/Ave;)V

    return-object v0
.end method

.method public static a$redex0(Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1766910
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->f:LX/AwR;

    invoke-virtual {v1}, LX/AwR;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".tmp"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 1766911
    :try_start_0
    invoke-static {v1}, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->d(Ljava/lang/String;)V

    .line 1766912
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    .line 1766913
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->c:LX/3AP;

    new-instance v2, LX/34X;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    new-instance v4, LX/BGJ;

    invoke-direct {v4, v1}, LX/BGJ;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v2, v3, v4, v5}, LX/34X;-><init>(Landroid/net/Uri;LX/1uy;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-virtual {v0, v2}, LX/3AP;->a(LX/34X;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 1766914
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    .line 1766915
    invoke-static {p2}, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->e(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1766916
    iget-object v2, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->f:LX/AwR;

    new-instance v3, LX/BGG;

    invoke-direct {v3, p0, v0}, LX/BGG;-><init>(Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;Ljava/io/File;)V

    .line 1766917
    iget-object v0, v2, LX/AwR;->a:Lcom/facebook/compactdisk/DiskCache;

    invoke-virtual {v0, p3, v3}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->storeManual(Ljava/lang/String;Lcom/facebook/compactdisk/ManualWrite;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v2, v0

    .line 1766918
    const v3, 0x17585220

    invoke-static {v2, v3}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    .line 1766919
    invoke-static {p0, p3}, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->f(Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    move-object v0, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1766920
    invoke-static {v1}, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->d(Ljava/lang/String;)V

    .line 1766921
    :goto_0
    return-object v0

    .line 1766922
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->f:LX/AwR;

    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v2, p2, v3}, LX/AwR;->a(Ljava/lang/String;Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v2

    .line 1766923
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1766924
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1766925
    invoke-static {v1}, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->d(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-static {v1}, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->d(Ljava/lang/String;)V

    .line 1766926
    const/4 v0, 0x0

    goto :goto_0

    .line 1766927
    :catchall_0
    move-exception v0

    invoke-static {v1}, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->d(Ljava/lang/String;)V

    throw v0
.end method

.method public static b$redex0(Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;)V
    .locals 3

    .prologue
    .line 1766906
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->h:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1766907
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->h:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 1766908
    iget-object v1, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->d:Ljava/util/concurrent/ExecutorService;

    const v2, 0x1eca4bc2

    invoke-static {v1, v0, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1766909
    :cond_0
    return-void
.end method

.method public static d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1766902
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1766903
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1766904
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1766905
    :cond_0
    return-void
.end method

.method public static e(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 1766901
    invoke-static {p0}, LX/1t3;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "msqrd"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static f(Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;Ljava/lang/String;)Ljava/io/File;
    .locals 2

    .prologue
    .line 1766897
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->f:LX/AwR;

    invoke-virtual {v0, p1}, LX/AwR;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1766898
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1766899
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1766900
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
