.class public final Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/Ave;

.field public final synthetic b:LX/BGE;

.field public final synthetic c:Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;LX/Ave;LX/BGE;)V
    .locals 0

    .prologue
    .line 1766850
    iput-object p1, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader$1;->c:Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;

    iput-object p2, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader$1;->a:LX/Ave;

    iput-object p3, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader$1;->b:LX/BGE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 14

    .prologue
    .line 1766851
    iget-object v0, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader$1;->c:Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;

    iget-object v1, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader$1;->a:LX/Ave;

    iget-object v2, p0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader$1;->b:LX/BGE;

    const/4 v4, 0x0

    .line 1766852
    new-instance v9, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v9, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 1766853
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 1766854
    iget-object v3, v2, LX/BGE;->a:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    move-object v3, v3

    .line 1766855
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1766856
    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1766857
    :cond_0
    new-instance v8, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-direct {v8, v3}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 1766858
    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v3

    if-nez v3, :cond_1

    .line 1766859
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, LX/Ave;->a(Z)V

    .line 1766860
    :goto_1
    return-void

    .line 1766861
    :cond_1
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v12

    move v10, v4

    :goto_2
    if-ge v10, v12, :cond_2

    invoke-virtual {v11, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 1766862
    iget-object v13, v0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->h:Ljava/util/Queue;

    .line 1766863
    iget-object v3, v2, LX/BGE;->a:Ljava/util/Map;

    invoke-interface {v3, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object v5, v3

    .line 1766864
    iget-object v3, v2, LX/BGE;->b:Ljava/lang/String;

    move-object v7, v3

    .line 1766865
    move-object v3, v0

    move-object v4, v1

    invoke-static/range {v3 .. v9}, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->a(Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;LX/Ave;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/atomic/AtomicInteger;Ljava/util/concurrent/atomic/AtomicBoolean;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-interface {v13, v3}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 1766866
    add-int/lit8 v3, v10, 0x1

    move v10, v3

    goto :goto_2

    .line 1766867
    :cond_2
    invoke-static {v0}, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->b$redex0(Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;)V

    goto :goto_1
.end method
