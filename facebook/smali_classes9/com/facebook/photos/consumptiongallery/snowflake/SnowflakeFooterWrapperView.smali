.class public Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeFooterWrapperView;
.super LX/Ca9;
.source ""


# instance fields
.field private a:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;

.field private b:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1917323
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeFooterWrapperView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1917324
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 1917315
    invoke-direct {p0, p1, p2}, LX/Ca9;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1917316
    const v0, 0x7f031377

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1917317
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeFooterWrapperView;->setOrientation(I)V

    .line 1917318
    const v0, 0x7f0d2cf6

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;

    iput-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeFooterWrapperView;->a:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;

    .line 1917319
    const v0, 0x7f0d2cf8

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;

    iput-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeFooterWrapperView;->b:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;

    .line 1917320
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeFooterWrapperView;->b:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;

    iget-object v1, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeFooterWrapperView;->a:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;

    .line 1917321
    iput-object v1, v0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->m:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;

    .line 1917322
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1917313
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeFooterWrapperView;->b:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;

    invoke-virtual {v0}, LX/Ca9;->a()V

    .line 1917314
    return-void
.end method

.method public final a(LX/Ca6;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 1

    .prologue
    .line 1917293
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeFooterWrapperView;->b:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;

    invoke-virtual {v0, p1, p2}, LX/Ca9;->a(LX/Ca6;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1917294
    return-void
.end method

.method public getCommentText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1917312
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeFooterWrapperView;->b:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;

    invoke-virtual {v0}, LX/Ca9;->getCommentText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLikeText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1917311
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeFooterWrapperView;->b:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;

    invoke-virtual {v0}, LX/Ca9;->getLikeText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setCommentsCount(I)V
    .locals 1

    .prologue
    .line 1917309
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeFooterWrapperView;->a:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;

    invoke-virtual {v0, p1}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->setCommentsCount(I)V

    .line 1917310
    return-void
.end method

.method public setIsExpanded(Z)V
    .locals 1

    .prologue
    .line 1917307
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeFooterWrapperView;->a:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;

    invoke-virtual {v0, p1}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->setIsExpanded(Z)V

    .line 1917308
    return-void
.end method

.method public setLikesCount(I)V
    .locals 1

    .prologue
    .line 1917305
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeFooterWrapperView;->a:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;

    invoke-virtual {v0, p1}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->setLikesCount(I)V

    .line 1917306
    return-void
.end method

.method public setListener(LX/CES;)V
    .locals 1

    .prologue
    .line 1917301
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeFooterWrapperView;->a:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;

    .line 1917302
    iput-object p1, v0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->j:LX/CES;

    .line 1917303
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeFooterWrapperView;->b:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;

    invoke-virtual {v0, p1}, LX/Ca9;->setListener(LX/CES;)V

    .line 1917304
    return-void
.end method

.method public setMenuButtonEnabled(Z)V
    .locals 1

    .prologue
    .line 1917299
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeFooterWrapperView;->b:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;

    invoke-virtual {v0, p1}, LX/Ca9;->setMenuButtonEnabled(Z)V

    .line 1917300
    return-void
.end method

.method public setShareButtonEnabled(Z)V
    .locals 1

    .prologue
    .line 1917297
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeFooterWrapperView;->b:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;

    invoke-virtual {v0, p1}, LX/Ca9;->setShareButtonEnabled(Z)V

    .line 1917298
    return-void
.end method

.method public setTagButtonEnabled(Z)V
    .locals 1

    .prologue
    .line 1917295
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeFooterWrapperView;->b:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;

    invoke-virtual {v0, p1}, LX/Ca9;->setTagButtonEnabled(Z)V

    .line 1917296
    return-void
.end method
