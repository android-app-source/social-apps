.class public Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;
.super LX/Ca9;
.source ""


# instance fields
.field public a:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/215;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/view/View;

.field private final e:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

.field private final f:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

.field private final g:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Landroid/graphics/drawable/Drawable;

.field private final k:Landroid/graphics/drawable/Drawable;

.field public l:LX/CES;

.field public m:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;

.field public n:LX/Ca6;

.field private o:Lcom/facebook/graphql/model/GraphQLStory;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1917290
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1917291
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const v3, -0x6e685d

    .line 1917269
    invoke-direct {p0, p1, p2}, LX/Ca9;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1917270
    const-class v0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;

    invoke-static {v0, p0}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1917271
    const v0, 0x7f031376

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1917272
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->setOrientation(I)V

    .line 1917273
    const v0, 0x7f0d2cf7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->d:Landroid/view/View;

    .line 1917274
    invoke-virtual {p0}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081107

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->h:Ljava/lang/String;

    .line 1917275
    invoke-virtual {p0}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081106

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->i:Ljava/lang/String;

    .line 1917276
    const v0, 0x7f0219c6

    invoke-direct {p0, v0, v3}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->j:Landroid/graphics/drawable/Drawable;

    .line 1917277
    const v0, 0x7f0219c6

    const v1, -0xa76f01

    invoke-direct {p0, v0, v1}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->k:Landroid/graphics/drawable/Drawable;

    .line 1917278
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->j:Landroid/graphics/drawable/Drawable;

    const v1, 0x7f080fc8

    const v2, 0x7f0d0c3f

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->a(Landroid/graphics/drawable/Drawable;II)Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->e:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 1917279
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->e:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-direct {p0}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->b()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1917280
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->e:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-direct {p0, v0}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->setButtonSpring(Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;)V

    .line 1917281
    const v0, 0x7f0219c3

    invoke-direct {p0, v0, v3}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const v1, 0x7f080fcc

    const v2, 0x7f0d0c42

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->a(Landroid/graphics/drawable/Drawable;II)Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->f:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 1917282
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->f:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    new-instance v1, LX/CaD;

    invoke-direct {v1, p0}, LX/CaD;-><init>(Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1917283
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->f:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-direct {p0, v0}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->setButtonSpring(Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;)V

    .line 1917284
    const v0, 0x7f0219c9

    invoke-direct {p0, v0, v3}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const v1, 0x7f080fd9

    const v2, 0x7f0d0c43

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->a(Landroid/graphics/drawable/Drawable;II)Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->g:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 1917285
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->g:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    new-instance v1, LX/CaE;

    invoke-direct {v1, p0}, LX/CaE;-><init>(Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1917286
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->g:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-direct {p0, v0}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->setButtonSpring(Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;)V

    .line 1917287
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->e:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    iget-object v1, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->f:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    iget-object v2, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->g:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-static {v0, v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->c:LX/0Rf;

    .line 1917288
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->c:LX/0Rf;

    invoke-static {v0}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->setButtonsEqualWeight(LX/0Rf;)V

    .line 1917289
    return-void
.end method

.method private a(II)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 1917268
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->a:LX/0wM;

    invoke-virtual {v0, p1, p2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/graphics/drawable/Drawable;II)Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;
    .locals 2

    .prologue
    .line 1917263
    invoke-virtual {p0, p3}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 1917264
    invoke-virtual {v0, p2}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setText(I)V

    .line 1917265
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setSoundEffectsEnabled(Z)V

    .line 1917266
    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1917267
    return-object v0
.end method

.method private static a(Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;LX/0wM;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;",
            "LX/0wM;",
            "LX/0Or",
            "<",
            "LX/215;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1917262
    iput-object p1, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->a:LX/0wM;

    iput-object p2, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->b:LX/0Or;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;

    invoke-static {v1}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    const/16 v2, 0x13a4

    invoke-static {v1, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->a(Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;LX/0wM;LX/0Or;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;ZZI)V
    .locals 2

    .prologue
    .line 1917254
    if-nez p2, :cond_0

    if-eqz p1, :cond_2

    if-lez p3, :cond_2

    .line 1917255
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->m:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;

    invoke-virtual {v0, p3}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->setLikesCount(I)V

    .line 1917256
    :cond_1
    :goto_0
    return-void

    .line 1917257
    :cond_2
    if-eqz p1, :cond_3

    if-nez p3, :cond_3

    .line 1917258
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->m:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->setIsExpanded(Z)V

    goto :goto_0

    .line 1917259
    :cond_3
    if-lez p3, :cond_1

    .line 1917260
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->m:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->setIsExpanded(Z)V

    .line 1917261
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->m:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;

    invoke-virtual {v0, p3}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->setLikesCount(I)V

    goto :goto_0
.end method

.method private b()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1917292
    new-instance v0, LX/CaF;

    invoke-direct {v0, p0}, LX/CaF;-><init>(Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;)V

    return-object v0
.end method

.method private c()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 1917241
    invoke-direct {p0}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->getButtonsSet()Ljava/util/List;

    move-result-object v1

    .line 1917242
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1917243
    invoke-virtual {p0, v5}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->setVisibility(I)V

    .line 1917244
    :goto_0
    return-void

    .line 1917245
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->c:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 1917246
    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1917247
    invoke-virtual {v0, v4}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setVisibility(I)V

    goto :goto_1

    .line 1917248
    :cond_1
    invoke-virtual {v0, v5}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setVisibility(I)V

    goto :goto_1

    .line 1917249
    :cond_2
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->e:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1917250
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->n:LX/Ca6;

    .line 1917251
    iget-boolean v1, v0, LX/Ca6;->j:Z

    move v0, v1

    .line 1917252
    invoke-virtual {p0, v0}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->setIsLiked(Z)V

    .line 1917253
    :cond_3
    invoke-virtual {p0, v4}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->setVisibility(I)V

    goto :goto_0
.end method

.method private getButtonsSet()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1917229
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1917230
    iget-object v1, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->n:LX/Ca6;

    .line 1917231
    iget-boolean v2, v1, LX/Ca6;->h:Z

    move v1, v2

    .line 1917232
    if-eqz v1, :cond_0

    .line 1917233
    iget-object v1, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->e:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1917234
    :cond_0
    iget-object v1, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->n:LX/Ca6;

    .line 1917235
    iget-boolean v2, v1, LX/Ca6;->i:Z

    move v1, v2

    .line 1917236
    if-eqz v1, :cond_1

    .line 1917237
    iget-object v1, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->f:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1917238
    :cond_1
    iget-object v1, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->o:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->o:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/214;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1917239
    iget-object v1, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->g:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1917240
    :cond_2
    return-object v0
.end method

.method private setButtonSpring(Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;)V
    .locals 1

    .prologue
    .line 1917202
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/215;

    invoke-virtual {p1, v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setSpring(LX/215;)V

    .line 1917203
    return-void
.end method

.method private static setButtonsEqualWeight(LX/0Rf;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rf",
            "<",
            "Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1917226
    invoke-virtual {p0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 1917227
    invoke-virtual {v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    goto :goto_0

    .line 1917228
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1917224
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->d:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1917225
    return-void
.end method

.method public final a(LX/Ca6;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 2

    .prologue
    .line 1917216
    iput-object p1, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->n:LX/Ca6;

    .line 1917217
    iput-object p2, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->o:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1917218
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->n:LX/Ca6;

    if-nez v0, :cond_0

    .line 1917219
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->setVisibility(I)V

    .line 1917220
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->m:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->a(LX/Ca6;)V

    .line 1917221
    :goto_0
    return-void

    .line 1917222
    :cond_0
    invoke-direct {p0}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->c()V

    .line 1917223
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->m:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;

    iget-object v1, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->n:LX/Ca6;

    invoke-virtual {v0, v1}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->a(LX/Ca6;)V

    goto :goto_0
.end method

.method public setBlingBar(Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;)V
    .locals 0

    .prologue
    .line 1917214
    iput-object p1, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->m:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;

    .line 1917215
    return-void
.end method

.method public setIsLiked(Z)V
    .locals 2

    .prologue
    .line 1917206
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->e:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    const v1, 0x7f080fc8

    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setText(I)V

    .line 1917207
    iget-object v1, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->e:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    if-eqz p1, :cond_0

    const v0, -0xa76f01

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setTextColor(I)V

    .line 1917208
    iget-object v1, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->e:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->k:Landroid/graphics/drawable/Drawable;

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1917209
    iget-object v1, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->e:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->h:Ljava/lang/String;

    :goto_2
    invoke-virtual {v1, v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1917210
    return-void

    .line 1917211
    :cond_0
    const v0, -0x6e685d

    goto :goto_0

    .line 1917212
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->j:Landroid/graphics/drawable/Drawable;

    goto :goto_1

    .line 1917213
    :cond_2
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->i:Ljava/lang/String;

    goto :goto_2
.end method

.method public setListener(LX/CES;)V
    .locals 0

    .prologue
    .line 1917204
    iput-object p1, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->l:LX/CES;

    .line 1917205
    return-void
.end method
