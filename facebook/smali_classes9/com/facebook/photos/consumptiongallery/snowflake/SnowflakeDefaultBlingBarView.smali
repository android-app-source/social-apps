.class public Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private final a:Landroid/widget/TextView;

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/view/View;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Landroid/view/animation/Animation;

.field private final i:Landroid/view/animation/Animation;

.field public j:LX/CES;

.field public k:LX/Ca6;

.field public l:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1917128
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1917129
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    .line 1917130
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1917131
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->l:Z

    .line 1917132
    const v0, 0x7f0301a0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1917133
    invoke-virtual {p0}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1917134
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v0, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1917135
    const v0, 0x7f0d0701

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->a:Landroid/widget/TextView;

    .line 1917136
    const v0, 0x7f0d0702

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->b:Landroid/widget/TextView;

    .line 1917137
    const v0, 0x7f0d0704

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->c:Landroid/view/View;

    .line 1917138
    const v0, 0x7f0810d0

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->d:Ljava/lang/String;

    .line 1917139
    const v0, 0x7f0810d1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->e:Ljava/lang/String;

    .line 1917140
    const v0, 0x7f0810d2

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->f:Ljava/lang/String;

    .line 1917141
    const v0, 0x7f0810d3

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->g:Ljava/lang/String;

    .line 1917142
    const v0, 0x7f0b0979

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1917143
    const v2, 0x7f0b097a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 1917144
    iget-object v3, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->c:Landroid/view/View;

    invoke-virtual {v3, v2, v0, v2, v0}, Landroid/view/View;->setPadding(IIII)V

    .line 1917145
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->c:Landroid/view/View;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    const v3, 0x7f0a00d5

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v2, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v0, v2}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1917146
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->c:Landroid/view/View;

    new-instance v1, LX/CaA;

    invoke-direct {v1, p0}, LX/CaA;-><init>(Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1917147
    invoke-direct {p0}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->b()Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->h:Landroid/view/animation/Animation;

    .line 1917148
    invoke-direct {p0}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->c()Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->i:Landroid/view/animation/Animation;

    .line 1917149
    return-void
.end method

.method private static a(Landroid/widget/TextView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1917101
    if-nez p1, :cond_0

    .line 1917102
    const-string v0, ""

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1917103
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1917104
    :goto_0
    return-void

    .line 1917105
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1917106
    invoke-virtual {p0}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    invoke-static {p2, p3, p1}, LX/1z0;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 1917107
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private b()Landroid/view/animation/Animation;
    .locals 4

    .prologue
    .line 1917123
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1917124
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1917125
    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1917126
    new-instance v1, LX/CaB;

    invoke-direct {v1, p0}, LX/CaB;-><init>(Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1917127
    return-object v0
.end method

.method private c()Landroid/view/animation/Animation;
    .locals 4

    .prologue
    .line 1917118
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1917119
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1917120
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1917121
    new-instance v1, LX/CaC;

    invoke-direct {v1, p0}, LX/CaC;-><init>(Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1917122
    return-object v0
.end method


# virtual methods
.method public final a(LX/Ca6;)V
    .locals 1

    .prologue
    .line 1917150
    iput-object p1, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->k:LX/Ca6;

    .line 1917151
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/Ca6;->h()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1917152
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->setVisibility(I)V

    .line 1917153
    :goto_0
    return-void

    .line 1917154
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->setVisibility(I)V

    .line 1917155
    iget v0, p1, LX/Ca6;->f:I

    move v0, v0

    .line 1917156
    invoke-virtual {p0, v0}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->setLikesCount(I)V

    .line 1917157
    iget v0, p1, LX/Ca6;->g:I

    move v0, v0

    .line 1917158
    invoke-virtual {p0, v0}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->setCommentsCount(I)V

    goto :goto_0
.end method

.method public setCommentsCount(I)V
    .locals 3

    .prologue
    .line 1917116
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->g:Ljava/lang/String;

    invoke-static {v0, p1, v1, v2}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->a(Landroid/widget/TextView;ILjava/lang/String;Ljava/lang/String;)V

    .line 1917117
    return-void
.end method

.method public setIsExpanded(Z)V
    .locals 1

    .prologue
    .line 1917112
    iget-boolean v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->l:Z

    if-nez v0, :cond_0

    .line 1917113
    :goto_0
    return-void

    .line 1917114
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->l:Z

    .line 1917115
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->i:Landroid/view/animation/Animation;

    :goto_1
    invoke-virtual {p0, v0}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->h:Landroid/view/animation/Animation;

    goto :goto_1
.end method

.method public setLikesCount(I)V
    .locals 3

    .prologue
    .line 1917110
    iget-object v0, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->e:Ljava/lang/String;

    invoke-static {v0, p1, v1, v2}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->a(Landroid/widget/TextView;ILjava/lang/String;Ljava/lang/String;)V

    .line 1917111
    return-void
.end method

.method public setListener(LX/CES;)V
    .locals 0

    .prologue
    .line 1917108
    iput-object p1, p0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->j:LX/CES;

    .line 1917109
    return-void
.end method
