.class public Lcom/facebook/photos/taggablegallery/GifVideoPlayerGalleryDelegate;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/BIT;


# instance fields
.field private a:LX/1Ad;

.field public b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private c:Landroid/view/ViewGroup;

.field public d:Lcom/facebook/gif/AnimatedImagePlayButtonView;

.field private e:LX/1aZ;


# direct methods
.method public constructor <init>(LX/1Ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1770621
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1770622
    iput-object p1, p0, Lcom/facebook/photos/taggablegallery/GifVideoPlayerGalleryDelegate;->a:LX/1Ad;

    .line 1770623
    return-void
.end method

.method public static e(Lcom/facebook/photos/taggablegallery/GifVideoPlayerGalleryDelegate;)Landroid/graphics/drawable/Animatable;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1770620
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/GifVideoPlayerGalleryDelegate;->e:LX/1aZ;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/GifVideoPlayerGalleryDelegate;->e:LX/1aZ;

    invoke-interface {v0}, LX/1aZ;->f()Landroid/graphics/drawable/Animatable;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1770614
    const v0, 0x7f0307af

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1770615
    const v0, 0x7f0d1479

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/photos/taggablegallery/GifVideoPlayerGalleryDelegate;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1770616
    const v0, 0x7f0d147a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/gif/AnimatedImagePlayButtonView;

    iput-object v0, p0, Lcom/facebook/photos/taggablegallery/GifVideoPlayerGalleryDelegate;->d:Lcom/facebook/gif/AnimatedImagePlayButtonView;

    .line 1770617
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/GifVideoPlayerGalleryDelegate;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    new-instance v2, LX/BIU;

    invoke-direct {v2, p0}, LX/BIU;-><init>(Lcom/facebook/photos/taggablegallery/GifVideoPlayerGalleryDelegate;)V

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v0, v1

    .line 1770618
    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/photos/taggablegallery/GifVideoPlayerGalleryDelegate;->c:Landroid/view/ViewGroup;

    .line 1770619
    return-object v1
.end method

.method public final a()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 1770608
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/GifVideoPlayerGalleryDelegate;->c:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 1770609
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/GifVideoPlayerGalleryDelegate;->a:LX/1Ad;

    invoke-virtual {v0, p1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/1Ae;->c(Z)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    .line 1770610
    new-instance v1, LX/BIV;

    invoke-direct {v1, p0}, LX/BIV;-><init>(Lcom/facebook/photos/taggablegallery/GifVideoPlayerGalleryDelegate;)V

    move-object v1, v1

    .line 1770611
    invoke-virtual {v0, v1}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/taggablegallery/GifVideoPlayerGalleryDelegate;->e:LX/1aZ;

    .line 1770612
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/GifVideoPlayerGalleryDelegate;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/photos/taggablegallery/GifVideoPlayerGalleryDelegate;->e:LX/1aZ;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1770613
    return-void
.end method
