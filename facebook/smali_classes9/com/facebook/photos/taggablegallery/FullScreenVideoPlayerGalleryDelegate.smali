.class public Lcom/facebook/photos/taggablegallery/FullScreenVideoPlayerGalleryDelegate;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/BIT;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:Lcom/facebook/video/player/RichVideoPlayer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1770549
    const-class v0, Lcom/facebook/photos/taggablegallery/FullScreenVideoPlayerGalleryDelegate;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/taggablegallery/FullScreenVideoPlayerGalleryDelegate;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1770550
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 1770551
    const v0, 0x7f031594

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1770552
    const v0, 0x7f0d21cd

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    iput-object v0, p0, Lcom/facebook/photos/taggablegallery/FullScreenVideoPlayerGalleryDelegate;->b:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1770553
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/FullScreenVideoPlayerGalleryDelegate;->b:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v2, LX/04D;->MEDIA_PICKER:LX/04D;

    invoke-virtual {v0, v2}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerOrigin(LX/04D;)V

    .line 1770554
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/FullScreenVideoPlayerGalleryDelegate;->b:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v2, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    invoke-virtual {v0, v2}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerType(LX/04G;)V

    .line 1770555
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1770556
    iget-object v2, p0, Lcom/facebook/photos/taggablegallery/FullScreenVideoPlayerGalleryDelegate;->b:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v3, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-direct {v3, v0}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    .line 1770557
    invoke-static {v2, v3}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1770558
    iget-object v2, p0, Lcom/facebook/photos/taggablegallery/FullScreenVideoPlayerGalleryDelegate;->b:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v3, Lcom/facebook/video/player/plugins/CoverImagePlugin;

    sget-object v4, Lcom/facebook/photos/taggablegallery/FullScreenVideoPlayerGalleryDelegate;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v3, v0, v4}, Lcom/facebook/video/player/plugins/CoverImagePlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1770559
    invoke-static {v2, v3}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1770560
    iget-object v2, p0, Lcom/facebook/photos/taggablegallery/FullScreenVideoPlayerGalleryDelegate;->b:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v3, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-direct {v3, v0}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;-><init>(Landroid/content/Context;)V

    .line 1770561
    invoke-static {v2, v3}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1770562
    iget-object v2, p0, Lcom/facebook/photos/taggablegallery/FullScreenVideoPlayerGalleryDelegate;->b:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v3, LX/7N4;

    invoke-direct {v3, v0}, LX/7N4;-><init>(Landroid/content/Context;)V

    .line 1770563
    invoke-static {v2, v3}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1770564
    return-object v1
.end method

.method public final a()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 1770565
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/FullScreenVideoPlayerGalleryDelegate;->b:Lcom/facebook/video/player/RichVideoPlayer;

    return-object v0
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 4

    .prologue
    .line 1770566
    new-instance v0, LX/2oE;

    invoke-direct {v0}, LX/2oE;-><init>()V

    .line 1770567
    iput-object p1, v0, LX/2oE;->a:Landroid/net/Uri;

    .line 1770568
    move-object v0, v0

    .line 1770569
    sget-object v1, LX/097;->FROM_LOCAL_STORAGE:LX/097;

    .line 1770570
    iput-object v1, v0, LX/2oE;->e:LX/097;

    .line 1770571
    move-object v0, v0

    .line 1770572
    invoke-virtual {v0}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v0

    .line 1770573
    new-instance v1, LX/2oH;

    invoke-direct {v1}, LX/2oH;-><init>()V

    invoke-virtual {v1, v0}, LX/2oH;->a(Lcom/facebook/video/engine/VideoDataSource;)LX/2oH;

    move-result-object v0

    invoke-virtual {v0}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v0

    .line 1770574
    new-instance v1, LX/0P2;

    invoke-direct {v1}, LX/0P2;-><init>()V

    const-string v2, "CoverImageParamsKey"

    invoke-static {p1}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    .line 1770575
    new-instance v2, LX/2pZ;

    invoke-direct {v2}, LX/2pZ;-><init>()V

    .line 1770576
    iput-object v0, v2, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1770577
    move-object v0, v2

    .line 1770578
    invoke-virtual {v0, v1}, LX/2pZ;->a(LX/0P1;)LX/2pZ;

    move-result-object v0

    invoke-virtual {v0}, LX/2pZ;->b()LX/2pa;

    move-result-object v0

    .line 1770579
    iget-object v1, p0, Lcom/facebook/photos/taggablegallery/FullScreenVideoPlayerGalleryDelegate;->b:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 1770580
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/FullScreenVideoPlayerGalleryDelegate;->b:Lcom/facebook/video/player/RichVideoPlayer;

    const/4 v1, 0x0

    sget-object v2, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 1770581
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/FullScreenVideoPlayerGalleryDelegate;->b:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 1770582
    return-void
.end method
