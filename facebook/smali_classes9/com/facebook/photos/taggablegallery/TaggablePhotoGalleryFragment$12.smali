.class public final Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment$12;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/74x;

.field public final synthetic b:LX/9iQ;

.field public final synthetic c:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;LX/74x;LX/9iQ;)V
    .locals 0

    .prologue
    .line 1770946
    iput-object p1, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment$12;->c:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    iput-object p2, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment$12;->a:LX/74x;

    iput-object p3, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment$12;->b:LX/9iQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 12

    .prologue
    .line 1770947
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment$12;->c:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    iget-object v1, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment$12;->a:LX/74x;

    iget-object v2, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment$12;->c:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    iget-object v2, v2, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->p:Lcom/facebook/photos/base/tagging/FaceBox;

    const/4 v4, 0x0

    .line 1770948
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1770949
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1770950
    iget-object v3, v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->c:LX/75F;

    invoke-virtual {v3, v1}, LX/75F;->a(LX/74x;)Ljava/util/List;

    move-result-object v3

    if-nez v3, :cond_2

    move-object v3, v4

    .line 1770951
    :goto_0
    move-object v1, v3

    .line 1770952
    if-eqz v1, :cond_0

    .line 1770953
    iget-boolean v0, v1, Lcom/facebook/photos/base/tagging/FaceBox;->f:Z

    move v0, v0

    .line 1770954
    if-eqz v0, :cond_1

    .line 1770955
    :cond_0
    :goto_1
    return-void

    .line 1770956
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment$12;->c:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    iget-object v2, v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->T:LX/9iw;

    iget-object v3, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment$12;->a:LX/74x;

    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment$12;->b:LX/9iQ;

    check-cast v0, LX/9ip;

    const/4 v4, 0x1

    .line 1770957
    iget-object v11, v2, LX/9iw;->d:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    new-instance v5, Lcom/facebook/photos/tagging/ui/TaggingInterfaceController$2;

    move-object v6, v2

    move-object v7, v3

    move-object v8, v1

    move v9, v4

    move-object v10, v0

    invoke-direct/range {v5 .. v10}, Lcom/facebook/photos/tagging/ui/TaggingInterfaceController$2;-><init>(LX/9iw;LX/74x;Lcom/facebook/photos/base/tagging/TagTarget;ZLX/9ip;)V

    invoke-static {v11, v5}, LX/8He;->c(Landroid/view/View;Ljava/lang/Runnable;)Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 1770958
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment$12;->c:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    const/4 v1, 0x0

    .line 1770959
    iput-object v1, v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->p:Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1770960
    goto :goto_1

    .line 1770961
    :cond_2
    invoke-static {v0}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->q(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)LX/9ip;

    move-result-object v5

    .line 1770962
    iget-object v3, v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->c:LX/75F;

    invoke-static {v0}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->p(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)Lcom/facebook/photos/base/photos/LocalPhoto;

    move-result-object v6

    invoke-virtual {v3, v6}, LX/75F;->a(LX/74x;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1770963
    iget-object v7, v3, Lcom/facebook/photos/base/tagging/FaceBox;->a:Ljava/lang/String;

    move-object v7, v7

    .line 1770964
    iget-object v8, v2, Lcom/facebook/photos/base/tagging/FaceBox;->a:Ljava/lang/String;

    move-object v8, v8

    .line 1770965
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1770966
    invoke-virtual {v5, v3}, LX/9ip;->a(Lcom/facebook/photos/base/tagging/FaceBox;)Lcom/facebook/photos/base/tagging/FaceBox;

    move-result-object v3

    goto :goto_0

    :cond_4
    move-object v3, v4

    .line 1770967
    goto :goto_0
.end method
