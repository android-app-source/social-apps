.class public Lcom/facebook/photos/taggablegallery/TaggableGalleryActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private p:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

.field private q:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/photos/base/media/PhotoItem;",
            ">;"
        }
    .end annotation
.end field

.field private r:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/photos/creativeediting/model/CreativeEditingData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1770823
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;
    .locals 3

    .prologue
    .line 1770824
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d002f

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    .line 1770825
    if-eqz v0, :cond_0

    .line 1770826
    :goto_0
    return-object v0

    .line 1770827
    :cond_0
    new-instance v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-direct {v0}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;-><init>()V

    .line 1770828
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    invoke-virtual {v1, v2, v0}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v1

    invoke-virtual {v1}, LX/0hH;->b()I

    goto :goto_0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 19

    .prologue
    .line 1770829
    invoke-super/range {p0 .. p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1770830
    const v2, 0x7f03132a

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/facebook/photos/taggablegallery/TaggableGalleryActivity;->setContentView(I)V

    .line 1770831
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/taggablegallery/TaggableGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 1770832
    const-string v3, "extra_taggable_gallery_photo_list"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/facebook/photos/taggablegallery/TaggableGalleryActivity;->q:Ljava/util/ArrayList;

    .line 1770833
    const-string v3, "extras_taggable_gallery_creative_editing_data_list"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1770834
    const-string v3, "extras_taggable_gallery_creative_editing_data_list"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/facebook/photos/taggablegallery/TaggableGalleryActivity;->r:Ljava/util/ArrayList;

    .line 1770835
    new-instance v4, LX/BIl;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/photos/taggablegallery/TaggableGalleryActivity;->q:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/photos/taggablegallery/TaggableGalleryActivity;->r:Ljava/util/ArrayList;

    invoke-direct {v4, v3, v5}, LX/BIl;-><init>(Ljava/util/List;Ljava/util/List;)V

    .line 1770836
    :goto_0
    const-string v3, "extra_taggable_gallery_photo_item_id"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Lcom/facebook/ipc/media/MediaIdKey;

    .line 1770837
    const-string v3, "extra_taggable_gallery_goto_facebox"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v12

    check-cast v12, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1770838
    invoke-direct/range {p0 .. p0}, Lcom/facebook/photos/taggablegallery/TaggableGalleryActivity;->a()Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/facebook/photos/taggablegallery/TaggableGalleryActivity;->p:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    .line 1770839
    new-instance v9, LX/BIe;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, LX/BIe;-><init>(Lcom/facebook/photos/taggablegallery/TaggableGalleryActivity;)V

    .line 1770840
    const-string v3, "extra_is_friend_tagging_enabled"

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    .line 1770841
    const-string v3, "extra_is_product_tagging_enabled"

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    .line 1770842
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/photos/taggablegallery/TaggableGalleryActivity;->p:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/photos/taggablegallery/TaggableGalleryActivity;->r:Ljava/util/ArrayList;

    sget-object v10, LX/BIf;->COMPOSER:LX/BIf;

    const-string v11, "extra_session_id"

    invoke-virtual {v2, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v13

    const-string v14, "extra_media_container_type"

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v13

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v14

    const-string v15, "extra_media_container_id"

    const-wide/16 v16, 0x0

    invoke-virtual/range {v14 .. v17}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v14

    const-string v16, "tag_typeahead_data_source_metadata"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v16

    check-cast v16, Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v17, "show_tag_expansion_information"

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v17

    invoke-virtual/range {v3 .. v17}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->a(LX/BIi;Ljava/util/ArrayList;Lcom/facebook/ipc/media/MediaIdKey;ZZLX/BH7;LX/BIf;Ljava/lang/String;Lcom/facebook/photos/base/tagging/FaceBox;IJLcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;Z)V

    .line 1770843
    return-void

    .line 1770844
    :cond_0
    new-instance v4, LX/BIl;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/photos/taggablegallery/TaggableGalleryActivity;->q:Ljava/util/ArrayList;

    invoke-direct {v4, v3}, LX/BIl;-><init>(Ljava/util/List;)V

    goto :goto_0
.end method

.method public final finish()V
    .locals 5

    .prologue
    .line 1770845
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggableGalleryActivity;->p:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggableGalleryActivity;->p:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    .line 1770846
    iget-boolean v1, v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->v:Z

    move v0, v1

    .line 1770847
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 1770848
    :goto_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "extra_photo_items_list"

    iget-object v3, p0, Lcom/facebook/photos/taggablegallery/TaggableGalleryActivity;->q:Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "extra_are_media_items_modified"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 1770849
    iget-object v1, p0, Lcom/facebook/photos/taggablegallery/TaggableGalleryActivity;->r:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/photos/taggablegallery/TaggableGalleryActivity;->r:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1770850
    const-string v1, "extras_taggable_gallery_creative_editing_data_list"

    iget-object v2, p0, Lcom/facebook/photos/taggablegallery/TaggableGalleryActivity;->p:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    .line 1770851
    iget-object v3, v2, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->R:Ljava/util/ArrayList;

    if-eqz v3, :cond_2

    new-instance v3, Ljava/util/ArrayList;

    iget-object v4, v2, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->R:Ljava/util/ArrayList;

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    :goto_1
    move-object v2, v3

    .line 1770852
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1770853
    :cond_0
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/facebook/photos/taggablegallery/TaggableGalleryActivity;->setResult(ILandroid/content/Intent;)V

    .line 1770854
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 1770855
    return-void

    .line 1770856
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 1770857
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggableGalleryActivity;->p:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-virtual {v0}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1770858
    :goto_0
    return-void

    .line 1770859
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    goto :goto_0
.end method
