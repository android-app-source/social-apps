.class public Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/photos/taggablegallery/GifVideoPlayerGalleryDelegate;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1770765
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryActivity;

    const/16 v1, 0x2ec1

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryActivity;->p:LX/0Or;

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 1770744
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1770745
    invoke-static {p0, p0}, Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1770746
    const v0, 0x7f031595

    invoke-virtual {p0, v0}, Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryActivity;->setContentView(I)V

    .line 1770747
    invoke-virtual {p0}, Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 1770748
    const-string v0, "extra_video_item"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/media/VideoItem;

    .line 1770749
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    const v3, 0x7f0d002f

    invoke-virtual {v1, v3}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryFragment;

    .line 1770750
    if-nez v1, :cond_0

    .line 1770751
    invoke-static {}, LX/BIf;->values()[LX/BIf;

    move-result-object v1

    const-string v3, "extra_source"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    aget-object v3, v1, v3

    const-string v1, "extra_session_id"

    invoke-virtual {v2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v1, "extra_video_uri"

    invoke-virtual {v2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 1770752
    new-instance v2, Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryFragment;

    invoke-direct {v2}, Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryFragment;-><init>()V

    .line 1770753
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 1770754
    const-string p1, "source"

    invoke-virtual {v5, p1, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1770755
    const-string p1, "session_id"

    invoke-virtual {v5, p1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1770756
    const-string p1, "video_item"

    invoke-virtual {v5, p1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1770757
    const-string p1, "video_uri"

    invoke-virtual {v5, p1, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1770758
    invoke-virtual {v2, v5}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1770759
    move-object v1, v2

    .line 1770760
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v2

    invoke-virtual {v2}, LX/0gc;->a()LX/0hH;

    move-result-object v2

    const v3, 0x7f0d002f

    invoke-virtual {v2, v3, v1}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v2

    invoke-virtual {v2}, LX/0hH;->b()I

    .line 1770761
    :cond_0
    if-eqz v0, :cond_1

    sget-object v2, Lcom/facebook/ipc/media/MediaItem;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryActivity;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BIT;

    .line 1770762
    :goto_0
    iput-object v0, v1, Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryFragment;->f:LX/BIT;

    .line 1770763
    return-void

    .line 1770764
    :cond_1
    new-instance v0, Lcom/facebook/photos/taggablegallery/FullScreenVideoPlayerGalleryDelegate;

    invoke-direct {v0}, Lcom/facebook/photos/taggablegallery/FullScreenVideoPlayerGalleryDelegate;-><init>()V

    goto :goto_0
.end method
