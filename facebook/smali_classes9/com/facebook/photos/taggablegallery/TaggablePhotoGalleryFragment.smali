.class public Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final o:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private A:Lcom/facebook/resources/ui/FbTextView;

.field private B:Landroid/widget/ImageView;

.field private C:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

.field private D:Landroid/graphics/drawable/Drawable;

.field private E:Landroid/graphics/drawable/Drawable;

.field public F:I

.field public G:I

.field public H:I

.field private I:I

.field public J:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation
.end field

.field private K:Lcom/facebook/ipc/media/MediaIdKey;

.field public L:LX/BH7;

.field public M:Lcom/facebook/photos/taggablegallery/PhotoGallery;

.field public N:Ljava/lang/String;

.field private O:Ljava/lang/String;

.field public P:LX/BIf;

.field public Q:LX/BIi;

.field public R:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/photos/creativeediting/model/CreativeEditingData;",
            ">;"
        }
    .end annotation
.end field

.field public S:LX/9ip;

.field public T:LX/9iw;

.field public U:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

.field private V:LX/8Hs;

.field private W:LX/8Hs;

.field public X:Lcom/facebook/photos/tagging/shared/TagTypeahead;

.field private Y:I

.field private Z:J

.field public a:LX/9iZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private aa:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

.field private ab:Z

.field public b:LX/9iq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/75F;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/8JL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/8HH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/facerec/manager/FaceBoxPrioritizer;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/8I2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/74G;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/BIk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/9ix;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/75Q;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/4mV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/1HI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:Lcom/facebook/photos/base/tagging/FaceBox;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Z

.field public r:Z

.field public s:Z

.field public t:Z

.field public u:LX/BJ1;

.field public v:Z

.field public w:Z

.field public x:Z

.field private y:Z

.field private z:Landroid/database/Cursor;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1771284
    const-class v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    const-string v1, "taggable_gallery"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->o:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1771285
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1771286
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 6

    .prologue
    const-wide/16 v2, 0x96

    const/4 v4, 0x0

    .line 1771287
    iget-boolean v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->ab:Z

    if-eqz v0, :cond_0

    .line 1771288
    const v0, 0x7f0d12a8

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 1771289
    :cond_0
    const v0, 0x7f0d00bc

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iput-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->U:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 1771290
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-static {p0, v0}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->a(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;I)V

    .line 1771291
    new-instance v0, LX/8Hs;

    iget-object v1, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->U:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iget-object v5, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->l:LX/4mV;

    invoke-direct/range {v0 .. v5}, LX/8Hs;-><init>(Landroid/view/View;JZLX/4mV;)V

    iput-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->W:LX/8Hs;

    .line 1771292
    new-instance v0, LX/8Hs;

    const v1, 0x7f0d2e81

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v5, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->l:LX/4mV;

    invoke-direct/range {v0 .. v5}, LX/8Hs;-><init>(Landroid/view/View;JZLX/4mV;)V

    iput-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->V:LX/8Hs;

    .line 1771293
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->P:LX/BIf;

    sget-object v1, LX/BIf;->COMPOSER:LX/BIf;

    if-eq v0, v1, :cond_1

    .line 1771294
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->U:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    const v1, 0x7f08135b

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(Ljava/lang/String;)V

    .line 1771295
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->U:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    new-instance v1, LX/BIq;

    invoke-direct {v1, p0}, LX/BIq;-><init>(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 1771296
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->P:LX/BIf;

    sget-object v2, LX/BIf;->COMPOSER:LX/BIf;

    if-ne v0, v2, :cond_2

    const v0, 0x7f08134f

    :goto_0
    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1771297
    iput-object v0, v1, LX/108;->g:Ljava/lang/String;

    .line 1771298
    move-object v0, v1

    .line 1771299
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    .line 1771300
    iget-object v1, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->U:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setButtonSpecs(Ljava/util/List;)V

    .line 1771301
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->U:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    new-instance v1, LX/BIr;

    invoke-direct {v1, p0}, LX/BIr;-><init>(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setOnToolbarButtonListener(LX/63W;)V

    .line 1771302
    const v0, 0x7f0d2e86

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1771303
    new-instance v1, LX/BIs;

    invoke-direct {v1, p0}, LX/BIs;-><init>(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1771304
    return-void

    .line 1771305
    :cond_2
    const v0, 0x7f08135a

    goto :goto_0
.end method

.method private static a(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;I)V
    .locals 3

    .prologue
    .line 1771157
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->U:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-virtual {v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 1771158
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    const v0, 0x7f0b0bb4

    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1771159
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->U:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1771160
    return-void

    .line 1771161
    :cond_0
    const v0, 0x7f0b0bb3

    goto :goto_0
.end method

.method public static a(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;II)V
    .locals 6

    .prologue
    .line 1771306
    const/4 v0, 0x0

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1771307
    iget-object v1, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->Q:LX/BIi;

    invoke-interface {v1}, LX/BIi;->a()I

    move-result v1

    invoke-static {p2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1771308
    :goto_0
    if-ge v0, v1, :cond_1

    .line 1771309
    iget-object v2, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->Q:LX/BIi;

    invoke-interface {v2, v0}, LX/BIi;->a(I)Lcom/facebook/photos/taggablegallery/PhotoGalleryContent;

    move-result-object v2

    .line 1771310
    iget-object v3, v2, Lcom/facebook/photos/taggablegallery/PhotoGalleryContent;->a:Lcom/facebook/photos/base/media/PhotoItem;

    move-object v2, v3

    .line 1771311
    iget-object v3, v2, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v2, v3

    .line 1771312
    sget-object v3, LX/74z;->THUMBNAIL:LX/74z;

    invoke-virtual {v2, v3}, LX/74w;->a(LX/74z;)LX/4n9;

    move-result-object v2

    .line 1771313
    iget-object v3, v2, LX/4n9;->k:LX/4n7;

    if-eqz v3, :cond_2

    .line 1771314
    iget-object v3, v2, LX/4n9;->k:LX/4n7;

    .line 1771315
    :goto_1
    move-object v3, v3

    .line 1771316
    if-eqz v3, :cond_0

    .line 1771317
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v2, v3}, LX/4eI;->a(LX/4n9;Landroid/content/res/Resources;)LX/1bX;

    move-result-object v2

    invoke-virtual {v2}, LX/1bX;->n()LX/1bf;

    move-result-object v2

    .line 1771318
    iget-object v3, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->n:LX/1HI;

    sget-object v4, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->o:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v2, v4}, LX/1HI;->d(LX/1bf;Ljava/lang/Object;)LX/1ca;

    .line 1771319
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1771320
    :cond_1
    return-void

    .line 1771321
    :cond_2
    iget-object v3, v2, LX/4n9;->d:LX/4n2;

    if-eqz v3, :cond_4

    .line 1771322
    iget-object v3, v2, LX/4n9;->e:Ljava/lang/String;

    sget-object v4, LX/4n2;->a:Ljava/lang/String;

    if-ne v3, v4, :cond_3

    .line 1771323
    const/4 v3, 0x0

    goto :goto_1

    .line 1771324
    :cond_3
    new-instance v3, LX/4n7;

    iget-object v4, v2, LX/4n9;->a:Landroid/net/Uri;

    iget-object v5, v2, LX/4n9;->b:LX/4n3;

    iget-object p1, v2, LX/4n9;->f:LX/4n5;

    iget-object p2, v2, LX/4n9;->e:Ljava/lang/String;

    invoke-direct {v3, v4, v5, p1, p2}, LX/4n7;-><init>(Landroid/net/Uri;LX/4n3;LX/4n5;Ljava/lang/String;)V

    iput-object v3, v2, LX/4n9;->k:LX/4n7;

    .line 1771325
    :goto_2
    iget-object v3, v2, LX/4n9;->k:LX/4n7;

    goto :goto_1

    .line 1771326
    :cond_4
    iget-object v3, v2, LX/4n9;->l:LX/4n7;

    if-nez v3, :cond_5

    .line 1771327
    new-instance v3, LX/4n7;

    iget-object v4, v2, LX/4n9;->a:Landroid/net/Uri;

    iget-object v5, v2, LX/4n9;->b:LX/4n3;

    iget-object p1, v2, LX/4n9;->f:LX/4n5;

    const/4 p2, 0x0

    invoke-direct {v3, v4, v5, p1, p2}, LX/4n7;-><init>(Landroid/net/Uri;LX/4n3;LX/4n5;Ljava/lang/String;)V

    iput-object v3, v2, LX/4n9;->l:LX/4n7;

    .line 1771328
    :cond_5
    iget-object v3, v2, LX/4n9;->l:LX/4n7;

    move-object v3, v3

    .line 1771329
    iput-object v3, v2, LX/4n9;->k:LX/4n7;

    goto :goto_2
.end method

.method private static a(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;LX/9iZ;LX/9iq;LX/75F;LX/8JL;LX/8HH;LX/0Ot;LX/8I2;LX/74G;LX/BIk;LX/9ix;LX/75Q;LX/4mV;LX/0ad;LX/1HI;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;",
            "LX/9iZ;",
            "LX/9iq;",
            "LX/75F;",
            "LX/8JL;",
            "LX/8HH;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/facerec/manager/FaceBoxPrioritizer;",
            ">;",
            "Lcom/facebook/photos/local/LocalMediaCursor;",
            "LX/74G;",
            "LX/BIk;",
            "LX/9ix;",
            "LX/75Q;",
            "Lcom/facebook/ui/animations/ViewAnimatorFactory;",
            "LX/0ad;",
            "LX/1HI;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1771330
    iput-object p1, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->a:LX/9iZ;

    iput-object p2, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->b:LX/9iq;

    iput-object p3, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->c:LX/75F;

    iput-object p4, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->d:LX/8JL;

    iput-object p5, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->e:LX/8HH;

    iput-object p6, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->f:LX/0Ot;

    iput-object p7, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->g:LX/8I2;

    iput-object p8, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->h:LX/74G;

    iput-object p9, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->i:LX/BIk;

    iput-object p10, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->j:LX/9ix;

    iput-object p11, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->k:LX/75Q;

    iput-object p12, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->l:LX/4mV;

    iput-object p13, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->m:LX/0ad;

    iput-object p14, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->n:LX/1HI;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 15

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v14

    move-object v0, p0

    check-cast v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-static {v14}, LX/9iZ;->b(LX/0QB;)LX/9iZ;

    move-result-object v1

    check-cast v1, LX/9iZ;

    invoke-static {v14}, LX/9iq;->c(LX/0QB;)LX/9iq;

    move-result-object v2

    check-cast v2, LX/9iq;

    invoke-static {v14}, LX/75F;->a(LX/0QB;)LX/75F;

    move-result-object v3

    check-cast v3, LX/75F;

    invoke-static {v14}, LX/8JL;->a(LX/0QB;)LX/8JL;

    move-result-object v4

    check-cast v4, LX/8JL;

    invoke-static {v14}, LX/8HH;->a(LX/0QB;)LX/8HH;

    move-result-object v5

    check-cast v5, LX/8HH;

    const/16 v6, 0x1c2d

    invoke-static {v14, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v14}, LX/8I3;->b(LX/0QB;)LX/8I2;

    move-result-object v7

    check-cast v7, LX/8I2;

    invoke-static {v14}, LX/74G;->b(LX/0QB;)LX/74G;

    move-result-object v8

    check-cast v8, LX/74G;

    const-class v9, LX/BIk;

    invoke-interface {v14, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/BIk;

    const-class v10, LX/9ix;

    invoke-interface {v14, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/9ix;

    invoke-static {v14}, LX/75Q;->a(LX/0QB;)LX/75Q;

    move-result-object v11

    check-cast v11, LX/75Q;

    invoke-static {v14}, LX/4mV;->a(LX/0QB;)LX/4mV;

    move-result-object v12

    check-cast v12, LX/4mV;

    invoke-static {v14}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v13

    check-cast v13, LX/0ad;

    invoke-static {v14}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v14

    check-cast v14, LX/1HI;

    invoke-static/range {v0 .. v14}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->a(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;LX/9iZ;LX/9iq;LX/75F;LX/8JL;LX/8HH;LX/0Ot;LX/8I2;LX/74G;LX/BIk;LX/9ix;LX/75Q;LX/4mV;LX/0ad;LX/1HI;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;LX/BJ1;)V
    .locals 3

    .prologue
    .line 1771331
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->t:Z

    .line 1771332
    iput-object p1, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->u:LX/BJ1;

    .line 1771333
    sget-object v0, LX/BIn;->b:[I

    invoke-virtual {p1}, LX/BJ1;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1771334
    :goto_0
    return-void

    .line 1771335
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->h:LX/74G;

    .line 1771336
    sget-object v1, LX/74F;->ENTER_TAGGING_MODE:LX/74F;

    invoke-static {v1}, LX/74G;->a(LX/74F;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-static {v0, v1}, LX/74G;->a(LX/74G;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1771337
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->M:Lcom/facebook/photos/taggablegallery/PhotoGallery;

    invoke-virtual {v0}, Lcom/facebook/photos/taggablegallery/PhotoGallery;->getPhotoViewsInPager()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9iQ;

    move-object v1, v0

    .line 1771338
    check-cast v1, LX/9ip;

    invoke-virtual {v1}, LX/9ip;->j()V

    .line 1771339
    check-cast v0, LX/9ip;

    invoke-virtual {v0}, LX/9ip;->n()V

    goto :goto_1

    .line 1771340
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->B:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->D:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1771341
    invoke-static {p0}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->t(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V

    goto :goto_0

    .line 1771342
    :pswitch_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 1771343
    iget-object v1, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->C:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setGlyphColor(I)V

    .line 1771344
    iget-object v1, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->C:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setTextColor(I)V

    .line 1771345
    invoke-static {p0}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->t(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private b(Landroid/view/View;)V
    .locals 10

    .prologue
    .line 1771346
    const v0, 0x7f0d2e85

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/tagging/shared/TagTypeahead;

    iput-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->X:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    .line 1771347
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->d:LX/8JL;

    invoke-virtual {v0}, LX/8JL;->b()V

    .line 1771348
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->X:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    new-instance v1, LX/BIt;

    invoke-direct {v1, p0}, LX/BIt;-><init>(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V

    .line 1771349
    iput-object v1, v0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->A:LX/8JS;

    .line 1771350
    iget-object v1, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->j:LX/9ix;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, LX/BIz;

    invoke-direct {v3, p0}, LX/BIz;-><init>(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V

    iget-object v4, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->X:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    iget v5, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->Y:I

    iget-wide v6, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->Z:J

    iget-object v8, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->aa:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    invoke-virtual/range {v1 .. v8}, LX/9ix;->a(Landroid/content/Context;LX/BIz;Lcom/facebook/photos/tagging/shared/TagTypeahead;IJLcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;)LX/9iw;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->T:LX/9iw;

    .line 1771351
    invoke-static {p0}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->l(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V

    .line 1771352
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->T:LX/9iw;

    new-instance v1, LX/BJ0;

    invoke-direct {v1, p0}, LX/BJ0;-><init>(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V

    .line 1771353
    iput-object v1, v0, LX/9iw;->k:LX/BJ0;

    .line 1771354
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    new-instance v1, LX/BIx;

    invoke-direct {v1, p0}, LX/BIx;-><init>(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V

    .line 1771355
    iput-object v1, v0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->n:LX/7yZ;

    .line 1771356
    const v0, 0x7f0d2e84

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->A:Lcom/facebook/resources/ui/FbTextView;

    .line 1771357
    return-void
.end method

.method public static synthetic b(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;LX/9iQ;I)V
    .locals 13

    .prologue
    .line 1771358
    const/4 v8, 0x0

    .line 1771359
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->P:LX/BIf;

    sget-object v1, LX/BIf;->COMPOSER:LX/BIf;

    if-ne v0, v1, :cond_0

    .line 1771360
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->U:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08135f

    const v3, 0x7f081360

    iget-object v4, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->Q:LX/BIi;

    invoke-interface {v4}, LX/BIi;->a()I

    move-result v4

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    add-int/lit8 v6, p2, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->Q:LX/BIi;

    invoke-interface {v7}, LX/BIi;->a()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v1, v2, v3, v4, v5}, LX/1z0;->a(Landroid/content/res/Resources;III[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(Ljava/lang/String;)V

    .line 1771361
    :cond_0
    add-int/lit8 v0, p2, -0x1

    iget v1, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->F:I

    if-ne v0, v1, :cond_9

    .line 1771362
    add-int/lit8 v0, p2, 0x2

    add-int/lit8 v1, p2, 0x2

    add-int/lit8 v1, v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->a(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;II)V

    .line 1771363
    :cond_1
    :goto_0
    iget v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->F:I

    const/4 v1, -0x2

    if-eq v0, v1, :cond_2

    iget v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->F:I

    if-eq v0, p2, :cond_2

    .line 1771364
    iget v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->F:I

    if-le v0, p2, :cond_7

    .line 1771365
    iget v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->G:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->G:I

    .line 1771366
    :cond_2
    :goto_1
    iget v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->F:I

    if-lt p2, v0, :cond_8

    sget-object v0, LX/BIy;->NEXT:LX/BIy;

    move-object v1, v0

    .line 1771367
    :goto_2
    iput p2, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->F:I

    .line 1771368
    iget-boolean v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->q:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->c:LX/75F;

    invoke-static {p0}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->p(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)Lcom/facebook/photos/base/photos/LocalPhoto;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/75F;->b(LX/74x;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1771369
    invoke-static {p0}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->t(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V

    .line 1771370
    :cond_3
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->S:LX/9ip;

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->w:Z

    if-eqz v0, :cond_4

    .line 1771371
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->S:LX/9ip;

    invoke-virtual {v0}, LX/9ip;->l()V

    :cond_4
    move-object v0, p1

    .line 1771372
    check-cast v0, LX/9ip;

    iput-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->S:LX/9ip;

    .line 1771373
    iget-boolean v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->w:Z

    if-eqz v0, :cond_6

    move-object v0, p1

    .line 1771374
    check-cast v0, LX/9ip;

    .line 1771375
    iget-object v9, v0, LX/9ip;->l:Lcom/facebook/performancelogger/PerformanceLogger;

    if-eqz v9, :cond_5

    iget-object v9, v0, LX/9ip;->m:LX/0Yj;

    if-nez v9, :cond_b

    .line 1771376
    :cond_5
    :goto_3
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1771377
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->M:Lcom/facebook/photos/taggablegallery/PhotoGallery;

    invoke-virtual {v0}, Lcom/facebook/photos/taggablegallery/PhotoGallery;->getPhotoViewsInPager()Ljava/util/List;

    move-result-object v5

    .line 1771378
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1771379
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    .line 1771380
    const/4 v0, 0x0

    :goto_4
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_12

    .line 1771381
    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-ne v3, p1, :cond_11

    .line 1771382
    :goto_5
    move v4, v0

    .line 1771383
    if-gez v4, :cond_c

    move-object v0, v2

    .line 1771384
    :goto_6
    move-object v1, v0

    .line 1771385
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    invoke-virtual {v0, v1}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->a(Ljava/util/List;)V

    .line 1771386
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->p:Lcom/facebook/photos/base/tagging/FaceBox;

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->t:Z

    if-eqz v0, :cond_6

    move-object v0, p1

    .line 1771387
    check-cast v0, LX/9ip;

    .line 1771388
    iget-object v1, v0, LX/9iQ;->b:LX/74w;

    move-object v0, v1

    .line 1771389
    check-cast v0, LX/74x;

    .line 1771390
    new-instance v1, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment$12;

    invoke-direct {v1, p0, v0, p1}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment$12;-><init>(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;LX/74x;LX/9iQ;)V

    invoke-virtual {p1, v1, v8}, LX/9iQ;->a(Ljava/lang/Runnable;Z)V

    .line 1771391
    :cond_6
    return-void

    .line 1771392
    :cond_7
    iget v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->H:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->H:I

    goto/16 :goto_1

    .line 1771393
    :cond_8
    sget-object v0, LX/BIy;->PREVIOUS:LX/BIy;

    move-object v1, v0

    goto/16 :goto_2

    .line 1771394
    :cond_9
    add-int/lit8 v0, p2, 0x1

    iget v1, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->F:I

    if-ne v0, v1, :cond_a

    .line 1771395
    add-int/lit8 v0, p2, -0x2

    add-int/lit8 v1, p2, -0x2

    add-int/lit8 v1, v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->a(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;II)V

    goto/16 :goto_0

    .line 1771396
    :cond_a
    iget v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->F:I

    if-eq p2, v0, :cond_1

    iget v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->F:I

    const/4 v1, -0x2

    if-eq v0, v1, :cond_1

    .line 1771397
    add-int/lit8 v0, p2, -0x2

    add-int/lit8 v1, p2, 0x2

    add-int/lit8 v1, v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->a(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;II)V

    goto/16 :goto_0

    .line 1771398
    :cond_b
    iget-object v9, v0, LX/9ip;->l:Lcom/facebook/performancelogger/PerformanceLogger;

    iget-object v10, v0, LX/9ip;->m:LX/0Yj;

    const/4 v11, 0x0

    invoke-interface {v9, v10, v11}, Lcom/facebook/performancelogger/PerformanceLogger;->a(LX/0Yj;Z)V

    .line 1771399
    iget-object v10, v0, LX/9ip;->v:LX/75F;

    .line 1771400
    iget-object v9, v0, LX/9iQ;->b:LX/74w;

    move-object v9, v9

    .line 1771401
    check-cast v9, LX/74x;

    invoke-virtual {v10, v9}, LX/75F;->c(LX/74x;)Z

    move-result v9

    if-eqz v9, :cond_5

    iget-object v9, v0, LX/9ip;->l:Lcom/facebook/performancelogger/PerformanceLogger;

    iget-object v10, v0, LX/9ip;->m:LX/0Yj;

    invoke-interface {v9, v10}, Lcom/facebook/performancelogger/PerformanceLogger;->e(LX/0Yj;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1771402
    iget-object v9, v0, LX/9ip;->l:Lcom/facebook/performancelogger/PerformanceLogger;

    iget-object v10, v0, LX/9ip;->m:LX/0Yj;

    const-wide/high16 v11, 0x3ff0000000000000L    # 1.0

    invoke-interface {v9, v10, v11, v12}, Lcom/facebook/performancelogger/PerformanceLogger;->a(LX/0Yj;D)V

    goto/16 :goto_3

    .line 1771403
    :cond_c
    sget-object v0, LX/BIy;->NEXT:LX/BIy;

    if-ne v1, v0, :cond_e

    move v3, v4

    .line 1771404
    :goto_7
    if-ge v3, v6, :cond_d

    .line 1771405
    new-instance v7, LX/7ye;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9iQ;

    invoke-static {p0, v0}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->c(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;LX/9iQ;)Lcom/facebook/photos/base/media/PhotoItem;

    move-result-object v0

    iget-object v9, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->k:LX/75Q;

    iget-object v10, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->c:LX/75F;

    invoke-direct {v7, v0, v9, v10}, LX/7ye;-><init>(Lcom/facebook/photos/base/media/PhotoItem;LX/75Q;LX/75F;)V

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1771406
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_7

    .line 1771407
    :cond_d
    add-int/lit8 v0, v4, -0x1

    move v3, v0

    :goto_8
    if-ltz v3, :cond_10

    .line 1771408
    new-instance v4, LX/7ye;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9iQ;

    invoke-static {p0, v0}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->c(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;LX/9iQ;)Lcom/facebook/photos/base/media/PhotoItem;

    move-result-object v0

    iget-object v6, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->k:LX/75Q;

    iget-object v7, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->c:LX/75F;

    invoke-direct {v4, v0, v6, v7}, LX/7ye;-><init>(Lcom/facebook/photos/base/media/PhotoItem;LX/75Q;LX/75F;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1771409
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_8

    :cond_e
    move v3, v4

    .line 1771410
    :goto_9
    if-ltz v3, :cond_f

    .line 1771411
    new-instance v7, LX/7ye;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9iQ;

    invoke-static {p0, v0}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->c(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;LX/9iQ;)Lcom/facebook/photos/base/media/PhotoItem;

    move-result-object v0

    iget-object v9, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->k:LX/75Q;

    iget-object v10, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->c:LX/75F;

    invoke-direct {v7, v0, v9, v10}, LX/7ye;-><init>(Lcom/facebook/photos/base/media/PhotoItem;LX/75Q;LX/75F;)V

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1771412
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_9

    .line 1771413
    :cond_f
    add-int/lit8 v0, v4, 0x1

    move v3, v0

    :goto_a
    if-ge v3, v6, :cond_10

    .line 1771414
    new-instance v4, LX/7ye;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9iQ;

    invoke-static {p0, v0}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->c(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;LX/9iQ;)Lcom/facebook/photos/base/media/PhotoItem;

    move-result-object v0

    iget-object v7, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->k:LX/75Q;

    iget-object v9, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->c:LX/75F;

    invoke-direct {v4, v0, v7, v9}, LX/7ye;-><init>(Lcom/facebook/photos/base/media/PhotoItem;LX/75Q;LX/75F;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1771415
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_a

    :cond_10
    move-object v0, v2

    .line 1771416
    goto/16 :goto_6

    .line 1771417
    :cond_11
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_4

    .line 1771418
    :cond_12
    const/4 v0, -0x1

    goto/16 :goto_5
.end method

.method public static c(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;LX/9iQ;)Lcom/facebook/photos/base/media/PhotoItem;
    .locals 2

    .prologue
    .line 1771419
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->Q:LX/BIi;

    invoke-static {p1}, Lcom/facebook/photos/taggablegallery/PhotoGallery;->a(LX/9iQ;)I

    move-result v1

    invoke-interface {v0, v1}, LX/BIi;->a(I)Lcom/facebook/photos/taggablegallery/PhotoGalleryContent;

    move-result-object v0

    .line 1771420
    iget-object v1, v0, Lcom/facebook/photos/taggablegallery/PhotoGalleryContent;->a:Lcom/facebook/photos/base/media/PhotoItem;

    move-object v0, v1

    .line 1771421
    return-object v0
.end method

.method public static e(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)Z
    .locals 1

    .prologue
    .line 1771422
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->M:Lcom/facebook/photos/taggablegallery/PhotoGallery;

    invoke-virtual {v0}, Lcom/facebook/photos/taggablegallery/PhotoGallery;->getCurrentPhotoView()Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/9iQ;

    .line 1771423
    iget-object p0, v0, LX/9iQ;->c:Lcom/facebook/widget/images/UrlImage;

    invoke-virtual {p0}, Lcom/facebook/widget/images/UrlImage;->getImageDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p0

    if-eqz p0, :cond_1

    invoke-virtual {v0}, LX/9iQ;->b()Z

    move-result p0

    if-nez p0, :cond_1

    const/4 p0, 0x1

    :goto_0
    move v0, p0

    .line 1771424
    if-eqz v0, :cond_0

    .line 1771425
    const/4 v0, 0x1

    .line 1771426
    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static l(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V
    .locals 2

    .prologue
    .line 1771427
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->aa:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->u:LX/BJ1;

    sget-object v1, LX/BJ1;->PRODUCT_TAGGING:LX/BJ1;

    if-ne v0, v1, :cond_1

    .line 1771428
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->X:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    new-instance v1, LX/BIv;

    invoke-direct {v1, p0}, LX/BIv;-><init>(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->a(LX/8JX;)V

    .line 1771429
    :goto_0
    return-void

    .line 1771430
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->e:LX/8HH;

    new-instance v1, LX/BIw;

    invoke-direct {v1, p0}, LX/BIw;-><init>(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V

    invoke-virtual {v0, v1}, LX/8HH;->a(LX/7yX;)V

    goto :goto_0
.end method

.method public static n(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V
    .locals 5

    .prologue
    const v3, -0x777778

    .line 1771431
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->h:LX/74G;

    .line 1771432
    sget-object v1, LX/74F;->EXIT_TAGGING_MODE:LX/74F;

    invoke-static {v1}, LX/74G;->a(LX/74F;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-static {v0, v1}, LX/74G;->a(LX/74G;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1771433
    invoke-static {p0}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->u(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V

    .line 1771434
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->t:Z

    .line 1771435
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->M:Lcom/facebook/photos/taggablegallery/PhotoGallery;

    invoke-virtual {v0}, Lcom/facebook/photos/taggablegallery/PhotoGallery;->getPhotoViewsInPager()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9iQ;

    move-object v1, v0

    .line 1771436
    check-cast v1, LX/9ip;

    .line 1771437
    invoke-virtual {v1}, LX/9ip;->l()V

    .line 1771438
    const/4 v4, 0x1

    invoke-static {v1, v4}, LX/9ip;->c(LX/9ip;Z)V

    .line 1771439
    check-cast v0, LX/9ip;

    invoke-virtual {v0}, LX/9ip;->o()V

    goto :goto_0

    .line 1771440
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->w:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->u:LX/BJ1;

    if-nez v0, :cond_2

    .line 1771441
    :cond_1
    :goto_1
    return-void

    .line 1771442
    :cond_2
    sget-object v0, LX/BIn;->b:[I

    iget-object v1, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->u:LX/BJ1;

    invoke-virtual {v1}, LX/BJ1;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_1

    .line 1771443
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->B:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->E:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 1771444
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->C:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setGlyphColor(I)V

    .line 1771445
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->C:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setTextColor(I)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static o(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)Lcom/facebook/photos/base/media/PhotoItem;
    .locals 2

    .prologue
    .line 1771278
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->Q:LX/BIi;

    iget-object v1, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->M:Lcom/facebook/photos/taggablegallery/PhotoGallery;

    invoke-virtual {v1}, Lcom/facebook/photos/taggablegallery/PhotoGallery;->getCurrentPosition()I

    move-result v1

    invoke-interface {v0, v1}, LX/BIi;->a(I)Lcom/facebook/photos/taggablegallery/PhotoGalleryContent;

    move-result-object v0

    .line 1771279
    iget-object v1, v0, Lcom/facebook/photos/taggablegallery/PhotoGalleryContent;->a:Lcom/facebook/photos/base/media/PhotoItem;

    move-object v0, v1

    .line 1771280
    return-object v0
.end method

.method public static p(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)Lcom/facebook/photos/base/photos/LocalPhoto;
    .locals 1

    .prologue
    .line 1771281
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->M:Lcom/facebook/photos/taggablegallery/PhotoGallery;

    .line 1771282
    iget-object p0, v0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->g:LX/74w;

    move-object v0, p0

    .line 1771283
    check-cast v0, Lcom/facebook/photos/base/photos/LocalPhoto;

    return-object v0
.end method

.method public static q(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)LX/9ip;
    .locals 1

    .prologue
    .line 1771156
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->M:Lcom/facebook/photos/taggablegallery/PhotoGallery;

    invoke-virtual {v0}, Lcom/facebook/photos/taggablegallery/PhotoGallery;->getCurrentPhotoView()Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/9ip;

    return-object v0
.end method

.method public static r$redex0(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V
    .locals 1

    .prologue
    .line 1771162
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->W:LX/8Hs;

    invoke-virtual {v0}, LX/8Hs;->d()V

    .line 1771163
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->V:LX/8Hs;

    invoke-virtual {v0}, LX/8Hs;->d()V

    .line 1771164
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->s:Z

    .line 1771165
    return-void
.end method

.method public static s$redex0(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V
    .locals 1

    .prologue
    .line 1771166
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->W:LX/8Hs;

    invoke-virtual {v0}, LX/8Hs;->c()V

    .line 1771167
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->V:LX/8Hs;

    invoke-virtual {v0}, LX/8Hs;->c()V

    .line 1771168
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->s:Z

    .line 1771169
    return-void
.end method

.method public static t(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V
    .locals 2

    .prologue
    .line 1771170
    iget-boolean v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->t:Z

    if-eqz v0, :cond_0

    .line 1771171
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->A:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1771172
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->q:Z

    .line 1771173
    :cond_0
    return-void
.end method

.method public static u(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V
    .locals 2

    .prologue
    .line 1771174
    iget-boolean v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->t:Z

    if-eqz v0, :cond_0

    .line 1771175
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->A:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1771176
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/BIi;Ljava/util/ArrayList;Lcom/facebook/ipc/media/MediaIdKey;ZZLX/BH7;LX/BIf;Ljava/lang/String;Lcom/facebook/photos/base/tagging/FaceBox;IJLcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;Z)V
    .locals 1
    .param p9    # Lcom/facebook/photos/base/tagging/FaceBox;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p13    # Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BIi;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/photos/creativeediting/model/CreativeEditingData;",
            ">;",
            "Lcom/facebook/ipc/media/MediaIdKey;",
            "ZZ",
            "LX/BH7;",
            "LX/BIf;",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/base/tagging/FaceBox;",
            "IJ",
            "Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 1771177
    iput-object p1, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->Q:LX/BIi;

    .line 1771178
    iput-object p2, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->R:Ljava/util/ArrayList;

    .line 1771179
    iput-object p3, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->K:Lcom/facebook/ipc/media/MediaIdKey;

    .line 1771180
    iput-boolean p4, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->x:Z

    .line 1771181
    iput-boolean p5, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->y:Z

    .line 1771182
    iget-boolean v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->x:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->y:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->w:Z

    .line 1771183
    iput-object p6, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->L:LX/BH7;

    .line 1771184
    iput-object p7, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->P:LX/BIf;

    .line 1771185
    iput-object p8, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->O:Ljava/lang/String;

    .line 1771186
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->v:Z

    .line 1771187
    iput-object p9, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->p:Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1771188
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->G:I

    .line 1771189
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->H:I

    .line 1771190
    iput p10, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->Y:I

    .line 1771191
    iput-wide p11, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->Z:J

    .line 1771192
    iput-object p13, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->aa:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    .line 1771193
    iput-boolean p14, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->ab:Z

    .line 1771194
    return-void

    .line 1771195
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 1771196
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1771197
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1771198
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->Q:LX/BIi;

    if-nez v0, :cond_0

    .line 1771199
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->g:LX/8I2;

    sget-object v1, LX/4gI;->PHOTO_ONLY_EXCLUDING_GIFS:LX/4gI;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/8I2;->a(LX/4gI;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->z:Landroid/database/Cursor;

    .line 1771200
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->i:LX/BIk;

    iget-object v1, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->z:Landroid/database/Cursor;

    .line 1771201
    new-instance v4, LX/BIj;

    invoke-static {v0}, LX/8I4;->b(LX/0QB;)LX/8I4;

    move-result-object v2

    check-cast v2, LX/8I4;

    invoke-static {v0}, LX/8I3;->b(LX/0QB;)LX/8I2;

    move-result-object v3

    check-cast v3, LX/8I2;

    invoke-direct {v4, v1, v2, v3}, LX/BIj;-><init>(Landroid/database/Cursor;LX/8I4;LX/8I2;)V

    .line 1771202
    move-object v0, v4

    .line 1771203
    iput-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->Q:LX/BIi;

    .line 1771204
    :cond_0
    if-eqz p1, :cond_1

    .line 1771205
    const-string v0, "start_photo_id_in_gallery"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaIdKey;

    iput-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->K:Lcom/facebook/ipc/media/MediaIdKey;

    .line 1771206
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->Q:LX/BIi;

    iget-object v1, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->K:Lcom/facebook/ipc/media/MediaIdKey;

    invoke-interface {v0, v1}, LX/BIi;->a(Lcom/facebook/ipc/media/MediaIdKey;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->I:I

    .line 1771207
    return-void
.end method

.method public final a(Lcom/facebook/ipc/media/MediaIdKey;ZLX/BH7;LX/BIf;Ljava/lang/String;)V
    .locals 16

    .prologue
    .line 1771208
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const-wide/16 v12, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v1, p0

    move-object/from16 v4, p1

    move/from16 v5, p2

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    invoke-virtual/range {v1 .. v15}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->a(LX/BIi;Ljava/util/ArrayList;Lcom/facebook/ipc/media/MediaIdKey;ZZLX/BH7;LX/BIf;Ljava/lang/String;Lcom/facebook/photos/base/tagging/FaceBox;IJLcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;Z)V

    .line 1771209
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 1771210
    iget-boolean v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->w:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->T:LX/9iw;

    invoke-virtual {v0}, LX/9iw;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1771211
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->T:LX/9iw;

    invoke-virtual {v0}, LX/9iw;->b()V

    .line 1771212
    :goto_0
    sget-object v0, LX/BIn;->a:[I

    iget-object v1, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->P:LX/BIf;

    invoke-virtual {v1}, LX/BIf;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1771213
    :goto_1
    const/4 v0, 0x1

    return v0

    .line 1771214
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->L:LX/BH7;

    invoke-static {p0}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->o(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)Lcom/facebook/photos/base/media/PhotoItem;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/BH7;->a(Lcom/facebook/photos/base/media/PhotoItem;Z)V

    goto :goto_0

    .line 1771215
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->h:LX/74G;

    invoke-virtual {v0}, LX/74G;->b()V

    goto :goto_1

    .line 1771216
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->h:LX/74G;

    invoke-virtual {v0}, LX/74G;->c()V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 1771217
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1771218
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-static {p0, v0}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->a(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;I)V

    .line 1771219
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x3a0bd16c

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1771220
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->h:LX/74G;

    iget-object v2, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->O:Ljava/lang/String;

    .line 1771221
    iput-object v2, v0, LX/74G;->a:Ljava/lang/String;

    .line 1771222
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->h:LX/74G;

    invoke-virtual {v0}, LX/74G;->a()V

    .line 1771223
    const v0, 0x7f03146e

    .line 1771224
    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 1771225
    invoke-direct {p0, v2}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->a(Landroid/view/View;)V

    .line 1771226
    iget-boolean v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->x:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->y:Z

    if-eqz v0, :cond_1

    .line 1771227
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->x:Z

    if-eqz v0, :cond_2

    .line 1771228
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->a:LX/9iZ;

    invoke-virtual {v0}, LX/9iZ;->b()V

    .line 1771229
    sget-object v0, LX/BJ1;->FRIEND_TAGGING:LX/BJ1;

    iput-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->u:LX/BJ1;

    .line 1771230
    :goto_0
    invoke-direct {p0, v2}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->b(Landroid/view/View;)V

    .line 1771231
    :cond_1
    const v0, 0x7f0d2e80

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/taggablegallery/PhotoGallery;

    iput-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->M:Lcom/facebook/photos/taggablegallery/PhotoGallery;

    .line 1771232
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->M:Lcom/facebook/photos/taggablegallery/PhotoGallery;

    .line 1771233
    new-instance v3, LX/BIu;

    invoke-direct {v3, p0}, LX/BIu;-><init>(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V

    move-object v3, v3

    .line 1771234
    invoke-virtual {v0, v3}, Lcom/facebook/photos/taggablegallery/PhotoGallery;->a(LX/BIX;)V

    .line 1771235
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->M:Lcom/facebook/photos/taggablegallery/PhotoGallery;

    iget-object v3, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->Q:LX/BIi;

    iget-object v4, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->b:LX/9iq;

    iget v5, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->I:I

    invoke-virtual {v0, v3, v4, v5}, Lcom/facebook/photos/taggablegallery/PhotoGallery;->a(LX/BIi;LX/9iR;I)V

    .line 1771236
    const v0, 0x7f0d2e82

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->B:Landroid/widget/ImageView;

    .line 1771237
    const v0, 0x7f0d2e83

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    iput-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->C:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    .line 1771238
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f020bdd

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->D:Landroid/graphics/drawable/Drawable;

    .line 1771239
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f020bdd

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->E:Landroid/graphics/drawable/Drawable;

    .line 1771240
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->E:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    new-instance v3, Landroid/graphics/PorterDuffColorFilter;

    const v4, -0x777778

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v3, v4, v5}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 1771241
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->B:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->D:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1771242
    invoke-static {p0}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->s$redex0(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V

    .line 1771243
    iget-boolean v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->x:Z

    if-eqz v0, :cond_3

    .line 1771244
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->B:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1771245
    sget-object v0, LX/BJ1;->FRIEND_TAGGING:LX/BJ1;

    invoke-static {p0, v0}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->a$redex0(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;LX/BJ1;)V

    .line 1771246
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->B:Landroid/widget/ImageView;

    new-instance v3, LX/BIo;

    invoke-direct {v3, p0}, LX/BIo;-><init>(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1771247
    :goto_1
    const v0, -0x65461c8b

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-object v2

    .line 1771248
    :cond_2
    sget-object v0, LX/BJ1;->PRODUCT_TAGGING:LX/BJ1;

    iput-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->u:LX/BJ1;

    goto/16 :goto_0

    .line 1771249
    :cond_3
    iget-boolean v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->y:Z

    if-eqz v0, :cond_4

    .line 1771250
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->C:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, v6}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setVisibility(I)V

    .line 1771251
    sget-object v0, LX/BJ1;->PRODUCT_TAGGING:LX/BJ1;

    invoke-static {p0, v0}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->a$redex0(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;LX/BJ1;)V

    .line 1771252
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->C:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    new-instance v3, LX/BIp;

    invoke-direct {v3, p0}, LX/BIp;-><init>(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 1771253
    :cond_4
    const v0, 0x7f0d2e81

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1771254
    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1771255
    invoke-static {p0}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->n(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V

    goto :goto_1
.end method

.method public final onDestroy()V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x1f35e2b7

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1771256
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1771257
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->h:LX/74G;

    iget v2, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->G:I

    iget v3, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->H:I

    .line 1771258
    sget-object v5, LX/74F;->SWIPE_PHOTOS:LX/74F;

    invoke-static {v5}, LX/74G;->a(LX/74F;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "photo_picker_swipes_to_left_count"

    invoke-virtual {v5, v6, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "photo_picker_swipes_to_right_count"

    invoke-virtual {v5, v6, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 1771259
    invoke-static {v0, v5}, LX/74G;->a(LX/74G;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1771260
    iget-boolean v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->w:Z

    if-eqz v0, :cond_1

    .line 1771261
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->e:LX/8HH;

    .line 1771262
    iget-object v2, v0, LX/8HH;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 1771263
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->P:LX/BIf;

    sget-object v2, LX/BIf;->SIMPLEPICKER:LX/BIf;

    if-eq v0, v2, :cond_0

    .line 1771264
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    invoke-virtual {v0}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->e()V

    .line 1771265
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    const/4 v2, 0x0

    .line 1771266
    iput-object v2, v0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->n:LX/7yZ;

    .line 1771267
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    invoke-virtual {v0}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->b()V

    .line 1771268
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->z:Landroid/database/Cursor;

    if-eqz v0, :cond_2

    .line 1771269
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->z:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1771270
    :cond_2
    const/16 v0, 0x2b

    const v2, 0x6d06cb0d

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x69b12318

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1771271
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1771272
    iget-object v1, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->T:LX/9iw;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->T:LX/9iw;

    invoke-virtual {v1}, LX/9iw;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1771273
    iget-object v1, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->T:LX/9iw;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/9iw;->a(Z)V

    .line 1771274
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x45f8a969

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1771275
    const-string v0, "start_photo_id_in_gallery"

    iget-object v1, p0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->K:Lcom/facebook/ipc/media/MediaIdKey;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1771276
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1771277
    return-void
.end method
