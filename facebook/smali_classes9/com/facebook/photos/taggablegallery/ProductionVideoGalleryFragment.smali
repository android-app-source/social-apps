.class public Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/74G;

.field private b:LX/BIf;

.field private c:Ljava/lang/String;

.field public d:Lcom/facebook/photos/base/media/VideoItem;

.field private e:Landroid/net/Uri;

.field public f:LX/BIT;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1770813
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1770814
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1770805
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1770806
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v0

    .line 1770807
    const-string v0, "source"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BIf;

    iput-object v0, p0, Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryFragment;->b:LX/BIf;

    .line 1770808
    const-string v0, "session_id"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryFragment;->c:Ljava/lang/String;

    .line 1770809
    const-string v0, "video_item"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/media/VideoItem;

    iput-object v0, p0, Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryFragment;->d:Lcom/facebook/photos/base/media/VideoItem;

    .line 1770810
    const-string v0, "video_uri"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryFragment;->e:Landroid/net/Uri;

    .line 1770811
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryFragment;

    invoke-static {v0}, LX/74G;->b(LX/0QB;)LX/74G;

    move-result-object v0

    check-cast v0, LX/74G;

    iput-object v0, p0, Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryFragment;->a:LX/74G;

    .line 1770812
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x5f325650

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1770783
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryFragment;->a:LX/74G;

    iget-object v1, p0, Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryFragment;->c:Ljava/lang/String;

    .line 1770784
    iput-object v1, v0, LX/74G;->a:Ljava/lang/String;

    .line 1770785
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryFragment;->a:LX/74G;

    invoke-virtual {v0}, LX/74G;->a()V

    .line 1770786
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryFragment;->f:LX/BIT;

    invoke-interface {v0, p1, p2}, LX/BIT;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 1770787
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryFragment;->f:LX/BIT;

    invoke-interface {v0}, LX/BIT;->a()Landroid/view/ViewGroup;

    move-result-object v0

    .line 1770788
    const v1, 0x7f03060a

    invoke-virtual {p1, v1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1770789
    const v0, 0x7f0d00bc

    invoke-static {v3, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 1770790
    new-instance v1, LX/BIb;

    invoke-direct {v1, p0}, LX/BIb;-><init>(Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 1770791
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v4

    iget-object v1, p0, Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryFragment;->b:LX/BIf;

    sget-object v5, LX/BIf;->COMPOSER:LX/BIf;

    if-ne v1, v5, :cond_1

    const v1, 0x7f08134f

    :goto_0
    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1770792
    iput-object v1, v4, LX/108;->g:Ljava/lang/String;

    .line 1770793
    move-object v1, v4

    .line 1770794
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    .line 1770795
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setButtonSpecs(Ljava/util/List;)V

    .line 1770796
    new-instance v1, LX/BIc;

    invoke-direct {v1, p0}, LX/BIc;-><init>(Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryFragment;)V

    move-object v1, v1

    .line 1770797
    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setOnToolbarButtonListener(LX/63W;)V

    .line 1770798
    const v1, 0x7f08135b

    .line 1770799
    iget-object v4, p0, Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryFragment;->b:LX/BIf;

    sget-object v5, LX/BIf;->COMPOSER:LX/BIf;

    if-ne v4, v5, :cond_0

    .line 1770800
    iget-object v1, p0, Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryFragment;->d:Lcom/facebook/photos/base/media/VideoItem;

    invoke-static {v1}, LX/74c;->a(Lcom/facebook/ipc/media/MediaItem;)Z

    move-result v1

    if-eqz v1, :cond_2

    const v1, 0x7f08135e

    .line 1770801
    :cond_0
    :goto_1
    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(Ljava/lang/String;)V

    .line 1770802
    const v0, -0x447350c5

    invoke-static {v0, v2}, LX/02F;->f(II)V

    return-object v3

    .line 1770803
    :cond_1
    const v1, 0x7f08135a

    goto :goto_0

    .line 1770804
    :cond_2
    const v1, 0x7f08135d

    goto :goto_1
.end method

.method public final onDestroy()V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x1a634ce9

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1770815
    sget-object v1, LX/BId;->a:[I

    iget-object v2, p0, Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryFragment;->b:LX/BIf;

    invoke-virtual {v2}, LX/BIf;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1770816
    :goto_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1770817
    const v1, -0x6f7767a9

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void

    .line 1770818
    :pswitch_0
    iget-object v1, p0, Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryFragment;->a:LX/74G;

    invoke-virtual {v1}, LX/74G;->b()V

    goto :goto_0

    .line 1770819
    :pswitch_1
    iget-object v1, p0, Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryFragment;->a:LX/74G;

    invoke-virtual {v1}, LX/74G;->c()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5997734

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1770781
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1770782
    const/16 v1, 0x2b

    const v2, 0x101abfd1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x29b35f12

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1770779
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1770780
    const/16 v1, 0x2b

    const v2, 0x7e205ba7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7c4de24f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1770776
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 1770777
    iget-object v1, p0, Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryFragment;->f:LX/BIT;

    iget-object v2, p0, Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryFragment;->e:Landroid/net/Uri;

    invoke-interface {v1, v2}, LX/BIT;->a(Landroid/net/Uri;)V

    .line 1770778
    const/16 v1, 0x2b

    const v2, -0x30d852d9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
