.class public Lcom/facebook/photos/taggablegallery/PhotoGalleryContent;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/taggablegallery/PhotoGalleryContent;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/photos/base/media/PhotoItem;

.field public final b:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1770730
    new-instance v0, LX/BIZ;

    invoke-direct {v0}, LX/BIZ;-><init>()V

    sput-object v0, Lcom/facebook/photos/taggablegallery/PhotoGalleryContent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1770731
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1770732
    const-class v0, Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    iput-object v0, p0, Lcom/facebook/photos/taggablegallery/PhotoGalleryContent;->a:Lcom/facebook/photos/base/media/PhotoItem;

    .line 1770733
    const-class v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    iput-object v0, p0, Lcom/facebook/photos/taggablegallery/PhotoGalleryContent;->b:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1770734
    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/base/media/PhotoItem;Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)V
    .locals 0

    .prologue
    .line 1770735
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1770736
    iput-object p1, p0, Lcom/facebook/photos/taggablegallery/PhotoGalleryContent;->a:Lcom/facebook/photos/base/media/PhotoItem;

    .line 1770737
    iput-object p2, p0, Lcom/facebook/photos/taggablegallery/PhotoGalleryContent;->b:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1770738
    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/photos/base/media/PhotoItem;Lcom/facebook/photos/creativeediting/model/CreativeEditingData;B)V
    .locals 0

    .prologue
    .line 1770739
    invoke-direct {p0, p1, p2}, Lcom/facebook/photos/taggablegallery/PhotoGalleryContent;-><init>(Lcom/facebook/photos/base/media/PhotoItem;Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)V

    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1770740
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1770741
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/PhotoGalleryContent;->a:Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1770742
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/PhotoGalleryContent;->b:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1770743
    return-void
.end method
