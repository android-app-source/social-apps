.class public Lcom/facebook/photos/taggablegallery/PhotoGallery;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# instance fields
.field public a:LX/75S;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/photos/photogallery/ZoomableViewPager;

.field private c:LX/BIY;

.field public d:LX/BIi;

.field public e:LX/9iR;

.field public f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/BIX;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/74w;

.field public h:LX/9iQ;

.field public i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1770715
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 1770716
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->i:I

    .line 1770717
    invoke-direct {p0}, Lcom/facebook/photos/taggablegallery/PhotoGallery;->c()V

    .line 1770718
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1770711
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1770712
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->i:I

    .line 1770713
    invoke-direct {p0}, Lcom/facebook/photos/taggablegallery/PhotoGallery;->c()V

    .line 1770714
    return-void
.end method

.method public static a(LX/9iQ;)I
    .locals 1

    .prologue
    .line 1770710
    invoke-virtual {p0}, LX/9iQ;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/photos/taggablegallery/PhotoGallery;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/photos/taggablegallery/PhotoGallery;

    invoke-static {v0}, LX/75S;->a(LX/0QB;)LX/75S;

    move-result-object v0

    check-cast v0, LX/75S;

    iput-object v0, p0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->a:LX/75S;

    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 1770701
    const-class v0, Lcom/facebook/photos/taggablegallery/PhotoGallery;

    invoke-static {v0, p0}, Lcom/facebook/photos/taggablegallery/PhotoGallery;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1770702
    const v0, 0x7f03146f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1770703
    const v0, 0x7f0d1241

    invoke-virtual {p0, v0}, Lcom/facebook/photos/taggablegallery/PhotoGallery;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/photogallery/ZoomableViewPager;

    iput-object v0, p0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->b:Lcom/facebook/photos/photogallery/ZoomableViewPager;

    .line 1770704
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->b:Lcom/facebook/photos/photogallery/ZoomableViewPager;

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    .line 1770705
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->b:Lcom/facebook/photos/photogallery/ZoomableViewPager;

    new-instance v1, LX/BIW;

    invoke-direct {v1, p0}, LX/BIW;-><init>(Lcom/facebook/photos/taggablegallery/PhotoGallery;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 1770706
    new-instance v0, LX/BIY;

    invoke-direct {v0, p0}, LX/BIY;-><init>(Lcom/facebook/photos/taggablegallery/PhotoGallery;)V

    iput-object v0, p0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->c:LX/BIY;

    .line 1770707
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->b:Lcom/facebook/photos/photogallery/ZoomableViewPager;

    iget-object v1, p0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->c:LX/BIY;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 1770708
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->f:Ljava/util/Set;

    .line 1770709
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 1770696
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BIX;

    .line 1770697
    invoke-interface {v0}, LX/BIX;->a()V

    goto :goto_0

    .line 1770698
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->g:LX/74w;

    .line 1770699
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->c:LX/BIY;

    invoke-virtual {v0}, LX/0gG;->kV_()V

    .line 1770700
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1770672
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->b:Lcom/facebook/photos/photogallery/ZoomableViewPager;

    invoke-virtual {v0}, Lcom/facebook/photos/photogallery/ZoomableViewPager;->g()V

    .line 1770673
    return-void
.end method

.method public final a(LX/BIX;)V
    .locals 1

    .prologue
    .line 1770694
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1770695
    return-void
.end method

.method public final a(LX/BIi;LX/9iR;I)V
    .locals 2

    .prologue
    .line 1770689
    iput-object p1, p0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->d:LX/BIi;

    .line 1770690
    iput-object p2, p0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->e:LX/9iR;

    .line 1770691
    invoke-direct {p0}, Lcom/facebook/photos/taggablegallery/PhotoGallery;->d()V

    .line 1770692
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->b:Lcom/facebook/photos/photogallery/ZoomableViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, p3, v1}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 1770693
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1770687
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->b:Lcom/facebook/photos/photogallery/ZoomableViewPager;

    invoke-virtual {v0}, Lcom/facebook/photos/photogallery/ZoomableViewPager;->h()V

    .line 1770688
    return-void
.end method

.method public getCurrentPhoto()LX/74w;
    .locals 1

    .prologue
    .line 1770686
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->g:LX/74w;

    return-object v0
.end method

.method public getCurrentPhotoView()Landroid/view/View;
    .locals 2

    .prologue
    .line 1770685
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->b:Lcom/facebook/photos/photogallery/ZoomableViewPager;

    iget v1, p0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->i:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/photos/photogallery/ZoomableViewPager;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentPosition()I
    .locals 1

    .prologue
    .line 1770684
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->b:Lcom/facebook/photos/photogallery/ZoomableViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    return v0
.end method

.method public getPhotoViewsInPager()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/9iQ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1770677
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1770678
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->b:Lcom/facebook/photos/photogallery/ZoomableViewPager;

    invoke-virtual {v0}, Lcom/facebook/photos/photogallery/ZoomableViewPager;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1770679
    iget-object v0, p0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->b:Lcom/facebook/photos/photogallery/ZoomableViewPager;

    invoke-virtual {v0, v1}, Lcom/facebook/photos/photogallery/ZoomableViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/9iQ;

    .line 1770680
    iget-object v3, p0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->h:LX/9iQ;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->h:LX/9iQ;

    invoke-static {v3}, Lcom/facebook/photos/taggablegallery/PhotoGallery;->a(LX/9iQ;)I

    move-result v3

    invoke-static {v0}, Lcom/facebook/photos/taggablegallery/PhotoGallery;->a(LX/9iQ;)I

    move-result v4

    sub-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    const/4 v4, 0x1

    if-gt v3, v4, :cond_1

    .line 1770681
    :cond_0
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1770682
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1770683
    :cond_2
    return-object v2
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 1770674
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1770675
    invoke-direct {p0}, Lcom/facebook/photos/taggablegallery/PhotoGallery;->d()V

    .line 1770676
    return-void
.end method
