.class public final Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6b08965f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1862193
    const-class v0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1862192
    const-class v0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1862190
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1862191
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1862182
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1862183
    invoke-virtual {p0}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/util/List;)I

    move-result v0

    .line 1862184
    invoke-virtual {p0}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1862185
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1862186
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1862187
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1862188
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1862189
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1862194
    iget-object v0, p0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel;->e:Ljava/util/List;

    .line 1862195
    iget-object v0, p0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1862174
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1862175
    invoke-virtual {p0}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1862176
    invoke-virtual {p0}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1862177
    if-eqz v1, :cond_0

    .line 1862178
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel;

    .line 1862179
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel;->f:Ljava/util/List;

    .line 1862180
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1862181
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1862171
    new-instance v0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel;

    invoke-direct {v0}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel;-><init>()V

    .line 1862172
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1862173
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1862170
    const v0, 0x6a6dd96

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1862169
    const v0, -0x6747e1ce

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1862167
    iget-object v0, p0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel;->f:Ljava/util/List;

    .line 1862168
    iget-object v0, p0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method
