.class public final Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7515318d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel$BackgroundImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1862110
    const-class v0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1862109
    const-class v0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1862107
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1862108
    return-void
.end method

.method private p()Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel$BackgroundImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1862105
    iget-object v0, p0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->h:Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel$BackgroundImageModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel$BackgroundImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel$BackgroundImageModel;

    iput-object v0, p0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->h:Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel$BackgroundImageModel;

    .line 1862106
    iget-object v0, p0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->h:Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel$BackgroundImageModel;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1862103
    iget-object v0, p0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->j:Ljava/lang/String;

    .line 1862104
    iget-object v0, p0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->j:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    .line 1862081
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1862082
    invoke-virtual {p0}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1862083
    invoke-virtual {p0}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1862084
    invoke-virtual {p0}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1862085
    invoke-direct {p0}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->p()Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel$BackgroundImageModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1862086
    invoke-virtual {p0}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1862087
    invoke-direct {p0}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->q()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1862088
    invoke-virtual {p0}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->m()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1862089
    invoke-virtual {p0}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->n()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1862090
    invoke-virtual {p0}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1862091
    const/16 v9, 0x9

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1862092
    const/4 v9, 0x0

    invoke-virtual {p1, v9, v0}, LX/186;->b(II)V

    .line 1862093
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1862094
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1862095
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1862096
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1862097
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1862098
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1862099
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1862100
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1862101
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1862102
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1862054
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1862055
    invoke-direct {p0}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->p()Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel$BackgroundImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1862056
    invoke-direct {p0}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->p()Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel$BackgroundImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel$BackgroundImageModel;

    .line 1862057
    invoke-direct {p0}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->p()Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel$BackgroundImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1862058
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;

    .line 1862059
    iput-object v0, v1, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->h:Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel$BackgroundImageModel;

    .line 1862060
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1862061
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1862079
    iget-object v0, p0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->e:Ljava/lang/String;

    .line 1862080
    iget-object v0, p0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1862076
    new-instance v0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;

    invoke-direct {v0}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;-><init>()V

    .line 1862077
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1862078
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1862075
    const v0, -0x1467d941

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1862074
    const v0, 0x6e7daaf3

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1862072
    iget-object v0, p0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->f:Ljava/lang/String;

    .line 1862073
    iget-object v0, p0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1862070
    iget-object v0, p0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->g:Ljava/lang/String;

    .line 1862071
    iget-object v0, p0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1862068
    iget-object v0, p0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->i:Ljava/lang/String;

    .line 1862069
    iget-object v0, p0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1862066
    iget-object v0, p0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->k:Ljava/lang/String;

    .line 1862067
    iget-object v0, p0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1862064
    iget-object v0, p0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->l:Ljava/lang/String;

    .line 1862065
    iget-object v0, p0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1862062
    iget-object v0, p0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->m:Ljava/lang/String;

    .line 1862063
    iget-object v0, p0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$ComposerTextFormatPresetsModel;->m:Ljava/lang/String;

    return-object v0
.end method
