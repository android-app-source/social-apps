.class public final Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1862144
    const-class v0, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel;

    new-instance v1, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1862145
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1862143
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1862111
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1862112
    const/4 v2, 0x0

    .line 1862113
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_6

    .line 1862114
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1862115
    :goto_0
    move v1, v2

    .line 1862116
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1862117
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1862118
    new-instance v1, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel;

    invoke-direct {v1}, Lcom/facebook/friendsharing/text/protocol/TextFormatPresetsGraphQLModels$TextFormatPresetsQueryModel;-><init>()V

    .line 1862119
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1862120
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1862121
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1862122
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1862123
    :cond_0
    return-object v1

    .line 1862124
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1862125
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, p0, :cond_5

    .line 1862126
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1862127
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1862128
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v4, :cond_2

    .line 1862129
    const-string p0, "composer_text_format_default_presets"

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1862130
    invoke-static {p1, v0}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1862131
    :cond_3
    const-string p0, "composer_text_format_presets"

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1862132
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1862133
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, p0, :cond_4

    .line 1862134
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, p0, :cond_4

    .line 1862135
    invoke-static {p1, v0}, LX/CEu;->b(LX/15w;LX/186;)I

    move-result v4

    .line 1862136
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1862137
    :cond_4
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 1862138
    goto :goto_1

    .line 1862139
    :cond_5
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1862140
    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1862141
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1862142
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_6
    move v1, v2

    move v3, v2

    goto :goto_1
.end method
