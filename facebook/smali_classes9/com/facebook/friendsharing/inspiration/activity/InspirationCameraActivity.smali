.class public Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f9;


# instance fields
.field private p:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

.field private q:LX/0fO;

.field private r:LX/0tS;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1718586
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1718587
    return-void
.end method

.method private a(LX/0fO;LX/0tS;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1718583
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraActivity;->q:LX/0fO;

    .line 1718584
    iput-object p2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraActivity;->r:LX/0tS;

    .line 1718585
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraActivity;

    invoke-static {v1}, LX/0fO;->b(LX/0QB;)LX/0fO;

    move-result-object v0

    check-cast v0, LX/0fO;

    invoke-static {v1}, LX/0tS;->b(LX/0QB;)LX/0tS;

    move-result-object v1

    check-cast v1, LX/0tS;

    invoke-direct {p0, v0, v1}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraActivity;->a(LX/0fO;LX/0tS;)V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1718563
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1718564
    invoke-static {p0, p0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1718565
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_creativecam_composer_session_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1718566
    const v1, 0x7f030931

    invoke-virtual {p0, v1}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraActivity;->setContentView(I)V

    .line 1718567
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraActivity;->setRequestedOrientation(I)V

    .line 1718568
    if-nez p1, :cond_0

    .line 1718569
    invoke-static {}, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;->newBuilder()Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig$Builder;

    move-result-object v1

    .line 1718570
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraActivity;->r:LX/0tS;

    invoke-virtual {v2}, LX/0tS;->a()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig$Builder;->setAspectRatio(F)Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig$Builder;

    .line 1718571
    invoke-virtual {v1}, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig$Builder;->a()Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;

    move-result-object v1

    .line 1718572
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1718573
    const-string p1, "inspiration_config"

    invoke-virtual {v2, p1, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1718574
    const-string p1, "inspiration_group_session_id"

    invoke-virtual {v2, p1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1718575
    new-instance p1, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    invoke-direct {p1}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;-><init>()V

    .line 1718576
    invoke-virtual {p1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1718577
    move-object v0, p1

    .line 1718578
    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraActivity;->p:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    .line 1718579
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d1783

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraActivity;->p:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    .line 1718580
    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1718581
    :goto_0
    return-void

    .line 1718582
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d1783

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraActivity;->p:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    goto :goto_0
.end method

.method public final mn_()LX/0iO;
    .locals 2

    .prologue
    .line 1718562
    new-instance v0, LX/Aqv;

    invoke-direct {v0, p0}, LX/Aqv;-><init>(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraActivity;)V

    return-object v0
.end method

.method public final mo_()LX/0iH;
    .locals 1

    .prologue
    .line 1718561
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1718556
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1718557
    if-eqz p1, :cond_0

    const/16 v0, 0x3c

    if-ne p1, v0, :cond_1

    .line 1718558
    :cond_0
    invoke-virtual {p0, p2, p3}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraActivity;->setResult(ILandroid/content/Intent;)V

    .line 1718559
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraActivity;->finish()V

    .line 1718560
    :cond_1
    return-void
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 1718553
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraActivity;->p:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraActivity;->p:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    invoke-virtual {v0}, Lcom/facebook/ui/drawers/DrawerContentFragment;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1718554
    :goto_0
    return-void

    .line 1718555
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    goto :goto_0
.end method
