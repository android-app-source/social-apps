.class public final Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment$8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)V
    .locals 0

    .prologue
    .line 1718643
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment$8;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 1718644
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment$8;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-boolean v0, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ap:Z

    if-eqz v0, :cond_0

    .line 1718645
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment$8;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->Z:Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;

    .line 1718646
    iget-object v1, v0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->t:LX/9d5;

    const-string v2, "Must create swipeable controller before running swipe nux"

    invoke-static {v1, v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1718647
    iget-object v1, v0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->i:LX/AuX;

    iget-object v2, v0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->v:LX/0Px;

    .line 1718648
    new-instance v4, LX/AuW;

    .line 1718649
    new-instance v6, LX/AuV;

    const-class v3, Landroid/content/Context;

    invoke-interface {v1, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v1}, LX/8Gc;->b(LX/0QB;)LX/8Gc;

    move-result-object v5

    check-cast v5, LX/8Gc;

    invoke-direct {v6, v3, v5}, LX/AuV;-><init>(Landroid/content/Context;LX/8Gc;)V

    .line 1718650
    move-object v3, v6

    .line 1718651
    check-cast v3, LX/AuV;

    invoke-direct {v4, v2, v3}, LX/AuW;-><init>(LX/0Px;LX/AuV;)V

    .line 1718652
    move-object v1, v4

    .line 1718653
    iget-object v2, v0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->t:LX/9d5;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/9d5;->a(LX/0Px;)V

    .line 1718654
    iget-object v1, v0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->t:LX/9d5;

    .line 1718655
    iget-object v2, v1, LX/9d5;->J:LX/9d9;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/9d9;->a(Z)V

    .line 1718656
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment$8;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    const/4 v1, 0x0

    .line 1718657
    iput-boolean v1, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ap:Z

    .line 1718658
    :cond_0
    return-void
.end method
