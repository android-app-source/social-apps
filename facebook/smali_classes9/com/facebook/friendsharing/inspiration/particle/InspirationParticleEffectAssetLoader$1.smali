.class public final Lcom/facebook/friendsharing/inspiration/particle/InspirationParticleEffectAssetLoader$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;

.field public final synthetic b:LX/89p;

.field public final synthetic c:LX/Aw8;


# direct methods
.method public constructor <init>(LX/Aw8;Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;LX/89p;)V
    .locals 0

    .prologue
    .line 1724972
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/particle/InspirationParticleEffectAssetLoader$1;->c:LX/Aw8;

    iput-object p2, p0, Lcom/facebook/friendsharing/inspiration/particle/InspirationParticleEffectAssetLoader$1;->a:Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;

    iput-object p3, p0, Lcom/facebook/friendsharing/inspiration/particle/InspirationParticleEffectAssetLoader$1;->b:LX/89p;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 15

    .prologue
    .line 1724973
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/particle/InspirationParticleEffectAssetLoader$1;->c:LX/Aw8;

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/particle/InspirationParticleEffectAssetLoader$1;->a:Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/particle/InspirationParticleEffectAssetLoader$1;->b:LX/89p;

    .line 1724974
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 1724975
    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    .line 1724976
    invoke-virtual {v1}, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;->getParticleEffectModel()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;->j()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel$EmittersModel;

    move-result-object v3

    .line 1724977
    if-nez v3, :cond_1

    .line 1724978
    if-eqz v2, :cond_0

    .line 1724979
    invoke-interface {v2}, LX/89p;->a()V

    .line 1724980
    :cond_0
    return-void

    .line 1724981
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel$EmittersModel;->a()LX/0Px;

    move-result-object v3

    .line 1724982
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;

    .line 1724983
    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->d()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$EmitterAssetsModel;

    move-result-object v3

    .line 1724984
    if-eqz v3, :cond_2

    .line 1724985
    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$EmitterAssetsModel;->a()LX/0Px;

    move-result-object v3

    .line 1724986
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_3
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$EmitterAssetsModel$NodesModel;

    .line 1724987
    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$EmitterAssetsModel$NodesModel;->a()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectImageFragmentModel;

    move-result-object v3

    .line 1724988
    if-eqz v3, :cond_3

    .line 1724989
    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectImageFragmentModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1724990
    :cond_4
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    .line 1724991
    const/4 v9, 0x0

    .line 1724992
    iget-object v8, v0, LX/Aw8;->a:LX/1HI;

    invoke-virtual {v8, v3}, LX/1HI;->b(Landroid/net/Uri;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1724993
    const/4 v9, 0x1

    .line 1724994
    :cond_5
    if-nez v9, :cond_6

    .line 1724995
    :try_start_0
    iget-object v8, v0, LX/Aw8;->a:LX/1HI;

    invoke-virtual {v8, v3}, LX/1HI;->d(Landroid/net/Uri;)LX/1ca;

    move-result-object v8

    invoke-static {v8}, LX/24r;->a(LX/1ca;)LX/24r;

    move-result-object v8

    const v10, -0x616e44f

    invoke-static {v8, v10}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    .line 1724996
    :goto_2
    if-eqz v8, :cond_7

    .line 1724997
    invoke-static {v0, v3, v4, v5, v2}, LX/Aw8;->b(LX/Aw8;Landroid/net/Uri;Ljava/util/List;Ljava/util/List;LX/89p;)V

    .line 1724998
    :goto_3
    goto :goto_1

    .line 1724999
    :catch_0
    :goto_4
    iget-object v8, v0, LX/Aw8;->e:LX/03V;

    sget-object v10, LX/Aw8;->f:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Exception while trying to loading image from disk: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, LX/0VG;->b(Ljava/lang/String;Ljava/lang/String;)LX/0VG;

    move-result-object v10

    invoke-virtual {v8, v10}, LX/03V;->a(LX/0VG;)V

    :cond_6
    move v8, v9

    goto :goto_2

    .line 1725000
    :cond_7
    iget-object v8, v0, LX/Aw8;->a:LX/1HI;

    invoke-static {v3}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v9

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-static {v10}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, LX/1HI;->d(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v14

    new-instance v8, LX/2rO;

    move-object v9, v0

    move-object v10, v3

    move-object v11, v4

    move-object v12, v5

    move-object v13, v2

    invoke-direct/range {v8 .. v13}, LX/2rO;-><init>(LX/Aw8;Landroid/net/Uri;Ljava/util/List;Ljava/util/List;LX/89p;)V

    iget-object v9, v0, LX/Aw8;->b:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v14, v8, v9}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    goto :goto_3

    .line 1725001
    :catch_1
    goto :goto_4
.end method
