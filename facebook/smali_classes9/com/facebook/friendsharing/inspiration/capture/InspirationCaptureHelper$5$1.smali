.class public final Lcom/facebook/friendsharing/inspiration/capture/InspirationCaptureHelper$5$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/Arm;


# direct methods
.method public constructor <init>(LX/Arm;)V
    .locals 0

    .prologue
    .line 1719648
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/capture/InspirationCaptureHelper$5$1;->a:LX/Arm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1719649
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/capture/InspirationCaptureHelper$5$1;->a:LX/Arm;

    iget-object v0, v0, LX/Arm;->b:LX/Arq;

    iget-object v0, v0, LX/Arq;->e:LX/Arh;

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/capture/InspirationCaptureHelper$5$1;->a:LX/Arm;

    iget-object v1, v1, LX/Arm;->a:Ljava/io/File;

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 1719650
    iget-object v2, v0, LX/Arh;->p:LX/74n;

    invoke-static {v2, v1}, LX/2rf;->a(LX/74n;Landroid/net/Uri;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    invoke-static {v2}, LX/7kv;->a(Lcom/facebook/ipc/media/MediaItem;)LX/7kv;

    move-result-object v2

    invoke-virtual {v2}, LX/7kv;->a()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v3

    .line 1719651
    iget-object v2, v0, LX/Arh;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0il;

    .line 1719652
    invoke-static {v0}, LX/Arh;->j(LX/Arh;)LX/0jL;

    move-result-object v4

    .line 1719653
    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0iq;

    sget-object p0, LX/875;->CAPTURE:LX/875;

    invoke-static {v4, v2, v3, p0}, LX/2rf;->a(LX/0jL;LX/0iq;Lcom/facebook/composer/attachments/ComposerAttachment;LX/875;)LX/0jL;

    .line 1719654
    invoke-virtual {v4}, LX/0jL;->a()V

    .line 1719655
    iget-object v2, v0, LX/Arh;->m:Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;

    invoke-virtual {v2}, Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;->a()V

    .line 1719656
    return-void
.end method
