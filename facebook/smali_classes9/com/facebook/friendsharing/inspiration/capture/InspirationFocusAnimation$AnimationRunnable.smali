.class public final Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public a:F

.field public b:F

.field public c:LX/0wd;

.field public d:I

.field public e:I

.field public f:LX/0xh;

.field public final synthetic g:LX/Art;


# direct methods
.method public constructor <init>(LX/Art;)V
    .locals 1

    .prologue
    .line 1719879
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->g:LX/Art;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1719880
    new-instance v0, LX/Ars;

    invoke-direct {v0, p0}, LX/Ars;-><init>(Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;)V

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->f:LX/0xh;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 1719866
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->g:LX/Art;

    iget-object v0, v0, LX/Art;->e:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->a:F

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->g:LX/Art;

    iget-object v1, v1, LX/Art;->e:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    sub-float v1, v2, v1

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;->setX(F)V

    .line 1719867
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->g:LX/Art;

    iget-object v0, v0, LX/Art;->e:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->b:F

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->g:LX/Art;

    iget-object v1, v1, LX/Art;->e:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    sub-float v1, v2, v1

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;->setY(F)V

    .line 1719868
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->g:LX/Art;

    iget-object v0, v0, LX/Art;->f:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->a:F

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->g:LX/Art;

    iget-object v1, v1, LX/Art;->f:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    sub-float v1, v2, v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setX(F)V

    .line 1719869
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->g:LX/Art;

    iget-object v0, v0, LX/Art;->f:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->b:F

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->g:LX/Art;

    iget-object v1, v1, LX/Art;->f:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    sub-float v1, v2, v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setY(F)V

    .line 1719870
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->g:LX/Art;

    iget-object v0, v0, LX/Art;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1aa7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->d:I

    .line 1719871
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->g:LX/Art;

    iget-object v0, v0, LX/Art;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1aa8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->e:I

    .line 1719872
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->g:LX/Art;

    iget-object v0, v0, LX/Art;->d:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v1, LX/Art;->a:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    const/4 v1, 0x1

    .line 1719873
    iput-boolean v1, v0, LX/0wd;->c:Z

    .line 1719874
    move-object v0, v0

    .line 1719875
    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->c:LX/0wd;

    .line 1719876
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->c:LX/0wd;

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->f:LX/0xh;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 1719877
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->c:LX/0wd;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1719878
    return-void
.end method
