.class public Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Landroid/net/Uri;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static g:LX/0Xm;


# instance fields
.field public c:LX/1HI;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/1FJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1719907
    const-string v0, "content://dummy_inspiration_capture_uri.jpg"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;->a:Landroid/net/Uri;

    .line 1719908
    const-class v0, Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1719926
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1719927
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;
    .locals 6

    .prologue
    .line 1719913
    const-class v1, Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;

    monitor-enter v1

    .line 1719914
    :try_start_0
    sget-object v0, Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1719915
    sput-object v2, Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1719916
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1719917
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1719918
    new-instance p0, Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;

    invoke-direct {p0}, Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;-><init>()V

    .line 1719919
    invoke-static {v0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v3

    check-cast v3, LX/1HI;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    .line 1719920
    iput-object v3, p0, Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;->c:LX/1HI;

    iput-object v4, p0, Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;->d:Ljava/util/concurrent/ExecutorService;

    iput-object v5, p0, Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;->e:LX/03V;

    .line 1719921
    move-object v0, p0

    .line 1719922
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1719923
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1719924
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1719925
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1719909
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;->f:LX/1FJ;

    if-eqz v0, :cond_0

    .line 1719910
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;->f:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->close()V

    .line 1719911
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;->f:LX/1FJ;

    .line 1719912
    :cond_0
    return-void
.end method
