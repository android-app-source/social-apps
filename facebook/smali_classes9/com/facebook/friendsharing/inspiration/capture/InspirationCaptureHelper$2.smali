.class public final Lcom/facebook/friendsharing/inspiration/capture/InspirationCaptureHelper$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/io/File;

.field public final synthetic b:LX/Arq;


# direct methods
.method public constructor <init>(LX/Arq;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 1719575
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/capture/InspirationCaptureHelper$2;->b:LX/Arq;

    iput-object p2, p0, Lcom/facebook/friendsharing/inspiration/capture/InspirationCaptureHelper$2;->a:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    .line 1719576
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/capture/InspirationCaptureHelper$2;->b:LX/Arq;

    iget-object v0, v0, LX/Arq;->e:LX/Arh;

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/capture/InspirationCaptureHelper$2;->a:Ljava/io/File;

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 1719577
    iget-object v2, v0, LX/Arh;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0il;

    .line 1719578
    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 1719579
    iget-object v3, v0, LX/Arh;->q:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    iget-wide v6, v0, LX/Arh;->z:J

    sub-long/2addr v4, v6

    .line 1719580
    const-wide/16 v6, 0x3e8

    cmp-long v3, v4, v6

    if-gez v3, :cond_0

    .line 1719581
    iget-object v2, v0, LX/Arh;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0kL;

    .line 1719582
    new-instance v3, LX/27k;

    const v4, 0x7f08278b

    invoke-direct {v3, v4}, LX/27k;-><init>(I)V

    invoke-virtual {v2, v3}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 1719583
    invoke-static {v0}, LX/Arh;->j(LX/Arh;)LX/0jL;

    move-result-object v2

    sget-object v3, LX/86q;->READY:LX/86q;

    invoke-static {v0, v2, v3}, LX/Arh;->a(LX/Arh;LX/0jL;LX/86q;)LX/0jL;

    move-result-object v2

    invoke-virtual {v2}, LX/0jL;->a()V

    .line 1719584
    :goto_0
    return-void

    .line 1719585
    :cond_0
    iget-object v3, v0, LX/Arh;->p:LX/74n;

    invoke-static {v3, v1}, LX/2rf;->a(LX/74n;Landroid/net/Uri;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v3

    .line 1719586
    new-instance v6, LX/74m;

    invoke-direct {v6}, LX/74m;-><init>()V

    .line 1719587
    iget-object v7, v3, Lcom/facebook/ipc/media/MediaItem;->c:Lcom/facebook/ipc/media/data/LocalMediaData;

    move-object v7, v7

    .line 1719588
    invoke-virtual {v7}, Lcom/facebook/ipc/media/data/LocalMediaData;->e()LX/4gN;

    move-result-object v7

    invoke-virtual {v3}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/media/data/MediaData;->k()LX/4gP;

    move-result-object v3

    invoke-static {v1}, LX/1kH;->c(Landroid/net/Uri;)I

    move-result v8

    .line 1719589
    iput v8, v3, LX/4gP;->f:I

    .line 1719590
    move-object v3, v3

    .line 1719591
    invoke-static {v1}, LX/1kH;->b(Landroid/net/Uri;)I

    move-result v8

    .line 1719592
    iput v8, v3, LX/4gP;->g:I

    .line 1719593
    move-object v3, v3

    .line 1719594
    invoke-virtual {v3}, LX/4gP;->a()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v3

    invoke-virtual {v7, v3}, LX/4gN;->a(Lcom/facebook/ipc/media/data/MediaData;)LX/4gN;

    move-result-object v3

    invoke-virtual {v3}, LX/4gN;->a()Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v3

    .line 1719595
    iput-object v3, v6, LX/74m;->e:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 1719596
    move-object v3, v6

    .line 1719597
    iput-wide v4, v3, LX/74m;->d:J

    .line 1719598
    move-object v3, v3

    .line 1719599
    invoke-virtual {v3}, LX/74m;->a()Lcom/facebook/photos/base/media/VideoItem;

    move-result-object v3

    .line 1719600
    iget-object v6, v0, LX/Arh;->n:LX/ArL;

    long-to-float v4, v4

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float/2addr v4, v5

    invoke-virtual {v6, v3, v4}, LX/ArL;->a(Lcom/facebook/ipc/media/MediaItem;F)V

    .line 1719601
    iget-object v4, v0, LX/Arh;->o:LX/ArT;

    sget-object v5, LX/ArJ;->CAMERA_CAPTURE:LX/ArJ;

    invoke-virtual {v4, v5}, LX/ArT;->q(LX/ArJ;)V

    .line 1719602
    iget-object v4, v0, LX/Arh;->o:LX/ArT;

    sget-object v5, LX/ArJ;->CAMERA_CAPTURE:LX/ArJ;

    invoke-virtual {v4, v5}, LX/ArT;->c(LX/ArJ;)V

    .line 1719603
    invoke-static {v0}, LX/Arh;->j(LX/Arh;)LX/0jL;

    move-result-object v4

    sget-object v5, LX/86q;->READY:LX/86q;

    invoke-static {v0, v4, v5}, LX/Arh;->a(LX/Arh;LX/0jL;LX/86q;)LX/0jL;

    move-result-object v4

    .line 1719604
    check-cast v2, LX/0iq;

    invoke-static {v3}, LX/7kv;->a(Lcom/facebook/ipc/media/MediaItem;)LX/7kv;

    move-result-object v3

    invoke-virtual {v3}, LX/7kv;->a()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v3

    sget-object v5, LX/875;->CAPTURE:LX/875;

    invoke-static {v4, v2, v3, v5}, LX/2rf;->a(LX/0jL;LX/0iq;Lcom/facebook/composer/attachments/ComposerAttachment;LX/875;)LX/0jL;

    .line 1719605
    invoke-virtual {v4}, LX/0jL;->a()V

    goto :goto_0
.end method
