.class public final Lcom/facebook/friendsharing/inspiration/shaderfilter/InspirationShaderFilterAssetLoader$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

.field public final synthetic b:LX/89p;

.field public final synthetic c:LX/AwI;


# direct methods
.method public constructor <init>(LX/AwI;Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;LX/89p;)V
    .locals 0

    .prologue
    .line 1726335
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/shaderfilter/InspirationShaderFilterAssetLoader$1;->c:LX/AwI;

    iput-object p2, p0, Lcom/facebook/friendsharing/inspiration/shaderfilter/InspirationShaderFilterAssetLoader$1;->a:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    iput-object p3, p0, Lcom/facebook/friendsharing/inspiration/shaderfilter/InspirationShaderFilterAssetLoader$1;->b:LX/89p;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    .line 1726336
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/shaderfilter/InspirationShaderFilterAssetLoader$1;->c:LX/AwI;

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/shaderfilter/InspirationShaderFilterAssetLoader$1;->a:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/shaderfilter/InspirationShaderFilterAssetLoader$1;->b:LX/89p;

    .line 1726337
    invoke-virtual {v1}, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->getShaderFilterModel()Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    move-result-object v8

    .line 1726338
    if-nez v8, :cond_1

    .line 1726339
    if-eqz v2, :cond_0

    .line 1726340
    invoke-interface {v2}, LX/89p;->b()V

    .line 1726341
    :cond_0
    :goto_0
    return-void

    .line 1726342
    :cond_1
    new-instance v3, LX/AvG;

    invoke-virtual {v8}, Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;->k()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8}, Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;->k()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".zip"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8}, Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;->l()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v8}, Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;->j()Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/AvG;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 1726343
    iget-object v4, v0, LX/AwI;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/friendsharing/inspiration/fetch/asset/InspirationAssetDownloader;

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    new-instance v5, LX/AwH;

    invoke-direct {v5, v0, v2}, LX/AwH;-><init>(LX/AwI;LX/89p;)V

    invoke-virtual {v4, v3, v5}, Lcom/facebook/friendsharing/inspiration/fetch/asset/InspirationAssetDownloader;->a(LX/0Px;LX/AvS;)V

    goto :goto_0
.end method
