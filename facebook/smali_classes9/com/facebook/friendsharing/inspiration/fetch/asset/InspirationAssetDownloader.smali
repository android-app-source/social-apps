.class public Lcom/facebook/friendsharing/inspiration/fetch/asset/InspirationAssetDownloader;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private final a:Lcom/facebook/common/callercontext/CallerContext;

.field private final b:LX/3AP;

.field private final c:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "LX/AvR;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/AvM;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/1Gm;LX/0Zb;LX/1Gk;LX/1Gl;LX/1Go;LX/13t;LX/1Ck;LX/AvM;)V
    .locals 11
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1724084
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1724085
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "inspiration_asset_download"

    invoke-static {v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/friendsharing/inspiration/fetch/asset/InspirationAssetDownloader;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1724086
    new-instance v1, LX/3AP;

    const-string v4, "inspiration_asset"

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    move-object v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    invoke-direct/range {v1 .. v10}, LX/3AP;-><init>(Landroid/content/Context;Lcom/facebook/http/common/FbHttpRequestProcessor;Ljava/lang/String;LX/1Gm;LX/0Zb;LX/1Gk;LX/1Gl;LX/1Go;LX/13t;)V

    iput-object v1, p0, Lcom/facebook/friendsharing/inspiration/fetch/asset/InspirationAssetDownloader;->b:LX/3AP;

    .line 1724087
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/fetch/asset/InspirationAssetDownloader;->c:LX/1Ck;

    .line 1724088
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/fetch/asset/InspirationAssetDownloader;->d:LX/AvM;

    .line 1724089
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/friendsharing/inspiration/fetch/asset/InspirationAssetDownloader;
    .locals 13

    .prologue
    .line 1724090
    new-instance v0, Lcom/facebook/friendsharing/inspiration/fetch/asset/InspirationAssetDownloader;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v2

    check-cast v2, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-static {p0}, LX/1Gm;->a(LX/0QB;)LX/1Gm;

    move-result-object v3

    check-cast v3, LX/1Gm;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {p0}, LX/1Gk;->a(LX/0QB;)LX/1Gk;

    move-result-object v5

    check-cast v5, LX/1Gk;

    invoke-static {p0}, LX/1Gl;->a(LX/0QB;)LX/1Gl;

    move-result-object v6

    check-cast v6, LX/1Gl;

    invoke-static {p0}, LX/1Gn;->a(LX/0QB;)LX/1Gn;

    move-result-object v7

    check-cast v7, LX/1Go;

    invoke-static {p0}, LX/13t;->a(LX/0QB;)LX/13t;

    move-result-object v8

    check-cast v8, LX/13t;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v9

    check-cast v9, LX/1Ck;

    .line 1724091
    new-instance v12, LX/AvM;

    invoke-static {p0}, LX/2Nz;->a(LX/0QB;)Lcom/facebook/compactdisk/StoreManagerFactory;

    move-result-object v10

    check-cast v10, Lcom/facebook/compactdisk/StoreManagerFactory;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v11

    check-cast v11, LX/1Ck;

    invoke-direct {v12, v10, v11}, LX/AvM;-><init>(Lcom/facebook/compactdisk/StoreManagerFactory;LX/1Ck;)V

    .line 1724092
    move-object v10, v12

    .line 1724093
    check-cast v10, LX/AvM;

    invoke-direct/range {v0 .. v10}, Lcom/facebook/friendsharing/inspiration/fetch/asset/InspirationAssetDownloader;-><init>(Landroid/content/Context;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/1Gm;LX/0Zb;LX/1Gk;LX/1Gl;LX/1Go;LX/13t;LX/1Ck;LX/AvM;)V

    .line 1724094
    return-object v0
.end method


# virtual methods
.method public final a(LX/0Px;LX/AvS;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/AvG;",
            ">;",
            "LX/AvS;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1724095
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_2

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AvG;

    .line 1724096
    iget-object v4, p0, Lcom/facebook/friendsharing/inspiration/fetch/asset/InspirationAssetDownloader;->d:LX/AvM;

    .line 1724097
    iget-boolean v1, v0, LX/AvG;->d:Z

    move v1, v1

    .line 1724098
    if-eqz v1, :cond_0

    .line 1724099
    iget-object v1, v0, LX/AvG;->e:Ljava/lang/String;

    move-object v1, v1

    .line 1724100
    :goto_1
    invoke-virtual {v4, v1}, LX/AvM;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1724101
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1724102
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1724103
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1724104
    invoke-interface {p2, v4}, LX/AvS;->a(Ljava/io/File;)V

    .line 1724105
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1724106
    :cond_0
    iget-object v1, v0, LX/AvG;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1724107
    goto :goto_1

    .line 1724108
    :cond_1
    iget-object v1, v0, LX/AvG;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1724109
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1724110
    new-instance v4, LX/AvQ;

    invoke-direct {v4, p0, v0, p2}, LX/AvQ;-><init>(Lcom/facebook/friendsharing/inspiration/fetch/asset/InspirationAssetDownloader;LX/AvG;LX/AvS;)V

    .line 1724111
    :try_start_0
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/fetch/asset/InspirationAssetDownloader;->b:LX/3AP;

    new-instance v5, LX/34X;

    iget-object v6, p0, Lcom/facebook/friendsharing/inspiration/fetch/asset/InspirationAssetDownloader;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v5, v1, v4, v6}, LX/34X;-><init>(Landroid/net/Uri;LX/1uy;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-virtual {v0, v5}, LX/3AP;->b(LX/34X;)LX/1j2;

    move-result-object v0

    .line 1724112
    iget-object v1, v0, LX/1j2;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v0, v1

    .line 1724113
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/fetch/asset/InspirationAssetDownloader;->c:LX/1Ck;

    sget-object v4, LX/AvR;->DOWNLOAD_ASSETS:LX/AvR;

    new-instance v5, LX/AvN;

    invoke-direct {v5, p0, p2}, LX/AvN;-><init>(Lcom/facebook/friendsharing/inspiration/fetch/asset/InspirationAssetDownloader;LX/AvS;)V

    invoke-virtual {v1, v4, v0, v5}, LX/1Ck;->c(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 1724114
    :catch_0
    invoke-interface {p2}, LX/AvS;->a()V

    goto :goto_2

    .line 1724115
    :cond_2
    return-void
.end method
