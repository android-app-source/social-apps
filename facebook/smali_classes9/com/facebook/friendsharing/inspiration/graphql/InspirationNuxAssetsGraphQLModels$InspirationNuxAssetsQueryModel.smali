.class public final Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x43bffa79
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1724371
    const-class v0, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1724374
    const-class v0, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1724372
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1724373
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1724365
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1724366
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel;->a()Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1724367
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1724368
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1724369
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1724370
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1724375
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1724376
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel;->a()Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1724377
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel;->a()Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel;

    .line 1724378
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel;->a()Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1724379
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel;

    .line 1724380
    iput-object v0, v1, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel;->e:Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel;

    .line 1724381
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1724382
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1724363
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel;->e:Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel;->e:Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel;

    .line 1724364
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel;->e:Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1724358
    new-instance v0, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel;

    invoke-direct {v0}, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel;-><init>()V

    .line 1724359
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1724360
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1724362
    const v0, -0x6d3d6512

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1724361
    const v0, -0x6747e1ce

    return v0
.end method
