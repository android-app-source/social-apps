.class public final Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x52be8ee7
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:I

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1724307
    const-class v0, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1724308
    const-class v0, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1724305
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1724306
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1724303
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1724304
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1724293
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1724294
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1724295
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1724296
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1724297
    iget v2, p0, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;->e:I

    invoke-virtual {p1, v4, v2, v4}, LX/186;->a(III)V

    .line 1724298
    const/4 v2, 0x1

    iget v3, p0, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;->f:I

    invoke-virtual {p1, v2, v3, v4}, LX/186;->a(III)V

    .line 1724299
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1724300
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1724301
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1724302
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1724290
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1724291
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1724292
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1724309
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1724310
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;->e:I

    .line 1724311
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;->f:I

    .line 1724312
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1724287
    new-instance v0, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;-><init>()V

    .line 1724288
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1724289
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1724286
    const v0, 0x35423a03

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1724285
    const v0, -0x27a58d37

    return v0
.end method

.method public final j()I
    .locals 2

    .prologue
    .line 1724283
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1724284
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;->f:I

    return v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1724279
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;->g:Ljava/lang/String;

    .line 1724280
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1724281
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;->h:Ljava/lang/String;

    .line 1724282
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/graphql/InspirationNuxAssetsGraphQLModels$InspirationNuxAssetsQueryModel$InspirationNuxAssetsModel$NodesModel;->h:Ljava/lang/String;

    return-object v0
.end method
