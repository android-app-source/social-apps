.class public Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Lcom/facebook/video/player/RichVideoPlayer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Landroid/widget/FrameLayout;

.field private final c:LX/3It;

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/media/AudioManager;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0tS;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/Avq;

.field private g:Z

.field private h:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1724779
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1724780
    new-instance v0, LX/2rT;

    invoke-direct {v0, p0}, LX/2rT;-><init>(Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;)V

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->c:LX/3It;

    .line 1724781
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1724782
    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->d:LX/0Ot;

    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    .line 1724777
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->a:Lcom/facebook/video/player/RichVideoPlayer;

    const/4 v1, 0x0

    sget-object v2, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(ILX/04g;)V

    .line 1724778
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1724774
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1724775
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;

    const/16 v0, 0x1f

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v0

    invoke-static {p1}, LX/0tS;->b(LX/0QB;)LX/0tS;

    move-result-object p1

    check-cast p1, LX/0tS;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->d:LX/0Ot;

    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->e:LX/0tS;

    .line 1724776
    return-void
.end method

.method public final b()V
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 1724733
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->a:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->g:Z

    if-eqz v0, :cond_1

    .line 1724734
    :cond_0
    :goto_0
    return-void

    .line 1724735
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->g:Z

    .line 1724736
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->a:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v1, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    .line 1724737
    invoke-static {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1724738
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->a:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, v3}, Lcom/facebook/video/player/RichVideoPlayer;->setVisibility(I)V

    .line 1724739
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1724740
    const-string v1, "inspiration_nux_video_uri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 1724741
    const/4 v6, 0x1

    .line 1724742
    invoke-static {}, Lcom/facebook/video/engine/VideoDataSource;->newBuilder()LX/2oE;

    move-result-object v4

    .line 1724743
    iput-object v0, v4, LX/2oE;->a:Landroid/net/Uri;

    .line 1724744
    move-object v4, v4

    .line 1724745
    sget-object v5, LX/097;->FROM_LOCAL_STORAGE:LX/097;

    .line 1724746
    iput-object v5, v4, LX/2oE;->e:LX/097;

    .line 1724747
    move-object v4, v4

    .line 1724748
    invoke-virtual {v4}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v4

    .line 1724749
    invoke-static {}, Lcom/facebook/video/engine/VideoPlayerParams;->newBuilder()LX/2oH;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/2oH;->a(Lcom/facebook/video/engine/VideoDataSource;)LX/2oH;

    move-result-object v4

    .line 1724750
    iput-boolean v6, v4, LX/2oH;->t:Z

    .line 1724751
    move-object v4, v4

    .line 1724752
    const/4 v5, 0x0

    .line 1724753
    iput-boolean v5, v4, LX/2oH;->g:Z

    .line 1724754
    move-object v4, v4

    .line 1724755
    iput-boolean v6, v4, LX/2oH;->o:Z

    .line 1724756
    move-object v4, v4

    .line 1724757
    invoke-virtual {v4}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v5

    .line 1724758
    new-instance v6, Landroid/util/DisplayMetrics;

    invoke-direct {v6}, Landroid/util/DisplayMetrics;-><init>()V

    .line 1724759
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v7, "window"

    invoke-virtual {v4, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/WindowManager;

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 1724760
    new-instance v4, LX/2pZ;

    invoke-direct {v4}, LX/2pZ;-><init>()V

    .line 1724761
    iget-object v7, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->e:LX/0tS;

    invoke-virtual {v7}, LX/0tS;->d()I

    move-result v7

    .line 1724762
    iput-object v5, v4, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1724763
    move-object v5, v4

    .line 1724764
    iget v6, v6, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-double v8, v6

    int-to-double v6, v7

    div-double v6, v8, v6

    .line 1724765
    iput-wide v6, v5, LX/2pZ;->e:D

    .line 1724766
    move-object v0, v4

    .line 1724767
    invoke-virtual {v0}, LX/2pZ;->b()LX/2pa;

    move-result-object v0

    .line 1724768
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->a:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 1724769
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->a:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->c:LX/3It;

    .line 1724770
    iput-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->F:LX/3It;

    .line 1724771
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->h:Z

    if-eqz v0, :cond_0

    .line 1724772
    iput-boolean v3, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->h:Z

    .line 1724773
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->c()V

    goto/16 :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1724783
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->a:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_1

    .line 1724784
    invoke-direct {p0}, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->e()V

    .line 1724785
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1724786
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->a:Lcom/facebook/video/player/RichVideoPlayer;

    const/4 v1, 0x0

    sget-object v2, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 1724787
    :goto_0
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->a:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 1724788
    :goto_1
    return-void

    .line 1724789
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->a:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v0, v2, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    goto :goto_0

    .line 1724790
    :cond_1
    iput-boolean v2, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->h:Z

    goto :goto_1
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 1724728
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->a:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_0

    .line 1724729
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->a:Lcom/facebook/video/player/RichVideoPlayer;

    const/4 v1, 0x1

    sget-object v2, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 1724730
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->a:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 1724731
    invoke-direct {p0}, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->e()V

    .line 1724732
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x43588762

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1724706
    const v1, 0x7f03094b

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x4cb0eb48

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x4bcf4d5c    # 2.7171512E7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1724722
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1724723
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->d()V

    .line 1724724
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->a:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v1, :cond_0

    .line 1724725
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->b:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_0

    .line 1724726
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->b:Landroid/widget/FrameLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1724727
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x5de2870b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1724713
    const v0, 0x7f0d17cb

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->a:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1724714
    const v0, 0x7f0d17cc

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->b:Landroid/widget/FrameLayout;

    .line 1724715
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1724716
    const-string v1, "video_foreground_color_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 1724717
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_0

    .line 1724718
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->b:Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1724719
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->b()V

    .line 1724720
    return-void

    .line 1724721
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->b:Landroid/widget/FrameLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public final setUserVisibleHint(Z)V
    .locals 1

    .prologue
    .line 1724707
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->setUserVisibleHint(Z)V

    .line 1724708
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 1724709
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->c()V

    .line 1724710
    :cond_0
    :goto_0
    return-void

    .line 1724711
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 1724712
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->d()V

    goto :goto_0
.end method
