.class public Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxCtaFragment;
.super Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;
.source ""


# instance fields
.field public c:Landroid/view/View$OnClickListener;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public d:LX/0iH;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public e:Landroid/view/View$OnClickListener;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private f:Landroid/widget/FrameLayout;

.field private g:Lcom/facebook/widget/text/BetterTextView;

.field private h:Landroid/view/View;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private final i:Landroid/view/GestureDetector$SimpleOnGestureListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1724791
    invoke-direct {p0}, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;-><init>()V

    .line 1724792
    new-instance v0, LX/Avu;

    invoke-direct {v0, p0}, LX/Avu;-><init>(Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxCtaFragment;)V

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxCtaFragment;->i:Landroid/view/GestureDetector$SimpleOnGestureListener;

    return-void
.end method

.method public static a(Landroid/net/Uri;IZ)Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxCtaFragment;
    .locals 3
    .param p0    # Landroid/net/Uri;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1724793
    new-instance v0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxCtaFragment;

    invoke-direct {v0}, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxCtaFragment;-><init>()V

    .line 1724794
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1724795
    if-eqz p0, :cond_0

    .line 1724796
    const-string v2, "inspiration_nux_video_uri"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1724797
    :cond_0
    const-string v2, "video_foreground_color_id"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1724798
    const-string v2, "extra_hide_back_to_top_button"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1724799
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1724800
    return-object v0
.end method


# virtual methods
.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x6b756c33

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1724801
    const v0, 0x7f030949

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxCtaFragment;->f:Landroid/widget/FrameLayout;

    .line 1724802
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxCtaFragment;->f:Landroid/widget/FrameLayout;

    const/16 v2, 0x2b

    const v3, -0x74ca3b8d

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1724803
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, LX/0fB;

    if-eqz v0, :cond_0

    .line 1724804
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, LX/0fB;

    .line 1724805
    if-nez v0, :cond_3

    .line 1724806
    :cond_0
    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1724807
    const-string v1, "inspiration_nux_video_uri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1724808
    const v0, 0x7f0d17cb

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxCtaFragment;->a:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1724809
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1724810
    const-string v1, "extra_hide_back_to_top_button"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1724811
    const v0, 0x7f0d17c7

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxCtaFragment;->h:Landroid/view/View;

    .line 1724812
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxCtaFragment;->h:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1724813
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxCtaFragment;->h:Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxCtaFragment;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1724814
    :cond_1
    const v0, 0x7f0d17cc

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxCtaFragment;->b:Landroid/widget/FrameLayout;

    .line 1724815
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1724816
    const-string v1, "video_foreground_color_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 1724817
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_2

    .line 1724818
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->b:Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1724819
    :goto_1
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxCtaFragment;->i:Landroid/view/GestureDetector$SimpleOnGestureListener;

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    .line 1724820
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxCtaFragment;->f:Landroid/widget/FrameLayout;

    new-instance v2, LX/Avv;

    invoke-direct {v2, p0, v0}, LX/Avv;-><init>(Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxCtaFragment;Landroid/view/GestureDetector;)V

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1724821
    const v0, 0x7f0d17c8

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxCtaFragment;->g:Lcom/facebook/widget/text/BetterTextView;

    .line 1724822
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxCtaFragment;->g:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxCtaFragment;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1724823
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->b()V

    .line 1724824
    return-void

    .line 1724825
    :cond_2
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxVideoFragment;->b:Landroid/widget/FrameLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_1

    .line 1724826
    :cond_3
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxCtaFragment;->c:Landroid/view/View$OnClickListener;

    if-nez v1, :cond_4

    .line 1724827
    invoke-interface {v0}, LX/0fB;->j()Landroid/view/View$OnClickListener;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxCtaFragment;->c:Landroid/view/View$OnClickListener;

    .line 1724828
    :cond_4
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxCtaFragment;->e:Landroid/view/View$OnClickListener;

    if-nez v1, :cond_5

    .line 1724829
    invoke-interface {v0}, LX/0fB;->k()Landroid/view/View$OnClickListener;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxCtaFragment;->e:Landroid/view/View$OnClickListener;

    .line 1724830
    :cond_5
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxCtaFragment;->d:LX/0iH;

    if-nez v1, :cond_0

    .line 1724831
    invoke-interface {v0}, LX/0fB;->h()LX/0iH;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/nux/InspirationNuxCtaFragment;->d:LX/0iH;

    goto/16 :goto_0
.end method
