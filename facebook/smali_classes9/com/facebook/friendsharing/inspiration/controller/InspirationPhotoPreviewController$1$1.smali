.class public final Lcom/facebook/friendsharing/inspiration/controller/InspirationPhotoPreviewController$1$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:I

.field public final synthetic c:LX/Atc;


# direct methods
.method public constructor <init>(LX/Atc;II)V
    .locals 0

    .prologue
    .line 1721592
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationPhotoPreviewController$1$1;->c:LX/Atc;

    iput p2, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationPhotoPreviewController$1$1;->a:I

    iput p3, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationPhotoPreviewController$1$1;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1721593
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationPhotoPreviewController$1$1;->c:LX/Atc;

    iget-object v0, v0, LX/Atc;->a:LX/Atd;

    iget-object v0, v0, LX/Atd;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1721594
    check-cast v0, LX/0im;

    invoke-interface {v0}, LX/0im;->c()LX/0jJ;

    move-result-object v0

    sget-object v1, LX/Atd;->a:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationPhotoPreviewController$1$1;->a:I

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationPhotoPreviewController$1$1;->b:I

    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationPhotoPreviewController$1$1;->c:LX/Atc;

    iget-object v3, v3, LX/Atc;->a:LX/Atd;

    iget-object v3, v3, LX/Atd;->e:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v3

    iget-object v4, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationPhotoPreviewController$1$1;->c:LX/Atc;

    iget-object v4, v4, LX/Atc;->a:LX/Atd;

    iget-object v4, v4, LX/Atd;->e:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v4

    invoke-static {v1, v2, v3, v4}, LX/AtT;->a(IIII)Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 1721595
    return-void
.end method
