.class public Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/AsO;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Landroid/net/Uri;

.field public static final b:Landroid/net/Uri;

.field private static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:LX/1Ad;

.field public final f:Landroid/content/Context;

.field private final g:LX/0wW;

.field public final h:LX/As2;

.field private final i:LX/ArL;

.field public j:LX/AsQ;

.field public k:I

.field public l:I

.field public m:I

.field public n:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1720461
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "res"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const v1, 0x7f020da7

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->a:Landroid/net/Uri;

    .line 1720462
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "res"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const v1, 0x7f02154a

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->b:Landroid/net/Uri;

    .line 1720463
    const-class v0, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/ArL;LX/1Ad;LX/0wW;Landroid/content/Context;LX/As3;)V
    .locals 1
    .param p1    # LX/ArL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1720502
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1720503
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->k:I

    .line 1720504
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->i:LX/ArL;

    .line 1720505
    iput-object p2, p0, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->e:LX/1Ad;

    .line 1720506
    iput-object p3, p0, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->g:LX/0wW;

    .line 1720507
    iput-object p4, p0, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->f:Landroid/content/Context;

    .line 1720508
    sget-object v0, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p5, v0}, LX/As3;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/As2;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->h:LX/As2;

    .line 1720509
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 1720494
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030468

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1720495
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 1720496
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->e:LX/1Ad;

    sget-object v2, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    .line 1720497
    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1720498
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->f:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082783

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1720499
    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->l:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1720500
    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->m:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1720501
    new-instance v1, LX/AsO;

    invoke-direct {v1, p0, v0}, LX/AsO;-><init>(Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 8

    .prologue
    .line 1720466
    check-cast p1, LX/AsO;

    .line 1720467
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->d:Ljava/util/List;

    if-nez v0, :cond_1

    .line 1720468
    :cond_0
    :goto_0
    return-void

    .line 1720469
    :cond_1
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->d:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    .line 1720470
    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getThumbnailUri()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getThumbnailUri()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    :goto_1
    move-object v2, v1

    .line 1720471
    iget v1, p0, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->k:I

    move v1, v1

    .line 1720472
    if-ne p2, v1, :cond_3

    const/4 v1, 0x1

    .line 1720473
    :goto_2
    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object v3

    if-eqz v1, :cond_4

    const v1, 0x7f020421

    .line 1720474
    :goto_3
    const-string v4, "1752514608329267"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 1720475
    const-string v4, "178267072637018"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 1720476
    if-eqz v5, :cond_6

    .line 1720477
    sget-object v4, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->a:Landroid/net/Uri;

    :goto_4
    move-object v2, v4

    .line 1720478
    :cond_2
    iget-object v4, p1, LX/AsO;->m:Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;

    iget-object v4, v4, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->h:LX/As2;

    iget-object v6, p1, LX/AsO;->l:LX/As1;

    invoke-virtual {v4, v6, v2, p1}, LX/As2;->a(LX/As1;Landroid/net/Uri;Landroid/view/View$OnClickListener;)V

    .line 1720479
    iget-object v4, p1, LX/AsO;->l:LX/As1;

    iget-object v4, v4, LX/As1;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1720480
    iget-object v6, p1, LX/AsO;->m:Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;

    iget-object v6, v6, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->f:Landroid/content/Context;

    iget-object v7, p1, LX/AsO;->m:Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;

    iget v7, v7, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->l:I

    invoke-static {v6, v7}, LX/As2;->a(Landroid/content/Context;I)LX/1af;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 1720481
    iget-object v4, p1, LX/AsO;->l:LX/As1;

    iget-object v4, v4, LX/As1;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v4}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v4

    check-cast v4, LX/1af;

    if-eqz v5, :cond_8

    sget-object v5, LX/1Up;->f:LX/1Up;

    :goto_5
    invoke-virtual {v4, v5}, LX/1af;->a(LX/1Up;)V

    .line 1720482
    iget-object v4, p1, LX/AsO;->l:LX/As1;

    iget-object v4, v4, LX/As1;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v4, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setBackgroundResource(I)V

    .line 1720483
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->j:LX/AsQ;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->j:LX/AsQ;

    .line 1720484
    iget-object v2, v1, LX/AsQ;->a:LX/AsR;

    iget-object v2, v2, LX/AsR;->m:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0ik;

    check-cast v2, LX/0il;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0io;

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v2}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->getOpenBottomTray()LX/86o;

    move-result-object v2

    iget-object v3, v1, LX/AsQ;->a:LX/AsR;

    invoke-virtual {v3}, LX/AsR;->b()LX/86o;

    move-result-object v3

    if-ne v2, v3, :cond_9

    const/4 v2, 0x1

    :goto_6
    move v1, v2

    .line 1720485
    if-eqz v1, :cond_0

    .line 1720486
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->i:LX/ArL;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object v0

    .line 1720487
    sget-object v2, LX/ArH;->THUMBNAIL_IMPRESSION:LX/ArH;

    invoke-static {v1, v2}, LX/ArL;->a(LX/ArL;LX/5oU;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/ArI;->THUMBNAIL_INDEX:LX/ArI;

    invoke-virtual {v3}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/ArI;->THUMBNAIL_PROMPT_ID:LX/ArI;

    invoke-virtual {v3}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-static {v1, v2}, LX/ArL;->a(LX/ArL;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1720488
    goto/16 :goto_0

    .line 1720489
    :cond_3
    const/4 v1, 0x0

    goto/16 :goto_2

    .line 1720490
    :cond_4
    const v1, 0x7f020420

    goto/16 :goto_3

    :cond_5
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 1720491
    :cond_6
    if-eqz v4, :cond_2

    .line 1720492
    iget-object v4, p1, LX/AsO;->m:Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;

    iget-boolean v4, v4, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->n:Z

    if-eqz v4, :cond_7

    const/4 v4, 0x0

    goto/16 :goto_4

    :cond_7
    sget-object v4, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->b:Landroid/net/Uri;

    goto/16 :goto_4

    .line 1720493
    :cond_8
    sget-object v5, LX/1Up;->g:LX/1Up;

    goto :goto_5

    :cond_9
    const/4 v2, 0x0

    goto :goto_6
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1720465
    const/4 v0, 0x0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1720464
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->d:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method
