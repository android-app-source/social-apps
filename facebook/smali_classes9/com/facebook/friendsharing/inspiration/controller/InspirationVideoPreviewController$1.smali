.class public final Lcom/facebook/friendsharing/inspiration/controller/InspirationVideoPreviewController$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/ipc/media/MediaItem;

.field public final synthetic b:LX/AuL;


# direct methods
.method public constructor <init>(LX/AuL;Lcom/facebook/ipc/media/MediaItem;)V
    .locals 0

    .prologue
    .line 1722441
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationVideoPreviewController$1;->b:LX/AuL;

    iput-object p2, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationVideoPreviewController$1;->a:Lcom/facebook/ipc/media/MediaItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 1722442
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationVideoPreviewController$1;->b:LX/AuL;

    iget-object v0, v0, LX/AuL;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1722443
    check-cast v0, LX/0im;

    invoke-interface {v0}, LX/0im;->c()LX/0jJ;

    move-result-object v0

    sget-object v1, LX/AuL;->a:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationVideoPreviewController$1;->a:Lcom/facebook/ipc/media/MediaItem;

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationVideoPreviewController$1;->b:LX/AuL;

    iget-object v2, v2, LX/AuL;->e:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v2}, Lcom/facebook/video/player/RichVideoPlayer;->getMeasuredWidth()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationVideoPreviewController$1;->b:LX/AuL;

    iget-object v3, v3, LX/AuL;->e:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v3}, Lcom/facebook/video/player/RichVideoPlayer;->getMeasuredHeight()I

    move-result v3

    .line 1722444
    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v4

    .line 1722445
    iget v5, v4, Lcom/facebook/ipc/media/data/MediaData;->mWidth:I

    move v5, v5

    .line 1722446
    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v4

    .line 1722447
    iget v6, v4, Lcom/facebook/ipc/media/data/MediaData;->mHeight:I

    move v4, v6

    .line 1722448
    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->g()I

    move-result v6

    .line 1722449
    const/16 v7, 0x5a

    if-eq v6, v7, :cond_0

    const/16 v7, 0x10e

    if-ne v6, v7, :cond_1

    :cond_0
    move p0, v5

    move v5, v4

    move v4, p0

    .line 1722450
    :cond_1
    invoke-static {v5, v4, v2, v3}, LX/AtT;->a(IIII)Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

    move-result-object v4

    move-object v1, v4

    .line 1722451
    invoke-virtual {v0, v1}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 1722452
    return-void
.end method
