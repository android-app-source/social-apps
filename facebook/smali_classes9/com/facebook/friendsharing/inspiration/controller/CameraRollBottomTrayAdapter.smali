.class public Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/As4;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/media/util/model/MediaModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:LX/1Ad;

.field public final d:Landroid/content/Context;

.field public final e:LX/As2;

.field public f:I

.field public g:LX/As7;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1720046
    const-class v0, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1Ad;Landroid/content/Context;LX/As3;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1720047
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1720048
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->c:LX/1Ad;

    .line 1720049
    iput-object p2, p0, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->d:Landroid/content/Context;

    .line 1720050
    sget-object v0, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p3, v0}, LX/As3;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/As2;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->e:LX/As2;

    .line 1720051
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1720052
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1720053
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->f:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1720054
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->f:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1720055
    :goto_0
    return-void

    .line 1720056
    :cond_0
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->f:I

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->f:I

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1720057
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;
    .locals 4

    .prologue
    .line 1720058
    new-instance v3, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v0

    check-cast v0, LX/1Ad;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    const-class v2, LX/As3;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/As3;

    invoke-direct {v3, v0, v1, v2}, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;-><init>(LX/1Ad;Landroid/content/Context;LX/As3;)V

    .line 1720059
    return-object v3
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    .line 1720060
    const/4 v2, 0x0

    .line 1720061
    sget-object v0, LX/As5;->VIDEO:LX/As5;

    invoke-virtual {v0}, LX/As5;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 1720062
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030230

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    .line 1720063
    :goto_0
    const v0, 0x7f0d0883

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1720064
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 1720065
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->c:LX/1Ad;

    sget-object v3, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v2

    .line 1720066
    invoke-virtual {v0, v2}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1720067
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082782

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1720068
    sget-object v2, LX/As5;->VIDEO:LX/As5;

    invoke-virtual {v2}, LX/As5;->ordinal()I

    move-result v2

    if-ne p2, v2, :cond_1

    .line 1720069
    invoke-direct {p0, v0}, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->a(Landroid/view/View;)V

    .line 1720070
    new-instance v0, LX/As6;

    invoke-direct {v0, p0, v1}, LX/As6;-><init>(Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;Landroid/view/View;)V

    .line 1720071
    :goto_1
    return-object v0

    .line 1720072
    :cond_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03022f

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 1720073
    :cond_1
    invoke-direct {p0, v0}, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->a(Landroid/view/View;)V

    .line 1720074
    new-instance v0, LX/As4;

    invoke-direct {v0, p0, v1}, LX/As4;-><init>(Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;Landroid/view/View;)V

    goto :goto_1
.end method

.method public final a(LX/1a1;I)V
    .locals 6

    .prologue
    .line 1720075
    check-cast p1, LX/As4;

    .line 1720076
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->b:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1720077
    :goto_0
    return-void

    .line 1720078
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/media/util/model/MediaModel;

    .line 1720079
    iget-object v1, v0, Lcom/facebook/media/util/model/MediaModel;->d:Landroid/net/Uri;

    move-object v1, v1

    .line 1720080
    iget-object v2, v0, Lcom/facebook/media/util/model/MediaModel;->b:LX/1po;

    move-object v2, v2

    .line 1720081
    sget-object v3, LX/1po;->VIDEO:LX/1po;

    if-ne v2, v3, :cond_1

    .line 1720082
    check-cast p1, LX/As6;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 1720083
    iget v3, v0, Lcom/facebook/media/util/model/MediaModel;->c:I

    move v0, v3

    .line 1720084
    int-to-long v4, v0

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    long-to-int v0, v2

    invoke-static {v0}, LX/BV7;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/As6;->a(Landroid/net/Uri;Ljava/lang/String;)V

    goto :goto_0

    .line 1720085
    :cond_1
    sget-object v0, LX/1po;->PHOTO:LX/1po;

    invoke-virtual {p1, v1, v0}, LX/As4;->a(Landroid/net/Uri;LX/1po;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/media/util/model/MediaModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1720086
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->b:Ljava/util/List;

    .line 1720087
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->f:I

    if-lez v0, :cond_0

    .line 1720088
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->e:LX/As2;

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->f:I

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->f:I

    invoke-virtual {v0, v1, v2}, LX/As2;->a(II)V

    .line 1720089
    :cond_0
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 1720090
    return-void
.end method

.method public final c(LX/1a1;)V
    .locals 1

    .prologue
    .line 1720091
    check-cast p1, LX/As4;

    .line 1720092
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/1a1;->a(Z)V

    .line 1720093
    return-void
.end method

.method public final d(LX/1a1;)V
    .locals 1

    .prologue
    .line 1720094
    check-cast p1, LX/As4;

    .line 1720095
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/1a1;->a(Z)V

    .line 1720096
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 1720097
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1720098
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/media/util/model/MediaModel;

    .line 1720099
    iget-object v1, v0, Lcom/facebook/media/util/model/MediaModel;->b:LX/1po;

    move-object v0, v1

    .line 1720100
    sget-object v1, LX/1po;->VIDEO:LX/1po;

    if-ne v0, v1, :cond_0

    .line 1720101
    sget-object v0, LX/As5;->VIDEO:LX/As5;

    invoke-virtual {v0}, LX/As5;->ordinal()I

    move-result v0

    .line 1720102
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/As5;->PHOTO:LX/As5;

    invoke-virtual {v0}, LX/As5;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1720103
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->b:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1720104
    const/4 v0, 0x0

    .line 1720105
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method
