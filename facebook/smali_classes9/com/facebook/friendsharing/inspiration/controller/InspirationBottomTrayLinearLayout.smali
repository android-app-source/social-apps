.class public Lcom/facebook/friendsharing/inspiration/controller/InspirationBottomTrayLinearLayout;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field private a:LX/3rW;

.field private b:LX/3rW;

.field private c:LX/0iH;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1720742
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/friendsharing/inspiration/controller/InspirationBottomTrayLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1720743
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1720744
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1720745
    return-void
.end method


# virtual methods
.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 1720746
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 1720747
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationBottomTrayLinearLayout;->a:LX/3rW;

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x12873487

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1720748
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationBottomTrayLinearLayout;->a:LX/3rW;

    if-nez v2, :cond_0

    .line 1720749
    const/4 v0, 0x0

    const v2, -0x414fac44

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1720750
    :goto_0
    return v0

    .line 1720751
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_1

    .line 1720752
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationBottomTrayLinearLayout;->c:LX/0iH;

    invoke-virtual {v2, p1}, LX/0iH;->a(Landroid/view/MotionEvent;)Z

    .line 1720753
    :cond_1
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationBottomTrayLinearLayout;->b:LX/3rW;

    invoke-virtual {v2, p1}, LX/3rW;->a(Landroid/view/MotionEvent;)Z

    .line 1720754
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationBottomTrayLinearLayout;->a:LX/3rW;

    invoke-virtual {v2, p1}, LX/3rW;->a(Landroid/view/MotionEvent;)Z

    .line 1720755
    const v2, 0x466a583c

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method
