.class public Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0iK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/friendsharing/inspiration/model/CameraStateSpec$ProvidesCameraState;",
        ":",
        "LX/0io;",
        ":",
        "LX/0j0;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$ProvidesInspirationLoggingData;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBoundsSpec$ProvidesInspirationPreviewBounds;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$ProvidesInspirationState;",
        ":",
        "LX/0is;",
        "DerivedData:",
        "Ljava/lang/Object;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$SetsInspirationLoggingData",
        "<TMutation;>;:",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$SetsInspirationState",
        "<TMutation;>;:",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModelSpec$SetsInspirationSwipeableModel",
        "<TMutation;>;Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;:",
        "LX/0im",
        "<TMutation;>;>",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;"
    }
.end annotation


# static fields
.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private A:Landroid/view/ViewParent;

.field private B:Landroid/view/View$OnClickListener;

.field public C:LX/Ar0;

.field private D:Landroid/view/View$OnClickListener;

.field public E:Lcom/facebook/resources/ui/FbTextView;

.field public F:Landroid/view/View;

.field public G:Lcom/facebook/widget/text/BetterTextView;

.field private H:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public I:Landroid/view/View;

.field public J:Landroid/view/ViewPropertyAnimator;

.field public K:Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;

.field public L:Z

.field public M:Z

.field private N:Z

.field private O:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
            ">;"
        }
    .end annotation
.end field

.field private P:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private Q:Z

.field private R:I

.field private S:I

.field private T:I

.field public a:Z

.field private final c:LX/8GH;

.field public final d:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private final e:LX/9d6;

.field private final f:LX/AuN;

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/8GP;

.field public final i:LX/AuX;

.field private final j:LX/AsN;

.field public final k:LX/0Sh;

.field private final l:LX/Arh;

.field private final m:Landroid/app/Activity;

.field private final n:LX/8GL;

.field private final o:LX/Aqz;

.field private final p:LX/87V;

.field public final q:LX/ArT;

.field public final r:LX/ArS;

.field private final s:LX/Aty;

.field public t:LX/9d5;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/friendsharing/inspiration/InspirationViewController;",
            ">;"
        }
    .end annotation
.end field

.field public y:I

.field public z:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1722194
    const-class v0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0il;LX/Arh;Landroid/app/Activity;Landroid/view/View;Landroid/view/ViewStub;Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;LX/Aqz;LX/ArT;LX/ArS;LX/AuO;LX/9d6;LX/0Or;LX/8GP;LX/AuX;LX/AsN;LX/0Sh;LX/87V;Landroid/content/Context;)V
    .locals 6
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Arh;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/app/Activity;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Landroid/view/ViewStub;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/Aqz;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # LX/ArT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # LX/ArS;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "Lcom/facebook/friendsharing/inspiration/capture/InspirationCameraEffectsApplier;",
            "Landroid/app/Activity;",
            "Landroid/view/View;",
            "Landroid/view/ViewStub;",
            "Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;",
            "Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;",
            "Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController$Delegate;",
            "LX/ArT;",
            "LX/ArS;",
            "LX/AuO;",
            "LX/9d6;",
            "LX/0Or",
            "<",
            "Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;",
            ">;",
            "LX/8GP;",
            "LX/AuX;",
            "LX/AsN;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/87V;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1722162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1722163
    new-instance v1, LX/Atr;

    invoke-direct {v1, p0}, LX/Atr;-><init>(Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;)V

    iput-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->c:LX/8GH;

    .line 1722164
    new-instance v1, LX/Aty;

    invoke-direct {v1}, LX/Aty;-><init>()V

    iput-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->s:LX/Aty;

    .line 1722165
    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->x:LX/0Px;

    .line 1722166
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->N:Z

    .line 1722167
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->d:Ljava/lang/ref/WeakReference;

    .line 1722168
    iput-object p2, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->l:LX/Arh;

    .line 1722169
    iput-object p3, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->m:Landroid/app/Activity;

    .line 1722170
    const v1, 0x7f0d1859

    invoke-static {p4, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    iput-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->E:Lcom/facebook/resources/ui/FbTextView;

    .line 1722171
    invoke-virtual/range {p19 .. p19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1ab9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->R:I

    .line 1722172
    invoke-virtual/range {p19 .. p19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1aba

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->S:I

    .line 1722173
    invoke-virtual/range {p19 .. p19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0786

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->T:I

    .line 1722174
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->E:Lcom/facebook/resources/ui/FbTextView;

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->S:I

    int-to-float v2, v2

    const/4 v3, 0x0

    iget v4, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->R:I

    int-to-float v4, v4

    iget v5, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->T:I

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/facebook/resources/ui/FbTextView;->setShadowLayer(FFFI)V

    .line 1722175
    const v1, 0x7f0d185a

    invoke-static {p4, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;

    iput-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->K:Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;

    .line 1722176
    invoke-static {p6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    iput-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->w:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    .line 1722177
    move-object/from16 v0, p11

    invoke-direct {p0, v0}, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->a(LX/AuO;)LX/AuN;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->f:LX/AuN;

    .line 1722178
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->e:LX/9d6;

    .line 1722179
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->g:LX/0Or;

    .line 1722180
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->h:LX/8GP;

    .line 1722181
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->i:LX/AuX;

    .line 1722182
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->j:LX/AsN;

    .line 1722183
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->k:LX/0Sh;

    .line 1722184
    iput-object p8, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->o:LX/Aqz;

    .line 1722185
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->p:LX/87V;

    .line 1722186
    invoke-direct {p0, p7}, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->a(Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;)LX/8GL;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->n:LX/8GL;

    .line 1722187
    iput-object p9, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->q:LX/ArT;

    .line 1722188
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->r:LX/ArS;

    .line 1722189
    invoke-virtual {p5}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->F:Landroid/view/View;

    .line 1722190
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->F:Landroid/view/View;

    const v2, 0x7f0d1782

    invoke-static {v1, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterTextView;

    iput-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->G:Lcom/facebook/widget/text/BetterTextView;

    .line 1722191
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->F:Landroid/view/View;

    const v2, 0x7f0d1781

    invoke-static {v1, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->H:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1722192
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->F:Landroid/view/View;

    const v2, 0x7f0d177f

    invoke-static {v1, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->I:Landroid/view/View;

    .line 1722193
    return-void
.end method

.method private a(Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;)LX/8GL;
    .locals 2

    .prologue
    .line 1722161
    new-instance v0, LX/8GL;

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->p:LX/87V;

    invoke-virtual {v1}, LX/87V;->a()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-direct {v0, p1, v1}, LX/8GL;-><init>(Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;I)V

    return-object v0
.end method

.method private a(LX/AuO;)LX/AuN;
    .locals 2

    .prologue
    .line 1722157
    new-instance v0, LX/Atw;

    invoke-direct {v0, p0}, LX/Atw;-><init>(Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;)V

    new-instance v1, LX/Atx;

    invoke-direct {v1, p0}, LX/Atx;-><init>(Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;)V

    .line 1722158
    new-instance p0, LX/AuN;

    invoke-direct {p0, v0, v1}, LX/AuN;-><init>(LX/Atw;LX/Atx;)V

    .line 1722159
    move-object v0, p0

    .line 1722160
    return-object v0
.end method

.method private a(Lcom/facebook/composer/system/model/ComposerModelImpl;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TModelData;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x4

    .line 1722138
    move-object v0, p1

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->getOpenBottomTray()LX/86o;

    move-result-object v0

    invoke-virtual {v0}, LX/86o;->shouldBlockOtherUIComponents()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1722139
    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->e$redex0(Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;)V

    :goto_0
    move-object v0, p1

    .line 1722140
    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->getOpenBottomTray()LX/86o;

    move-result-object v0

    .line 1722141
    sget-object v1, LX/86o;->NONE:LX/86o;

    if-ne v0, v1, :cond_2

    move-object v0, p1

    .line 1722142
    check-cast v0, LX/0is;

    invoke-interface {v0}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getSelectedId()Ljava/lang/String;

    move-result-object v0

    .line 1722143
    check-cast p1, LX/0io;

    invoke-static {p1}, LX/87R;->a(LX/0io;)Z

    move-result v1

    .line 1722144
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->s:LX/Aty;

    invoke-virtual {v2, v0, v1}, LX/Aty;->a(Ljava/lang/String;Z)LX/AsM;

    move-result-object v0

    .line 1722145
    iget-object v1, v0, LX/AsM;->m:LX/AtP;

    move-object v0, v1

    .line 1722146
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->N:Z

    .line 1722147
    if-eqz v0, :cond_0

    .line 1722148
    invoke-interface {v0}, LX/89u;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0}, LX/89u;->h()Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v1, v0}, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->a(Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;Ljava/lang/String;Landroid/net/Uri;)V

    .line 1722149
    :cond_0
    :goto_1
    return-void

    .line 1722150
    :cond_1
    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->d(Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;)V

    goto :goto_0

    .line 1722151
    :cond_2
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->J:Landroid/view/ViewPropertyAnimator;

    if-eqz v0, :cond_3

    .line 1722152
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->J:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 1722153
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->N:Z

    .line 1722154
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->F:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 1722155
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->G:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1722156
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->I:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method private static a(Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 6

    .prologue
    .line 1722128
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->N:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->M:Z

    if-nez v0, :cond_0

    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p2, :cond_1

    .line 1722129
    :cond_0
    :goto_0
    return-void

    .line 1722130
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->M:Z

    .line 1722131
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->G:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1722132
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->H:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p2, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1722133
    const/4 v3, 0x0

    .line 1722134
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->G:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1722135
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->I:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1722136
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->F:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const-wide/16 v4, 0x12c

    invoke-virtual {v2, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    new-instance v3, LX/Att;

    invoke-direct {v3, p0}, LX/Att;-><init>(Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->J:Landroid/view/ViewPropertyAnimator;

    .line 1722137
    goto :goto_0
.end method

.method public static a(Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;Z)V
    .locals 4

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 1722120
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->J:Landroid/view/ViewPropertyAnimator;

    if-eqz v0, :cond_0

    .line 1722121
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->J:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 1722122
    :cond_0
    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->M:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->F:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getAlpha()F

    move-result v0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    .line 1722123
    :cond_1
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->G:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1722124
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->I:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1722125
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->F:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 1722126
    :goto_0
    return-void

    .line 1722127
    :cond_2
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->F:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, LX/Atu;

    invoke-direct {v1, p0}, LX/Atu;-><init>(Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->J:Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method

.method public static b$redex0(Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1722195
    if-nez p1, :cond_1

    .line 1722196
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->E:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->clearAnimation()V

    .line 1722197
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->E:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1722198
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->E:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setAlpha(F)V

    .line 1722199
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->E:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1722200
    iput-boolean v3, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->L:Z

    .line 1722201
    :cond_0
    :goto_0
    return-void

    .line 1722202
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->L:Z

    if-nez v0, :cond_0

    .line 1722203
    iput-boolean v3, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->L:Z

    .line 1722204
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->E:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x9c4

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method

.method private c()V
    .locals 14

    .prologue
    const/4 v5, 0x0

    .line 1722066
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-object v0, v6

    .line 1722067
    check-cast v0, LX/0io;

    invoke-static {v0}, LX/87R;->a(LX/0io;)Z

    move-result v12

    .line 1722068
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->t:LX/9d5;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->u:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    if-nez v0, :cond_1

    .line 1722069
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->u:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    .line 1722070
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->e:LX/9d6;

    new-instance v1, LX/9c0;

    invoke-direct {v1}, LX/9c0;-><init>()V

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->u:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    const/4 v3, 0x0

    const-string v4, "-1"

    invoke-virtual/range {v0 .. v5}, LX/9d6;->a(LX/9bz;Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;Landroid/net/Uri;Ljava/lang/String;Z)LX/9d5;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->t:LX/9d5;

    .line 1722071
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->t:LX/9d5;

    .line 1722072
    iput-boolean v5, v0, LX/9d5;->M:Z

    .line 1722073
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->t:LX/9d5;

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->c:LX/8GH;

    invoke-virtual {v0, v1}, LX/9d5;->a(LX/8GH;)V

    .line 1722074
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->t:LX/9d5;

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->f:LX/AuN;

    invoke-virtual {v0, v1}, LX/9d5;->a(LX/8GH;)V

    .line 1722075
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->t:LX/9d5;

    .line 1722076
    iget-object v1, v0, LX/9d5;->L:LX/9dI;

    .line 1722077
    iput-boolean v5, v1, LX/9dI;->k:Z

    .line 1722078
    :cond_1
    move-object v0, v6

    .line 1722079
    check-cast v0, LX/0is;

    invoke-interface {v0}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getSelectedId()Ljava/lang/String;

    move-result-object v2

    .line 1722080
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->p:LX/87V;

    check-cast v6, LX/0is;

    invoke-virtual {v0, v6}, LX/87V;->a(LX/0is;)LX/0Px;

    move-result-object v3

    .line 1722081
    if-eqz v2, :cond_2

    if-eqz v3, :cond_2

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1722082
    :cond_2
    :goto_0
    return-void

    .line 1722083
    :cond_3
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->O:LX/0Px;

    invoke-static {v0, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->j(Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->Q:Z

    if-ne v0, v12, :cond_4

    .line 1722084
    iput-object v2, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->P:Ljava/lang/String;

    goto :goto_0

    .line 1722085
    :cond_4
    iput-object v3, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->O:LX/0Px;

    .line 1722086
    iput-boolean v12, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->Q:Z

    .line 1722087
    iput-boolean v5, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->a:Z

    .line 1722088
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 1722089
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v13

    move v1, v5

    :goto_1
    if-ge v1, v13, :cond_6

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    .line 1722090
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->s:LX/Aty;

    invoke-virtual {v9}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6, v12}, LX/Aty;->a(Ljava/lang/String;Z)LX/AsM;

    move-result-object v0

    .line 1722091
    if-nez v0, :cond_5

    .line 1722092
    iget-object v6, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->j:LX/AsN;

    iget-object v7, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->l:LX/Arh;

    iget-object v8, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->m:Landroid/app/Activity;

    new-instance v10, LX/Ats;

    invoke-direct {v10, p0}, LX/Ats;-><init>(Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;)V

    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v11

    invoke-virtual/range {v6 .. v11}, LX/AsN;->a(LX/Arh;Landroid/app/Activity;Lcom/facebook/friendsharing/inspiration/model/InspirationModel;LX/Ats;Ljava/lang/Object;)LX/AsM;

    move-result-object v0

    .line 1722093
    iget-object v6, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->s:LX/Aty;

    invoke-virtual {v9}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object v7

    .line 1722094
    iget-object v8, v6, LX/Aty;->a:Ljava/util/Map;

    .line 1722095
    move-object v9, v7

    .line 1722096
    invoke-interface {v8, v9, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1722097
    iget-object v6, v0, LX/AsM;->m:LX/AtP;

    iget-object v7, v0, LX/AsM;->n:Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    invoke-virtual {v6, v7}, LX/AtP;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationModel;)V

    .line 1722098
    :cond_5
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1722099
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1722100
    :cond_6
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->x:LX/0Px;

    .line 1722101
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->P:Ljava/lang/String;

    invoke-static {v0, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1722102
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->s:LX/Aty;

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->P:Ljava/lang/String;

    invoke-virtual {v0, v1, v12}, LX/Aty;->a(Ljava/lang/String;Z)LX/AsM;

    move-result-object v0

    .line 1722103
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->P:Ljava/lang/String;

    if-eqz v1, :cond_7

    if-eqz v0, :cond_7

    .line 1722104
    iget-object v1, v0, LX/AsM;->m:LX/AtP;

    move-object v0, v1

    .line 1722105
    invoke-interface {v0}, LX/89u;->b()V

    .line 1722106
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->r:LX/ArS;

    invoke-virtual {v0}, LX/ArS;->b()V

    .line 1722107
    :cond_7
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-object v1, v0

    .line 1722108
    check-cast v1, LX/0is;

    invoke-interface {v1}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v3

    move-object v1, v0

    .line 1722109
    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v1

    .line 1722110
    iget-object v4, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->r:LX/ArS;

    check-cast v0, LX/0j0;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0, v3, v1}, LX/ArS;->a(Ljava/lang/String;Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;Lcom/facebook/friendsharing/inspiration/model/InspirationState;)V

    .line 1722111
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->s:LX/Aty;

    invoke-virtual {v0, v2, v12}, LX/Aty;->a(Ljava/lang/String;Z)LX/AsM;

    move-result-object v0

    .line 1722112
    iget-object v1, v0, LX/AsM;->m:LX/AtP;

    move-object v0, v1

    .line 1722113
    invoke-interface {v0}, LX/89u;->c()V

    .line 1722114
    iput-object v2, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->P:Ljava/lang/String;

    .line 1722115
    iput-boolean v5, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->M:Z

    .line 1722116
    :cond_8
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->f:LX/AuN;

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->x:LX/0Px;

    .line 1722117
    iput-object v1, v0, LX/AuN;->c:LX/0Px;

    .line 1722118
    iget-object v2, v0, LX/AuN;->b:LX/Atx;

    invoke-virtual {v2}, LX/Atx;->a()V

    .line 1722119
    goto/16 :goto_0
.end method

.method private static d(Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;)V
    .locals 2

    .prologue
    .line 1722060
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->t:LX/9d5;

    if-eqz v0, :cond_0

    .line 1722061
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->t:LX/9d5;

    .line 1722062
    iget-object v1, v0, LX/9d5;->L:LX/9dI;

    .line 1722063
    const/4 v0, 0x1

    iput-boolean v0, v1, LX/9dI;->B:Z

    .line 1722064
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->n:LX/8GL;

    invoke-virtual {v0}, LX/8GL;->b()V

    .line 1722065
    return-void
.end method

.method private static e$redex0(Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;)V
    .locals 1

    .prologue
    .line 1722056
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->t:LX/9d5;

    if-eqz v0, :cond_0

    .line 1722057
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->t:LX/9d5;

    invoke-virtual {v0}, LX/9d5;->c()V

    .line 1722058
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->n:LX/8GL;

    invoke-virtual {v0}, LX/8GL;->c()V

    .line 1722059
    return-void
.end method

.method public static h(Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;)V
    .locals 11

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1721994
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->k:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1721995
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->y:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->z:I

    if-gtz v0, :cond_1

    .line 1721996
    :cond_0
    :goto_0
    return-void

    .line 1721997
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->a:Z

    if-eqz v0, :cond_0

    .line 1721998
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 1721999
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->t:LX/9d5;

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->w:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    iget v3, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->y:I

    iget v4, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->z:I

    invoke-virtual {v1, v2, v3, v4, v5}, LX/9d5;->a(Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;IIZ)V

    .line 1722000
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->A:Landroid/view/ViewParent;

    if-eqz v1, :cond_2

    .line 1722001
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->t:LX/9d5;

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->A:Landroid/view/ViewParent;

    invoke-virtual {v1, v5, v2}, LX/9d5;->a(ZLandroid/view/ViewParent;)V

    .line 1722002
    :cond_2
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->t:LX/9d5;

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->B:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, LX/9d5;->a(Landroid/view/View$OnClickListener;)V

    .line 1722003
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->t:LX/9d5;

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->C:LX/Ar0;

    invoke-virtual {v1, v2}, LX/9d5;->a(LX/Ar0;)V

    .line 1722004
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->t:LX/9d5;

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->D:Landroid/view/View$OnClickListener;

    .line 1722005
    iget-object v3, v1, LX/9d5;->L:LX/9dI;

    .line 1722006
    iput-object v2, v3, LX/9dI;->y:Landroid/view/View$OnClickListener;

    .line 1722007
    move-object v1, v0

    .line 1722008
    check-cast v1, LX/0is;

    invoke-interface {v1}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getSelectedId()Ljava/lang/String;

    move-result-object v2

    .line 1722009
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->h:LX/8GP;

    iget v3, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->y:I

    iget v4, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->z:I

    invoke-virtual {v1, v3, v4}, LX/8GP;->a(II)LX/8GO;

    move-result-object v4

    .line 1722010
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->x:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v7

    const/4 v1, 0x0

    move v3, v1

    :goto_1
    if-ge v3, v7, :cond_4

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->x:LX/0Px;

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/AsM;

    .line 1722011
    iget v8, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->y:I

    iget v9, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->z:I

    .line 1722012
    iput v8, v1, LX/AsM;->o:I

    .line 1722013
    iput v9, v1, LX/AsM;->p:I

    .line 1722014
    iget-object v10, v1, LX/AsM;->m:LX/AtP;

    invoke-virtual {v10}, LX/AtP;->d()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v10

    move-object v1, v10

    .line 1722015
    if-eqz v1, :cond_3

    .line 1722016
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1722017
    invoke-static {v4, v1}, LX/8GO;->b(LX/8GO;Lcom/facebook/photos/creativeediting/model/SwipeableParams;)V

    .line 1722018
    :cond_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 1722019
    :cond_4
    invoke-virtual {v4}, LX/8GO;->b()LX/0Px;

    move-result-object v1

    move-object v1, v1

    .line 1722020
    iput-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->v:LX/0Px;

    .line 1722021
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->t:LX/9d5;

    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->v:LX/0Px;

    invoke-virtual {v1, v3, v2}, LX/9d5;->a(LX/0Px;Ljava/lang/String;)V

    .line 1722022
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->t:LX/9d5;

    invoke-virtual {v1, v5}, LX/9d5;->b(Z)V

    .line 1722023
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->n:LX/8GL;

    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->v:LX/0Px;

    invoke-virtual {v1, v6, v3, v2, v6}, LX/8GL;->a(ZLX/0Px;Ljava/lang/String;Z)V

    .line 1722024
    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->n:LX/8GL;

    iget-object v4, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->p:LX/87V;

    move-object v1, v0

    check-cast v1, LX/0is;

    .line 1722025
    invoke-virtual {v4, v1}, LX/87V;->b(LX/0is;)LX/0Px;

    move-result-object v5

    invoke-interface {v1}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getStarId()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, LX/87V;->a(LX/0Px;Ljava/lang/String;)Z

    move-result v5

    move v1, v5

    .line 1722026
    iget-object v4, v3, LX/8GL;->c:Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;

    .line 1722027
    iput-boolean v1, v4, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->k:Z

    .line 1722028
    check-cast v0, LX/0io;

    invoke-static {v0}, LX/87R;->a(LX/0io;)Z

    move-result v0

    .line 1722029
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->s:LX/Aty;

    invoke-virtual {v1, v2, v0}, LX/Aty;->a(Ljava/lang/String;Z)LX/AsM;

    move-result-object v0

    .line 1722030
    iget-object v1, v0, LX/AsM;->m:LX/AtP;

    move-object v0, v1

    .line 1722031
    invoke-interface {v0}, LX/89u;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0}, LX/89u;->h()Landroid/net/Uri;

    move-result-object v2

    invoke-static {p0, v1, v2}, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->a(Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;Ljava/lang/String;Landroid/net/Uri;)V

    .line 1722032
    invoke-interface {v0}, LX/89u;->f()Ljava/lang/String;

    move-result-object v1

    const/4 v8, 0x0

    .line 1722033
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1722034
    invoke-static {p0, v8}, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->b$redex0(Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;Z)V

    .line 1722035
    :cond_5
    :goto_2
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->o:LX/Aqz;

    invoke-interface {v0}, LX/89u;->e()Landroid/net/Uri;

    move-result-object v0

    .line 1722036
    iget-object v2, v1, LX/Aqz;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    const/4 v4, 0x0

    .line 1722037
    invoke-static {v2}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->aa(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v1

    .line 1722038
    if-nez v1, :cond_9

    .line 1722039
    :cond_6
    :goto_3
    goto/16 :goto_0

    .line 1722040
    :cond_7
    iget-object v7, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->E:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v7}, Lcom/facebook/resources/ui/FbTextView;->getVisibility()I

    move-result v7

    if-nez v7, :cond_8

    iget-object v7, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->E:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v7}, Lcom/facebook/resources/ui/FbTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 1722041
    :cond_8
    iput-boolean v8, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->L:Z

    .line 1722042
    iget-object v7, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->E:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v7, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1722043
    iget-object v7, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->E:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v7, v8}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1722044
    iget-object v7, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->E:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v7}, Lcom/facebook/resources/ui/FbTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-virtual {v7, v8}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    const-wide/16 v9, 0x12c

    invoke-virtual {v7, v9, v10}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    const-wide/16 v9, 0xc8

    invoke-virtual {v7, v9, v10}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    new-instance v8, LX/Atv;

    invoke-direct {v8, p0}, LX/Atv;-><init>(Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;)V

    invoke-virtual {v7, v8}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    goto :goto_2

    .line 1722045
    :cond_9
    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v3

    if-eqz v3, :cond_a

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 1722046
    :goto_4
    invoke-static {v3, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 1722047
    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v3

    if-eqz v3, :cond_b

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v3

    .line 1722048
    :goto_5
    invoke-static {v1}, LX/7kv;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)LX/7kv;

    move-result-object v1

    if-nez v0, :cond_c

    :goto_6
    invoke-virtual {v3, v4}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setEditedUri(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v3

    .line 1722049
    iput-object v3, v1, LX/7kv;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1722050
    move-object v3, v1

    .line 1722051
    invoke-virtual {v3}, LX/7kv;->a()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v3

    .line 1722052
    iget-object v4, v2, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v4}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getMediaSource()LX/875;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;Lcom/facebook/composer/attachments/ComposerAttachment;LX/875;)V

    goto/16 :goto_3

    :cond_a
    move-object v3, v4

    .line 1722053
    goto :goto_4

    .line 1722054
    :cond_b
    invoke-static {}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->newBuilder()Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v3

    goto :goto_5

    .line 1722055
    :cond_c
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_6
.end method

.method public static j(Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1721989
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->t:LX/9d5;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->t:LX/9d5;

    invoke-virtual {v0}, LX/9d5;->k()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1721990
    :cond_0
    const/4 v0, 0x0

    .line 1721991
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->t:LX/9d5;

    invoke-virtual {v0}, LX/9d5;->k()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v0

    .line 1721992
    iget-object p0, v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    move-object v0, p0

    .line 1721993
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 0

    .prologue
    .line 1721988
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1721955
    check-cast p1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 1721956
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1721957
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-object v2, v1

    .line 1721958
    check-cast v2, LX/0io;

    invoke-static {v2}, LX/87R;->a(LX/0io;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1721959
    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getCaptureState()LX/86q;

    move-result-object v2

    sget-object v3, LX/86q;->UNINITIALIZED:LX/86q;

    if-eq v2, v3, :cond_7

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 1721960
    if-eqz v2, :cond_1

    .line 1721961
    :cond_0
    invoke-direct {p0}, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->c()V

    :cond_1
    move-object v2, p1

    .line 1721962
    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v2}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v2

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-static {v2, v0}, LX/87N;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1721963
    invoke-direct {p0, v1}, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)V

    .line 1721964
    :cond_2
    invoke-static {p1, v1}, LX/Arx;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;Lcom/facebook/composer/system/model/ComposerModelImpl;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1721965
    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->e$redex0(Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;)V

    .line 1721966
    :cond_3
    :goto_1
    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->m()Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

    move-result-object v1

    move-object v0, p1

    .line 1721967
    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->m()Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

    move-result-object v0

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1721968
    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->getRight()I

    move-result v0

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->getLeft()I

    move-result v2

    sub-int/2addr v0, v2

    move v0, v0

    .line 1721969
    iput v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->y:I

    .line 1721970
    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->getBottom()I

    move-result v0

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->getTop()I

    move-result v2

    sub-int/2addr v0, v2

    move v0, v0

    .line 1721971
    iput v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->z:I

    .line 1721972
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->w:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1721973
    iget v2, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->y:I

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 1721974
    iget v2, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->z:I

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 1721975
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->getTop()I

    move-result v3

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iget p2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {v0, v2, v3, v4, p2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 1721976
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->w:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    invoke-virtual {v2, v0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1721977
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->w:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->requestLayout()V

    .line 1721978
    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->h(Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;)V

    .line 1721979
    :cond_4
    check-cast p1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {p1, v0}, LX/87R;->c(Lcom/facebook/composer/system/model/ComposerModelImpl;Lcom/facebook/composer/system/model/ComposerModelImpl;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1721980
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isLocationFetchAndRequeryingEffectsInProgress()Z

    move-result v2

    .line 1721981
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->x:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_6

    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->x:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AsM;

    .line 1721982
    iget-object v4, v0, LX/AsM;->m:LX/AtP;

    invoke-virtual {v4, v2}, LX/AtP;->a(Z)V

    .line 1721983
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1721984
    :cond_5
    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getCaptureState()LX/86q;

    move-result-object v0

    sget-object v2, LX/86q;->RECORDING_VIDEO:LX/86q;

    if-ne v0, v2, :cond_8

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getCaptureState()LX/86q;

    move-result-object v0

    sget-object v2, LX/86q;->RECORDING_VIDEO:LX/86q;

    if-eq v0, v2, :cond_8

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getCaptureState()LX/86q;

    move-result-object v0

    sget-object v2, LX/86q;->FLIPPING:LX/86q;

    if-eq v0, v2, :cond_8

    const/4 v0, 0x1

    :goto_3
    move v0, v0

    .line 1721985
    if-eqz v0, :cond_3

    .line 1721986
    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->d(Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;)V

    goto/16 :goto_1

    .line 1721987
    :cond_6
    return-void

    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_8
    const/4 v0, 0x0

    goto :goto_3
.end method
