.class public Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/As8;
.implements LX/AsV;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static final b:Landroid/graphics/DrawFilter;


# instance fields
.field public final c:Landroid/content/res/Resources;

.field public final d:LX/1Uo;

.field public final e:LX/1Ad;

.field private final f:LX/1FZ;

.field public final g:LX/1o9;

.field private final h:LX/AsT;

.field public final i:LX/33B;

.field private final j:I

.field public final k:I

.field public final l:I

.field public final m:I

.field public final n:LX/Ash;

.field public final o:LX/1af;

.field private p:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1720808
    const-class v0, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1720809
    new-instance v0, Landroid/graphics/PaintFlagsDrawFilter;

    const/4 v1, 0x0

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Landroid/graphics/PaintFlagsDrawFilter;-><init>(II)V

    sput-object v0, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;->b:Landroid/graphics/DrawFilter;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1Uo;LX/1Ad;LX/1FZ;LX/AsT;)V
    .locals 3
    .param p5    # LX/AsT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1720762
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1720763
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;->c:Landroid/content/res/Resources;

    .line 1720764
    iput-object p2, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;->d:LX/1Uo;

    .line 1720765
    iput-object p3, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;->e:LX/1Ad;

    .line 1720766
    iput-object p4, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;->f:LX/1FZ;

    .line 1720767
    iput-object p5, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;->h:LX/AsT;

    .line 1720768
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1a75    # 1.8490006E38f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;->j:I

    .line 1720769
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1a76    # 1.8490008E38f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;->k:I

    .line 1720770
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1a77

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;->l:I

    .line 1720771
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;->l:I

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;->m:I

    .line 1720772
    new-instance v0, LX/Ash;

    invoke-direct {v0, p0}, LX/Ash;-><init>(Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;)V

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;->n:LX/Ash;

    .line 1720773
    new-instance v0, LX/1o9;

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;->j:I

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;->j:I

    invoke-direct {v0, v1, v2}, LX/1o9;-><init>(II)V

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;->g:LX/1o9;

    .line 1720774
    new-instance v0, LX/Asg;

    invoke-direct {v0, p0}, LX/Asg;-><init>(Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;)V

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;->i:LX/33B;

    .line 1720775
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;->d:LX/1Uo;

    .line 1720776
    invoke-static {v0}, LX/1Uo;->v(LX/1Uo;)V

    .line 1720777
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;->d:LX/1Uo;

    const/4 v1, 0x0

    .line 1720778
    iput v1, v0, LX/1Uo;->d:I

    .line 1720779
    move-object v0, v0

    .line 1720780
    const v1, 0x7f021550

    invoke-virtual {v0, v1}, LX/1Uo;->b(I)LX/1Uo;

    move-result-object v0

    const v1, 0x7f021550

    .line 1720781
    iget-object v2, v0, LX/1Uo;->c:Landroid/content/res/Resources;

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v0, LX/1Uo;->j:Landroid/graphics/drawable/Drawable;

    .line 1720782
    move-object v0, v0

    .line 1720783
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;->c:Landroid/content/res/Resources;

    const v2, 0x7f02154f    # 1.7291028E38f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1720784
    iput-object v1, v0, LX/1Uo;->r:Landroid/graphics/drawable/Drawable;

    .line 1720785
    move-object v0, v0

    .line 1720786
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;->c:Landroid/content/res/Resources;

    const v2, 0x7f021551

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Uo;->f(Landroid/graphics/drawable/Drawable;)LX/1Uo;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;->n:LX/Ash;

    invoke-virtual {v0, v1}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v0

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    move-object v0, v0

    .line 1720787
    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;->o:LX/1af;

    .line 1720788
    return-void
.end method


# virtual methods
.method public final a()LX/AsT;
    .locals 1

    .prologue
    .line 1720807
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;->h:LX/AsT;

    return-object v0
.end method

.method public final a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V
    .locals 4

    .prologue
    .line 1720792
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;->p:Landroid/net/Uri;

    if-nez v0, :cond_0

    .line 1720793
    const v0, 0x7f021550

    invoke-virtual {p1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageResource(I)V

    .line 1720794
    :goto_0
    return-void

    .line 1720795
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;->p:Landroid/net/Uri;

    .line 1720796
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;->o:LX/1af;

    invoke-virtual {p1, v1}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 1720797
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;->e:LX/1Ad;

    invoke-virtual {v1}, LX/1Ad;->o()LX/1Ad;

    .line 1720798
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;->e:LX/1Ad;

    sget-object v2, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;->g:LX/1o9;

    .line 1720799
    iput-object v3, v2, LX/1bX;->c:LX/1o9;

    .line 1720800
    move-object v2, v2

    .line 1720801
    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;->i:LX/33B;

    .line 1720802
    iput-object v3, v2, LX/1bX;->j:LX/33B;

    .line 1720803
    move-object v2, v2

    .line 1720804
    invoke-virtual {v2}, LX/1bX;->n()LX/1bf;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    move-object v1, v1

    .line 1720805
    invoke-virtual {p1, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1720806
    goto :goto_0
.end method

.method public final a(Lcom/facebook/media/util/model/MediaModel;)V
    .locals 1

    .prologue
    .line 1720789
    iget-object v0, p1, Lcom/facebook/media/util/model/MediaModel;->d:Landroid/net/Uri;

    move-object v0, v0

    .line 1720790
    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;->p:Landroid/net/Uri;

    .line 1720791
    return-void
.end method
