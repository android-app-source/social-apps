.class public Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0iK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0is;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$ProvidesInspirationState;",
        "DerivedData:",
        "Ljava/lang/Object;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$SetsInspirationState",
        "<TMutation;>;Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0im",
        "<TMutation;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;"
    }
.end annotation


# static fields
.field public static final a:LX/0jK;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field public static final c:Lcom/facebook/location/FbLocationOperationParams;


# instance fields
.field public final d:LX/0il;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TServices;"
        }
    .end annotation
.end field

.field public final e:LX/Ata;

.field public final f:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/1Ck;

.field public final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1sS;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/Awo;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 1722601
    const-class v0, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;

    invoke-static {v0}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->a:LX/0jK;

    .line 1722602
    const-class v0, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 1722603
    sget-object v0, LX/0yF;->HIGH_ACCURACY:LX/0yF;

    invoke-static {v0}, Lcom/facebook/location/FbLocationOperationParams;->a(LX/0yF;)LX/1S7;

    move-result-object v0

    const-wide/32 v2, 0xdbba0

    .line 1722604
    iput-wide v2, v0, LX/1S7;->b:J

    .line 1722605
    move-object v0, v0

    .line 1722606
    const/high16 v1, 0x44960000    # 1200.0f

    .line 1722607
    iput v1, v0, LX/1S7;->c:F

    .line 1722608
    move-object v0, v0

    .line 1722609
    const-wide/16 v2, 0x3a98

    .line 1722610
    iput-wide v2, v0, LX/1S7;->d:J

    .line 1722611
    move-object v0, v0

    .line 1722612
    invoke-virtual {v0}, LX/1S7;->a()Lcom/facebook/location/FbLocationOperationParams;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->c:Lcom/facebook/location/FbLocationOperationParams;

    return-void
.end method

.method public constructor <init>(LX/0il;LX/Ata;Landroid/view/ViewStub;LX/0Or;LX/1Ck;Landroid/content/res/Resources;)V
    .locals 3
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Ata;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/view/ViewStub;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "LX/Ata;",
            "Landroid/view/ViewStub;",
            "LX/0Or",
            "<",
            "LX/1sS;",
            ">;",
            "LX/1Ck;",
            "Landroid/content/res/Resources;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1722613
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1722614
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->d:LX/0il;

    .line 1722615
    iput-object p2, p0, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->e:LX/Ata;

    .line 1722616
    new-instance v0, LX/0zw;

    invoke-direct {v0, p3}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->f:LX/0zw;

    .line 1722617
    iput-object p5, p0, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->g:LX/1Ck;

    .line 1722618
    iput-object p4, p0, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->h:LX/0Or;

    .line 1722619
    new-instance v0, LX/Awo;

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    const/high16 v2, 0x10e0000

    invoke-virtual {p6, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/Awo;-><init>(Landroid/animation/TimeInterpolator;I)V

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->i:LX/Awo;

    .line 1722620
    return-void
.end method

.method public static a$redex0(Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;Z)V
    .locals 2

    .prologue
    .line 1722621
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->d:LX/0il;

    check-cast v0, LX/0im;

    invoke-interface {v0}, LX/0im;->c()LX/0jJ;

    move-result-object v0

    sget-object v1, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->a:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->d:LX/0il;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0is;

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->setIsLocationFetchAndRequeryingEffectsInProgress(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 1722622
    return-void
.end method

.method public static d(Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;)V
    .locals 5

    .prologue
    .line 1722623
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->f:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->i:LX/Awo;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->i:LX/Awo;

    invoke-virtual {v1}, LX/Awo;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->i:LX/Awo;

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->f:LX/0zw;

    invoke-virtual {v2}, LX/0zw;->a()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/Awo;->b(Landroid/view/View;)Landroid/animation/Animator$AnimatorListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 1722624
    sget-object v0, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->a:LX/0jK;

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->d:LX/0il;

    sget-object v2, LX/86t;->LOCATION_NUX:LX/86t;

    .line 1722625
    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v3}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->getInspirationBackStack()LX/0Px;

    move-result-object v3

    .line 1722626
    invoke-static {v3, v2}, LX/87R;->b(LX/0Px;LX/86t;)LX/0Px;

    move-result-object p0

    move-object v3, v1

    .line 1722627
    check-cast v3, LX/0im;

    invoke-interface {v3}, LX/0im;->c()LX/0jJ;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v3

    check-cast v3, LX/0jL;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v4}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v4

    invoke-virtual {v4, p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->setInspirationBackStack(LX/0Px;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v4

    const/4 p0, 0x0

    invoke-virtual {v4, p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->setIsBackPressed(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0jL;

    invoke-virtual {v3}, LX/0jL;->a()V

    .line 1722628
    return-void
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 0

    .prologue
    .line 1722629
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 1722630
    check-cast p1, LX/0is;

    .line 1722631
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->d:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0is;

    invoke-interface {v0}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getSelectedId()Ljava/lang/String;

    move-result-object v1

    .line 1722632
    invoke-interface {p1}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getSelectedId()Ljava/lang/String;

    move-result-object v2

    .line 1722633
    invoke-interface {p1}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getSelectedId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1722634
    const-string v0, "178267072637018"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->d:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0is;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isLocationFetchAndRequeryingEffectsInProgress()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1722635
    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->f:LX/0zw;

    invoke-virtual {v3}, LX/0zw;->b()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1722636
    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->f:LX/0zw;

    invoke-virtual {v3}, LX/0zw;->a()Landroid/view/View;

    move-result-object v3

    const v4, 0x3a83126f    # 0.001f

    invoke-virtual {v3, v4}, Landroid/view/View;->setAlpha(F)V

    .line 1722637
    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->f:LX/0zw;

    invoke-virtual {v3}, LX/0zw;->a()Landroid/view/View;

    move-result-object v3

    const/high16 v4, -0x1000000

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundColor(I)V

    .line 1722638
    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->f:LX/0zw;

    invoke-virtual {v3}, LX/0zw;->a()Landroid/view/View;

    move-result-object v3

    const v4, 0x7f0d19dc

    invoke-static {v3, v4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/resources/ui/FbButton;

    .line 1722639
    new-instance v4, LX/AuT;

    invoke-direct {v4, p0}, LX/AuT;-><init>(Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;)V

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1722640
    :cond_0
    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->d:LX/0il;

    sget-object v4, LX/86t;->LOCATION_NUX:LX/86t;

    sget-object v5, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->a:LX/0jK;

    invoke-static {v3, v4, v5}, LX/87R;->a(LX/0il;LX/86t;LX/0jK;)V

    .line 1722641
    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->f:LX/0zw;

    invoke-virtual {v3}, LX/0zw;->a()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->i:LX/Awo;

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->i:LX/Awo;

    invoke-virtual {v4}, LX/Awo;->b()J

    move-result-wide v5

    invoke-virtual {v3, v5, v6}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->i:LX/Awo;

    iget-object v5, p0, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->f:LX/0zw;

    invoke-virtual {v5}, LX/0zw;->a()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/Awo;->a(Landroid/view/View;)Landroid/animation/Animator$AnimatorListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 1722642
    :cond_1
    :goto_0
    check-cast p1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->d:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0is;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    sget-object v2, LX/86t;->LOCATION_NUX:LX/86t;

    invoke-static {v1, v0, v2}, LX/87R;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;Lcom/facebook/friendsharing/inspiration/model/InspirationState;LX/86t;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1722643
    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->d(Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;)V

    .line 1722644
    :cond_2
    return-void

    .line 1722645
    :cond_3
    const-string v0, "178267072637018"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "178267072637018"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1722646
    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;->d(Lcom/facebook/friendsharing/inspiration/controller/LocationNuxController;)V

    goto :goto_0
.end method
