.class public final Lcom/facebook/friendsharing/inspiration/controller/InspirationProgressController$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/Ath;


# direct methods
.method public constructor <init>(LX/Ath;)V
    .locals 0

    .prologue
    .line 1721666
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationProgressController$1;->a:LX/Ath;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 1721667
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationProgressController$1;->a:LX/Ath;

    iget-object v0, v0, LX/Ath;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    move-object v1, v0

    .line 1721668
    check-cast v1, LX/0im;

    invoke-interface {v1}, LX/0im;->c()LX/0jJ;

    move-result-object v1

    const-class v2, LX/Ath;

    invoke-static {v2}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->setIsRecordingAtLimit(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 1721669
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationProgressController$1;->a:LX/Ath;

    iget-object v0, v0, LX/Ath;->b:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1721670
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/controller/InspirationProgressController$1;->a:LX/Ath;

    invoke-static {v0}, LX/Ath;->c$redex0(LX/Ath;)Lcom/facebook/facecast/view/CircularProgressView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/view/CircularProgressView;->setProgress(F)V

    .line 1721671
    return-void
.end method
