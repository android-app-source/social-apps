.class public final Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration_BuilderDeserializer;
.end annotation


# instance fields
.field public a:Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/ArJ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1726411
    const-class v0, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration_BuilderDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1726409
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1726410
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;
    .locals 2

    .prologue
    .line 1726404
    new-instance v0, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;

    invoke-direct {v0, p0}, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;-><init>(Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration$Builder;)V

    return-object v0
.end method

.method public setCTAConfig(Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;)Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration$Builder;
    .locals 0
    .param p1    # Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "c_t_a_config"
    .end annotation

    .prologue
    .line 1726407
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration$Builder;->a:Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;

    .line 1726408
    return-object p0
.end method

.method public setStartReason(LX/ArJ;)Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration$Builder;
    .locals 0
    .param p1    # LX/ArJ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "start_reason"
    .end annotation

    .prologue
    .line 1726405
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration$Builder;->b:LX/ArJ;

    .line 1726406
    return-object p0
.end method
