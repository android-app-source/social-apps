.class public Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfigurationSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1726467
    const-class v0, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;

    new-instance v1, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfigurationSerializer;

    invoke-direct {v1}, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfigurationSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1726468
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1726466
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1726460
    if-nez p0, :cond_0

    .line 1726461
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1726462
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1726463
    invoke-static {p0, p1, p2}, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfigurationSerializer;->b(Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;LX/0nX;LX/0my;)V

    .line 1726464
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1726465
    return-void
.end method

.method private static b(Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1726457
    const-string v0, "c_t_a_config"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;->getCTAConfig()Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1726458
    const-string v0, "start_reason"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;->getStartReason()LX/ArJ;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1726459
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1726456
    check-cast p1, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;

    invoke-static {p1, p2, p3}, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfigurationSerializer;->a(Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;LX/0nX;LX/0my;)V

    return-void
.end method
