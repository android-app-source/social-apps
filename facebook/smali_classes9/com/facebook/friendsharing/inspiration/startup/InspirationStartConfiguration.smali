.class public Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfigurationSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:LX/ArJ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1726454
    const-class v0, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1726453
    const-class v0, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfigurationSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1726452
    new-instance v0, LX/AwM;

    invoke-direct {v0}, LX/AwM;-><init>()V

    sput-object v0, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1726444
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1726445
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 1726446
    iput-object v1, p0, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;->a:Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;

    .line 1726447
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    .line 1726448
    iput-object v1, p0, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;->b:LX/ArJ;

    .line 1726449
    :goto_1
    return-void

    .line 1726450
    :cond_0
    sget-object v0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;->a:Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;

    goto :goto_0

    .line 1726451
    :cond_1
    invoke-static {}, LX/ArJ;->values()[LX/ArJ;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;->b:LX/ArJ;

    goto :goto_1
.end method

.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration$Builder;)V
    .locals 1

    .prologue
    .line 1726430
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1726431
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration$Builder;->a:Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;->a:Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;

    .line 1726432
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration$Builder;->b:LX/ArJ;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;->b:LX/ArJ;

    .line 1726433
    return-void
.end method

.method public static newBuilder()Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration$Builder;
    .locals 2

    .prologue
    .line 1726443
    new-instance v0, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration$Builder;

    invoke-direct {v0}, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1726455
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1726434
    if-ne p0, p1, :cond_1

    .line 1726435
    :cond_0
    :goto_0
    return v0

    .line 1726436
    :cond_1
    instance-of v2, p1, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;

    if-nez v2, :cond_2

    move v0, v1

    .line 1726437
    goto :goto_0

    .line 1726438
    :cond_2
    check-cast p1, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;

    .line 1726439
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;->a:Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;->a:Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1726440
    goto :goto_0

    .line 1726441
    :cond_3
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;->b:LX/ArJ;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;->b:LX/ArJ;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1726442
    goto :goto_0
.end method

.method public getCTAConfig()Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "c_t_a_config"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1726429
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;->a:Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;

    return-object v0
.end method

.method public getStartReason()LX/ArJ;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "start_reason"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1726428
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;->b:LX/ArJ;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1726427
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;->a:Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;->b:LX/ArJ;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1726418
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;->a:Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;

    if-nez v0, :cond_0

    .line 1726419
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1726420
    :goto_0
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;->b:LX/ArJ;

    if-nez v0, :cond_1

    .line 1726421
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1726422
    :goto_1
    return-void

    .line 1726423
    :cond_0
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1726424
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;->a:Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 1726425
    :cond_1
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1726426
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/startup/InspirationStartConfiguration;->b:LX/ArJ;

    invoke-virtual {v0}, LX/ArJ;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1
.end method
