.class public final Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;)V
    .locals 0

    .prologue
    .line 1727118
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner$1;->a:Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 1727119
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner$1;->a:Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner$1;->a:Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;->setPivotX(F)V

    .line 1727120
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner$1;->a:Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner$1;->a:Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;->getMeasuredHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;->setPivotY(F)V

    .line 1727121
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner$1;->a:Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;

    invoke-static {v0}, Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;->c(Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;)V

    .line 1727122
    return-void
.end method
