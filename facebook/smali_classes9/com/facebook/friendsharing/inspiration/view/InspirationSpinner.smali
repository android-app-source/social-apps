.class public Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;
.super Lcom/facebook/widget/FbImageView;
.source ""


# instance fields
.field private final a:LX/Awo;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1727123
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1727124
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 1727125
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/FbImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1727126
    new-instance v0, LX/Awo;

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    const/16 v2, 0xc8

    invoke-direct {v0, v1, v2}, LX/Awo;-><init>(Landroid/animation/TimeInterpolator;I)V

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;->a:LX/Awo;

    .line 1727127
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;->a(Landroid/content/Context;)Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1727128
    invoke-virtual {p0, v3}, Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;->setScaleX(F)V

    .line 1727129
    invoke-virtual {p0, v3}, Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;->setScaleY(F)V

    .line 1727130
    const v0, 0x3a83126f    # 0.001f

    invoke-virtual {p0, v0}, Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;->setAlpha(F)V

    .line 1727131
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/facebook/drawee/drawable/AutoRotateDrawable;
    .locals 3

    .prologue
    .line 1727132
    new-instance v0, Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020da4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const/16 v2, 0x708

    invoke-direct {v0, v1, v2}, Lcom/facebook/drawee/drawable/AutoRotateDrawable;-><init>(Landroid/graphics/drawable/Drawable;I)V

    return-object v0
.end method

.method public static c(Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 1727133
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;->a:LX/Awo;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;->a:LX/Awo;

    invoke-virtual {v1}, LX/Awo;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;->a:LX/Awo;

    invoke-virtual {v1, p0}, LX/Awo;->a(Landroid/view/View;)Landroid/animation/Animator$AnimatorListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 1727134
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1727135
    new-instance v0, Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner$1;

    invoke-direct {v0, p0}, Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner$1;-><init>(Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;)V

    invoke-static {p0, v0}, LX/Awl;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 1727136
    return-void
.end method

.method public final b()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1727137
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;->a:LX/Awo;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;->a:LX/Awo;

    invoke-virtual {v1}, LX/Awo;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationSpinner;->a:LX/Awo;

    invoke-virtual {v1, p0}, LX/Awo;->b(Landroid/view/View;)Landroid/animation/Animator$AnimatorListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 1727138
    return-void
.end method
