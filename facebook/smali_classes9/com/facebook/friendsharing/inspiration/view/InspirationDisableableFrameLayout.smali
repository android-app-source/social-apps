.class public Lcom/facebook/friendsharing/inspiration/view/InspirationDisableableFrameLayout;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1726959
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1726960
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationDisableableFrameLayout;->a:Z

    .line 1726961
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1726947
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1726948
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationDisableableFrameLayout;->a:Z

    .line 1726949
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1726956
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1726957
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationDisableableFrameLayout;->a:Z

    .line 1726958
    return-void
.end method


# virtual methods
.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1726953
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationDisableableFrameLayout;->a:Z

    if-nez v0, :cond_0

    .line 1726954
    const/4 v0, 0x1

    .line 1726955
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public setEnabled(Z)V
    .locals 0

    .prologue
    .line 1726950
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->setEnabled(Z)V

    .line 1726951
    iput-boolean p1, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationDisableableFrameLayout;->a:Z

    .line 1726952
    return-void
.end method
