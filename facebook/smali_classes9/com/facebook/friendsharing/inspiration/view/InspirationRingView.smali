.class public Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;
.super Landroid/view/View;
.source ""


# instance fields
.field private a:Landroid/graphics/Paint;

.field private b:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1727091
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1727092
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1727093
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1727094
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 1727095
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1727096
    sget-object v0, LX/03r;->InspirationRingView:[I

    invoke-virtual {p1, p2, v0, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1727097
    if-nez v0, :cond_0

    .line 1727098
    :goto_0
    return-void

    .line 1727099
    :cond_0
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 1727100
    const/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    .line 1727101
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;->a:Landroid/graphics/Paint;

    .line 1727102
    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;->a:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1727103
    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;->a:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 1727104
    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;->a:Landroid/graphics/Paint;

    int-to-float v1, v1

    invoke-virtual {v3, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1727105
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;->a:Landroid/graphics/Paint;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1727106
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1727107
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;->b:Landroid/graphics/RectF;

    goto :goto_0
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 1727108
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1727109
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;->a:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    .line 1727110
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;->b:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;->getLeft()I

    move-result v2

    int-to-float v2, v2

    div-float v3, v0, v6

    add-float/2addr v2, v3

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;->getTop()I

    move-result v3

    int-to-float v3, v3

    div-float v4, v0, v6

    add-float/2addr v3, v4

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;->getRight()I

    move-result v4

    int-to-float v4, v4

    div-float v5, v0, v6

    sub-float/2addr v4, v5

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;->getBottom()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v0, v6

    sub-float v0, v5, v0

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1727111
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;->b:Landroid/graphics/RectF;

    const/4 v2, 0x0

    const/high16 v3, 0x43b40000    # 360.0f

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;->a:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 1727112
    return-void
.end method

.method public setRingThickness(F)V
    .locals 2

    .prologue
    .line 1727113
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Please set a non-zero positive thickness"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1727114
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1727115
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;->invalidate()V

    .line 1727116
    return-void

    .line 1727117
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
