.class public Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:Landroid/view/GestureDetector;

.field private b:Landroid/view/ScaleGestureDetector;

.field public c:Z

.field public d:Z

.field public e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/Ard;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/Arf;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/Asb;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/Asv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1727059
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1727060
    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->c:Z

    .line 1727061
    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->d:Z

    .line 1727062
    invoke-direct {p0}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->a()V

    .line 1727063
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1727049
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1727050
    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->c:Z

    .line 1727051
    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->d:Z

    .line 1727052
    invoke-direct {p0}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->a()V

    .line 1727053
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1727054
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1727055
    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->c:Z

    .line 1727056
    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->d:Z

    .line 1727057
    invoke-direct {p0}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->a()V

    .line 1727058
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1727035
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, LX/Awi;

    invoke-direct {v2, p0}, LX/Awi;-><init>(Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->a:Landroid/view/GestureDetector;

    .line 1727036
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->a:Landroid/view/GestureDetector;

    invoke-virtual {v0, v3}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    .line 1727037
    new-instance v0, Landroid/view/ScaleGestureDetector;

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, LX/Awj;

    invoke-direct {v2, p0}, LX/Awj;-><init>(Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;)V

    invoke-direct {v0, v1, v2}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->b:Landroid/view/ScaleGestureDetector;

    .line 1727038
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 1727039
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->b:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0, v3}, Landroid/view/ScaleGestureDetector;->setQuickScaleEnabled(Z)V

    .line 1727040
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_1

    .line 1727041
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->b:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0, v3}, Landroid/view/ScaleGestureDetector;->setStylusScaleEnabled(Z)V

    .line 1727042
    :cond_1
    invoke-static {}, LX/0RA;->b()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->f:Ljava/util/Set;

    .line 1727043
    invoke-static {}, LX/0RA;->b()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->e:Ljava/util/Set;

    .line 1727044
    invoke-static {}, LX/0RA;->b()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->g:Ljava/util/Set;

    .line 1727045
    invoke-static {}, LX/0RA;->b()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->h:Ljava/util/Set;

    .line 1727046
    return-void
.end method

.method private a(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1727064
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->d:Z

    if-eqz v0, :cond_0

    .line 1727065
    const/4 v0, 0x0

    .line 1727066
    :goto_0
    return v0

    .line 1727067
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->b:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1727068
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->c:Z

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/Ard;)V
    .locals 2

    .prologue
    .line 1727069
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->e:Ljava/util/Set;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1727070
    return-void
.end method

.method public final a(LX/Arf;)V
    .locals 2

    .prologue
    .line 1727071
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->f:Ljava/util/Set;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1727072
    return-void
.end method

.method public final a(LX/Asb;)V
    .locals 2

    .prologue
    .line 1727073
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->g:Ljava/util/Set;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1727074
    return-void
.end method

.method public final a(LX/Asv;)V
    .locals 1

    .prologue
    .line 1727075
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1727076
    return-void
.end method

.method public final b(LX/Ard;)Z
    .locals 1

    .prologue
    .line 1727047
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(LX/Arf;)Z
    .locals 1

    .prologue
    .line 1727048
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(LX/Asb;)Z
    .locals 1

    .prologue
    .line 1727019
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(LX/Asv;)Z
    .locals 1

    .prologue
    .line 1727020
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final c(LX/Ard;)Z
    .locals 1

    .prologue
    .line 1727021
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final c(LX/Arf;)Z
    .locals 1

    .prologue
    .line 1727022
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final c(LX/Asb;)Z
    .locals 1

    .prologue
    .line 1727023
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final c(LX/Asv;)Z
    .locals 1

    .prologue
    .line 1727024
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1727025
    invoke-direct {p0, p1}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->a(Landroid/view/MotionEvent;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1727026
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_2

    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->d:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1727027
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Asv;

    .line 1727028
    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->h:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1727029
    invoke-interface {v0, p1}, LX/Asv;->a(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 1727030
    :cond_1
    iput-boolean v1, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->d:Z

    .line 1727031
    :cond_2
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->a:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move v0, v1

    .line 1727032
    :cond_3
    return v0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x27bc4d8e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1727033
    invoke-direct {p0, p1}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->a(Landroid/view/MotionEvent;)Z

    move-result v1

    const v2, 0x795c676e

    invoke-static {v3, v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 2

    .prologue
    .line 1727034
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This class handles its own touch overriding, hence does not support setting a touch listener."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
