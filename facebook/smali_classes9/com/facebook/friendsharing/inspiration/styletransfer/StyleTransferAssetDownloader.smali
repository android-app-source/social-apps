.class public Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/89q;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "LX/89q",
        "<",
        "Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:LX/3AP;

.field private final c:Ljava/util/concurrent/ExecutorService;

.field private final d:LX/AwR;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1726922
    const-class v0, Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;

    const-string v1, "asset_download"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/1Gm;LX/0Zb;LX/1Gk;LX/1Gl;LX/1Go;LX/13t;Ljava/util/concurrent/ExecutorService;LX/AwR;)V
    .locals 11
    .param p9    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .param p10    # LX/AwR;
        .annotation runtime Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferCache;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1726923
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1726924
    new-instance v1, LX/3AP;

    const-string v4, "style_transfer"

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    move-object v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    invoke-direct/range {v1 .. v10}, LX/3AP;-><init>(Landroid/content/Context;Lcom/facebook/http/common/FbHttpRequestProcessor;Ljava/lang/String;LX/1Gm;LX/0Zb;LX/1Gk;LX/1Gl;LX/1Go;LX/13t;)V

    iput-object v1, p0, Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;->b:LX/3AP;

    .line 1726925
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;->c:Ljava/util/concurrent/ExecutorService;

    .line 1726926
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;->d:LX/AwR;

    .line 1726927
    return-void
.end method

.method public static a$redex0(Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1726928
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;->d:LX/AwR;

    invoke-virtual {v0, p2}, LX/AwR;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1726929
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;->d:LX/AwR;

    invoke-virtual {v1, p2}, LX/AwR;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1726930
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1726931
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;->b:LX/3AP;

    new-instance v1, LX/34X;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    new-instance v3, LX/Awh;

    iget-object v4, p0, Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;->d:LX/AwR;

    invoke-direct {v3, v4, p2}, LX/Awh;-><init>(LX/AwR;Ljava/lang/String;)V

    sget-object v4, Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v1, v2, v3, v4}, LX/34X;-><init>(Landroid/net/Uri;LX/1uy;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-virtual {v0, v1}, LX/3AP;->a(LX/34X;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    goto :goto_0
.end method

.method public static b(LX/0QB;)Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;
    .locals 11

    .prologue
    .line 1726932
    new-instance v0, Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v2

    check-cast v2, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-static {p0}, LX/1Gm;->a(LX/0QB;)LX/1Gm;

    move-result-object v3

    check-cast v3, LX/1Gm;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {p0}, LX/1Gk;->a(LX/0QB;)LX/1Gk;

    move-result-object v5

    check-cast v5, LX/1Gk;

    invoke-static {p0}, LX/1Gl;->a(LX/0QB;)LX/1Gl;

    move-result-object v6

    check-cast v6, LX/1Gl;

    invoke-static {p0}, LX/1Gn;->a(LX/0QB;)LX/1Gn;

    move-result-object v7

    check-cast v7, LX/1Go;

    invoke-static {p0}, LX/13t;->a(LX/0QB;)LX/13t;

    move-result-object v8

    check-cast v8, LX/13t;

    invoke-static {p0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/AwU;->a(LX/0QB;)LX/AwR;

    move-result-object v10

    check-cast v10, LX/AwR;

    invoke-direct/range {v0 .. v10}, Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;-><init>(Landroid/content/Context;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/1Gm;LX/0Zb;LX/1Gk;LX/1Gl;LX/1Go;LX/13t;Ljava/util/concurrent/ExecutorService;LX/AwR;)V

    .line 1726933
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/AwX;)V
    .locals 7

    .prologue
    .line 1726934
    iget-object v6, p0, Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader$2;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader$2;-><init>(Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/AwX;)V

    const v1, 0x1612e5db

    invoke-static {v6, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1726935
    return-void
.end method
