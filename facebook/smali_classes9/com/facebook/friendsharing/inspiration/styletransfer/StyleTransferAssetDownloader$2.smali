.class public final Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/AwX;

.field public final synthetic e:Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/AwX;)V
    .locals 0

    .prologue
    .line 1726913
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader$2;->e:Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;

    iput-object p2, p0, Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader$2;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader$2;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader$2;->c:Ljava/lang/String;

    iput-object p5, p0, Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader$2;->d:LX/AwX;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1726914
    :try_start_0
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader$2;->e:Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader$2;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader$2;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "InitNet"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;->a$redex0(Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 1726915
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader$2;->e:Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader$2;->c:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader$2;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "PredictNet"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;->a$redex0(Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 1726916
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 1726917
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader$2;->d:LX/AwX;

    invoke-interface {v2, v0, v1}, LX/AwX;->a(Ljava/io/File;Ljava/io/File;)V

    .line 1726918
    :goto_0
    return-void

    .line 1726919
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader$2;->d:LX/AwX;

    new-instance v1, Ljava/lang/Exception;

    const-string v2, "Failed to save model to cache"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, LX/AwX;->a(Ljava/lang/Exception;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1726920
    :catch_0
    move-exception v0

    .line 1726921
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader$2;->d:LX/AwX;

    invoke-interface {v1, v0}, LX/AwX;->a(Ljava/lang/Exception;)V

    goto :goto_0
.end method
