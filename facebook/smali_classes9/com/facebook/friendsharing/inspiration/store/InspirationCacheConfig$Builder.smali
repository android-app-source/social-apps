.class public final Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig_BuilderDeserializer;
.end annotation


# static fields
.field private static final a:Ljava/lang/Long;

.field private static final b:Ljava/lang/Long;


# instance fields
.field public c:Ljava/lang/Long;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/Long;

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/Long;

.field public g:Ljava/lang/Long;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1726507
    const-class v0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig_BuilderDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1726508
    new-instance v0, LX/AwO;

    invoke-direct {v0}, LX/AwO;-><init>()V

    .line 1726509
    const-wide/16 v1, 0x5

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    move-object v0, v1

    .line 1726510
    sput-object v0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;->a:Ljava/lang/Long;

    .line 1726511
    new-instance v0, LX/AwP;

    invoke-direct {v0}, LX/AwP;-><init>()V

    .line 1726512
    const-wide/16 v1, 0x1c

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    move-object v0, v1

    .line 1726513
    sput-object v0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;->b:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1726503
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1726504
    sget-object v0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;->a:Ljava/lang/Long;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;->d:Ljava/lang/Long;

    .line 1726505
    sget-object v0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;->b:Ljava/lang/Long;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;->f:Ljava/lang/Long;

    .line 1726506
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;
    .locals 2

    .prologue
    .line 1726502
    new-instance v0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;

    invoke-direct {v0, p0}, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;-><init>(Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;)V

    return-object v0
.end method

.method public setCapacity(Ljava/lang/Long;)Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;
    .locals 0
    .param p1    # Ljava/lang/Long;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "capacity"
    .end annotation

    .prologue
    .line 1726514
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;->c:Ljava/lang/Long;

    .line 1726515
    return-object p0
.end method

.method public setMaxSize(Ljava/lang/Long;)Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "max_size"
    .end annotation

    .prologue
    .line 1726500
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;->d:Ljava/lang/Long;

    .line 1726501
    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation

    .prologue
    .line 1726498
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;->e:Ljava/lang/String;

    .line 1726499
    return-object p0
.end method

.method public setStaleDays(Ljava/lang/Long;)Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "stale_days"
    .end annotation

    .prologue
    .line 1726494
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;->f:Ljava/lang/Long;

    .line 1726495
    return-object p0
.end method

.method public setVersion(Ljava/lang/Long;)Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;
    .locals 0
    .param p1    # Ljava/lang/Long;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "version"
    .end annotation

    .prologue
    .line 1726496
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;->g:Ljava/lang/Long;

    .line 1726497
    return-object p0
.end method
