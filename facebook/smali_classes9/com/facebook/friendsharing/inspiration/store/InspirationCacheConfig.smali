.class public Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfigSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/Long;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:Ljava/lang/Long;

.field private final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:Ljava/lang/Long;

.field private final e:Ljava/lang/Long;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1726582
    const-class v0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1726581
    const-class v0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfigSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1726580
    new-instance v0, LX/AwN;

    invoke-direct {v0}, LX/AwN;-><init>()V

    sput-object v0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1726567
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1726568
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 1726569
    iput-object v2, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->a:Ljava/lang/Long;

    .line 1726570
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->b:Ljava/lang/Long;

    .line 1726571
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    .line 1726572
    iput-object v2, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->c:Ljava/lang/String;

    .line 1726573
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->d:Ljava/lang/Long;

    .line 1726574
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_2

    .line 1726575
    iput-object v2, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->e:Ljava/lang/Long;

    .line 1726576
    :goto_2
    return-void

    .line 1726577
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->a:Ljava/lang/Long;

    goto :goto_0

    .line 1726578
    :cond_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->c:Ljava/lang/String;

    goto :goto_1

    .line 1726579
    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->e:Ljava/lang/Long;

    goto :goto_2
.end method

.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;)V
    .locals 1

    .prologue
    .line 1726522
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1726523
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;->c:Ljava/lang/Long;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->a:Ljava/lang/Long;

    .line 1726524
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;->d:Ljava/lang/Long;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->b:Ljava/lang/Long;

    .line 1726525
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->c:Ljava/lang/String;

    .line 1726526
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;->f:Ljava/lang/Long;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->d:Ljava/lang/Long;

    .line 1726527
    iget-object v0, p1, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;->g:Ljava/lang/Long;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->e:Ljava/lang/Long;

    .line 1726528
    return-void
.end method

.method public static newBuilder()Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;
    .locals 2

    .prologue
    .line 1726566
    new-instance v0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;

    invoke-direct {v0}, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1726565
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1726550
    if-ne p0, p1, :cond_1

    .line 1726551
    :cond_0
    :goto_0
    return v0

    .line 1726552
    :cond_1
    instance-of v2, p1, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;

    if-nez v2, :cond_2

    move v0, v1

    .line 1726553
    goto :goto_0

    .line 1726554
    :cond_2
    check-cast p1, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;

    .line 1726555
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->a:Ljava/lang/Long;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->a:Ljava/lang/Long;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1726556
    goto :goto_0

    .line 1726557
    :cond_3
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->b:Ljava/lang/Long;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->b:Ljava/lang/Long;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1726558
    goto :goto_0

    .line 1726559
    :cond_4
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->c:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 1726560
    goto :goto_0

    .line 1726561
    :cond_5
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->d:Ljava/lang/Long;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->d:Ljava/lang/Long;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1726562
    goto :goto_0

    .line 1726563
    :cond_6
    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->e:Ljava/lang/Long;

    iget-object v3, p1, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->e:Ljava/lang/Long;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1726564
    goto :goto_0
.end method

.method public getCapacity()Ljava/lang/Long;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "capacity"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1726549
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->a:Ljava/lang/Long;

    return-object v0
.end method

.method public getMaxSize()Ljava/lang/Long;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "max_size"
    .end annotation

    .prologue
    .line 1726548
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->b:Ljava/lang/Long;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1726547
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getStaleDays()Ljava/lang/Long;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "stale_days"
    .end annotation

    .prologue
    .line 1726546
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->d:Ljava/lang/Long;

    return-object v0
.end method

.method public getVersion()Ljava/lang/Long;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "version"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1726545
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->e:Ljava/lang/Long;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1726544
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->a:Ljava/lang/Long;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->b:Ljava/lang/Long;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->d:Ljava/lang/Long;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->e:Ljava/lang/Long;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1726529
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->a:Ljava/lang/Long;

    if-nez v0, :cond_0

    .line 1726530
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1726531
    :goto_0
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1726532
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1726533
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1726534
    :goto_1
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->d:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1726535
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->e:Ljava/lang/Long;

    if-nez v0, :cond_2

    .line 1726536
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1726537
    :goto_2
    return-void

    .line 1726538
    :cond_0
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 1726539
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    goto :goto_0

    .line 1726540
    :cond_1
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 1726541
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_1

    .line 1726542
    :cond_2
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 1726543
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->e:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    goto :goto_2
.end method
