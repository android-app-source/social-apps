.class public Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfigSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1726583
    const-class v0, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;

    new-instance v1, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfigSerializer;

    invoke-direct {v1}, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfigSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1726584
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1726592
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1726593
    if-nez p0, :cond_0

    .line 1726594
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1726595
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1726596
    invoke-static {p0, p1, p2}, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfigSerializer;->b(Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;LX/0nX;LX/0my;)V

    .line 1726597
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1726598
    return-void
.end method

.method private static b(Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1726586
    const-string v0, "capacity"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->getCapacity()Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1726587
    const-string v0, "max_size"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->getMaxSize()Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1726588
    const-string v0, "name"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1726589
    const-string v0, "stale_days"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->getStaleDays()Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1726590
    const-string v0, "version"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->getVersion()Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1726591
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1726585
    check-cast p1, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;

    invoke-static {p1, p2, p3}, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfigSerializer;->a(Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;LX/0nX;LX/0my;)V

    return-void
.end method
