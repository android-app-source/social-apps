.class public Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;
.super Landroid/view/View;
.source ""


# static fields
.field private static final b:LX/0wT;


# instance fields
.field private A:I

.field private B:I

.field private C:F

.field private D:F

.field private E:F

.field private F:I

.field private G:I

.field private H:I

.field private I:F

.field private J:F

.field private K:F

.field private L:Z

.field private M:I

.field private N:I

.field private O:I

.field public a:LX/0wW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:I

.field private d:Landroid/content/res/Resources;

.field public e:F

.field public f:F

.field private g:LX/0wd;

.field private h:LX/0wd;

.field public i:F

.field public j:F

.field public k:F

.field public l:F

.field public m:F

.field public n:F

.field public o:F

.field public p:F

.field public q:F

.field private r:Landroid/graphics/Paint;

.field private s:Landroid/graphics/Paint;

.field private t:Landroid/graphics/Paint;

.field private u:F

.field private v:Landroid/graphics/RectF;

.field private w:Landroid/graphics/RectF;

.field private x:I

.field private y:I

.field private z:I


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1722845
    const-wide/high16 v0, 0x4069000000000000L    # 200.0

    const-wide/high16 v2, 0x4034000000000000L    # 20.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->b:LX/0wT;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1722957
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1722958
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1722955
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1722956
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1722938
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1722939
    const-class v0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;

    invoke-static {v0, p0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1722940
    sget-object v0, LX/03r;->InspirationStrokeIndicator:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1722941
    const/16 v1, 0x0

    const/high16 v2, 0x41200000    # 10.0f

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->C:F

    .line 1722942
    const/16 v1, 0x1

    const/high16 v2, 0x41f00000    # 30.0f

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->D:F

    .line 1722943
    const/16 v1, 0x2

    const/high16 v2, 0x42480000    # 50.0f

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->E:F

    .line 1722944
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1722945
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->d:Landroid/content/res/Resources;

    .line 1722946
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->d:Landroid/content/res/Resources;

    const v1, 0x7f0b1a98

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->F:I

    .line 1722947
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->d:Landroid/content/res/Resources;

    const v1, 0x7f0b1a99

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->G:I

    .line 1722948
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->d:Landroid/content/res/Resources;

    const v1, 0x7f0b1a93

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->H:I

    .line 1722949
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->d:Landroid/content/res/Resources;

    const v1, 0x7f0b1a9a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->z:I

    .line 1722950
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->d:Landroid/content/res/Resources;

    const v1, 0x7f0b1a9b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->A:I

    .line 1722951
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->d:Landroid/content/res/Resources;

    const v1, 0x7f0b1a9c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->B:I

    .line 1722952
    invoke-direct {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->b()V

    .line 1722953
    invoke-direct {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->c()V

    .line 1722954
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;

    invoke-static {v0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v0

    check-cast v0, LX/0wW;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->a:LX/0wW;

    return-void
.end method

.method private b()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1722919
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->d:Landroid/content/res/Resources;

    const v1, 0x7f0b1a9e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->M:I

    .line 1722920
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->d:Landroid/content/res/Resources;

    const v1, 0x7f0b1a9f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->N:I

    .line 1722921
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->d:Landroid/content/res/Resources;

    const v1, 0x7f0a077f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->O:I

    .line 1722922
    const/4 v0, 0x0

    invoke-virtual {p0, v4, v0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->setLayerType(ILandroid/graphics/Paint;)V

    .line 1722923
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->r:Landroid/graphics/Paint;

    .line 1722924
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->r:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1722925
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->r:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1722926
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->r:Landroid/graphics/Paint;

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->M:I

    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->N:I

    int-to-float v2, v2

    iget v3, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->O:I

    invoke-virtual {v0, v1, v5, v2, v3}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 1722927
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->s:Landroid/graphics/Paint;

    .line 1722928
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->s:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1722929
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->s:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1722930
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->s:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->d:Landroid/content/res/Resources;

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1722931
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->s:Landroid/graphics/Paint;

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->M:I

    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->N:I

    int-to-float v2, v2

    iget v3, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->O:I

    invoke-virtual {v0, v1, v5, v2, v3}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 1722932
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->t:Landroid/graphics/Paint;

    .line 1722933
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->t:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1722934
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->t:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1722935
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->t:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->d:Landroid/content/res/Resources;

    const v2, 0x7f0a0100

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1722936
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->t:Landroid/graphics/Paint;

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->M:I

    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->N:I

    int-to-float v2, v2

    iget v3, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->O:I

    invoke-virtual {v0, v1, v5, v2, v3}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 1722937
    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1722910
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->a:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    move-result-object v0

    sget-object v1, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->b:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    .line 1722911
    iput-boolean v3, v0, LX/0wd;->c:Z

    .line 1722912
    move-object v0, v0

    .line 1722913
    new-instance v1, LX/Aua;

    invoke-direct {v1, p0}, LX/Aua;-><init>(Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->g:LX/0wd;

    .line 1722914
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->a:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    move-result-object v0

    sget-object v1, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->b:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    .line 1722915
    iput-boolean v3, v0, LX/0wd;->c:Z

    .line 1722916
    move-object v0, v0

    .line 1722917
    new-instance v1, LX/Aub;

    invoke-direct {v1, p0}, LX/Aub;-><init>(Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->h:LX/0wd;

    .line 1722918
    return-void
.end method

.method private d()V
    .locals 6

    .prologue
    .line 1722907
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->i:F

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->x:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->u:F

    iget v3, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->y:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->i:F

    iget v4, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->x:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget v4, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->u:F

    iget v5, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->y:I

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    add-float/2addr v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->v:Landroid/graphics/RectF;

    .line 1722908
    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->v:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->z:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iget-object v2, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->v:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    iget v3, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->z:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->v:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    iget v4, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->z:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->v:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    iget v5, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->z:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->w:Landroid/graphics/RectF;

    .line 1722909
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1722904
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->L:Z

    .line 1722905
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->invalidate()V

    .line 1722906
    return-void
.end method

.method public final a(FFII)V
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 1722890
    iput p1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->i:F

    .line 1722891
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->i:F

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->j:F

    .line 1722892
    iput p2, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->u:F

    .line 1722893
    iput p3, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->x:I

    .line 1722894
    iput p4, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->y:I

    .line 1722895
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->u:F

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->y:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->F:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->C:F

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->I:F

    .line 1722896
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->I:F

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->C:F

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->F:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->D:F

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->J:F

    .line 1722897
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->J:F

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->D:F

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->F:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->E:F

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->K:F

    .line 1722898
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->l:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 1722899
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->I:F

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->l:F

    .line 1722900
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->C:F

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->m:F

    .line 1722901
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->m:F

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->n:F

    .line 1722902
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->C:F

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->p:F

    .line 1722903
    :cond_0
    return-void
.end method

.method public final a(IF)V
    .locals 1

    .prologue
    .line 1722885
    iput p1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->c:I

    .line 1722886
    iput p2, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->i:F

    .line 1722887
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->i:F

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->j:F

    .line 1722888
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->invalidate()V

    .line 1722889
    return-void
.end method

.method public final a(IFFZ)V
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v2, 0x0

    .line 1722861
    iput-boolean p4, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->L:Z

    .line 1722862
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->u:F

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->y:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->H:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    cmpl-float v0, p3, v0

    if-lez v0, :cond_1

    .line 1722863
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->g:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1722864
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->k:F

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->j:F

    .line 1722865
    :cond_0
    iput p2, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->k:F

    .line 1722866
    iput p1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->c:I

    .line 1722867
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->k:F

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->j:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    .line 1722868
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->g:LX/0wd;

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, LX/0wd;->b(D)LX/0wd;

    .line 1722869
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->L:Z

    if-eqz v0, :cond_3

    .line 1722870
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->h:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->i()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1722871
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->o:F

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->l:F

    .line 1722872
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->p:F

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->m:F

    .line 1722873
    :cond_2
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->I:F

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->C:F

    div-float/2addr v1, v6

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->G:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    cmpl-float v0, p3, v0

    if-lez v0, :cond_4

    .line 1722874
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->I:F

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->o:F

    .line 1722875
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->C:F

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->p:F

    .line 1722876
    :goto_0
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->o:F

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->l:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_3

    .line 1722877
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->h:LX/0wd;

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, LX/0wd;->b(D)LX/0wd;

    .line 1722878
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->invalidate()V

    .line 1722879
    return-void

    .line 1722880
    :cond_4
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->J:F

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->D:F

    div-float/2addr v1, v6

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->G:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    cmpl-float v0, p3, v0

    if-lez v0, :cond_5

    .line 1722881
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->J:F

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->o:F

    .line 1722882
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->D:F

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->p:F

    goto :goto_0

    .line 1722883
    :cond_5
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->K:F

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->o:F

    .line 1722884
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->E:F

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->p:F

    goto :goto_0
.end method

.method public getStrokeColor()I
    .locals 1

    .prologue
    .line 1722860
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->c:I

    return v0
.end method

.method public getStrokeSize()F
    .locals 1

    .prologue
    .line 1722859
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->p:F

    return v0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 1722846
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1722847
    invoke-direct {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->d()V

    .line 1722848
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->r:Landroid/graphics/Paint;

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->c:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1722849
    invoke-direct {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->d()V

    .line 1722850
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->w:Landroid/graphics/RectF;

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->A:I

    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->A:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->s:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 1722851
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->v:Landroid/graphics/RectF;

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->B:I

    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->B:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->r:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 1722852
    iget-boolean v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->L:Z

    if-eqz v0, :cond_0

    .line 1722853
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->i:F

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->I:F

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->C:F

    div-float/2addr v2, v4

    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->t:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1722854
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->i:F

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->J:F

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->D:F

    div-float/2addr v2, v4

    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->t:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1722855
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->i:F

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->K:F

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->E:F

    div-float/2addr v2, v4

    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->t:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1722856
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->i:F

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->l:F

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->n:F

    div-float/2addr v2, v4

    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->r:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1722857
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->i:F

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->o:F

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->q:F

    div-float/2addr v2, v4

    iget-object v3, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->r:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1722858
    :cond_0
    return-void
.end method
