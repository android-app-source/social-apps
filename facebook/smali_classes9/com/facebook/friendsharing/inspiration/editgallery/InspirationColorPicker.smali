.class public Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# static fields
.field private static final a:[I


# instance fields
.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final c:I

.field public d:LX/AsF;

.field private e:Landroid/view/GestureDetector;

.field public f:I

.field public g:I

.field private h:I

.field private i:I

.field private j:F

.field private k:F

.field private l:F

.field private m:F

.field public n:F


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0xff

    const/16 v2, 0xd

    .line 1722773
    const/16 v0, 0xc

    new-array v0, v0, [I

    const/4 v1, 0x0

    invoke-static {v2, v2, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0xbe

    const/16 v3, 0xc2

    const/16 v4, 0xc9

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x2

    invoke-static {v5, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x3

    const/16 v2, 0xa0

    const/16 v3, 0x4e

    const/16 v4, 0x46

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x4

    const/16 v2, 0xec

    const/16 v3, 0x3d

    const/16 v4, 0x3f

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x5

    const/16 v2, 0xfa

    const/16 v3, 0x86

    const/16 v4, 0x2c

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0xfe

    const/16 v3, 0xcc

    const/16 v4, 0x37

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x7

    const/16 v2, 0x4b

    const/16 v3, 0xa2

    const/16 v4, 0x6e

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0x59

    const/16 v3, 0xc6

    const/16 v4, 0xe9

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x9

    const/16 v2, 0x22

    const/16 v3, 0x8b

    const/16 v4, 0xcf

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xa

    const/16 v2, 0x8c

    const/16 v3, 0x72

    const/16 v4, 0xcb

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xb

    const/16 v2, 0xea

    const/16 v3, 0x68

    const/16 v4, 0xb5

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    sput-object v0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->a:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1722741
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1722742
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1722743
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1722744
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1722745
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1722746
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->b:Ljava/util/List;

    .line 1722747
    invoke-direct {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->a()V

    .line 1722748
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->c:I

    .line 1722749
    const/4 v0, 0x2

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->g:I

    .line 1722750
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->getDefaultColor()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->f:I

    .line 1722751
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, LX/AuY;

    invoke-direct {v1, p0}, LX/AuY;-><init>(Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->e:Landroid/view/GestureDetector;

    .line 1722752
    invoke-virtual {p0, v3}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->setFocusable(Z)V

    .line 1722753
    invoke-virtual {p0, v3}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->setFocusableInTouchMode(Z)V

    .line 1722754
    return-void
.end method

.method private a(F)I
    .locals 2

    .prologue
    .line 1722682
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->getLeft()I

    move-result v0

    int-to-float v0, v0

    sub-float v0, p1, v0

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->h:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    float-to-int v0, v0

    .line 1722683
    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->c:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method private a()V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1722755
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1722756
    sget-object v0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->a:[I

    array-length v7, v0

    move v6, v2

    .line 1722757
    :goto_0
    if-ge v6, v7, :cond_4

    .line 1722758
    new-instance v8, LX/AuZ;

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v8, v0}, LX/AuZ;-><init>(Landroid/content/Context;)V

    .line 1722759
    sget-object v0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->a:[I

    aget v0, v0, v6

    .line 1722760
    iput v0, v8, LX/AuZ;->b:I

    .line 1722761
    if-nez v6, :cond_0

    move v5, v1

    :goto_1
    if-nez v6, :cond_1

    move v4, v1

    :goto_2
    add-int/lit8 v0, v7, -0x1

    if-ne v6, v0, :cond_2

    move v3, v1

    :goto_3
    add-int/lit8 v0, v7, -0x1

    if-ne v6, v0, :cond_3

    move v0, v1

    :goto_4
    invoke-virtual {v8, v5, v4, v3, v0}, LX/AuZ;->a(ZZZZ)V

    .line 1722762
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->b:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1722763
    invoke-virtual {p0, v8, v2, v2}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->addView(Landroid/view/View;II)V

    .line 1722764
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    :cond_0
    move v5, v2

    .line 1722765
    goto :goto_1

    :cond_1
    move v4, v2

    goto :goto_2

    :cond_2
    move v3, v2

    goto :goto_3

    :cond_3
    move v0, v2

    goto :goto_4

    .line 1722766
    :cond_4
    return-void
.end method

.method private b(F)F
    .locals 3

    .prologue
    .line 1722767
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->getLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->getRight()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-static {p1, v0, v1}, LX/0yq;->b(FFF)F

    move-result v0

    return v0
.end method

.method private static b(I)I
    .locals 2

    .prologue
    .line 1722768
    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->a:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 1722769
    sget-object v1, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->a:[I

    aget v1, v1, v0

    if-ne p0, v1, :cond_0

    .line 1722770
    :goto_1
    return v0

    .line 1722771
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1722772
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private c(F)F
    .locals 2

    .prologue
    .line 1722739
    const/4 v0, 0x0

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->j:F

    invoke-static {p1, v0, v1}, LX/0yq;->b(FFF)F

    move-result v0

    return v0
.end method

.method public static e(Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;I)F
    .locals 3

    .prologue
    .line 1722740
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->getLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    int-to-float v0, v0

    int-to-float v1, p1

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->h:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method


# virtual methods
.method public final a(I)F
    .locals 2

    .prologue
    .line 1722735
    invoke-static {p1}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->b(I)I

    move-result v1

    .line 1722736
    if-ltz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1722737
    invoke-static {p0, v1}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->e(Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;I)F

    move-result v0

    return v0

    .line 1722738
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentColor()I
    .locals 1

    .prologue
    .line 1722734
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->f:I

    return v0
.end method

.method public getDefaultColor()I
    .locals 2

    .prologue
    .line 1722733
    sget-object v0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->a:[I

    const/4 v1, 0x2

    aget v0, v0, v1

    return v0
.end method

.method public final onLayout(ZIIII)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1722718
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomFrameLayout;->onLayout(ZIIII)V

    .line 1722719
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1722720
    :goto_0
    return-void

    .line 1722721
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    .line 1722722
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .line 1722723
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->getPaddingTop()I

    move-result v5

    .line 1722724
    add-int v6, v5, v0

    .line 1722725
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->getPaddingLeft()I

    move-result v0

    move v2, v0

    .line 1722726
    :goto_1
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->c:I

    if-ge v1, v0, :cond_1

    .line 1722727
    add-int v3, v2, v4

    .line 1722728
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1722729
    invoke-virtual {v0, v2, v5, v3, v6}, Landroid/view/View;->layout(IIII)V

    .line 1722730
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v2, v3

    goto :goto_1

    .line 1722731
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->getPaddingTop()I

    move-result v0

    add-int/2addr v0, p3

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->i:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    int-to-float v0, v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->j:F

    .line 1722732
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->d:LX/AsF;

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->g:I

    invoke-static {p0, v1}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->e(Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;I)F

    move-result v1

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->j:F

    iget v3, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->h:I

    iget v4, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->i:I

    invoke-interface {v0, v1, v2, v3, v4}, LX/AsF;->a(FFII)V

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 6

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 1722707
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 1722708
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 1722709
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->getPaddingLeft()I

    move-result v0

    sub-int v0, v1, v0

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->getPaddingRight()I

    move-result v3

    sub-int/2addr v0, v3

    .line 1722710
    iget v3, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->c:I

    div-int/2addr v0, v3

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->h:I

    .line 1722711
    iput v2, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->i:I

    .line 1722712
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->h:I

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 1722713
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->i:I

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 1722714
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1722715
    invoke-virtual {v0, v3, v4}, Landroid/view/View;->measure(II)V

    goto :goto_0

    .line 1722716
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->getPaddingBottom()I

    move-result v0

    add-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->getPaddingTop()I

    move-result v2

    add-int/2addr v0, v2

    invoke-virtual {p0, v1, v0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->setMeasuredDimension(II)V

    .line 1722717
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/high16 v3, 0x42200000    # 40.0f

    const/4 v6, 0x1

    const v0, -0x4b88391a

    invoke-static {v4, v6, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1722691
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->d:LX/AsF;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1722692
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    invoke-direct {p0, v1}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->b(F)F

    move-result v1

    iput v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->m:F

    .line 1722693
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    invoke-direct {p0, v1}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->c(F)F

    move-result v1

    iput v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->n:F

    .line 1722694
    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->m:F

    invoke-direct {p0, v1}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->a(F)I

    move-result v1

    iput v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->g:I

    .line 1722695
    sget-object v1, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->a:[I

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->g:I

    aget v1, v1, v2

    iput v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->f:I

    .line 1722696
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->e:Landroid/view/GestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1722697
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_1

    .line 1722698
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iput v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->k:F

    .line 1722699
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iput v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->l:F

    .line 1722700
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->d:LX/AsF;

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->f:I

    iget v3, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->g:I

    invoke-static {p0, v3}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->e(Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;I)F

    move-result v3

    iget v4, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->n:F

    const/4 v5, 0x0

    invoke-interface {v1, v2, v3, v4, v5}, LX/AsF;->a(IFFZ)V

    .line 1722701
    :cond_0
    :goto_0
    const v1, 0x65323a30

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return v6

    .line 1722702
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v4, :cond_3

    .line 1722703
    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->k:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, v3

    if-gtz v1, :cond_2

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->l:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, v3

    if-lez v1, :cond_0

    .line 1722704
    :cond_2
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->d:LX/AsF;

    iget v2, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->f:I

    iget v3, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->g:I

    invoke-static {p0, v3}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->e(Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;I)F

    move-result v3

    iget v4, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->n:F

    invoke-interface {v1, v2, v3, v4, v6}, LX/AsF;->a(IFFZ)V

    goto :goto_0

    .line 1722705
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v6, :cond_0

    .line 1722706
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->d:LX/AsF;

    invoke-interface {v1}, LX/AsF;->a()V

    goto :goto_0
.end method

.method public setCurrentColor(I)V
    .locals 1

    .prologue
    .line 1722686
    iput p1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->f:I

    .line 1722687
    invoke-static {p1}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->b(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->g:I

    .line 1722688
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->g:I

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1722689
    return-void

    .line 1722690
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setOnColourPickerInteractionListener(LX/AsF;)V
    .locals 0

    .prologue
    .line 1722684
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->d:LX/AsF;

    .line 1722685
    return-void
.end method
