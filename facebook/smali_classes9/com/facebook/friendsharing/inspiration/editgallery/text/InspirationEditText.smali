.class public Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;
.super Landroid/widget/EditText;
.source ""


# static fields
.field private static final b:I

.field private static final c:Ljava/lang/String;


# instance fields
.field public a:LX/0w3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Landroid/graphics/Typeface;

.field public e:LX/Auj;

.field public f:Landroid/view/inputmethod/InputMethodManager;

.field public g:I

.field public h:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1723196
    const/16 v0, 0x66

    invoke-static {v0, v1, v1, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sput v0, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->b:I

    .line 1723197
    const-class v0, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1723191
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 1723192
    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->g:I

    .line 1723193
    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->h:I

    .line 1723194
    invoke-direct {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->b()V

    .line 1723195
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1723186
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1723187
    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->g:I

    .line 1723188
    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->h:I

    .line 1723189
    invoke-direct {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->b()V

    .line 1723190
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1723135
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1723136
    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->g:I

    .line 1723137
    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->h:I

    .line 1723138
    invoke-direct {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->b()V

    .line 1723139
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;

    invoke-static {v0}, LX/0w3;->a(LX/0QB;)LX/0w3;

    move-result-object v0

    check-cast v0, LX/0w3;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->a:LX/0w3;

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 1723178
    const-class v0, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;

    invoke-static {v0, p0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1723179
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->f:Landroid/view/inputmethod/InputMethodManager;

    .line 1723180
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v1, "fonts/MontserratSemiBold.otf"

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->d:Landroid/graphics/Typeface;

    .line 1723181
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->d:Landroid/graphics/Typeface;

    invoke-virtual {p0, v0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1723182
    const/4 v0, 0x0

    sget v1, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->b:I

    invoke-virtual {p0, v2, v0, v2, v1}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->setShadowLayer(FFFI)V

    .line 1723183
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->setHorizontallyScrolling(Z)V

    .line 1723184
    const/16 v0, 0xf

    invoke-virtual {p0, v0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->setMaxLines(I)V

    .line 1723185
    return-void
.end method

.method private getTextPaintWidth()I
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 1723171
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v1, "fonts/MontserratSemiBold.otf"

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 1723172
    new-instance v1, Landroid/text/TextPaint;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, Landroid/text/TextPaint;-><init>(I)V

    .line 1723173
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->getCurrentTextColor()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 1723174
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->getTextSize()F

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 1723175
    invoke-virtual {v1, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1723176
    const/4 v0, 0x0

    sget v2, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->b:I

    invoke-virtual {v1, v4, v0, v4, v2}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 1723177
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->getLayout()Landroid/text/Layout;

    move-result-object v0

    check-cast v0, Landroid/text/DynamicLayout;

    invoke-static {v1, v0}, LX/8Gf;->a(Landroid/text/TextPaint;Landroid/text/DynamicLayout;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 1723168
    invoke-direct {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->getTextPaintWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->g:I

    .line 1723169
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->h:I

    .line 1723170
    invoke-static {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->setText(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->getCurrentTextColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->setTextColor(I)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->setSizeMultiplier(I)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->getTextSize()F

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->scaledDensity:F

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->setTextSize(F)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;

    move-result-object v0

    const-string v1, "fonts/MontserratSemiBold.otf"

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->setTypeface(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->setShadowRadius(F)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->setShadowDX(F)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->setShadowDY(F)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;

    move-result-object v0

    sget v1, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->b:I

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->setShadowColor(I)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;

    move-result-object v0

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->setBorderColor(I)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;

    move-result-object v0

    const/16 v1, 0x1a

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->setBorderAlpha(I)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;

    move-result-object v0

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->setBorderWidth(F)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;

    move-result-object v0

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->h:I

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->setTextHeight(I)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;

    move-result-object v0

    iget v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->g:I

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->setTextWidth(I)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->setRotation(F)Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 1723160
    invoke-virtual {p0, v0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->setEnabled(Z)V

    .line 1723161
    invoke-virtual {p0, v0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->setFocusable(Z)V

    .line 1723162
    invoke-virtual {p0, v0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->setFocusableInTouchMode(Z)V

    .line 1723163
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->bringToFront()V

    .line 1723164
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->requestFocus()Z

    .line 1723165
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->setSelection(I)V

    .line 1723166
    new-instance v0, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText$1;

    invoke-direct {v0, p0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText$1;-><init>(Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;)V

    invoke-virtual {p0, v0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->post(Ljava/lang/Runnable;)Z

    .line 1723167
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1723154
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->clearFocus()V

    .line 1723155
    invoke-virtual {p0, v2}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->setFocusable(Z)V

    .line 1723156
    invoke-virtual {p0, v2}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->setFocusableInTouchMode(Z)V

    .line 1723157
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->f:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1723158
    iget-object v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->e:LX/Auj;

    invoke-interface {v0, p1}, LX/Auj;->a(Z)V

    .line 1723159
    return-void
.end method

.method public getSavedHeight()I
    .locals 1

    .prologue
    .line 1723153
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->h:I

    return v0
.end method

.method public getSavedWidth()I
    .locals 1

    .prologue
    .line 1723152
    iget v0, p0, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->g:I

    return v0
.end method

.method public final onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1723147
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 1723148
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->a(Z)V

    .line 1723149
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1723150
    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_0
    move v0, v1

    .line 1723151
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p2}, Landroid/widget/EditText;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onLayout(ZIIII)V
    .locals 0

    .prologue
    .line 1723145
    invoke-super/range {p0 .. p5}, Landroid/widget/EditText;->onLayout(ZIIII)V

    .line 1723146
    return-void
.end method

.method public final onMeasure(II)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x5d371c30

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1723142
    iget-object v1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->a:LX/0w3;

    invoke-virtual {v1, p0, p2}, LX/0w3;->a(Landroid/view/View;I)V

    .line 1723143
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->onMeasure(II)V

    .line 1723144
    const/16 v1, 0x2d

    const v2, 0x5d01a2c2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setCallBack(LX/Auj;)V
    .locals 0

    .prologue
    .line 1723140
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/editgallery/text/InspirationEditText;->e:LX/Auj;

    .line 1723141
    return-void
.end method
