.class public Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration_BuilderDeserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# static fields
.field private static a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/common/json/FbJsonField;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1723669
    const-class v0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration$Builder;

    new-instance v1, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration_BuilderDeserializer;

    invoke-direct {v1}, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration_BuilderDeserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1723670
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1723671
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    .line 1723672
    const-class v0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration$Builder;

    invoke-virtual {p0, v0}, Lcom/facebook/common/json/FbJsonDeserializer;->init(Ljava/lang/Class;)V

    .line 1723673
    return-void
.end method


# virtual methods
.method public final getField(Ljava/lang/String;)Lcom/facebook/common/json/FbJsonField;
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1723674
    const-class v3, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration_BuilderDeserializer;

    monitor-enter v3

    .line 1723675
    :try_start_0
    sget-object v0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration_BuilderDeserializer;->a:Ljava/util/Map;

    if-nez v0, :cond_2

    .line 1723676
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration_BuilderDeserializer;->a:Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1723677
    :cond_0
    const/4 v0, -0x1

    :try_start_1
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_1
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 1723678
    invoke-super {p0, p1}, Lcom/facebook/common/json/FbJsonDeserializer;->getField(Ljava/lang/String;)Lcom/facebook/common/json/FbJsonField;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    :try_start_2
    monitor-exit v3

    .line 1723679
    :goto_1
    return-object v0

    .line 1723680
    :cond_2
    sget-object v0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration_BuilderDeserializer;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/json/FbJsonField;

    .line 1723681
    if-eqz v0, :cond_0

    .line 1723682
    monitor-exit v3

    goto :goto_1

    .line 1723683
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1723684
    :sswitch_0
    :try_start_3
    const-string v2, "applied_inspirations"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string v1, "story_id"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v0, v2

    goto :goto_0

    .line 1723685
    :pswitch_0
    const-class v0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration$Builder;

    const-string v1, "setAppliedInspirations"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, LX/0Px;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const-class v1, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    invoke-static {v0, v1}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;Ljava/lang/Class;)Lcom/facebook/common/json/FbJsonField;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    .line 1723686
    :goto_2
    :try_start_4
    sget-object v1, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration_BuilderDeserializer;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1723687
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 1723688
    :pswitch_1
    :try_start_5
    const-class v0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration$Builder;

    const-string v1, "setStoryId"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Ljava/lang/String;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v0

    goto :goto_2

    .line 1723689
    :catch_0
    move-exception v0

    .line 1723690
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :sswitch_data_0
    .sparse-switch
        0xf0c9f4b -> :sswitch_0
        0x6662d8a5 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
