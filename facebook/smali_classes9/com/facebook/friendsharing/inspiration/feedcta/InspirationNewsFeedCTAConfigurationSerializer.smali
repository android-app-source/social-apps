.class public Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfigurationSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1723667
    const-class v0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;

    new-instance v1, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfigurationSerializer;

    invoke-direct {v1}, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfigurationSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1723668
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1723666
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1723660
    if-nez p0, :cond_0

    .line 1723661
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1723662
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1723663
    invoke-static {p0, p1, p2}, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfigurationSerializer;->b(Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;LX/0nX;LX/0my;)V

    .line 1723664
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1723665
    return-void
.end method

.method private static b(Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1723657
    const-string v0, "applied_inspirations"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;->getAppliedInspirations()LX/0Px;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1723658
    const-string v0, "story_id"

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;->getStoryId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1723659
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1723656
    check-cast p1, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;

    invoke-static {p1, p2, p3}, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfigurationSerializer;->a(Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;LX/0nX;LX/0my;)V

    return-void
.end method
