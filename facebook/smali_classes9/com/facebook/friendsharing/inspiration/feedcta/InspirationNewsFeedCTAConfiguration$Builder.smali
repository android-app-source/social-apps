.class public final Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration_BuilderDeserializer;
.end annotation


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1723647
    const-class v0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration_BuilderDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1723648
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1723649
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;
    .locals 2

    .prologue
    .line 1723646
    new-instance v0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;

    invoke-direct {v0, p0}, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;-><init>(Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration$Builder;)V

    return-object v0
.end method

.method public setAppliedInspirations(LX/0Px;)Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration$Builder;
    .locals 0
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "applied_inspirations"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
            ">;)",
            "Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration$Builder;"
        }
    .end annotation

    .prologue
    .line 1723644
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration$Builder;->a:LX/0Px;

    .line 1723645
    return-object p0
.end method

.method public setStoryId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "story_id"
    .end annotation

    .prologue
    .line 1723642
    iput-object p1, p0, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration$Builder;->b:Ljava/lang/String;

    .line 1723643
    return-object p0
.end method
