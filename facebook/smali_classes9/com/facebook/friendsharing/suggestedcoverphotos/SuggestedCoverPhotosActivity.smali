.class public Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private p:Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1732439
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Landroid/content/Context;LX/0Px;Lcom/facebook/productionprompts/logging/PromptAnalytics;)Landroid/content/Intent;
    .locals 2
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/productionprompts/logging/PromptAnalytics;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 1732440
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "prompt_session_id"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "photo_urls"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "prompt_analytics"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1732441
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1732442
    const v0, 0x7f03142a

    invoke-virtual {p0, v0}, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosActivity;->setContentView(I)V

    .line 1732443
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    sget-object v1, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosFragment;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosFragment;

    iput-object v0, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosActivity;->p:Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosFragment;

    .line 1732444
    iget-object v0, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosActivity;->p:Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosFragment;

    if-nez v0, :cond_1

    .line 1732445
    new-instance v0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosFragment;

    invoke-direct {v0}, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosFragment;-><init>()V

    iput-object v0, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosActivity;->p:Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosFragment;

    .line 1732446
    invoke-virtual {p0}, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1732447
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1732448
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1732449
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 1732450
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosActivity;->p:Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosFragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1732451
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d002f

    iget-object v2, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosActivity;->p:Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1732452
    :cond_1
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 1732453
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1732454
    invoke-virtual {p0}, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosActivity;->finish()V

    .line 1732455
    return-void
.end method

.method public final onBackPressed()V
    .locals 4

    .prologue
    .line 1732456
    iget-object v0, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosActivity;->p:Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosFragment;

    .line 1732457
    iget-object v1, v0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosFragment;->c:LX/AzQ;

    iget-object v2, v0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosFragment;->e:Ljava/lang/String;

    .line 1732458
    iget-object v3, v1, LX/AzQ;->a:LX/0Zb;

    sget-object v0, LX/AzP;->CANCEL_PICKER:LX/AzP;

    invoke-virtual {v0}, LX/AzP;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LX/AzQ;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-interface {v3, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1732459
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 1732460
    return-void
.end method
