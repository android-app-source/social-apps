.class public Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:LX/AzH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/AzQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/lang/String;

.field public f:Lcom/facebook/productionprompts/logging/PromptAnalytics;

.field public g:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public h:Lcom/facebook/ui/titlebar/Fb4aTitleBar;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1732492
    const-class v0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1732471
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1732489
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1732490
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosFragment;

    const-class p1, LX/AzH;

    invoke-interface {v0, p1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/AzH;

    invoke-static {v0}, LX/AzQ;->b(LX/0QB;)LX/AzQ;

    move-result-object v0

    check-cast v0, LX/AzQ;

    iput-object p1, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosFragment;->b:LX/AzH;

    iput-object v0, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosFragment;->c:LX/AzQ;

    .line 1732491
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x3d97c0a0

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1732488
    const v1, 0x7f03142b

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x33eee54a    # -3.8038232E7f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 9
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1732472
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1732473
    const v0, 0x7f0d2e3a

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosFragment;->g:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1732474
    const v0, 0x7f0d00bc

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iput-object v0, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosFragment;->h:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 1732475
    iget-object v0, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosFragment;->h:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    const v1, 0x7f08273a

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(I)V

    .line 1732476
    iget-object v0, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosFragment;->h:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    new-instance v1, LX/AzM;

    invoke-direct {v1, p0}, LX/AzM;-><init>(Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 1732477
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1732478
    const-string v1, "photo_urls"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosFragment;->d:LX/0Px;

    .line 1732479
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1732480
    const-string v1, "prompt_session_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosFragment;->e:Ljava/lang/String;

    .line 1732481
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1732482
    const-string v1, "prompt_analytics"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/logging/PromptAnalytics;

    iput-object v0, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosFragment;->f:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 1732483
    new-instance v2, LX/1P0;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct {v2, v3, v4, v5}, LX/1P0;-><init>(Landroid/content/Context;IZ)V

    .line 1732484
    iget-object v3, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosFragment;->g:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v3, v2}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1732485
    iget-object v8, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosFragment;->g:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v2, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosFragment;->b:LX/AzH;

    iget-object v3, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosFragment;->d:LX/0Px;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v6, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosFragment;->e:Ljava/lang/String;

    iget-object v7, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosFragment;->f:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-object v5, p0

    invoke-virtual/range {v2 .. v7}, LX/AzH;->a(LX/0Px;Landroid/content/Context;Landroid/support/v4/app/Fragment;Ljava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;)Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;

    move-result-object v2

    invoke-virtual {v8, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1732486
    iget-object v2, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosFragment;->g:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v3, LX/AzN;

    invoke-direct {v3, p0}, LX/AzN;-><init>(Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotosFragment;)V

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 1732487
    return-void
.end method
