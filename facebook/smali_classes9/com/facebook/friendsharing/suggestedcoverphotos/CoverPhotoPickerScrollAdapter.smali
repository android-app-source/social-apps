.class public Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/AzG;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Landroid/content/Context;

.field public final d:Landroid/support/v4/app/Fragment;

.field public final e:LX/1Ad;

.field public final f:LX/AzL;

.field public final g:LX/AzQ;

.field public final h:Ljava/lang/String;

.field public final i:Lcom/facebook/productionprompts/logging/PromptAnalytics;

.field public j:Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1732363
    const-class v0, Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Px;Landroid/content/Context;Landroid/support/v4/app/Fragment;Ljava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;LX/1Ad;LX/AzL;LX/AzQ;)V
    .locals 0
    .param p1    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/support/v4/app/Fragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/productionprompts/logging/PromptAnalytics;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/Context;",
            "Landroid/support/v4/app/Fragment;",
            "Ljava/lang/String;",
            "Lcom/facebook/productionprompts/logging/PromptAnalytics;",
            "LX/1Ad;",
            "LX/AzL;",
            "LX/AzQ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1732336
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1732337
    iput-object p1, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;->b:LX/0Px;

    .line 1732338
    iput-object p2, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;->c:Landroid/content/Context;

    .line 1732339
    iput-object p3, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;->d:Landroid/support/v4/app/Fragment;

    .line 1732340
    iput-object p4, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;->h:Ljava/lang/String;

    .line 1732341
    iput-object p5, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;->i:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 1732342
    iput-object p6, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;->e:LX/1Ad;

    .line 1732343
    iput-object p7, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;->f:LX/AzL;

    .line 1732344
    iput-object p8, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;->g:LX/AzQ;

    .line 1732345
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 1732346
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03039d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1732347
    new-instance v1, LX/AzG;

    invoke-direct {v1, p0, v0}, LX/AzG;-><init>(Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 1732348
    check-cast p1, LX/AzG;

    .line 1732349
    iget-object v0, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;->b:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1732350
    iget-object v1, p1, LX/AzG;->l:Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;

    iget-object v1, v1, Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1a19

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1732351
    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v2

    const/4 p0, 0x1

    .line 1732352
    iput-boolean p0, v2, LX/1bX;->g:Z

    .line 1732353
    move-object v2, v2

    .line 1732354
    new-instance p0, LX/1o9;

    invoke-direct {p0, v1, v1}, LX/1o9;-><init>(II)V

    .line 1732355
    iput-object p0, v2, LX/1bX;->c:LX/1o9;

    .line 1732356
    move-object v1, v2

    .line 1732357
    invoke-virtual {v1}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    .line 1732358
    iget-object v2, p1, LX/AzG;->l:Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;

    iget-object v2, v2, Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;->e:LX/1Ad;

    invoke-virtual {v2}, LX/1Ad;->o()LX/1Ad;

    move-result-object v2

    sget-object p0, Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, p0}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    .line 1732359
    iget-object v2, p1, LX/AzG;->m:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v2, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1732360
    iget-object v1, p1, LX/AzG;->m:Lcom/facebook/drawee/view/DraweeView;

    new-instance v2, LX/AzF;

    invoke-direct {v2, p1, p2, v0}, LX/AzF;-><init>(LX/AzG;ILandroid/net/Uri;)V

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/view/DraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1732361
    return-void
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1732362
    iget-object v0, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/CoverPhotoPickerScrollAdapter;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
