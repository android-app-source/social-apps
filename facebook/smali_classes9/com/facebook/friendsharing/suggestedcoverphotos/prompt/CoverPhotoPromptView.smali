.class public Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;
.super LX/Ale;
.source ""

# interfaces
.implements LX/AkM;


# instance fields
.field public b:LX/AzV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/5oY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/AzQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:Z

.field private f:Z

.field private g:LX/1RN;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1732622
    invoke-direct {p0, p1}, LX/Ale;-><init>(Landroid/content/Context;)V

    .line 1732623
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1732624
    invoke-direct {p0, p1, p2}, LX/Ale;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1732625
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1732626
    invoke-direct {p0, p1, p2, p3}, LX/Ale;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1732627
    return-void
.end method

.method private static a(Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;LX/AzV;LX/5oY;LX/AzQ;)V
    .locals 0

    .prologue
    .line 1732628
    iput-object p1, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;->b:LX/AzV;

    iput-object p2, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;->c:LX/5oY;

    iput-object p3, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;->d:LX/AzQ;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;

    const-class v0, LX/AzV;

    invoke-interface {v2, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/AzV;

    invoke-static {v2}, LX/5oY;->a(LX/0QB;)LX/5oY;

    move-result-object v1

    check-cast v1, LX/5oY;

    invoke-static {v2}, LX/AzQ;->b(LX/0QB;)LX/AzQ;

    move-result-object v2

    check-cast v2, LX/AzQ;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;->a(Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;LX/AzV;LX/5oY;LX/AzQ;)V

    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 0

    .prologue
    .line 1732629
    return-object p0
.end method

.method public final a(LX/1RN;)V
    .locals 10

    .prologue
    .line 1732630
    iput-object p1, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;->g:LX/1RN;

    .line 1732631
    iget-object v0, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;->b:LX/AzV;

    invoke-virtual {p0}, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;->getImageTrayHeight()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 1732632
    new-instance v2, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v5

    check-cast v5, LX/1Ad;

    invoke-static {v0}, LX/B5l;->b(LX/0QB;)LX/B5l;

    move-result-object v6

    check-cast v6, LX/B5l;

    invoke-static {v0}, LX/BMP;->b(LX/0QB;)LX/BMP;

    move-result-object v7

    check-cast v7, LX/BMP;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    move-object v3, v1

    move-object v4, p1

    invoke-direct/range {v2 .. v9}, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;-><init>(Ljava/lang/Integer;LX/1RN;LX/1Ad;LX/B5l;LX/BMP;Landroid/content/Context;LX/0ad;)V

    .line 1732633
    move-object v0, v2

    .line 1732634
    iget-object v1, p0, LX/Ale;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v1, v1

    .line 1732635
    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1732636
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1732619
    const-class v0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;

    invoke-static {v0, p0}, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1732620
    invoke-super {p0}, LX/Ale;->b()V

    .line 1732621
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1732637
    const/4 v0, 0x0

    return v0
.end method

.method public getCollapseAnimator()Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 1732617
    iget-object v0, p0, LX/Ale;->e:Landroid/animation/ValueAnimator;

    move-object v0, v0

    .line 1732618
    return-object v0
.end method

.method public getExpandAnimator()Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 1732615
    iget-object v0, p0, LX/Ale;->d:Landroid/animation/ValueAnimator;

    move-object v0, v0

    .line 1732616
    return-object v0
.end method

.method public getImageTrayHeight()I
    .locals 2

    .prologue
    .line 1732614
    invoke-virtual {p0}, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1a1a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 1732613
    iget-boolean v0, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;->e:Z

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 1732612
    iget-boolean v0, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;->f:Z

    return v0
.end method

.method public final k()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1732608
    iput-object v1, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;->g:LX/1RN;

    .line 1732609
    iget-object v0, p0, LX/Ale;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v0, v0

    .line 1732610
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1732611
    return-void
.end method

.method public setHasBeenShown(Z)V
    .locals 3

    .prologue
    .line 1732600
    iput-boolean p1, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;->e:Z

    .line 1732601
    if-eqz p1, :cond_0

    .line 1732602
    iget-object v0, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;->d:LX/AzQ;

    iget-object v1, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;->c:LX/5oY;

    iget-object v2, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;->g:LX/1RN;

    iget-object v2, v2, LX/1RN;->a:LX/1kK;

    invoke-interface {v2}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/5oY;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1732603
    iget-object v2, v0, LX/AzQ;->a:LX/0Zb;

    sget-object p0, LX/AzP;->IMPRESSION:LX/AzP;

    invoke-virtual {p0}, LX/AzP;->name()Ljava/lang/String;

    move-result-object p0

    invoke-static {v1, p0}, LX/AzQ;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v2, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1732604
    :goto_0
    return-void

    .line 1732605
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;->d:LX/AzQ;

    iget-object v1, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;->c:LX/5oY;

    iget-object v2, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;->g:LX/1RN;

    iget-object v2, v2, LX/1RN;->a:LX/1kK;

    invoke-interface {v2}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/5oY;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1732606
    iget-object v2, v0, LX/AzQ;->a:LX/0Zb;

    sget-object p0, LX/AzP;->HIDE:LX/AzP;

    invoke-virtual {p0}, LX/AzP;->name()Ljava/lang/String;

    move-result-object p0

    invoke-static {v1, p0}, LX/AzQ;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v2, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1732607
    goto :goto_0
.end method

.method public setIsAnimationRunning(Z)V
    .locals 0

    .prologue
    .line 1732598
    iput-boolean p1, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptView;->f:Z

    .line 1732599
    return-void
.end method
