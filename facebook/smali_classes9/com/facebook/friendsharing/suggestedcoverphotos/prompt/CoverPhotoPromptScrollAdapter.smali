.class public Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/AzU;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:I

.field private final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/1Ad;

.field public final e:LX/B5l;

.field public final f:LX/1RN;

.field public final g:LX/BMP;

.field public final h:LX/0ad;

.field public i:Landroid/graphics/drawable/Drawable;

.field public j:Landroid/graphics/drawable/Drawable;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1732595
    const-class v0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;

    const-class v1, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;LX/1RN;LX/1Ad;LX/B5l;LX/BMP;Landroid/content/Context;LX/0ad;)V
    .locals 3
    .param p1    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1RN;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1732582
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1732583
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;->b:I

    .line 1732584
    iput-object p3, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;->d:LX/1Ad;

    .line 1732585
    iput-object p4, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;->e:LX/B5l;

    .line 1732586
    iput-object p5, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;->g:LX/BMP;

    .line 1732587
    iput-object p2, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;->f:LX/1RN;

    .line 1732588
    iput-object p7, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;->h:LX/0ad;

    .line 1732589
    iget-object v0, p2, LX/1RN;->a:LX/1kK;

    check-cast v0, LX/1kW;

    .line 1732590
    iget-object v1, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, v1

    .line 1732591
    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->A()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;->c:LX/0Px;

    .line 1732592
    invoke-virtual {p6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020f36

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;->i:Landroid/graphics/drawable/Drawable;

    .line 1732593
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00fb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;->j:Landroid/graphics/drawable/Drawable;

    .line 1732594
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 1732571
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03039f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1732572
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v2, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;->b:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1732573
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v2, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;->b:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1732574
    new-instance v1, LX/AzU;

    invoke-direct {v1, p0, v0}, LX/AzU;-><init>(Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 2

    .prologue
    .line 1732577
    check-cast p1, LX/AzU;

    .line 1732578
    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p2, v0, :cond_0

    .line 1732579
    const/4 v0, 0x0

    const/4 v1, -0x2

    invoke-static {p1, v0, v1}, LX/AzU;->a$redex0(LX/AzU;Landroid/net/Uri;I)V

    .line 1732580
    :goto_0
    return-void

    .line 1732581
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;->c:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p1, v0, p2}, LX/AzU;->a$redex0(LX/AzU;Landroid/net/Uri;I)V

    goto :goto_0
.end method

.method public final ij_()I
    .locals 3

    .prologue
    .line 1732575
    iget-object v0, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;->h:LX/0ad;

    sget v1, LX/AzO;->f:I

    const/16 v2, 0x8

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    move v0, v0

    .line 1732576
    iget-object v1, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;->c:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    add-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/CoverPhotoPromptScrollAdapter;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
