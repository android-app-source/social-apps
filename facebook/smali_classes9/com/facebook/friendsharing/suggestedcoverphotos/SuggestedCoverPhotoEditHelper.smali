.class public Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:Landroid/app/Activity;

.field public final c:Ljava/lang/String;

.field private final d:LX/1HI;

.field public final e:LX/9iU;

.field public final f:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation
.end field

.field public final g:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field

.field public final h:LX/74n;

.field public final i:LX/BP8;

.field public final j:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation
.end field

.field private final k:LX/0kL;

.field public final l:LX/AzQ;

.field public final m:Lcom/facebook/productionprompts/logging/PromptAnalytics;

.field public n:LX/BP7;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1732412
    const-class v0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;

    const-class v1, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;LX/1HI;LX/9iU;LX/0TD;Ljava/util/concurrent/ExecutorService;LX/74n;LX/BP8;Ljava/lang/String;LX/0kL;LX/AzQ;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation

        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/productionprompts/logging/PromptAnalytics;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p7    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p10    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1732413
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1732414
    iput-object p1, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->b:Landroid/app/Activity;

    .line 1732415
    iput-object p2, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->c:Ljava/lang/String;

    .line 1732416
    iput-object p4, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->d:LX/1HI;

    .line 1732417
    iput-object p5, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->e:LX/9iU;

    .line 1732418
    iput-object p6, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->f:LX/0TD;

    .line 1732419
    iput-object p7, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->g:Ljava/util/concurrent/ExecutorService;

    .line 1732420
    iput-object p8, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->h:LX/74n;

    .line 1732421
    iput-object p9, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->i:LX/BP8;

    .line 1732422
    iput-object p10, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->j:Ljava/lang/String;

    .line 1732423
    iput-object p11, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->k:LX/0kL;

    .line 1732424
    iput-object p12, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->l:LX/AzQ;

    .line 1732425
    iput-object p3, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->m:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 1732426
    return-void
.end method

.method public static b(Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;)V
    .locals 3

    .prologue
    .line 1732427
    iget-object v0, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->k:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f08273c

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 1732428
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 1732429
    invoke-static {p1}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    .line 1732430
    if-nez v0, :cond_0

    .line 1732431
    invoke-static {p0}, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->b(Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;)V

    .line 1732432
    :goto_0
    return-void

    .line 1732433
    :cond_0
    iget-object v1, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->d:LX/1HI;

    sget-object v2, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, LX/1HI;->c(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    .line 1732434
    new-instance v1, LX/AzI;

    invoke-direct {v1, p0}, LX/AzI;-><init>(Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;)V

    iget-object v2, p0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->g:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, v1, v2}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
