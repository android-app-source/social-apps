.class public Lcom/facebook/friendsharing/meme/prompt/MemePromptScrollAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/Awy;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:I

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/1Ad;

.field public e:LX/B5l;

.field public f:LX/BMP;

.field public g:LX/1RN;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1727407
    const-class v0, Lcom/facebook/friendsharing/meme/prompt/MemePromptView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/meme/prompt/MemePromptScrollAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;LX/1RN;LX/1Ad;LX/B5l;LX/BMP;)V
    .locals 1
    .param p1    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1RN;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1727397
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1727398
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/meme/prompt/MemePromptScrollAdapter;->b:I

    .line 1727399
    iget-object v0, p2, LX/1RN;->a:LX/1kK;

    check-cast v0, LX/1kW;

    .line 1727400
    iget-object p1, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, p1

    .line 1727401
    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->z()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;->c()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/friendsharing/meme/prompt/MemePromptScrollAdapter;->c:LX/0Px;

    .line 1727402
    iput-object p2, p0, Lcom/facebook/friendsharing/meme/prompt/MemePromptScrollAdapter;->g:LX/1RN;

    .line 1727403
    iput-object p3, p0, Lcom/facebook/friendsharing/meme/prompt/MemePromptScrollAdapter;->d:LX/1Ad;

    .line 1727404
    iput-object p4, p0, Lcom/facebook/friendsharing/meme/prompt/MemePromptScrollAdapter;->e:LX/B5l;

    .line 1727405
    iput-object p5, p0, Lcom/facebook/friendsharing/meme/prompt/MemePromptScrollAdapter;->f:LX/BMP;

    .line 1727406
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 1727395
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030ac2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1727396
    new-instance v1, LX/Awy;

    invoke-direct {v1, p0, v0}, LX/Awy;-><init>(Lcom/facebook/friendsharing/meme/prompt/MemePromptScrollAdapter;Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 2

    .prologue
    .line 1727376
    check-cast p1, LX/Awy;

    .line 1727377
    const/4 v0, 0x3

    .line 1727378
    if-lt p2, v0, :cond_0

    if-ne p2, v0, :cond_1

    iget-object v0, p1, LX/Awy;->l:Lcom/facebook/friendsharing/meme/prompt/MemePromptScrollAdapter;

    iget-object v0, v0, Lcom/facebook/friendsharing/meme/prompt/MemePromptScrollAdapter;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p2, v0, :cond_1

    .line 1727379
    :cond_0
    iget-object v0, p1, LX/Awy;->m:Landroid/view/View;

    const v1, 0x7f0d1b7a

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1727380
    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget-object p0, p1, LX/Awy;->l:Lcom/facebook/friendsharing/meme/prompt/MemePromptScrollAdapter;

    iget p0, p0, Lcom/facebook/friendsharing/meme/prompt/MemePromptScrollAdapter;->b:I

    iput p0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1727381
    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget-object p0, p1, LX/Awy;->l:Lcom/facebook/friendsharing/meme/prompt/MemePromptScrollAdapter;

    iget p0, p0, Lcom/facebook/friendsharing/meme/prompt/MemePromptScrollAdapter;->b:I

    iput p0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1727382
    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, LX/Awy;->a(Landroid/content/res/Resources;)LX/1af;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 1727383
    iget-object v1, p1, LX/Awy;->l:Lcom/facebook/friendsharing/meme/prompt/MemePromptScrollAdapter;

    iget-object v1, v1, Lcom/facebook/friendsharing/meme/prompt/MemePromptScrollAdapter;->c:LX/0Px;

    invoke-virtual {v1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {p1, v1}, LX/Awy;->a(LX/Awy;Ljava/lang/String;)LX/1aZ;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1727384
    iget-object v1, p1, LX/Awy;->l:Lcom/facebook/friendsharing/meme/prompt/MemePromptScrollAdapter;

    iget-object v1, v1, Lcom/facebook/friendsharing/meme/prompt/MemePromptScrollAdapter;->c:LX/0Px;

    invoke-virtual {v1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {p1, v1}, LX/Awy;->b(LX/Awy;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1727385
    :goto_0
    return-void

    .line 1727386
    :cond_1
    iget-object v0, p1, LX/Awy;->m:Landroid/view/View;

    const v1, 0x7f0d1b7b

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/meme/prompt/MemePromptImageView;

    .line 1727387
    invoke-virtual {v0}, Lcom/facebook/friendsharing/meme/prompt/MemePromptImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget-object p0, p1, LX/Awy;->l:Lcom/facebook/friendsharing/meme/prompt/MemePromptScrollAdapter;

    iget p0, p0, Lcom/facebook/friendsharing/meme/prompt/MemePromptScrollAdapter;->b:I

    iput p0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1727388
    invoke-virtual {v0}, Lcom/facebook/friendsharing/meme/prompt/MemePromptImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget-object p0, p1, LX/Awy;->l:Lcom/facebook/friendsharing/meme/prompt/MemePromptScrollAdapter;

    iget p0, p0, Lcom/facebook/friendsharing/meme/prompt/MemePromptScrollAdapter;->b:I

    iput p0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1727389
    invoke-virtual {v0}, Lcom/facebook/friendsharing/meme/prompt/MemePromptImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, LX/Awy;->a(Landroid/content/res/Resources;)LX/1af;

    move-result-object v1

    iget-object p0, p1, LX/Awy;->m:Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-static {v1, p0}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object p0

    .line 1727390
    iget-object v1, p1, LX/Awy;->l:Lcom/facebook/friendsharing/meme/prompt/MemePromptScrollAdapter;

    iget-object v1, v1, Lcom/facebook/friendsharing/meme/prompt/MemePromptScrollAdapter;->c:LX/0Px;

    invoke-virtual {v1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {p1, v1}, LX/Awy;->a(LX/Awy;Ljava/lang/String;)LX/1aZ;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/1aX;->a(LX/1aZ;)V

    .line 1727391
    invoke-virtual {p0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/meme/prompt/MemePromptImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1727392
    iget-object v1, p1, LX/Awy;->l:Lcom/facebook/friendsharing/meme/prompt/MemePromptScrollAdapter;

    iget-object v1, v1, Lcom/facebook/friendsharing/meme/prompt/MemePromptScrollAdapter;->c:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    sub-int/2addr v1, p2

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/meme/prompt/MemePromptImageView;->setCount(I)V

    .line 1727393
    const/4 v1, 0x0

    invoke-static {p1, v1}, LX/Awy;->b(LX/Awy;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/meme/prompt/MemePromptImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1727394
    goto :goto_0
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 1727375
    iget-object v0, p0, Lcom/facebook/friendsharing/meme/prompt/MemePromptScrollAdapter;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x4

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method
