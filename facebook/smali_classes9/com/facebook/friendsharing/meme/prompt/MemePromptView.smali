.class public Lcom/facebook/friendsharing/meme/prompt/MemePromptView;
.super LX/Ale;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/AkB;
.implements LX/AkM;


# instance fields
.field public b:LX/Awz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Ax6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Z

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1727453
    invoke-direct {p0, p1}, LX/Ale;-><init>(Landroid/content/Context;)V

    .line 1727454
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/meme/prompt/MemePromptView;->d:Z

    .line 1727455
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1727450
    invoke-direct {p0, p1, p2}, LX/Ale;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1727451
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/meme/prompt/MemePromptView;->d:Z

    .line 1727452
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1727447
    invoke-direct {p0, p1, p2, p3}, LX/Ale;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1727448
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/meme/prompt/MemePromptView;->d:Z

    .line 1727449
    return-void
.end method

.method private static a(Lcom/facebook/friendsharing/meme/prompt/MemePromptView;LX/Awz;LX/Ax6;)V
    .locals 0

    .prologue
    .line 1727446
    iput-object p1, p0, Lcom/facebook/friendsharing/meme/prompt/MemePromptView;->b:LX/Awz;

    iput-object p2, p0, Lcom/facebook/friendsharing/meme/prompt/MemePromptView;->c:LX/Ax6;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/friendsharing/meme/prompt/MemePromptView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/friendsharing/meme/prompt/MemePromptView;

    const-class v0, LX/Awz;

    invoke-interface {v1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/Awz;

    const-class v2, LX/Ax6;

    invoke-interface {v1, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/Ax6;

    invoke-static {p0, v0, v1}, Lcom/facebook/friendsharing/meme/prompt/MemePromptView;->a(Lcom/facebook/friendsharing/meme/prompt/MemePromptView;LX/Awz;LX/Ax6;)V

    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 0

    .prologue
    .line 1727445
    return-object p0
.end method

.method public final a(LX/1RN;)V
    .locals 8

    .prologue
    .line 1727425
    iget-object v0, p1, LX/1RN;->a:LX/1kK;

    check-cast v0, LX/1kW;

    .line 1727426
    iget-object v1, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v1, v1

    .line 1727427
    if-eqz v1, :cond_0

    .line 1727428
    iget-object v1, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v1, v1

    .line 1727429
    invoke-virtual {v1}, Lcom/facebook/productionprompts/model/ProductionPrompt;->z()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    move-result-object v1

    if-nez v1, :cond_2

    :cond_0
    const/4 v0, 0x0

    .line 1727430
    :goto_0
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel$MemeStoriesModel;->a()I

    move-result v0

    if-lez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 1727431
    if-eqz v0, :cond_1

    .line 1727432
    iget-object v0, p0, Lcom/facebook/friendsharing/meme/prompt/MemePromptView;->c:LX/Ax6;

    invoke-virtual {p0}, Lcom/facebook/friendsharing/meme/prompt/MemePromptView;->getImageTrayHeight()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 1727433
    new-instance v2, Lcom/facebook/friendsharing/meme/prompt/MemeStoryPromptScrollAdapter;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v5

    check-cast v5, LX/1Ad;

    invoke-static {v0}, LX/B5l;->b(LX/0QB;)LX/B5l;

    move-result-object v6

    check-cast v6, LX/B5l;

    invoke-static {v0}, LX/BMP;->b(LX/0QB;)LX/BMP;

    move-result-object v7

    check-cast v7, LX/BMP;

    move-object v3, v1

    move-object v4, p1

    invoke-direct/range {v2 .. v7}, Lcom/facebook/friendsharing/meme/prompt/MemeStoryPromptScrollAdapter;-><init>(Ljava/lang/Integer;LX/1RN;LX/1Ad;LX/B5l;LX/BMP;)V

    .line 1727434
    move-object v0, v2

    .line 1727435
    :goto_2
    iget-object v1, p0, LX/Ale;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v1, v1

    .line 1727436
    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1727437
    return-void

    .line 1727438
    :cond_1
    iget-object v0, p0, Lcom/facebook/friendsharing/meme/prompt/MemePromptView;->b:LX/Awz;

    invoke-virtual {p0}, Lcom/facebook/friendsharing/meme/prompt/MemePromptView;->getImageTrayHeight()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 1727439
    new-instance v2, Lcom/facebook/friendsharing/meme/prompt/MemePromptScrollAdapter;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v5

    check-cast v5, LX/1Ad;

    invoke-static {v0}, LX/B5l;->b(LX/0QB;)LX/B5l;

    move-result-object v6

    check-cast v6, LX/B5l;

    invoke-static {v0}, LX/BMP;->b(LX/0QB;)LX/BMP;

    move-result-object v7

    check-cast v7, LX/BMP;

    move-object v3, v1

    move-object v4, p1

    invoke-direct/range {v2 .. v7}, Lcom/facebook/friendsharing/meme/prompt/MemePromptScrollAdapter;-><init>(Ljava/lang/Integer;LX/1RN;LX/1Ad;LX/B5l;LX/BMP;)V

    .line 1727440
    move-object v0, v2

    .line 1727441
    goto :goto_2

    .line 1727442
    :cond_2
    iget-object v1, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, v1

    .line 1727443
    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->z()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;->b()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel$MemeStoriesModel;

    move-result-object v0

    goto :goto_0

    .line 1727444
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1727422
    const-class v0, Lcom/facebook/friendsharing/meme/prompt/MemePromptView;

    invoke-static {v0, p0}, Lcom/facebook/friendsharing/meme/prompt/MemePromptView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1727423
    invoke-super {p0}, LX/Ale;->b()V

    .line 1727424
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1727456
    const/4 v0, 0x0

    return v0
.end method

.method public getCollapseAnimator()Landroid/animation/Animator;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1727421
    const/4 v0, 0x0

    return-object v0
.end method

.method public getExpandAnimator()Landroid/animation/Animator;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1727420
    const/4 v0, 0x0

    return-object v0
.end method

.method public getImageTrayHeight()I
    .locals 2

    .prologue
    .line 1727419
    invoke-virtual {p0}, Lcom/facebook/friendsharing/meme/prompt/MemePromptView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1a15

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 1727418
    iget-boolean v0, p0, Lcom/facebook/friendsharing/meme/prompt/MemePromptView;->e:Z

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 1727417
    iget-boolean v0, p0, Lcom/facebook/friendsharing/meme/prompt/MemePromptView;->d:Z

    return v0
.end method

.method public final k()V
    .locals 2

    .prologue
    .line 1727414
    iget-object v0, p0, LX/Ale;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v0, v0

    .line 1727415
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1727416
    return-void
.end method

.method public setHasBeenShown(Z)V
    .locals 0

    .prologue
    .line 1727412
    iput-boolean p1, p0, Lcom/facebook/friendsharing/meme/prompt/MemePromptView;->e:Z

    .line 1727413
    return-void
.end method

.method public setIsAnimationRunning(Z)V
    .locals 0

    .prologue
    .line 1727410
    iput-boolean p1, p0, Lcom/facebook/friendsharing/meme/prompt/MemePromptView;->d:Z

    .line 1727411
    return-void
.end method
