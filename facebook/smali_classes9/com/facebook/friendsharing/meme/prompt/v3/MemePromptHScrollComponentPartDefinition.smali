.class public Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptHScrollComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/1Ri;",
        "TE;>;"
    }
.end annotation


# instance fields
.field public final d:LX/0ad;

.field private final e:LX/Ax9;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0ad;LX/Ax9;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1727636
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1727637
    iput-object p2, p0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptHScrollComponentPartDefinition;->d:LX/0ad;

    .line 1727638
    iput-object p3, p0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptHScrollComponentPartDefinition;->e:LX/Ax9;

    .line 1727639
    return-void
.end method

.method private a(LX/1De;LX/1Ri;LX/1Pq;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1Ri;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1727640
    iget-object v0, p0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptHScrollComponentPartDefinition;->e:LX/Ax9;

    const/4 v1, 0x0

    .line 1727641
    new-instance v2, LX/Ax8;

    invoke-direct {v2, v0}, LX/Ax8;-><init>(LX/Ax9;)V

    .line 1727642
    iget-object p0, v0, LX/Ax9;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Ax7;

    .line 1727643
    if-nez p0, :cond_0

    .line 1727644
    new-instance p0, LX/Ax7;

    invoke-direct {p0, v0}, LX/Ax7;-><init>(LX/Ax9;)V

    .line 1727645
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/Ax7;->a$redex0(LX/Ax7;LX/1De;IILX/Ax8;)V

    .line 1727646
    move-object v2, p0

    .line 1727647
    move-object v1, v2

    .line 1727648
    move-object v0, v1

    .line 1727649
    iget-object v1, v0, LX/Ax7;->a:LX/Ax8;

    iput-object p3, v1, LX/Ax8;->a:LX/1Pq;

    .line 1727650
    iget-object v1, v0, LX/Ax7;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1727651
    move-object v0, v0

    .line 1727652
    iget-object v1, v0, LX/Ax7;->a:LX/Ax8;

    iput-object p2, v1, LX/Ax8;->b:LX/1Ri;

    .line 1727653
    iget-object v1, v0, LX/Ax7;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1727654
    move-object v0, v0

    .line 1727655
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1727656
    check-cast p2, LX/1Ri;

    check-cast p3, LX/1Pq;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptHScrollComponentPartDefinition;->a(LX/1De;LX/1Ri;LX/1Pq;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1727657
    check-cast p2, LX/1Ri;

    check-cast p3, LX/1Pq;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptHScrollComponentPartDefinition;->a(LX/1De;LX/1Ri;LX/1Pq;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 1727658
    check-cast p1, LX/1Ri;

    const/4 v1, 0x0

    .line 1727659
    if-eqz p1, :cond_0

    iget-object v0, p1, LX/1Ri;->a:LX/1RN;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    instance-of v0, v0, LX/1kW;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    check-cast v0, LX/1kW;

    .line 1727660
    iget-object v2, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, v2

    .line 1727661
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 1727662
    :goto_0
    return v0

    .line 1727663
    :cond_1
    iget-object v0, p1, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->a:LX/1kK;

    check-cast v0, LX/1kW;

    .line 1727664
    iget-object v2, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, v2

    .line 1727665
    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->z()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {p1}, LX/BMT;->a(LX/1Ri;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1727666
    iget-object v0, p0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptHScrollComponentPartDefinition;->d:LX/0ad;

    sget-short v2, LX/Awu;->a:S

    const/4 p1, 0x0

    invoke-interface {v0, v2, p1}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 1727667
    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1727668
    const/4 v0, 0x0

    return-object v0
.end method
