.class public Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptHScrollItemComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1727772
    const-class v0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptHScrollItemComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptHScrollItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1727758
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1727759
    iput-object p1, p0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptHScrollItemComponentSpec;->b:LX/0Or;

    .line 1727760
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptHScrollItemComponentSpec;
    .locals 4

    .prologue
    .line 1727761
    const-class v1, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptHScrollItemComponentSpec;

    monitor-enter v1

    .line 1727762
    :try_start_0
    sget-object v0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptHScrollItemComponentSpec;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1727763
    sput-object v2, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptHScrollItemComponentSpec;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1727764
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1727765
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1727766
    new-instance v3, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptHScrollItemComponentSpec;

    const/16 p0, 0x509

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptHScrollItemComponentSpec;-><init>(LX/0Or;)V

    .line 1727767
    move-object v0, v3

    .line 1727768
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1727769
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptHScrollItemComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1727770
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1727771
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
