.class public Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/Aws;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLInterfaces$MemeStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:LX/1Ad;

.field public final e:Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;

.field public final f:I

.field public final g:Landroid/content/res/Resources;

.field public h:Landroid/graphics/drawable/Drawable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1727304
    const-class v0, Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Px;LX/0Px;Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;Ljava/lang/Integer;LX/1Ad;)V
    .locals 3
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLInterfaces$MemeStory;",
            ">;",
            "Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter$MemeSelectDelegate;",
            "Ljava/lang/Integer;",
            "LX/1Ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1727295
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1727296
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;->g:Landroid/content/res/Resources;

    .line 1727297
    iput-object p2, p0, Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;->b:LX/0Px;

    .line 1727298
    iput-object p3, p0, Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;->c:LX/0Px;

    .line 1727299
    iput-object p6, p0, Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;->d:LX/1Ad;

    .line 1727300
    iput-object p4, p0, Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;->e:Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;

    .line 1727301
    invoke-virtual {p5}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;->g:Landroid/content/res/Resources;

    const v2, 0x7f0b1a17

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    mul-int/2addr v0, v1

    .line 1727302
    iget-object v1, p0, Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;->g:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    sub-int v0, v1, v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;->f:I

    .line 1727303
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 1727282
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030abf

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1727283
    new-instance v1, LX/Aws;

    invoke-direct {v1, p0, v0}, LX/Aws;-><init>(Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 2

    .prologue
    .line 1727285
    check-cast p1, LX/Aws;

    .line 1727286
    iget-object v0, p0, Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;->c:LX/0Px;

    if-nez v0, :cond_0

    .line 1727287
    iget-object v0, p0, Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;->b:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1727288
    iget-object p0, p1, LX/Aws;->m:Lcom/facebook/drawee/view/DraweeView;

    invoke-static {p1, v0}, LX/Aws;->b(LX/Aws;Ljava/lang/String;)LX/1aZ;

    move-result-object p2

    invoke-virtual {p0, p2}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1727289
    iget-object p0, p1, LX/Aws;->m:Lcom/facebook/drawee/view/DraweeView;

    new-instance p2, LX/Awq;

    invoke-direct {p2, p1, v0}, LX/Awq;-><init>(LX/Aws;Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lcom/facebook/drawee/view/DraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1727290
    :goto_0
    return-void

    .line 1727291
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;->c:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;

    .line 1727292
    iget-object p0, p1, LX/Aws;->m:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;->c()LX/0Px;

    move-result-object v1

    const/4 p2, 0x0

    invoke-virtual {v1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel;

    invoke-virtual {v1}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel;->a()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel;->a()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel$ImageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel$ImageModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, LX/Aws;->b(LX/Aws;Ljava/lang/String;)LX/1aZ;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1727293
    iget-object v1, p1, LX/Aws;->m:Lcom/facebook/drawee/view/DraweeView;

    new-instance p0, LX/Awr;

    invoke-direct {p0, p1, v0}, LX/Awr;-><init>(LX/Aws;Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;)V

    invoke-virtual {v1, p0}, Lcom/facebook/drawee/view/DraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1727294
    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1727284
    iget-object v0, p0, Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;->c:LX/0Px;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_0
.end method
