.class public Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:LX/Awt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1Kf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Awp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Landroid/support/v7/widget/RecyclerView;

.field public f:Ljava/lang/String;

.field private g:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

.field public h:Lcom/facebook/productionprompts/logging/PromptAnalytics;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1727251
    const-class v0, Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1727250
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;LX/0Px;LX/0Px;)LX/1OM;
    .locals 13
    .param p0    # Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLInterfaces$MemeStory;",
            ">;)",
            "LX/1OM;"
        }
    .end annotation

    .prologue
    .line 1727246
    iget-object v0, p0, Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;->b:LX/Awt;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object v2, p1

    move-object v3, p2

    move-object v4, p0

    .line 1727247
    new-instance v6, Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v12

    check-cast v12, LX/1Ad;

    move-object v7, v1

    move-object v8, v2

    move-object v9, v3

    move-object v10, v4

    move-object v11, v5

    invoke-direct/range {v6 .. v12}, Lcom/facebook/friendsharing/meme/activity/MemePickerScrollAdapter;-><init>(Landroid/content/Context;LX/0Px;LX/0Px;Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;Ljava/lang/Integer;LX/1Ad;)V

    .line 1727248
    move-object v0, v6

    .line 1727249
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1727252
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1727253
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;

    const-class v2, LX/Awt;

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/Awt;

    invoke-static {v0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object p1

    check-cast p1, LX/1Kf;

    invoke-static {v0}, LX/Awp;->b(LX/0QB;)LX/Awp;

    move-result-object v0

    check-cast v0, LX/Awp;

    iput-object v2, p0, Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;->b:LX/Awt;

    iput-object p1, p0, Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;->c:LX/1Kf;

    iput-object v0, p0, Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;->d:LX/Awp;

    .line 1727254
    return-void
.end method

.method public final a(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)V
    .locals 4

    .prologue
    .line 1727244
    iget-object v0, p0, Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;->c:LX/1Kf;

    iget-object v1, p0, Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;->g:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-static {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialShareParams(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v0, v1, v2, v3, p0}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/support/v4/app/Fragment;)V

    .line 1727245
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1727240
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 1727241
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1727242
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 1727243
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x337dac42

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1727239
    const v1, 0x7f030ac1

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0xbe26481

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1727217
    const v0, 0x7f0d1b78

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;->e:Landroid/support/v7/widget/RecyclerView;

    .line 1727218
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1727219
    const-string v1, "extra_meme_picker_session_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;->f:Ljava/lang/String;

    .line 1727220
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1727221
    const-string v1, "extra_meme_picker_composer_config"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    iput-object v0, p0, Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;->g:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1727222
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1727223
    const-string v1, "extra_meme_category"

    invoke-static {v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    .line 1727224
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 1727225
    const-string v2, "extra_prompt_analytics"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/productionprompts/logging/PromptAnalytics;

    iput-object v1, p0, Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;->h:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 1727226
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 1727227
    const-string v2, "extra_selected_meme_share_params"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    .line 1727228
    if-eqz v1, :cond_0

    .line 1727229
    invoke-virtual {p0, v1}, Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;->a(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)V

    .line 1727230
    :goto_0
    return-void

    .line 1727231
    :cond_0
    const/4 p2, 0x0

    .line 1727232
    new-instance v1, LX/62T;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, LX/62T;-><init>(Landroid/content/Context;I)V

    .line 1727233
    iget-object v2, p0, Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1727234
    iget-object v1, p0, Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;->e:Landroid/support/v7/widget/RecyclerView;

    new-instance v2, LX/62Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const p1, 0x7f0b1a17

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-direct {v2, v3}, LX/62Y;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 1727235
    invoke-virtual {v0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;->b()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel$MemeStoriesModel;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;->b()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel$MemeStoriesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel$MemeStoriesModel;->a()I

    move-result v1

    if-lez v1, :cond_1

    .line 1727236
    iget-object v1, p0, Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, LX/AxJ;->a(Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;)LX/0Px;

    move-result-object v2

    invoke-static {p0, p2, v2}, Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;->a(Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;LX/0Px;LX/0Px;)LX/1OM;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1727237
    :goto_1
    goto :goto_0

    .line 1727238
    :cond_1
    iget-object v1, p0, Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;->c()LX/0Px;

    move-result-object v2

    invoke-static {p0, v2, p2}, Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;->a(Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;LX/0Px;LX/0Px;)LX/1OM;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    goto :goto_1
.end method
