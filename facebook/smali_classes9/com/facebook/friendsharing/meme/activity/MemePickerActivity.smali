.class public Lcom/facebook/friendsharing/meme/activity/MemePickerActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private p:Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1727207
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Landroid/content/Context;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;Lcom/facebook/productionprompts/logging/PromptAnalytics;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Landroid/content/Intent;
    .locals 2
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/productionprompts/logging/PromptAnalytics;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/ipc/composer/intent/ComposerShareParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1727204
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/friendsharing/meme/activity/MemePickerActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "extra_meme_picker_session_id"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_meme_picker_composer_config"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_prompt_analytics"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_selected_meme_share_params"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 1727205
    const-string v1, "extra_meme_category"

    invoke-static {v0, v1, p3}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1727206
    return-object v0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1727181
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1727182
    const v0, 0x7f030ac0

    invoke-virtual {p0, v0}, Lcom/facebook/friendsharing/meme/activity/MemePickerActivity;->setContentView(I)V

    .line 1727183
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    sget-object v1, Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;

    iput-object v0, p0, Lcom/facebook/friendsharing/meme/activity/MemePickerActivity;->p:Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;

    .line 1727184
    iget-object v0, p0, Lcom/facebook/friendsharing/meme/activity/MemePickerActivity;->p:Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;

    if-nez v0, :cond_1

    .line 1727185
    invoke-virtual {p0}, Lcom/facebook/friendsharing/meme/activity/MemePickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1727186
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1727187
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1727188
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 1727189
    :cond_0
    new-instance v0, Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;

    invoke-direct {v0}, Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;-><init>()V

    iput-object v0, p0, Lcom/facebook/friendsharing/meme/activity/MemePickerActivity;->p:Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;

    .line 1727190
    iget-object v0, p0, Lcom/facebook/friendsharing/meme/activity/MemePickerActivity;->p:Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1727191
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d002f

    iget-object v2, p0, Lcom/facebook/friendsharing/meme/activity/MemePickerActivity;->p:Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;

    sget-object v3, Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1727192
    :cond_1
    return-void
.end method

.method public final onBackPressed()V
    .locals 7

    .prologue
    .line 1727193
    iget-object v0, p0, Lcom/facebook/friendsharing/meme/activity/MemePickerActivity;->p:Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;

    .line 1727194
    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x0

    .line 1727195
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 1727196
    iget-object v4, v0, Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;->h:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    if-eqz v4, :cond_0

    .line 1727197
    iget-object v4, v0, Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;->h:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    const-class v5, LX/1kW;

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-static {v4, v5, v6}, LX/BMQ;->a(Lcom/facebook/productionprompts/logging/PromptAnalytics;Ljava/lang/Class;Ljava/lang/Boolean;)Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1727198
    :cond_0
    move-object v3, v3

    .line 1727199
    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1727200
    iget-object v1, v0, Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;->d:LX/Awp;

    iget-object v2, v0, Lcom/facebook/friendsharing/meme/activity/MemePickerFragment;->f:Ljava/lang/String;

    .line 1727201
    iget-object v3, v1, LX/Awp;->a:LX/0Zb;

    const-string v4, "back_press"

    invoke-static {v4, v2}, LX/Awp;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v3, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1727202
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 1727203
    return-void
.end method
