.class public Lcom/facebook/friendsharing/gif/activity/GifPickerActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1718118
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1718119
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1718120
    const v0, 0x7f0307ac

    invoke-virtual {p0, v0}, Lcom/facebook/friendsharing/gif/activity/GifPickerActivity;->setContentView(I)V

    .line 1718121
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d1472

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    iput-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerActivity;->p:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    .line 1718122
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerActivity;->p:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    if-nez v0, :cond_0

    .line 1718123
    new-instance v0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    invoke-direct {v0}, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;-><init>()V

    iput-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerActivity;->p:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    .line 1718124
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerActivity;->p:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    invoke-virtual {p0}, Lcom/facebook/friendsharing/gif/activity/GifPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1718125
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d1472

    iget-object v2, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerActivity;->p:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1718126
    :cond_0
    return-void
.end method

.method public final onBackPressed()V
    .locals 4

    .prologue
    .line 1718127
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerActivity;->p:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    .line 1718128
    iget-object v1, v0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->q:Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;

    .line 1718129
    iget-object v2, v1, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->b:Lcom/facebook/ui/search/SearchEditText;

    move-object v1, v2

    .line 1718130
    invoke-static {v1}, Lcom/facebook/ui/search/SearchEditText;->i(Lcom/facebook/ui/search/SearchEditText;)V

    .line 1718131
    iget-object v1, v0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->d:LX/Aqg;

    iget-object v2, v0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->k:Ljava/lang/String;

    .line 1718132
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "gif_picker_back_pressed"

    invoke-direct {v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v0, "session_id"

    invoke-virtual {v3, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 1718133
    iget-object v0, v1, LX/Aqg;->a:LX/0Zb;

    invoke-interface {v0, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1718134
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 1718135
    return-void
.end method
