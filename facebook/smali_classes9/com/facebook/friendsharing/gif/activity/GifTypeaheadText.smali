.class public Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/0wW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/ui/search/SearchEditText;

.field public c:Landroid/widget/ImageView;

.field public d:LX/0wd;

.field private final e:Landroid/text/TextWatcher;

.field private final f:LX/0xi;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1718400
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1718401
    new-instance v0, LX/Aqr;

    invoke-direct {v0, p0}, LX/Aqr;-><init>(Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;)V

    iput-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->e:Landroid/text/TextWatcher;

    .line 1718402
    new-instance v0, LX/Aqs;

    invoke-direct {v0, p0}, LX/Aqs;-><init>(Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;)V

    iput-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->f:LX/0xi;

    .line 1718403
    invoke-direct {p0}, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->a()V

    .line 1718404
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1718437
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1718438
    new-instance v0, LX/Aqr;

    invoke-direct {v0, p0}, LX/Aqr;-><init>(Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;)V

    iput-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->e:Landroid/text/TextWatcher;

    .line 1718439
    new-instance v0, LX/Aqs;

    invoke-direct {v0, p0}, LX/Aqs;-><init>(Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;)V

    iput-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->f:LX/0xi;

    .line 1718440
    invoke-direct {p0}, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->a()V

    .line 1718441
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1718432
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1718433
    new-instance v0, LX/Aqr;

    invoke-direct {v0, p0}, LX/Aqr;-><init>(Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;)V

    iput-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->e:Landroid/text/TextWatcher;

    .line 1718434
    new-instance v0, LX/Aqs;

    invoke-direct {v0, p0}, LX/Aqs;-><init>(Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;)V

    iput-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->f:LX/0xi;

    .line 1718435
    invoke-direct {p0}, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->a()V

    .line 1718436
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1718425
    const-class v0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;

    invoke-static {v0, p0}, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1718426
    const v0, 0x7f0307ae

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1718427
    const v0, 0x7f0d1477

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/search/SearchEditText;

    iput-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->b:Lcom/facebook/ui/search/SearchEditText;

    .line 1718428
    const v0, 0x7f0d1478

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->c:Landroid/widget/ImageView;

    .line 1718429
    invoke-direct {p0}, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->b()V

    .line 1718430
    invoke-direct {p0}, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->c()V

    .line 1718431
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;

    invoke-static {v0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v0

    check-cast v0, LX/0wW;

    iput-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->a:LX/0wW;

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1718421
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-ge v0, v1, :cond_0

    .line 1718422
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->b:Lcom/facebook/ui/search/SearchEditText;

    const/16 v1, 0x13

    invoke-virtual {v0, v1}, Lcom/facebook/ui/search/SearchEditText;->setGravity(I)V

    .line 1718423
    :goto_0
    return-void

    .line 1718424
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->b:Lcom/facebook/ui/search/SearchEditText;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/facebook/ui/search/SearchEditText;->setTextAlignment(I)V

    goto :goto_0
.end method

.method private c()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 1718418
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->a:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    const-wide/high16 v4, 0x4028000000000000L    # 12.0

    invoke-static {v2, v3, v4, v5}, LX/0wT;->b(DD)LX/0wT;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, LX/0wd;->b(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->d:LX/0wd;

    .line 1718419
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->c:Landroid/widget/ImageView;

    new-instance v1, LX/Aqt;

    invoke-direct {v1, p0}, LX/Aqt;-><init>(Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1718420
    return-void
.end method


# virtual methods
.method public getSearchEditText()Lcom/facebook/ui/search/SearchEditText;
    .locals 1

    .prologue
    .line 1718417
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->b:Lcom/facebook/ui/search/SearchEditText;

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x53c023a6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1718411
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onAttachedToWindow()V

    .line 1718412
    iget-object v1, p0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->b:Lcom/facebook/ui/search/SearchEditText;

    iget-object v2, p0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->e:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Lcom/facebook/ui/search/SearchEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1718413
    iget-object v1, p0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->d:LX/0wd;

    iget-object v2, p0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->f:LX/0xi;

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 1718414
    iget-object v1, p0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->b:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v1}, Lcom/facebook/ui/search/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1718415
    iget-object v1, p0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->c:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1718416
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x2e4eb70f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x6196d393

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1718407
    iget-object v1, p0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->b:Lcom/facebook/ui/search/SearchEditText;

    iget-object v2, p0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->e:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Lcom/facebook/ui/search/SearchEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1718408
    iget-object v1, p0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->d:LX/0wd;

    invoke-virtual {v1}, LX/0wd;->k()LX/0wd;

    .line 1718409
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onDetachedFromWindow()V

    .line 1718410
    const/16 v1, 0x2d

    const v2, -0x35500c4b    # -5765594.5f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setClearTextButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1718405
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1718406
    return-void
.end method
