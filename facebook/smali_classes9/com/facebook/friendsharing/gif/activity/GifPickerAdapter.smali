.class public Lcom/facebook/friendsharing/gif/activity/GifPickerAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/Aqu;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/gif/model/GifModelContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/1Ad;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1718157
    const-class v0, Lcom/facebook/friendsharing/gif/activity/GifPickerAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/gif/activity/GifPickerAdapter;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1Ad;LX/0Ot;Landroid/content/Context;LX/0Px;Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;)V
    .locals 0
    .param p3    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ad;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/gif/model/GifModelContainer;",
            ">;",
            "Lcom/facebook/friendsharing/gif/activity/GifPickerAdapter$OnSelect;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1718158
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1718159
    iput-object p1, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerAdapter;->d:LX/1Ad;

    .line 1718160
    iput-object p2, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerAdapter;->e:LX/0Ot;

    .line 1718161
    iput-object p3, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerAdapter;->b:Landroid/content/Context;

    .line 1718162
    iput-object p4, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerAdapter;->c:LX/0Px;

    .line 1718163
    iput-object p5, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerAdapter;->f:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    .line 1718164
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 1718165
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerAdapter;->b:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 1718166
    const v1, 0x7f03133c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1718167
    new-instance v1, LX/Aqu;

    invoke-direct {v1, v0}, LX/Aqu;-><init>(Landroid/view/View;)V

    .line 1718168
    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 4

    .prologue
    .line 1718169
    check-cast p1, LX/Aqu;

    .line 1718170
    iget-object v0, p1, LX/Aqu;->m:Landroid/view/View;

    move-object v0, v0

    .line 1718171
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerAdapter;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1718172
    iget-object v0, p1, LX/Aqu;->m:Landroid/view/View;

    move-object v0, v0

    .line 1718173
    new-instance v1, LX/Aqd;

    invoke-direct {v1, p0, p2}, LX/Aqd;-><init>(Lcom/facebook/friendsharing/gif/activity/GifPickerAdapter;I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1718174
    iget-object v0, p1, LX/Aqu;->n:Landroid/widget/ProgressBar;

    move-object v0, v0

    .line 1718175
    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1718176
    iget-object v0, p1, LX/Aqu;->n:Landroid/widget/ProgressBar;

    move-object v0, v0

    .line 1718177
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1718178
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerAdapter;->d:LX/1Ad;

    const-class v1, Lcom/facebook/friendsharing/gif/activity/GifPickerAdapter;

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    .line 1718179
    iget-object v1, p1, LX/Aqu;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v1, v1

    .line 1718180
    invoke-virtual {v1}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    iget-object v1, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerAdapter;->c:LX/0Px;

    invoke-virtual {v1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsharing/gif/model/GifModelContainer;

    invoke-virtual {v1}, Lcom/facebook/friendsharing/gif/model/GifModelContainer;->a()Lcom/facebook/friendsharing/gif/model/GifModelSubContainer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/gif/model/GifModelSubContainer;->a()Lcom/facebook/friendsharing/gif/model/GifModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/gif/model/GifModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v0

    new-instance v1, LX/Aqe;

    invoke-direct {v1, p0, p1}, LX/Aqe;-><init>(Lcom/facebook/friendsharing/gif/activity/GifPickerAdapter;LX/Aqu;)V

    invoke-virtual {v0, v1}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1718181
    iget-object v1, p1, LX/Aqu;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v1, v1

    .line 1718182
    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1718183
    :goto_0
    return-void

    .line 1718184
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1718185
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerAdapter;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v2, Lcom/facebook/friendsharing/gif/activity/GifPickerAdapter;->a:Ljava/lang/String;

    const-string v3, "Problem with DraweeView "

    invoke-virtual {v0, v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1718186
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerAdapter;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
