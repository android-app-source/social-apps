.class public Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field public static final h:Ljava/lang/String;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Aqg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/Aqf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Aqc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/1Kf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private i:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

.field public j:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

.field public k:Ljava/lang/String;

.field private l:Lcom/facebook/friendsharing/gif/activity/GifPickerAdapter;

.field private m:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field private n:LX/3xT;

.field private o:Landroid/widget/ProgressBar;

.field public p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/friendsharing/gif/model/GifModelContainer;",
            ">;"
        }
    .end annotation
.end field

.field public q:Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;

.field public r:Landroid/widget/TextView;

.field private s:LX/Aqq;

.field private final t:Landroid/view/View$OnClickListener;

.field private final u:Landroid/text/TextWatcher;

.field private final v:Landroid/view/View$OnClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1718375
    const-class v0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->h:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1718371
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1718372
    new-instance v0, LX/Aqh;

    invoke-direct {v0, p0}, LX/Aqh;-><init>(Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;)V

    iput-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->t:Landroid/view/View$OnClickListener;

    .line 1718373
    new-instance v0, LX/Aqi;

    invoke-direct {v0, p0}, LX/Aqi;-><init>(Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;)V

    iput-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->u:Landroid/text/TextWatcher;

    .line 1718374
    new-instance v0, LX/Aqj;

    invoke-direct {v0, p0}, LX/Aqj;-><init>(Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;)V

    iput-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->v:Landroid/view/View$OnClickListener;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    move-object v1, p1

    check-cast v1, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    const/16 v2, 0x12b1

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x259

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {p0}, LX/0tb;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, LX/0TD;

    new-instance v6, LX/Aqg;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-direct {v6, v5}, LX/Aqg;-><init>(LX/0Zb;)V

    move-object v5, v6

    check-cast v5, LX/Aqg;

    const-class v6, LX/Aqf;

    invoke-interface {p0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/Aqf;

    new-instance p1, LX/Aqc;

    const/16 v7, 0x259

    invoke-static {p0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v7

    check-cast v7, LX/0lB;

    invoke-direct {p1, v0, v7}, LX/Aqc;-><init>(LX/0Ot;LX/0lB;)V

    move-object v7, p1

    check-cast v7, LX/Aqc;

    invoke-static {p0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object p0

    check-cast p0, LX/1Kf;

    iput-object v2, v1, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->a:LX/0Ot;

    iput-object v3, v1, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->b:LX/0Ot;

    iput-object v4, v1, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->c:LX/0TD;

    iput-object v5, v1, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->d:LX/Aqg;

    iput-object v6, v1, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->e:LX/Aqf;

    iput-object v7, v1, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->f:LX/Aqc;

    iput-object p0, v1, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->g:LX/1Kf;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;LX/Aqq;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/16 v2, 0x8

    .line 1718342
    iput-object p1, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->s:LX/Aqq;

    .line 1718343
    sget-object v0, LX/Aqp;->a:[I

    invoke-virtual {p1}, LX/Aqq;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1718344
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->m:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 1718345
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->o:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1718346
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1718347
    :goto_0
    return-void

    .line 1718348
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1718349
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->m:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 1718350
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->o:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 1718351
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1718352
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->o:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1718353
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->m:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v5}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    goto :goto_0

    .line 1718354
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->m:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 1718355
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->o:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1718356
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->r:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08272c

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->q:Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;

    .line 1718357
    iget-object p1, v4, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->b:Lcom/facebook/ui/search/SearchEditText;

    move-object v4, p1

    .line 1718358
    invoke-virtual {v4}, Lcom/facebook/ui/search/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1718359
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->r:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08272c

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->q:Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;

    .line 1718360
    iget-object p1, v4, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->b:Lcom/facebook/ui/search/SearchEditText;

    move-object v4, p1

    .line 1718361
    invoke-virtual {v4}, Lcom/facebook/ui/search/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1718362
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 1718363
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 1718364
    :pswitch_3
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->m:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 1718365
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->o:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1718366
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->r:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08272b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1718367
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->r:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08272b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1718368
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 1718369
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1718370
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->r:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->t:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static c(Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/gif/model/GifModelContainer;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1718341
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->c:LX/0TD;

    new-instance v1, LX/Aqo;

    invoke-direct {v1, p0, p1}, LX/Aqo;-><init>(Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;Ljava/lang/String;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static d$redex0(Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1718338
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->q:Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;

    .line 1718339
    iget-object p0, v0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->b:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, p0

    .line 1718340
    invoke-virtual {v0}, Lcom/facebook/ui/search/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static e$redex0(Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;)V
    .locals 4

    .prologue
    .line 1718376
    const-string v0, "http://api.giphy.com/v1/gifs/trending?api_key=dc6zaTOxFJmzC"

    invoke-static {p0, v0}, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->c(Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 1718377
    sget-object v0, LX/Aqq;->FETCH:LX/Aqq;

    invoke-static {p0, v0}, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->a$redex0(Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;LX/Aqq;)V

    .line 1718378
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    const-string v2, "TrendingGifsTask"

    new-instance v3, LX/Aqm;

    invoke-direct {v3, p0, v1}, LX/Aqm;-><init>(Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;Lcom/google/common/util/concurrent/ListenableFuture;)V

    invoke-virtual {v0, v2, v1, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1718379
    return-void
.end method

.method public static k(Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;)V
    .locals 9

    .prologue
    .line 1718324
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->s:LX/Aqq;

    sget-object v1, LX/Aqq;->ERROR:LX/Aqq;

    if-ne v0, v1, :cond_0

    .line 1718325
    :goto_0
    return-void

    .line 1718326
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1718327
    sget-object v0, LX/Aqq;->NO_RESULTS:LX/Aqq;

    invoke-static {p0, v0}, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->a$redex0(Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;LX/Aqq;)V

    goto :goto_0

    .line 1718328
    :cond_1
    sget-object v0, LX/Aqq;->IDLE:LX/Aqq;

    invoke-static {p0, v0}, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->a$redex0(Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;LX/Aqq;)V

    .line 1718329
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->l:Lcom/facebook/friendsharing/gif/activity/GifPickerAdapter;

    if-eqz v0, :cond_2

    .line 1718330
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->l:Lcom/facebook/friendsharing/gif/activity/GifPickerAdapter;

    iget-object v1, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->p:Ljava/util/List;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 1718331
    iput-object v1, v0, Lcom/facebook/friendsharing/gif/activity/GifPickerAdapter;->c:LX/0Px;

    .line 1718332
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->l:Lcom/facebook/friendsharing/gif/activity/GifPickerAdapter;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    goto :goto_0

    .line 1718333
    :cond_2
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->e:LX/Aqf;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->p:Ljava/util/List;

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    .line 1718334
    new-instance v3, Lcom/facebook/friendsharing/gif/activity/GifPickerAdapter;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v4

    check-cast v4, LX/1Ad;

    const/16 v5, 0x259

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    move-object v6, v1

    move-object v7, v2

    move-object v8, p0

    invoke-direct/range {v3 .. v8}, Lcom/facebook/friendsharing/gif/activity/GifPickerAdapter;-><init>(LX/1Ad;LX/0Ot;Landroid/content/Context;LX/0Px;Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;)V

    .line 1718335
    move-object v0, v3

    .line 1718336
    iput-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->l:Lcom/facebook/friendsharing/gif/activity/GifPickerAdapter;

    .line 1718337
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->m:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->l:Lcom/facebook/friendsharing/gif/activity/GifPickerAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1718320
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1718321
    const-class v0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    invoke-static {v0, p0}, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1718322
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->p:Ljava/util/List;

    .line 1718323
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1718316
    packed-switch p1, :pswitch_data_0

    .line 1718317
    :goto_0
    return-void

    .line 1718318
    :pswitch_0
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1718319
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x4d8
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/16 v0, 0x2a

    const v1, -0x8b7e96b

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1718288
    const v0, 0x7f0307ab

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 1718289
    const v0, 0x7f0d146d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iput-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->i:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 1718290
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->i:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f082729

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(Ljava/lang/String;)V

    .line 1718291
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->i:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    new-instance v3, LX/Aqk;

    invoke-direct {v3, p0}, LX/Aqk;-><init>(Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 1718292
    if-eqz p3, :cond_1

    .line 1718293
    const-string v0, "gif_picker_session_id_instance_state"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->k:Ljava/lang/String;

    .line 1718294
    :cond_0
    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1718295
    const-string v3, "extra_gif_picker_launcher_settings"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    iput-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->j:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1718296
    const v0, 0x7f0d1471

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->o:Landroid/widget/ProgressBar;

    .line 1718297
    const v0, 0x7f0d1470

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->m:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1718298
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->m:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1718299
    iput-boolean v5, v0, Landroid/support/v7/widget/RecyclerView;->v:Z

    .line 1718300
    new-instance v0, LX/3xT;

    const/4 v3, 0x1

    invoke-direct {v0, v6, v3}, LX/3xT;-><init>(II)V

    iput-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->n:LX/3xT;

    .line 1718301
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->m:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v3, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->n:LX/3xT;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1718302
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->m:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v3, LX/62Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b1a0b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-direct {v3, v4}, LX/62Y;-><init>(I)V

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 1718303
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->m:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v3, LX/Aql;

    invoke-direct {v3, p0}, LX/Aql;-><init>(Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;)V

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(LX/1OX;)V

    .line 1718304
    const v0, 0x7f0d146e

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;

    iput-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->q:Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;

    .line 1718305
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->q:Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;

    iget-object v3, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->v:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->setClearTextButtonListener(Landroid/view/View$OnClickListener;)V

    .line 1718306
    const v0, 0x7f0d146f

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->r:Landroid/widget/TextView;

    .line 1718307
    invoke-static {p0}, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->e$redex0(Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;)V

    .line 1718308
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->d:LX/Aqg;

    iget-object v3, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->k:Ljava/lang/String;

    .line 1718309
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "gif_picker_on_fragment_create"

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "session_id"

    invoke-virtual {v4, v5, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 1718310
    iget-object v5, v0, LX/Aqg;->a:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1718311
    const v0, 0xb9e1c83

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-object v2

    .line 1718312
    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1718313
    const-string v3, "extra_gif_picker_launcher_id"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->k:Ljava/lang/String;

    .line 1718314
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->k:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1718315
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->k:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x2b269724

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1718282
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 1718283
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->q:Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;

    .line 1718284
    iget-object v2, v0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->b:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, v2

    .line 1718285
    iget-object v2, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->u:Landroid/text/TextWatcher;

    invoke-virtual {v0, v2}, Lcom/facebook/ui/search/SearchEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1718286
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1718287
    const/16 v0, 0x2b

    const v2, 0x205d8ad0

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0xd78075a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1718274
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1718275
    iget-object v1, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->q:Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;

    .line 1718276
    iget-object v2, v1, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->b:Lcom/facebook/ui/search/SearchEditText;

    move-object v1, v2

    .line 1718277
    iget-object v2, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->u:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Lcom/facebook/ui/search/SearchEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1718278
    const/16 v1, 0x2b

    const v2, -0x31032d4b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1718279
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1718280
    const-string v0, "gif_picker_session_id_instance_state"

    iget-object v1, p0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1718281
    return-void
.end method
