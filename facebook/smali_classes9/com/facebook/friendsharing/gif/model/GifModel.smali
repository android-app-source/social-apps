.class public Lcom/facebook/friendsharing/gif/model/GifModel;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/gif/model/GifModelDeserializer;
.end annotation


# instance fields
.field private mHeight:Ljava/lang/Integer;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "height"
    .end annotation
.end field

.field private mUrl:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "url"
    .end annotation
.end field

.field private mWidth:Ljava/lang/Integer;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "width"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1718471
    const-class v0, Lcom/facebook/friendsharing/gif/model/GifModelDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1718472
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1718473
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/model/GifModel;->mUrl:Ljava/lang/String;

    return-object v0
.end method
