.class public Lcom/facebook/friendsharing/gif/model/GifModelSubContainer;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/gif/model/GifModelSubContainerDeserializer;
.end annotation


# instance fields
.field private mModel:Lcom/facebook/friendsharing/gif/model/GifModel;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "fixed_width"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1718521
    const-class v0, Lcom/facebook/friendsharing/gif/model/GifModelSubContainerDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1718522
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/friendsharing/gif/model/GifModel;
    .locals 1

    .prologue
    .line 1718523
    iget-object v0, p0, Lcom/facebook/friendsharing/gif/model/GifModelSubContainer;->mModel:Lcom/facebook/friendsharing/gif/model/GifModel;

    return-object v0
.end method
