.class public Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierModelParams;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field public mModelId:Ljava/lang/String;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public mSerializedModel:Ljava/lang/String;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public mThreshold:D
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1728419
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1728420
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierModelParams;->mSerializedModel:Ljava/lang/String;

    .line 1728421
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierModelParams;->mModelId:Ljava/lang/String;

    .line 1728422
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierModelParams;->mThreshold:D

    .line 1728423
    return-void
.end method


# virtual methods
.method public setModelId(Ljava/lang/String;)V
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1728424
    iput-object p1, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierModelParams;->mModelId:Ljava/lang/String;

    .line 1728425
    return-void
.end method

.method public setSerializedModel(Ljava/lang/String;)V
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1728426
    iput-object p1, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierModelParams;->mSerializedModel:Ljava/lang/String;

    .line 1728427
    return-void
.end method

.method public setThreshold(D)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1728428
    iput-wide p1, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierModelParams;->mThreshold:D

    .line 1728429
    return-void
.end method
