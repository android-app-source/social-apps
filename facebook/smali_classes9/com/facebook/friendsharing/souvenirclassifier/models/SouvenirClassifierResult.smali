.class public Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierResult;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field public mIsOverThreshold:Z
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public mLoggingData:Ljava/util/Map;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mScore:D
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public mSouvenirClassifierResult:Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierResult$SouvenirClassifierResultWrapper;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierResult$SouvenirClassifierResultWrapper;)V
    .locals 4
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1728435
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1728436
    iput-object p1, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierResult;->mSouvenirClassifierResult:Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierResult$SouvenirClassifierResultWrapper;

    .line 1728437
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierResult;->mSouvenirClassifierResult:Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierResult$SouvenirClassifierResultWrapper;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierResult$SouvenirClassifierResultWrapper;->getAllKeysFromLoggingData()[[B

    move-result-object v1

    .line 1728438
    if-eqz v1, :cond_0

    array-length v0, v1

    if-nez v0, :cond_2

    .line 1728439
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierResult;->mLoggingData:Ljava/util/Map;

    .line 1728440
    :cond_1
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierResult;->mSouvenirClassifierResult:Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierResult$SouvenirClassifierResultWrapper;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierResult$SouvenirClassifierResultWrapper;->getScore()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierResult;->mScore:D

    .line 1728441
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierResult;->mSouvenirClassifierResult:Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierResult$SouvenirClassifierResultWrapper;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierResult$SouvenirClassifierResultWrapper;->getIsOverThreshold()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierResult;->mIsOverThreshold:Z

    .line 1728442
    return-void

    .line 1728443
    :cond_2
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierResult;->mLoggingData:Ljava/util/Map;

    .line 1728444
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 1728445
    new-instance v2, Ljava/lang/String;

    aget-object v3, v1, v0

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([B)V

    .line 1728446
    new-instance v3, Ljava/lang/String;

    iget-object p1, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierResult;->mSouvenirClassifierResult:Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierResult$SouvenirClassifierResultWrapper;

    invoke-virtual {p1, v2}, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierResult$SouvenirClassifierResultWrapper;->getValueFromLoggingDataWithKey(Ljava/lang/String;)[B

    move-result-object p1

    invoke-direct {v3, p1}, Ljava/lang/String;-><init>([B)V

    .line 1728447
    iget-object p1, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierResult;->mLoggingData:Ljava/util/Map;

    invoke-interface {p1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1728448
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
