.class public final Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierResult$SouvenirClassifierResultWrapper;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field public final mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1728433
    const-string v0, "souvenirclassifier-jni"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 1728434
    return-void
.end method

.method public constructor <init>(Lcom/facebook/jni/HybridData;)V
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1728430
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1728431
    iput-object p1, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierResult$SouvenirClassifierResultWrapper;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 1728432
    return-void
.end method


# virtual methods
.method public native getAllKeysFromLoggingData()[[B
.end method

.method public native getIsOverThreshold()Z
.end method

.method public native getScore()D
.end method

.method public native getValueFromLoggingDataWithKey(Ljava/lang/String;)[B
.end method
