.class public final Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifier$SouvenirClassifierWrapper;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1728406
    const-string v0, "souvenirclassifier-jni"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 1728407
    return-void
.end method

.method public constructor <init>(Lcom/facebook/compactdisk/StoreManagerFactory;)V
    .locals 2

    .prologue
    .line 1728402
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1728403
    const-string v0, "diskstoremanager_fb4a"

    invoke-virtual {p1, v0}, Lcom/facebook/compactdisk/StoreManagerFactory;->a(Ljava/lang/String;)Lcom/facebook/compactdisk/StoreManager;

    move-result-object v0

    const-string v1, "souvenirs_classifier_cache"

    invoke-virtual {v0, v1}, Lcom/facebook/compactdisk/StoreManager;->a(Ljava/lang/String;)Lcom/facebook/compactdisk/PersistentKeyValueStore;

    move-result-object v0

    .line 1728404
    invoke-static {v0}, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifier$SouvenirClassifierWrapper;->initHybrid(Lcom/facebook/compactdisk/PersistentKeyValueStore;)Lcom/facebook/jni/HybridData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifier$SouvenirClassifierWrapper;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 1728405
    return-void
.end method

.method private static native initHybrid(Lcom/facebook/compactdisk/PersistentKeyValueStore;)Lcom/facebook/jni/HybridData;
.end method


# virtual methods
.method public native classifySouvenir(Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierInput;)Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierResult$SouvenirClassifierResultWrapper;
.end method

.method public native currentlyUsedModelId()[B
.end method

.method public native setModelParams(Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierModelParams;)V
.end method

.method public native setServerFeatures(Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierServerFeatures;)V
.end method
