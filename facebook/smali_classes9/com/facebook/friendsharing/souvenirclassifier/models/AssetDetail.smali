.class public Lcom/facebook/friendsharing/souvenirclassifier/models/AssetDetail;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final assetID:Ljava/lang/String;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final assetType:Ljava/lang/String;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final blurrinessMetric:Lcom/facebook/friendsharing/souvenirclassifier/models/OptionalImageMetricValue;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final creationDateSinceEpoch:J
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final darknessMetric:Lcom/facebook/friendsharing/souvenirclassifier/models/OptionalImageMetricValue;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final isFavorite:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final isHidden:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final latitude:D
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final longitude:D
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final mediaSubtypes:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final modificationDateSinceEpoch:J
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final pixelHeight:D
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final pixelWidth:D
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final sourceType:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final videoDuration:D
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;DDDDDLjava/util/Date;IIIILcom/facebook/friendsharing/souvenirclassifier/models/OptionalImageMetricValue;Lcom/facebook/friendsharing/souvenirclassifier/models/OptionalImageMetricValue;)V
    .locals 4

    .prologue
    .line 1728368
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1728369
    iput-object p1, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/AssetDetail;->assetID:Ljava/lang/String;

    .line 1728370
    iput-object p2, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/AssetDetail;->assetType:Ljava/lang/String;

    .line 1728371
    iput-wide p6, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/AssetDetail;->pixelHeight:D

    .line 1728372
    iput-wide p4, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/AssetDetail;->pixelWidth:D

    .line 1728373
    iput-wide p8, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/AssetDetail;->latitude:D

    .line 1728374
    iput-wide p10, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/AssetDetail;->longitude:D

    .line 1728375
    move-wide/from16 v0, p12

    iput-wide v0, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/AssetDetail;->videoDuration:D

    .line 1728376
    move/from16 v0, p15

    iput v0, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/AssetDetail;->isFavorite:I

    .line 1728377
    move/from16 v0, p16

    iput v0, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/AssetDetail;->isHidden:I

    .line 1728378
    move/from16 v0, p17

    iput v0, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/AssetDetail;->mediaSubtypes:I

    .line 1728379
    move/from16 v0, p18

    iput v0, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/AssetDetail;->sourceType:I

    .line 1728380
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/AssetDetail;->blurrinessMetric:Lcom/facebook/friendsharing/souvenirclassifier/models/OptionalImageMetricValue;

    .line 1728381
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/AssetDetail;->darknessMetric:Lcom/facebook/friendsharing/souvenirclassifier/models/OptionalImageMetricValue;

    .line 1728382
    invoke-virtual {p3}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/AssetDetail;->creationDateSinceEpoch:J

    .line 1728383
    if-nez p14, :cond_0

    .line 1728384
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/AssetDetail;->modificationDateSinceEpoch:J

    .line 1728385
    :goto_0
    return-void

    .line 1728386
    :cond_0
    invoke-virtual/range {p14 .. p14}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/AssetDetail;->modificationDateSinceEpoch:J

    goto :goto_0
.end method
