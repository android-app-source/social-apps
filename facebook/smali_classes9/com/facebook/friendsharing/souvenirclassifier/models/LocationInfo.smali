.class public Lcom/facebook/friendsharing/souvenirclassifier/models/LocationInfo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final isLandmark:Z
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final latitude:D
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final longitude:D
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;ZDD)V
    .locals 1

    .prologue
    .line 1728392
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1728393
    iput-object p1, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/LocationInfo;->name:Ljava/lang/String;

    .line 1728394
    iput-boolean p2, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/LocationInfo;->isLandmark:Z

    .line 1728395
    iput-wide p3, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/LocationInfo;->longitude:D

    .line 1728396
    iput-wide p5, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/LocationInfo;->latitude:D

    .line 1728397
    return-void
.end method
