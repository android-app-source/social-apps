.class public Lcom/facebook/friendsharing/souvenirclassifier/models/AssetCounts;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final burstCount:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final facesCount:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final loopsCount:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final photosCount:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final photosWithFacesCount:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final tileCount:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final totalAssetsCount:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final totalAssetsWithinBurstsCount:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final totalAssetsWithinLoopsCount:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final videosCount:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public constructor <init>(IIIIIIIIII)V
    .locals 0

    .prologue
    .line 1728356
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1728357
    iput p1, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/AssetCounts;->tileCount:I

    .line 1728358
    iput p2, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/AssetCounts;->totalAssetsCount:I

    .line 1728359
    iput p3, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/AssetCounts;->photosCount:I

    .line 1728360
    iput p4, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/AssetCounts;->burstCount:I

    .line 1728361
    iput p5, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/AssetCounts;->loopsCount:I

    .line 1728362
    iput p6, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/AssetCounts;->videosCount:I

    .line 1728363
    iput p7, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/AssetCounts;->totalAssetsWithinBurstsCount:I

    .line 1728364
    iput p8, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/AssetCounts;->totalAssetsWithinLoopsCount:I

    .line 1728365
    iput p9, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/AssetCounts;->facesCount:I

    .line 1728366
    iput p10, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/AssetCounts;->photosWithFacesCount:I

    .line 1728367
    return-void
.end method
