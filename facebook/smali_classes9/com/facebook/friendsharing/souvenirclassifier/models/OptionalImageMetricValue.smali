.class public Lcom/facebook/friendsharing/souvenirclassifier/models/OptionalImageMetricValue;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final isCalculated:Z
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final value:D
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public constructor <init>(ZD)V
    .locals 0

    .prologue
    .line 1728398
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1728399
    iput-boolean p1, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/OptionalImageMetricValue;->isCalculated:Z

    .line 1728400
    iput-wide p2, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/OptionalImageMetricValue;->value:D

    .line 1728401
    return-void
.end method
