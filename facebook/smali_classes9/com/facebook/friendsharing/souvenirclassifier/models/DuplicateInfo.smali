.class public Lcom/facebook/friendsharing/souvenirclassifier/models/DuplicateInfo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final assetIdA:Ljava/lang/String;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final assetIdB:Ljava/lang/String;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final dupeCoefficient:D
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;D)V
    .locals 1

    .prologue
    .line 1728387
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1728388
    iput-object p1, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/DuplicateInfo;->assetIdA:Ljava/lang/String;

    .line 1728389
    iput-object p2, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/DuplicateInfo;->assetIdB:Ljava/lang/String;

    .line 1728390
    iput-wide p3, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/DuplicateInfo;->dupeCoefficient:D

    .line 1728391
    return-void
.end method
