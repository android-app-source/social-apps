.class public Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierInput;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field public final assetCounts:Lcom/facebook/friendsharing/souvenirclassifier/models/AssetCounts;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final assetDetails:[Lcom/facebook/friendsharing/souvenirclassifier/models/AssetDetail;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final collectionID:Ljava/lang/String;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final duplicateInfos:[Lcom/facebook/friendsharing/souvenirclassifier/models/DuplicateInfo;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final locationInfo:Lcom/facebook/friendsharing/souvenirclassifier/models/LocationInfo;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final timeSinceStory:D
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;DLcom/facebook/friendsharing/souvenirclassifier/models/LocationInfo;Lcom/facebook/friendsharing/souvenirclassifier/models/AssetCounts;[Lcom/facebook/friendsharing/souvenirclassifier/models/AssetDetail;[Lcom/facebook/friendsharing/souvenirclassifier/models/DuplicateInfo;)V
    .locals 0

    .prologue
    .line 1728411
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1728412
    iput-object p1, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierInput;->collectionID:Ljava/lang/String;

    .line 1728413
    iput-wide p2, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierInput;->timeSinceStory:D

    .line 1728414
    iput-object p4, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierInput;->locationInfo:Lcom/facebook/friendsharing/souvenirclassifier/models/LocationInfo;

    .line 1728415
    iput-object p5, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierInput;->assetCounts:Lcom/facebook/friendsharing/souvenirclassifier/models/AssetCounts;

    .line 1728416
    iput-object p6, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierInput;->assetDetails:[Lcom/facebook/friendsharing/souvenirclassifier/models/AssetDetail;

    .line 1728417
    iput-object p7, p0, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierInput;->duplicateInfos:[Lcom/facebook/friendsharing/souvenirclassifier/models/DuplicateInfo;

    .line 1728418
    return-void
.end method
