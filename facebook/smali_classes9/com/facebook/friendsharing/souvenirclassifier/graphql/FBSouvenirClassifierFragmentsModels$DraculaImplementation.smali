.class public final Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1727979
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1727980
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1727858
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1727859
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 10

    .prologue
    const-wide/16 v4, 0x0

    const/4 v7, 0x2

    const-wide/16 v8, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1727931
    if-nez p1, :cond_0

    .line 1727932
    :goto_0
    return v0

    .line 1727933
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1727934
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1727935
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v2

    .line 1727936
    const v3, 0x589340c2

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v2

    .line 1727937
    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1727938
    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 1727939
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1727940
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v2

    .line 1727941
    const v3, 0x4d32ac3e    # 1.87352032E8f

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 1727942
    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1727943
    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 1727944
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1727945
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v2

    .line 1727946
    const v3, 0x537097f7

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v6

    .line 1727947
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 1727948
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 1727949
    invoke-virtual {p3, v0, v6}, LX/186;->b(II)V

    move-object v0, p3

    .line 1727950
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1727951
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1727952
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1727953
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1727954
    invoke-virtual {p0, p1, v1, v8, v9}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1727955
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 1727956
    invoke-virtual {p3, v0, v4}, LX/186;->b(II)V

    move-object v0, p3

    move-wide v4, v8

    .line 1727957
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1727958
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1727959
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v2

    .line 1727960
    const v3, -0x3ea3ceed

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v2

    .line 1727961
    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1727962
    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 1727963
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1727964
    :sswitch_5
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v2

    .line 1727965
    const v3, -0x17ed3715

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 1727966
    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1727967
    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 1727968
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1727969
    :sswitch_6
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1727970
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1727971
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1727972
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1727973
    invoke-virtual {p0, p1, v7, v8, v9}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1727974
    const/4 v6, 0x3

    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1727975
    invoke-virtual {p3, v0, v4}, LX/186;->b(II)V

    .line 1727976
    invoke-virtual {p3, v1, v5}, LX/186;->b(II)V

    move-object v0, p3

    move v1, v7

    move-wide v4, v8

    .line 1727977
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1727978
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x3ea3ceed -> :sswitch_5
        -0x17ed3715 -> :sswitch_6
        0x24cec9ee -> :sswitch_4
        0x4d32ac3e -> :sswitch_2
        0x537097f7 -> :sswitch_3
        0x589340c2 -> :sswitch_1
        0x7ec5eca8 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1727930
    new-instance v0, Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1727917
    sparse-switch p2, :sswitch_data_0

    .line 1727918
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1727919
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1727920
    const v1, 0x589340c2

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1727921
    :goto_0
    :sswitch_1
    return-void

    .line 1727922
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1727923
    const v1, 0x4d32ac3e    # 1.87352032E8f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1727924
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1727925
    const v1, 0x537097f7

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1727926
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1727927
    const v1, -0x3ea3ceed

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1727928
    :sswitch_5
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1727929
    const v1, -0x17ed3715

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x3ea3ceed -> :sswitch_5
        -0x17ed3715 -> :sswitch_1
        0x24cec9ee -> :sswitch_4
        0x4d32ac3e -> :sswitch_3
        0x537097f7 -> :sswitch_1
        0x589340c2 -> :sswitch_2
        0x7ec5eca8 -> :sswitch_0
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1727907
    if-nez p1, :cond_0

    move v0, v1

    .line 1727908
    :goto_0
    return v0

    .line 1727909
    :cond_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v2

    .line 1727910
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1727911
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1727912
    invoke-virtual {p0, p1, v1}, LX/15i;->q(II)I

    move-result v3

    .line 1727913
    invoke-static {p0, v3, p2, p3}, Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    aput v3, v0, v1

    .line 1727914
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1727915
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1727916
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 1727900
    if-eqz p1, :cond_0

    .line 1727901
    invoke-virtual {p0, p1}, LX/15i;->d(I)I

    move-result v1

    .line 1727902
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1727903
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v2

    .line 1727904
    invoke-static {p0, v2, p2, p3}, Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1727905
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1727906
    :cond_0
    return-void
.end method

.method private static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1727894
    if-eqz p1, :cond_0

    .line 1727895
    invoke-static {p0, p1, p2}, Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$DraculaImplementation;

    move-result-object v1

    .line 1727896
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$DraculaImplementation;

    .line 1727897
    if-eq v0, v1, :cond_0

    .line 1727898
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1727899
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1727893
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1727891
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1727892
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1727886
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1727887
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1727888
    :cond_0
    iput-object p1, p0, Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$DraculaImplementation;->a:LX/15i;

    .line 1727889
    iput p2, p0, Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$DraculaImplementation;->b:I

    .line 1727890
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1727885
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1727884
    new-instance v0, Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1727881
    iget v0, p0, LX/1vt;->c:I

    .line 1727882
    move v0, v0

    .line 1727883
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1727878
    iget v0, p0, LX/1vt;->c:I

    .line 1727879
    move v0, v0

    .line 1727880
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1727875
    iget v0, p0, LX/1vt;->b:I

    .line 1727876
    move v0, v0

    .line 1727877
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1727872
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1727873
    move-object v0, v0

    .line 1727874
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1727863
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1727864
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1727865
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1727866
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1727867
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1727868
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1727869
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1727870
    invoke-static {v3, v9, v2}, Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1727871
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1727860
    iget v0, p0, LX/1vt;->c:I

    .line 1727861
    move v0, v0

    .line 1727862
    return v0
.end method
