.class public final Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$FBSouvenirClassifierFeaturesQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1728037
    const-class v0, Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$FBSouvenirClassifierFeaturesQueryModel;

    new-instance v1, Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$FBSouvenirClassifierFeaturesQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$FBSouvenirClassifierFeaturesQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1728038
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1728039
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1727981
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1727982
    const/4 v2, 0x0

    .line 1727983
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1727984
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1727985
    :goto_0
    move v1, v2

    .line 1727986
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1727987
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1727988
    new-instance v1, Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$FBSouvenirClassifierFeaturesQueryModel;

    invoke-direct {v1}, Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$FBSouvenirClassifierFeaturesQueryModel;-><init>()V

    .line 1727989
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1727990
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1727991
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1727992
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1727993
    :cond_0
    return-object v1

    .line 1727994
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1727995
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1727996
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1727997
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1727998
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 1727999
    const-string v4, "souvenir_classifier_features_vectors"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1728000
    const/4 v3, 0x0

    .line 1728001
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_9

    .line 1728002
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1728003
    :goto_2
    move v1, v3

    .line 1728004
    goto :goto_1

    .line 1728005
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1728006
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1728007
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 1728008
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1728009
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_8

    .line 1728010
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1728011
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1728012
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 1728013
    const-string v5, "edges"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1728014
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1728015
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_7

    .line 1728016
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_7

    .line 1728017
    const/4 v5, 0x0

    .line 1728018
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v6, :cond_d

    .line 1728019
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1728020
    :goto_5
    move v4, v5

    .line 1728021
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1728022
    :cond_7
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 1728023
    goto :goto_3

    .line 1728024
    :cond_8
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1728025
    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1728026
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v1, v3

    goto :goto_3

    .line 1728027
    :cond_a
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1728028
    :cond_b
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, p0, :cond_c

    .line 1728029
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1728030
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1728031
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_b

    if-eqz v6, :cond_b

    .line 1728032
    const-string p0, "node"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1728033
    invoke-static {p1, v0}, LX/AxP;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_6

    .line 1728034
    :cond_c
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 1728035
    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 1728036
    invoke-virtual {v0}, LX/186;->d()I

    move-result v5

    goto :goto_5

    :cond_d
    move v4, v5

    goto :goto_6
.end method
