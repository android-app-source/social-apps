.class public Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;
.super Landroid/widget/ImageView;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:LX/1HI;

.field public c:LX/1Ck;

.field public d:LX/1ap;

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:Ljava/lang/String;

.field public final j:Landroid/graphics/Paint;

.field public final k:Landroid/graphics/Paint;

.field private final l:Landroid/graphics/Rect;

.field public m:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;

.field public n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1861566
    const-class v0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;

    const-string v1, "souvenirs"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1861548
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1861549
    iput v0, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->e:I

    .line 1861550
    iput v0, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->f:I

    .line 1861551
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->j:Landroid/graphics/Paint;

    .line 1861552
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->k:Landroid/graphics/Paint;

    .line 1861553
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->l:Landroid/graphics/Rect;

    .line 1861554
    const/4 p1, 0x1

    .line 1861555
    const-class v0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;

    invoke-static {v0, p0}, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1861556
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1861557
    const v1, 0x7f0b1a28

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->h:I

    .line 1861558
    const v1, 0x7f0b1a29

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->g:I

    .line 1861559
    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->j:Landroid/graphics/Paint;

    const v2, 0x7f0a0766

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1861560
    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->j:Landroid/graphics/Paint;

    invoke-virtual {v1, p1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1861561
    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->k:Landroid/graphics/Paint;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1861562
    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->k:Landroid/graphics/Paint;

    const v2, 0x7f0b1a2a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1861563
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->k:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1861564
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->k:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 1861565
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;

    invoke-static {p0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v1

    check-cast v1, LX/1HI;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object p0

    check-cast p0, LX/1Ck;

    iput-object v1, p1, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->b:LX/1HI;

    iput-object p0, p1, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->c:LX/1Ck;

    return-void
.end method

.method private e()V
    .locals 1

    .prologue
    .line 1861545
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->m:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;

    if-nez v0, :cond_0

    .line 1861546
    :goto_0
    return-void

    .line 1861547
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->m:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;

    invoke-virtual {p0, v0}, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->a(Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;)V

    goto :goto_0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 1861540
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->n:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1861541
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 1861542
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0

    .line 1861543
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1861544
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1861535
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->d:LX/1ap;

    if-nez v0, :cond_0

    .line 1861536
    :goto_0
    return-void

    .line 1861537
    :cond_0
    iget v0, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->e:I

    iget v1, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->f:I

    if-lt v0, v1, :cond_1

    .line 1861538
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->e:I

    .line 1861539
    :cond_1
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->d:LX/1ap;

    iget v1, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->e:I

    invoke-virtual {v0, v1}, LX/1ap;->g(I)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 1861512
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->BURST:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    if-eq v1, v2, :cond_1

    .line 1861513
    :cond_0
    :goto_0
    return-void

    .line 1861514
    :cond_1
    iput-object p1, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->m:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;

    .line 1861515
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1861516
    iput v0, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->e:I

    .line 1861517
    iput v0, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->f:I

    .line 1861518
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1861519
    invoke-virtual {p1}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->c()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v0

    .line 1861520
    :goto_1
    if-ge v1, v4, :cond_3

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;

    .line 1861521
    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;->a()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 1861522
    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;->a()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->j()LX/1Fb;

    move-result-object v0

    .line 1861523
    if-eqz v0, :cond_2

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 1861524
    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const/16 v7, 0x190

    .line 1861525
    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v5

    new-instance v6, LX/1o9;

    invoke-direct {v6, v7, v7}, LX/1o9;-><init>(II)V

    .line 1861526
    iput-object v6, v5, LX/1bX;->c:LX/1o9;

    .line 1861527
    move-object v5, v5

    .line 1861528
    const/4 v6, 0x1

    invoke-virtual {v5, v6}, LX/1bX;->a(Z)LX/1bX;

    move-result-object v5

    .line 1861529
    invoke-virtual {v5}, LX/1bX;->n()LX/1bf;

    move-result-object v5

    .line 1861530
    iget-object v6, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->b:LX/1HI;

    sget-object v7, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v6, v5, v7}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v5

    invoke-static {v5}, LX/24r;->a(LX/1ca;)LX/24r;

    move-result-object v5

    move-object v0, v5

    .line 1861531
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1861532
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1861533
    :cond_3
    invoke-static {v2}, LX/0Vg;->b(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1861534
    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->c:LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "fetching_media_for_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/CEa;

    invoke-direct {v3, p0, v0}, LX/CEa;-><init>(Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;Lcom/google/common/util/concurrent/ListenableFuture;)V

    new-instance v0, LX/CEb;

    invoke-direct {v0, p0}, LX/CEb;-><init>(Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;)V

    invoke-virtual {v1, v2, v3, v0}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto/16 :goto_0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x6f6a5b32

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1861509
    invoke-super {p0}, Landroid/widget/ImageView;->onAttachedToWindow()V

    .line 1861510
    invoke-direct {p0}, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->e()V

    .line 1861511
    const/16 v1, 0x2d

    const v2, 0x2f8baf1e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x5fbc36f6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1861492
    invoke-super {p0}, Landroid/widget/ImageView;->onDetachedFromWindow()V

    .line 1861493
    invoke-direct {p0}, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->f()V

    .line 1861494
    const/16 v1, 0x2d

    const v2, 0x4163ac30

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    .line 1861501
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 1861502
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->d:LX/1ap;

    if-eqz v0, :cond_0

    .line 1861503
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->g:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->h:I

    sub-int/2addr v0, v1

    .line 1861504
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    iget v2, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->g:I

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->h:I

    sub-int/2addr v1, v2

    .line 1861505
    int-to-float v2, v0

    int-to-float v3, v1

    iget v4, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->g:I

    int-to-float v4, v4

    iget-object v5, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->j:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1861506
    iget-object v2, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->k:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->i:Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->i:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    iget-object v6, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->l:Landroid/graphics/Rect;

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 1861507
    iget-object v2, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->i:Ljava/lang/String;

    int-to-float v0, v0

    int-to-float v1, v1

    iget-object v3, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->l:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v3

    sub-float/2addr v1, v3

    iget-object v3, p0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->k:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v0, v1, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1861508
    :cond_0
    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 0

    .prologue
    .line 1861498
    invoke-super {p0}, Landroid/widget/ImageView;->onFinishTemporaryDetach()V

    .line 1861499
    invoke-direct {p0}, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->e()V

    .line 1861500
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 0

    .prologue
    .line 1861495
    invoke-super {p0}, Landroid/widget/ImageView;->onStartTemporaryDetach()V

    .line 1861496
    invoke-direct {p0}, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->f()V

    .line 1861497
    return-void
.end method
