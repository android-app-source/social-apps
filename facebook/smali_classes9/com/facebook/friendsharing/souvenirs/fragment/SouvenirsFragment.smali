.class public Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;
.super Lcom/facebook/widget/popover/PopoverFragment;
.source ""


# static fields
.field private static final P:Landroid/graphics/Rect;

.field private static final y:Ljava/lang/String;


# instance fields
.field private final A:LX/CET;

.field private final B:LX/CEW;

.field public C:Ljava/util/TimerTask;

.field public D:Lcom/facebook/greetingcards/verve/render/FlatVerveRecyclerView;

.field public E:Landroid/widget/ProgressBar;

.field private F:LX/8qU;

.field private G:LX/Ca8;

.field public H:Landroid/os/Handler;

.field public I:Ljava/util/Timer;

.field private J:I

.field public K:Ljava/lang/String;

.field public L:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:I

.field public N:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;",
            ">;"
        }
    .end annotation
.end field

.field private volatile O:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;",
            ">;"
        }
    .end annotation
.end field

.field private Q:Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;

.field public R:I

.field public m:LX/CEg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/AyD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/CEn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/CEd;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1L1;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/16I;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/2hU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/Ca4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/ipc/composer/launch/ShareComposerLauncher;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/CEZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final z:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1861423
    const-class v0, LX/CEg;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->y:Ljava/lang/String;

    .line 1861424
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->P:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1861367
    invoke-direct {p0}, Lcom/facebook/widget/popover/PopoverFragment;-><init>()V

    .line 1861368
    new-instance v0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment$1;

    invoke-direct {v0, p0}, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment$1;-><init>(Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;)V

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->z:Ljava/lang/Runnable;

    .line 1861369
    new-instance v0, LX/CET;

    invoke-direct {v0, p0}, LX/CET;-><init>(Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;)V

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->A:LX/CET;

    .line 1861370
    new-instance v0, LX/CEW;

    invoke-direct {v0, p0}, LX/CEW;-><init>(Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;)V

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->B:LX/CEW;

    .line 1861371
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->J:I

    .line 1861372
    iput v1, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->M:I

    .line 1861373
    iput v1, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->R:I

    .line 1861374
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    move-object v1, p1

    check-cast v1, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;

    invoke-static {p0}, LX/CEg;->a(LX/0QB;)LX/CEg;

    move-result-object v2

    check-cast v2, LX/CEg;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {p0}, LX/AyD;->a(LX/0QB;)LX/AyD;

    move-result-object v4

    check-cast v4, LX/AyD;

    const-class v5, LX/CEn;

    invoke-interface {p0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/CEn;

    invoke-static {p0}, LX/CEd;->a(LX/0QB;)LX/CEd;

    move-result-object v6

    check-cast v6, LX/CEd;

    const/16 v7, 0xf2b

    invoke-static {p0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {p0}, LX/16I;->a(LX/0QB;)LX/16I;

    move-result-object v8

    check-cast v8, LX/16I;

    invoke-static {p0}, LX/2hU;->b(LX/0QB;)LX/2hU;

    move-result-object v9

    check-cast v9, LX/2hU;

    invoke-static {p0}, LX/Ca4;->b(LX/0QB;)LX/Ca4;

    move-result-object v10

    check-cast v10, LX/Ca4;

    const/16 v11, 0xbc6

    invoke-static {p0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x2581

    invoke-static {p0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    new-instance v0, LX/CEZ;

    invoke-static {p0}, LX/1Bv;->a(LX/0QB;)LX/1Bv;

    move-result-object p1

    check-cast p1, LX/1Bv;

    invoke-direct {v0, p1}, LX/CEZ;-><init>(LX/1Bv;)V

    move-object p0, v0

    check-cast p0, LX/CEZ;

    iput-object v2, v1, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->m:LX/CEg;

    iput-object v3, v1, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->n:LX/1Ck;

    iput-object v4, v1, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->o:LX/AyD;

    iput-object v5, v1, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->p:LX/CEn;

    iput-object v6, v1, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->q:LX/CEd;

    iput-object v7, v1, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->r:LX/0Ot;

    iput-object v8, v1, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->s:LX/16I;

    iput-object v9, v1, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->t:LX/2hU;

    iput-object v10, v1, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->u:LX/Ca4;

    iput-object v11, v1, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->v:LX/0Ot;

    iput-object v12, v1, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->w:LX/0Ot;

    iput-object p0, v1, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->x:LX/CEZ;

    return-void
.end method

.method public static a(Landroid/content/Context;LX/2hU;LX/16I;)Z
    .locals 2

    .prologue
    .line 1861375
    invoke-virtual {p2}, LX/16I;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1861376
    new-instance v1, LX/BEF;

    const-class v0, Landroid/app/Activity;

    invoke-static {p0, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/BEF;-><init>(Landroid/content/Context;)V

    const/16 v0, 0xbb8

    invoke-virtual {p1, v1, v0}, LX/2hU;->a(Landroid/view/View;I)LX/4nS;

    move-result-object v0

    invoke-virtual {v0}, LX/4nS;->a()V

    .line 1861377
    const/4 v0, 0x1

    .line 1861378
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;)V
    .locals 4

    .prologue
    .line 1861379
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->K:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1861380
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->n:LX/1Ck;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "fetchSouvenir_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->K:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/CEN;

    invoke-direct {v2, p0}, LX/CEN;-><init>(Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;)V

    new-instance v3, LX/CEQ;

    invoke-direct {v3, p0}, LX/CEQ;-><init>(Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1861381
    return-void

    .line 1861382
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;LX/Ca6;)V
    .locals 3
    .param p0    # Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1861383
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->L:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 1861384
    :goto_0
    iget-object v2, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->G:LX/Ca8;

    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->L:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->L:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, LX/14w;->q(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-interface {v2, v1}, LX/Ca7;->setShareButtonEnabled(Z)V

    .line 1861385
    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->G:LX/Ca8;

    invoke-interface {v1, p1, v0}, LX/Ca7;->a(LX/Ca6;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1861386
    return-void

    .line 1861387
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->L:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1861388
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1861389
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    goto :goto_0

    .line 1861390
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static b(Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;)V
    .locals 3

    .prologue
    .line 1861391
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->N:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1861392
    :cond_0
    :goto_0
    return-void

    .line 1861393
    :cond_1
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->Q:Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->Q:Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;

    .line 1861394
    iget v1, v0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->e:I

    iget v2, v0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->f:I

    if-ge v1, v2, :cond_5

    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 1861395
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->Q:Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;

    sget-object v1, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->P:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1861396
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->Q:Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->a()V

    goto :goto_0

    .line 1861397
    :cond_2
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->O:Ljava/util/Iterator;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->O:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1861398
    :cond_3
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->N:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->O:Ljava/util/Iterator;

    .line 1861399
    :cond_4
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->O:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1861400
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->O:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;

    .line 1861401
    sget-object v1, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->P:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1861402
    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->Q:Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;

    .line 1861403
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->Q:Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;

    .line 1861404
    const/4 v1, 0x0

    iput v1, v0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->e:I

    .line 1861405
    iget-object v1, v0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->d:LX/1ap;

    if-nez v1, :cond_6

    .line 1861406
    :goto_2
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->Q:Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->a()V

    goto :goto_0

    :cond_5
    const/4 v1, 0x0

    goto :goto_1

    .line 1861407
    :cond_6
    iget-object v1, v0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->d:LX/1ap;

    iget v2, v0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->e:I

    invoke-virtual {v1, v2}, LX/1ap;->g(I)V

    goto :goto_2
.end method


# virtual methods
.method public final S_()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1861408
    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->u:LX/Ca4;

    .line 1861409
    invoke-static {v1}, LX/Ca4;->e(LX/Ca4;)LX/0gv;

    move-result-object v2

    .line 1861410
    if-nez v2, :cond_2

    .line 1861411
    const/4 v2, 0x0

    .line 1861412
    :goto_0
    move v1, v2

    .line 1861413
    if-eqz v1, :cond_0

    .line 1861414
    :goto_1
    return v0

    .line 1861415
    :cond_0
    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->u:LX/Ca4;

    invoke-virtual {v1}, LX/Ca4;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1861416
    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->u:LX/Ca4;

    .line 1861417
    invoke-static {v1}, LX/Ca4;->e(LX/Ca4;)LX/0gv;

    .line 1861418
    goto :goto_1

    .line 1861419
    :cond_1
    invoke-super {p0}, Lcom/facebook/widget/popover/PopoverFragment;->S_()Z

    move-result v0

    goto :goto_1

    :cond_2
    invoke-interface {v2}, LX/0gv;->a()Z

    move-result v2

    goto :goto_0
.end method

.method public final o()LX/8qU;
    .locals 1

    .prologue
    .line 1861420
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->F:LX/8qU;

    if-nez v0, :cond_0

    .line 1861421
    new-instance v0, LX/CER;

    invoke-direct {v0, p0}, LX/CER;-><init>(Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;)V

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->F:LX/8qU;

    .line 1861422
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->F:LX/8qU;

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0xda73344

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1861330
    invoke-super {p0, p1}, Lcom/facebook/widget/popover/PopoverFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1861331
    const v0, 0x7f0e09cb

    invoke-virtual {p0, v3, v0}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 1861332
    const-class v0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;

    invoke-static {v0, p0}, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1861333
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->setHasOptionsMenu(Z)V

    .line 1861334
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1861335
    const-string v2, "param_souvenir_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->K:Ljava/lang/String;

    .line 1861336
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1861337
    const-string v2, "param_feed_cache_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->L:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1861338
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->N:Ljava/util/List;

    .line 1861339
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->H:Landroid/os/Handler;

    .line 1861340
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->I:Ljava/util/Timer;

    .line 1861341
    invoke-static {p0}, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->a$redex0(Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;)V

    .line 1861342
    const/16 v0, 0x2b

    const v2, 0x28742fa

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x3b7f5b73

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1861343
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/widget/popover/PopoverFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v3

    .line 1861344
    const v0, 0x7f0d0807

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1861345
    const v1, 0x7f031391

    invoke-virtual {p1, v1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1861346
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0b1a23

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->M:I

    .line 1861347
    const v0, 0x7f0d2d37

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/verve/render/FlatVerveRecyclerView;

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->D:Lcom/facebook/greetingcards/verve/render/FlatVerveRecyclerView;

    .line 1861348
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->D:Lcom/facebook/greetingcards/verve/render/FlatVerveRecyclerView;

    new-instance v4, LX/CEM;

    invoke-direct {v4, p0}, LX/CEM;-><init>(Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;)V

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 1861349
    const v0, 0x7f0d2d38

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->E:Landroid/widget/ProgressBar;

    .line 1861350
    const v0, 0x7f0d2d39

    invoke-static {v1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/Ca8;

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->G:LX/Ca8;

    .line 1861351
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->G:LX/Ca8;

    .line 1861352
    new-instance v1, LX/CES;

    invoke-direct {v1, p0}, LX/CES;-><init>(Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;)V

    move-object v1, v1

    .line 1861353
    invoke-interface {v0, v1}, LX/Ca7;->setListener(LX/CES;)V

    .line 1861354
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->G:LX/Ca8;

    invoke-interface {v0}, LX/Ca7;->a()V

    .line 1861355
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->G:LX/Ca8;

    invoke-interface {v0, v5}, LX/Ca7;->setTagButtonEnabled(Z)V

    .line 1861356
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->G:LX/Ca8;

    invoke-interface {v0, v5}, LX/Ca7;->setMenuButtonEnabled(Z)V

    .line 1861357
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->L:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->L:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1861358
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1861359
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 1861360
    :goto_0
    invoke-static {p0, v0}, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->a$redex0(Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;LX/Ca6;)V

    .line 1861361
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->u:LX/Ca4;

    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->B:LX/CEW;

    .line 1861362
    iput-object v1, v0, LX/Ca4;->f:LX/CEW;

    .line 1861363
    const v0, -0x63c10386

    invoke-static {v0, v2}, LX/02F;->f(II)V

    return-object v3

    .line 1861364
    :cond_1
    new-instance v1, LX/Ca6;

    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->L:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1861365
    iget-object v4, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v4

    .line 1861366
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-direct {v1, v0}, LX/Ca6;-><init>(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5cc8cd9e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1861290
    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->u:LX/Ca4;

    invoke-virtual {v1}, LX/Ca4;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1861291
    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->u:LX/Ca4;

    .line 1861292
    invoke-static {v1}, LX/Ca4;->e(LX/Ca4;)LX/0gv;

    .line 1861293
    :cond_0
    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->u:LX/Ca4;

    .line 1861294
    const/4 v2, 0x0

    iput-object v2, v1, LX/Ca4;->f:LX/CEW;

    .line 1861295
    iget-object v2, v1, LX/Ca4;->k:LX/1My;

    if-eqz v2, :cond_1

    .line 1861296
    iget-object v2, v1, LX/Ca4;->k:LX/1My;

    invoke-virtual {v2}, LX/1My;->a()V

    .line 1861297
    :cond_1
    invoke-super {p0}, Lcom/facebook/widget/popover/PopoverFragment;->onDestroy()V

    .line 1861298
    const/16 v1, 0x2b

    const v2, 0x4ecb49d4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0xa987f93

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1861299
    invoke-super {p0}, Lcom/facebook/widget/popover/PopoverFragment;->onDestroyView()V

    .line 1861300
    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->N:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1861301
    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->I:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 1861302
    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->I:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 1861303
    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->I:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->purge()I

    .line 1861304
    :cond_0
    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->H:Landroid/os/Handler;

    if-eqz v1, :cond_1

    .line 1861305
    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->H:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1861306
    :cond_1
    const/16 v1, 0x2b

    const v2, -0x7c7715ac

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6386c06b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1861307
    invoke-super {p0}, Lcom/facebook/widget/popover/PopoverFragment;->onPause()V

    .line 1861308
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1L1;

    iget-object v2, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->A:LX/CET;

    invoke-virtual {v0, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 1861309
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->x:LX/CEZ;

    .line 1861310
    iget-object v2, v0, LX/CEZ;->e:LX/CEY;

    if-eqz v2, :cond_0

    .line 1861311
    iget-object v2, v0, LX/CEZ;->e:LX/CEY;

    invoke-static {v2}, LX/CEZ;->c(LX/CEY;)V

    .line 1861312
    const/4 v2, 0x0

    iput-object v2, v0, LX/CEZ;->e:LX/CEY;

    .line 1861313
    :cond_0
    const/16 v0, 0x2b

    const v2, 0x620da159

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x2c68f741

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1861314
    invoke-super {p0}, Lcom/facebook/widget/popover/PopoverFragment;->onResume()V

    .line 1861315
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1L1;

    iget-object v2, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->A:LX/CET;

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 1861316
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->x:LX/CEZ;

    .line 1861317
    invoke-static {v0}, LX/CEZ;->c(LX/CEZ;)V

    .line 1861318
    const/16 v0, 0x2b

    const v2, 0x752f7e7d

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6eb2e83b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1861319
    invoke-virtual {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->h()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v1

    iput v1, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->J:I

    .line 1861320
    invoke-virtual {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->h()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 1861321
    invoke-super {p0}, Lcom/facebook/widget/popover/PopoverFragment;->onStart()V

    .line 1861322
    const/16 v1, 0x2b

    const v2, 0x596f1075

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x25ed93ad

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1861327
    invoke-virtual {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->h()Landroid/app/Activity;

    move-result-object v1

    iget v2, p0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->J:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 1861328
    invoke-super {p0}, Lcom/facebook/widget/popover/PopoverFragment;->onStop()V

    .line 1861329
    const/16 v1, 0x2b

    const v2, 0x3b048793

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final s()I
    .locals 2

    .prologue
    .line 1861323
    sget-object v0, LX/31M;->LEFT:LX/31M;

    invoke-virtual {v0}, LX/31M;->flag()I

    move-result v0

    sget-object v1, LX/31M;->RIGHT:LX/31M;

    invoke-virtual {v1}, LX/31M;->flag()I

    move-result v1

    or-int/2addr v0, v1

    return v0
.end method

.method public final t()I
    .locals 2

    .prologue
    .line 1861324
    sget-object v0, LX/31M;->LEFT:LX/31M;

    invoke-virtual {v0}, LX/31M;->flag()I

    move-result v0

    sget-object v1, LX/31M;->RIGHT:LX/31M;

    invoke-virtual {v1}, LX/31M;->flag()I

    move-result v1

    or-int/2addr v0, v1

    return v0
.end method

.method public final u()LX/31M;
    .locals 1

    .prologue
    .line 1861325
    sget-object v0, LX/31M;->LEFT:LX/31M;

    return-object v0
.end method

.method public final v()LX/31M;
    .locals 1

    .prologue
    .line 1861326
    sget-object v0, LX/31M;->RIGHT:LX/31M;

    return-object v0
.end method
