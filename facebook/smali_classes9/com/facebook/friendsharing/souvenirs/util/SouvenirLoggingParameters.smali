.class public Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:J

.field public h:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1732091
    new-instance v0, LX/Az3;

    invoke-direct {v0}, LX/Az3;-><init>()V

    sput-object v0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1732082
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1732083
    iput v0, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->a:I

    .line 1732084
    iput v0, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->b:I

    .line 1732085
    iput v0, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->c:I

    .line 1732086
    iput v0, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->d:I

    .line 1732087
    iput v0, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->e:I

    .line 1732088
    iput v0, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->f:I

    .line 1732089
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->g:J

    .line 1732090
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1732102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1732103
    iput v0, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->a:I

    .line 1732104
    iput v0, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->b:I

    .line 1732105
    iput v0, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->c:I

    .line 1732106
    iput v0, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->d:I

    .line 1732107
    iput v0, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->e:I

    .line 1732108
    iput v0, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->f:I

    .line 1732109
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->g:J

    .line 1732110
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->a:I

    .line 1732111
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->b:I

    .line 1732112
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->c:I

    .line 1732113
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->d:I

    .line 1732114
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->e:I

    .line 1732115
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->f:I

    .line 1732116
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->g:J

    .line 1732117
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->h:Ljava/lang/String;

    .line 1732118
    return-void
.end method

.method public static a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;LX/0SG;)Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;
    .locals 7

    .prologue
    .line 1732119
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1732120
    new-instance v2, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;

    invoke-direct {v2}, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;-><init>()V

    .line 1732121
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;->b()LX/0Px;

    move-result-object v3

    .line 1732122
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    iput v0, v2, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->d:I

    .line 1732123
    invoke-interface {p1}, LX/0SG;->a()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;->a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->c()J

    move-result-wide v4

    sub-long/2addr v0, v4

    const-wide/32 v4, 0xea60

    div-long/2addr v0, v4

    iput-wide v0, v2, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->g:J

    .line 1732124
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem;

    .line 1732125
    sget-object v5, LX/Az4;->a:[I

    invoke-interface {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem;->iq_()LX/Ay0;

    move-result-object v6

    invoke-virtual {v6}, LX/Ay0;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 1732126
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1732127
    :pswitch_0
    iget v0, v2, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v2, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->a:I

    .line 1732128
    iget v0, v2, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v2, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->e:I

    goto :goto_1

    .line 1732129
    :pswitch_1
    iget v0, v2, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v2, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->b:I

    .line 1732130
    iget v0, v2, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v2, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->e:I

    goto :goto_1

    .line 1732131
    :pswitch_2
    iget v5, v2, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->c:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v2, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->c:I

    .line 1732132
    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 1732133
    iget v5, v2, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->e:I

    add-int/2addr v5, v0

    iput v5, v2, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->e:I

    .line 1732134
    iget v5, v2, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->f:I

    add-int/2addr v0, v5

    iput v0, v2, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->f:I

    goto :goto_1

    .line 1732135
    :cond_0
    return-object v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1732101
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1732092
    iget v0, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1732093
    iget v0, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1732094
    iget v0, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1732095
    iget v0, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1732096
    iget v0, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1732097
    iget v0, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1732098
    iget-wide v0, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->g:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1732099
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1732100
    return-void
.end method
