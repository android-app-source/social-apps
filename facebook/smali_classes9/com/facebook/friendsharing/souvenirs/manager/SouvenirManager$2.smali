.class public final Lcom/facebook/friendsharing/souvenirs/manager/SouvenirManager$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/Axt;


# direct methods
.method public constructor <init>(LX/Axt;)V
    .locals 0

    .prologue
    .line 1728824
    iput-object p1, p0, Lcom/facebook/friendsharing/souvenirs/manager/SouvenirManager$2;->a:LX/Axt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1728825
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/manager/SouvenirManager$2;->a:LX/Axt;

    iget-object v0, v0, LX/Axt;->r:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1728826
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/manager/SouvenirManager$2;->a:LX/Axt;

    .line 1728827
    iget-object v1, v0, LX/Axt;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/AxW;

    iget-object v2, v0, LX/Axt;->n:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/AxX;

    .line 1728828
    iget-object v3, v2, LX/AxX;->a:Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifier$SouvenirClassifierWrapper;

    invoke-virtual {v3}, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifier$SouvenirClassifierWrapper;->currentlyUsedModelId()[B

    move-result-object v3

    .line 1728829
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v3}, Ljava/lang/String;-><init>([B)V

    move-object v2, v4

    .line 1728830
    new-instance v3, LX/AxL;

    invoke-direct {v3}, LX/AxL;-><init>()V

    move-object v3, v3

    .line 1728831
    const-string v4, "model_id"

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1728832
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    .line 1728833
    iget-object v4, v1, LX/AxW;->b:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 1728834
    new-instance v4, LX/AxV;

    invoke-direct {v4, v1, v2}, LX/AxV;-><init>(LX/AxW;Ljava/lang/String;)V

    .line 1728835
    sget-object p0, LX/131;->INSTANCE:LX/131;

    move-object p0, p0

    .line 1728836
    invoke-static {v3, v4, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v1, v3

    .line 1728837
    new-instance v2, LX/Axo;

    invoke-direct {v2, v0}, LX/Axo;-><init>(LX/Axt;)V

    invoke-static {v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1728838
    return-void
.end method
