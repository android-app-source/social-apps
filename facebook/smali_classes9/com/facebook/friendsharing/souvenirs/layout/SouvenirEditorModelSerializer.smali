.class public Lcom/facebook/friendsharing/souvenirs/layout/SouvenirEditorModelSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/friendsharing/souvenirs/layout/SouvenirEditorModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1728721
    const-class v0, Lcom/facebook/friendsharing/souvenirs/layout/SouvenirEditorModel;

    new-instance v1, Lcom/facebook/friendsharing/souvenirs/layout/SouvenirEditorModelSerializer;

    invoke-direct {v1}, Lcom/facebook/friendsharing/souvenirs/layout/SouvenirEditorModelSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1728722
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1728723
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friendsharing/souvenirs/layout/SouvenirEditorModel;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1728724
    if-nez p0, :cond_0

    .line 1728725
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1728726
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1728727
    invoke-static {p0, p1, p2}, Lcom/facebook/friendsharing/souvenirs/layout/SouvenirEditorModelSerializer;->b(Lcom/facebook/friendsharing/souvenirs/layout/SouvenirEditorModel;LX/0nX;LX/0my;)V

    .line 1728728
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1728729
    return-void
.end method

.method private static b(Lcom/facebook/friendsharing/souvenirs/layout/SouvenirEditorModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1728730
    const-string v0, "metadata"

    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/layout/SouvenirEditorModel;->mMetadata:Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1728731
    const-string v0, "rows"

    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/layout/SouvenirEditorModel;->mRows:LX/0Px;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1728732
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1728733
    check-cast p1, Lcom/facebook/friendsharing/souvenirs/layout/SouvenirEditorModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/friendsharing/souvenirs/layout/SouvenirEditorModelSerializer;->a(Lcom/facebook/friendsharing/souvenirs/layout/SouvenirEditorModel;LX/0nX;LX/0my;)V

    return-void
.end method
