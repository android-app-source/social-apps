.class public Lcom/facebook/friendsharing/souvenirs/layout/SouvenirRow;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/souvenirs/layout/SouvenirRowDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsharing/souvenirs/layout/SouvenirRowSerializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/layout/SouvenirRow;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final mItems:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "items"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem;",
            ">;"
        }
    .end annotation
.end field

.field public final mTemplate:Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "template"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1728741
    const-class v0, Lcom/facebook/friendsharing/souvenirs/layout/SouvenirRowDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1728742
    const-class v0, Lcom/facebook/friendsharing/souvenirs/layout/SouvenirRowSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1728743
    new-instance v0, LX/Axe;

    invoke-direct {v0}, LX/Axe;-><init>()V

    sput-object v0, Lcom/facebook/friendsharing/souvenirs/layout/SouvenirRow;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1728744
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1728745
    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/layout/SouvenirRow;->mTemplate:Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    .line 1728746
    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/layout/SouvenirRow;->mItems:LX/0Px;

    .line 1728747
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1728748
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1728749
    const-class v0, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/layout/SouvenirRow;->mTemplate:Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    .line 1728750
    invoke-static {p1}, LX/Ay2;->a(Landroid/os/Parcel;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/layout/SouvenirRow;->mItems:LX/0Px;

    .line 1728751
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1728752
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1728753
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/layout/SouvenirRow;->mTemplate:Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1728754
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/layout/SouvenirRow;->mItems:LX/0Px;

    invoke-static {v0, p1, p2}, LX/Ay2;->a(LX/0Px;Landroid/os/Parcel;I)V

    .line 1728755
    return-void
.end method
