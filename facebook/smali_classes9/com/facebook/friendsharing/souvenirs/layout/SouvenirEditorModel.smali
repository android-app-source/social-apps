.class public Lcom/facebook/friendsharing/souvenirs/layout/SouvenirEditorModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/souvenirs/layout/SouvenirEditorModelDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsharing/souvenirs/layout/SouvenirEditorModelSerializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/layout/SouvenirEditorModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final mMetadata:Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "metadata"
    .end annotation
.end field

.field public final mRows:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "rows"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/layout/SouvenirRow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1728684
    const-class v0, Lcom/facebook/friendsharing/souvenirs/layout/SouvenirEditorModelDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1728685
    const-class v0, Lcom/facebook/friendsharing/souvenirs/layout/SouvenirEditorModelSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1728686
    new-instance v0, LX/Axc;

    invoke-direct {v0}, LX/Axc;-><init>()V

    sput-object v0, Lcom/facebook/friendsharing/souvenirs/layout/SouvenirEditorModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1728687
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1728688
    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/layout/SouvenirEditorModel;->mMetadata:Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    .line 1728689
    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/layout/SouvenirEditorModel;->mRows:LX/0Px;

    .line 1728690
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1728691
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1728692
    const-class v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/layout/SouvenirEditorModel;->mMetadata:Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    .line 1728693
    sget-object v0, Lcom/facebook/friendsharing/souvenirs/layout/SouvenirRow;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/layout/SouvenirEditorModel;->mRows:LX/0Px;

    .line 1728694
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1728695
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1728696
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/layout/SouvenirEditorModel;->mMetadata:Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1728697
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/layout/SouvenirEditorModel;->mRows:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1728698
    return-void
.end method
