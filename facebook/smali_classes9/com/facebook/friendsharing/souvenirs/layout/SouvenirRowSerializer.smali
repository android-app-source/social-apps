.class public Lcom/facebook/friendsharing/souvenirs/layout/SouvenirRowSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/friendsharing/souvenirs/layout/SouvenirRow;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1728778
    const-class v0, Lcom/facebook/friendsharing/souvenirs/layout/SouvenirRow;

    new-instance v1, Lcom/facebook/friendsharing/souvenirs/layout/SouvenirRowSerializer;

    invoke-direct {v1}, Lcom/facebook/friendsharing/souvenirs/layout/SouvenirRowSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1728779
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1728780
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friendsharing/souvenirs/layout/SouvenirRow;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1728781
    if-nez p0, :cond_0

    .line 1728782
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1728783
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1728784
    invoke-static {p0, p1, p2}, Lcom/facebook/friendsharing/souvenirs/layout/SouvenirRowSerializer;->b(Lcom/facebook/friendsharing/souvenirs/layout/SouvenirRow;LX/0nX;LX/0my;)V

    .line 1728785
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1728786
    return-void
.end method

.method private static b(Lcom/facebook/friendsharing/souvenirs/layout/SouvenirRow;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1728787
    const-string v0, "template"

    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/layout/SouvenirRow;->mTemplate:Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1728788
    const-string v0, "items"

    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/layout/SouvenirRow;->mItems:LX/0Px;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1728789
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1728790
    check-cast p1, Lcom/facebook/friendsharing/souvenirs/layout/SouvenirRow;

    invoke-static {p1, p2, p3}, Lcom/facebook/friendsharing/souvenirs/layout/SouvenirRowSerializer;->a(Lcom/facebook/friendsharing/souvenirs/layout/SouvenirRow;LX/0nX;LX/0my;)V

    return-void
.end method
