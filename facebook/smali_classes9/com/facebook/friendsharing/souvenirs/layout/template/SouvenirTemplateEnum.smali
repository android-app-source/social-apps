.class public final enum Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum SINGLE_ITEM:Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

.field public static final enum THREE_SQUARE_ITEMS:Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

.field public static final enum TRIPLE_ITEMS_FIRST_PORTRAIT:Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

.field public static final enum TRIPLE_ITEMS_THIRD_PORTRAIT:Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

.field public static final enum TWO_SQUARE_ITEMS:Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;


# instance fields
.field private final mTemplate:LX/Axg;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1728801
    new-instance v0, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    const-string v1, "SINGLE_ITEM"

    new-instance v2, LX/Axi;

    invoke-direct {v2}, LX/Axi;-><init>()V

    invoke-direct {v0, v1, v3, v2}, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;-><init>(Ljava/lang/String;ILX/Axg;)V

    sput-object v0, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;->SINGLE_ITEM:Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    .line 1728802
    new-instance v0, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    const-string v1, "TWO_SQUARE_ITEMS"

    new-instance v2, LX/Axh;

    invoke-direct {v2, v5}, LX/Axh;-><init>(I)V

    invoke-direct {v0, v1, v4, v2}, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;-><init>(Ljava/lang/String;ILX/Axg;)V

    sput-object v0, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;->TWO_SQUARE_ITEMS:Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    .line 1728803
    new-instance v0, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    const-string v1, "THREE_SQUARE_ITEMS"

    new-instance v2, LX/Axh;

    invoke-direct {v2, v6}, LX/Axh;-><init>(I)V

    invoke-direct {v0, v1, v5, v2}, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;-><init>(Ljava/lang/String;ILX/Axg;)V

    sput-object v0, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;->THREE_SQUARE_ITEMS:Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    .line 1728804
    new-instance v0, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    const-string v1, "TRIPLE_ITEMS_FIRST_PORTRAIT"

    new-instance v2, LX/Axk;

    invoke-direct {v2, v4}, LX/Axk;-><init>(Z)V

    invoke-direct {v0, v1, v6, v2}, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;-><init>(Ljava/lang/String;ILX/Axg;)V

    sput-object v0, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;->TRIPLE_ITEMS_FIRST_PORTRAIT:Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    .line 1728805
    new-instance v0, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    const-string v1, "TRIPLE_ITEMS_THIRD_PORTRAIT"

    new-instance v2, LX/Axk;

    invoke-direct {v2, v3}, LX/Axk;-><init>(Z)V

    invoke-direct {v0, v1, v7, v2}, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;-><init>(Ljava/lang/String;ILX/Axg;)V

    sput-object v0, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;->TRIPLE_ITEMS_THIRD_PORTRAIT:Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    .line 1728806
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    sget-object v1, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;->SINGLE_ITEM:Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;->TWO_SQUARE_ITEMS:Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;->THREE_SQUARE_ITEMS:Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;->TRIPLE_ITEMS_FIRST_PORTRAIT:Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;->TRIPLE_ITEMS_THIRD_PORTRAIT:Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    aput-object v1, v0, v7

    sput-object v0, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;->$VALUES:[Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    .line 1728807
    new-instance v0, LX/Axj;

    invoke-direct {v0}, LX/Axj;-><init>()V

    sput-object v0, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILX/Axg;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Axg;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1728808
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1728809
    iput-object p3, p0, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;->mTemplate:LX/Axg;

    .line 1728810
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;
    .locals 1

    .prologue
    .line 1728811
    const-class v0, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    return-object v0
.end method

.method public static values()[Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;
    .locals 1

    .prologue
    .line 1728812
    sget-object v0, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;->$VALUES:[Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1728813
    const/4 v0, 0x0

    return v0
.end method

.method public final getTemplate()LX/Axg;
    .locals 1

    .prologue
    .line 1728814
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;->mTemplate:LX/Axg;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1728815
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1728816
    return-void
.end method
