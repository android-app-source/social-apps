.class public Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptAttachment;
.super Landroid/widget/LinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/AkM;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public c:Landroid/widget/TextView;

.field public d:Landroid/widget/TextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1729999
    const-class v0, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptAttachment;

    const-string v1, "souvenirs"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptAttachment;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1730000
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1730001
    const v0, 0x7f031390

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1730002
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptAttachment;->setOrientation(I)V

    .line 1730003
    const v0, 0x7f0d2d35

    invoke-virtual {p0, v0}, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptAttachment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptAttachment;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1730004
    const v0, 0x7f0d0b8d

    invoke-virtual {p0, v0}, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptAttachment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptAttachment;->c:Landroid/widget/TextView;

    .line 1730005
    const v0, 0x7f0d0b8e

    invoke-virtual {p0, v0}, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptAttachment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptAttachment;->d:Landroid/widget/TextView;

    .line 1730006
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 0

    .prologue
    .line 1730007
    return-object p0
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;Landroid/text/SpannableStringBuilder;)V
    .locals 2

    .prologue
    .line 1730008
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptAttachment;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptAttachment;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1730009
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptAttachment;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1730010
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptAttachment;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1730011
    return-void
.end method

.method public getCollapseAnimator()Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 1730012
    const/4 v0, 0x0

    return-object v0
.end method

.method public getExpandAnimator()Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 1730013
    const/4 v0, 0x0

    return-object v0
.end method
