.class public Lcom/facebook/friendsharing/souvenirs/prompt/composer/SouvenirPromptPluginConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/88f;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/souvenirs/prompt/composer/SouvenirPromptPluginConfigDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsharing/souvenirs/prompt/composer/SouvenirPromptPluginConfigSerializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final promptAnalytics:Lcom/facebook/productionprompts/logging/PromptAnalytics;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "prompt_entry_point_analytics"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1730157
    const-class v0, Lcom/facebook/friendsharing/souvenirs/prompt/composer/SouvenirPromptPluginConfigDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1730145
    const-class v0, Lcom/facebook/friendsharing/souvenirs/prompt/composer/SouvenirPromptPluginConfigSerializer;

    return-object v0
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1730146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1730147
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/prompt/composer/SouvenirPromptPluginConfig;->promptAnalytics:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 1730148
    return-void
.end method

.method private constructor <init>(Lcom/facebook/productionprompts/logging/PromptAnalytics;)V
    .locals 0

    .prologue
    .line 1730149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1730150
    iput-object p1, p0, Lcom/facebook/friendsharing/souvenirs/prompt/composer/SouvenirPromptPluginConfig;->promptAnalytics:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 1730151
    return-void
.end method

.method public static a(Lcom/facebook/productionprompts/logging/PromptAnalytics;)Lcom/facebook/friendsharing/souvenirs/prompt/composer/SouvenirPromptPluginConfig;
    .locals 1

    .prologue
    .line 1730152
    new-instance v0, Lcom/facebook/friendsharing/souvenirs/prompt/composer/SouvenirPromptPluginConfig;

    invoke-direct {v0, p0}, Lcom/facebook/friendsharing/souvenirs/prompt/composer/SouvenirPromptPluginConfig;-><init>(Lcom/facebook/productionprompts/logging/PromptAnalytics;)V

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1730153
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/prompt/composer/SouvenirPromptPluginConfig;->promptAnalytics:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1730154
    return-void

    .line 1730155
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1730156
    const-string v0, "SOUVENIR_PROMPT_PERSISTENCE_KEY"

    return-object v0
.end method
