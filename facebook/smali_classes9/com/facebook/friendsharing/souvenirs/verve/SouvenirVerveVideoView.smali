.class public Lcom/facebook/friendsharing/souvenirs/verve/SouvenirVerveVideoView;
.super Lcom/facebook/greetingcards/vervecontrols/InlineVerveVideoView;
.source ""

# interfaces
.implements LX/CEY;


# static fields
.field private static final m:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private n:LX/2pa;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1861673
    const-class v0, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirVerveVideoView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirVerveVideoView;->m:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1861674
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirVerveVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1861675
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1861676
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirVerveVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1861677
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1861678
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/greetingcards/vervecontrols/InlineVerveVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1861679
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "LX/0Px",
            "<+",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1861680
    new-instance v0, Lcom/facebook/video/player/plugins/CoverImagePlugin;

    sget-object v1, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirVerveVideoView;->m:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v0, p1, v1}, Lcom/facebook/video/player/plugins/CoverImagePlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    new-instance v1, LX/2pM;

    invoke-direct {v1, p1}, LX/2pM;-><init>(Landroid/content/Context;)V

    new-instance v2, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-direct {v2, p1}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1861681
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirVerveVideoView;->n:LX/2pa;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pa;

    invoke-super {p0, v0}, Lcom/facebook/greetingcards/vervecontrols/InlineVerveVideoView;->c(LX/2pa;)V

    .line 1861682
    return-void
.end method

.method public final c(LX/2pa;)V
    .locals 1

    .prologue
    .line 1861683
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1861684
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_0

    .line 1861685
    iput-object p1, p0, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirVerveVideoView;->n:LX/2pa;

    .line 1861686
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/greetingcards/vervecontrols/InlineVerveVideoView;->c(LX/2pa;)V

    .line 1861687
    return-void
.end method

.method public setVideoListener(LX/3It;)V
    .locals 0

    .prologue
    .line 1861688
    iput-object p1, p0, Lcom/facebook/video/player/RichVideoPlayer;->F:LX/3It;

    .line 1861689
    return-void
.end method
