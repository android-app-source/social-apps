.class public Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;
.super LX/CEm;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/23R;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/AzA;

.field public final d:Lcom/facebook/content/SecureContextHelper;

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPhoto;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsInterfaces$SouvenirsMediaElementFields;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/model/GraphQLVideo;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Landroid/graphics/drawable/Drawable;

.field public final j:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final k:Landroid/view/View$OnClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1861914
    const-class v0, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;

    const-string v1, "souvenirs"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/AzA;Landroid/content/res/Resources;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 7
    .param p5    # Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/23R;",
            ">;",
            "LX/AzA;",
            "Landroid/content/res/Resources;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsInterfaces$SouvenirsDetailsFields;",
            "Lcom/facebook/feed/rows/core/props/FeedProps;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1861838
    invoke-direct {p0}, LX/CEm;-><init>()V

    .line 1861839
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;->f:Ljava/util/HashMap;

    .line 1861840
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;->g:Ljava/util/HashMap;

    .line 1861841
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;->h:Ljava/util/HashMap;

    .line 1861842
    new-instance v0, LX/CEh;

    invoke-direct {v0, p0}, LX/CEh;-><init>(Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;)V

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;->k:Landroid/view/View$OnClickListener;

    .line 1861843
    iput-object p1, p0, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;->b:LX/0Or;

    .line 1861844
    iput-object p2, p0, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;->c:LX/AzA;

    .line 1861845
    iput-object p4, p0, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;->d:Lcom/facebook/content/SecureContextHelper;

    .line 1861846
    invoke-virtual {p5}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;->b()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel$EdgesModel;

    .line 1861847
    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel$EdgesModel;->a()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;

    move-result-object v0

    .line 1861848
    invoke-static {v0}, LX/AzA;->a(Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;)Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;

    move-result-object v4

    .line 1861849
    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    move-result-object v5

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->BURST:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    if-ne v5, v6, :cond_1

    .line 1861850
    iget-object v4, p0, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1861851
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1861852
    :cond_1
    if-eqz v4, :cond_3

    .line 1861853
    const/4 v0, 0x0

    .line 1861854
    if-nez v4, :cond_5

    .line 1861855
    :cond_2
    :goto_2
    move-object v0, v0

    .line 1861856
    if-eqz v0, :cond_3

    .line 1861857
    iget-object v4, p0, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->az()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1861858
    :cond_3
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v4}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v5, 0x4984e12

    if-ne v0, v5, :cond_0

    .line 1861859
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;->g:Ljava/util/HashMap;

    invoke-virtual {v4}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->j()LX/1Fb;

    move-result-object v5

    invoke-interface {v5}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v5, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1861860
    :cond_4
    iput-object p6, p0, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;->j:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1861861
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 1861862
    if-nez p5, :cond_6

    .line 1861863
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1861864
    :goto_3
    move-object v0, v1

    .line 1861865
    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;->e:LX/0Px;

    .line 1861866
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const v1, 0x7f0a0765

    invoke-virtual {p3, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;->i:Landroid/graphics/drawable/Drawable;

    .line 1861867
    return-void

    .line 1861868
    :cond_5
    invoke-virtual {v4}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    .line 1861869
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v5

    const v6, 0x4ed245b

    if-ne v5, v6, :cond_2

    .line 1861870
    new-instance v0, LX/2oI;

    invoke-direct {v0}, LX/2oI;-><init>()V

    .line 1861871
    invoke-virtual {v4}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->k()Z

    move-result v5

    .line 1861872
    iput-boolean v5, v0, LX/2oI;->m:Z

    .line 1861873
    invoke-virtual {v4}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->l()Z

    move-result v5

    .line 1861874
    iput-boolean v5, v0, LX/2oI;->n:Z

    .line 1861875
    invoke-virtual {v4}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->m()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel$CreationStoryModel;

    move-result-object v5

    invoke-static {v5}, LX/Az0;->a(Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel$CreationStoryModel;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    .line 1861876
    iput-object v5, v0, LX/2oI;->u:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1861877
    invoke-virtual {v4}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->n()I

    move-result v5

    .line 1861878
    iput v5, v0, LX/2oI;->M:I

    .line 1861879
    invoke-virtual {v4}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->d()Ljava/lang/String;

    move-result-object v5

    .line 1861880
    iput-object v5, v0, LX/2oI;->N:Ljava/lang/String;

    .line 1861881
    invoke-virtual {v4}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->e()LX/1Fb;

    move-result-object v5

    invoke-static {v5}, LX/Az0;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    .line 1861882
    iput-object v5, v0, LX/2oI;->O:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1861883
    invoke-virtual {v4}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->aj_()LX/1Fb;

    move-result-object v5

    invoke-static {v5}, LX/Az0;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    .line 1861884
    iput-object v5, v0, LX/2oI;->P:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1861885
    invoke-virtual {v4}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->ai_()LX/1Fb;

    move-result-object v5

    invoke-static {v5}, LX/Az0;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    .line 1861886
    iput-object v5, v0, LX/2oI;->T:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1861887
    invoke-virtual {v4}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->j()LX/1Fb;

    move-result-object v5

    invoke-static {v5}, LX/Az0;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    .line 1861888
    iput-object v5, v0, LX/2oI;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1861889
    invoke-virtual {v4}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->o()Z

    move-result v5

    .line 1861890
    iput-boolean v5, v0, LX/2oI;->am:Z

    .line 1861891
    invoke-virtual {v4}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->p()I

    move-result v5

    .line 1861892
    iput v5, v0, LX/2oI;->aS:I

    .line 1861893
    invoke-virtual {v4}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->q()Ljava/lang/String;

    move-result-object v5

    .line 1861894
    iput-object v5, v0, LX/2oI;->aT:Ljava/lang/String;

    .line 1861895
    invoke-virtual {v4}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->r()I

    move-result v5

    .line 1861896
    iput v5, v0, LX/2oI;->cd:I

    .line 1861897
    invoke-virtual {v0}, LX/2oI;->a()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    goto/16 :goto_2

    .line 1861898
    :cond_6
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 1861899
    invoke-virtual {p5}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;->b()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel;->a()LX/0Px;

    move-result-object p1

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result p2

    move v5, v4

    :goto_4
    if-ge v5, p2, :cond_b

    invoke-virtual {p1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel$EdgesModel;

    .line 1861900
    invoke-virtual {v1}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel$EdgesModel;->a()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;

    move-result-object p4

    .line 1861901
    invoke-virtual {p4}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->c()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;

    move-result-object v1

    if-eqz v1, :cond_a

    invoke-virtual {p4}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->c()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_a

    invoke-virtual {p4}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->c()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_a

    .line 1861902
    invoke-virtual {p4}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->VIDEO:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    if-eq v1, v2, :cond_a

    .line 1861903
    invoke-virtual {p4}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    if-eq v1, v2, :cond_7

    if-nez v0, :cond_9

    :cond_7
    const/4 v1, 0x1

    move v2, v1

    :goto_5
    move v3, v4

    .line 1861904
    :goto_6
    if-ge v3, v2, :cond_a

    .line 1861905
    invoke-virtual {p4}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->c()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;

    invoke-virtual {v1}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;->a()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;

    move-result-object v1

    .line 1861906
    if-eqz v1, :cond_8

    .line 1861907
    invoke-static {v1}, LX/Az0;->a(Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;)Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v1

    .line 1861908
    if-eqz v1, :cond_8

    .line 1861909
    invoke-virtual {v6, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1861910
    :cond_8
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_6

    .line 1861911
    :cond_9
    invoke-virtual {p4}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->c()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    move v2, v1

    goto :goto_5

    .line 1861912
    :cond_a
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_4

    .line 1861913
    :cond_b
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    goto/16 :goto_3
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum;",
            ":",
            "LX/CEj;",
            ">()",
            "LX/0Rf",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1861837
    invoke-static {}, LX/CEk;->values()[LX/CEk;

    move-result-object v0

    check-cast v0, [Ljava/lang/Enum;

    invoke-static {v0}, LX/0Rf;->copyOf([Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/greetingcards/verve/model/VMView;)Landroid/widget/ImageView;
    .locals 2

    .prologue
    .line 1861834
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;->f:Ljava/util/HashMap;

    iget-object v1, p2, Lcom/facebook/greetingcards/verve/model/VMView;->src:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1861835
    new-instance v0, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;

    invoke-direct {v0, p1}, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;-><init>(Landroid/content/Context;)V

    .line 1861836
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/CEc;

    invoke-direct {v0, p1}, LX/CEc;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/greetingcards/verve/model/VMView;)Ljava/lang/Enum;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum;",
            ":",
            "LX/CEj;",
            ">(",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            ")TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1861830
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->src:Ljava/lang/String;

    .line 1861831
    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;->f:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1861832
    sget-object v0, LX/CEk;->BURST_VIEW:LX/CEk;

    .line 1861833
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, LX/CEm;->a(Lcom/facebook/greetingcards/verve/model/VMView;)Ljava/lang/Enum;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/greetingcards/verve/model/VMView;LX/CEe;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            "LX/CEe;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 1861759
    check-cast p2, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirVerveVideoView;

    .line 1861760
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMView;->src:Ljava/lang/String;

    .line 1861761
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;->h:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1861762
    :cond_0
    :goto_0
    return-void

    .line 1861763
    :cond_1
    iget-object v2, p0, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;->h:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideo;

    .line 1861764
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->N()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-static {v2}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v2

    .line 1861765
    invoke-static {}, Lcom/facebook/video/engine/VideoDataSource;->newBuilder()LX/2oE;

    move-result-object v3

    sget-object v4, LX/097;->FROM_STREAM:LX/097;

    .line 1861766
    iput-object v4, v3, LX/2oE;->e:LX/097;

    .line 1861767
    move-object v3, v3

    .line 1861768
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->az()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 1861769
    iput-object v4, v3, LX/2oE;->a:Landroid/net/Uri;

    .line 1861770
    move-object v3, v3

    .line 1861771
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->aw()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 1861772
    iput-object v4, v3, LX/2oE;->b:Landroid/net/Uri;

    .line 1861773
    move-object v3, v3

    .line 1861774
    invoke-virtual {v3}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v3

    .line 1861775
    invoke-static {}, Lcom/facebook/video/engine/VideoPlayerParams;->newBuilder()LX/2oH;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/2oH;->a(Lcom/facebook/video/engine/VideoDataSource;)LX/2oH;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v4

    .line 1861776
    iput-object v4, v3, LX/2oH;->b:Ljava/lang/String;

    .line 1861777
    move-object v3, v3

    .line 1861778
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->ay()I

    move-result v4

    .line 1861779
    iput v4, v3, LX/2oH;->c:I

    .line 1861780
    move-object v3, v3

    .line 1861781
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->q()I

    move-result v4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->D()I

    move-result v5

    invoke-virtual {v3, v4, v5}, LX/2oH;->a(II)LX/2oH;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;->h:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    if-ne v4, v1, :cond_3

    .line 1861782
    :goto_1
    iput-boolean v1, v3, LX/2oH;->g:Z

    .line 1861783
    move-object v1, v3

    .line 1861784
    invoke-virtual {v1}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v1

    .line 1861785
    invoke-virtual {p1}, Lcom/facebook/greetingcards/verve/model/VMView;->h()F

    move-result v3

    invoke-virtual {p1}, Lcom/facebook/greetingcards/verve/model/VMView;->i()F

    move-result v4

    div-float/2addr v3, v4

    float-to-double v4, v3

    .line 1861786
    new-instance v3, LX/0P2;

    invoke-direct {v3}, LX/0P2;-><init>()V

    const-string v6, "CoverImageParamsKey"

    invoke-virtual {v3, v6, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v2

    .line 1861787
    iget-object v7, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v8, p2, Lcom/facebook/greetingcards/vervecontrols/InlineVerveVideoView;->n:Ljava/lang/String;

    invoke-static {v7, v8}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1861788
    invoke-virtual {p2}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 1861789
    iget-object v7, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iput-object v7, p2, Lcom/facebook/greetingcards/vervecontrols/InlineVerveVideoView;->n:Ljava/lang/String;

    .line 1861790
    :cond_2
    new-instance v7, LX/2pZ;

    invoke-direct {v7}, LX/2pZ;-><init>()V

    .line 1861791
    iput-object v1, v7, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1861792
    move-object v7, v7

    .line 1861793
    iput-wide v4, v7, LX/2pZ;->e:D

    .line 1861794
    move-object v7, v7

    .line 1861795
    invoke-virtual {v7, v2}, LX/2pZ;->a(LX/0P1;)LX/2pZ;

    move-result-object v7

    invoke-virtual {v7}, LX/2pZ;->b()LX/2pa;

    move-result-object v7

    .line 1861796
    invoke-virtual {p2, v7}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 1861797
    new-instance v1, LX/CEi;

    invoke-direct {v1, p0, p2, v0}, LX/CEi;-><init>(Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;Lcom/facebook/friendsharing/souvenirs/verve/SouvenirVerveVideoView;Lcom/facebook/graphql/model/GraphQLVideo;)V

    invoke-virtual {p2, v1}, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirVerveVideoView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 1861798
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/greetingcards/verve/model/VMView;Lcom/facebook/greetingcards/verve/model/VMSlide;LX/0P1;Landroid/graphics/drawable/Drawable;Landroid/widget/ImageView;)V
    .locals 5
    .param p4    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            "Lcom/facebook/greetingcards/verve/model/VMSlide;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Landroid/graphics/drawable/Drawable;",
            "Landroid/widget/ImageView;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1861801
    invoke-virtual {p5, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1861802
    invoke-virtual {p5, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1861803
    iget-object v1, p1, Lcom/facebook/greetingcards/verve/model/VMView;->src:Ljava/lang/String;

    .line 1861804
    if-nez v1, :cond_0

    .line 1861805
    :goto_0
    return-void

    .line 1861806
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;->f:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;->f:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->BURST:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    if-ne v0, v2, :cond_1

    .line 1861807
    check-cast p5, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;

    .line 1861808
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;->f:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;

    invoke-virtual {p5, v0}, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->a(Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;)V

    .line 1861809
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;->k:Landroid/view/View$OnClickListener;

    invoke-virtual {p5, v0}, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_1
    move-object v0, p5

    .line 1861810
    check-cast v0, LX/CEc;

    .line 1861811
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1861812
    iget-object v1, p1, Lcom/facebook/greetingcards/verve/model/VMView;->src:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1861813
    :cond_2
    invoke-virtual {v0, v3}, LX/CEc;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1861814
    new-instance v1, LX/1Uo;

    invoke-virtual {p5}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    sget-object v2, LX/1Up;->h:LX/1Up;

    invoke-virtual {v1, v2}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v1

    .line 1861815
    iget-object v2, p0, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;->i:Landroid/graphics/drawable/Drawable;

    .line 1861816
    iput-object v2, v1, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 1861817
    invoke-virtual {v0}, LX/CEc;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 1861818
    iget-object v3, p2, Lcom/facebook/greetingcards/verve/model/VMSlide;->name:Ljava/lang/String;

    const-string v4, "Cover_0"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1861819
    new-instance v3, LX/4Ab;

    invoke-direct {v3}, LX/4Ab;-><init>()V

    const/4 v4, 0x1

    .line 1861820
    iput-boolean v4, v3, LX/4Ab;->b:Z

    .line 1861821
    move-object v3, v3

    .line 1861822
    iput-object v3, v1, LX/1Uo;->u:LX/4Ab;

    .line 1861823
    const v3, 0x7f080f63

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/CEc;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1861824
    :goto_1
    invoke-virtual {v1}, LX/1Uo;->u()LX/1af;

    move-result-object v1

    .line 1861825
    iget-object v2, p1, Lcom/facebook/greetingcards/verve/model/VMView;->b:Landroid/graphics/PointF;

    if-eqz v2, :cond_3

    .line 1861826
    iget-object v2, p1, Lcom/facebook/greetingcards/verve/model/VMView;->b:Landroid/graphics/PointF;

    invoke-virtual {v1, v2}, LX/1af;->a(Landroid/graphics/PointF;)V

    .line 1861827
    :cond_3
    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    goto/16 :goto_0

    .line 1861828
    :cond_4
    iget-object v3, p0, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;->k:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, LX/CEc;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1861829
    const v3, 0x7f080072

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/CEc;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public final b(Landroid/content/Context;Lcom/facebook/greetingcards/verve/model/VMView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1861800
    new-instance v0, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirVerveVideoView;

    invoke-direct {v0, p1}, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirVerveVideoView;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final c(Landroid/content/Context;Lcom/facebook/greetingcards/verve/model/VMView;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 1861799
    const/4 v0, 0x0

    return-object v0
.end method
