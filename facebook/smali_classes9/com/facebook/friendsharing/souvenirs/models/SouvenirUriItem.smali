.class public abstract Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem;
.implements LX/4gK;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1729529
    const-class v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItemDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1729518
    const-class v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItemSerializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1729527
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1729528
    return-void
.end method

.method public constructor <init>(B)V
    .locals 0

    .prologue
    .line 1729525
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1729526
    return-void
.end method


# virtual methods
.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1729522
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 1729523
    iget-object p0, v0, Lcom/facebook/ipc/media/data/MediaData;->mId:Ljava/lang/String;

    move-object v0, p0

    .line 1729524
    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1729521
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1729520
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;->iq_()LX/Ay0;

    move-result-object v0

    invoke-virtual {v0}, LX/Ay0;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 1729519
    return-void
.end method
