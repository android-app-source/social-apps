.class public final Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItem;
.super Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;
.source ""

# interfaces
.implements LX/4gK;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
    baseDeserializer = "com.facebook.friendsharing.souvenirs.models.SouvenirUriItemDeserializer"
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItem$Serializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final mMediaData:Lcom/facebook/ipc/media/data/MediaData;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        a = true
        value = "media_data"
    .end annotation
.end field

.field public final mMediaModel:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        a = true
        value = "model"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1729624
    const-class v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItemDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1729623
    const-class v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItem$Serializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1729622
    new-instance v0, LX/Ay8;

    invoke-direct {v0}, LX/Ay8;-><init>()V

    sput-object v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1729618
    invoke-direct {p0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;-><init>()V

    .line 1729619
    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItem;->mMediaData:Lcom/facebook/ipc/media/data/MediaData;

    .line 1729620
    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItem;->mMediaModel:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;

    .line 1729621
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1729614
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;-><init>(B)V

    .line 1729615
    const-class v0, Lcom/facebook/ipc/media/data/MediaData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/data/MediaData;

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItem;->mMediaData:Lcom/facebook/ipc/media/data/MediaData;

    .line 1729616
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItem;->mMediaModel:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;

    .line 1729617
    return-void
.end method


# virtual methods
.method public final b()Lcom/facebook/ipc/media/data/MediaData;
    .locals 1

    .prologue
    .line 1729625
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItem;->mMediaData:Lcom/facebook/ipc/media/data/MediaData;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    .line 1729612
    if-ne p1, p0, :cond_1

    .line 1729613
    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v1, p1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItem;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItem;->mMediaModel:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;

    check-cast p1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItem;

    iget-object v2, p1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItem;->mMediaModel:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1729609
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, 0x55

    .line 1729610
    mul-int/lit8 v0, v0, 0x11

    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItem;->iq_()LX/Ay0;

    move-result-object v1

    invoke-virtual {v1}, LX/Ay0;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1729611
    return v0
.end method

.method public final iq_()LX/Ay0;
    .locals 2

    .prologue
    .line 1729606
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 1729607
    iget-object v1, v0, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    move-object v0, v1

    .line 1729608
    sget-object v1, LX/4gQ;->Photo:LX/4gQ;

    if-ne v0, v1, :cond_0

    sget-object v0, LX/Ay0;->Photo:LX/Ay0;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/Ay0;->Video:LX/Ay0;

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 1729605
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "{SouvenirRemoteItem %s}"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/media/data/MediaData;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1729601
    invoke-super {p0, p1, p2}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1729602
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItem;->mMediaData:Lcom/facebook/ipc/media/data/MediaData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1729603
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItem;->mMediaModel:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1729604
    return-void
.end method
