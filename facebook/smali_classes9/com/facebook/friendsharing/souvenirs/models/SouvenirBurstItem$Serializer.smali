.class public final Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1729184
    const-class v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;

    new-instance v1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem$Serializer;

    invoke-direct {v1}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1729185
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1729183
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1729186
    if-nez p0, :cond_0

    .line 1729187
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SouvenirBurstItem.serialize"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1729188
    :cond_0
    const-class v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1729189
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1729190
    invoke-static {p0, p1, p2}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem$Serializer;->b(Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;LX/0nX;LX/0my;)V

    .line 1729191
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1729192
    return-void
.end method

.method private static b(Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1729180
    const-string v0, "photo_items"

    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;->mItems:LX/0Px;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1729181
    const-string v0, "selected_index"

    iget v1, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;->mSelectedIndex:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1729182
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1729179
    check-cast p1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;

    invoke-static {p1, p2, p3}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem$Serializer;->a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;LX/0nX;LX/0my;)V

    return-void
.end method
