.class public Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModel;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModelDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field public final assetCount:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "assetCount"
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1729259
    const-class v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModelDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1729254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1729255
    iput-object v1, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModel;->name:Ljava/lang/String;

    .line 1729256
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModel;->assetCount:I

    .line 1729257
    iput-object v1, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModel;->a:Ljava/lang/String;

    .line 1729258
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 1729260
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1729261
    iput-object p1, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModel;->name:Ljava/lang/String;

    .line 1729262
    iput p2, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModel;->assetCount:I

    .line 1729263
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1729251
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModel;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1729252
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModel;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModel;->assetCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModel;->a:Ljava/lang/String;

    .line 1729253
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModel;->a:Ljava/lang/String;

    return-object v0
.end method
