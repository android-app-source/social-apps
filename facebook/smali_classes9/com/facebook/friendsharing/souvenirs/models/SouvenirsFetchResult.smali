.class public Lcom/facebook/friendsharing/souvenirs/models/SouvenirsFetchResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirsFetchResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1729744
    new-instance v0, LX/AyA;

    invoke-direct {v0}, LX/AyA;-><init>()V

    sput-object v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirsFetchResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1729740
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1729741
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirsFetchResult;->a:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;

    .line 1729742
    const-class v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirsFetchResult;->b:LX/0Px;

    .line 1729743
    return-void
.end method

.method public constructor <init>(Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1729736
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1729737
    iput-object p1, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirsFetchResult;->a:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;

    .line 1729738
    invoke-static {p2}, LX/17G;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirsFetchResult;->b:LX/0Px;

    .line 1729739
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1729732
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1729733
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirsFetchResult;->a:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1729734
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirsFetchResult;->b:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1729735
    return-void
.end method
