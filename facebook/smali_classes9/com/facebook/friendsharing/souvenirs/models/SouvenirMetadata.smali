.class public Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadataDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadataSerializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final mClassifierScore:D
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "classifier_score"
    .end annotation
.end field

.field public final mEndDate:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "end_date"
    .end annotation
.end field

.field public final mId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "local_id"
    .end annotation
.end field

.field public final mLoggingData:Ljava/util/Map;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "logging_data"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final mShareabilityScore:F
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "shareability_score"
    .end annotation
.end field

.field public final mStartDate:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "start_date"
    .end annotation
.end field

.field public final mTitle:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "title"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1729386
    const-class v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadataDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1729335
    const-class v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadataSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1729385
    new-instance v0, LX/Ay3;

    invoke-direct {v0}, LX/Ay3;-><init>()V

    sput-object v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const-wide/16 v0, -0x1

    const/4 v2, 0x0

    .line 1729376
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1729377
    iput-object v2, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mId:Ljava/lang/String;

    .line 1729378
    iput-object v2, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mTitle:Ljava/lang/String;

    .line 1729379
    iput-wide v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mStartDate:J

    .line 1729380
    iput-wide v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mEndDate:J

    .line 1729381
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mShareabilityScore:F

    .line 1729382
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mClassifierScore:D

    .line 1729383
    iput-object v2, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mLoggingData:Ljava/util/Map;

    .line 1729384
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 5

    .prologue
    .line 1729363
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1729364
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mId:Ljava/lang/String;

    .line 1729365
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mTitle:Ljava/lang/String;

    .line 1729366
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mStartDate:J

    .line 1729367
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mEndDate:J

    .line 1729368
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mShareabilityScore:F

    .line 1729369
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mClassifierScore:D

    .line 1729370
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1729371
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mLoggingData:Ljava/util/Map;

    .line 1729372
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1729373
    iget-object v2, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mLoggingData:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1729374
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1729375
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;JJFDLjava/util/Map;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "JJFD",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1729354
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1729355
    iput-object p1, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mId:Ljava/lang/String;

    .line 1729356
    iput-object p2, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mTitle:Ljava/lang/String;

    .line 1729357
    iput-wide p3, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mStartDate:J

    .line 1729358
    iput-wide p5, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mEndDate:J

    .line 1729359
    iput p7, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mShareabilityScore:F

    .line 1729360
    iput-wide p8, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mClassifierScore:D

    .line 1729361
    iput-object p10, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mLoggingData:Ljava/util/Map;

    .line 1729362
    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;JJFDLjava/util/Map;B)V
    .locals 1

    .prologue
    .line 1729353
    invoke-direct/range {p0 .. p10}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;-><init>(Ljava/lang/String;Ljava/lang/String;JJFDLjava/util/Map;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1729387
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 1729352
    iget-wide v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mStartDate:J

    return-wide v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 1729351
    iget-wide v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mEndDate:J

    return-wide v0
.end method

.method public final d()D
    .locals 2

    .prologue
    .line 1729350
    iget-wide v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mClassifierScore:D

    return-wide v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1729349
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1729348
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mLoggingData:Ljava/util/Map;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 1729347
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "Souvenir(%s)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mId:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 1729336
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1729337
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1729338
    iget-wide v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mStartDate:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1729339
    iget-wide v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mEndDate:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1729340
    iget v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mShareabilityScore:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1729341
    iget-wide v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mClassifierScore:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 1729342
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mLoggingData:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1729343
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mLoggingData:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1729344
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1729345
    iget-object v2, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mLoggingData:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 1729346
    :cond_0
    return-void
.end method
