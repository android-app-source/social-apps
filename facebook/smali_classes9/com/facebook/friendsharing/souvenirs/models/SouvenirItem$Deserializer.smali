.class public final Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem$Deserializer;
.super Lcom/fasterxml/jackson/databind/JsonDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonDeserializer",
        "<",
        "Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/common/json/FbJsonDeserializer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1729286
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    .line 1729287
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem$Deserializer;->a:Ljava/util/HashMap;

    return-void
.end method

.method private a(LX/15w;LX/0n3;)Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem;
    .locals 4

    .prologue
    .line 1729288
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    .line 1729289
    invoke-direct {p0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem$Deserializer;->a()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/json/FbJsonDeserializer;

    .line 1729290
    if-nez v0, :cond_0

    .line 1729291
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SouvenirItem deserialize: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1729292
    :cond_0
    invoke-virtual {v0, p1, p2}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem;

    return-object v0
.end method

.method private a()Ljava/util/HashMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/common/json/FbJsonDeserializer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1729293
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem$Deserializer;->a:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 1729294
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem$Deserializer;->a:Ljava/util/HashMap;

    .line 1729295
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem$Deserializer;->a:Ljava/util/HashMap;

    const-class v1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItemDeserializer;

    invoke-direct {v2}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItemDeserializer;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1729296
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem$Deserializer;->a:Ljava/util/HashMap;

    const-class v1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItemDeserializer;

    invoke-direct {v2}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItemDeserializer;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1729297
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem$Deserializer;->a:Ljava/util/HashMap;

    const-class v1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItemDeserializer;

    invoke-direct {v2}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItemDeserializer;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1729298
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem$Deserializer;->a:Ljava/util/HashMap;

    const-class v1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItem;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItemDeserializer;

    invoke-direct {v2}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItemDeserializer;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1729299
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem$Deserializer;->a:Ljava/util/HashMap;

    return-object v0
.end method


# virtual methods
.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1729300
    invoke-direct {p0, p1, p2}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem$Deserializer;->a(LX/15w;LX/0n3;)Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem;

    move-result-object v0

    return-object v0
.end method
