.class public Lcom/facebook/friendsharing/souvenirs/models/SouvenirModelSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1729494
    const-class v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    new-instance v1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModelSerializer;

    invoke-direct {v1}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModelSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1729495
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1729493
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1729496
    if-nez p0, :cond_0

    .line 1729497
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1729498
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1729499
    invoke-static {p0, p1, p2}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModelSerializer;->b(Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;LX/0nX;LX/0my;)V

    .line 1729500
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1729501
    return-void
.end method

.method private static b(Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1729490
    const-string v0, "metadata"

    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;->mMetadata:Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1729491
    const-string v0, "items"

    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;->mItems:LX/0Px;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1729492
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1729489
    check-cast p1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModelSerializer;->a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;LX/0nX;LX/0my;)V

    return-void
.end method
