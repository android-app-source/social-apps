.class public final Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItem$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItem;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1729587
    const-class v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItem;

    new-instance v1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItem$Serializer;

    invoke-direct {v1}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItem$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1729588
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1729589
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItem;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1729590
    if-nez p0, :cond_0

    .line 1729591
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SouvenirPhotoItem.serialize"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1729592
    :cond_0
    const-class v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1729593
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1729594
    invoke-static {p0, p1, p2}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItem$Serializer;->b(Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItem;LX/0nX;LX/0my;)V

    .line 1729595
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1729596
    return-void
.end method

.method private static b(Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItem;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1729597
    const-string v0, "media_data"

    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItem;->mMediaData:Lcom/facebook/ipc/media/data/MediaData;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1729598
    const-string v0, "model"

    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItem;->mMediaModel:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1729599
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1729600
    check-cast p1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItem;

    invoke-static {p1, p2, p3}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItem$Serializer;->a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirRemoteItem;LX/0nX;LX/0my;)V

    return-void
.end method
