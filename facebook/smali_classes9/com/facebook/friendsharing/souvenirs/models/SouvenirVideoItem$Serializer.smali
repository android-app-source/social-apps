.class public final Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1729660
    const-class v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem;

    new-instance v1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem$Serializer;

    invoke-direct {v1}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1729661
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1729662
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1729663
    if-nez p0, :cond_0

    .line 1729664
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SouvenirVideoItem.serialize"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1729665
    :cond_0
    const-class v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1729666
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1729667
    invoke-static {p0, p1, p2}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem$Serializer;->b(Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem;LX/0nX;LX/0my;)V

    .line 1729668
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1729669
    return-void
.end method

.method private static b(Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1729670
    const-string v0, "media_data"

    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem;->mLocalMediaData:Lcom/facebook/ipc/media/data/LocalMediaData;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1729671
    const-string v0, "duration"

    iget-wide v2, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem;->mDurationMsecs:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1729672
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1729673
    check-cast p1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem;

    invoke-static {p1, p2, p3}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem$Serializer;->a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem;LX/0nX;LX/0my;)V

    return-void
.end method
