.class public final Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1729505
    const-class v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;

    new-instance v1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem$Serializer;

    invoke-direct {v1}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1729506
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1729507
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1729508
    if-nez p0, :cond_0

    .line 1729509
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SouvenirPhotoItem.serialize"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1729510
    :cond_0
    const-class v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1729511
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1729512
    invoke-static {p0, p1, p2}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem$Serializer;->b(Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;LX/0nX;LX/0my;)V

    .line 1729513
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1729514
    return-void
.end method

.method private static b(Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1729515
    const-string v0, "media_data"

    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;->mLocalMediaData:Lcom/facebook/ipc/media/data/LocalMediaData;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1729516
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1729517
    check-cast p1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;

    invoke-static {p1, p2, p3}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem$Serializer;->a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;LX/0nX;LX/0my;)V

    return-void
.end method
