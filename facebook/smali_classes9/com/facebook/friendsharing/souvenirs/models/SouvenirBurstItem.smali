.class public final Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem$Serializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final mItems:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "photo_items"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;",
            ">;"
        }
    .end annotation
.end field

.field public final mSelectedIndex:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "selected_index"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1729228
    const-class v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItemDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1729227
    const-class v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem$Serializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1729226
    new-instance v0, LX/Axz;

    invoke-direct {v0}, LX/Axz;-><init>()V

    sput-object v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1729222
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1729223
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;->mItems:LX/0Px;

    .line 1729224
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;->mSelectedIndex:I

    .line 1729225
    return-void
.end method

.method public constructor <init>(LX/0Px;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 1729216
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1729217
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1729218
    iput-object p1, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;->mItems:LX/0Px;

    .line 1729219
    iput p2, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;->mSelectedIndex:I

    .line 1729220
    return-void

    .line 1729221
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1729208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1729209
    sget-object v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;->mItems:LX/0Px;

    .line 1729210
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;->mSelectedIndex:I

    .line 1729211
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;->mItems:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1729212
    iget v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;->mSelectedIndex:I

    iget-object v3, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;->mItems:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1729213
    return-void

    :cond_0
    move v0, v2

    .line 1729214
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1729215
    goto :goto_1
.end method

.method private d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1729207
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;->mItems:LX/0Px;

    iget v1, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;->mSelectedIndex:I

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final b()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1729206
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;->mItems:LX/0Px;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1729193
    iget v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;->mSelectedIndex:I

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1729205
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 1729203
    if-ne p1, p0, :cond_0

    move v0, v1

    .line 1729204
    :goto_0
    return v0

    :cond_0
    instance-of v0, p1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;

    invoke-direct {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;->d()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    check-cast p1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;

    invoke-virtual {p1}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1729199
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;->mItems:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, 0x55

    .line 1729200
    mul-int/lit8 v0, v0, 0x11

    iget v1, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;->mSelectedIndex:I

    add-int/2addr v0, v1

    .line 1729201
    mul-int/lit8 v0, v0, 0x11

    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;->iq_()LX/Ay0;

    move-result-object v1

    invoke-virtual {v1}, LX/Ay0;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1729202
    return v0
.end method

.method public final iq_()LX/Ay0;
    .locals 1

    .prologue
    .line 1729198
    sget-object v0, LX/Ay0;->Burst:LX/Ay0;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 1729197
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "{SouvenirBurstItem size=%d selected=%s}"

    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget-object v4, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;->mItems:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;->mItems:LX/0Px;

    iget v5, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;->mSelectedIndex:I

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1729194
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;->mItems:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1729195
    iget v0, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;->mSelectedIndex:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1729196
    return-void
.end method
