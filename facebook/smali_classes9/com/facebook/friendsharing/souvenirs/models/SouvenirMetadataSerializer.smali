.class public Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadataSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1729415
    const-class v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    new-instance v1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadataSerializer;

    invoke-direct {v1}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadataSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1729416
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1729417
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1729418
    if-nez p0, :cond_0

    .line 1729419
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1729420
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1729421
    invoke-static {p0, p1, p2}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadataSerializer;->b(Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;LX/0nX;LX/0my;)V

    .line 1729422
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1729423
    return-void
.end method

.method private static b(Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1729424
    const-string v0, "local_id"

    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1729425
    const-string v0, "title"

    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mTitle:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1729426
    const-string v0, "start_date"

    iget-wide v2, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mStartDate:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1729427
    const-string v0, "end_date"

    iget-wide v2, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mEndDate:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1729428
    const-string v0, "shareability_score"

    iget v1, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mShareabilityScore:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 1729429
    const-string v0, "classifier_score"

    iget-wide v2, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mClassifierScore:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Double;)V

    .line 1729430
    const-string v0, "logging_data"

    iget-object v1, p0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->mLoggingData:Ljava/util/Map;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1729431
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1729432
    check-cast p1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    invoke-static {p1, p2, p3}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadataSerializer;->a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;LX/0nX;LX/0my;)V

    return-void
.end method
