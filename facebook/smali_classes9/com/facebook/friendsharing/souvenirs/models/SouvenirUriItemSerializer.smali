.class public Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItemSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1729648
    const-class v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;

    new-instance v1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItemSerializer;

    invoke-direct {v1}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItemSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1729649
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1729650
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1729651
    if-nez p0, :cond_0

    .line 1729652
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1729653
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1729654
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1729655
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1729656
    check-cast p1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;

    invoke-static {p1, p2, p3}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItemSerializer;->a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;LX/0nX;LX/0my;)V

    return-void
.end method
