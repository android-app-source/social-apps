.class public final Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2e8d85a1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1730297
    const-class v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1730296
    const-class v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1730333
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1730334
    return-void
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1730330
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1730331
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1730332
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private l()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel$ProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1730328
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel;->g:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel$ProfilePictureModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel$ProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel$ProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel;->g:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel$ProfilePictureModel;

    .line 1730329
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel;->g:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel$ProfilePictureModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1730318
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1730319
    invoke-direct {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel;->k()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1730320
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1730321
    invoke-direct {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel;->l()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel$ProfilePictureModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1730322
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1730323
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1730324
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1730325
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1730326
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1730327
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1730310
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1730311
    invoke-direct {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel;->l()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1730312
    invoke-direct {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel;->l()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel$ProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel$ProfilePictureModel;

    .line 1730313
    invoke-direct {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel;->l()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel$ProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1730314
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel;

    .line 1730315
    iput-object v0, v1, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel;->g:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel$ProfilePictureModel;

    .line 1730316
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1730317
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1730309
    new-instance v0, LX/Ayg;

    invoke-direct {v0, p1}, LX/Ayg;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final synthetic a()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel$ProfilePictureModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1730308
    invoke-direct {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel;->l()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel$ProfilePictureModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1730306
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1730307
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1730305
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1730302
    new-instance v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel;

    invoke-direct {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel;-><init>()V

    .line 1730303
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1730304
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1730301
    const v0, 0x886bd6a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1730300
    const v0, 0x3c2b9d5

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1730298
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel;->f:Ljava/lang/String;

    .line 1730299
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel;->f:Ljava/lang/String;

    return-object v0
.end method
