.class public final Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5c7c0c54
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1730485
    const-class v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1730484
    const-class v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1730482
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1730483
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1730476
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1730477
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel$EdgesModel;->j()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1730478
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1730479
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1730480
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1730481
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1730468
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1730469
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel$EdgesModel;->j()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1730470
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel$EdgesModel;->j()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;

    .line 1730471
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel$EdgesModel;->j()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1730472
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel$EdgesModel;

    .line 1730473
    iput-object v0, v1, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel$EdgesModel;->e:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;

    .line 1730474
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1730475
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1730467
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel$EdgesModel;->j()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1730464
    new-instance v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel$EdgesModel;-><init>()V

    .line 1730465
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1730466
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1730463
    const v0, 0x71cd6f7d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1730460
    const v0, 0x5c5cdd52

    return v0
.end method

.method public final j()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1730461
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel$EdgesModel;->e:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel$EdgesModel;->e:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;

    .line 1730462
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel$EdgesModel;->e:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;

    return-object v0
.end method
