.class public final Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4fb7c128
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1730385
    const-class v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1730384
    const-class v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1730382
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1730383
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1730376
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1730377
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1730378
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1730379
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1730380
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1730381
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1730374
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel;->e:Ljava/util/List;

    .line 1730375
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1730366
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1730367
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1730368
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1730369
    if-eqz v1, :cond_0

    .line 1730370
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel;

    .line 1730371
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel;->e:Ljava/util/List;

    .line 1730372
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1730373
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1730365
    new-instance v0, LX/Ayh;

    invoke-direct {v0, p1}, LX/Ayh;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1730363
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1730364
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1730362
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1730359
    new-instance v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel;

    invoke-direct {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel;-><init>()V

    .line 1730360
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1730361
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1730357
    const v0, -0x157f8db9

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1730358
    const v0, 0x4c808d5

    return v0
.end method
