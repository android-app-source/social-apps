.class public final Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x72419070
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1730688
    const-class v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1730687
    const-class v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1730685
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1730686
    return-void
.end method

.method private j()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1730683
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;->e:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;

    iput-object v0, p0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;->e:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;

    .line 1730684
    iget-object v0, p0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;->e:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1730689
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1730690
    invoke-direct {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;->j()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1730691
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1730692
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1730693
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1730694
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1730675
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1730676
    invoke-direct {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;->j()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1730677
    invoke-direct {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;->j()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;

    .line 1730678
    invoke-direct {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;->j()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1730679
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;

    .line 1730680
    iput-object v0, v1, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;->e:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;

    .line 1730681
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1730682
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1730674
    invoke-direct {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;->j()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1730669
    new-instance v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;-><init>()V

    .line 1730670
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1730671
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1730673
    const v0, 0x337e92b5

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1730672
    const v0, 0x5fcb943a

    return v0
.end method
