.class public Lcom/facebook/goodwill/publish/PublishUnifiedCollageMethod_ThrowbackCardPojoSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/goodwill/publish/PublishUnifiedCollageMethod$ThrowbackCardPojo;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1863811
    const-class v0, Lcom/facebook/goodwill/publish/PublishUnifiedCollageMethod$ThrowbackCardPojo;

    new-instance v1, Lcom/facebook/goodwill/publish/PublishUnifiedCollageMethod_ThrowbackCardPojoSerializer;

    invoke-direct {v1}, Lcom/facebook/goodwill/publish/PublishUnifiedCollageMethod_ThrowbackCardPojoSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1863812
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1863813
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/goodwill/publish/PublishUnifiedCollageMethod$ThrowbackCardPojo;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1863814
    if-nez p0, :cond_0

    .line 1863815
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1863816
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1863817
    invoke-static {p0, p1, p2}, Lcom/facebook/goodwill/publish/PublishUnifiedCollageMethod_ThrowbackCardPojoSerializer;->b(Lcom/facebook/goodwill/publish/PublishUnifiedCollageMethod$ThrowbackCardPojo;LX/0nX;LX/0my;)V

    .line 1863818
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1863819
    return-void
.end method

.method private static b(Lcom/facebook/goodwill/publish/PublishUnifiedCollageMethod$ThrowbackCardPojo;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1863820
    const-string v0, "campaign_id"

    iget-wide v2, p0, Lcom/facebook/goodwill/publish/PublishUnifiedCollageMethod$ThrowbackCardPojo;->campaign_id:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1863821
    const-string v0, "source"

    iget-object v1, p0, Lcom/facebook/goodwill/publish/PublishUnifiedCollageMethod$ThrowbackCardPojo;->source:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1863822
    const-string v0, "media_ids"

    iget-object v1, p0, Lcom/facebook/goodwill/publish/PublishUnifiedCollageMethod$ThrowbackCardPojo;->media_ids:Ljava/util/List;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1863823
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1863824
    check-cast p1, Lcom/facebook/goodwill/publish/PublishUnifiedCollageMethod$ThrowbackCardPojo;

    invoke-static {p1, p2, p3}, Lcom/facebook/goodwill/publish/PublishUnifiedCollageMethod_ThrowbackCardPojoSerializer;->a(Lcom/facebook/goodwill/publish/PublishUnifiedCollageMethod$ThrowbackCardPojo;LX/0nX;LX/0my;)V

    return-void
.end method
