.class public Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;",
            ">;"
        }
    .end annotation
.end field

.field public final l:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1863762
    new-instance v0, LX/CFc;

    invoke-direct {v0}, LX/CFc;-><init>()V

    sput-object v0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1863746
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1863747
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->a:Ljava/lang/String;

    .line 1863748
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->b:Ljava/lang/String;

    .line 1863749
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->c:Ljava/lang/String;

    .line 1863750
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->d:Ljava/lang/String;

    .line 1863751
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->e:Ljava/lang/String;

    .line 1863752
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->f:Ljava/lang/String;

    .line 1863753
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->g:Ljava/lang/String;

    .line 1863754
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->h:Ljava/lang/String;

    .line 1863755
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->i:Ljava/lang/String;

    .line 1863756
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->j:Ljava/util/List;

    .line 1863757
    iget-object v0, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->j:Ljava/util/List;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 1863758
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->k:Ljava/util/List;

    .line 1863759
    iget-object v0, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->k:Ljava/util/List;

    sget-object v1, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 1863760
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->l:Ljava/lang/String;

    .line 1863761
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/0Px;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1863718
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1863719
    iput-object p1, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->a:Ljava/lang/String;

    .line 1863720
    iput-object p2, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->b:Ljava/lang/String;

    .line 1863721
    iput-object p3, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->c:Ljava/lang/String;

    .line 1863722
    iput-object p7, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->d:Ljava/lang/String;

    .line 1863723
    iput-object p8, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->e:Ljava/lang/String;

    .line 1863724
    iput-object p9, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->f:Ljava/lang/String;

    .line 1863725
    iput-object p4, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->g:Ljava/lang/String;

    .line 1863726
    iput-object p5, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->h:Ljava/lang/String;

    .line 1863727
    iput-object p6, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->i:Ljava/lang/String;

    .line 1863728
    iput-object p10, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->j:Ljava/util/List;

    .line 1863729
    iput-object p11, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->k:Ljava/util/List;

    .line 1863730
    iput-object p12, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->l:Ljava/lang/String;

    .line 1863731
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1863745
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1863732
    iget-object v0, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1863733
    iget-object v0, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1863734
    iget-object v0, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1863735
    iget-object v0, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1863736
    iget-object v0, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1863737
    iget-object v0, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1863738
    iget-object v0, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1863739
    iget-object v0, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1863740
    iget-object v0, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1863741
    iget-object v0, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->j:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1863742
    iget-object v0, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->k:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1863743
    iget-object v0, p0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1863744
    return-void
.end method
