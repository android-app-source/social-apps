.class public Lcom/facebook/goodwill/publish/GoodwillPublishLifeEventParam;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/goodwill/publish/GoodwillPublishLifeEventParam;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1863376
    new-instance v0, LX/CFU;

    invoke-direct {v0}, LX/CFU;-><init>()V

    sput-object v0, Lcom/facebook/goodwill/publish/GoodwillPublishLifeEventParam;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/CFV;)V
    .locals 1

    .prologue
    .line 1863393
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1863394
    iget-object v0, p1, LX/CFV;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishLifeEventParam;->a:Ljava/lang/String;

    .line 1863395
    iget-object v0, p1, LX/CFV;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishLifeEventParam;->b:Ljava/lang/String;

    .line 1863396
    iget-object v0, p1, LX/CFV;->d:Ljava/util/List;

    iput-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishLifeEventParam;->c:Ljava/util/List;

    .line 1863397
    iget-object v0, p1, LX/CFV;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishLifeEventParam;->d:Ljava/lang/String;

    .line 1863398
    iget-object v0, p1, LX/CFV;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishLifeEventParam;->e:Ljava/lang/String;

    .line 1863399
    iget-object v0, p1, LX/CFV;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishLifeEventParam;->f:Ljava/lang/String;

    .line 1863400
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1863385
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1863386
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishLifeEventParam;->a:Ljava/lang/String;

    .line 1863387
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishLifeEventParam;->b:Ljava/lang/String;

    .line 1863388
    const-class v0, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishLifeEventParam;->c:Ljava/util/List;

    .line 1863389
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishLifeEventParam;->d:Ljava/lang/String;

    .line 1863390
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishLifeEventParam;->e:Ljava/lang/String;

    .line 1863391
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishLifeEventParam;->f:Ljava/lang/String;

    .line 1863392
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1863384
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1863377
    iget-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishLifeEventParam;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1863378
    iget-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishLifeEventParam;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1863379
    iget-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishLifeEventParam;->c:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1863380
    iget-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishLifeEventParam;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1863381
    iget-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishLifeEventParam;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1863382
    iget-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishLifeEventParam;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1863383
    return-void
.end method
