.class public Lcom/facebook/goodwill/publish/PublishUnifiedCollageParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/goodwill/publish/PublishUnifiedCollageParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/facebook/composer/publish/common/PublishPostParams;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1863828
    new-instance v0, LX/CFe;

    invoke-direct {v0}, LX/CFe;-><init>()V

    sput-object v0, Lcom/facebook/goodwill/publish/PublishUnifiedCollageParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1863829
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1863830
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/publish/PublishUnifiedCollageParams;->a:Ljava/lang/String;

    .line 1863831
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/publish/PublishUnifiedCollageParams;->b:Ljava/lang/String;

    .line 1863832
    const-class v0, Lcom/facebook/composer/publish/common/PublishPostParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PublishPostParams;

    iput-object v0, p0, Lcom/facebook/goodwill/publish/PublishUnifiedCollageParams;->d:Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 1863833
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/goodwill/publish/PublishUnifiedCollageParams;->c:Ljava/util/List;

    .line 1863834
    iget-object v0, p0, Lcom/facebook/goodwill/publish/PublishUnifiedCollageParams;->c:Ljava/util/List;

    sget-object v1, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 1863835
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/composer/publish/common/PublishPostParams;LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/composer/publish/common/PublishPostParams;",
            "LX/0Px",
            "<",
            "Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1863836
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1863837
    iput-object p1, p0, Lcom/facebook/goodwill/publish/PublishUnifiedCollageParams;->a:Ljava/lang/String;

    .line 1863838
    iput-object p2, p0, Lcom/facebook/goodwill/publish/PublishUnifiedCollageParams;->b:Ljava/lang/String;

    .line 1863839
    iput-object p3, p0, Lcom/facebook/goodwill/publish/PublishUnifiedCollageParams;->d:Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 1863840
    iput-object p4, p0, Lcom/facebook/goodwill/publish/PublishUnifiedCollageParams;->c:Ljava/util/List;

    .line 1863841
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1863842
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1863843
    iget-object v0, p0, Lcom/facebook/goodwill/publish/PublishUnifiedCollageParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1863844
    iget-object v0, p0, Lcom/facebook/goodwill/publish/PublishUnifiedCollageParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1863845
    iget-object v0, p0, Lcom/facebook/goodwill/publish/PublishUnifiedCollageParams;->d:Lcom/facebook/composer/publish/common/PublishPostParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1863846
    iget-object v0, p0, Lcom/facebook/goodwill/publish/PublishUnifiedCollageParams;->c:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1863847
    return-void
.end method
