.class public Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1863404
    new-instance v0, LX/CFW;

    invoke-direct {v0}, LX/CFW;-><init>()V

    sput-object v0, Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1863405
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1863406
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;->a:Ljava/lang/String;

    .line 1863407
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;->b:Ljava/lang/String;

    .line 1863408
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;->c:Ljava/lang/String;

    .line 1863409
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1863410
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1863411
    iput-object p1, p0, Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;->a:Ljava/lang/String;

    .line 1863412
    iput-object p2, p0, Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;->b:Ljava/lang/String;

    .line 1863413
    iput-object p3, p0, Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;->c:Ljava/lang/String;

    .line 1863414
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1863415
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1863416
    iget-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1863417
    iget-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1863418
    iget-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1863419
    return-void
.end method
