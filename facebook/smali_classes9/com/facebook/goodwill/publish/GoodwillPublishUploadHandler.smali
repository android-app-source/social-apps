.class public Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1qM;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CFb;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CFa;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CFd;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/8OJ;

.field private final f:LX/0aG;

.field private final g:LX/73w;

.field private final h:LX/8Ne;

.field private final i:Landroid/app/NotificationManager;

.field private final j:LX/8Ko;

.field public final k:LX/03V;

.field private final l:Ljava/util/concurrent/ExecutorService;

.field public final m:LX/2HB;


# direct methods
.method public constructor <init>(LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;Ljava/util/concurrent/ExecutorService;LX/8OJ;LX/73w;Landroid/app/NotificationManager;LX/0aG;LX/8Ko;LX/8Ne;LX/03V;Landroid/content/Context;)V
    .locals 1
    .param p5    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CFb;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CFa;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CFd;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/8OJ;",
            "Lcom/facebook/photos/base/analytics/PhotoFlowLogger;",
            "Landroid/app/NotificationManager;",
            "LX/0aG;",
            "Lcom/facebook/photos/upload/manager/UploadNotificationConfiguration;",
            "LX/8Ne;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1863625
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1863626
    iput-object p1, p0, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->a:LX/0Or;

    .line 1863627
    iput-object p2, p0, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->b:LX/0Ot;

    .line 1863628
    iput-object p3, p0, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->c:LX/0Ot;

    .line 1863629
    iput-object p4, p0, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->d:LX/0Ot;

    .line 1863630
    iput-object p5, p0, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->l:Ljava/util/concurrent/ExecutorService;

    .line 1863631
    iput-object p6, p0, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->e:LX/8OJ;

    .line 1863632
    iput-object p7, p0, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->g:LX/73w;

    .line 1863633
    iput-object p8, p0, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->i:Landroid/app/NotificationManager;

    .line 1863634
    iput-object p9, p0, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->f:LX/0aG;

    .line 1863635
    iput-object p10, p0, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->j:LX/8Ko;

    .line 1863636
    iput-object p11, p0, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->h:LX/8Ne;

    .line 1863637
    iput-object p12, p0, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->k:LX/03V;

    .line 1863638
    new-instance v0, LX/2HB;

    invoke-direct {v0, p13}, LX/2HB;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->m:LX/2HB;

    .line 1863639
    return-void
.end method

.method private a(LX/1qK;LX/0e6;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 7

    .prologue
    .line 1863608
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v3, v0

    .line 1863609
    if-nez v3, :cond_0

    .line 1863610
    sget-object v0, LX/1nY;->API_ERROR:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 1863611
    :goto_0
    return-object v0

    .line 1863612
    :cond_0
    const-string v0, "request_notification"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;

    .line 1863613
    const-string v1, "request_callback"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler$UploadStatusCallback;

    .line 1863614
    const-string v2, "request_photos"

    invoke-virtual {v3, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 1863615
    const-string v4, "request_privacy"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1863616
    const-string v5, "request_composer_session_id"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1863617
    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    .line 1863618
    invoke-direct {p0, v2, v4, v3, v5}, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->a(Ljava/util/List;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 1863619
    :cond_1
    iget-object v2, p0, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/11H;

    const-string v4, "request_params"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    invoke-virtual {v2, p2, v3, v4}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    .line 1863620
    iget-object v2, p0, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->m:LX/2HB;

    const/4 v3, 0x1

    invoke-static {p0, v2, v0, v3}, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->a$redex0(Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;LX/2HB;Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;Z)V

    .line 1863621
    if-eqz v1, :cond_2

    .line 1863622
    invoke-virtual {v1}, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler$UploadStatusCallback;->a()V

    .line 1863623
    :cond_2
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1863624
    goto :goto_0
.end method

.method private static a(Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;Landroid/os/Bundle;Ljava/lang/String;Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler$UploadStatusCallback;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1863597
    if-eqz p3, :cond_0

    .line 1863598
    const-string v0, "request_notification"

    invoke-virtual {p1, v0, p3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1863599
    iget-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->m:LX/2HB;

    invoke-virtual {v0, v1, v1, v2}, LX/2HB;->a(IIZ)LX/2HB;

    move-result-object v0

    .line 1863600
    iget-object v1, p3, Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1863601
    invoke-virtual {v0, v1}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->j:LX/8Ko;

    invoke-virtual {v1}, LX/8Ko;->a()I

    move-result v1

    invoke-virtual {v0, v1}, LX/2HB;->a(I)LX/2HB;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/2HB;->a(Z)LX/2HB;

    .line 1863602
    iget-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->i:Landroid/app/NotificationManager;

    const/16 v1, 0x7f82

    iget-object v2, p0, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->m:LX/2HB;

    invoke-virtual {v2}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 1863603
    :cond_0
    const-string v0, "request_callback"

    invoke-virtual {p1, v0, p4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1863604
    iget-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->f:LX/0aG;

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const-class v1, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, 0x32cf4256

    move-object v1, p2

    move-object v2, p1

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    .line 1863605
    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 1863606
    new-instance v1, LX/CFY;

    invoke-direct {v1, p0, p3, p2, p4}, LX/CFY;-><init>(Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;Ljava/lang/String;Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler$UploadStatusCallback;)V

    iget-object v2, p0, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->l:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1863607
    return-void
.end method

.method private static a(Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;Landroid/os/Bundle;Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1863461
    :try_start_0
    new-instance v13, Lorg/json/JSONObject;

    move-object/from16 v0, p2

    invoke-direct {v13, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1863462
    const/4 v2, 0x0

    .line 1863463
    invoke-virtual {v13}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v3

    .line 1863464
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1863465
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v13, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 1863466
    const-string v4, "photos"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1863467
    const-string v4, "photos"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 1863468
    const/4 v1, 0x0

    move v14, v1

    move v1, v2

    move v2, v14

    :goto_1
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v2, v5, :cond_2

    .line 1863469
    invoke-virtual {v4, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 1863470
    const-string v6, "mutations"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1863471
    const-string v6, "mutations"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    .line 1863472
    const-string v6, "id"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1863473
    const-string v6, "id"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1863474
    const-string v7, "uploaded_"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1863475
    const/16 v7, 0x9

    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 1863476
    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1863477
    const-string v1, "id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v1, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1863478
    const/4 v1, 0x1

    .line 1863479
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    move v2, v1

    .line 1863480
    goto :goto_0

    .line 1863481
    :cond_3
    if-nez v2, :cond_4

    .line 1863482
    :goto_2
    return-void

    .line 1863483
    :cond_4
    const-string v1, "request_params"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;

    move-object v12, v0

    .line 1863484
    new-instance v1, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;

    iget-object v2, v12, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->a:Ljava/lang/String;

    iget-object v3, v12, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->b:Ljava/lang/String;

    iget-object v4, v12, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->c:Ljava/lang/String;

    iget-object v5, v12, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->g:Ljava/lang/String;

    iget-object v6, v12, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->h:Ljava/lang/String;

    iget-object v7, v12, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->i:Ljava/lang/String;

    iget-object v8, v12, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->d:Ljava/lang/String;

    iget-object v9, v12, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->e:Ljava/lang/String;

    iget-object v10, v12, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->f:Ljava/lang/String;

    iget-object v11, v12, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->j:Ljava/util/List;

    invoke-static {v11}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v11

    iget-object v12, v12, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->k:Ljava/util/List;

    invoke-static {v12}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v12

    invoke-virtual {v13}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct/range {v1 .. v13}, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/0Px;Ljava/lang/String;)V

    .line 1863485
    const-string v2, "request_params"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 1863486
    :catch_0
    move-exception v1

    .line 1863487
    iget-object v2, p0, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->k:LX/03V;

    const-string v3, "GoodwillPublishUpload"

    const-string v4, "Failed to parse share payload when updating with uploaded photos!"

    invoke-virtual {v2, v3, v4, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method private a(Ljava/util/List;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;",
            ">;",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1863555
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 1863556
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;

    .line 1863557
    iget-boolean v2, v0, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->c:Z

    move v2, v2

    .line 1863558
    if-eqz v2, :cond_0

    .line 1863559
    const-wide/16 v2, 0x0

    .line 1863560
    iget-object v5, v0, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->a:Ljava/lang/String;

    move-object v5, v5

    .line 1863561
    if-eqz v5, :cond_1

    .line 1863562
    iget-object v2, v0, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1863563
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1863564
    :cond_1
    new-instance v5, LX/8NJ;

    new-instance v6, Lcom/facebook/photos/upload/protocol/UploadPhotoSource;

    .line 1863565
    iget-object v7, v0, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->b:Ljava/lang/String;

    move-object v0, v7

    .line 1863566
    const-string v7, "file://"

    invoke-virtual {v0, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1863567
    const/4 v7, 0x7

    invoke-virtual {v0, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 1863568
    :cond_2
    move-object v0, v0

    .line 1863569
    invoke-direct {v6, v0, v2, v3}, Lcom/facebook/photos/upload/protocol/UploadPhotoSource;-><init>(Ljava/lang/String;J)V

    invoke-direct {v5, v6}, LX/8NJ;-><init>(Lcom/facebook/photos/upload/protocol/UploadPhotoSource;)V

    new-instance v0, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    invoke-direct {v0, p2}, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;-><init>(Ljava/lang/String;)V

    .line 1863570
    iput-object v0, v5, LX/8NJ;->i:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1863571
    move-object v0, v5

    .line 1863572
    invoke-virtual {v0}, LX/8NJ;->a()Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    move-result-object v0

    .line 1863573
    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1863574
    :cond_3
    invoke-virtual {v1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1863575
    :cond_4
    return-void

    .line 1863576
    :cond_5
    iget-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->g:LX/73w;

    invoke-virtual {v0, p4}, LX/73w;->a(Ljava/lang/String;)V

    .line 1863577
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 1863578
    new-instance v2, LX/CFZ;

    invoke-direct {v2, p0, v9}, LX/CFZ;-><init>(Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;Ljava/util/HashMap;)V

    .line 1863579
    new-instance v3, LX/8OL;

    invoke-direct {v3}, LX/8OL;-><init>()V

    .line 1863580
    iget-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->e:LX/8OJ;

    iget-object v4, p0, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->g:LX/73w;

    iget-object v5, p0, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->g:LX/73w;

    const-string v6, "2.0"

    invoke-virtual {v5, v6}, LX/73w;->j(Ljava/lang/String;)LX/74b;

    move-result-object v5

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->h:LX/8Ne;

    const-class v8, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;

    invoke-static {v8}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v8

    invoke-virtual/range {v0 .. v8}, LX/8OJ;->a(Ljava/util/Collection;LX/8O7;LX/8OL;LX/73w;LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Ne;Lcom/facebook/common/callercontext/CallerContext;)Ljava/util/Map;

    .line 1863581
    const-string v0, "request_params"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    .line 1863582
    instance-of v1, v0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;

    if-eqz v1, :cond_8

    .line 1863583
    check-cast v0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;

    .line 1863584
    iget-object v0, v0, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;->l:Ljava/lang/String;

    .line 1863585
    :goto_1
    move-object v0, v0

    .line 1863586
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 1863587
    invoke-static {p0, p3, v0, v9}, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->a(Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;Landroid/os/Bundle;Ljava/lang/String;Ljava/util/HashMap;)V

    .line 1863588
    :cond_6
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_7
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;

    .line 1863589
    iget-boolean v1, v0, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->c:Z

    move v1, v1

    .line 1863590
    if-eqz v1, :cond_7

    .line 1863591
    iget-object v1, v0, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1863592
    invoke-virtual {v9, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1863593
    if-eqz v1, :cond_7

    .line 1863594
    iput-object v1, v0, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->a:Ljava/lang/String;

    .line 1863595
    const/4 v3, 0x0

    iput-boolean v3, v0, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->c:Z

    .line 1863596
    goto :goto_2

    :cond_8
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;LX/2HB;Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1863542
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    .line 1863543
    if-eqz p3, :cond_1

    .line 1863544
    iget-object v0, p2, Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1863545
    move-object v1, v0

    .line 1863546
    :goto_0
    if-eqz p3, :cond_2

    iget-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->j:LX/8Ko;

    invoke-virtual {v0}, LX/8Ko;->c()I

    move-result v0

    .line 1863547
    :goto_1
    invoke-virtual {p1, v4, v4, v4}, LX/2HB;->a(IIZ)LX/2HB;

    move-result-object v2

    .line 1863548
    iget-object v3, p2, Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1863549
    invoke-virtual {v2, v3}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/2HB;->a(I)LX/2HB;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/2HB;->a(Z)LX/2HB;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    .line 1863550
    iget-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->i:Landroid/app/NotificationManager;

    const/16 v1, 0x7f82

    invoke-virtual {p1}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 1863551
    :cond_0
    return-void

    .line 1863552
    :cond_1
    iget-object v0, p2, Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1863553
    move-object v1, v0

    goto :goto_0

    .line 1863554
    :cond_2
    const v0, 0x1080078

    goto :goto_1
.end method

.method public static b(LX/0QB;)Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;
    .locals 14

    .prologue
    .line 1863540
    new-instance v0, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;

    const/16 v1, 0xb83

    invoke-static {p0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0x2354

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x2353

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x2355

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/8OJ;->b(LX/0QB;)LX/8OJ;

    move-result-object v6

    check-cast v6, LX/8OJ;

    invoke-static {p0}, LX/73w;->b(LX/0QB;)LX/73w;

    move-result-object v7

    check-cast v7, LX/73w;

    invoke-static {p0}, LX/1s4;->b(LX/0QB;)Landroid/app/NotificationManager;

    move-result-object v8

    check-cast v8, Landroid/app/NotificationManager;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v9

    check-cast v9, LX/0aG;

    invoke-static {p0}, LX/8LN;->b(LX/0QB;)LX/8Ko;

    move-result-object v10

    check-cast v10, LX/8Ko;

    invoke-static {p0}, LX/8LE;->b(LX/0QB;)LX/8Ne;

    move-result-object v11

    check-cast v11, LX/8Ne;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v12

    check-cast v12, LX/03V;

    const-class v13, Landroid/content/Context;

    invoke-interface {p0, v13}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/content/Context;

    invoke-direct/range {v0 .. v13}, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;-><init>(LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;Ljava/util/concurrent/ExecutorService;LX/8OJ;LX/73w;Landroid/app/NotificationManager;LX/0aG;LX/8Ko;LX/8Ne;LX/03V;Landroid/content/Context;)V

    .line 1863541
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/composer/publish/common/PublishPostParams;Ljava/lang/String;LX/0Px;Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler$UploadStatusCallback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/composer/publish/common/PublishPostParams;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;",
            ">;",
            "Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;",
            "Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler$UploadStatusCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1863515
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1863516
    new-instance v1, LX/CFV;

    invoke-direct {v1}, LX/CFV;-><init>()V

    iget-object v2, p2, Lcom/facebook/composer/publish/common/PublishPostParams;->tracking:Ljava/lang/String;

    .line 1863517
    iput-object v2, v1, LX/CFV;->a:Ljava/lang/String;

    .line 1863518
    move-object v1, v1

    .line 1863519
    iget-object v2, p2, Lcom/facebook/composer/publish/common/PublishPostParams;->rawMessage:Ljava/lang/String;

    .line 1863520
    iput-object v2, v1, LX/CFV;->e:Ljava/lang/String;

    .line 1863521
    move-object v1, v1

    .line 1863522
    iget-object v2, p2, Lcom/facebook/composer/publish/common/PublishPostParams;->privacy:Ljava/lang/String;

    .line 1863523
    iput-object v2, v1, LX/CFV;->b:Ljava/lang/String;

    .line 1863524
    move-object v1, v1

    .line 1863525
    iget-object v2, p2, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    .line 1863526
    iput-object v2, v1, LX/CFV;->c:Ljava/lang/String;

    .line 1863527
    move-object v1, v1

    .line 1863528
    iput-object p4, v1, LX/CFV;->d:Ljava/util/List;

    .line 1863529
    move-object v1, v1

    .line 1863530
    iput-object p3, v1, LX/CFV;->f:Ljava/lang/String;

    .line 1863531
    move-object v1, v1

    .line 1863532
    new-instance v2, Lcom/facebook/goodwill/publish/GoodwillPublishLifeEventParam;

    invoke-direct {v2, v1}, Lcom/facebook/goodwill/publish/GoodwillPublishLifeEventParam;-><init>(LX/CFV;)V

    move-object v1, v2

    .line 1863533
    const-string v2, "request_params"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1863534
    const-string v1, "request_privacy"

    iget-object v2, p2, Lcom/facebook/composer/publish/common/PublishPostParams;->privacy:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1863535
    const-string v1, "request_composer_session_id"

    iget-object v2, p2, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1863536
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1863537
    const-string v2, "request_photos"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1863538
    const-string v1, "publish_goodwill_life_event"

    invoke-static {p0, v0, v1, p5, p6}, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->a(Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;Landroid/os/Bundle;Ljava/lang/String;Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler$UploadStatusCallback;)V

    .line 1863539
    return-void
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/composer/publish/common/PublishPostParams;LX/0Px;Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler$UploadStatusCallback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/composer/publish/common/PublishPostParams;",
            "LX/0Px",
            "<",
            "Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;",
            ">;",
            "Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;",
            "Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler$UploadStatusCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1863506
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1863507
    new-instance v1, Lcom/facebook/goodwill/publish/PublishUnifiedCollageParams;

    invoke-direct {v1, p2, p3, p4, p5}, Lcom/facebook/goodwill/publish/PublishUnifiedCollageParams;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/composer/publish/common/PublishPostParams;LX/0Px;)V

    .line 1863508
    const-string v2, "request_params"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1863509
    const-string v1, "request_privacy"

    iget-object v2, p4, Lcom/facebook/composer/publish/common/PublishPostParams;->privacy:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1863510
    const-string v1, "request_composer_session_id"

    iget-object v2, p4, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1863511
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1863512
    const-string v2, "request_photos"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1863513
    const-string v1, "publish_goodwill_unified_collage"

    invoke-static {p0, v0, v1, p6, p7}, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->a(Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;Landroid/os/Bundle;Ljava/lang/String;Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler$UploadStatusCallback;)V

    .line 1863514
    return-void
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/0Px;Ljava/lang/String;Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler$UploadStatusCallback;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;",
            "Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler$UploadStatusCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1863496
    new-instance v2, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    invoke-direct/range {v2 .. v14}, Lcom/facebook/goodwill/publish/PublishGoodwillVideoParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/0Px;Ljava/lang/String;)V

    .line 1863497
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1863498
    const-string v4, "request_params"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1863499
    const-string v2, "request_privacy"

    move-object/from16 v0, p8

    invoke-virtual {v3, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1863500
    const-string v2, "request_composer_session_id"

    move-object/from16 v0, p10

    invoke-virtual {v3, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1863501
    if-eqz p12, :cond_0

    .line 1863502
    new-instance v2, Ljava/util/ArrayList;

    move-object/from16 v0, p12

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1863503
    const-string v4, "request_photos"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1863504
    :cond_0
    const-string v2, "publish_goodwill_video"

    move-object/from16 v0, p14

    move-object/from16 v1, p15

    invoke-static {p0, v3, v2, v0, v1}, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->a(Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;Landroid/os/Bundle;Ljava/lang/String;Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler$UploadStatusCallback;)V

    .line 1863505
    return-void
.end method

.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 1863488
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v1, v0

    .line 1863489
    const/4 v0, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 1863490
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1863491
    :sswitch_0
    const-string v2, "publish_goodwill_video"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v2, "publish_goodwill_life_event"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "publish_goodwill_unified_collage"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    .line 1863492
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    invoke-direct {p0, p1, v0}, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->a(LX/1qK;LX/0e6;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 1863493
    :goto_1
    return-object v0

    .line 1863494
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    invoke-direct {p0, p1, v0}, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->a(LX/1qK;LX/0e6;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_1

    .line 1863495
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    invoke-direct {p0, p1, v0}, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->a(LX/1qK;LX/0e6;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x3241a01a -> :sswitch_2
        -0x2663d485 -> :sswitch_0
        0x2987a097 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
