.class public final Lcom/facebook/goodwill/publish/PublishUnifiedCollageMethod$ThrowbackCardPojo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation


# instance fields
.field public final campaign_id:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "campaign_id"
    .end annotation
.end field

.field public final media_ids:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "media_ids"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final source:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "source"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1863763
    const-class v0, Lcom/facebook/goodwill/publish/PublishUnifiedCollageMethod_ThrowbackCardPojoDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1863764
    const-class v0, Lcom/facebook/goodwill/publish/PublishUnifiedCollageMethod_ThrowbackCardPojoSerializer;

    return-object v0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1863765
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1863766
    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/goodwill/publish/PublishUnifiedCollageMethod$ThrowbackCardPojo;->campaign_id:J

    .line 1863767
    iput-object p1, p0, Lcom/facebook/goodwill/publish/PublishUnifiedCollageMethod$ThrowbackCardPojo;->source:Ljava/lang/String;

    .line 1863768
    iput-object p3, p0, Lcom/facebook/goodwill/publish/PublishUnifiedCollageMethod$ThrowbackCardPojo;->media_ids:Ljava/util/List;

    .line 1863769
    return-void
.end method
