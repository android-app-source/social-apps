.class public Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1863423
    new-instance v0, LX/CFX;

    invoke-direct {v0}, LX/CFX;-><init>()V

    sput-object v0, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1863444
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1863445
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->a:Ljava/lang/String;

    .line 1863446
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->b:Ljava/lang/String;

    .line 1863447
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->c:Z

    .line 1863448
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1863437
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1863438
    iput-object p1, p0, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->a:Ljava/lang/String;

    .line 1863439
    iput-object p2, p0, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->b:Ljava/lang/String;

    .line 1863440
    iget-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->b:Ljava/lang/String;

    const-string v1, "http"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->c:Z

    .line 1863441
    return-void

    .line 1863442
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1863443
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1863429
    instance-of v0, p1, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;

    if-nez v0, :cond_1

    .line 1863430
    :cond_0
    :goto_0
    return v1

    .line 1863431
    :cond_1
    check-cast p1, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;

    .line 1863432
    iget-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->a:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, p1, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->a:Ljava/lang/String;

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->a:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_3
    move v0, v2

    .line 1863433
    :goto_1
    iget-object v3, p0, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->b:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, p1, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->b:Ljava/lang/String;

    if-eqz v3, :cond_5

    :cond_4
    iget-object v3, p0, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->b:Ljava/lang/String;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->b:Ljava/lang/String;

    iget-object v4, p1, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    :cond_5
    move v3, v2

    .line 1863434
    :goto_2
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    iget-boolean v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->c:Z

    iget-boolean v3, p1, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->c:Z

    if-ne v0, v3, :cond_0

    move v1, v2

    goto :goto_0

    :cond_6
    move v0, v1

    .line 1863435
    goto :goto_1

    :cond_7
    move v3, v1

    .line 1863436
    goto :goto_2
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1863428
    iget-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1863424
    iget-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1863425
    iget-object v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1863426
    iget-boolean v0, p0, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1863427
    return-void
.end method
