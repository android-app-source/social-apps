.class public Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfigSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1863056
    const-class v0, Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;

    new-instance v1, Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfigSerializer;

    invoke-direct {v1}, Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfigSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1863057
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1863058
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1863050
    if-nez p0, :cond_0

    .line 1863051
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1863052
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1863053
    invoke-static {p0, p1, p2}, Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfigSerializer;->b(Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;LX/0nX;LX/0my;)V

    .line 1863054
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1863055
    return-void
.end method

.method private static b(Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1863045
    const-string v0, "campaign_id"

    iget-wide v2, p0, Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;->mCampaignId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1863046
    const-string v0, "source"

    iget-object v1, p0, Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;->mSource:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1863047
    const-string v0, "media_ids"

    iget-object v1, p0, Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;->mMediaIds:Ljava/util/List;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1863048
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1863049
    check-cast p1, Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;

    invoke-static {p1, p2, p3}, Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfigSerializer;->a(Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;LX/0nX;LX/0my;)V

    return-void
.end method
