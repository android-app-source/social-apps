.class public final Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/88f;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfigDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfigSerializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final mComposerHint:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "composer_hint"
    .end annotation
.end field

.field public final mComposerTitle:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "composer_title"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1862437
    const-class v0, Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfigDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1862436
    const-class v0, Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfigSerializer;

    return-object v0
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1862432
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1862433
    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfig;->mComposerHint:Ljava/lang/String;

    .line 1862434
    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfig;->mComposerTitle:Ljava/lang/String;

    .line 1862435
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1862422
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1862423
    iput-object p1, p0, Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfig;->mComposerHint:Ljava/lang/String;

    .line 1862424
    iput-object p2, p0, Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfig;->mComposerTitle:Ljava/lang/String;

    .line 1862425
    invoke-virtual {p0}, Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfig;->a()V

    .line 1862426
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfig;
    .locals 1

    .prologue
    .line 1862431
    new-instance v0, Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfig;

    invoke-direct {v0, p0, p1}, Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfig;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1862430
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1862429
    const-string v0, "GoodwillCampaignComposerPluginConfig"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1862428
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfig;->mComposerHint:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1862427
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfig;->mComposerTitle:Ljava/lang/String;

    return-object v0
.end method
