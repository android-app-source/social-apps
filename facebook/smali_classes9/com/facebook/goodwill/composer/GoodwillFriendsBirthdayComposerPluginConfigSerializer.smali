.class public Lcom/facebook/goodwill/composer/GoodwillFriendsBirthdayComposerPluginConfigSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/goodwill/composer/GoodwillFriendsBirthdayComposerPluginConfig;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1862916
    const-class v0, Lcom/facebook/goodwill/composer/GoodwillFriendsBirthdayComposerPluginConfig;

    new-instance v1, Lcom/facebook/goodwill/composer/GoodwillFriendsBirthdayComposerPluginConfigSerializer;

    invoke-direct {v1}, Lcom/facebook/goodwill/composer/GoodwillFriendsBirthdayComposerPluginConfigSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1862917
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1862918
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/goodwill/composer/GoodwillFriendsBirthdayComposerPluginConfig;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1862919
    if-nez p0, :cond_0

    .line 1862920
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1862921
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1862922
    invoke-static {p0, p1, p2}, Lcom/facebook/goodwill/composer/GoodwillFriendsBirthdayComposerPluginConfigSerializer;->b(Lcom/facebook/goodwill/composer/GoodwillFriendsBirthdayComposerPluginConfig;LX/0nX;LX/0my;)V

    .line 1862923
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1862924
    return-void
.end method

.method private static b(Lcom/facebook/goodwill/composer/GoodwillFriendsBirthdayComposerPluginConfig;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1862925
    const-string v0, "composer_hint"

    iget-object v1, p0, Lcom/facebook/goodwill/composer/GoodwillFriendsBirthdayComposerPluginConfig;->mComposerHint:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1862926
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1862927
    check-cast p1, Lcom/facebook/goodwill/composer/GoodwillFriendsBirthdayComposerPluginConfig;

    invoke-static {p1, p2, p3}, Lcom/facebook/goodwill/composer/GoodwillFriendsBirthdayComposerPluginConfigSerializer;->a(Lcom/facebook/goodwill/composer/GoodwillFriendsBirthdayComposerPluginConfig;LX/0nX;LX/0my;)V

    return-void
.end method
