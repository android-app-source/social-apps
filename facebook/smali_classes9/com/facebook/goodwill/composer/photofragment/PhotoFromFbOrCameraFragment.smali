.class public Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;
.super Lcom/facebook/goodwill/composer/GoodwillComposerFragment;
.source ""

# interfaces
.implements LX/BXc;


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;

.field public b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field public c:Landroid/view/ViewStub;

.field public d:Lcom/facebook/widget/mediareorderview/ImagesReorderView;

.field public e:Landroid/view/ViewGroup;

.field public f:Landroid/widget/ScrollView;

.field public g:Lcom/facebook/goodwill/composer/photofragment/InterceptingRelativeLayout;

.field public h:LX/0wM;

.field private i:Lcom/facebook/fbui/widget/text/ImageWithTextView;

.field private j:Lcom/facebook/fbui/widget/text/ImageWithTextView;

.field public k:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1863354
    invoke-direct {p0}, Lcom/facebook/goodwill/composer/GoodwillComposerFragment;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;Z)V
    .locals 2

    .prologue
    .line 1863346
    iget-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->e:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 1863347
    new-instance v0, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;-><init>(Landroid/content/Context;)V

    .line 1863348
    invoke-virtual {v0, p1}, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->setPhoto(Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;)V

    .line 1863349
    iput-object p0, v0, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->d:Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;

    .line 1863350
    iget-object v1, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1863351
    if-eqz p2, :cond_0

    .line 1863352
    iget-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->f:Landroid/widget/ScrollView;

    new-instance v1, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment$5;

    invoke-direct {v1, p0}, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment$5;-><init>(Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    .line 1863353
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1863339
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1863340
    iget-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;

    .line 1863341
    iget-object v2, v0, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1863342
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1863343
    :cond_0
    iget-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->d:Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->setVisibility(I)V

    .line 1863344
    iget-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 1863345
    return-void
.end method

.method public final a(II)V
    .locals 3

    .prologue
    .line 1863318
    iget-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->g:Lcom/facebook/goodwill/composer/photofragment/InterceptingRelativeLayout;

    const/4 v1, 0x0

    .line 1863319
    iput-object v1, v0, Lcom/facebook/goodwill/composer/photofragment/InterceptingRelativeLayout;->a:Landroid/view/View;

    .line 1863320
    iget-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestFocus()Z

    .line 1863321
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1863322
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1863323
    iget-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;

    invoke-virtual {v0}, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->getPhotoRect()Landroid/graphics/Rect;

    move-result-object v0

    .line 1863324
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1863325
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1863326
    :cond_0
    iget-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->d:Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->a(Ljava/util/List;)V

    .line 1863327
    if-eq p1, p2, :cond_1

    .line 1863328
    iget-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0, p2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;

    .line 1863329
    iget-object v1, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v1, p2}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 1863330
    iget-object v1, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 1863331
    iget-object v1, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->k:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    .line 1863332
    iget-object v2, v0, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->e:Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;

    move-object v2, v2

    .line 1863333
    invoke-virtual {v1, v2}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->b(Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;)V

    .line 1863334
    iget-object v1, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->k:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    .line 1863335
    iget-object v2, v0, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->e:Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;

    move-object v0, v2

    .line 1863336
    iget-object v2, v1, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->g:Ljava/util/List;

    invoke-interface {v2, p1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1863337
    :cond_1
    iget-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 1863338
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1863315
    invoke-super {p0, p1}, Lcom/facebook/goodwill/composer/GoodwillComposerFragment;->a(Landroid/os/Bundle;)V

    .line 1863316
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;

    invoke-static {v0}, LX/6Hl;->b(LX/0QB;)LX/6Hl;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/74n;->b(LX/0QB;)LX/74n;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object p1

    check-cast p1, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    iput-object v2, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->a:Lcom/facebook/content/SecureContextHelper;

    iput-object p1, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iput-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->h:LX/0wM;

    .line 1863317
    return-void
.end method

.method public final a(Lcom/facebook/goodwill/composer/GoodwillComposerEvent;LX/CF8;)V
    .locals 0

    .prologue
    .line 1863355
    iput-object p1, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->k:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    .line 1863356
    invoke-super {p0, p1, p2}, Lcom/facebook/goodwill/composer/GoodwillComposerFragment;->a(Lcom/facebook/goodwill/composer/GoodwillComposerEvent;LX/CF8;)V

    .line 1863357
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1863314
    iget-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->k:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    invoke-virtual {v0}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->g()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/16 v1, 0xa

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 1863312
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082a2f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1863313
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const v1, 0xffff

    const/4 v2, 0x1

    .line 1863297
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 1863298
    and-int v0, p1, v1

    if-ne v0, v2, :cond_1

    .line 1863299
    const-string v0, "extra_media_items"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1863300
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 1863301
    new-instance v1, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;

    invoke-direct {v1, v0}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;-><init>(Lcom/facebook/ipc/media/MediaItem;)V

    .line 1863302
    iget-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->k:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    invoke-virtual {v0, v1}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->a(Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;)V

    .line 1863303
    invoke-direct {p0, v1, v2}, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->a(Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;Z)V

    .line 1863304
    :cond_0
    :goto_0
    return-void

    .line 1863305
    :cond_1
    and-int v0, p1, v1

    const/16 v1, 0x26b9

    if-ne v0, v1, :cond_0

    .line 1863306
    const-string v0, "photo"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 1863307
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1863308
    invoke-static {v0}, LX/6X5;->a(Lcom/facebook/graphql/model/GraphQLPhoto;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 1863309
    new-instance v1, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;

    invoke-direct {v1, v0}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;-><init>(Lcom/facebook/graphql/model/GraphQLMedia;)V

    .line 1863310
    iget-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->k:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    invoke-virtual {v0, v1}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->a(Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;)V

    .line 1863311
    invoke-direct {p0, v1, v2}, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->a(Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;Z)V

    goto :goto_0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    .line 1863290
    invoke-super {p0, p1}, Lcom/facebook/goodwill/composer/GoodwillComposerFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1863291
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1863292
    iget-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;

    .line 1863293
    iget-object v2, v0, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->e:Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;

    move-object v2, v2

    .line 1863294
    invoke-virtual {v0, v2}, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->setPhoto(Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;)V

    .line 1863295
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1863296
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x56bfddb5

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1863289
    const v1, 0x7f0300e5

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x2bf9e7cc

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1863270
    invoke-super {p0, p1, p2}, Lcom/facebook/goodwill/composer/GoodwillComposerFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1863271
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 1863272
    const v0, 0x7f0d0544

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/ImageWithTextView;

    iput-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->j:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    .line 1863273
    iget-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->j:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    new-instance v3, LX/CFQ;

    invoke-direct {v3, p0}, LX/CFQ;-><init>(Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1863274
    iget-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->j:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    iget-object v3, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->h:LX/0wM;

    const v4, 0x7f02095e

    invoke-virtual {v3, v4, v1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1863275
    const v0, 0x7f0d0545

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/ImageWithTextView;

    iput-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->i:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    .line 1863276
    iget-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->i:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    new-instance v3, LX/CFR;

    invoke-direct {v3, p0}, LX/CFR;-><init>(Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1863277
    iget-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->i:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    iget-object v3, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->h:LX/0wM;

    const v4, 0x7f02095b

    invoke-virtual {v3, v4, v1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1863278
    const v0, 0x7f0d0541

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/composer/ui/GoodwillComposerTitleBar;

    .line 1863279
    const v1, 0x7f082a27

    invoke-virtual {v0, v1}, Lcom/facebook/goodwill/composer/ui/GoodwillComposerTitleBar;->setTitle(I)V

    .line 1863280
    new-instance v1, LX/CFS;

    invoke-direct {v1, p0}, LX/CFS;-><init>(Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;)V

    new-instance v3, LX/CFT;

    invoke-direct {v3, p0}, LX/CFT;-><init>(Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;)V

    invoke-virtual {v0, v1, v3}, Lcom/facebook/goodwill/composer/ui/GoodwillComposerTitleBar;->a(Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    .line 1863281
    const v0, 0x7f0d0540

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->c:Landroid/view/ViewStub;

    .line 1863282
    const v0, 0x7f0d053f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->e:Landroid/view/ViewGroup;

    .line 1863283
    const v0, 0x7f0d053e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->f:Landroid/widget/ScrollView;

    .line 1863284
    const v0, 0x7f0d053d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/composer/photofragment/InterceptingRelativeLayout;

    iput-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->g:Lcom/facebook/goodwill/composer/photofragment/InterceptingRelativeLayout;

    .line 1863285
    iget-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->k:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    invoke-virtual {v0}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->g()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;

    .line 1863286
    invoke-direct {p0, v0, v2}, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->a(Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;Z)V

    .line 1863287
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1863288
    :cond_0
    return-void
.end method
