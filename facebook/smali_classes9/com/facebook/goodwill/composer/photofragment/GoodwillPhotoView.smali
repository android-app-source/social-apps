.class public Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;
.super Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public c:Landroid/widget/ImageView;

.field public d:Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;

.field public e:Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;

.field public final f:Landroid/graphics/Point;

.field public g:Landroid/net/Uri;

.field public h:LX/1f1;

.field public i:LX/1Ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1863169
    const-class v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const-string v1, "goodwill_composer"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1863170
    invoke-direct {p0, p1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1863171
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->f:Landroid/graphics/Point;

    .line 1863172
    const-class v0, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;

    invoke-static {v0, p0}, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1863173
    invoke-virtual {p0}, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0307db

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1863174
    const v0, 0x7f0d14bd

    invoke-virtual {p0, v0}, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1863175
    const v0, 0x7f0d14be

    invoke-virtual {p0, v0}, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->c:Landroid/widget/ImageView;

    .line 1863176
    iget-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->c:Landroid/widget/ImageView;

    new-instance v1, LX/CFN;

    invoke-direct {v1, p0}, LX/CFN;-><init>(Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1863177
    iget-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    new-instance v1, LX/CFO;

    invoke-direct {v1, p0}, LX/CFO;-><init>(Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1863178
    iget-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    new-instance v1, LX/CFP;

    invoke-direct {v1, p0}, LX/CFP;-><init>(Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1863179
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;

    invoke-static {p0}, LX/1f1;->a(LX/0QB;)LX/1f1;

    move-result-object v1

    check-cast v1, LX/1f1;

    invoke-static {p0}, LX/2Qx;->a(LX/0QB;)LX/2Qx;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object p0

    check-cast p0, LX/1Ad;

    iput-object v1, p1, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->h:LX/1f1;

    iput-object p0, p1, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->i:LX/1Ad;

    return-void
.end method


# virtual methods
.method public getPhotoRect()Landroid/graphics/Rect;
    .locals 5

    .prologue
    .line 1863180
    invoke-virtual {p0}, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLeft()I

    move-result v1

    add-int/2addr v0, v1

    .line 1863181
    invoke-virtual {p0}, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->getTop()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getTop()I

    move-result v2

    add-int/2addr v1, v2

    .line 1863182
    iget-object v2, p0, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getWidth()I

    move-result v2

    add-int/2addr v2, v0

    .line 1863183
    iget-object v3, p0, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getHeight()I

    move-result v3

    add-int/2addr v3, v1

    .line 1863184
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v4
.end method

.method public setPhoto(Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;)V
    .locals 5

    .prologue
    .line 1863185
    const/4 v1, 0x0

    .line 1863186
    iput-object p1, p0, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->e:Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;

    .line 1863187
    invoke-virtual {p0}, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b1d9e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    .line 1863188
    invoke-virtual {p0}, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {p0}, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b1d9e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    sub-int v2, v0, v2

    .line 1863189
    const/4 v0, 0x0

    .line 1863190
    iget-object v3, p1, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;->b:Lcom/facebook/ipc/media/MediaItem;

    move-object v3, v3

    .line 1863191
    if-eqz v3, :cond_3

    .line 1863192
    iget-object v0, p1, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;->b:Lcom/facebook/ipc/media/MediaItem;

    move-object v0, v0

    .line 1863193
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v1

    .line 1863194
    const/high16 v3, 0x3f800000    # 1.0f

    .line 1863195
    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/2Qx;->a(Ljava/lang/String;)LX/434;

    move-result-object v0

    .line 1863196
    iget v4, v0, LX/434;->b:I

    .line 1863197
    iget v0, v0, LX/434;->a:I

    .line 1863198
    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, LX/2Qx;->b(Ljava/lang/String;)I

    move-result p1

    .line 1863199
    if-lez v0, :cond_7

    .line 1863200
    int-to-float v4, v4

    int-to-float v0, v0

    div-float v0, v4, v0

    .line 1863201
    :goto_0
    const/16 v4, 0x5a

    if-eq p1, v4, :cond_0

    const/16 v4, 0x10e

    if-ne p1, v4, :cond_1

    .line 1863202
    :cond_0
    div-float v0, v3, v0

    .line 1863203
    :cond_1
    move v0, v0

    .line 1863204
    :cond_2
    :goto_1
    if-nez v1, :cond_6

    .line 1863205
    :goto_2
    return-void

    .line 1863206
    :cond_3
    iget-object v3, p1, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;->c:Lcom/facebook/graphql/model/GraphQLMedia;

    move-object v3, v3

    .line 1863207
    if-eqz v3, :cond_2

    .line 1863208
    iget-object v3, p1, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;->c:Lcom/facebook/graphql/model/GraphQLMedia;

    move-object v3, v3

    .line 1863209
    invoke-static {v3, v2}, LX/1f1;->a(Lcom/facebook/graphql/model/GraphQLMedia;I)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    .line 1863210
    if-eqz v3, :cond_2

    .line 1863211
    invoke-static {v3}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v1

    .line 1863212
    const/4 p1, 0x0

    .line 1863213
    invoke-static {v3}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLImage;)F

    move-result v0

    .line 1863214
    cmpg-float v4, v0, p1

    if-gtz v4, :cond_4

    .line 1863215
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v4

    if-lez v4, :cond_4

    .line 1863216
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v0, v4

    .line 1863217
    :cond_4
    cmpg-float v4, v0, p1

    if-gtz v4, :cond_5

    const/high16 v0, 0x3f800000    # 1.0f

    :cond_5
    move v0, v0

    .line 1863218
    goto :goto_1

    .line 1863219
    :cond_6
    iput-object v1, p0, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->g:Landroid/net/Uri;

    .line 1863220
    int-to-float v1, v2

    div-float v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 1863221
    iget-object v1, p0, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1863222
    iget-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1863223
    iget-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->i:LX/1Ad;

    iget-object v1, p0, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->g:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    sget-object v1, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1863224
    iget-object v1, p0, Lcom/facebook/goodwill/composer/photofragment/GoodwillPhotoView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    goto :goto_2

    :cond_7
    move v0, v3

    goto :goto_0
.end method
