.class public Lcom/facebook/goodwill/composer/photofragment/InterceptingRelativeLayout;
.super Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;
.source ""


# instance fields
.field public a:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1863233
    invoke-direct {p0, p1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1863234
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1863235
    invoke-direct {p0, p1, p2}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1863236
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1863230
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1863231
    return-void
.end method


# virtual methods
.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1863232
    iget-object v0, p0, Lcom/facebook/goodwill/composer/photofragment/InterceptingRelativeLayout;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x2

    const v0, 0x3df18957

    invoke-static {v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1863225
    iget-object v1, p0, Lcom/facebook/goodwill/composer/photofragment/InterceptingRelativeLayout;->a:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 1863226
    iget-object v1, p0, Lcom/facebook/goodwill/composer/photofragment/InterceptingRelativeLayout;->a:Landroid/view/View;

    invoke-virtual {v1, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1863227
    :cond_0
    const v1, 0x7e4cfb81

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v3
.end method

.method public setDispatchTarget(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1863228
    iput-object p1, p0, Lcom/facebook/goodwill/composer/photofragment/InterceptingRelativeLayout;->a:Landroid/view/View;

    .line 1863229
    return-void
.end method
