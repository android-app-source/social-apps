.class public Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/88f;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfigDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfigSerializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final mCampaignId:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "campaign_id"
    .end annotation
.end field

.field public final mMediaIds:Ljava/util/List;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "media_ids"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final mSource:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "source"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1863003
    const-class v0, Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfigDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1863021
    const-class v0, Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfigSerializer;

    return-object v0
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1863016
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1863017
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;->mCampaignId:J

    .line 1863018
    iput-object v2, p0, Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;->mSource:Ljava/lang/String;

    .line 1863019
    iput-object v2, p0, Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;->mMediaIds:Ljava/util/List;

    .line 1863020
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1863010
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1863011
    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;->mCampaignId:J

    .line 1863012
    iput-object p1, p0, Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;->mSource:Ljava/lang/String;

    .line 1863013
    iput-object p3, p0, Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;->mMediaIds:Ljava/util/List;

    .line 1863014
    invoke-virtual {p0}, Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;->a()V

    .line 1863015
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;
    .locals 2

    .prologue
    .line 1863009
    new-instance v0, Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1863005
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;->mSource:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1863006
    iget-wide v0, p0, Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;->mCampaignId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1863007
    return-void

    .line 1863008
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1863004
    const-string v0, "GoodwillFriendversaryCardComposerPluginConfig"

    return-object v0
.end method
