.class public final Lcom/facebook/goodwill/composer/GoodwillComposerActivity$UploadStatusCallback;
.super Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler$UploadStatusCallback;
.source ""


# instance fields
.field public final a:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/goodwill/composer/GoodwillComposerActivity$UploadStatusCallback;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic b:Lcom/facebook/goodwill/composer/GoodwillComposerActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/goodwill/composer/GoodwillComposerActivity;)V
    .locals 1

    .prologue
    .line 1862507
    iput-object p1, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity$UploadStatusCallback;->b:Lcom/facebook/goodwill/composer/GoodwillComposerActivity;

    .line 1862508
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler$UploadStatusCallback;-><init>(Landroid/os/Parcel;)V

    .line 1862509
    new-instance v0, LX/CFA;

    invoke-direct {v0, p0}, LX/CFA;-><init>(Lcom/facebook/goodwill/composer/GoodwillComposerActivity$UploadStatusCallback;)V

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity$UploadStatusCallback;->a:Landroid/os/Parcelable$Creator;

    .line 1862510
    return-void
.end method

.method public constructor <init>(Lcom/facebook/goodwill/composer/GoodwillComposerActivity;Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1862520
    iput-object p1, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity$UploadStatusCallback;->b:Lcom/facebook/goodwill/composer/GoodwillComposerActivity;

    .line 1862521
    invoke-direct {p0, p2}, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler$UploadStatusCallback;-><init>(Landroid/os/Parcel;)V

    .line 1862522
    new-instance v0, LX/CFA;

    invoke-direct {v0, p0}, LX/CFA;-><init>(Lcom/facebook/goodwill/composer/GoodwillComposerActivity$UploadStatusCallback;)V

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity$UploadStatusCallback;->a:Landroid/os/Parcelable$Creator;

    .line 1862523
    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/goodwill/composer/GoodwillComposerActivity;Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 1862519
    invoke-direct {p0, p1, p2}, Lcom/facebook/goodwill/composer/GoodwillComposerActivity$UploadStatusCallback;-><init>(Lcom/facebook/goodwill/composer/GoodwillComposerActivity;Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1862524
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity$UploadStatusCallback;->b:Lcom/facebook/goodwill/composer/GoodwillComposerActivity;

    iget-object v0, v0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->p:LX/1Cn;

    iget-object v1, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity$UploadStatusCallback;->b:Lcom/facebook/goodwill/composer/GoodwillComposerActivity;

    iget-object v1, v1, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->u:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    .line 1862525
    iget-object v2, v1, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1862526
    iget-object v2, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity$UploadStatusCallback;->b:Lcom/facebook/goodwill/composer/GoodwillComposerActivity;

    iget-object v2, v2, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->u:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    .line 1862527
    iget-object v3, v2, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->i:Ljava/lang/String;

    move-object v2, v3

    .line 1862528
    sget-object v3, LX/3lw;->GOODWILL_COMPOSER_POST_SUBMITTED:LX/3lw;

    invoke-virtual {v0, v1, v2, v3}, LX/1Cn;->a(Ljava/lang/String;Ljava/lang/String;LX/3lw;)V

    .line 1862529
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1862513
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity$UploadStatusCallback;->b:Lcom/facebook/goodwill/composer/GoodwillComposerActivity;

    iget-object v0, v0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->p:LX/1Cn;

    iget-object v1, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity$UploadStatusCallback;->b:Lcom/facebook/goodwill/composer/GoodwillComposerActivity;

    iget-object v1, v1, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->u:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    .line 1862514
    iget-object v2, v1, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1862515
    iget-object v2, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity$UploadStatusCallback;->b:Lcom/facebook/goodwill/composer/GoodwillComposerActivity;

    iget-object v2, v2, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->u:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    .line 1862516
    iget-object v3, v2, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->i:Ljava/lang/String;

    move-object v2, v3

    .line 1862517
    sget-object v3, LX/3lw;->GOODWILL_COMPOSER_POST_FAILED:LX/3lw;

    invoke-virtual {v0, v1, v2, v3}, LX/1Cn;->a(Ljava/lang/String;Ljava/lang/String;LX/3lw;)V

    .line 1862518
    return-void
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1862512
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 1862511
    return-void
.end method
