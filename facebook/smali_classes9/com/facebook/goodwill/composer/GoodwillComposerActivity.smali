.class public Lcom/facebook/goodwill/composer/GoodwillComposerActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/1Cn;

.field public q:LX/1Nq;

.field private r:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/goodwill/composer/GoodwillComposerFragment;",
            ">;"
        }
    .end annotation
.end field

.field private s:[Ljava/lang/String;

.field private t:Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;

.field public u:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

.field public v:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/CF9;",
            ">;"
        }
    .end annotation
.end field

.field public w:I

.field public x:Z

.field private y:Ljava/lang/String;

.field private z:LX/1Kf;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1862694
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1862695
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->v:Ljava/util/List;

    .line 1862696
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->w:I

    .line 1862697
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->x:Z

    .line 1862698
    return-void
.end method

.method private static a(Ljava/util/List;)LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1862685
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1862686
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;

    .line 1862687
    iget-object v1, v0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1862688
    if-nez v1, :cond_0

    .line 1862689
    iget-object v1, v0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;->b:Lcom/facebook/ipc/media/MediaItem;

    move-object v1, v1

    .line 1862690
    iget-wide v6, v1, Lcom/facebook/ipc/media/MediaItem;->e:J

    move-wide v4, v6

    .line 1862691
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 1862692
    :cond_0
    new-instance v4, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;

    invoke-virtual {v0}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;->d()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v1, v0}, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1862693
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/goodwill/composer/GoodwillComposerEvent;[Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1862680
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1862681
    const-string v1, "STEPS"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1862682
    const-string v1, "INPUT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1862683
    const-string v1, "OUTPUT"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1862684
    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1862676
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->r:Ljava/util/HashMap;

    .line 1862677
    new-instance v0, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;

    invoke-direct {v0}, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;-><init>()V

    .line 1862678
    iget-object v1, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->r:Ljava/util/HashMap;

    const-string v2, "photos"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1862679
    return-void
.end method

.method private a(LX/1Cn;LX/1Kf;Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;LX/1Nq;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1862671
    iput-object p2, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->z:LX/1Kf;

    .line 1862672
    iput-object p3, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->t:Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;

    .line 1862673
    iput-object p1, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->p:LX/1Cn;

    .line 1862674
    iput-object p4, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->q:LX/1Nq;

    .line 1862675
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;

    invoke-static {v3}, LX/1Cn;->a(LX/0QB;)LX/1Cn;

    move-result-object v0

    check-cast v0, LX/1Cn;

    invoke-static {v3}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v1

    check-cast v1, LX/1Kf;

    invoke-static {v3}, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->b(LX/0QB;)Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;

    move-result-object v2

    check-cast v2, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;

    invoke-static {v3}, LX/1Nq;->b(LX/0QB;)LX/1Nq;

    move-result-object v3

    check-cast v3, LX/1Nq;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->a(LX/1Cn;LX/1Kf;Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;LX/1Nq;)V

    return-void
.end method

.method private b()V
    .locals 5

    .prologue
    .line 1862665
    const/4 v0, 0x0

    .line 1862666
    iget-object v1, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->v:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CF9;

    .line 1862667
    iget-object v0, v0, LX/CF9;->a:Lcom/facebook/goodwill/composer/GoodwillComposerFragment;

    iget-object v3, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->u:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    new-instance v4, LX/CF8;

    invoke-direct {v4, p0, v1}, LX/CF8;-><init>(Lcom/facebook/goodwill/composer/GoodwillComposerActivity;I)V

    invoke-virtual {v0, v3, v4}, Lcom/facebook/goodwill/composer/GoodwillComposerFragment;->a(Lcom/facebook/goodwill/composer/GoodwillComposerEvent;LX/CF8;)V

    .line 1862668
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 1862669
    goto :goto_0

    .line 1862670
    :cond_0
    return-void
.end method

.method private c(Landroid/content/Intent;)V
    .locals 16

    .prologue
    .line 1862657
    const-string v1, "publishPostParams"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 1862658
    new-instance v6, Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->u:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    invoke-virtual {v1}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->d()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->u:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    invoke-virtual {v2}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->e()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->u:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    invoke-virtual {v4}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->f()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v6, v1, v2, v4}, Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1862659
    new-instance v7, Lcom/facebook/goodwill/composer/GoodwillComposerActivity$UploadStatusCallback;

    move-object/from16 v0, p0

    invoke-direct {v7, v0}, Lcom/facebook/goodwill/composer/GoodwillComposerActivity$UploadStatusCallback;-><init>(Lcom/facebook/goodwill/composer/GoodwillComposerActivity;)V

    .line 1862660
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->y:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 1862661
    :goto_1
    return-void

    .line 1862662
    :sswitch_0
    const-string v4, "mle"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :sswitch_1
    const-string v4, "friendversary_collage"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v4, "faceversary_collage"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    .line 1862663
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->t:Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->u:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    invoke-virtual {v2}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->i()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->u:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    invoke-virtual {v2}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->g()LX/0Px;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->a(Ljava/util/List;)LX/0Px;

    move-result-object v5

    move-object/from16 v2, p0

    invoke-virtual/range {v1 .. v7}, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->a(Landroid/content/Context;Lcom/facebook/composer/publish/common/PublishPostParams;Ljava/lang/String;LX/0Px;Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler$UploadStatusCallback;)V

    goto :goto_1

    .line 1862664
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->t:Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->u:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    invoke-virtual {v1}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->a()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->u:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    invoke-virtual {v1}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->i()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->u:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    invoke-virtual {v1}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->g()LX/0Px;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->a(Ljava/util/List;)LX/0Px;

    move-result-object v13

    move-object/from16 v9, p0

    move-object v12, v3

    move-object v14, v6

    move-object v15, v7

    invoke-virtual/range {v8 .. v15}, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/composer/publish/common/PublishPostParams;LX/0Px;Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler$UploadStatusCallback;)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x22f3bb71 -> :sswitch_2
        0x1a6a6 -> :sswitch_0
        0x1910e62e -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private l()V
    .locals 5

    .prologue
    .line 1862699
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->s:[Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 1862700
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->r:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->s:[Ljava/lang/String;

    aget-object v2, v2, v1

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/composer/GoodwillComposerFragment;

    .line 1862701
    if-eqz v0, :cond_0

    .line 1862702
    iget-object v2, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->v:Ljava/util/List;

    new-instance v3, LX/CF9;

    iget-object v4, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->s:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-direct {v3, v0, v4}, LX/CF9;-><init>(Lcom/facebook/goodwill/composer/GoodwillComposerFragment;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1862703
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1862704
    :cond_1
    return-void
.end method

.method public static m(Lcom/facebook/goodwill/composer/GoodwillComposerActivity;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1862642
    iget v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->w:I

    iget-object v3, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->v:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-lt v0, v3, :cond_0

    .line 1862643
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->w:I

    .line 1862644
    invoke-direct {p0}, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->o()V

    move v0, v1

    .line 1862645
    :goto_0
    return v0

    .line 1862646
    :cond_0
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->v:Ljava/util/List;

    iget v3, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->w:I

    add-int/lit8 v3, v3, 0x1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CF9;

    iget-boolean v0, v0, LX/CF9;->b:Z

    if-nez v0, :cond_1

    .line 1862647
    iput-boolean v2, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->x:Z

    move v0, v1

    .line 1862648
    goto :goto_0

    .line 1862649
    :cond_1
    iget v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->w:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->w:I

    .line 1862650
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->v:Ljava/util/List;

    iget v1, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->w:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CF9;

    .line 1862651
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    .line 1862652
    const v3, 0x7f0d002f

    iget-object v0, v0, LX/CF9;->a:Lcom/facebook/goodwill/composer/GoodwillComposerFragment;

    invoke-virtual {v1, v3, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 1862653
    iget v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->w:I

    const/4 v3, -0x1

    if-eq v0, v3, :cond_2

    .line 1862654
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    .line 1862655
    :cond_2
    invoke-virtual {v1}, LX/0hH;->b()I

    move v0, v2

    .line 1862656
    goto :goto_0
.end method

.method public static n(Lcom/facebook/goodwill/composer/GoodwillComposerActivity;)Z
    .locals 1

    .prologue
    .line 1862635
    iget v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->w:I

    if-nez v0, :cond_0

    .line 1862636
    invoke-static {p0}, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->q(Lcom/facebook/goodwill/composer/GoodwillComposerActivity;)V

    .line 1862637
    const/4 v0, 0x0

    .line 1862638
    :goto_0
    return v0

    .line 1862639
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->e()Z

    .line 1862640
    iget v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->w:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->w:I

    .line 1862641
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private o()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v1, 0x0

    .line 1862592
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->u:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    invoke-virtual {v0}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->g()LX/0Px;

    move-result-object v0

    .line 1862593
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1862594
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1862595
    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;

    invoke-virtual {v0}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;->d()Landroid/net/Uri;

    move-result-object v0

    .line 1862596
    :goto_0
    new-instance v3, LX/39x;

    invoke-direct {v3}, LX/39x;-><init>()V

    iget-object v4, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->u:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    .line 1862597
    iget-object v5, v4, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->c:Ljava/lang/String;

    move-object v4, v5

    .line 1862598
    iput-object v4, v3, LX/39x;->t:Ljava/lang/String;

    .line 1862599
    move-object v3, v3

    .line 1862600
    if-nez v0, :cond_0

    move-object v0, v1

    .line 1862601
    :goto_1
    iput-object v0, v3, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1862602
    move-object v0, v3

    .line 1862603
    invoke-virtual {v0}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 1862604
    invoke-static {}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->newBuilder()Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    sget-object v4, LX/2rt;->GOODWILL_CAMPAIGN:LX/2rt;

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setComposerType(LX/2rt;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v4

    sget-object v5, LX/21D;->NEWSFEED:LX/21D;

    invoke-virtual {v4, v5}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->setSourceSurface(LX/21D;)Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v4

    const-string v5, "goodwillComposer"

    invoke-virtual {v4, v5}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->setEntryPointName(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setLaunchLoggingParams(Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->q:LX/1Nq;

    iget-object v5, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->u:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    .line 1862605
    iget-object v6, v5, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->b:Ljava/lang/String;

    move-object v5, v6

    .line 1862606
    invoke-direct {p0}, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->p()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfig;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfig;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/1Nq;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsEditTagEnabled(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    const-string v4, "goodwill_composer"

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setNectarModule(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    invoke-virtual {v3, v8}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableFriendTagging(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    invoke-virtual {v3, v8}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableMentions(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->u:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    .line 1862607
    iget-object v5, v4, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->j:Ljava/util/List;

    move-object v4, v5

    .line 1862608
    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTaggedUsers(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->u:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    .line 1862609
    iget-object v5, v4, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->a:Ljava/lang/String;

    move-object v4, v5

    .line 1862610
    iget-object v5, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->u:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    .line 1862611
    iget v6, v5, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->h:I

    move v5, v6

    .line 1862612
    invoke-static {v4, v5}, LX/16z;->a(Ljava/lang/String;I)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v4

    invoke-static {v4}, LX/89G;->a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/89G;

    move-result-object v4

    .line 1862613
    iput-object v0, v4, LX/89G;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1862614
    move-object v0, v4

    .line 1862615
    iget-object v4, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->u:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    .line 1862616
    iget-object v5, v4, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->a:Ljava/lang/String;

    move-object v4, v5

    .line 1862617
    iput-object v4, v0, LX/89G;->d:Ljava/lang/String;

    .line 1862618
    move-object v0, v0

    .line 1862619
    invoke-virtual {v0}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialShareParams(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 1862620
    const-string v3, "extra_composer_configuration"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1862621
    iget-object v2, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->p:LX/1Cn;

    iget-object v3, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->u:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    .line 1862622
    iget-object v4, v3, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->a:Ljava/lang/String;

    move-object v3, v4

    .line 1862623
    iget-object v4, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->u:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    .line 1862624
    iget-object v5, v4, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->i:Ljava/lang/String;

    move-object v4, v5

    .line 1862625
    sget-object v5, LX/3lw;->GOODWILL_COMPOSER_FINAL_STEP:LX/3lw;

    invoke-virtual {v2, v3, v4, v5}, LX/1Cn;->a(Ljava/lang/String;Ljava/lang/String;LX/3lw;)V

    .line 1862626
    iget-object v2, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->z:LX/1Kf;

    const/16 v3, 0x3b2f

    invoke-interface {v2, v1, v0, v3, p0}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 1862627
    return-void

    .line 1862628
    :cond_0
    new-instance v4, LX/4XB;

    invoke-direct {v4}, LX/4XB;-><init>()V

    new-instance v5, LX/2dc;

    invoke-direct {v5}, LX/2dc;-><init>()V

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1862629
    iput-object v0, v5, LX/2dc;->h:Ljava/lang/String;

    .line 1862630
    move-object v0, v5

    .line 1862631
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 1862632
    iput-object v0, v4, LX/4XB;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1862633
    move-object v0, v4

    .line 1862634
    invoke-virtual {v0}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    goto/16 :goto_1

    :cond_1
    move-object v0, v1

    goto/16 :goto_0
.end method

.method private p()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1862586
    const-string v0, "mle"

    iget-object v1, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->y:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1862587
    invoke-virtual {p0}, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082a25

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1862588
    :goto_0
    return-object v0

    .line 1862589
    :cond_0
    const-string v0, "faceversary_collage"

    iget-object v1, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->y:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1862590
    invoke-virtual {p0}, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082a26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1862591
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static q(Lcom/facebook/goodwill/composer/GoodwillComposerActivity;)V
    .locals 4

    .prologue
    .line 1862579
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->p:LX/1Cn;

    iget-object v1, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->u:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    .line 1862580
    iget-object v2, v1, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1862581
    iget-object v2, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->u:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    .line 1862582
    iget-object v3, v2, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->i:Ljava/lang/String;

    move-object v2, v3

    .line 1862583
    sget-object v3, LX/3lw;->GOODWILL_COMPOSER_CANCEL:LX/3lw;

    invoke-virtual {v0, v1, v2, v3}, LX/1Cn;->a(Ljava/lang/String;Ljava/lang/String;LX/3lw;)V

    .line 1862584
    invoke-virtual {p0}, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->finish()V

    .line 1862585
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1862552
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1862553
    invoke-static {p0, p0}, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1862554
    invoke-direct {p0}, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->a()V

    .line 1862555
    const v0, 0x7f030a62

    invoke-virtual {p0, v0}, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->setContentView(I)V

    .line 1862556
    invoke-virtual {p0}, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "STEPS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->s:[Ljava/lang/String;

    .line 1862557
    invoke-virtual {p0}, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INPUT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->u:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    .line 1862558
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->u:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    if-nez v0, :cond_0

    .line 1862559
    new-instance v0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    invoke-direct {v0}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;-><init>()V

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->u:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    .line 1862560
    :cond_0
    invoke-direct {p0}, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->l()V

    .line 1862561
    if-eqz p1, :cond_1

    .line 1862562
    const-string v0, "CURSTEP"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->w:I

    .line 1862563
    const-string v0, "INPUT"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->u:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    .line 1862564
    :cond_1
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->p:LX/1Cn;

    iget-object v1, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->u:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    .line 1862565
    iget-object v2, v1, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1862566
    iget-object v2, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->u:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    .line 1862567
    iget-object v3, v2, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->i:Ljava/lang/String;

    move-object v2, v3

    .line 1862568
    sget-object v3, LX/3lw;->GOODWILL_COMPOSER_LAUNCH:LX/3lw;

    invoke-virtual {v0, v1, v2, v3}, LX/1Cn;->a(Ljava/lang/String;Ljava/lang/String;LX/3lw;)V

    .line 1862569
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->p:LX/1Cn;

    iget-object v1, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->u:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    .line 1862570
    iget-object v2, v1, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1862571
    iget-object v2, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->u:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    .line 1862572
    iget-object v3, v2, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->i:Ljava/lang/String;

    move-object v2, v3

    .line 1862573
    invoke-virtual {v0, v1, v2}, LX/1Cn;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1862574
    invoke-virtual {p0}, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "OUTPUT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->y:Ljava/lang/String;

    .line 1862575
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->y:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1862576
    const-string v0, "mle"

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->y:Ljava/lang/String;

    .line 1862577
    :cond_2
    invoke-direct {p0}, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->b()V

    .line 1862578
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1862545
    iget v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->w:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->w:I

    iget-object v1, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->v:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1862546
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->v:Ljava/util/List;

    iget v1, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->w:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CF9;

    iget-object v0, v0, LX/CF9;->a:Lcom/facebook/goodwill/composer/GoodwillComposerFragment;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1862547
    :goto_0
    return-void

    .line 1862548
    :cond_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    const/16 v0, 0x3b2f

    if-ne p1, v0, :cond_1

    .line 1862549
    invoke-direct {p0, p3}, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->c(Landroid/content/Intent;)V

    .line 1862550
    invoke-virtual {p0}, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->finish()V

    goto :goto_0

    .line 1862551
    :cond_1
    iget v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->w:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->w:I

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 1862539
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 1862540
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->v:Ljava/util/List;

    iget v1, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->w:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 1862541
    iget v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->w:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->w:I

    .line 1862542
    iget v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->w:I

    if-gez v0, :cond_0

    .line 1862543
    invoke-static {p0}, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->q(Lcom/facebook/goodwill/composer/GoodwillComposerActivity;)V

    .line 1862544
    :cond_0
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1862530
    iget v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->w:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->w:I

    iget-object v1, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->v:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1862531
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->v:Ljava/util/List;

    iget v1, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->w:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CF9;

    iget-object v0, v0, LX/CF9;->a:Lcom/facebook/goodwill/composer/GoodwillComposerFragment;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1862532
    :cond_0
    iget v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->w:I

    iget-object v1, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->v:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 1862533
    const-string v0, "CURSTEP"

    iget-object v1, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->v:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1862534
    :goto_0
    const-string v0, "INPUT"

    iget-object v1, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->u:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1862535
    const-string v0, "STEPS"

    iget-object v1, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->s:[Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1862536
    const-string v0, "OUTPUT"

    iget-object v1, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->y:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1862537
    return-void

    .line 1862538
    :cond_1
    const-string v0, "CURSTEP"

    iget v1, p0, Lcom/facebook/goodwill/composer/GoodwillComposerActivity;->w:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0
.end method
