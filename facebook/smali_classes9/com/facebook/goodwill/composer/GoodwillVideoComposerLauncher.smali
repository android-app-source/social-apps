.class public Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private C:LX/21D;

.field private D:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/1Nq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/1Kf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/1Cn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0Ot;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1863061
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(LX/21D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLX/0Px;Ljava/lang/String;LX/1Nq;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
    .locals 5
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/21D;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;",
            "Ljava/lang/String;",
            "LX/1Nq;",
            ")",
            "Lcom/facebook/ipc/composer/intent/ComposerConfiguration;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 1863062
    new-instance v0, LX/39x;

    invoke-direct {v0}, LX/39x;-><init>()V

    .line 1863063
    iput-object p2, v0, LX/39x;->t:Ljava/lang/String;

    .line 1863064
    move-object v0, v0

    .line 1863065
    new-instance v1, LX/4XB;

    invoke-direct {v1}, LX/4XB;-><init>()V

    new-instance v2, LX/2dc;

    invoke-direct {v2}, LX/2dc;-><init>()V

    .line 1863066
    iput-object p3, v2, LX/2dc;->h:Ljava/lang/String;

    .line 1863067
    move-object v2, v2

    .line 1863068
    invoke-virtual {v2}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 1863069
    iput-object v2, v1, LX/4XB;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1863070
    move-object v1, v1

    .line 1863071
    invoke-virtual {v1}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 1863072
    iput-object v1, v0, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1863073
    move-object v0, v0

    .line 1863074
    invoke-virtual {v0}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 1863075
    invoke-static {}, LX/89G;->a()LX/89G;

    move-result-object v1

    .line 1863076
    iput-object v0, v1, LX/89G;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1863077
    move-object v0, v1

    .line 1863078
    iput-object p1, v0, LX/89G;->d:Ljava/lang/String;

    .line 1863079
    move-object v0, v0

    .line 1863080
    invoke-virtual {v0}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    .line 1863081
    invoke-static {}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->newBuilder()Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    sget-object v2, LX/2rt;->GOODWILL_CAMPAIGN:LX/2rt;

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setComposerType(LX/2rt;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->setSourceSurface(LX/21D;)Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v2

    const-string v3, "goodwillVideoComposerLauncher"

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->setEntryPointName(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setLaunchLoggingParams(Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-static {p5, p8}, Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfig;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfig;

    move-result-object v2

    invoke-virtual {p9, v2}, LX/1Nq;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    sget-object v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, p6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsEditTagEnabled(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    const-string v2, "goodwill_composer"

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setNectarModule(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialShareParams(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    .line 1863082
    if-nez p6, :cond_0

    .line 1863083
    invoke-virtual {v0, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableFriendTagging(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableMentions(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 1863084
    :cond_0
    if-eqz p7, :cond_1

    .line 1863085
    invoke-virtual {v0, p7}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTaggedUsers(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 1863086
    :cond_1
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1863087
    new-instance v1, LX/173;

    invoke-direct {v1}, LX/173;-><init>()V

    .line 1863088
    iput-object p4, v1, LX/173;->f:Ljava/lang/String;

    .line 1863089
    move-object v1, v1

    .line 1863090
    invoke-virtual {v1}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialText(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 1863091
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;LX/1Nq;LX/1Kf;Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;LX/1Cn;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;",
            "LX/1Nq;",
            "LX/1Kf;",
            "Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;",
            "LX/1Cn;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1863092
    iput-object p1, p0, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->p:LX/1Nq;

    iput-object p2, p0, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->q:LX/1Kf;

    iput-object p3, p0, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->r:Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;

    iput-object p4, p0, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->s:LX/1Cn;

    iput-object p5, p0, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->t:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;

    invoke-static {v5}, LX/1Nq;->b(LX/0QB;)LX/1Nq;

    move-result-object v1

    check-cast v1, LX/1Nq;

    invoke-static {v5}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v2

    check-cast v2, LX/1Kf;

    invoke-static {v5}, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->b(LX/0QB;)Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;

    move-result-object v3

    check-cast v3, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;

    invoke-static {v5}, LX/1Cn;->a(LX/0QB;)LX/1Cn;

    move-result-object v4

    check-cast v4, LX/1Cn;

    const/16 v6, 0x12cb

    invoke-static {v5, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->a(Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;LX/1Nq;LX/1Kf;Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;LX/1Cn;LX/0Ot;)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 1863093
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->C:LX/21D;

    iget-object v1, p0, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->u:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->x:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->w:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->y:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->z:Ljava/lang/String;

    iget-object v7, p0, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->D:LX/0Px;

    iget-object v9, p0, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->p:LX/1Nq;

    invoke-static/range {v0 .. v9}, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->a(LX/21D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLX/0Px;Ljava/lang/String;LX/1Nq;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 1863094
    iget-object v1, p0, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->s:LX/1Cn;

    iget-object v2, p0, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->A:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->B:Ljava/lang/String;

    invoke-virtual {v1, p1, v2, v3, v8}, LX/1Cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1863095
    iget-object v1, p0, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->q:LX/1Kf;

    invoke-interface {v1, v8, v0, v6, p0}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 1863096
    return-void
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1863097
    if-nez p0, :cond_0

    .line 1863098
    const/4 v0, 0x0

    .line 1863099
    :goto_0
    return-object v0

    :cond_0
    const/16 v0, 0x2b

    const/16 v1, 0x20

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1863100
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1863101
    invoke-static {p0, p0}, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1863102
    invoke-virtual {p0}, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "campaign_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->u:Ljava/lang/String;

    .line 1863103
    invoke-virtual {p0}, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "campaign_type"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->v:Ljava/lang/String;

    .line 1863104
    invoke-virtual {p0}, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "share_preview"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->w:Ljava/lang/String;

    .line 1863105
    invoke-virtual {p0}, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "share_preview_title"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->x:Ljava/lang/String;

    .line 1863106
    invoke-virtual {p0}, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "default_share_message"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->y:Ljava/lang/String;

    .line 1863107
    invoke-virtual {p0}, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "placeholder_text"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->z:Ljava/lang/String;

    .line 1863108
    invoke-virtual {p0}, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "tagged_users"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 1863109
    if-eqz v0, :cond_1

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->D:LX/0Px;

    .line 1863110
    invoke-virtual {p0}, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "source"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->A:Ljava/lang/String;

    .line 1863111
    invoke-virtual {p0}, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "direct_source"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->B:Ljava/lang/String;

    .line 1863112
    invoke-virtual {p0}, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "composer_source_surface"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/21D;

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->C:LX/21D;

    .line 1863113
    if-nez p1, :cond_0

    .line 1863114
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->u:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->b(Ljava/lang/String;)V

    .line 1863115
    :cond_0
    return-void

    .line 1863116
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1863117
    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 20

    .prologue
    .line 1863118
    const/4 v3, -0x1

    move/from16 v0, p2

    if-ne v0, v3, :cond_0

    const/4 v3, 0x1

    move/from16 v0, p1

    if-ne v0, v3, :cond_0

    .line 1863119
    const-string v3, "publishPostParams"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 1863120
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->r:Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->t:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/user/model/User;

    invoke-virtual {v5}, Lcom/facebook/user/model/User;->c()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->v:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->u:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->A:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->B:Ljava/lang/String;

    const/4 v10, 0x0

    iget-object v11, v4, Lcom/facebook/composer/publish/common/PublishPostParams;->privacy:Ljava/lang/String;

    iget-object v12, v4, Lcom/facebook/composer/publish/common/PublishPostParams;->rawMessage:Ljava/lang/String;

    iget-object v13, v4, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    iget-object v14, v4, Lcom/facebook/composer/publish/common/PublishPostParams;->taggedIds:LX/0Px;

    const/4 v15, 0x0

    const/16 v16, 0x0

    new-instance v17, Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;

    const v4, 0x7f082a1f

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v18, 0x7f082a22

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->getString(I)Ljava/lang/String;

    move-result-object v18

    const v19, 0x7f082a23

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->getString(I)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v4, v1, v2}, Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/16 v18, 0x0

    move-object/from16 v4, p0

    invoke-virtual/range {v3 .. v18}, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/0Px;Ljava/lang/String;Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler$UploadStatusCallback;)V

    .line 1863121
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->finish()V

    .line 1863122
    return-void
.end method
