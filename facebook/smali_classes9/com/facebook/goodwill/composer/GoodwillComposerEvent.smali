.class public Lcom/facebook/goodwill/composer/GoodwillComposerEvent;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/goodwill/composer/GoodwillComposerEvent;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field public final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;",
            ">;"
        }
    .end annotation
.end field

.field public final h:I

.field public final i:Ljava/lang/String;

.field public final j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1862760
    new-instance v0, LX/CFB;

    invoke-direct {v0}, LX/CFB;-><init>()V

    sput-object v0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1862761
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1862762
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->k:Ljava/lang/String;

    .line 1862763
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->a:Ljava/lang/String;

    .line 1862764
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->c:Ljava/lang/String;

    .line 1862765
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->b:Ljava/lang/String;

    .line 1862766
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->d:Ljava/lang/String;

    .line 1862767
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->e:Ljava/lang/String;

    .line 1862768
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->f:Ljava/lang/String;

    .line 1862769
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->g:Ljava/util/List;

    .line 1862770
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->h:I

    .line 1862771
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->i:Ljava/lang/String;

    .line 1862772
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->j:Ljava/util/List;

    .line 1862773
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1862788
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1862789
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->k:Ljava/lang/String;

    .line 1862790
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->a:Ljava/lang/String;

    .line 1862791
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->c:Ljava/lang/String;

    .line 1862792
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->b:Ljava/lang/String;

    .line 1862793
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->d:Ljava/lang/String;

    .line 1862794
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->e:Ljava/lang/String;

    .line 1862795
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->f:Ljava/lang/String;

    .line 1862796
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->g:Ljava/util/List;

    .line 1862797
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->g:Ljava/util/List;

    sget-object v1, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 1862798
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->h:I

    .line 1862799
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->k:Ljava/lang/String;

    .line 1862800
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->i:Ljava/lang/String;

    .line 1862801
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->j:Ljava/util/List;

    .line 1862802
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->j:Ljava/util/List;

    sget-object v1, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 1862803
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1862774
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1862775
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->k:Ljava/lang/String;

    .line 1862776
    iput-object p1, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->a:Ljava/lang/String;

    .line 1862777
    iput-object p2, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->c:Ljava/lang/String;

    .line 1862778
    iput-object p3, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->b:Ljava/lang/String;

    .line 1862779
    iput-object p4, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->d:Ljava/lang/String;

    .line 1862780
    iput-object p5, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->e:Ljava/lang/String;

    .line 1862781
    iput-object p6, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->f:Ljava/lang/String;

    .line 1862782
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->g:Ljava/util/List;

    .line 1862783
    iput p7, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->h:I

    .line 1862784
    iput-object p8, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->i:Ljava/lang/String;

    .line 1862785
    iput-object p9, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->j:Ljava/util/List;

    .line 1862786
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1862787
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;)V
    .locals 1

    .prologue
    .line 1862756
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1862757
    return-void
.end method

.method public final b(Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;)V
    .locals 1

    .prologue
    .line 1862758
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1862759
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1862755
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1862754
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1862753
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1862752
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final g()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1862751
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->g:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1862750
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1862738
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1862739
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1862740
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1862741
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1862742
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1862743
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1862744
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->g:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1862745
    iget v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1862746
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1862747
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1862748
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->j:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1862749
    return-void
.end method
