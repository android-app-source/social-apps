.class public Lcom/facebook/goodwill/composer/ui/GoodwillComposerTitleBar;
.super Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;
.source ""


# instance fields
.field private a:Landroid/view/View;

.field private b:Lcom/facebook/resources/ui/FbTextView;

.field private c:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1863358
    invoke-direct {p0, p1, p2}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1863359
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 1863360
    const v1, 0x7f0307d2

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1863361
    const v1, 0x7f0d0508

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/goodwill/composer/ui/GoodwillComposerTitleBar;->a:Landroid/view/View;

    .line 1863362
    const v1, 0x7f0d14af

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/goodwill/composer/ui/GoodwillComposerTitleBar;->c:Landroid/view/View;

    .line 1863363
    const v1, 0x7f0d02c4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/goodwill/composer/ui/GoodwillComposerTitleBar;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1863364
    invoke-virtual {p0, v2}, Lcom/facebook/goodwill/composer/ui/GoodwillComposerTitleBar;->setClickable(Z)V

    .line 1863365
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1863366
    iget-object v0, p0, Lcom/facebook/goodwill/composer/ui/GoodwillComposerTitleBar;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1863367
    iget-object v0, p0, Lcom/facebook/goodwill/composer/ui/GoodwillComposerTitleBar;->c:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1863368
    return-void
.end method

.method public setTitle(I)V
    .locals 1

    .prologue
    .line 1863369
    iget-object v0, p0, Lcom/facebook/goodwill/composer/ui/GoodwillComposerTitleBar;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1863370
    return-void
.end method
