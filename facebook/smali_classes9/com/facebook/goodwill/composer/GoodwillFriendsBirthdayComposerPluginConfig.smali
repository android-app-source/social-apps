.class public final Lcom/facebook/goodwill/composer/GoodwillFriendsBirthdayComposerPluginConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/88f;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/goodwill/composer/GoodwillFriendsBirthdayComposerPluginConfigDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/goodwill/composer/GoodwillFriendsBirthdayComposerPluginConfigSerializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final mComposerHint:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "composer_hint"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1862881
    const-class v0, Lcom/facebook/goodwill/composer/GoodwillFriendsBirthdayComposerPluginConfigDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1862882
    const-class v0, Lcom/facebook/goodwill/composer/GoodwillFriendsBirthdayComposerPluginConfigSerializer;

    return-object v0
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1862883
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1862884
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillFriendsBirthdayComposerPluginConfig;->mComposerHint:Ljava/lang/String;

    .line 1862885
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1862886
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1862887
    iput-object p1, p0, Lcom/facebook/goodwill/composer/GoodwillFriendsBirthdayComposerPluginConfig;->mComposerHint:Ljava/lang/String;

    .line 1862888
    invoke-virtual {p0}, Lcom/facebook/goodwill/composer/GoodwillFriendsBirthdayComposerPluginConfig;->a()V

    .line 1862889
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/facebook/goodwill/composer/GoodwillFriendsBirthdayComposerPluginConfig;
    .locals 1

    .prologue
    .line 1862890
    new-instance v0, Lcom/facebook/goodwill/composer/GoodwillFriendsBirthdayComposerPluginConfig;

    invoke-direct {v0, p0}, Lcom/facebook/goodwill/composer/GoodwillFriendsBirthdayComposerPluginConfig;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1862891
    invoke-virtual {p0}, Lcom/facebook/goodwill/composer/GoodwillFriendsBirthdayComposerPluginConfig;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1862892
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1862893
    const-string v0, "GoodwillFriendsBirthdayComposerPluginConfig"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1862894
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillFriendsBirthdayComposerPluginConfig;->mComposerHint:Ljava/lang/String;

    return-object v0
.end method
