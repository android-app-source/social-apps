.class public Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfigSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfig;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1862465
    const-class v0, Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfig;

    new-instance v1, Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfigSerializer;

    invoke-direct {v1}, Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfigSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1862466
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1862460
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfig;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1862467
    if-nez p0, :cond_0

    .line 1862468
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1862469
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1862470
    invoke-static {p0, p1, p2}, Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfigSerializer;->b(Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfig;LX/0nX;LX/0my;)V

    .line 1862471
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1862472
    return-void
.end method

.method private static b(Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfig;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1862462
    const-string v0, "composer_hint"

    iget-object v1, p0, Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfig;->mComposerHint:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1862463
    const-string v0, "composer_title"

    iget-object v1, p0, Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfig;->mComposerTitle:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1862464
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1862461
    check-cast p1, Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfig;

    invoke-static {p1, p2, p3}, Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfigSerializer;->a(Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfig;LX/0nX;LX/0my;)V

    return-void
.end method
