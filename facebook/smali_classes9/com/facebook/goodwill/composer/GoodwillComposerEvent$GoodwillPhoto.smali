.class public final Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public final b:Lcom/facebook/ipc/media/MediaItem;

.field public final c:Lcom/facebook/graphql/model/GraphQLMedia;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1862711
    new-instance v0, LX/CFC;

    invoke-direct {v0}, LX/CFC;-><init>()V

    sput-object v0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1862712
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1862713
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;->a:Ljava/lang/String;

    .line 1862714
    const-class v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;->b:Lcom/facebook/ipc/media/MediaItem;

    .line 1862715
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;->c:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1862716
    return-void
.end method

.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLMedia;)V
    .locals 1

    .prologue
    .line 1862717
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1862718
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;->a:Ljava/lang/String;

    .line 1862719
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;->b:Lcom/facebook/ipc/media/MediaItem;

    .line 1862720
    iput-object p1, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;->c:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1862721
    return-void
.end method

.method public constructor <init>(Lcom/facebook/ipc/media/MediaItem;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1862722
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1862723
    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;->a:Ljava/lang/String;

    .line 1862724
    iput-object p1, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;->b:Lcom/facebook/ipc/media/MediaItem;

    .line 1862725
    iput-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;->c:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1862726
    return-void
.end method


# virtual methods
.method public final d()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1862727
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;->b:Lcom/facebook/ipc/media/MediaItem;

    if-eqz v0, :cond_0

    .line 1862728
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;->b:Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v0

    .line 1862729
    :goto_0
    return-object v0

    .line 1862730
    :cond_0
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;->c:Lcom/facebook/graphql/model/GraphQLMedia;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;->c:Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1862731
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;->c:Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {v0}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 1862732
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1862733
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1862734
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1862735
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;->b:Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1862736
    iget-object v0, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;->c:Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1862737
    return-void
.end method
