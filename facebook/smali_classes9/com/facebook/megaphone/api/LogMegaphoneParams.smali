.class public Lcom/facebook/megaphone/api/LogMegaphoneParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/megaphone/api/LogMegaphoneParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1751435
    new-instance v0, LX/B9G;

    invoke-direct {v0}, LX/B9G;-><init>()V

    sput-object v0, Lcom/facebook/megaphone/api/LogMegaphoneParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1751430
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1751431
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/megaphone/api/LogMegaphoneParams;->a:Ljava/lang/String;

    .line 1751432
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/megaphone/api/LogMegaphoneParams;->b:Ljava/lang/String;

    .line 1751433
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lcom/facebook/megaphone/api/LogMegaphoneParams;->c:Ljava/util/Map;

    .line 1751434
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1751436
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1751437
    iput-object p1, p0, Lcom/facebook/megaphone/api/LogMegaphoneParams;->a:Ljava/lang/String;

    .line 1751438
    iput-object p2, p0, Lcom/facebook/megaphone/api/LogMegaphoneParams;->b:Ljava/lang/String;

    .line 1751439
    iput-object p3, p0, Lcom/facebook/megaphone/api/LogMegaphoneParams;->c:Ljava/util/Map;

    .line 1751440
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1751429
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1751425
    iget-object v0, p0, Lcom/facebook/megaphone/api/LogMegaphoneParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1751426
    iget-object v0, p0, Lcom/facebook/megaphone/api/LogMegaphoneParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1751427
    iget-object v0, p0, Lcom/facebook/megaphone/api/LogMegaphoneParams;->c:Ljava/util/Map;

    invoke-static {v0}, LX/0PM;->a(Ljava/util/Map;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1751428
    return-void
.end method
