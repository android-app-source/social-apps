.class public Lcom/facebook/megaphone/fetcher/MegaphoneWithLayoutResult;
.super Lcom/facebook/fbservice/results/BaseResult;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/megaphone/fetcher/MegaphoneWithLayoutResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/graphql/model/GraphQLMegaphone;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1751485
    new-instance v0, LX/B9I;

    invoke-direct {v0}, LX/B9I;-><init>()V

    sput-object v0, Lcom/facebook/megaphone/fetcher/MegaphoneWithLayoutResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1751481
    invoke-direct {p0, p1}, Lcom/facebook/fbservice/results/BaseResult;-><init>(Landroid/os/Parcel;)V

    .line 1751482
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMegaphone;

    iput-object v0, p0, Lcom/facebook/megaphone/fetcher/MegaphoneWithLayoutResult;->a:Lcom/facebook/graphql/model/GraphQLMegaphone;

    .line 1751483
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/megaphone/fetcher/MegaphoneWithLayoutResult;->b:Ljava/lang/String;

    .line 1751484
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1751476
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1751477
    invoke-super {p0, p1, p2}, Lcom/facebook/fbservice/results/BaseResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1751478
    iget-object v0, p0, Lcom/facebook/megaphone/fetcher/MegaphoneWithLayoutResult;->a:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1751479
    iget-object v0, p0, Lcom/facebook/megaphone/fetcher/MegaphoneWithLayoutResult;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1751480
    return-void
.end method
