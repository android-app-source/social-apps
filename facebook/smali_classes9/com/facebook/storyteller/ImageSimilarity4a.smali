.class public Lcom/facebook/storyteller/ImageSimilarity4a;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/facebook/photos/imageprocessing/ImageDupeDetector;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1780237
    const-class v0, Lcom/facebook/storyteller/ImageSimilarity4a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/storyteller/ImageSimilarity4a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/imageprocessing/ImageDupeDetector;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1780238
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1780239
    iput-object p1, p0, Lcom/facebook/storyteller/ImageSimilarity4a;->b:Lcom/facebook/photos/imageprocessing/ImageDupeDetector;

    .line 1780240
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/storyteller/ImageSimilarity4a;
    .locals 5

    .prologue
    .line 1780241
    new-instance v1, Lcom/facebook/storyteller/ImageSimilarity4a;

    .line 1780242
    new-instance v4, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;

    invoke-static {p0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v0

    check-cast v0, LX/1HI;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, LX/0TD;

    invoke-static {p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v3

    check-cast v3, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-direct {v4, v0, v2, v3}, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;-><init>(LX/1HI;LX/0TD;Lcom/facebook/quicklog/QuickPerformanceLogger;)V

    .line 1780243
    move-object v0, v4

    .line 1780244
    check-cast v0, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;

    invoke-direct {v1, v0}, Lcom/facebook/storyteller/ImageSimilarity4a;-><init>(Lcom/facebook/photos/imageprocessing/ImageDupeDetector;)V

    .line 1780245
    return-object v1
.end method


# virtual methods
.method public similarityScore(Ljava/lang/String;DIZLjava/lang/String;DIZ)F
    .locals 11
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1780246
    iget-object v0, p0, Lcom/facebook/storyteller/ImageSimilarity4a;->b:Lcom/facebook/photos/imageprocessing/ImageDupeDetector;

    move-object v1, p1

    move-wide v2, p2

    move v4, p4

    move/from16 v5, p5

    move-object/from16 v6, p6

    move-wide/from16 v7, p7

    move/from16 v9, p9

    move/from16 v10, p10

    invoke-virtual/range {v0 .. v10}, Lcom/facebook/photos/imageprocessing/ImageDupeDetector;->similarityScore(Ljava/lang/String;DIZLjava/lang/String;DIZ)F

    move-result v0

    .line 1780247
    return v0
.end method
