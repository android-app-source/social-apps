.class public Lcom/facebook/storyteller/AssetProvider4a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/8I2;

.field private final c:LX/BOo;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0SG;

.field private final f:Ljava/lang/Object;

.field private g:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mIteratorLock"
    .end annotation
.end field

.field private h:LX/1FJ;

.field private i:Lcom/facebook/ipc/media/MediaItem;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1780072
    const-class v0, Lcom/facebook/storyteller/AssetProvider4a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/storyteller/AssetProvider4a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/8I2;LX/BOo;LX/0Ot;LX/0SG;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/local/LocalMediaCursor;",
            "LX/BOo;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1780073
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1780074
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/facebook/storyteller/AssetProvider4a;->f:Ljava/lang/Object;

    .line 1780075
    iput-object p1, p0, Lcom/facebook/storyteller/AssetProvider4a;->b:LX/8I2;

    .line 1780076
    invoke-virtual {p1}, LX/8I2;->a()LX/1FJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyteller/AssetProvider4a;->h:LX/1FJ;

    .line 1780077
    iput-object p2, p0, Lcom/facebook/storyteller/AssetProvider4a;->c:LX/BOo;

    .line 1780078
    iput-object p3, p0, Lcom/facebook/storyteller/AssetProvider4a;->d:LX/0Ot;

    .line 1780079
    iput-object p4, p0, Lcom/facebook/storyteller/AssetProvider4a;->e:LX/0SG;

    .line 1780080
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/storyteller/AssetProvider4a;
    .locals 5

    .prologue
    .line 1780081
    new-instance v3, Lcom/facebook/storyteller/AssetProvider4a;

    invoke-static {p0}, LX/8I3;->b(LX/0QB;)LX/8I2;

    move-result-object v0

    check-cast v0, LX/8I2;

    invoke-static {p0}, LX/BOo;->a(LX/0QB;)LX/BOo;

    move-result-object v1

    check-cast v1, LX/BOo;

    const/16 v2, 0x259

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-direct {v3, v0, v1, v4, v2}, Lcom/facebook/storyteller/AssetProvider4a;-><init>(LX/8I2;LX/BOo;LX/0Ot;LX/0SG;)V

    .line 1780082
    return-object v3
.end method

.method private b()V
    .locals 3

    .prologue
    .line 1780083
    iget-object v1, p0, Lcom/facebook/storyteller/AssetProvider4a;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 1780084
    :try_start_0
    iget-object v0, p0, Lcom/facebook/storyteller/AssetProvider4a;->g:Ljava/util/Iterator;

    if-eqz v0, :cond_0

    .line 1780085
    monitor-exit v1

    .line 1780086
    :goto_0
    return-void

    .line 1780087
    :cond_0
    iget-object v0, p0, Lcom/facebook/storyteller/AssetProvider4a;->b:LX/8I2;

    invoke-virtual {v0}, LX/8I2;->a()LX/1FJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyteller/AssetProvider4a;->h:LX/1FJ;

    .line 1780088
    invoke-virtual {p0}, Lcom/facebook/storyteller/AssetProvider4a;->a()Ljava/util/List;

    move-result-object v0

    .line 1780089
    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/facebook/storyteller/AssetProvider4a;->g:Ljava/util/Iterator;

    .line 1780090
    iget-object v0, p0, Lcom/facebook/storyteller/AssetProvider4a;->g:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/storyteller/AssetProvider4a;->g:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    :goto_2
    iput-object v0, p0, Lcom/facebook/storyteller/AssetProvider4a;->i:Lcom/facebook/ipc/media/MediaItem;

    .line 1780091
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1780092
    :cond_1
    :try_start_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    new-instance v2, LX/BOj;

    invoke-direct {v2, p0}, LX/BOj;-><init>(Lcom/facebook/storyteller/AssetProvider4a;)V

    invoke-static {v0, v2}, LX/0RZ;->b(Ljava/util/Iterator;LX/0Rl;)LX/0Rc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_1

    .line 1780093
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1780094
    iget-object v0, p0, Lcom/facebook/storyteller/AssetProvider4a;->b:LX/8I2;

    iget-object v1, p0, Lcom/facebook/storyteller/AssetProvider4a;->e:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    iget-object v1, p0, Lcom/facebook/storyteller/AssetProvider4a;->c:LX/BOo;

    invoke-virtual {v1}, LX/BOo;->b()J

    move-result-wide v4

    sub-long/2addr v2, v4

    iget-object v1, p0, Lcom/facebook/storyteller/AssetProvider4a;->c:LX/BOo;

    .line 1780095
    iget-object v4, v1, LX/BOo;->f:LX/0ad;

    sget v5, LX/1kO;->Q:I

    const/16 p0, 0x40

    invoke-interface {v4, v5, p0}, LX/0ad;->a(II)I

    move-result v4

    move v1, v4

    .line 1780096
    invoke-static {v0, v2, v3, v1}, LX/BOl;->a(LX/8I2;JI)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public advance()V
    .locals 2
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1780097
    invoke-direct {p0}, Lcom/facebook/storyteller/AssetProvider4a;->b()V

    .line 1780098
    iget-object v1, p0, Lcom/facebook/storyteller/AssetProvider4a;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 1780099
    :try_start_0
    iget-object v0, p0, Lcom/facebook/storyteller/AssetProvider4a;->g:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/storyteller/AssetProvider4a;->g:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    :goto_0
    iput-object v0, p0, Lcom/facebook/storyteller/AssetProvider4a;->i:Lcom/facebook/ipc/media/MediaItem;

    .line 1780100
    monitor-exit v1

    return-void

    .line 1780101
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1780102
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public close()V
    .locals 2
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mIteratorLock"
    .end annotation

    .prologue
    .line 1780103
    iget-object v1, p0, Lcom/facebook/storyteller/AssetProvider4a;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 1780104
    :try_start_0
    iget-object v0, p0, Lcom/facebook/storyteller/AssetProvider4a;->h:LX/1FJ;

    if-eqz v0, :cond_0

    .line 1780105
    iget-object v0, p0, Lcom/facebook/storyteller/AssetProvider4a;->h:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->close()V

    .line 1780106
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/storyteller/AssetProvider4a;->h:LX/1FJ;

    .line 1780107
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public hasNext()Z
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1780108
    invoke-direct {p0}, Lcom/facebook/storyteller/AssetProvider4a;->b()V

    .line 1780109
    iget-object v0, p0, Lcom/facebook/storyteller/AssetProvider4a;->i:Lcom/facebook/ipc/media/MediaItem;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public obtain()[B
    .locals 3
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1780110
    invoke-direct {p0}, Lcom/facebook/storyteller/AssetProvider4a;->b()V

    .line 1780111
    iget-object v0, p0, Lcom/facebook/storyteller/AssetProvider4a;->i:Lcom/facebook/ipc/media/MediaItem;

    if-nez v0, :cond_0

    .line 1780112
    iget-object v0, p0, Lcom/facebook/storyteller/AssetProvider4a;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, Lcom/facebook/storyteller/AssetProvider4a;->a:Ljava/lang/String;

    const-string v2, "obtain() called with null nextItem"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1780113
    const/4 v0, 0x0

    .line 1780114
    :goto_0
    return-object v0

    .line 1780115
    :cond_0
    new-instance v0, LX/0eX;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/0eX;-><init>(I)V

    .line 1780116
    iget-object v0, p0, Lcom/facebook/storyteller/AssetProvider4a;->i:Lcom/facebook/ipc/media/MediaItem;

    .line 1780117
    new-instance v1, LX/0eX;

    const/16 v2, 0x80

    invoke-direct {v1, v2}, LX/0eX;-><init>(I)V

    .line 1780118
    invoke-static {v1, v0}, LX/BOl;->a(LX/0eX;Lcom/facebook/ipc/media/MediaItem;)I

    move-result v2

    .line 1780119
    invoke-virtual {v1, v2}, LX/0eX;->c(I)V

    .line 1780120
    invoke-virtual {v1}, LX/0eX;->e()[B

    move-result-object v1

    move-object v0, v1

    .line 1780121
    goto :goto_0
.end method
