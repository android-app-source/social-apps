.class public final Lcom/facebook/storyteller/StoryTeller$StorytellerHybrid;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final mHybridData:Lcom/facebook/jni/HybridData;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1780253
    const-string v0, "storyteller4a"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 1780254
    return-void
.end method

.method public constructor <init>(Lcom/facebook/compactdisk/PersistentKeyValueStore;Lcom/facebook/xanalytics/XAnalyticsNative;)V
    .locals 1

    .prologue
    .line 1780250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1780251
    invoke-static {p1, p2}, Lcom/facebook/storyteller/StoryTeller$StorytellerHybrid;->initHybrid(Lcom/facebook/compactdisk/PersistentKeyValueStore;Lcom/facebook/xanalytics/XAnalyticsNative;)Lcom/facebook/jni/HybridData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/storyteller/StoryTeller$StorytellerHybrid;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 1780252
    return-void
.end method

.method private static native initHybrid(Lcom/facebook/compactdisk/PersistentKeyValueStore;Lcom/facebook/xanalytics/XAnalyticsNative;)Lcom/facebook/jni/HybridData;
.end method


# virtual methods
.method public native allClustersFromAssets([BLcom/facebook/storyteller/ImageSimilarity4a;[B)[[B
.end method

.method public native nextCluster(Lcom/facebook/storyteller/AssetProvider4a;Lcom/facebook/storyteller/ImageSimilarity4a;[B)[B
.end method
