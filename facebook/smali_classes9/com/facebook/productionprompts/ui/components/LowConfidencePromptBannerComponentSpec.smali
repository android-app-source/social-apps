.class public Lcom/facebook/productionprompts/ui/components/LowConfidencePromptBannerComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static c:LX/0Xm;


# instance fields
.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8yV;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1777650
    const-class v0, Lcom/facebook/productionprompts/ui/components/LowConfidencePromptBannerComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/productionprompts/ui/components/LowConfidencePromptBannerComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/8yV;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1777647
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1777648
    iput-object p1, p0, Lcom/facebook/productionprompts/ui/components/LowConfidencePromptBannerComponentSpec;->b:LX/0Ot;

    .line 1777649
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/productionprompts/ui/components/LowConfidencePromptBannerComponentSpec;
    .locals 4

    .prologue
    .line 1777636
    const-class v1, Lcom/facebook/productionprompts/ui/components/LowConfidencePromptBannerComponentSpec;

    monitor-enter v1

    .line 1777637
    :try_start_0
    sget-object v0, Lcom/facebook/productionprompts/ui/components/LowConfidencePromptBannerComponentSpec;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1777638
    sput-object v2, Lcom/facebook/productionprompts/ui/components/LowConfidencePromptBannerComponentSpec;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1777639
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1777640
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1777641
    new-instance v3, Lcom/facebook/productionprompts/ui/components/LowConfidencePromptBannerComponentSpec;

    const/16 p0, 0x1936

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, Lcom/facebook/productionprompts/ui/components/LowConfidencePromptBannerComponentSpec;-><init>(LX/0Ot;)V

    .line 1777642
    move-object v0, v3

    .line 1777643
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1777644
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/productionprompts/ui/components/LowConfidencePromptBannerComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1777645
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1777646
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
