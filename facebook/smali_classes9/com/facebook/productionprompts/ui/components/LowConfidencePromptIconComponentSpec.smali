.class public Lcom/facebook/productionprompts/ui/components/LowConfidencePromptIconComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static c:LX/0Xm;


# instance fields
.field public final b:LX/1nu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1nu",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1777857
    const-class v0, Lcom/facebook/productionprompts/ui/components/LowConfidencePromptIconComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/productionprompts/ui/components/LowConfidencePromptIconComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1777858
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1777859
    iput-object p1, p0, Lcom/facebook/productionprompts/ui/components/LowConfidencePromptIconComponentSpec;->b:LX/1nu;

    .line 1777860
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/productionprompts/ui/components/LowConfidencePromptIconComponentSpec;
    .locals 4

    .prologue
    .line 1777861
    const-class v1, Lcom/facebook/productionprompts/ui/components/LowConfidencePromptIconComponentSpec;

    monitor-enter v1

    .line 1777862
    :try_start_0
    sget-object v0, Lcom/facebook/productionprompts/ui/components/LowConfidencePromptIconComponentSpec;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1777863
    sput-object v2, Lcom/facebook/productionprompts/ui/components/LowConfidencePromptIconComponentSpec;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1777864
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1777865
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1777866
    new-instance p0, Lcom/facebook/productionprompts/ui/components/LowConfidencePromptIconComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v3

    check-cast v3, LX/1nu;

    invoke-direct {p0, v3}, Lcom/facebook/productionprompts/ui/components/LowConfidencePromptIconComponentSpec;-><init>(LX/1nu;)V

    .line 1777867
    move-object v0, p0

    .line 1777868
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1777869
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/productionprompts/ui/components/LowConfidencePromptIconComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1777870
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1777871
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
