.class public Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/88f;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfigDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfigSerializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final mIsPrefilled:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_prefilled"
    .end annotation
.end field

.field public final mPrompt:Lcom/facebook/productionprompts/model/ProductionPrompt;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "prompt"
    .end annotation
.end field

.field public final mPromptAnalytics:Lcom/facebook/productionprompts/logging/PromptAnalytics;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "prompt_entry_point_analytics"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mPromptMediaPath:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "prompt_media_path"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1777509
    const-class v0, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfigDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1777512
    const-class v0, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfigSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1777513
    const-class v0, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1777514
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1777515
    iput-object v1, p0, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;->mPrompt:Lcom/facebook/productionprompts/model/ProductionPrompt;

    .line 1777516
    iput-object v1, p0, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;->mPromptAnalytics:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 1777517
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;->mIsPrefilled:Z

    .line 1777518
    iput-object v1, p0, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;->mPromptMediaPath:Ljava/lang/String;

    .line 1777519
    return-void
.end method

.method private constructor <init>(Lcom/facebook/productionprompts/model/ProductionPrompt;Lcom/facebook/productionprompts/logging/PromptAnalytics;ZLjava/lang/String;)V
    .locals 0
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1777520
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1777521
    iput-object p1, p0, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;->mPrompt:Lcom/facebook/productionprompts/model/ProductionPrompt;

    .line 1777522
    iput-object p2, p0, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;->mPromptAnalytics:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 1777523
    iput-boolean p3, p0, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;->mIsPrefilled:Z

    .line 1777524
    iput-object p4, p0, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;->mPromptMediaPath:Ljava/lang/String;

    .line 1777525
    invoke-virtual {p0}, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;->a()V

    .line 1777526
    return-void
.end method

.method public static a(Lcom/facebook/productionprompts/model/ProductionPrompt;Lcom/facebook/productionprompts/logging/PromptAnalytics;Z)Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;
    .locals 1

    .prologue
    .line 1777510
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;->a(Lcom/facebook/productionprompts/model/ProductionPrompt;Lcom/facebook/productionprompts/logging/PromptAnalytics;ZLjava/lang/String;)Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/productionprompts/model/ProductionPrompt;Lcom/facebook/productionprompts/logging/PromptAnalytics;ZLjava/lang/String;)Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;
    .locals 1
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1777511
    new-instance v0, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;-><init>(Lcom/facebook/productionprompts/model/ProductionPrompt;Lcom/facebook/productionprompts/logging/PromptAnalytics;ZLjava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1777503
    invoke-virtual {p0}, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;->c()Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1777504
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1777505
    sget-object v0, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Lcom/facebook/productionprompts/model/ProductionPrompt;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1777506
    iget-object v0, p0, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;->mPrompt:Lcom/facebook/productionprompts/model/ProductionPrompt;

    return-object v0
.end method

.method public final d()Lcom/facebook/productionprompts/logging/PromptAnalytics;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1777507
    iget-object v0, p0, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;->mPromptAnalytics:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1777508
    iget-object v0, p0, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;->mPromptMediaPath:Ljava/lang/String;

    return-object v0
.end method
