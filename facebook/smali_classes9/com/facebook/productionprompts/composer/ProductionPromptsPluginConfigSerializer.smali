.class public Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfigSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1777551
    const-class v0, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;

    new-instance v1, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfigSerializer;

    invoke-direct {v1}, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfigSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1777552
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1777553
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1777554
    if-nez p0, :cond_0

    .line 1777555
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1777556
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1777557
    invoke-static {p0, p1, p2}, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfigSerializer;->b(Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;LX/0nX;LX/0my;)V

    .line 1777558
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1777559
    return-void
.end method

.method private static b(Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1777560
    const-string v0, "prompt"

    iget-object v1, p0, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;->mPrompt:Lcom/facebook/productionprompts/model/ProductionPrompt;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1777561
    const-string v0, "prompt_entry_point_analytics"

    iget-object v1, p0, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;->mPromptAnalytics:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1777562
    const-string v0, "is_prefilled"

    iget-boolean v1, p0, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;->mIsPrefilled:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1777563
    const-string v0, "prompt_media_path"

    iget-object v1, p0, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;->mPromptMediaPath:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1777564
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1777565
    check-cast p1, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;

    invoke-static {p1, p2, p3}, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfigSerializer;->a(Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;LX/0nX;LX/0my;)V

    return-void
.end method
