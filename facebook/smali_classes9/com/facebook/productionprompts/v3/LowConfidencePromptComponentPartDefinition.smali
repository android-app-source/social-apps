.class public Lcom/facebook/productionprompts/v3/LowConfidencePromptComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/1Ri;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final d:LX/BMf;

.field private final e:LX/1V0;

.field private final f:Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;

.field private final g:LX/1E1;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/BMf;LX/1V0;Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;LX/1E1;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1778005
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1778006
    iput-object p2, p0, Lcom/facebook/productionprompts/v3/LowConfidencePromptComponentPartDefinition;->d:LX/BMf;

    .line 1778007
    iput-object p3, p0, Lcom/facebook/productionprompts/v3/LowConfidencePromptComponentPartDefinition;->e:LX/1V0;

    .line 1778008
    iput-object p4, p0, Lcom/facebook/productionprompts/v3/LowConfidencePromptComponentPartDefinition;->f:Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;

    .line 1778009
    iput-object p5, p0, Lcom/facebook/productionprompts/v3/LowConfidencePromptComponentPartDefinition;->g:LX/1E1;

    .line 1778010
    return-void
.end method

.method private a(LX/1De;LX/1Ri;LX/1Pr;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1Ri;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1777966
    new-instance v1, LX/1X6;

    sget-object v0, LX/1Ua;->h:LX/1Ua;

    invoke-direct {v1, v0}, LX/1X6;-><init>(LX/1Ua;)V

    .line 1777967
    iget-object v0, p0, Lcom/facebook/productionprompts/v3/LowConfidencePromptComponentPartDefinition;->d:LX/BMf;

    const/4 v2, 0x0

    .line 1777968
    new-instance v3, LX/BMe;

    invoke-direct {v3, v0}, LX/BMe;-><init>(LX/BMf;)V

    .line 1777969
    iget-object v4, v0, LX/BMf;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/BMd;

    .line 1777970
    if-nez v4, :cond_0

    .line 1777971
    new-instance v4, LX/BMd;

    invoke-direct {v4, v0}, LX/BMd;-><init>(LX/BMf;)V

    .line 1777972
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/BMd;->a$redex0(LX/BMd;LX/1De;IILX/BMe;)V

    .line 1777973
    move-object v3, v4

    .line 1777974
    move-object v2, v3

    .line 1777975
    move-object v0, v2

    .line 1777976
    iget-object v2, v0, LX/BMd;->a:LX/BMe;

    iput-object p2, v2, LX/BMe;->a:LX/1Ri;

    .line 1777977
    iget-object v2, v0, LX/BMd;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1777978
    move-object v2, v0

    .line 1777979
    move-object v0, p3

    check-cast v0, LX/1Pp;

    .line 1777980
    iget-object v3, v2, LX/BMd;->a:LX/BMe;

    iput-object v0, v3, LX/BMe;->b:LX/1Pp;

    .line 1777981
    iget-object v3, v2, LX/BMd;->e:Ljava/util/BitSet;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 1777982
    move-object v0, v2

    .line 1777983
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 1777984
    iget-object v2, p0, Lcom/facebook/productionprompts/v3/LowConfidencePromptComponentPartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/1aD;LX/1Ri;LX/1Pr;)LX/1dV;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;",
            "LX/1Ri;",
            "TE;)",
            "LX/1dV;"
        }
    .end annotation

    .prologue
    .line 1778003
    iget-object v0, p0, Lcom/facebook/productionprompts/v3/LowConfidencePromptComponentPartDefinition;->f:Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;

    new-instance v1, LX/24N;

    iget-object v2, p2, LX/1Ri;->c:LX/AkL;

    iget-object v3, p2, LX/1Ri;->a:LX/1RN;

    iget-object v4, p2, LX/1Ri;->b:LX/0jW;

    invoke-direct {v1, v2, v3, p3, v4}, LX/24N;-><init>(LX/AkL;LX/1RN;LX/1Pr;LX/0jW;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1778004
    check-cast p3, LX/1Pn;

    invoke-super {p0, p1, p2, p3}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(LX/1aD;Ljava/lang/Object;LX/1Pn;)LX/1dV;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/productionprompts/v3/LowConfidencePromptComponentPartDefinition;
    .locals 9

    .prologue
    .line 1777992
    const-class v1, Lcom/facebook/productionprompts/v3/LowConfidencePromptComponentPartDefinition;

    monitor-enter v1

    .line 1777993
    :try_start_0
    sget-object v0, Lcom/facebook/productionprompts/v3/LowConfidencePromptComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1777994
    sput-object v2, Lcom/facebook/productionprompts/v3/LowConfidencePromptComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1777995
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1777996
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1777997
    new-instance v3, Lcom/facebook/productionprompts/v3/LowConfidencePromptComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/BMf;->a(LX/0QB;)LX/BMf;

    move-result-object v5

    check-cast v5, LX/BMf;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v6

    check-cast v6, LX/1V0;

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;

    invoke-static {v0}, LX/1E1;->b(LX/0QB;)LX/1E1;

    move-result-object v8

    check-cast v8, LX/1E1;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/productionprompts/v3/LowConfidencePromptComponentPartDefinition;-><init>(Landroid/content/Context;LX/BMf;LX/1V0;Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;LX/1E1;)V

    .line 1777998
    move-object v0, v3

    .line 1777999
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1778000
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/productionprompts/v3/LowConfidencePromptComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1778001
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1778002
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1777991
    check-cast p2, LX/1Ri;

    check-cast p3, LX/1Pr;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/productionprompts/v3/LowConfidencePromptComponentPartDefinition;->a(LX/1De;LX/1Ri;LX/1Pr;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1777990
    check-cast p2, LX/1Ri;

    check-cast p3, LX/1Pr;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/productionprompts/v3/LowConfidencePromptComponentPartDefinition;->a(LX/1De;LX/1Ri;LX/1Pr;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1Pn;)LX/1dV;
    .locals 1

    .prologue
    .line 1777989
    check-cast p2, LX/1Ri;

    check-cast p3, LX/1Pr;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/productionprompts/v3/LowConfidencePromptComponentPartDefinition;->a(LX/1aD;LX/1Ri;LX/1Pr;)LX/1dV;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1777988
    check-cast p2, LX/1Ri;

    check-cast p3, LX/1Pr;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/productionprompts/v3/LowConfidencePromptComponentPartDefinition;->a(LX/1aD;LX/1Ri;LX/1Pr;)LX/1dV;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1777985
    iget-object v0, p0, Lcom/facebook/productionprompts/v3/LowConfidencePromptComponentPartDefinition;->g:LX/1E1;

    .line 1777986
    iget-object v1, v0, LX/1E1;->a:LX/0ad;

    sget-short p0, LX/1Nu;->a:S

    const/4 p1, 0x0

    invoke-interface {v1, p0, p1}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 1777987
    return v0
.end method
