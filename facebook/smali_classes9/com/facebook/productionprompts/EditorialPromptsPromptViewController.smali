.class public Lcom/facebook/productionprompts/EditorialPromptsPromptViewController;
.super LX/Aqa;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static final b:LX/BMB;


# instance fields
.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BMP;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/B5l;

.field private final e:LX/BMA;

.field private final f:LX/BMC;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1776856
    const-class v0, LX/1RQ;

    const-string v1, "inline_composer"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/productionprompts/EditorialPromptsPromptViewController;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1776857
    new-instance v0, LX/BMB;

    invoke-direct {v0}, LX/BMB;-><init>()V

    sput-object v0, Lcom/facebook/productionprompts/EditorialPromptsPromptViewController;->b:LX/BMB;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/B5l;LX/BMC;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/BMP;",
            ">;",
            "LX/B5l;",
            "LX/BMC;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1776850
    invoke-direct {p0}, LX/Aqa;-><init>()V

    .line 1776851
    iput-object p1, p0, Lcom/facebook/productionprompts/EditorialPromptsPromptViewController;->c:LX/0Ot;

    .line 1776852
    iput-object p2, p0, Lcom/facebook/productionprompts/EditorialPromptsPromptViewController;->d:LX/B5l;

    .line 1776853
    new-instance v0, LX/BMA;

    invoke-direct {v0, p0}, LX/BMA;-><init>(Lcom/facebook/productionprompts/EditorialPromptsPromptViewController;)V

    iput-object v0, p0, Lcom/facebook/productionprompts/EditorialPromptsPromptViewController;->e:LX/BMA;

    .line 1776854
    iput-object p3, p0, Lcom/facebook/productionprompts/EditorialPromptsPromptViewController;->f:LX/BMC;

    .line 1776855
    return-void
.end method

.method public static a(LX/BM7;)LX/AlW;
    .locals 1

    .prologue
    .line 1776834
    sget-object v0, Lcom/facebook/productionprompts/EditorialPromptsPromptViewController;->b:LX/BMB;

    .line 1776835
    iput-object p0, v0, LX/BMB;->a:LX/BM7;

    .line 1776836
    sget-object v0, Lcom/facebook/productionprompts/EditorialPromptsPromptViewController;->b:LX/BMB;

    return-object v0
.end method


# virtual methods
.method public a(LX/1RN;Landroid/content/Context;)LX/AlW;
    .locals 2

    .prologue
    .line 1776858
    iget-object v0, p1, LX/1RN;->a:LX/1kK;

    check-cast v0, LX/1kW;

    .line 1776859
    iget-object v1, p0, Lcom/facebook/productionprompts/EditorialPromptsPromptViewController;->f:LX/BMC;

    .line 1776860
    iget-object p0, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, p0

    .line 1776861
    new-instance p2, LX/BM7;

    invoke-static {v1}, LX/1bQ;->b(LX/0QB;)LX/1bQ;

    move-result-object p0

    check-cast p0, LX/1bQ;

    invoke-static {v1}, LX/1E1;->b(LX/0QB;)LX/1E1;

    move-result-object p1

    check-cast p1, LX/1E1;

    invoke-direct {p2, v0, p0, p1}, LX/BM7;-><init>(Lcom/facebook/productionprompts/model/ProductionPrompt;LX/1bQ;LX/1E1;)V

    .line 1776862
    move-object v0, p2

    .line 1776863
    invoke-static {v0}, Lcom/facebook/productionprompts/EditorialPromptsPromptViewController;->a(LX/BM7;)LX/AlW;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/2xq;LX/1RN;)V
    .locals 2
    .param p1    # LX/2xq;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1776844
    instance-of v0, p1, LX/2xq;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1776845
    check-cast p1, LX/2xq;

    .line 1776846
    iget-object v0, p0, Lcom/facebook/productionprompts/EditorialPromptsPromptViewController;->e:LX/BMA;

    .line 1776847
    iput-object p2, v0, LX/BMA;->b:LX/1RN;

    .line 1776848
    iget-object v0, p1, LX/2xq;->a:Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/productionprompts/EditorialPromptsPromptViewController;->e:LX/BMA;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1776849
    return-void
.end method

.method public final b(LX/2xq;LX/1RN;)V
    .locals 2
    .param p1    # LX/2xq;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1776838
    instance-of v0, p1, LX/2xq;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1776839
    check-cast p1, LX/2xq;

    .line 1776840
    iget-object v0, p0, Lcom/facebook/productionprompts/EditorialPromptsPromptViewController;->e:LX/BMA;

    .line 1776841
    iput-object v1, v0, LX/BMA;->b:LX/1RN;

    .line 1776842
    iget-object v0, p1, LX/2xq;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1776843
    return-void
.end method

.method public e(LX/1RN;)Z
    .locals 1

    .prologue
    .line 1776837
    const/4 v0, 0x1

    return v0
.end method
