.class public Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:LX/1Uo;

.field public final c:LX/1Ad;

.field public final d:LX/1o9;

.field public final e:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1777347
    const-class v0, Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1Uo;LX/1Ad;Landroid/content/res/Resources;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1777348
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1777349
    iput-object p1, p0, Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;->b:LX/1Uo;

    .line 1777350
    iput-object p2, p0, Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;->c:LX/1Ad;

    .line 1777351
    iput-object p3, p0, Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;->e:Landroid/content/res/Resources;

    .line 1777352
    const v0, 0x7f0b116c

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1777353
    new-instance v1, LX/1o9;

    invoke-direct {v1, v0, v0}, LX/1o9;-><init>(II)V

    iput-object v1, p0, Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;->d:LX/1o9;

    .line 1777354
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;
    .locals 4

    .prologue
    .line 1777355
    new-instance v3, Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;

    invoke-static {p0}, LX/1qZ;->b(LX/0QB;)LX/1Uo;

    move-result-object v0

    check-cast v0, LX/1Uo;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    invoke-direct {v3, v0, v1, v2}, Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;-><init>(LX/1Uo;LX/1Ad;Landroid/content/res/Resources;)V

    .line 1777356
    return-object v3
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Landroid/content/Context;)LX/1aX;
    .locals 4

    .prologue
    .line 1777357
    iget-object v0, p0, Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;->c:LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->o()LX/1Ad;

    .line 1777358
    iget-object v0, p0, Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;->b:LX/1Uo;

    sget-object v1, LX/1Up;->c:LX/1Up;

    invoke-virtual {v0, v1}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v0

    const/4 v1, 0x0

    .line 1777359
    iput v1, v0, LX/1Uo;->d:I

    .line 1777360
    move-object v0, v0

    .line 1777361
    new-instance v1, LX/4Ab;

    invoke-direct {v1}, LX/4Ab;-><init>()V

    iget-object v2, p0, Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;->e:Landroid/content/res/Resources;

    const v3, 0x7f0b1175

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, LX/4Ab;->a(F)LX/4Ab;

    move-result-object v1

    .line 1777362
    iput-object v1, v0, LX/1Uo;->u:LX/4Ab;

    .line 1777363
    move-object v0, v0

    .line 1777364
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    move-object v0, v0

    .line 1777365
    iget-object v1, p0, Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;->c:LX/1Ad;

    invoke-virtual {v1}, LX/1Ad;->o()LX/1Ad;

    .line 1777366
    iget-object v1, p0, Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;->c:LX/1Ad;

    sget-object v2, Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-static {p1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/1bX;->a(Z)LX/1bX;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/productionprompts/common/v3/V3PromptsDraweeHelper;->d:LX/1o9;

    .line 1777367
    iput-object v3, v2, LX/1bX;->c:LX/1o9;

    .line 1777368
    move-object v2, v2

    .line 1777369
    invoke-virtual {v2}, LX/1bX;->n()LX/1bf;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    move-object v1, v1

    .line 1777370
    invoke-static {v0, p2}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v0

    .line 1777371
    invoke-virtual {v0, v1}, LX/1aX;->a(LX/1aZ;)V

    .line 1777372
    return-object v0
.end method
