.class public Lcom/facebook/productionprompts/common/v3/OverlappingImageView;
.super Landroid/view/View;
.source ""


# static fields
.field private static final a:Landroid/graphics/DrawFilter;


# instance fields
.field private final b:LX/4Ac;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4Ac",
            "<",
            "LX/1af;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/Alv;

.field private d:LX/Alv;

.field private e:I

.field private f:D

.field private g:D

.field private h:Landroid/graphics/Paint;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1777299
    new-instance v0, Landroid/graphics/PaintFlagsDrawFilter;

    const/4 v1, 0x0

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Landroid/graphics/PaintFlagsDrawFilter;-><init>(II)V

    sput-object v0, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->a:Landroid/graphics/DrawFilter;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1777300
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1777301
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1777302
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1777303
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1777322
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1777323
    new-instance v0, LX/4Ac;

    invoke-direct {v0}, LX/4Ac;-><init>()V

    iput-object v0, p0, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->b:LX/4Ac;

    .line 1777324
    return-void
.end method

.method private a(LX/Alv;)V
    .locals 5

    .prologue
    .line 1777304
    if-eqz p1, :cond_0

    .line 1777305
    invoke-interface {p1}, LX/Alv;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aX;

    .line 1777306
    iget-object v4, p0, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->b:LX/4Ac;

    invoke-virtual {v4, v0}, LX/4Ac;->a(LX/1aX;)V

    .line 1777307
    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1777308
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1777309
    :cond_0
    return-void
.end method

.method private a(LX/Alv;Landroid/graphics/Canvas;IFFII)V
    .locals 6
    .param p1    # LX/Alv;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1777273
    invoke-virtual {p2}, Landroid/graphics/Canvas;->save()I

    .line 1777274
    int-to-float v0, p3

    invoke-virtual {p2, v0}, Landroid/graphics/Canvas;->rotate(F)V

    .line 1777275
    invoke-virtual {p2, p4, p5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1777276
    if-eqz p1, :cond_0

    .line 1777277
    invoke-virtual {p0}, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {p0}, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-interface {p1, p2, v0, v2}, LX/Alv;->a(Landroid/graphics/Canvas;II)V

    .line 1777278
    :cond_0
    iget-object v0, p0, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->h:Landroid/graphics/Paint;

    if-eqz v0, :cond_1

    .line 1777279
    int-to-float v3, p6

    int-to-float v4, p7

    iget-object v5, p0, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->h:Landroid/graphics/Paint;

    move-object v0, p2

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1777280
    :cond_1
    invoke-virtual {p2}, Landroid/graphics/Canvas;->restore()V

    .line 1777281
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 2

    .prologue
    .line 1777310
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->h:Landroid/graphics/Paint;

    .line 1777311
    iget-object v0, p0, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->h:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1777312
    iget-object v0, p0, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->h:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1777313
    iget-object v0, p0, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->h:Landroid/graphics/Paint;

    int-to-float v1, p2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1777314
    return-void
.end method

.method public final a(LX/Alv;LX/Alv;)V
    .locals 1

    .prologue
    .line 1777315
    iput-object p1, p0, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->c:LX/Alv;

    .line 1777316
    iput-object p2, p0, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->d:LX/Alv;

    .line 1777317
    iget-object v0, p0, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->b:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->c()V

    .line 1777318
    invoke-direct {p0, p1}, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->a(LX/Alv;)V

    .line 1777319
    invoke-direct {p0, p2}, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->a(LX/Alv;)V

    .line 1777320
    invoke-virtual {p0}, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->invalidate()V

    .line 1777321
    return-void
.end method

.method public final invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 1777294
    invoke-virtual {p0}, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->invalidate()V

    .line 1777295
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0xf75735c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1777296
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 1777297
    iget-object v1, p0, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->b:LX/4Ac;

    invoke-virtual {v1}, LX/4Ac;->a()V

    .line 1777298
    const/16 v1, 0x2d

    const v2, -0x45e870ee

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x7bd49ba5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1777291
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 1777292
    iget-object v1, p0, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->b:LX/4Ac;

    invoke-virtual {v1}, LX/4Ac;->b()V

    .line 1777293
    const/16 v1, 0x2d

    const v2, -0xb70c5e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 12

    .prologue
    .line 1777285
    invoke-virtual {p0}, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v6, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1777286
    invoke-virtual {p0}, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v7, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1777287
    sget-object v0, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->a:Landroid/graphics/DrawFilter;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->setDrawFilter(Landroid/graphics/DrawFilter;)V

    .line 1777288
    iget-object v1, p0, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->c:LX/Alv;

    iget v3, p0, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->e:I

    int-to-double v4, v7

    iget-wide v8, p0, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->g:D

    mul-double/2addr v4, v8

    double-to-float v4, v4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v7}, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->a(LX/Alv;Landroid/graphics/Canvas;IFFII)V

    .line 1777289
    iget-object v1, p0, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->d:LX/Alv;

    iget v0, p0, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->e:I

    neg-int v3, v0

    const/4 v4, 0x0

    int-to-double v8, v6

    iget-wide v10, p0, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->g:D

    mul-double/2addr v8, v10

    double-to-float v5, v8

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v7}, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->a(LX/Alv;Landroid/graphics/Canvas;IFFII)V

    .line 1777290
    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 1777282
    invoke-super {p0}, Landroid/view/View;->onFinishTemporaryDetach()V

    .line 1777283
    iget-object v0, p0, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->b:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->a()V

    .line 1777284
    return-void
.end method

.method public final onMeasure(II)V
    .locals 8

    .prologue
    .line 1777266
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 1777267
    invoke-virtual {p0}, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->getMeasuredWidth()I

    move-result v0

    .line 1777268
    invoke-virtual {p0}, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->getMeasuredHeight()I

    move-result v1

    .line 1777269
    int-to-double v2, v0

    iget-wide v4, p0, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->f:D

    mul-double/2addr v2, v4

    int-to-double v4, v1

    iget-wide v6, p0, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->g:D

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    double-to-int v2, v2

    .line 1777270
    int-to-double v4, v0

    iget-wide v6, p0, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->g:D

    mul-double/2addr v4, v6

    int-to-double v0, v1

    iget-wide v6, p0, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->f:D

    mul-double/2addr v0, v6

    add-double/2addr v0, v4

    double-to-int v0, v0

    .line 1777271
    invoke-virtual {p0, v2, v0}, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->setMeasuredDimension(II)V

    .line 1777272
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 1777263
    invoke-super {p0}, Landroid/view/View;->onStartTemporaryDetach()V

    .line 1777264
    iget-object v0, p0, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->b:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->b()V

    .line 1777265
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, -0x2d8c8319

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1777262
    iget-object v2, p0, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->b:LX/4Ac;

    invoke-virtual {v2, p1}, LX/4Ac;->a(Landroid/view/MotionEvent;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    const v2, -0x366fb0ae

    invoke-static {v2, v1}, LX/02F;->a(II)V

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setRotateDegree(I)V
    .locals 4

    .prologue
    .line 1777256
    iput p1, p0, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->e:I

    .line 1777257
    iget v0, p0, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->e:I

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    .line 1777258
    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->f:D

    .line 1777259
    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->g:D

    .line 1777260
    invoke-virtual {p0}, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->invalidate()V

    .line 1777261
    return-void
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 1777255
    iget-object v0, p0, Lcom/facebook/productionprompts/common/v3/OverlappingImageView;->b:LX/4Ac;

    invoke-virtual {v0, p1}, LX/4Ac;->a(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
