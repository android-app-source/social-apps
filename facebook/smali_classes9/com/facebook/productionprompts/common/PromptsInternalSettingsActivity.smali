.class public Lcom/facebook/productionprompts/common/PromptsInternalSettingsActivity;
.super Lcom/facebook/base/activity/FbPreferenceActivity;
.source ""


# instance fields
.field public a:LX/1kD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1777230
    invoke-direct {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;LX/1RN;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1777229
    if-nez p1, :cond_0

    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n\nPrompt ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, LX/1RN;->a:LX/1kK;

    invoke-interface {v1}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nTracking String: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, LX/1RN;->b:LX/1lP;

    iget-object v1, v1, LX/1lP;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nRanking Score: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, LX/1RN;->b:LX/1lP;

    iget-wide v2, v1, LX/1lP;->d:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nPrompt Confidence: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, LX/1RN;->b:LX/1lP;

    iget-object v1, v1, LX/1lP;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1777201
    invoke-virtual {p0}, Lcom/facebook/productionprompts/common/PromptsInternalSettingsActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 1777202
    iget-object v1, p0, Lcom/facebook/productionprompts/common/PromptsInternalSettingsActivity;->a:LX/1kD;

    .line 1777203
    iget-object v2, v1, LX/1kD;->d:LX/1RN;

    move-object v1, v2

    .line 1777204
    invoke-direct {p0, v0, v1}, Lcom/facebook/productionprompts/common/PromptsInternalSettingsActivity;->a(Landroid/preference/PreferenceScreen;LX/1RN;)V

    .line 1777205
    invoke-direct {p0, v0, v1}, Lcom/facebook/productionprompts/common/PromptsInternalSettingsActivity;->b(Landroid/preference/PreferenceScreen;LX/1RN;)V

    .line 1777206
    invoke-virtual {p0, v0}, Lcom/facebook/productionprompts/common/PromptsInternalSettingsActivity;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 1777207
    return-void
.end method

.method private a(Landroid/preference/PreferenceCategory;Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 1777223
    new-instance v1, Landroid/preference/Preference;

    invoke-direct {v1, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1777224
    invoke-virtual {v1, p2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 1777225
    if-eqz p3, :cond_0

    const-string v0, ""

    :goto_0
    iget-object v2, p0, Lcom/facebook/productionprompts/common/PromptsInternalSettingsActivity;->a:LX/1kD;

    invoke-virtual {v2, p2}, LX/1kD;->b(Ljava/lang/String;)LX/1RN;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/facebook/productionprompts/common/PromptsInternalSettingsActivity;->a(Ljava/lang/String;LX/1RN;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1777226
    invoke-virtual {p1, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 1777227
    return-void

    .line 1777228
    :cond_0
    iget-object v0, p0, Lcom/facebook/productionprompts/common/PromptsInternalSettingsActivity;->a:LX/1kD;

    invoke-virtual {v0, p2}, LX/1kD;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Landroid/preference/PreferenceScreen;LX/1RN;)V
    .locals 3

    .prologue
    .line 1777231
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 1777232
    const-string v1, "Prompt being shown"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 1777233
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1777234
    if-eqz p2, :cond_0

    .line 1777235
    iget-object v1, p2, LX/1RN;->a:LX/1kK;

    invoke-interface {v1}, LX/1kK;->c()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/productionprompts/common/PromptsInternalSettingsActivity;->a(Landroid/preference/PreferenceCategory;Ljava/lang/String;Z)V

    .line 1777236
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/productionprompts/common/PromptsInternalSettingsActivity;

    invoke-static {v0}, LX/1kD;->a(LX/0QB;)LX/1kD;

    move-result-object v0

    check-cast v0, LX/1kD;

    iput-object v0, p0, Lcom/facebook/productionprompts/common/PromptsInternalSettingsActivity;->a:LX/1kD;

    return-void
.end method

.method private b(Landroid/preference/PreferenceScreen;LX/1RN;)V
    .locals 4

    .prologue
    .line 1777214
    new-instance v1, Landroid/preference/PreferenceCategory;

    invoke-direct {v1, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 1777215
    const-string v0, "Prompts that are not shown"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 1777216
    invoke-virtual {p1, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1777217
    iget-object v0, p0, Lcom/facebook/productionprompts/common/PromptsInternalSettingsActivity;->a:LX/1kD;

    invoke-virtual {v0}, LX/1kD;->c()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 1777218
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1777219
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1777220
    if-eqz p2, :cond_1

    iget-object v3, p2, LX/1RN;->a:LX/1kK;

    invoke-interface {v3}, LX/1kK;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1777221
    :cond_1
    const/4 v3, 0x0

    invoke-direct {p0, v1, v0, v3}, Lcom/facebook/productionprompts/common/PromptsInternalSettingsActivity;->a(Landroid/preference/PreferenceCategory;Ljava/lang/String;Z)V

    goto :goto_0

    .line 1777222
    :cond_2
    return-void
.end method


# virtual methods
.method public final c(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1777210
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->c(Landroid/os/Bundle;)V

    .line 1777211
    invoke-static {p0, p0}, Lcom/facebook/productionprompts/common/PromptsInternalSettingsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1777212
    invoke-direct {p0}, Lcom/facebook/productionprompts/common/PromptsInternalSettingsActivity;->a()V

    .line 1777213
    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x55c31edc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1777208
    invoke-super {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;->onStop()V

    .line 1777209
    const/16 v1, 0x23

    const v2, 0x48b56390    # 371484.5f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
