.class public Lcom/facebook/ipc/friendsharing/listeningto/ListeningToComposerPluginConfigSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ipc/friendsharing/listeningto/ListeningToComposerPluginConfig;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1745940
    const-class v0, Lcom/facebook/ipc/friendsharing/listeningto/ListeningToComposerPluginConfig;

    new-instance v1, Lcom/facebook/ipc/friendsharing/listeningto/ListeningToComposerPluginConfigSerializer;

    invoke-direct {v1}, Lcom/facebook/ipc/friendsharing/listeningto/ListeningToComposerPluginConfigSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1745941
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1745942
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ipc/friendsharing/listeningto/ListeningToComposerPluginConfig;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1745943
    if-nez p0, :cond_0

    .line 1745944
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1745945
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1745946
    invoke-static {p0, p1, p2}, Lcom/facebook/ipc/friendsharing/listeningto/ListeningToComposerPluginConfigSerializer;->b(Lcom/facebook/ipc/friendsharing/listeningto/ListeningToComposerPluginConfig;LX/0nX;LX/0my;)V

    .line 1745947
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1745948
    return-void
.end method

.method private static b(Lcom/facebook/ipc/friendsharing/listeningto/ListeningToComposerPluginConfig;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1745949
    const-string v0, "artist_name"

    iget-object v1, p0, Lcom/facebook/ipc/friendsharing/listeningto/ListeningToComposerPluginConfig;->mArtistName:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1745950
    const-string v0, "track_name"

    iget-object v1, p0, Lcom/facebook/ipc/friendsharing/listeningto/ListeningToComposerPluginConfig;->mTrackName:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1745951
    const-string v0, "composer_session_id"

    iget-object v1, p0, Lcom/facebook/ipc/friendsharing/listeningto/ListeningToComposerPluginConfig;->mComposerSessionId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1745952
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1745953
    check-cast p1, Lcom/facebook/ipc/friendsharing/listeningto/ListeningToComposerPluginConfig;

    invoke-static {p1, p2, p3}, Lcom/facebook/ipc/friendsharing/listeningto/ListeningToComposerPluginConfigSerializer;->a(Lcom/facebook/ipc/friendsharing/listeningto/ListeningToComposerPluginConfig;LX/0nX;LX/0my;)V

    return-void
.end method
