.class public Lcom/facebook/ipc/friendsharing/listeningto/ListeningToComposerPluginConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/88f;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/friendsharing/listeningto/ListeningToComposerPluginConfigDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ipc/friendsharing/listeningto/ListeningToComposerPluginConfigSerializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final mArtistName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "artist_name"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mComposerSessionId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "composer_session_id"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mTrackName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "track_name"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1745916
    const-class v0, Lcom/facebook/ipc/friendsharing/listeningto/ListeningToComposerPluginConfigDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1745914
    const-class v0, Lcom/facebook/ipc/friendsharing/listeningto/ListeningToComposerPluginConfigSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1745915
    const-class v0, Lcom/facebook/ipc/friendsharing/listeningto/ListeningToComposerPluginConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/ipc/friendsharing/listeningto/ListeningToComposerPluginConfig;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1745905
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1745906
    iput-object v0, p0, Lcom/facebook/ipc/friendsharing/listeningto/ListeningToComposerPluginConfig;->mArtistName:Ljava/lang/String;

    .line 1745907
    iput-object v0, p0, Lcom/facebook/ipc/friendsharing/listeningto/ListeningToComposerPluginConfig;->mTrackName:Ljava/lang/String;

    .line 1745908
    iput-object v0, p0, Lcom/facebook/ipc/friendsharing/listeningto/ListeningToComposerPluginConfig;->mComposerSessionId:Ljava/lang/String;

    .line 1745909
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1745910
    iget-object v0, p0, Lcom/facebook/ipc/friendsharing/listeningto/ListeningToComposerPluginConfig;->mArtistName:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1745911
    iget-object v0, p0, Lcom/facebook/ipc/friendsharing/listeningto/ListeningToComposerPluginConfig;->mTrackName:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1745912
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1745913
    sget-object v0, Lcom/facebook/ipc/friendsharing/listeningto/ListeningToComposerPluginConfig;->a:Ljava/lang/String;

    return-object v0
.end method
