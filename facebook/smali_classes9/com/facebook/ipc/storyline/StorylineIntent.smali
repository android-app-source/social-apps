.class public Lcom/facebook/ipc/storyline/StorylineIntent;
.super Landroid/content/Intent;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1746302
    invoke-direct {p0}, Landroid/content/Intent;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;LX/B68;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 1746303
    new-instance v0, Lcom/facebook/ipc/storyline/StorylineIntent;

    invoke-direct {v0}, Lcom/facebook/ipc/storyline/StorylineIntent;-><init>()V

    .line 1746304
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.facebook.storyline.fb4a.activity.StorylineActivity"

    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/storyline/StorylineIntent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1746305
    const-string v1, "extra_storyline_entry_point"

    invoke-virtual {p1}, LX/B68;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/ipc/storyline/StorylineIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1746306
    return-object v0
.end method
