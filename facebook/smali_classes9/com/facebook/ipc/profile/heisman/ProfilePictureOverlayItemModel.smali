.class public Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1746086
    new-instance v0, LX/B5w;

    invoke-direct {v0}, LX/B5w;-><init>()V

    sput-object v0, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/B5x;)V
    .locals 1

    .prologue
    .line 1746087
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1746088
    iget-object v0, p1, LX/B5x;->a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    iput-object v0, p0, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    .line 1746089
    iget-object v0, p1, LX/B5x;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->b:Ljava/lang/String;

    .line 1746090
    iget-object v0, p1, LX/B5x;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->c:Ljava/lang/String;

    .line 1746091
    iget-object v0, p1, LX/B5x;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->d:Ljava/lang/String;

    .line 1746092
    iget-object v0, p1, LX/B5x;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->e:Ljava/lang/String;

    .line 1746093
    iget-object v0, p1, LX/B5x;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->f:Ljava/lang/String;

    .line 1746094
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1746095
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1746096
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    iput-object v0, p0, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    .line 1746097
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->b:Ljava/lang/String;

    .line 1746098
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->c:Ljava/lang/String;

    .line 1746099
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->d:Ljava/lang/String;

    .line 1746100
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->e:Ljava/lang/String;

    .line 1746101
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->f:Ljava/lang/String;

    .line 1746102
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1746103
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1746104
    iget-object v0, p0, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1746105
    iget-object v0, p0, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1746106
    iget-object v0, p0, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1746107
    iget-object v0, p0, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1746108
    iget-object v0, p0, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1746109
    iget-object v0, p0, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1746110
    return-void
.end method
