.class public Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ipc/slideshow/SlideshowEditConfigurationSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LX/B63;

.field private final b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/lang/String;

.field private final e:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final f:LX/B66;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1746210
    const-class v0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1746209
    const-class v0, Lcom/facebook/ipc/slideshow/SlideshowEditConfigurationSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1746208
    new-instance v0, LX/B62;

    invoke-direct {v0}, LX/B62;-><init>()V

    sput-object v0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1746191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1746192
    invoke-static {}, LX/B63;->values()[LX/B63;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->a:LX/B63;

    .line 1746193
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 1746194
    iput-object v3, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1746195
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v2, v0, [Lcom/facebook/ipc/media/MediaItem;

    .line 1746196
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    array-length v0, v2

    if-ge v1, v0, :cond_1

    .line 1746197
    const-class v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 1746198
    aput-object v0, v2, v1

    .line 1746199
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1746200
    :cond_0
    sget-object v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    iput-object v0, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    goto :goto_0

    .line 1746201
    :cond_1
    invoke-static {v2}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->c:LX/0Px;

    .line 1746202
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->d:Ljava/lang/String;

    .line 1746203
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_2

    .line 1746204
    iput-object v3, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->e:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    .line 1746205
    :goto_2
    invoke-static {}, LX/B66;->values()[LX/B66;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->f:LX/B66;

    .line 1746206
    return-void

    .line 1746207
    :cond_2
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerSlideshowData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    iput-object v0, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->e:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    goto :goto_2
.end method

.method public constructor <init>(Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;)V
    .locals 1

    .prologue
    .line 1746183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1746184
    iget-object v0, p1, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;->c:LX/B63;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B63;

    iput-object v0, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->a:LX/B63;

    .line 1746185
    iget-object v0, p1, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;->d:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    iput-object v0, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1746186
    iget-object v0, p1, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;->e:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->c:LX/0Px;

    .line 1746187
    iget-object v0, p1, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;->f:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->d:Ljava/lang/String;

    .line 1746188
    iget-object v0, p1, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;->g:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    iput-object v0, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->e:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    .line 1746189
    iget-object v0, p1, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;->h:LX/B66;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B66;

    iput-object v0, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->f:LX/B66;

    .line 1746190
    return-void
.end method

.method public static newBuilder()Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;
    .locals 2

    .prologue
    .line 1746182
    new-instance v0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;

    invoke-direct {v0}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1746181
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1746211
    if-ne p0, p1, :cond_1

    .line 1746212
    :cond_0
    :goto_0
    return v0

    .line 1746213
    :cond_1
    instance-of v2, p1, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;

    if-nez v2, :cond_2

    move v0, v1

    .line 1746214
    goto :goto_0

    .line 1746215
    :cond_2
    check-cast p1, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;

    .line 1746216
    iget-object v2, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->a:LX/B63;

    iget-object v3, p1, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->a:LX/B63;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1746217
    goto :goto_0

    .line 1746218
    :cond_3
    iget-object v2, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    iget-object v3, p1, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1746219
    goto :goto_0

    .line 1746220
    :cond_4
    iget-object v2, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->c:LX/0Px;

    iget-object v3, p1, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->c:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 1746221
    goto :goto_0

    .line 1746222
    :cond_5
    iget-object v2, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->d:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1746223
    goto :goto_0

    .line 1746224
    :cond_6
    iget-object v2, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->e:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    iget-object v3, p1, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->e:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 1746225
    goto :goto_0

    .line 1746226
    :cond_7
    iget-object v2, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->f:LX/B66;

    iget-object v3, p1, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->f:LX/B66;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1746227
    goto :goto_0
.end method

.method public getActionWhenDone()LX/B63;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "action_when_done"
    .end annotation

    .prologue
    .line 1746180
    iget-object v0, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->a:LX/B63;

    return-object v0
.end method

.method public getComposerConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "composer_configuration"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1746179
    iget-object v0, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    return-object v0
.end method

.method public getMediaItems()LX/0Px;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "media_items"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1746178
    iget-object v0, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->c:LX/0Px;

    return-object v0
.end method

.method public getSessionId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "session_id"
    .end annotation

    .prologue
    .line 1746177
    iget-object v0, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getSlideshowData()Lcom/facebook/ipc/composer/model/ComposerSlideshowData;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "slideshow_data"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1746176
    iget-object v0, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->e:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    return-object v0
.end method

.method public getSource()LX/B66;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "source"
    .end annotation

    .prologue
    .line 1746175
    iget-object v0, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->f:LX/B66;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1746174
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->a:LX/B63;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->c:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->e:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->f:LX/B66;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 1746158
    iget-object v0, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->a:LX/B63;

    invoke-virtual {v0}, LX/B63;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1746159
    iget-object v0, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    if-nez v0, :cond_0

    .line 1746160
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1746161
    :goto_0
    iget-object v0, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1746162
    iget-object v0, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    move v1, v2

    :goto_1
    if-ge v1, v3, :cond_1

    iget-object v0, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->c:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 1746163
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1746164
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1746165
    :cond_0
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 1746166
    iget-object v0, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 1746167
    :cond_1
    iget-object v0, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1746168
    iget-object v0, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->e:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    if-nez v0, :cond_2

    .line 1746169
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1746170
    :goto_2
    iget-object v0, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->f:LX/B66;

    invoke-virtual {v0}, LX/B66;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1746171
    return-void

    .line 1746172
    :cond_2
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 1746173
    iget-object v0, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->e:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerSlideshowData;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_2
.end method
