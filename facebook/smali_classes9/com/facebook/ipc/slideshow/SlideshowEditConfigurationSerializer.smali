.class public Lcom/facebook/ipc/slideshow/SlideshowEditConfigurationSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1746243
    const-class v0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;

    new-instance v1, Lcom/facebook/ipc/slideshow/SlideshowEditConfigurationSerializer;

    invoke-direct {v1}, Lcom/facebook/ipc/slideshow/SlideshowEditConfigurationSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1746244
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1746242
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1746228
    if-nez p0, :cond_0

    .line 1746229
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1746230
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1746231
    invoke-static {p0, p1, p2}, Lcom/facebook/ipc/slideshow/SlideshowEditConfigurationSerializer;->b(Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;LX/0nX;LX/0my;)V

    .line 1746232
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1746233
    return-void
.end method

.method private static b(Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1746235
    const-string v0, "action_when_done"

    invoke-virtual {p0}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->getActionWhenDone()LX/B63;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1746236
    const-string v0, "composer_configuration"

    invoke-virtual {p0}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->getComposerConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1746237
    const-string v0, "media_items"

    invoke-virtual {p0}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->getMediaItems()LX/0Px;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1746238
    const-string v0, "session_id"

    invoke-virtual {p0}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1746239
    const-string v0, "slideshow_data"

    invoke-virtual {p0}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->getSlideshowData()Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1746240
    const-string v0, "source"

    invoke-virtual {p0}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->getSource()LX/B66;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1746241
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1746234
    check-cast p1, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;

    invoke-static {p1, p2, p3}, Lcom/facebook/ipc/slideshow/SlideshowEditConfigurationSerializer;->a(Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;LX/0nX;LX/0my;)V

    return-void
.end method
