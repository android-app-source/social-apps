.class public final Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration_BuilderDeserializer;
.end annotation


# static fields
.field private static final a:LX/B63;

.field private static final b:LX/B66;


# instance fields
.field public c:LX/B63;

.field public d:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/lang/String;

.field public g:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/B66;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1746143
    const-class v0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration_BuilderDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1746144
    new-instance v0, LX/B64;

    invoke-direct {v0}, LX/B64;-><init>()V

    .line 1746145
    sget-object v0, LX/B63;->NONE:LX/B63;

    move-object v0, v0

    .line 1746146
    sput-object v0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;->a:LX/B63;

    .line 1746147
    new-instance v0, LX/B65;

    invoke-direct {v0}, LX/B65;-><init>()V

    .line 1746148
    sget-object v0, LX/B66;->SIMPLE_PICKER:LX/B66;

    move-object v0, v0

    .line 1746149
    sput-object v0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;->b:LX/B66;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1746125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1746126
    sget-object v0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;->a:LX/B63;

    iput-object v0, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;->c:LX/B63;

    .line 1746127
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1746128
    iput-object v0, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;->e:LX/0Px;

    .line 1746129
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;->f:Ljava/lang/String;

    .line 1746130
    sget-object v0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;->b:LX/B66;

    iput-object v0, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;->h:LX/B66;

    .line 1746131
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;
    .locals 2

    .prologue
    .line 1746142
    new-instance v0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;

    invoke-direct {v0, p0}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;-><init>(Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;)V

    return-object v0
.end method

.method public setActionWhenDone(LX/B63;)Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "action_when_done"
    .end annotation

    .prologue
    .line 1746140
    iput-object p1, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;->c:LX/B63;

    .line 1746141
    return-object p0
.end method

.method public setComposerConfiguration(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;
    .locals 0
    .param p1    # Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "composer_configuration"
    .end annotation

    .prologue
    .line 1746150
    iput-object p1, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;->d:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1746151
    return-object p0
.end method

.method public setMediaItems(LX/0Px;)Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "media_items"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;)",
            "Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;"
        }
    .end annotation

    .prologue
    .line 1746138
    iput-object p1, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;->e:LX/0Px;

    .line 1746139
    return-object p0
.end method

.method public setSessionId(Ljava/lang/String;)Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "session_id"
    .end annotation

    .prologue
    .line 1746136
    iput-object p1, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;->f:Ljava/lang/String;

    .line 1746137
    return-object p0
.end method

.method public setSlideshowData(Lcom/facebook/ipc/composer/model/ComposerSlideshowData;)Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;
    .locals 0
    .param p1    # Lcom/facebook/ipc/composer/model/ComposerSlideshowData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "slideshow_data"
    .end annotation

    .prologue
    .line 1746134
    iput-object p1, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;->g:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    .line 1746135
    return-object p0
.end method

.method public setSource(LX/B66;)Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "source"
    .end annotation

    .prologue
    .line 1746132
    iput-object p1, p0, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;->h:LX/B66;

    .line 1746133
    return-object p0
.end method
