.class public Lcom/facebook/video/followvideos/VideoHomeToggleButton;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Z

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:LX/BUq;

.field private final e:Landroid/widget/TextView;

.field private final f:Lcom/facebook/fbui/glyph/GlyphView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1789750
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/followvideos/VideoHomeToggleButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1789751
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1789748
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/followvideos/VideoHomeToggleButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1789749
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1789713
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1789714
    const v0, 0x7f03159b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1789715
    invoke-virtual {p0}, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1789716
    const v0, 0x7f021a44

    invoke-virtual {p0, v0}, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->setBackgroundResource(I)V

    .line 1789717
    :cond_0
    const v0, 0x7f0d02a7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->e:Landroid/widget/TextView;

    .line 1789718
    const v0, 0x7f0d0281

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->f:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1789719
    new-instance v0, LX/BUw;

    invoke-direct {v0, p0}, LX/BUw;-><init>(Lcom/facebook/video/followvideos/VideoHomeToggleButton;)V

    invoke-virtual {p0, v0}, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1789720
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1789741
    iget-boolean v0, p0, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->a:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->b:Ljava/lang/String;

    .line 1789742
    :goto_0
    if-eqz v0, :cond_0

    .line 1789743
    iget-object v1, p0, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1789744
    :cond_0
    iget-object v1, p0, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->f:Lcom/facebook/fbui/glyph/GlyphView;

    iget-boolean v0, p0, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->a:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1789745
    return-void

    .line 1789746
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->c:Ljava/lang/String;

    goto :goto_0

    .line 1789747
    :cond_2
    const/16 v0, 0x8

    goto :goto_1
.end method

.method public static c(Lcom/facebook/video/followvideos/VideoHomeToggleButton;)V
    .locals 2

    .prologue
    .line 1789735
    iget-boolean v0, p0, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->a:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->a:Z

    .line 1789736
    invoke-direct {p0}, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->b()V

    .line 1789737
    iget-object v0, p0, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->d:LX/BUq;

    if-eqz v0, :cond_0

    .line 1789738
    iget-object v0, p0, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->d:LX/BUq;

    iget-boolean v1, p0, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->a:Z

    invoke-interface {v0, v1}, LX/BUq;->a(Z)V

    .line 1789739
    :cond_0
    return-void

    .line 1789740
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1789731
    iget-object v0, p0, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->e:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00a3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1789732
    iget-object v0, p0, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->f:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00a3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1789733
    const v0, 0x7f021a45

    invoke-virtual {p0, v0}, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->setBackgroundResource(I)V

    .line 1789734
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1789728
    iput-boolean p1, p0, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->a:Z

    .line 1789729
    invoke-direct {p0}, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->b()V

    .line 1789730
    return-void
.end method

.method public final a(ZLjava/lang/String;Ljava/lang/String;LX/BUq;)V
    .locals 1

    .prologue
    .line 1789721
    iput-object p2, p0, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->b:Ljava/lang/String;

    .line 1789722
    iput-object p3, p0, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->c:Ljava/lang/String;

    .line 1789723
    iget-boolean v0, p0, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->a:Z

    if-eq p1, v0, :cond_0

    .line 1789724
    iput-boolean p1, p0, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->a:Z

    .line 1789725
    :cond_0
    invoke-direct {p0}, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->b()V

    .line 1789726
    iput-object p4, p0, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->d:LX/BUq;

    .line 1789727
    return-void
.end method
