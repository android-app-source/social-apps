.class public Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/3Gp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Z

.field private c:Z

.field private d:Lcom/facebook/graphql/model/GraphQLActor;

.field private e:Ljava/lang/String;

.field private f:LX/BUv;

.field private g:LX/Aek;

.field private h:Lcom/facebook/video/followvideos/VideoHomeToggleButton;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final i:Landroid/content/Context;

.field private final j:Lcom/facebook/video/followvideos/VideoHomeToggleButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1789690
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1789691
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1789699
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1789700
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1789692
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1789693
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->h:Lcom/facebook/video/followvideos/VideoHomeToggleButton;

    .line 1789694
    const-class v0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;

    invoke-static {v0, p0}, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1789695
    const v0, 0x7f031598

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1789696
    const v0, 0x7f0d30b2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/followvideos/VideoHomeToggleButton;

    iput-object v0, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->j:Lcom/facebook/video/followvideos/VideoHomeToggleButton;

    .line 1789697
    iput-object p1, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->i:Landroid/content/Context;

    .line 1789698
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;

    invoke-static {v0}, LX/3Gp;->a(LX/0QB;)LX/3Gp;

    move-result-object v0

    check-cast v0, LX/3Gp;

    iput-object v0, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->a:LX/3Gp;

    return-void
.end method

.method private a(ZLjava/lang/String;Ljava/lang/String;LX/BUq;)V
    .locals 1

    .prologue
    .line 1789687
    iput-boolean p1, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->c:Z

    .line 1789688
    iget-object v0, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->j:Lcom/facebook/video/followvideos/VideoHomeToggleButton;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->a(ZLjava/lang/String;Ljava/lang/String;LX/BUq;)V

    .line 1789689
    return-void
.end method

.method public static a$redex0(Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1789675
    iput-boolean p1, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->c:Z

    .line 1789676
    iget-object v0, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->h:Lcom/facebook/video/followvideos/VideoHomeToggleButton;

    if-eqz v0, :cond_0

    .line 1789677
    iget-boolean v0, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->c:Z

    if-eqz v0, :cond_3

    .line 1789678
    iget-object v0, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->h:Lcom/facebook/video/followvideos/VideoHomeToggleButton;

    invoke-virtual {v0, v1}, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->setVisibility(I)V

    .line 1789679
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->c:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->b:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->g:LX/Aek;

    if-eqz v0, :cond_1

    .line 1789680
    iget-object v0, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->i:Landroid/content/Context;

    iget-object v1, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->d:Lcom/facebook/graphql/model/GraphQLActor;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->g:LX/Aek;

    invoke-static {v0, v1, v2, v3}, LX/BUp;->a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/graphql/model/GraphQLMedia;LX/Aek;)V

    .line 1789681
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->f:LX/BUv;

    if-eqz v0, :cond_2

    .line 1789682
    iget-object v0, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->f:LX/BUv;

    iget-boolean v1, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->c:Z

    invoke-interface {v0, v1}, LX/BUv;->a(Z)V

    .line 1789683
    :cond_2
    return-void

    .line 1789684
    :cond_3
    iput-boolean v1, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->b:Z

    .line 1789685
    iget-object v0, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->h:Lcom/facebook/video/followvideos/VideoHomeToggleButton;

    invoke-virtual {v0, v1}, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->a(Z)V

    .line 1789686
    iget-object v0, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->h:Lcom/facebook/video/followvideos/VideoHomeToggleButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->setVisibility(I)V

    goto :goto_0
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1789701
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->b:Z

    .line 1789702
    iput-object v2, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->e:Ljava/lang/String;

    .line 1789703
    iput-object v2, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->f:LX/BUv;

    .line 1789704
    iput-object v2, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->g:LX/Aek;

    .line 1789705
    iput-object v2, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->d:Lcom/facebook/graphql/model/GraphQLActor;

    .line 1789706
    iget-object v0, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->h:Lcom/facebook/video/followvideos/VideoHomeToggleButton;

    if-eqz v0, :cond_0

    .line 1789707
    iget-object v0, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->h:Lcom/facebook/video/followvideos/VideoHomeToggleButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->setVisibility(I)V

    .line 1789708
    iput-object v2, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->h:Lcom/facebook/video/followvideos/VideoHomeToggleButton;

    .line 1789709
    :cond_0
    return-void
.end method

.method public static b(Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Z
        .annotation build Lcom/facebook/graphql/calls/VideoChannelSubscriptionSurfaces;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/VideoChannelSubscriptionSurfaces;
        .end annotation
    .end param

    .prologue
    .line 1789670
    iput-boolean p1, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->b:Z

    .line 1789671
    if-eqz p1, :cond_0

    .line 1789672
    iget-object v0, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->a:LX/3Gp;

    iget-object v1, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, LX/3Gp;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1789673
    :goto_0
    return-void

    .line 1789674
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->a:LX/3Gp;

    iget-object v1, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, p3}, LX/3Gp;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1789668
    iget-object v0, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->j:Lcom/facebook/video/followvideos/VideoHomeToggleButton;

    invoke-virtual {v0}, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->a()V

    .line 1789669
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLActor;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/VideoChannelSubscriptionSurfaces;
        .end annotation
    .end param

    .prologue
    .line 1789662
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    .line 1789663
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1789664
    :goto_0
    return-void

    .line 1789665
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->aE()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->b:Z

    .line 1789666
    iput-object p1, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->d:Lcom/facebook/graphql/model/GraphQLActor;

    .line 1789667
    new-instance v0, LX/BUu;

    invoke-direct {v0, p0, p2}, LX/BUu;-><init>(Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->g:LX/Aek;

    goto :goto_0
.end method

.method public final a(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/VideoChannelSubscriptionSurfaces;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/VideoChannelSubscriptionSurfaces;
        .end annotation
    .end param

    .prologue
    .line 1789651
    iput-boolean p1, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->b:Z

    .line 1789652
    new-instance v1, LX/BUt;

    invoke-direct {v1, p0, p2, p3}, LX/BUt;-><init>(Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;Ljava/lang/String;Ljava/lang/String;)V

    .line 1789653
    iget-object v0, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->i:Landroid/content/Context;

    .line 1789654
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081a2d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 1789655
    iget-object v0, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->i:Landroid/content/Context;

    .line 1789656
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const p2, 0x7f081a2e

    invoke-virtual {v3, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v3, v3

    .line 1789657
    const v0, 0x7f0d30b3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/followvideos/VideoHomeToggleButton;

    iput-object v0, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->h:Lcom/facebook/video/followvideos/VideoHomeToggleButton;

    .line 1789658
    iget-object v0, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->h:Lcom/facebook/video/followvideos/VideoHomeToggleButton;

    invoke-virtual {v0, p1, v2, v3, v1}, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->a(ZLjava/lang/String;Ljava/lang/String;LX/BUq;)V

    .line 1789659
    iget-boolean v0, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->c:Z

    if-eqz v0, :cond_0

    .line 1789660
    iget-object v0, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->h:Lcom/facebook/video/followvideos/VideoHomeToggleButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/video/followvideos/VideoHomeToggleButton;->setVisibility(I)V

    .line 1789661
    :cond_0
    return-void
.end method

.method public final a(ZLjava/lang/String;Ljava/lang/String;LX/BUv;)V
    .locals 1

    .prologue
    .line 1789646
    invoke-direct {p0}, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->b()V

    .line 1789647
    iput-object p4, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->f:LX/BUv;

    .line 1789648
    new-instance v0, LX/BUs;

    invoke-direct {v0, p0}, LX/BUs;-><init>(Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;)V

    .line 1789649
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->a(ZLjava/lang/String;Ljava/lang/String;LX/BUq;)V

    .line 1789650
    return-void
.end method

.method public final a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/VideoChannelFollowSurfaces;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/VideoChannelFollowSurfaces;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1789641
    invoke-direct {p0}, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->b()V

    .line 1789642
    iput-object p2, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->e:Ljava/lang/String;

    .line 1789643
    new-instance v0, LX/BUr;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p5

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/BUr;-><init>(Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1789644
    invoke-virtual {p0}, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/BUj;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/BUj;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p1, v1, v2, v0}, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->a(ZLjava/lang/String;Ljava/lang/String;LX/BUq;)V

    .line 1789645
    return-void
.end method

.method public setFollowStateChangedListener(LX/BUv;)V
    .locals 0

    .prologue
    .line 1789639
    iput-object p1, p0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->f:LX/BUv;

    .line 1789640
    return-void
.end method
