.class public Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;
.super Lcom/facebook/base/activity/FbPreferenceActivity;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/BSB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:LX/4oi;

.field private d:LX/4oi;

.field private e:LX/4oi;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1785408
    invoke-direct {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;-><init>()V

    return-void
.end method

.method private a(Landroid/preference/PreferenceScreen;LX/0Tn;Ljava/lang/String;)LX/4oi;
    .locals 2

    .prologue
    .line 1785409
    new-instance v0, LX/4oi;

    invoke-direct {v0, p0}, LX/4oi;-><init>(Landroid/content/Context;)V

    .line 1785410
    invoke-virtual {v0, p2}, LX/4oi;->a(LX/0Tn;)V

    .line 1785411
    invoke-virtual {v0, p3}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 1785412
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/4oi;->setPersistent(Z)V

    .line 1785413
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1785414
    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1785425
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/BSN;->b:LX/0Tn;

    sget-object v2, LX/BSN;->c:LX/0Tn;

    invoke-virtual {v2}, LX/0To;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1785426
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->c:LX/4oi;

    .line 1785427
    sget-object v2, LX/BSN;->c:LX/0Tn;

    invoke-virtual {v2}, LX/0To;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1785428
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->c:LX/4oi;

    .line 1785429
    :cond_0
    :goto_0
    invoke-direct {p0, v0}, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->a(LX/4oi;)V

    .line 1785430
    const-string v0, "impression"

    invoke-direct {p0, v0, v1}, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1785431
    return-void

    .line 1785432
    :cond_1
    sget-object v2, LX/BSN;->d:LX/0Tn;

    invoke-virtual {v2}, LX/0To;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1785433
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->d:LX/4oi;

    goto :goto_0

    .line 1785434
    :cond_2
    sget-object v2, LX/BSN;->e:LX/0Tn;

    invoke-virtual {v2}, LX/0To;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1785435
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->e:LX/4oi;

    goto :goto_0
.end method

.method private a(LX/4oi;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1785415
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->c:LX/4oi;

    invoke-virtual {v0, v1}, LX/4oi;->setChecked(Z)V

    .line 1785416
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->d:LX/4oi;

    invoke-virtual {v0, v1}, LX/4oi;->setChecked(Z)V

    .line 1785417
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->e:LX/4oi;

    invoke-virtual {v0, v1}, LX/4oi;->setChecked(Z)V

    .line 1785418
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/4oi;->setChecked(Z)V

    .line 1785419
    return-void
.end method

.method private a(Landroid/preference/PreferenceScreen;)V
    .locals 2

    .prologue
    .line 1785420
    new-instance v0, LX/4om;

    invoke-direct {v0, p0}, LX/4om;-><init>(Landroid/content/Context;)V

    .line 1785421
    const v1, 0x7f082708

    invoke-virtual {p0, v1}, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4om;->setSummary(Ljava/lang/CharSequence;)V

    .line 1785422
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/4om;->setEnabled(Z)V

    .line 1785423
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1785424
    return-void
.end method

.method private static a(Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/BSB;)V
    .locals 0

    .prologue
    .line 1785407
    iput-object p1, p0, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p2, p0, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->b:LX/BSB;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;

    invoke-static {v1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v1}, LX/BSB;->b(LX/0QB;)LX/BSB;

    move-result-object v1

    check-cast v1, LX/BSB;

    invoke-static {p0, v0, v1}, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->a(Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/BSB;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1785404
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/BSN;->b:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1785405
    const-string v0, "saved"

    invoke-direct {p0, v0, p1}, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1785406
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1785402
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->b:LX/BSB;

    const-string v1, "BackgroundPlaySettingsActivity"

    invoke-virtual {v0, p1, p2, v1}, LX/BSB;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1785403
    return-void
.end method

.method private b(Landroid/preference/PreferenceScreen;)V
    .locals 2

    .prologue
    .line 1785395
    sget-object v0, LX/BSN;->c:LX/0Tn;

    const v1, 0x7f08270a

    invoke-virtual {p0, v1}, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->a(Landroid/preference/PreferenceScreen;LX/0Tn;Ljava/lang/String;)LX/4oi;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->c:LX/4oi;

    .line 1785396
    sget-object v0, LX/BSN;->d:LX/0Tn;

    const v1, 0x7f08270b

    invoke-virtual {p0, v1}, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->a(Landroid/preference/PreferenceScreen;LX/0Tn;Ljava/lang/String;)LX/4oi;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->d:LX/4oi;

    .line 1785397
    sget-object v0, LX/BSN;->e:LX/0Tn;

    const v1, 0x7f08270c

    invoke-virtual {p0, v1}, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->a(Landroid/preference/PreferenceScreen;LX/0Tn;Ljava/lang/String;)LX/4oi;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->e:LX/4oi;

    .line 1785398
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->c:LX/4oi;

    invoke-virtual {v0, p0}, LX/4oi;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 1785399
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->d:LX/4oi;

    invoke-virtual {v0, p0}, LX/4oi;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 1785400
    iget-object v0, p0, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->e:LX/4oi;

    invoke-virtual {v0, p0}, LX/4oi;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 1785401
    return-void
.end method


# virtual methods
.method public final c(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1785383
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->c(Landroid/os/Bundle;)V

    .line 1785384
    invoke-static {p0, p0}, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1785385
    const v0, 0x7f082707

    invoke-virtual {p0, v0}, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->setTitle(I)V

    .line 1785386
    invoke-virtual {p0}, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 1785387
    invoke-virtual {p0, v0}, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 1785388
    invoke-direct {p0, v0}, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->a(Landroid/preference/PreferenceScreen;)V

    .line 1785389
    new-instance v1, Landroid/preference/PreferenceCategory;

    invoke-direct {v1, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 1785390
    const v2, 0x7f082709

    invoke-virtual {p0, v2}, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 1785391
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1785392
    invoke-direct {p0, v0}, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->b(Landroid/preference/PreferenceScreen;)V

    .line 1785393
    invoke-direct {p0}, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->a()V

    .line 1785394
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x1cefcd55

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1785374
    invoke-super {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;->onDestroy()V

    .line 1785375
    const-string v1, "close"

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1785376
    const/16 v1, 0x23

    const v2, 0x3dfe139b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1785377
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1785378
    const/4 v0, 0x0

    .line 1785379
    :goto_0
    return v0

    :cond_0
    move-object v0, p1

    .line 1785380
    check-cast v0, LX/4oi;

    invoke-direct {p0, v0}, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->a(LX/4oi;)V

    .line 1785381
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/video/backgroundplay/settings/BackgroundPlaySettingsActivity;->a(Ljava/lang/String;)V

    .line 1785382
    const/4 v0, 0x1

    goto :goto_0
.end method
