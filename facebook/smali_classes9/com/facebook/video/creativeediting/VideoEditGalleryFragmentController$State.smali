.class public final Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/5SK;

.field public b:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

.field public c:Z

.field public d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1786029
    new-instance v0, LX/BSq;

    invoke-direct {v0}, LX/BSq;-><init>()V

    sput-object v0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1786012
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1786013
    invoke-static {}, LX/5SK;->values()[LX/5SK;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->a:LX/5SK;

    .line 1786014
    const-class v0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->b:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1786015
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->c:Z

    .line 1786016
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->d:Ljava/lang/String;

    .line 1786017
    return-void
.end method

.method public constructor <init>(Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;LX/5SK;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1786024
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1786025
    iput-object p1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->b:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1786026
    iput-object p2, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->a:LX/5SK;

    .line 1786027
    iput-object p3, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->d:Ljava/lang/String;

    .line 1786028
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;
    .locals 1

    .prologue
    .line 1786023
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->b:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1786022
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1786018
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->a:LX/5SK;

    invoke-virtual {v0}, LX/5SK;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1786019
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->b:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1786020
    iget-boolean v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1786021
    return-void
.end method
