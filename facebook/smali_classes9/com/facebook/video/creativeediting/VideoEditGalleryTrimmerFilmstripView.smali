.class public Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

.field public b:Landroid/view/View;

.field public c:Landroid/view/View;

.field public d:Landroid/view/View;

.field public e:Landroid/view/View;

.field public f:Landroid/view/View;

.field public g:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1786269
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1786270
    invoke-direct {p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->a()V

    .line 1786271
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1786292
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1786293
    invoke-direct {p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->a()V

    .line 1786294
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1786289
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1786290
    invoke-direct {p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->a()V

    .line 1786291
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 1786279
    const v0, 0x7f03158f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1786280
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1786281
    const v0, 0x7f0d3097

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->a:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    .line 1786282
    const v0, 0x7f0d309b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->b:Landroid/view/View;

    .line 1786283
    const v0, 0x7f0d309c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->c:Landroid/view/View;

    .line 1786284
    const v0, 0x7f0d3098

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->d:Landroid/view/View;

    .line 1786285
    const v0, 0x7f0d3099

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->e:Landroid/view/View;

    .line 1786286
    const v0, 0x7f0d309a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->f:Landroid/view/View;

    .line 1786287
    const v0, 0x7f0d309d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->g:Landroid/view/View;

    .line 1786288
    return-void
.end method


# virtual methods
.method public getFilmstripBorder()Landroid/view/View;
    .locals 1

    .prologue
    .line 1786278
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->f:Landroid/view/View;

    return-object v0
.end method

.method public getFilmstripLeftMask()Landroid/view/View;
    .locals 1

    .prologue
    .line 1786277
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->d:Landroid/view/View;

    return-object v0
.end method

.method public getFilmstripRightMask()Landroid/view/View;
    .locals 1

    .prologue
    .line 1786276
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->e:Landroid/view/View;

    return-object v0
.end method

.method public getFilmstripScrubber()Landroid/view/View;
    .locals 1

    .prologue
    .line 1786275
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->g:Landroid/view/View;

    return-object v0
.end method

.method public getStripView()Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;
    .locals 1

    .prologue
    .line 1786274
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->a:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    return-object v0
.end method

.method public getTrimmingEndHandle()Landroid/view/View;
    .locals 1

    .prologue
    .line 1786273
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->c:Landroid/view/View;

    return-object v0
.end method

.method public getTrimmingStartHandle()Landroid/view/View;
    .locals 1

    .prologue
    .line 1786272
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->b:Landroid/view/View;

    return-object v0
.end method
