.class public Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;
.super LX/BTV;
.source ""


# instance fields
.field private a:LX/BTR;

.field private b:LX/BTR;

.field public c:Z

.field private d:Landroid/animation/ObjectAnimator;

.field private e:LX/BTW;

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1787763
    invoke-direct {p0, p1}, LX/BTV;-><init>(Landroid/content/Context;)V

    .line 1787764
    invoke-direct {p0}, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->c()V

    .line 1787765
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1787760
    invoke-direct {p0, p1, p2}, LX/BTV;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1787761
    invoke-direct {p0}, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->c()V

    .line 1787762
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1787688
    invoke-direct {p0, p1, p2, p3}, LX/BTV;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1787689
    invoke-direct {p0}, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->c()V

    .line 1787690
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1787758
    const-string v0, "animationProgress"

    const/4 v1, 0x1

    new-array v1, v1, [I

    aput v2, v1, v2

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->d:Landroid/animation/ObjectAnimator;

    .line 1787759
    return-void
.end method

.method private d(I)V
    .locals 2

    .prologue
    .line 1787753
    sget-object v0, LX/BTW;->EXPAND:LX/BTW;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->e:LX/BTW;

    .line 1787754
    iput p1, p0, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->f:I

    .line 1787755
    iget-object v0, p0, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->d:Landroid/animation/ObjectAnimator;

    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setIntValues([I)V

    .line 1787756
    iget-object v0, p0, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->d:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 1787757
    return-void

    nop

    :array_0
    .array-data 4
        0x64
        0x0
    .end array-data
.end method

.method private e(I)V
    .locals 2

    .prologue
    .line 1787748
    sget-object v0, LX/BTW;->COLLAPSE:LX/BTW;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->e:LX/BTW;

    .line 1787749
    iput p1, p0, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->f:I

    .line 1787750
    iget-object v0, p0, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->d:Landroid/animation/ObjectAnimator;

    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setIntValues([I)V

    .line 1787751
    iget-object v0, p0, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->d:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 1787752
    return-void

    nop

    :array_0
    .array-data 4
        0x64
        0x0
    .end array-data
.end method


# virtual methods
.method public final a(F)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1787744
    iput-boolean v1, p0, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->c:Z

    .line 1787745
    iget-object v0, p0, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->a:LX/BTR;

    invoke-virtual {p0, v0, v1}, LX/BTV;->a(LX/BTR;I)V

    .line 1787746
    float-to-int v0, p1

    invoke-direct {p0, v0}, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->e(I)V

    .line 1787747
    return-void
.end method

.method public final a(LX/BTR;LX/BTR;)V
    .locals 2

    .prologue
    .line 1787740
    iput-object p1, p0, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->a:LX/BTR;

    .line 1787741
    iput-object p2, p0, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->b:LX/BTR;

    .line 1787742
    iget-object v0, p0, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->a:LX/BTR;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/BTV;->a(LX/BTR;I)V

    .line 1787743
    return-void
.end method

.method public final a(Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 1787731
    iget-object v0, p0, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->e:LX/BTW;

    sget-object v1, LX/BTW;->EXPAND:LX/BTW;

    if-ne v0, v1, :cond_1

    .line 1787732
    iget v0, p0, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->f:I

    iget v1, p1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    iget v1, p0, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->g:I

    mul-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x64

    .line 1787733
    iget v1, p1, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v0

    iput v1, p1, Landroid/graphics/Rect;->left:I

    .line 1787734
    iget v1, p1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 1787735
    :cond_0
    :goto_0
    return-void

    .line 1787736
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->e:LX/BTW;

    sget-object v1, LX/BTW;->COLLAPSE:LX/BTW;

    if-ne v0, v1, :cond_0

    .line 1787737
    iget v0, p1, Landroid/graphics/Rect;->left:I

    iget v1, p0, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->f:I

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    iget v1, p0, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->g:I

    mul-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x64

    .line 1787738
    iget v1, p1, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v0

    iput v1, p1, Landroid/graphics/Rect;->left:I

    .line 1787739
    iget v1, p1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->right:I

    goto :goto_0
.end method

.method public final a(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1787726
    invoke-virtual {p0}, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->getScrollX()I

    move-result v1

    .line 1787727
    if-gtz v1, :cond_0

    .line 1787728
    :goto_0
    return v0

    .line 1787729
    :cond_0
    sub-int/2addr v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->setScrollX(I)V

    .line 1787730
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(I)Z
    .locals 4

    .prologue
    .line 1787716
    invoke-virtual {p0}, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->getScrollX()I

    move-result v0

    .line 1787717
    invoke-virtual {p0}, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->getWidth()I

    move-result v1

    add-int/2addr v1, v0

    iget-object v2, p0, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->b:LX/BTR;

    .line 1787718
    iget v3, v2, LX/BTR;->b:I

    move v2, v3

    .line 1787719
    if-lt v1, v2, :cond_0

    .line 1787720
    const/4 v0, 0x0

    .line 1787721
    :goto_0
    return v0

    .line 1787722
    :cond_0
    add-int/2addr v0, p1

    iget-object v1, p0, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->b:LX/BTR;

    .line 1787723
    iget v2, v1, LX/BTR;->b:I

    move v1, v2

    .line 1787724
    invoke-virtual {p0}, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->setScrollX(I)V

    .line 1787725
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final c(I)V
    .locals 4

    .prologue
    .line 1787703
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->c:Z

    .line 1787704
    iget-object v0, p0, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->a:LX/BTR;

    .line 1787705
    iget v1, v0, LX/BTR;->c:I

    move v0, v1

    .line 1787706
    iget-object v1, p0, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->b:LX/BTR;

    .line 1787707
    iget v2, v1, LX/BTR;->c:I

    move v1, v2

    .line 1787708
    iget-object v2, p0, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->a:LX/BTR;

    .line 1787709
    iget v3, v2, LX/BTR;->d:I

    move v2, v3

    .line 1787710
    sub-int v2, p1, v2

    mul-int/2addr v1, v2

    div-int v0, v1, v0

    .line 1787711
    iget-object v1, p0, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->b:LX/BTR;

    sub-int v2, v0, p1

    iget-object v3, p0, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->a:LX/BTR;

    .line 1787712
    iget p1, v3, LX/BTR;->d:I

    move v3, p1

    .line 1787713
    add-int/2addr v2, v3

    invoke-virtual {p0, v1, v2}, LX/BTV;->a(LX/BTR;I)V

    .line 1787714
    invoke-direct {p0, v0}, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->d(I)V

    .line 1787715
    return-void
.end method

.method public final getAnimatedAlpha$134621()I
    .locals 1

    .prologue
    .line 1787702
    iget v0, p0, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->g:I

    rsub-int/lit8 v0, v0, 0x64

    mul-int/lit16 v0, v0, 0xff

    div-int/lit8 v0, v0, 0x64

    return v0
.end method

.method public getAnimationProgress()I
    .locals 1

    .prologue
    .line 1787701
    iget v0, p0, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->g:I

    return v0
.end method

.method public getZoomedInLeftOffset()I
    .locals 1

    .prologue
    .line 1787700
    invoke-virtual {p0}, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->getScrollX()I

    move-result v0

    neg-int v0, v0

    return v0
.end method

.method public getZoomedInStripContentWidth()I
    .locals 1

    .prologue
    .line 1787697
    iget-object v0, p0, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->b:LX/BTR;

    .line 1787698
    iget p0, v0, LX/BTR;->c:I

    move v0, p0

    .line 1787699
    return v0
.end method

.method public getZoomedInWidth()I
    .locals 1

    .prologue
    .line 1787694
    iget-object v0, p0, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->b:LX/BTR;

    .line 1787695
    iget p0, v0, LX/BTR;->b:I

    move v0, p0

    .line 1787696
    return v0
.end method

.method public getZoomedOutStripContentWidth()I
    .locals 1

    .prologue
    .line 1787691
    iget-object v0, p0, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->a:LX/BTR;

    .line 1787692
    iget p0, v0, LX/BTR;->c:I

    move v0, p0

    .line 1787693
    return v0
.end method

.method public getZoomedOutWidth()I
    .locals 1

    .prologue
    .line 1787687
    invoke-virtual {p0}, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->getWidth()I

    move-result v0

    return v0
.end method

.method public setAnimationProgress(I)V
    .locals 0

    .prologue
    .line 1787684
    iput p1, p0, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->g:I

    .line 1787685
    invoke-virtual {p0}, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->invalidate()V

    .line 1787686
    return-void
.end method
