.class public Lcom/facebook/video/creativeediting/ui/fresco/CropGridView;
.super Landroid/view/View;
.source ""


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/BTU;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1787574
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1787575
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/creativeediting/ui/fresco/CropGridView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1787576
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1787568
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1787569
    invoke-direct {p0, p1, p2}, Lcom/facebook/video/creativeediting/ui/fresco/CropGridView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1787570
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1787571
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1787572
    invoke-direct {p0, p1, p2}, Lcom/facebook/video/creativeediting/ui/fresco/CropGridView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1787573
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 1787559
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/video/creativeediting/ui/fresco/CropGridView;->d:Landroid/graphics/Paint;

    .line 1787560
    iget-object v0, p0, Lcom/facebook/video/creativeediting/ui/fresco/CropGridView;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1787561
    iget-object v0, p0, Lcom/facebook/video/creativeediting/ui/fresco/CropGridView;->d:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1787562
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, LX/03r;->CropGridView:[I

    invoke-virtual {v0, p2, v1, v2, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1787563
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/video/creativeediting/ui/fresco/CropGridView;->a:I

    .line 1787564
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/video/creativeediting/ui/fresco/CropGridView;->b:I

    .line 1787565
    new-instance v1, Ljava/util/ArrayList;

    iget v2, p0, Lcom/facebook/video/creativeediting/ui/fresco/CropGridView;->a:I

    iget v3, p0, Lcom/facebook/video/creativeediting/ui/fresco/CropGridView;->b:I

    add-int/2addr v2, v3

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/facebook/video/creativeediting/ui/fresco/CropGridView;->c:Ljava/util/List;

    .line 1787566
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1787567
    return-void
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    .line 1787556
    iget-object v0, p0, Lcom/facebook/video/creativeediting/ui/fresco/CropGridView;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BTU;

    .line 1787557
    iget v1, v0, LX/BTU;->a:I

    int-to-float v1, v1

    iget v2, v0, LX/BTU;->b:I

    int-to-float v2, v2

    iget v3, v0, LX/BTU;->c:I

    int-to-float v3, v3

    iget v0, v0, LX/BTU;->d:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/facebook/video/creativeediting/ui/fresco/CropGridView;->d:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 1787558
    :cond_0
    return-void
.end method

.method public final onSizeChanged(IIII)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x2

    const/16 v2, 0x2c

    const v3, 0x18620875

    invoke-static {v0, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1787536
    iget-object v0, p0, Lcom/facebook/video/creativeediting/ui/fresco/CropGridView;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1787537
    iget v0, p0, Lcom/facebook/video/creativeediting/ui/fresco/CropGridView;->b:I

    add-int/lit8 v0, v0, 0x1

    div-int v3, p1, v0

    .line 1787538
    iget v0, p0, Lcom/facebook/video/creativeediting/ui/fresco/CropGridView;->a:I

    add-int/lit8 v0, v0, 0x1

    div-int v4, p2, v0

    move v0, v1

    .line 1787539
    :goto_0
    iget v5, p0, Lcom/facebook/video/creativeediting/ui/fresco/CropGridView;->b:I

    if-ge v0, v5, :cond_0

    .line 1787540
    new-instance v5, LX/BTU;

    invoke-direct {v5}, LX/BTU;-><init>()V

    .line 1787541
    mul-int v6, v3, v0

    add-int/2addr v6, v3

    iput v6, v5, LX/BTU;->a:I

    .line 1787542
    iput v1, v5, LX/BTU;->b:I

    .line 1787543
    iget v6, v5, LX/BTU;->a:I

    iput v6, v5, LX/BTU;->c:I

    .line 1787544
    iput p2, v5, LX/BTU;->d:I

    .line 1787545
    iget-object v6, p0, Lcom/facebook/video/creativeediting/ui/fresco/CropGridView;->c:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1787546
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    .line 1787547
    :goto_1
    iget v3, p0, Lcom/facebook/video/creativeediting/ui/fresco/CropGridView;->a:I

    if-ge v0, v3, :cond_1

    .line 1787548
    new-instance v3, LX/BTU;

    invoke-direct {v3}, LX/BTU;-><init>()V

    .line 1787549
    iput v1, v3, LX/BTU;->a:I

    .line 1787550
    mul-int v5, v4, v0

    add-int/2addr v5, v4

    iput v5, v3, LX/BTU;->b:I

    .line 1787551
    iput p1, v3, LX/BTU;->c:I

    .line 1787552
    iget v5, v3, LX/BTU;->b:I

    iput v5, v3, LX/BTU;->d:I

    .line 1787553
    iget-object v5, p0, Lcom/facebook/video/creativeediting/ui/fresco/CropGridView;->c:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1787554
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1787555
    :cond_1
    const v0, -0xbddd4fc    # -5.13931E31f

    invoke-static {v0, v2}, LX/02F;->g(II)V

    return-void
.end method
