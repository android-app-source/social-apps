.class public final Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public a:J

.field public b:Landroid/net/Uri;

.field public c:Ljava/lang/String;

.field public d:LX/1aZ;

.field public e:LX/1aZ;

.field public f:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(JIILjava/io/File;LX/1Ad;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1787387
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1787388
    iput-wide p1, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;->a:J

    .line 1787389
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1, v1, p3, p4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;->f:Landroid/graphics/Rect;

    .line 1787390
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;->c:Ljava/lang/String;

    .line 1787391
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;->c:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;->b:Landroid/net/Uri;

    .line 1787392
    const-class v0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    invoke-virtual {p6, v0}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;->e:LX/1aZ;

    .line 1787393
    iget-object v0, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;->e:LX/1aZ;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;->d:LX/1aZ;

    .line 1787394
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1787395
    iget-object v0, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;->e:LX/1aZ;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;->d:LX/1aZ;

    .line 1787396
    return-void
.end method

.method public final a(LX/1Ad;Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 1787397
    const-class v0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;->d:LX/1aZ;

    .line 1787398
    return-void
.end method
