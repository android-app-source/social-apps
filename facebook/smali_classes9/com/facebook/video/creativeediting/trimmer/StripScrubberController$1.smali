.class public final Lcom/facebook/video/creativeediting/trimmer/StripScrubberController$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/BTL;


# direct methods
.method public constructor <init>(LX/BTL;I)V
    .locals 0

    .prologue
    .line 1787212
    iput-object p1, p0, Lcom/facebook/video/creativeediting/trimmer/StripScrubberController$1;->b:LX/BTL;

    iput p2, p0, Lcom/facebook/video/creativeediting/trimmer/StripScrubberController$1;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1787213
    iget-object v0, p0, Lcom/facebook/video/creativeediting/trimmer/StripScrubberController$1;->b:LX/BTL;

    iget-object v0, v0, LX/BTL;->f:LX/BSr;

    .line 1787214
    iget-object v1, v0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->k()Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->getCurrentPositionMs()I

    move-result v1

    move v0, v1

    .line 1787215
    iget-object v1, p0, Lcom/facebook/video/creativeediting/trimmer/StripScrubberController$1;->b:LX/BTL;

    iget-object v1, v1, LX/BTL;->e:LX/BTX;

    invoke-virtual {v1, v0}, LX/BTX;->a(I)I

    move-result v1

    iget-object v2, p0, Lcom/facebook/video/creativeediting/trimmer/StripScrubberController$1;->b:LX/BTL;

    iget-object v2, v2, LX/BTL;->g:LX/BTJ;

    invoke-virtual {v2}, LX/BTJ;->a()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1787216
    iget-object v2, p0, Lcom/facebook/video/creativeediting/trimmer/StripScrubberController$1;->b:LX/BTL;

    iget-object v2, v2, LX/BTL;->c:Landroid/view/View;

    iget-object v3, p0, Lcom/facebook/video/creativeediting/trimmer/StripScrubberController$1;->b:LX/BTL;

    invoke-virtual {v3}, LX/BTL;->e()I

    move-result v3

    sub-int/2addr v1, v3

    invoke-virtual {v2, v1}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 1787217
    iget-object v1, p0, Lcom/facebook/video/creativeediting/trimmer/StripScrubberController$1;->b:LX/BTL;

    iget-object v1, v1, LX/BTL;->f:LX/BSr;

    invoke-virtual {v1}, LX/BSr;->k()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/video/creativeediting/trimmer/StripScrubberController$1;->b:LX/BTL;

    iget-object v1, v1, LX/BTL;->e:LX/BTX;

    iget-object v2, p0, Lcom/facebook/video/creativeediting/trimmer/StripScrubberController$1;->b:LX/BTL;

    iget-object v2, v2, LX/BTL;->g:LX/BTJ;

    invoke-virtual {v2}, LX/BTJ;->b()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/BTX;->a(IZ)I

    move-result v1

    if-lt v0, v1, :cond_2

    .line 1787218
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/creativeediting/trimmer/StripScrubberController$1;->b:LX/BTL;

    iget-object v0, v0, LX/BTL;->d:LX/BT5;

    .line 1787219
    iget-object v1, v0, LX/BT5;->a:LX/BT9;

    .line 1787220
    iget-object v2, v1, LX/BT9;->r:LX/BTL;

    invoke-virtual {v2}, LX/BTL;->c()V

    .line 1787221
    iget-object v2, v1, LX/BT9;->b:LX/BSr;

    invoke-virtual {v2}, LX/BSr;->i()V

    .line 1787222
    iget-object v1, v0, LX/BT5;->a:LX/BT9;

    iget-boolean v1, v1, LX/BT9;->i:Z

    if-eqz v1, :cond_1

    .line 1787223
    iget-object v1, v0, LX/BT5;->a:LX/BT9;

    invoke-static {v1}, LX/BT9;->u$redex0(LX/BT9;)V

    .line 1787224
    :cond_1
    :goto_0
    return-void

    .line 1787225
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/creativeediting/trimmer/StripScrubberController$1;->b:LX/BTL;

    iget-object v0, v0, LX/BTL;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/video/creativeediting/trimmer/StripScrubberController$1;->b:LX/BTL;

    iget-object v1, v1, LX/BTL;->a:Ljava/lang/Runnable;

    iget v2, p0, Lcom/facebook/video/creativeediting/trimmer/StripScrubberController$1;->a:I

    int-to-long v2, v2

    const v4, -0x1d00a5e7

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0
.end method
