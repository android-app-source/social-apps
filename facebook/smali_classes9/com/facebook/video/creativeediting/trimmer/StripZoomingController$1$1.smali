.class public final Lcom/facebook/video/creativeediting/trimmer/StripZoomingController$1$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/video/creativeediting/trimmer/StripZoomingController$1;


# direct methods
.method public constructor <init>(Lcom/facebook/video/creativeediting/trimmer/StripZoomingController$1;)V
    .locals 0

    .prologue
    .line 1787308
    iput-object p1, p0, Lcom/facebook/video/creativeediting/trimmer/StripZoomingController$1$1;->a:Lcom/facebook/video/creativeediting/trimmer/StripZoomingController$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 1787309
    iget-object v0, p0, Lcom/facebook/video/creativeediting/trimmer/StripZoomingController$1$1;->a:Lcom/facebook/video/creativeediting/trimmer/StripZoomingController$1;

    iget-object v0, v0, Lcom/facebook/video/creativeediting/trimmer/StripZoomingController$1;->b:LX/BTP;

    iget-object v1, p0, Lcom/facebook/video/creativeediting/trimmer/StripZoomingController$1$1;->a:Lcom/facebook/video/creativeediting/trimmer/StripZoomingController$1;

    iget-object v1, v1, Lcom/facebook/video/creativeediting/trimmer/StripZoomingController$1;->b:LX/BTP;

    iget-object v1, v1, LX/BTP;->c:LX/BTJ;

    iget-object v2, p0, Lcom/facebook/video/creativeediting/trimmer/StripZoomingController$1$1;->a:Lcom/facebook/video/creativeediting/trimmer/StripZoomingController$1;

    iget-object v2, v2, Lcom/facebook/video/creativeediting/trimmer/StripZoomingController$1;->a:LX/BTI;

    invoke-virtual {v1, v2}, LX/BTJ;->a(LX/BTI;)I

    move-result v1

    iget-object v2, p0, Lcom/facebook/video/creativeediting/trimmer/StripZoomingController$1$1;->a:Lcom/facebook/video/creativeediting/trimmer/StripZoomingController$1;

    iget-object v2, v2, Lcom/facebook/video/creativeediting/trimmer/StripZoomingController$1;->a:LX/BTI;

    const/4 p0, 0x0

    .line 1787310
    iget-boolean v3, v0, LX/BTP;->h:Z

    if-eqz v3, :cond_0

    .line 1787311
    :goto_0
    return-void

    .line 1787312
    :cond_0
    const/4 v3, 0x1

    iput-boolean v3, v0, LX/BTP;->h:Z

    .line 1787313
    iget-object v3, v0, LX/BTP;->f:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    invoke-virtual {v3, v1}, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->c(I)V

    .line 1787314
    sget-object v3, LX/BTI;->LEFT:LX/BTI;

    if-ne v2, v3, :cond_1

    .line 1787315
    iget-object v3, v0, LX/BTP;->c:LX/BTJ;

    .line 1787316
    iget-object v4, v3, LX/BTJ;->f:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setLeft(I)V

    .line 1787317
    iget-object v3, v0, LX/BTP;->c:LX/BTJ;

    iget-object v4, v0, LX/BTP;->b:LX/BTl;

    iget-object v5, v0, LX/BTP;->c:LX/BTJ;

    invoke-virtual {v5}, LX/BTJ;->b()I

    move-result v5

    invoke-virtual {v4, v5}, LX/BTl;->b(I)I

    move-result v4

    invoke-virtual {v3, v4, p0}, LX/BTJ;->b(IZ)V

    .line 1787318
    :goto_1
    iget-object v3, v0, LX/BTP;->e:LX/BT7;

    .line 1787319
    iget-object v4, v3, LX/BT7;->a:LX/BT9;

    iget-object v4, v4, LX/BT9;->W:Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;

    .line 1787320
    sget-object v5, LX/BTI;->LEFT:LX/BTI;

    if-ne v2, v5, :cond_2

    .line 1787321
    iget v5, v4, Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;->c:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v4, Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;->c:I

    .line 1787322
    :goto_2
    iget-object v4, v3, LX/BT7;->a:LX/BT9;

    iget-object v4, v4, LX/BT9;->b:LX/BSr;

    iget-object v5, v3, LX/BT7;->a:LX/BT9;

    iget-object v5, v5, LX/BT9;->o:LX/BTX;

    const/4 p0, 0x0

    invoke-virtual {v5, v1, p0}, LX/BTX;->a(IZ)I

    move-result v5

    invoke-virtual {v4, v5}, LX/BSr;->a(I)V

    .line 1787323
    iget-object v3, v0, LX/BTP;->d:LX/BTL;

    invoke-virtual {v3}, LX/BTL;->c()V

    goto :goto_0

    .line 1787324
    :cond_1
    iget-object v3, v0, LX/BTP;->c:LX/BTJ;

    .line 1787325
    iget-object v4, v3, LX/BTJ;->g:Landroid/view/View;

    iget-object v5, v3, LX/BTJ;->c:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    invoke-virtual {v5}, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->getZoomedOutWidth()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setRight(I)V

    .line 1787326
    iget-object v3, v0, LX/BTP;->c:LX/BTJ;

    iget-object v4, v0, LX/BTP;->b:LX/BTl;

    iget-object v5, v0, LX/BTP;->c:LX/BTJ;

    invoke-virtual {v5}, LX/BTJ;->a()I

    move-result v5

    invoke-virtual {v4, v5}, LX/BTl;->b(I)I

    move-result v4

    invoke-virtual {v3, v4, p0}, LX/BTJ;->a(IZ)V

    goto :goto_1

    .line 1787327
    :cond_2
    iget v5, v4, Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;->d:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v4, Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;->d:I

    goto :goto_2
.end method
