.class public final Lcom/facebook/video/creativeediting/trimmer/VideoStripController$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:LX/0Px;

.field public final synthetic c:LX/BTS;


# direct methods
.method public constructor <init>(LX/BTS;LX/0Px;LX/0Px;)V
    .locals 0

    .prologue
    .line 1787386
    iput-object p1, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$3;->c:LX/BTS;

    iput-object p2, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$3;->a:LX/0Px;

    iput-object p3, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$3;->b:LX/0Px;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 14

    .prologue
    const/4 v11, 0x0

    .line 1787362
    iget-object v2, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$3;->c:LX/BTS;

    iget-object v2, v2, LX/BTS;->h:LX/7SE;

    invoke-virtual {v2}, LX/7SE;->a()LX/7SD;

    move-result-object v2

    .line 1787363
    iget-object v3, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$3;->c:LX/BTS;

    iget-object v3, v3, LX/BTS;->n:Landroid/net/Uri;

    if-eqz v3, :cond_0

    .line 1787364
    iget-object v3, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$3;->c:LX/BTS;

    iget-object v3, v3, LX/BTS;->n:Landroid/net/Uri;

    invoke-virtual {v2, v3}, LX/7SD;->a(Landroid/net/Uri;)LX/7SD;

    .line 1787365
    :cond_0
    iget-object v3, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$3;->c:LX/BTS;

    iget-object v3, v3, LX/BTS;->g:LX/BV0;

    iget-object v4, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$3;->c:LX/BTS;

    iget-object v4, v4, LX/BTS;->b:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual {v2}, LX/7SD;->a()LX/0Px;

    move-result-object v2

    iget-object v6, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$3;->c:LX/BTS;

    iget-object v6, v6, LX/BTS;->o:LX/7Sv;

    invoke-virtual {v3, v4, v5, v2, v6}, LX/BV0;->a(Landroid/net/Uri;Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;Ljava/util/List;LX/7Sv;)LX/BUz;

    move-result-object v2

    .line 1787366
    :try_start_0
    iget-object v3, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$3;->a:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v13

    move v12, v11

    :goto_0
    if-ge v12, v13, :cond_2

    iget-object v3, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$3;->a:LX/0Px;

    invoke-virtual {v3, v12}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;

    move-object v10, v0

    .line 1787367
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->isInterrupted()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_1

    .line 1787368
    invoke-virtual {v2}, LX/BUz;->a()V

    .line 1787369
    :goto_1
    return-void

    .line 1787370
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$3;->c:LX/BTS;

    iget-object v3, v3, LX/BTS;->e:LX/1FZ;

    iget-wide v4, v10, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;->a:J

    iget-object v6, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$3;->c:LX/BTS;

    iget v6, v6, LX/BTS;->m:F

    iget-object v7, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$3;->c:LX/BTS;

    iget v7, v7, LX/BTS;->s:I

    iget-object v8, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$3;->c:LX/BTS;

    iget v8, v8, LX/BTS;->t:I

    iget-object v9, v10, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;->c:Ljava/lang/String;

    invoke-static/range {v2 .. v9}, LX/BTS;->a(LX/BUz;LX/1FZ;JFIILjava/lang/String;)V

    .line 1787371
    invoke-virtual {v10}, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;->a()V

    .line 1787372
    iget-object v3, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$3;->c:LX/BTS;

    invoke-static {v3}, LX/BTS;->e(LX/BTS;)V

    .line 1787373
    add-int/lit8 v3, v12, 0x1

    move v12, v3

    goto :goto_0

    .line 1787374
    :cond_2
    iget-object v3, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$3;->b:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v12

    :goto_2
    if-ge v11, v12, :cond_5

    iget-object v3, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$3;->b:LX/0Px;

    invoke-virtual {v3, v11}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;

    move-object v10, v0

    .line 1787375
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->isInterrupted()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-eqz v3, :cond_3

    .line 1787376
    invoke-virtual {v2}, LX/BUz;->a()V

    goto :goto_1

    .line 1787377
    :cond_3
    :try_start_2
    iget-object v3, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$3;->c:LX/BTS;

    iget-object v3, v3, LX/BTS;->e:LX/1FZ;

    iget-wide v4, v10, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;->a:J

    iget-object v6, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$3;->c:LX/BTS;

    iget v6, v6, LX/BTS;->m:F

    iget-object v7, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$3;->c:LX/BTS;

    iget v7, v7, LX/BTS;->s:I

    iget-object v8, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$3;->c:LX/BTS;

    iget v8, v8, LX/BTS;->t:I

    iget-object v9, v10, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;->c:Ljava/lang/String;

    invoke-static/range {v2 .. v9}, LX/BTS;->a(LX/BUz;LX/1FZ;JFIILjava/lang/String;)V

    .line 1787378
    invoke-virtual {v10}, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$BitmapData;->a()V

    .line 1787379
    iget-object v3, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$3;->c:LX/BTS;

    iget-object v3, v3, LX/BTS;->p:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$3;->c:LX/BTS;

    iget-object v3, v3, LX/BTS;->p:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    .line 1787380
    iget-boolean v0, v3, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->c:Z

    move v3, v0

    .line 1787381
    if-eqz v3, :cond_4

    .line 1787382
    iget-object v3, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$3;->c:LX/BTS;

    invoke-static {v3}, LX/BTS;->e(LX/BTS;)V

    .line 1787383
    :cond_4
    add-int/lit8 v3, v11, 0x1

    move v11, v3

    goto :goto_2

    .line 1787384
    :cond_5
    iget-object v3, p0, Lcom/facebook/video/creativeediting/trimmer/VideoStripController$3;->c:LX/BTS;

    invoke-static {v3}, LX/BTS;->e(LX/BTS;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1787385
    invoke-virtual {v2}, LX/BUz;->a()V

    goto :goto_1

    :catchall_0
    move-exception v3

    invoke-virtual {v2}, LX/BUz;->a()V

    throw v3
.end method
