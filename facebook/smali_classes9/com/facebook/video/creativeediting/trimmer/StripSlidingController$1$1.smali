.class public final Lcom/facebook/video/creativeediting/trimmer/StripSlidingController$1$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/video/creativeediting/trimmer/StripSlidingController$1;


# direct methods
.method public constructor <init>(Lcom/facebook/video/creativeediting/trimmer/StripSlidingController$1;)V
    .locals 0

    .prologue
    .line 1787254
    iput-object p1, p0, Lcom/facebook/video/creativeediting/trimmer/StripSlidingController$1$1;->a:Lcom/facebook/video/creativeediting/trimmer/StripSlidingController$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1787255
    iget-object v0, p0, Lcom/facebook/video/creativeediting/trimmer/StripSlidingController$1$1;->a:Lcom/facebook/video/creativeediting/trimmer/StripSlidingController$1;

    iget-object v0, v0, Lcom/facebook/video/creativeediting/trimmer/StripSlidingController$1;->c:LX/BTN;

    iget-boolean v0, v0, LX/BTN;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/creativeediting/trimmer/StripSlidingController$1$1;->a:Lcom/facebook/video/creativeediting/trimmer/StripSlidingController$1;

    iget-object v0, v0, Lcom/facebook/video/creativeediting/trimmer/StripSlidingController$1;->c:LX/BTN;

    iget-object v0, v0, LX/BTN;->d:LX/BTZ;

    iget-object v1, p0, Lcom/facebook/video/creativeediting/trimmer/StripSlidingController$1$1;->a:Lcom/facebook/video/creativeediting/trimmer/StripSlidingController$1;

    iget-object v1, v1, Lcom/facebook/video/creativeediting/trimmer/StripSlidingController$1;->c:LX/BTN;

    iget-object v1, v1, LX/BTN;->c:LX/BTX;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/facebook/video/creativeediting/trimmer/StripSlidingController$1$1;->a:Lcom/facebook/video/creativeediting/trimmer/StripSlidingController$1;

    iget-object v3, v3, Lcom/facebook/video/creativeediting/trimmer/StripSlidingController$1;->c:LX/BTN;

    iget-object v3, v3, LX/BTN;->b:LX/BTJ;

    invoke-virtual {v3}, LX/BTJ;->a()I

    move-result v3

    iget-object v4, p0, Lcom/facebook/video/creativeediting/trimmer/StripSlidingController$1$1;->a:Lcom/facebook/video/creativeediting/trimmer/StripSlidingController$1;

    iget-object v4, v4, Lcom/facebook/video/creativeediting/trimmer/StripSlidingController$1;->c:LX/BTN;

    iget-object v4, v4, LX/BTN;->b:LX/BTJ;

    invoke-virtual {v4}, LX/BTJ;->b()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, LX/BTX;->a(ZII)I

    move-result v1

    invoke-virtual {v0, v1}, LX/BTZ;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1787256
    :cond_0
    :goto_0
    return-void

    .line 1787257
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/creativeediting/trimmer/StripSlidingController$1$1;->a:Lcom/facebook/video/creativeediting/trimmer/StripSlidingController$1;

    iget-object v0, v0, Lcom/facebook/video/creativeediting/trimmer/StripSlidingController$1;->c:LX/BTN;

    iget-object v1, p0, Lcom/facebook/video/creativeediting/trimmer/StripSlidingController$1$1;->a:Lcom/facebook/video/creativeediting/trimmer/StripSlidingController$1;

    iget-object v1, v1, Lcom/facebook/video/creativeediting/trimmer/StripSlidingController$1;->a:LX/BTM;

    iget-object v2, p0, Lcom/facebook/video/creativeediting/trimmer/StripSlidingController$1$1;->a:Lcom/facebook/video/creativeediting/trimmer/StripSlidingController$1;

    iget-object v2, v2, Lcom/facebook/video/creativeediting/trimmer/StripSlidingController$1;->b:LX/BTI;

    const/16 v3, 0xa

    .line 1787258
    sget-object v4, LX/BTM;->LEFT:LX/BTM;

    if-ne v1, v4, :cond_5

    .line 1787259
    iget-object v4, v0, LX/BTN;->f:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    invoke-virtual {v4, v3}, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->a(I)Z

    move-result v4

    .line 1787260
    if-nez v4, :cond_3

    .line 1787261
    :cond_2
    :goto_1
    goto :goto_0

    .line 1787262
    :cond_3
    sget-object v4, LX/BTI;->LEFT:LX/BTI;

    if-ne v2, v4, :cond_4

    .line 1787263
    iget-object v4, v0, LX/BTN;->b:LX/BTJ;

    invoke-virtual {v4, v3}, LX/BTJ;->b(I)V

    .line 1787264
    :goto_2
    iget-object v4, v0, LX/BTN;->e:LX/BT6;

    const/4 v1, 0x1

    .line 1787265
    iget-object p0, v4, LX/BT6;->a:LX/BT9;

    iget-object p0, p0, LX/BT9;->r:LX/BTL;

    invoke-virtual {p0}, LX/BTL;->c()V

    .line 1787266
    sget-object p0, LX/BTI;->LEFT:LX/BTI;

    if-ne v2, p0, :cond_7

    .line 1787267
    iget-object p0, v4, LX/BT6;->a:LX/BT9;

    iget-object v0, v4, LX/BT6;->a:LX/BT9;

    iget-object v0, v0, LX/BT9;->q:LX/BTJ;

    invoke-virtual {v0}, LX/BTJ;->a()I

    move-result v0

    invoke-static {p0, v0, v1}, LX/BT9;->a$redex0(LX/BT9;IZ)V

    .line 1787268
    iget-object p0, v4, LX/BT6;->a:LX/BT9;

    iget-object v0, v4, LX/BT6;->a:LX/BT9;

    iget-object v0, v0, LX/BT9;->L:LX/BT8;

    invoke-static {p0, v0}, LX/BT9;->a$redex0(LX/BT9;LX/0hs;)V

    .line 1787269
    :goto_3
    iget-object p0, v4, LX/BT6;->a:LX/BT9;

    invoke-static {p0}, LX/BT9;->z(LX/BT9;)V

    .line 1787270
    goto :goto_1

    .line 1787271
    :cond_4
    iget-object v4, v0, LX/BTN;->b:LX/BTJ;

    invoke-virtual {v4, v3}, LX/BTJ;->a(I)V

    goto :goto_2

    .line 1787272
    :cond_5
    iget-object v4, v0, LX/BTN;->f:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    invoke-virtual {v4, v3}, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->b(I)Z

    move-result v4

    .line 1787273
    if-eqz v4, :cond_2

    .line 1787274
    sget-object v4, LX/BTI;->LEFT:LX/BTI;

    if-ne v2, v4, :cond_6

    .line 1787275
    iget-object v4, v0, LX/BTN;->b:LX/BTJ;

    neg-int p0, v3

    invoke-virtual {v4, p0}, LX/BTJ;->b(I)V

    goto :goto_2

    .line 1787276
    :cond_6
    iget-object v4, v0, LX/BTN;->b:LX/BTJ;

    neg-int p0, v3

    invoke-virtual {v4, p0}, LX/BTJ;->a(I)V

    goto :goto_2

    .line 1787277
    :cond_7
    iget-object p0, v4, LX/BT6;->a:LX/BT9;

    iget-object v0, v4, LX/BT6;->a:LX/BT9;

    iget-object v0, v0, LX/BT9;->q:LX/BTJ;

    invoke-virtual {v0}, LX/BTJ;->b()I

    move-result v0

    invoke-static {p0, v0, v1}, LX/BT9;->b$redex0(LX/BT9;IZ)V

    .line 1787278
    iget-object p0, v4, LX/BT6;->a:LX/BT9;

    iget-object v0, v4, LX/BT6;->a:LX/BT9;

    iget-object v0, v0, LX/BT9;->M:LX/BT8;

    invoke-static {p0, v0}, LX/BT9;->a$redex0(LX/BT9;LX/0hs;)V

    goto :goto_3
.end method
