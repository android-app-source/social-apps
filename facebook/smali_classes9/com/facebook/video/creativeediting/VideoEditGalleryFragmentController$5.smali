.class public final Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/BSr;


# direct methods
.method public constructor <init>(LX/BSr;)V
    .locals 0

    .prologue
    .line 1785918
    iput-object p1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$5;->a:LX/BSr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    .prologue
    .line 1785919
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$5;->a:LX/BSr;

    iget-object v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$5;->a:LX/BSr;

    iget-object v1, v1, LX/BSr;->e:LX/2MV;

    iget-object v2, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$5;->a:LX/BSr;

    iget-object v2, v2, LX/BSr;->j:Landroid/net/Uri;

    invoke-interface {v1, v2}, LX/2MV;->a(Landroid/net/Uri;)LX/60x;

    move-result-object v1

    .line 1785920
    iput-object v1, v0, LX/BSr;->n:LX/60x;

    .line 1785921
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$5;->a:LX/BSr;

    .line 1785922
    iget-object v5, v0, LX/BSr;->b:Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    .line 1785923
    iget-boolean v6, v5, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->j:Z

    move v5, v6

    .line 1785924
    if-eqz v5, :cond_1

    .line 1785925
    iget-object v5, v0, LX/BSr;->s:LX/BTi;

    if-nez v5, :cond_0

    .line 1785926
    iget-object v5, v0, LX/BSr;->n:LX/60x;

    iget v5, v5, LX/60x;->d:I

    rem-int/lit16 v5, v5, 0xb4

    if-nez v5, :cond_2

    iget-object v5, v0, LX/BSr;->n:LX/60x;

    iget v5, v5, LX/60x;->b:I

    int-to-float v5, v5

    iget-object v6, v0, LX/BSr;->n:LX/60x;

    iget v6, v6, LX/60x;->c:I

    int-to-float v6, v6

    div-float/2addr v5, v6

    .line 1785927
    :goto_0
    iget-object v6, v0, LX/BSr;->r:LX/BTj;

    iget-object v7, v0, LX/BSr;->j:Landroid/net/Uri;

    iget-object v8, v0, LX/BSr;->n:LX/60x;

    iget-wide v9, v8, LX/60x;->a:J

    invoke-virtual {v6, v7, v9, v10, v5}, LX/BTj;->a(Landroid/net/Uri;JF)LX/BTi;

    move-result-object v5

    iput-object v5, v0, LX/BSr;->s:LX/BTi;

    .line 1785928
    :cond_0
    iget-object v5, v0, LX/BSr;->C:Ljava/util/concurrent/Future;

    if-nez v5, :cond_1

    .line 1785929
    iget-object v5, v0, LX/BSr;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v6, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$7;

    invoke-direct {v6, v0}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$7;-><init>(LX/BSr;)V

    const v7, -0x7fae8d6d

    invoke-static {v5, v6, v7}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    move-result-object v5

    iput-object v5, v0, LX/BSr;->C:Ljava/util/concurrent/Future;

    .line 1785930
    :cond_1
    iget-object v3, v0, LX/BSr;->c:LX/0Sh;

    new-instance v4, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$6;

    invoke-direct {v4, v0}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$6;-><init>(LX/BSr;)V

    invoke-virtual {v3, v4}, LX/0Sh;->a(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1785931
    :goto_1
    return-void

    .line 1785932
    :catch_0
    move-exception v0

    .line 1785933
    iget-object v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$5;->a:LX/BSr;

    iget-object v1, v1, LX/BSr;->g:LX/03V;

    const-string v2, "Failed to extract video metadata"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1785934
    :cond_2
    iget-object v5, v0, LX/BSr;->n:LX/60x;

    iget v5, v5, LX/60x;->c:I

    int-to-float v5, v5

    iget-object v6, v0, LX/BSr;->n:LX/60x;

    iget v6, v6, LX/60x;->b:I

    int-to-float v6, v6

    div-float/2addr v5, v6

    goto :goto_0
.end method
