.class public Lcom/facebook/video/creativeediting/VideoEditGalleryPreviewLayout;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;

.field public b:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

.field public c:Landroid/view/ViewStub;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1786263
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1786264
    invoke-direct {p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryPreviewLayout;->a()V

    .line 1786265
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1786266
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1786267
    invoke-direct {p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryPreviewLayout;->a()V

    .line 1786268
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1786252
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1786253
    invoke-direct {p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryPreviewLayout;->a()V

    .line 1786254
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1786255
    const v0, 0x7f03158d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1786256
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/video/creativeediting/VideoEditGalleryPreviewLayout;->setOrientation(I)V

    .line 1786257
    const v0, 0x7f0d3093

    invoke-virtual {p0, v0}, Lcom/facebook/video/creativeediting/VideoEditGalleryPreviewLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryPreviewLayout;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;

    .line 1786258
    const v0, 0x7f0d3091

    invoke-virtual {p0, v0}, Lcom/facebook/video/creativeediting/VideoEditGalleryPreviewLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryPreviewLayout;->b:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    .line 1786259
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryPreviewLayout;->b:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    iget-object v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryPreviewLayout;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;

    .line 1786260
    iput-object v1, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->j:Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;

    .line 1786261
    const v0, 0x7f0d3092

    invoke-virtual {p0, v0}, Lcom/facebook/video/creativeediting/VideoEditGalleryPreviewLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryPreviewLayout;->c:Landroid/view/ViewStub;

    .line 1786262
    return-void
.end method


# virtual methods
.method public getFilmstrip()Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;
    .locals 1

    .prologue
    .line 1786251
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryPreviewLayout;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;

    return-object v0
.end method

.method public getInlineMetadataView()Landroid/view/ViewStub;
    .locals 1

    .prologue
    .line 1786250
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryPreviewLayout;->c:Landroid/view/ViewStub;

    return-object v0
.end method

.method public getVideoPreviewView()Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;
    .locals 1

    .prologue
    .line 1786249
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryPreviewLayout;->b:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    return-object v0
.end method
