.class public Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# instance fields
.field public A:LX/0wL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private B:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/BTd;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/BTg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public a:Landroid/widget/LinearLayout;

.field private b:Landroid/widget/LinearLayout;

.field private c:Lcom/facebook/fbui/glyph/GlyphView;

.field private d:Lcom/facebook/resources/ui/FbTextView;

.field private e:Lcom/facebook/fbui/glyph/GlyphView;

.field private f:Lcom/facebook/resources/ui/FbTextView;

.field public g:Lcom/facebook/video/player/RichVideoPlayer;

.field private h:Landroid/graphics/Matrix;

.field public i:LX/BSm;

.field public j:Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;

.field public k:Landroid/view/View;

.field public l:Lcom/facebook/widget/FbImageView;

.field public m:Lcom/facebook/widget/FbImageView;

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Landroid/net/Uri;

.field public p:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:I

.field private r:I

.field public s:I

.field private final t:[F

.field public u:Landroid/graphics/RectF;

.field public v:Z

.field public w:Z

.field public x:Z

.field public y:Z

.field public z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1786456
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 1786457
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->t:[F

    .line 1786458
    iput-boolean v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->v:Z

    .line 1786459
    iput-boolean v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->w:Z

    .line 1786460
    iput-boolean v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->x:Z

    .line 1786461
    iput-boolean v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->y:Z

    .line 1786462
    iput-boolean v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->z:Z

    .line 1786463
    invoke-direct {p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g()V

    .line 1786464
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1786465
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1786466
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->t:[F

    .line 1786467
    iput-boolean v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->v:Z

    .line 1786468
    iput-boolean v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->w:Z

    .line 1786469
    iput-boolean v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->x:Z

    .line 1786470
    iput-boolean v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->y:Z

    .line 1786471
    iput-boolean v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->z:Z

    .line 1786472
    invoke-direct {p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g()V

    .line 1786473
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1786474
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1786475
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->t:[F

    .line 1786476
    iput-boolean v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->v:Z

    .line 1786477
    iput-boolean v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->w:Z

    .line 1786478
    iput-boolean v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->x:Z

    .line 1786479
    iput-boolean v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->y:Z

    .line 1786480
    iput-boolean v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->z:Z

    .line 1786481
    invoke-direct {p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g()V

    .line 1786482
    return-void
.end method

.method private a(F)V
    .locals 3

    .prologue
    .line 1786483
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->k:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1786484
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->k:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setY(F)V

    .line 1786485
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->j:Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;

    iget-object v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->k:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getY()F

    move-result v1

    iget-object v2, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->k:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->setY(F)V

    .line 1786486
    :goto_0
    return-void

    .line 1786487
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->j:Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;

    invoke-virtual {p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0c4a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v1, p1

    invoke-virtual {v0, v1}, Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;->setY(F)V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;LX/0wL;LX/0ad;LX/BTd;LX/BTg;)V
    .locals 0

    .prologue
    .line 1786488
    iput-object p1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->A:LX/0wL;

    iput-object p2, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->B:LX/0ad;

    iput-object p3, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->C:LX/BTd;

    iput-object p4, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->D:LX/BTg;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    invoke-static {v3}, LX/0wL;->a(LX/0QB;)LX/0wL;

    move-result-object v0

    check-cast v0, LX/0wL;

    invoke-static {v3}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    const-class v2, LX/BTd;

    invoke-interface {v3, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/BTd;

    invoke-static {v3}, LX/BTg;->b(LX/0QB;)LX/BTg;

    move-result-object v3

    check-cast v3, LX/BTg;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->a(Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;LX/0wL;LX/0ad;LX/BTd;LX/BTg;)V

    return-void
.end method

.method private b(I)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1786489
    iget-object v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    const-string v2, "rotation"

    const/4 v3, 0x2

    new-array v3, v3, [F

    iget v4, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->s:I

    int-to-float v4, v4

    aput v4, v3, v0

    const/4 v4, 0x1

    int-to-float v5, p1

    aput v5, v3, v4

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 1786490
    const-wide/16 v2, 0x64

    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1786491
    new-instance v2, LX/BSy;

    invoke-direct {v2, p0}, LX/BSy;-><init>(Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;)V

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1786492
    const/16 v2, -0x168

    if-gt p1, v2, :cond_0

    move p1, v0

    :cond_0
    iput p1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->s:I

    .line 1786493
    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    .line 1786494
    return-void
.end method

.method private g()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 1786495
    const v0, 0x7f031591

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1786496
    const v0, 0x7f0d0917

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1786497
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v1, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-virtual {p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    .line 1786498
    invoke-static {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1786499
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v1, Lcom/facebook/video/player/plugins/OverlayImagePlugin;

    invoke-virtual {p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/OverlayImagePlugin;-><init>(Landroid/content/Context;)V

    .line 1786500
    invoke-static {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1786501
    const v0, 0x7f0d1cb2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->a:Landroid/widget/LinearLayout;

    .line 1786502
    const v0, 0x7f0d30a4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->c:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1786503
    const v0, 0x7f0d30a5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 1786504
    const v0, 0x7f0d30a6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->b:Landroid/widget/LinearLayout;

    .line 1786505
    const v0, 0x7f0d30a7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->e:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1786506
    const v0, 0x7f0d30a8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 1786507
    const v0, 0x7f0d3038

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbImageView;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->l:Lcom/facebook/widget/FbImageView;

    .line 1786508
    const v0, 0x7f0d30a1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbImageView;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->m:Lcom/facebook/widget/FbImageView;

    .line 1786509
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1786510
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->b:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1786511
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->l:Lcom/facebook/widget/FbImageView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/FbImageView;->setVisibility(I)V

    .line 1786512
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->s:I

    .line 1786513
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->h:Landroid/graphics/Matrix;

    .line 1786514
    const-class v0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    invoke-static {v0, p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1786515
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->a:Landroid/widget/LinearLayout;

    new-instance v1, LX/BSv;

    invoke-direct {v1, p0}, LX/BSv;-><init>(Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1786516
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->b:Landroid/widget/LinearLayout;

    new-instance v1, LX/BSw;

    invoke-direct {v1, p0}, LX/BSw;-><init>(Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1786517
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v1, LX/BSx;

    invoke-direct {v1, p0}, LX/BSx;-><init>(Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1786518
    return-void
.end method

.method private getOriginalVideoAspectRatio()F
    .locals 2

    .prologue
    .line 1786519
    iget v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->q:I

    int-to-float v0, v0

    iget v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->r:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method private getVideoHeight()I
    .locals 1

    .prologue
    .line 1786581
    iget v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->s:I

    rem-int/lit16 v0, v0, 0xb4

    if-nez v0, :cond_0

    iget v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->r:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->q:I

    goto :goto_0
.end method

.method private getVideoWidth()I
    .locals 1

    .prologue
    .line 1786520
    iget v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->s:I

    rem-int/lit16 v0, v0, 0xb4

    if-nez v0, :cond_0

    iget v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->q:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->r:I

    goto :goto_0
.end method

.method private getViewAspectRatio()F
    .locals 1

    .prologue
    .line 1786521
    iget-boolean v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->y:Z

    if-eqz v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->getViewAspectRatioAfterRotation()F

    move-result v0

    goto :goto_0
.end method

.method private getViewAspectRatioAfterRotation()F
    .locals 2

    .prologue
    .line 1786522
    invoke-direct {p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->getVideoWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-direct {p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->getVideoHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method public static h(Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;)V
    .locals 3

    .prologue
    .line 1786523
    iget-boolean v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->v:Z

    if-eqz v0, :cond_0

    .line 1786524
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    const/4 v1, 0x1

    sget-object v2, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 1786525
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->c:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020a26

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1786526
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0813e5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1786527
    :goto_0
    return-void

    .line 1786528
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    const/4 v1, 0x0

    sget-object v2, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 1786529
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->c:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020a23

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1786530
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0813e4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static i$redex0(Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;)V
    .locals 3

    .prologue
    .line 1786531
    iget-boolean v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->w:Z

    if-eqz v0, :cond_0

    .line 1786532
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0813e9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1786533
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->e:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1786534
    :goto_0
    return-void

    .line 1786535
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0813ea

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1786536
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->e:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1786537
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04D;->VIDEO_EDITING_GALLERY:LX/04D;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerOrigin(LX/04D;)V

    .line 1786538
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    iget-boolean v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->y:Z

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setShouldCropToFit(Z)V

    .line 1786539
    invoke-static {}, Lcom/facebook/video/engine/VideoDataSource;->newBuilder()LX/2oE;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->o:Landroid/net/Uri;

    .line 1786540
    iput-object v1, v0, LX/2oE;->a:Landroid/net/Uri;

    .line 1786541
    move-object v0, v0

    .line 1786542
    sget-object v1, LX/097;->FROM_LOCAL_STORAGE:LX/097;

    .line 1786543
    iput-object v1, v0, LX/2oE;->e:LX/097;

    .line 1786544
    move-object v1, v0

    .line 1786545
    iget-boolean v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->z:Z

    if-eqz v0, :cond_1

    sget-object v0, LX/2oF;->MIRROR_HORIZONTALLY:LX/2oF;

    .line 1786546
    :goto_0
    iput-object v0, v1, LX/2oE;->g:LX/2oF;

    .line 1786547
    move-object v0, v1

    .line 1786548
    invoke-virtual {v0}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v0

    .line 1786549
    invoke-static {}, Lcom/facebook/video/engine/VideoPlayerParams;->newBuilder()LX/2oH;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/2oH;->a(Lcom/facebook/video/engine/VideoDataSource;)LX/2oH;

    move-result-object v0

    const/4 v1, 0x1

    .line 1786550
    iput-boolean v1, v0, LX/2oH;->t:Z

    .line 1786551
    move-object v0, v0

    .line 1786552
    iput-boolean v4, v0, LX/2oH;->g:Z

    .line 1786553
    move-object v0, v0

    .line 1786554
    iput-boolean v4, v0, LX/2oH;->o:Z

    .line 1786555
    move-object v0, v0

    .line 1786556
    new-instance v1, LX/2pZ;

    invoke-direct {v1}, LX/2pZ;-><init>()V

    .line 1786557
    invoke-virtual {v0}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v0

    .line 1786558
    iput-object v0, v1, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1786559
    move-object v0, v1

    .line 1786560
    invoke-direct {p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->getOriginalVideoAspectRatio()F

    move-result v2

    float-to-double v2, v2

    .line 1786561
    iput-wide v2, v0, LX/2pZ;->e:D

    .line 1786562
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->p:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 1786563
    const-string v0, "OverlayImageParamsKey"

    iget-object v2, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->p:Landroid/net/Uri;

    invoke-virtual {v1, v0, v2}, LX/2pZ;->a(Ljava/lang/String;Ljava/lang/Object;)LX/2pZ;

    .line 1786564
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, LX/2pZ;->b()LX/2pa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 1786565
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->u:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setCropRect(Landroid/graphics/RectF;)V

    .line 1786566
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1786567
    invoke-static {p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->h(Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;)V

    .line 1786568
    invoke-static {p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->i$redex0(Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;)V

    .line 1786569
    return-void

    .line 1786570
    :cond_1
    sget-object v0, LX/2oF;->NONE:LX/2oF;

    goto :goto_0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 1786571
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(ILX/04g;)V

    .line 1786572
    return-void
.end method

.method public final a(II)V
    .locals 0

    .prologue
    .line 1786573
    iput p1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->q:I

    .line 1786574
    iput p2, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->r:I

    .line 1786575
    invoke-virtual {p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->requestLayout()V

    .line 1786576
    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 1786577
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->m:Lcom/facebook/widget/FbImageView;

    invoke-virtual {v0}, Lcom/facebook/widget/FbImageView;->isShown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1786578
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->m:Lcom/facebook/widget/FbImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setVisibility(I)V

    .line 1786579
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->m:Lcom/facebook/widget/FbImageView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/FbImageView;->setImageURI(Landroid/net/Uri;)V

    .line 1786580
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1786448
    const-string v0, "high"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->w:Z

    .line 1786449
    invoke-static {p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->i$redex0(Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;)V

    .line 1786450
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1786451
    return-void

    :cond_0
    move v0, v1

    .line 1786452
    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1786453
    iget v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->s:I

    add-int/lit8 v0, v0, -0x5a

    .line 1786454
    invoke-direct {p0, v0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->b(I)V

    .line 1786455
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1786353
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->x:Z

    .line 1786354
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->l:Lcom/facebook/widget/FbImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setVisibility(I)V

    .line 1786355
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1786356
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v0, v1

    .line 1786357
    iget-object v1, v0, LX/2pb;->y:LX/2qV;

    move-object v0, v1

    .line 1786358
    invoke-virtual {v0}, LX/2qV;->isPlayingState()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1786359
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 1786360
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->d()V

    .line 1786361
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1786362
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->m:Lcom/facebook/widget/FbImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setVisibility(I)V

    .line 1786363
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1786364
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1786365
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v0, v1

    .line 1786366
    iget-object v1, v0, LX/2pb;->y:LX/2qV;

    move-object v0, v1

    .line 1786367
    invoke-virtual {v0}, LX/2qV;->isPlayingState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1786368
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 1786369
    :cond_0
    return-void
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 1786370
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->s()Z

    move-result v0

    return v0
.end method

.method public getCurrentPositionMs()I
    .locals 1

    .prologue
    .line 1786371
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1786372
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getVideoDurationMs()I

    move-result v0

    .line 1786373
    :goto_0
    return v0

    .line 1786374
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1786375
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getLastStartPosition()I

    move-result v0

    goto :goto_0

    .line 1786376
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v0

    goto :goto_0
.end method

.method public getShouldUploadVideoInHD()Z
    .locals 1

    .prologue
    .line 1786377
    iget-boolean v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->w:Z

    return v0
.end method

.method public getVideoDurationMs()I
    .locals 1

    .prologue
    .line 1786378
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getVideoDurationMs()I

    move-result v0

    return v0
.end method

.method public getVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;
    .locals 1

    .prologue
    .line 1786379
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    return-object v0
.end method

.method public final onLayout(ZIIII)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1786380
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomViewGroup;->onLayout(ZIIII)V

    .line 1786381
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->t:[F

    iget-object v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->getX()F

    move-result v1

    aput v1, v0, v6

    .line 1786382
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->t:[F

    iget-object v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->getY()F

    move-result v1

    aput v1, v0, v5

    .line 1786383
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->h:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 1786384
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->h:Landroid/graphics/Matrix;

    iget v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->s:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v2}, Lcom/facebook/video/player/RichVideoPlayer;->getX()F

    move-result v2

    iget-object v3, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v3}, Lcom/facebook/video/player/RichVideoPlayer;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v3}, Lcom/facebook/video/player/RichVideoPlayer;->getY()F

    move-result v3

    iget-object v4, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v4}, Lcom/facebook/video/player/RichVideoPlayer;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    add-float/2addr v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 1786385
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->h:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->t:[F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 1786386
    iget v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->s:I

    const/16 v1, -0x5a

    if-ne v0, v1, :cond_1

    .line 1786387
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->t:[F

    aget v2, v0, v6

    .line 1786388
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->t:[F

    aget v1, v0, v5

    .line 1786389
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->t:[F

    aget v0, v0, v5

    iget-object v3, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v3}, Lcom/facebook/video/player/RichVideoPlayer;->getWidth()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v0, v3

    .line 1786390
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0c30

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    .line 1786391
    iget-object v4, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->a:Landroid/widget/LinearLayout;

    add-float v5, v2, v3

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setX(F)V

    .line 1786392
    iget-object v4, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->a:Landroid/widget/LinearLayout;

    add-float/2addr v0, v3

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->setY(F)V

    .line 1786393
    invoke-virtual {p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b0c4b

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 1786394
    iget-object v3, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->b:Landroid/widget/LinearLayout;

    add-float/2addr v2, v0

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->setX(F)V

    .line 1786395
    iget-object v2, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->b:Landroid/widget/LinearLayout;

    sub-float v0, v1, v0

    iget-object v3, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v0, v3

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setY(F)V

    .line 1786396
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->j:Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;

    if-eqz v0, :cond_0

    .line 1786397
    invoke-direct {p0, v1}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->a(F)V

    .line 1786398
    :cond_0
    return-void

    .line 1786399
    :cond_1
    iget v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->s:I

    const/16 v1, -0x10e

    if-ne v0, v1, :cond_2

    .line 1786400
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->t:[F

    aget v0, v0, v6

    iget-object v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->getHeight()I

    move-result v1

    int-to-float v1, v1

    sub-float v2, v0, v1

    .line 1786401
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->t:[F

    aget v0, v0, v5

    iget-object v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->getWidth()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v1, v0

    .line 1786402
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->t:[F

    aget v0, v0, v5

    goto :goto_0

    .line 1786403
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getX()F

    move-result v2

    .line 1786404
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getY()F

    move-result v0

    iget-object v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->getHeight()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v1, v0

    .line 1786405
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->g:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getY()F

    move-result v0

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 7

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 1786406
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 1786407
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 1786408
    invoke-direct {p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->getViewAspectRatio()F

    move-result v2

    .line 1786409
    int-to-float v3, v1

    div-float/2addr v3, v2

    int-to-float v4, v0

    cmpg-float v3, v3, v4

    if-gez v3, :cond_1

    .line 1786410
    int-to-float v0, v1

    div-float/2addr v0, v2

    float-to-int v0, v0

    .line 1786411
    :goto_0
    iget v2, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->s:I

    rem-int/lit16 v2, v2, 0xb4

    if-eqz v2, :cond_3

    .line 1786412
    :goto_1
    invoke-static {v0, p1}, Landroid/view/View;->resolveSize(II)I

    move-result v2

    invoke-static {v1, p2}, Landroid/view/View;->resolveSize(II)I

    move-result v3

    invoke-virtual {p0, v2, v3}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->setMeasuredDimension(II)V

    .line 1786413
    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 1786414
    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 1786415
    const/4 v0, 0x0

    :goto_2
    invoke-virtual {p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 1786416
    invoke-virtual {p0, v0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1786417
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_0

    .line 1786418
    invoke-virtual {p0, v3, v2, v1}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->measureChild(Landroid/view/View;II)V

    .line 1786419
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1786420
    :cond_1
    int-to-float v1, v0

    mul-float/2addr v1, v2

    float-to-int v1, v1

    goto :goto_0

    .line 1786421
    :cond_2
    return-void

    :cond_3
    move v6, v0

    move v0, v1

    move v1, v6

    goto :goto_1
.end method

.method public setComposerSessionId(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1786422
    iput-object p1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->n:Ljava/lang/String;

    .line 1786423
    return-void
.end method

.method public setCropRect(Landroid/graphics/RectF;)V
    .locals 0

    .prologue
    .line 1786424
    iput-object p1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->u:Landroid/graphics/RectF;

    .line 1786425
    return-void
.end method

.method public setFilmstrip(Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;)V
    .locals 0

    .prologue
    .line 1786426
    iput-object p1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->j:Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;

    .line 1786427
    return-void
.end method

.method public setListener(LX/BSm;)V
    .locals 0

    .prologue
    .line 1786428
    iput-object p1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->i:LX/BSm;

    .line 1786429
    return-void
.end method

.method public setOverlayImageUri(Landroid/net/Uri;)V
    .locals 0
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1786430
    iput-object p1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->p:Landroid/net/Uri;

    .line 1786431
    return-void
.end method

.method public setRotationAngle(I)V
    .locals 1

    .prologue
    .line 1786432
    if-nez p1, :cond_0

    .line 1786433
    :goto_0
    return-void

    .line 1786434
    :cond_0
    if-lez p1, :cond_1

    .line 1786435
    add-int/lit16 v0, p1, -0x168

    rem-int/lit16 p1, v0, 0x168

    .line 1786436
    :cond_1
    invoke-direct {p0, p1}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->b(I)V

    goto :goto_0
.end method

.method public setShouldCenterSquareCrop(Z)V
    .locals 0

    .prologue
    .line 1786437
    iput-boolean p1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->y:Z

    .line 1786438
    return-void
.end method

.method public setShouldFlipHorizontally(Z)V
    .locals 0

    .prologue
    .line 1786439
    iput-boolean p1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->z:Z

    .line 1786440
    return-void
.end method

.method public setVideoEditGalleryInlineMetadataView(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1786441
    iput-object p1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->k:Landroid/view/View;

    .line 1786442
    return-void
.end method

.method public setVideoMuted(Z)V
    .locals 0

    .prologue
    .line 1786443
    iput-boolean p1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->v:Z

    .line 1786444
    invoke-static {p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->h(Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;)V

    .line 1786445
    return-void
.end method

.method public setVideoUri(Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 1786446
    iput-object p1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->o:Landroid/net/Uri;

    .line 1786447
    return-void
.end method
