.class public Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1787017
    new-instance v0, LX/BTD;

    invoke-direct {v0}, LX/BTD;-><init>()V

    sput-object v0, Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1787002
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1787007
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1787008
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;->a:I

    .line 1787009
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;->b:I

    .line 1787010
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;->c:I

    .line 1787011
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;->d:I

    .line 1787012
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;->e:I

    .line 1787013
    return-void
.end method


# virtual methods
.method public final c(LX/BTI;)I
    .locals 1

    .prologue
    .line 1787014
    sget-object v0, LX/BTI;->LEFT:LX/BTI;

    if-ne p1, v0, :cond_0

    .line 1787015
    iget v0, p0, Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;->a:I

    .line 1787016
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;->b:I

    goto :goto_0
.end method

.method public final d(LX/BTI;)I
    .locals 1

    .prologue
    .line 1787004
    sget-object v0, LX/BTI;->LEFT:LX/BTI;

    if-ne p1, v0, :cond_0

    .line 1787005
    iget v0, p0, Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;->c:I

    .line 1787006
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;->d:I

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1787003
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1786996
    iget v0, p0, Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1786997
    iget v0, p0, Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1786998
    iget v0, p0, Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1786999
    iget v0, p0, Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1787000
    iget v0, p0, Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1787001
    return-void
.end method
