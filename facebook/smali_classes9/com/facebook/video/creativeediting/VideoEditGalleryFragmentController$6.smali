.class public final Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/BSr;


# direct methods
.method public constructor <init>(LX/BSr;)V
    .locals 0

    .prologue
    .line 1785935
    iput-object p1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$6;->a:LX/BSr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    .prologue
    .line 1785936
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$6;->a:LX/BSr;

    iget-object v0, v0, LX/BSr;->o:LX/BT9;

    iget-object v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$6;->a:LX/BSr;

    iget-object v1, v1, LX/BSr;->n:LX/60x;

    .line 1785937
    iput-object v1, v0, LX/BT9;->Y:LX/60x;

    .line 1785938
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$6;->a:LX/BSr;

    .line 1785939
    iget-object v1, v0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->k()Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    move-result-object v1

    .line 1785940
    iget-object v2, v0, LX/BSr;->j:Landroid/net/Uri;

    .line 1785941
    iput-object v2, v1, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->o:Landroid/net/Uri;

    .line 1785942
    iget-object v2, v0, LX/BSr;->D:Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;

    .line 1785943
    iget-object v3, v2, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->b:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object v2, v3

    .line 1785944
    invoke-virtual {v2}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getCropRect()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v2

    invoke-static {v2}, LX/63w;->c(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Landroid/graphics/RectF;

    move-result-object v2

    .line 1785945
    iput-object v2, v1, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->u:Landroid/graphics/RectF;

    .line 1785946
    iget-object v2, v0, LX/BSr;->D:Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;

    iget-object v2, v2, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->b:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    invoke-virtual {v2}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getOverlayUri()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1785947
    iput-object v2, v1, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->p:Landroid/net/Uri;

    .line 1785948
    iget-object v2, v0, LX/BSr;->H:LX/BSm;

    .line 1785949
    iput-object v2, v1, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->i:LX/BSm;

    .line 1785950
    iget-object v2, v0, LX/BSr;->D:Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;

    .line 1785951
    iget-object v3, v2, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->b:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object v2, v3

    .line 1785952
    invoke-virtual {v2}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->isVideoMuted()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->setVideoMuted(Z)V

    .line 1785953
    iget-object v2, v0, LX/BSr;->D:Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;

    .line 1785954
    iget-object v3, v2, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->b:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object v2, v3

    .line 1785955
    invoke-virtual {v2}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getRotationAngle()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->setRotationAngle(I)V

    .line 1785956
    iget-object v2, v0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    .line 1785957
    iget-object v3, v2, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->o:Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    move-object v2, v3

    .line 1785958
    iget-boolean v3, v2, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->h:Z

    move v2, v3

    .line 1785959
    iput-boolean v2, v1, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->y:Z

    .line 1785960
    iget-object v2, v0, LX/BSr;->D:Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;

    .line 1785961
    iget-object v3, v2, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->b:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object v2, v3

    .line 1785962
    invoke-virtual {v2}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->shouldFlipHorizontally()Z

    move-result v2

    .line 1785963
    iput-boolean v2, v1, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->z:Z

    .line 1785964
    iget-object v2, v0, LX/BSr;->n:LX/60x;

    iget v2, v2, LX/60x;->d:I

    rem-int/lit16 v2, v2, 0xb4

    if-nez v2, :cond_6

    .line 1785965
    iget-object v2, v0, LX/BSr;->n:LX/60x;

    iget v2, v2, LX/60x;->b:I

    iget-object v3, v0, LX/BSr;->n:LX/60x;

    iget v3, v3, LX/60x;->c:I

    invoke-virtual {v1, v2, v3}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->a(II)V

    .line 1785966
    :goto_0
    iget-object v2, v0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    invoke-virtual {v2}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->p()Ljava/lang/String;

    move-result-object v2

    .line 1785967
    iput-object v2, v1, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->n:Ljava/lang/String;

    .line 1785968
    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->a()V

    .line 1785969
    iget-object v1, v0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->k()Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    move-result-object v2

    .line 1785970
    iget-object v1, v0, LX/BSr;->x:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, v0, LX/BSr;->w:LX/9fh;

    if-eqz v1, :cond_1

    .line 1785971
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 1785972
    instance-of v3, v2, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    if-eqz v3, :cond_0

    .line 1785973
    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v3

    int-to-float v3, v3

    iput v3, v1, Landroid/graphics/RectF;->bottom:F

    .line 1785974
    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v3

    int-to-float v3, v3

    iput v3, v1, Landroid/graphics/RectF;->right:F

    .line 1785975
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v3

    int-to-float v3, v3

    iput v3, v1, Landroid/graphics/RectF;->left:F

    .line 1785976
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v3

    int-to-float v3, v3

    iput v3, v1, Landroid/graphics/RectF;->top:F

    .line 1785977
    :cond_0
    new-instance v3, LX/9fi;

    iget-object v4, v0, LX/BSr;->w:LX/9fh;

    invoke-static {v2, v1}, LX/9fh;->a(Landroid/view/View;Landroid/graphics/RectF;)LX/9fh;

    move-result-object v1

    new-instance v5, Landroid/graphics/PointF;

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v7

    int-to-float v7, v7

    invoke-direct {v5, v6, v7}, Landroid/graphics/PointF;-><init>(FF)V

    iget-object v6, v0, LX/BSr;->u:LX/0hB;

    invoke-virtual {v6}, LX/0hB;->d()I

    move-result v6

    invoke-direct {v3, v4, v1, v5, v6}, LX/9fi;-><init>(LX/9fh;LX/9fh;Landroid/graphics/PointF;I)V

    invoke-static {v3}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    iput-object v1, v0, LX/BSr;->x:LX/0am;

    .line 1785978
    iget-object v1, v0, LX/BSr;->x:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9fi;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/9fi;->a(Landroid/view/View;Landroid/view/animation/Animation$AnimationListener;)V

    .line 1785979
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$6;->a:LX/BSr;

    .line 1785980
    iget-object v2, v0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    .line 1785981
    iget-object v3, v2, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->q:Landroid/widget/LinearLayout;

    move-object v10, v3

    .line 1785982
    const v2, 0x7f0d3095

    invoke-static {v10, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    .line 1785983
    new-instance v2, LX/9fk;

    iget-object v3, v0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v5, v0, LX/BSr;->G:LX/9f2;

    iget-object v6, v0, LX/BSr;->o:LX/BT9;

    iget-object v7, v0, LX/BSr;->f:Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    .line 1785984
    iget-object v8, v7, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->w:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    move-object v7, v8

    .line 1785985
    const v8, 0x7f0d0d05

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageButton;

    const v9, 0x7f0d0d06

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Lcom/facebook/resources/ui/FbTextView;

    invoke-direct/range {v2 .. v9}, LX/9fk;-><init>(Landroid/content/Context;Landroid/view/View;LX/9f2;LX/9ea;Lcom/facebook/ui/titlebar/Fb4aTitleBar;Landroid/widget/ImageButton;Lcom/facebook/resources/ui/FbTextView;)V

    .line 1785986
    iget-object v3, v0, LX/BSr;->t:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1785987
    invoke-static {v0, v10}, LX/BSr;->a(LX/BSr;Landroid/view/View;)LX/9fk;

    move-result-object v2

    .line 1785988
    if-eqz v2, :cond_2

    .line 1785989
    iget-object v3, v0, LX/BSr;->t:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1785990
    :cond_2
    iget-object v2, v0, LX/BSr;->t:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_3

    .line 1785991
    const/16 v2, 0x8

    invoke-virtual {v10, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1785992
    :cond_3
    iget-object v2, v0, LX/BSr;->t:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/9fk;

    .line 1785993
    iget-object v4, v2, LX/9fk;->b:LX/9ea;

    move-object v4, v4

    .line 1785994
    invoke-interface {v4}, LX/9ea;->j()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, v0, LX/BSr;->D:Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;

    .line 1785995
    iget-object v6, v5, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->a:LX/5SK;

    move-object v5, v6

    .line 1785996
    if-ne v4, v5, :cond_4

    .line 1785997
    invoke-static {v0, v2}, LX/BSr;->a$redex0(LX/BSr;LX/9fk;)V

    .line 1785998
    :cond_5
    return-void

    .line 1785999
    :cond_6
    iget-object v2, v0, LX/BSr;->n:LX/60x;

    iget v2, v2, LX/60x;->c:I

    iget-object v3, v0, LX/BSr;->n:LX/60x;

    iget v3, v3, LX/60x;->b:I

    invoke-virtual {v1, v2, v3}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->a(II)V

    goto/16 :goto_0
.end method
