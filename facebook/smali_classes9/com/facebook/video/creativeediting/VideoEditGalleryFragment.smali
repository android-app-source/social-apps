.class public Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:LX/BSs;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:Landroid/net/Uri;

.field public o:Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

.field public p:LX/BSr;

.field public q:Landroid/widget/LinearLayout;

.field private r:Landroid/view/ViewStub;

.field public s:LX/ATX;

.field public t:Ljava/lang/String;

.field public u:Ljava/lang/String;

.field private v:Landroid/view/View;

.field public w:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

.field private x:Lcom/facebook/video/creativeediting/VideoEditGalleryPreviewLayout;

.field public y:LX/9fh;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1785837
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    const-class p0, LX/BSs;

    invoke-interface {v1, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/BSs;

    iput-object v1, p1, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->m:LX/BSs;

    return-void
.end method


# virtual methods
.method public final k()Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;
    .locals 1

    .prologue
    .line 1785834
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->x:Lcom/facebook/video/creativeediting/VideoEditGalleryPreviewLayout;

    .line 1785835
    iget-object p0, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryPreviewLayout;->b:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    move-object v0, p0

    .line 1785836
    return-object v0
.end method

.method public final l()Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;
    .locals 1

    .prologue
    .line 1785831
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->x:Lcom/facebook/video/creativeediting/VideoEditGalleryPreviewLayout;

    .line 1785832
    iget-object p0, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryPreviewLayout;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;

    move-object v0, p0

    .line 1785833
    return-object v0
.end method

.method public final m()Landroid/view/ViewStub;
    .locals 1

    .prologue
    .line 1785828
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->x:Lcom/facebook/video/creativeediting/VideoEditGalleryPreviewLayout;

    .line 1785829
    iget-object p0, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryPreviewLayout;->c:Landroid/view/ViewStub;

    move-object v0, p0

    .line 1785830
    return-object v0
.end method

.method public final n()Landroid/view/ViewStub;
    .locals 1

    .prologue
    .line 1785827
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->r:Landroid/view/ViewStub;

    return-object v0
.end method

.method public final o()Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;
    .locals 1

    .prologue
    .line 1785826
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->o:Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    return-object v0
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 1785820
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 1785821
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->s:LX/ATX;

    if-eqz v0, :cond_0

    .line 1785822
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->s:LX/ATX;

    iget-object v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->x:Lcom/facebook/video/creativeediting/VideoEditGalleryPreviewLayout;

    .line 1785823
    iget-object p0, v1, Lcom/facebook/video/creativeediting/VideoEditGalleryPreviewLayout;->b:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    move-object v1, p0

    .line 1785824
    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->getCurrentPositionMs()I

    move-result v1

    invoke-interface {v0, v1}, LX/ATX;->a(I)V

    .line 1785825
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x47ac6496

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1785816
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1785817
    const-class v1, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    invoke-static {v1, p0}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1785818
    const v1, 0x7f0e0646

    invoke-virtual {p0, v3, v1}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 1785819
    const/16 v1, 0x2b

    const v2, -0x64fb2ac2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x219b8e3a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1785838
    const v0, 0x7f03158c

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 1785839
    const v0, 0x7f0d0d0d

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->v:Landroid/view/View;

    .line 1785840
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->v:Landroid/view/View;

    const v3, 0x7f0d00bc

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->w:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 1785841
    const v0, 0x7f0d308f

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/creativeediting/VideoEditGalleryPreviewLayout;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->x:Lcom/facebook/video/creativeediting/VideoEditGalleryPreviewLayout;

    .line 1785842
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->x:Lcom/facebook/video/creativeediting/VideoEditGalleryPreviewLayout;

    .line 1785843
    iget-object v3, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryPreviewLayout;->b:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    move-object v0, v3

    .line 1785844
    iget-object v3, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->u:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->a(Ljava/lang/String;)V

    .line 1785845
    const v0, 0x7f0d3090

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->r:Landroid/view/ViewStub;

    .line 1785846
    const v0, 0x7f0d3094

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->q:Landroid/widget/LinearLayout;

    .line 1785847
    const/16 v0, 0x2b

    const v3, 0x1aed6945

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, -0x7e304db7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1785805
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDestroyView()V

    .line 1785806
    iget-object v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->p:LX/BSr;

    .line 1785807
    iget-boolean v4, v1, LX/BSr;->F:Z

    if-nez v4, :cond_0

    .line 1785808
    invoke-static {v1}, LX/BSr;->o(LX/BSr;)V

    .line 1785809
    :cond_0
    invoke-static {v1}, LX/BSr;->p(LX/BSr;)V

    .line 1785810
    iput-object v2, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->v:Landroid/view/View;

    .line 1785811
    iput-object v2, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->r:Landroid/view/ViewStub;

    .line 1785812
    iput-object v2, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->w:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 1785813
    iput-object v2, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->x:Lcom/facebook/video/creativeediting/VideoEditGalleryPreviewLayout;

    .line 1785814
    iput-object v2, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->q:Landroid/widget/LinearLayout;

    .line 1785815
    const/16 v1, 0x2b

    const v2, 0x32e602e1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x34113539

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1785799
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onPause()V

    .line 1785800
    iget-object v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->p:LX/BSr;

    .line 1785801
    iget-object v2, v1, LX/BSr;->t:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/9fk;

    .line 1785802
    iget-object v1, v2, LX/9fk;->b:LX/9ea;

    move-object v2, v1

    .line 1785803
    invoke-interface {v2}, LX/9ea;->g()V

    goto :goto_0

    .line 1785804
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x13cd8e41

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3c6543fe

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1785793
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onResume()V

    .line 1785794
    iget-object v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->p:LX/BSr;

    .line 1785795
    iget-object v2, v1, LX/BSr;->t:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/9fk;

    .line 1785796
    iget-object v1, v2, LX/9fk;->b:LX/9ea;

    move-object v2, v1

    .line 1785797
    invoke-interface {v2}, LX/9ea;->f()V

    goto :goto_0

    .line 1785798
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x40f8d60c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1785782
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1785783
    const-string v0, "config"

    iget-object v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->o:Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1785784
    const-string v0, "video_uri"

    iget-object v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->n:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1785785
    const-string v0, "state"

    iget-object v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->p:LX/BSr;

    .line 1785786
    invoke-static {v1}, LX/BSr;->q(LX/BSr;)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v2

    .line 1785787
    iget-object v3, v1, LX/BSr;->D:Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;

    .line 1785788
    iput-object v2, v3, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->b:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1785789
    iget-object v2, v1, LX/BSr;->D:Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;

    move-object v1, v2

    .line 1785790
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1785791
    const-string v0, "entry_point"

    iget-object v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->t:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1785792
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x1fa4813

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1785750
    invoke-virtual {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->h()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1785751
    invoke-virtual {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->h()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 1785752
    :cond_0
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onStart()V

    .line 1785753
    const/16 v1, 0x2b

    const v2, -0x766c5b95

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x24159468

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1785778
    invoke-virtual {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->h()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1785779
    invoke-virtual {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->h()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 1785780
    :cond_0
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onStop()V

    .line 1785781
    const/16 v1, 0x2b

    const v2, -0x5df017b5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewStateRestored(Landroid/os/Bundle;)V
    .locals 12
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x2

    const/16 v2, 0x2a

    const v3, 0x2bdeaa1c

    invoke-static {v0, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1785758
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onViewStateRestored(Landroid/os/Bundle;)V

    .line 1785759
    if-eqz p1, :cond_2

    .line 1785760
    const-string v0, "config"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->o:Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    .line 1785761
    const-string v0, "video_uri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->n:Landroid/net/Uri;

    .line 1785762
    const-string v0, "state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;

    .line 1785763
    const-string v3, "entry_point"

    invoke-virtual {p1, v3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1785764
    const-string v1, "unknown"

    iput-object v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->t:Ljava/lang/String;

    .line 1785765
    :goto_0
    if-nez v0, :cond_0

    .line 1785766
    new-instance v0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;

    iget-object v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->o:Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    .line 1785767
    iget-object v3, v1, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->c:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object v1, v3

    .line 1785768
    iget-object v3, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->o:Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    .line 1785769
    iget-object v4, v3, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->a:LX/5SK;

    move-object v3, v4

    .line 1785770
    iget-object v4, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->o:Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    .line 1785771
    iget-object p1, v4, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->q:Ljava/lang/String;

    move-object v4, p1

    .line 1785772
    invoke-direct {v0, v1, v3, v4}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;-><init>(Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;LX/5SK;Ljava/lang/String;)V

    .line 1785773
    :cond_0
    iget-object v5, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->m:LX/BSs;

    iget-object v7, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->n:Landroid/net/Uri;

    iget-object v9, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->s:LX/ATX;

    iget-object v10, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->y:LX/9fh;

    iget-object v11, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->o:Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    move-object v6, p0

    move-object v8, v0

    invoke-virtual/range {v5 .. v11}, LX/BSs;->a(Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;Landroid/net/Uri;Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;LX/ATX;LX/9fh;Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;)LX/BSr;

    move-result-object v5

    iput-object v5, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->p:LX/BSr;

    .line 1785774
    iget-object v5, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->p:LX/BSr;

    .line 1785775
    iget-object v6, v5, LX/BSr;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v7, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$5;

    invoke-direct {v7, v5}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$5;-><init>(LX/BSr;)V

    const v8, -0x4f2af404

    invoke-static {v6, v7, v8}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 1785776
    const v0, 0x1d567e77

    invoke-static {v0, v2}, LX/02F;->f(II)V

    return-void

    .line 1785777
    :cond_1
    const-string v1, "entry_point"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->t:Ljava/lang/String;

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1785755
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->o:Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    .line 1785756
    iget-object p0, v0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->b:Ljava/lang/String;

    move-object v0, p0

    .line 1785757
    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1785754
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->t:Ljava/lang/String;

    return-object v0
.end method
