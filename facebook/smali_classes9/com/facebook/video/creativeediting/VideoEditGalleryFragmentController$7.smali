.class public final Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/BSr;


# direct methods
.method public constructor <init>(LX/BSr;)V
    .locals 0

    .prologue
    .line 1786000
    iput-object p1, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$7;->a:LX/BSr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    .line 1786001
    iget-object v0, p0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$7;->a:LX/BSr;

    iget-object v0, v0, LX/BSr;->s:LX/BTi;

    const/4 v4, 0x0

    .line 1786002
    iget-object v1, v0, LX/BTi;->f:LX/BV0;

    iget-object v2, v0, LX/BTi;->h:Landroid/net/Uri;

    sget-object v3, LX/7Sv;->NONE:LX/7Sv;

    invoke-virtual {v1, v2, v4, v4, v3}, LX/BV0;->a(Landroid/net/Uri;Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;Ljava/util/List;LX/7Sv;)LX/BUz;

    move-result-object v2

    .line 1786003
    const/4 v1, 0x0

    :goto_0
    int-to-long v3, v1

    iget-wide v5, v0, LX/BTi;->i:J

    const-wide/16 v7, 0x2

    div-long/2addr v5, v7

    cmp-long v3, v3, v5

    if-gez v3, :cond_0

    .line 1786004
    invoke-static {v0, v2, v1}, LX/BTi;->a(LX/BTi;LX/BUz;I)V

    .line 1786005
    iget-wide v3, v0, LX/BTi;->i:J

    iget v5, v0, LX/BTi;->d:I

    int-to-long v5, v5

    div-long/2addr v3, v5

    iget v5, v0, LX/BTi;->d:I

    int-to-long v5, v5

    mul-long/2addr v3, v5

    long-to-int v3, v3

    sub-int/2addr v3, v1

    invoke-static {v0, v2, v3}, LX/BTi;->a(LX/BTi;LX/BUz;I)V

    .line 1786006
    iget v3, v0, LX/BTi;->d:I

    add-int/2addr v1, v3

    goto :goto_0

    .line 1786007
    :cond_0
    invoke-virtual {v2}, LX/BUz;->a()V

    .line 1786008
    return-void
.end method
