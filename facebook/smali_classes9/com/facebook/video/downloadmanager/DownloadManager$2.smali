.class public final Lcom/facebook/video/downloadmanager/DownloadManager$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/BUA;


# direct methods
.method public constructor <init>(LX/BUA;)V
    .locals 0

    .prologue
    .line 1788235
    iput-object p1, p0, Lcom/facebook/video/downloadmanager/DownloadManager$2;->a:LX/BUA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 1788236
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/downloadmanager/DownloadManager$2;->a:LX/BUA;

    sget-object v1, LX/7Jb;->FEATURE_DISABLED:LX/7Jb;

    invoke-virtual {v0, v1}, LX/BUA;->a(LX/7Jb;)V
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1788237
    :goto_0
    return-void

    .line 1788238
    :catch_0
    move-exception v0

    .line 1788239
    sget-object v1, LX/BUA;->d:Ljava/lang/String;

    const-string v2, "Exception removing offline videos on feature disabled"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
