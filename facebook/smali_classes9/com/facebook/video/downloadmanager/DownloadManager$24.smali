.class public final Lcom/facebook/video/downloadmanager/DownloadManager$24;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/1zt;

.field public final synthetic c:LX/BUA;


# direct methods
.method public constructor <init>(LX/BUA;Ljava/lang/String;LX/1zt;)V
    .locals 0

    .prologue
    .line 1788224
    iput-object p1, p0, Lcom/facebook/video/downloadmanager/DownloadManager$24;->c:LX/BUA;

    iput-object p2, p0, Lcom/facebook/video/downloadmanager/DownloadManager$24;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/video/downloadmanager/DownloadManager$24;->b:LX/1zt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1788225
    iget-object v0, p0, Lcom/facebook/video/downloadmanager/DownloadManager$24;->c:LX/BUA;

    iget-object v0, v0, LX/BUA;->e:LX/19w;

    iget-object v1, p0, Lcom/facebook/video/downloadmanager/DownloadManager$24;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/19w;->o(Ljava/lang/String;)LX/7Jh;

    move-result-object v6

    .line 1788226
    if-eqz v6, :cond_0

    iget-object v0, v6, LX/7Jh;->c:[B

    if-nez v0, :cond_1

    .line 1788227
    :cond_0
    :goto_0
    return-void

    .line 1788228
    :cond_1
    new-instance v0, LX/15i;

    iget-object v1, v6, LX/7Jh;->c:[B

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    const/4 v4, 0x0

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1788229
    const-string v1, "DownloadManager.updateFeedbackReaction"

    invoke-virtual {v0, v1}, LX/15i;->a(Ljava/lang/String;)V

    .line 1788230
    sget-object v1, LX/16Z;->a:LX/16Z;

    invoke-virtual {v0, v1}, LX/15i;->a(LX/16a;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1788231
    iget-object v1, p0, Lcom/facebook/video/downloadmanager/DownloadManager$24;->c:LX/BUA;

    iget-object v1, v1, LX/BUA;->q:LX/20h;

    iget-object v2, p0, Lcom/facebook/video/downloadmanager/DownloadManager$24;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/video/downloadmanager/DownloadManager$24;->b:LX/1zt;

    invoke-virtual {v1, v0, v2, v3}, LX/20h;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;LX/1zt;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1788232
    sget-object v1, LX/16Z;->a:LX/16Z;

    invoke-static {v0, v1}, LX/186;->b(Lcom/facebook/flatbuffers/Flattenable;LX/16a;)[B

    move-result-object v0

    iput-object v0, v6, LX/7Jh;->c:[B

    .line 1788233
    iget-object v0, p0, Lcom/facebook/video/downloadmanager/DownloadManager$24;->c:LX/BUA;

    iget-object v0, v0, LX/BUA;->e:LX/19w;

    invoke-virtual {v0, v6}, LX/19w;->a(LX/7Jh;)V

    goto :goto_0
.end method
