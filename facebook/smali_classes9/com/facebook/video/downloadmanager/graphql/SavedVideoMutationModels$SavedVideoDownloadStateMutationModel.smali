.class public final Lcom/facebook/video/downloadmanager/graphql/SavedVideoMutationModels$SavedVideoDownloadStateMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x448ff759
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/video/downloadmanager/graphql/SavedVideoMutationModels$SavedVideoDownloadStateMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/video/downloadmanager/graphql/SavedVideoMutationModels$SavedVideoDownloadStateMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1789307
    const-class v0, Lcom/facebook/video/downloadmanager/graphql/SavedVideoMutationModels$SavedVideoDownloadStateMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1789288
    const-class v0, Lcom/facebook/video/downloadmanager/graphql/SavedVideoMutationModels$SavedVideoDownloadStateMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1789305
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1789306
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1789303
    iget-object v0, p0, Lcom/facebook/video/downloadmanager/graphql/SavedVideoMutationModels$SavedVideoDownloadStateMutationModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/downloadmanager/graphql/SavedVideoMutationModels$SavedVideoDownloadStateMutationModel;->e:Ljava/lang/String;

    .line 1789304
    iget-object v0, p0, Lcom/facebook/video/downloadmanager/graphql/SavedVideoMutationModels$SavedVideoDownloadStateMutationModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1789297
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1789298
    invoke-direct {p0}, Lcom/facebook/video/downloadmanager/graphql/SavedVideoMutationModels$SavedVideoDownloadStateMutationModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1789299
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1789300
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1789301
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1789302
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1789294
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1789295
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1789296
    return-object p0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1789291
    new-instance v0, Lcom/facebook/video/downloadmanager/graphql/SavedVideoMutationModels$SavedVideoDownloadStateMutationModel;

    invoke-direct {v0}, Lcom/facebook/video/downloadmanager/graphql/SavedVideoMutationModels$SavedVideoDownloadStateMutationModel;-><init>()V

    .line 1789292
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1789293
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1789290
    const v0, 0x54ca76a8

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1789289
    const v0, 0x69e46e58

    return v0
.end method
