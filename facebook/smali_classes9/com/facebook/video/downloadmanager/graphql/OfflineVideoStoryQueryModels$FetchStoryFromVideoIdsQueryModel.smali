.class public final Lcom/facebook/video/downloadmanager/graphql/OfflineVideoStoryQueryModels$FetchStoryFromVideoIdsQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x58c54db6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/video/downloadmanager/graphql/OfflineVideoStoryQueryModels$FetchStoryFromVideoIdsQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/video/downloadmanager/graphql/OfflineVideoStoryQueryModels$FetchStoryFromVideoIdsQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1789218
    const-class v0, Lcom/facebook/video/downloadmanager/graphql/OfflineVideoStoryQueryModels$FetchStoryFromVideoIdsQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1789217
    const-class v0, Lcom/facebook/video/downloadmanager/graphql/OfflineVideoStoryQueryModels$FetchStoryFromVideoIdsQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1789215
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1789216
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1789213
    iget-object v0, p0, Lcom/facebook/video/downloadmanager/graphql/OfflineVideoStoryQueryModels$FetchStoryFromVideoIdsQueryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/downloadmanager/graphql/OfflineVideoStoryQueryModels$FetchStoryFromVideoIdsQueryModel;->f:Ljava/lang/String;

    .line 1789214
    iget-object v0, p0, Lcom/facebook/video/downloadmanager/graphql/OfflineVideoStoryQueryModels$FetchStoryFromVideoIdsQueryModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1789205
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1789206
    invoke-virtual {p0}, Lcom/facebook/video/downloadmanager/graphql/OfflineVideoStoryQueryModels$FetchStoryFromVideoIdsQueryModel;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1789207
    invoke-direct {p0}, Lcom/facebook/video/downloadmanager/graphql/OfflineVideoStoryQueryModels$FetchStoryFromVideoIdsQueryModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1789208
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1789209
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1789210
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1789211
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1789212
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1789189
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1789190
    invoke-virtual {p0}, Lcom/facebook/video/downloadmanager/graphql/OfflineVideoStoryQueryModels$FetchStoryFromVideoIdsQueryModel;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1789191
    invoke-virtual {p0}, Lcom/facebook/video/downloadmanager/graphql/OfflineVideoStoryQueryModels$FetchStoryFromVideoIdsQueryModel;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1789192
    invoke-virtual {p0}, Lcom/facebook/video/downloadmanager/graphql/OfflineVideoStoryQueryModels$FetchStoryFromVideoIdsQueryModel;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1789193
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/downloadmanager/graphql/OfflineVideoStoryQueryModels$FetchStoryFromVideoIdsQueryModel;

    .line 1789194
    iput-object v0, v1, Lcom/facebook/video/downloadmanager/graphql/OfflineVideoStoryQueryModels$FetchStoryFromVideoIdsQueryModel;->e:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1789195
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1789196
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1789204
    invoke-direct {p0}, Lcom/facebook/video/downloadmanager/graphql/OfflineVideoStoryQueryModels$FetchStoryFromVideoIdsQueryModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1789201
    new-instance v0, Lcom/facebook/video/downloadmanager/graphql/OfflineVideoStoryQueryModels$FetchStoryFromVideoIdsQueryModel;

    invoke-direct {v0}, Lcom/facebook/video/downloadmanager/graphql/OfflineVideoStoryQueryModels$FetchStoryFromVideoIdsQueryModel;-><init>()V

    .line 1789202
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1789203
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1789200
    const v0, -0x691297bc

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1789199
    const v0, 0x4ed245b

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1789197
    iget-object v0, p0, Lcom/facebook/video/downloadmanager/graphql/OfflineVideoStoryQueryModels$FetchStoryFromVideoIdsQueryModel;->e:Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/video/downloadmanager/graphql/OfflineVideoStoryQueryModels$FetchStoryFromVideoIdsQueryModel;->e:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1789198
    iget-object v0, p0, Lcom/facebook/video/downloadmanager/graphql/OfflineVideoStoryQueryModels$FetchStoryFromVideoIdsQueryModel;->e:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method
