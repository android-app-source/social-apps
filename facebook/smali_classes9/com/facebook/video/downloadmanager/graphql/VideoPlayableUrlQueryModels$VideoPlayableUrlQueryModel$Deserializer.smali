.class public final Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1789322
    const-class v0, Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel;

    new-instance v1, Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1789323
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1789324
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 1789325
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1789326
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1789327
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_9

    .line 1789328
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1789329
    :goto_0
    move v1, v2

    .line 1789330
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1789331
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1789332
    new-instance v1, Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel;

    invoke-direct {v1}, Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel;-><init>()V

    .line 1789333
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1789334
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1789335
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1789336
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1789337
    :cond_0
    return-object v1

    .line 1789338
    :cond_1
    const-string p0, "video_full_size"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1789339
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v1

    move v4, v1

    move v1, v3

    .line 1789340
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, p0, :cond_7

    .line 1789341
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1789342
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1789343
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v8, :cond_2

    .line 1789344
    const-string p0, "__type__"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_3

    const-string p0, "__typename"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1789345
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v7

    goto :goto_1

    .line 1789346
    :cond_4
    const-string p0, "id"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1789347
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 1789348
    :cond_5
    const-string p0, "playable_url"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 1789349
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 1789350
    :cond_6
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1789351
    :cond_7
    const/4 v8, 0x4

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 1789352
    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1789353
    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1789354
    const/4 v3, 0x2

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 1789355
    if-eqz v1, :cond_8

    .line 1789356
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v4, v2}, LX/186;->a(III)V

    .line 1789357
    :cond_8
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_9
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    goto :goto_1
.end method
