.class public final Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x296c89bf
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1789423
    const-class v0, Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1789422
    const-class v0, Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1789420
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1789421
    return-void
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1789417
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1789418
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1789419
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1789415
    iget-object v0, p0, Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel;->f:Ljava/lang/String;

    .line 1789416
    iget-object v0, p0, Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1789404
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1789405
    invoke-direct {p0}, Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel;->l()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1789406
    invoke-direct {p0}, Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1789407
    invoke-virtual {p0}, Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1789408
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1789409
    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1789410
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1789411
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1789412
    const/4 v0, 0x3

    iget v1, p0, Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel;->h:I

    invoke-virtual {p1, v0, v1, v4}, LX/186;->a(III)V

    .line 1789413
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1789414
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1789401
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1789402
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1789403
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1789400
    new-instance v0, LX/BUd;

    invoke-direct {v0, p1}, LX/BUd;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1789424
    invoke-direct {p0}, Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1789397
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1789398
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel;->h:I

    .line 1789399
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1789395
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1789396
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1789394
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1789385
    new-instance v0, Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel;

    invoke-direct {v0}, Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel;-><init>()V

    .line 1789386
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1789387
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1789393
    const v0, -0x40db8798

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1789392
    const v0, 0x252222

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1789390
    iget-object v0, p0, Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel;->g:Ljava/lang/String;

    .line 1789391
    iget-object v0, p0, Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final k()I
    .locals 2

    .prologue
    .line 1789388
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1789389
    iget v0, p0, Lcom/facebook/video/downloadmanager/graphql/VideoPlayableUrlQueryModels$VideoPlayableUrlQueryModel;->h:I

    return v0
.end method
