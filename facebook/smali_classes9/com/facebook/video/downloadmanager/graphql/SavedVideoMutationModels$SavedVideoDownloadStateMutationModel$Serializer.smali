.class public final Lcom/facebook/video/downloadmanager/graphql/SavedVideoMutationModels$SavedVideoDownloadStateMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/video/downloadmanager/graphql/SavedVideoMutationModels$SavedVideoDownloadStateMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1789275
    const-class v0, Lcom/facebook/video/downloadmanager/graphql/SavedVideoMutationModels$SavedVideoDownloadStateMutationModel;

    new-instance v1, Lcom/facebook/video/downloadmanager/graphql/SavedVideoMutationModels$SavedVideoDownloadStateMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/video/downloadmanager/graphql/SavedVideoMutationModels$SavedVideoDownloadStateMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1789276
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1789277
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/video/downloadmanager/graphql/SavedVideoMutationModels$SavedVideoDownloadStateMutationModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1789278
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1789279
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1789280
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1789281
    const/4 p0, 0x0

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1789282
    if-eqz p0, :cond_0

    .line 1789283
    const-string p2, "video_id"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1789284
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1789285
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1789286
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1789287
    check-cast p1, Lcom/facebook/video/downloadmanager/graphql/SavedVideoMutationModels$SavedVideoDownloadStateMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/video/downloadmanager/graphql/SavedVideoMutationModels$SavedVideoDownloadStateMutationModel$Serializer;->a(Lcom/facebook/video/downloadmanager/graphql/SavedVideoMutationModels$SavedVideoDownloadStateMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
