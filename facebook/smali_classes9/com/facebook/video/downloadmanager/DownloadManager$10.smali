.class public final Lcom/facebook/video/downloadmanager/DownloadManager$10;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;

.field public final synthetic b:LX/BUA;

.field public final synthetic c:LX/BUA;


# direct methods
.method public constructor <init>(LX/BUA;Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;LX/BUA;)V
    .locals 0

    .prologue
    .line 1788031
    iput-object p1, p0, Lcom/facebook/video/downloadmanager/DownloadManager$10;->c:LX/BUA;

    iput-object p2, p0, Lcom/facebook/video/downloadmanager/DownloadManager$10;->a:Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;

    iput-object p3, p0, Lcom/facebook/video/downloadmanager/DownloadManager$10;->b:LX/BUA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 12

    .prologue
    .line 1788016
    iget-object v0, p0, Lcom/facebook/video/downloadmanager/DownloadManager$10;->a:Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;

    iget-object v1, p0, Lcom/facebook/video/downloadmanager/DownloadManager$10;->b:LX/BUA;

    .line 1788017
    iput-object v1, v0, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->k:LX/BUA;

    .line 1788018
    iget-object v2, v0, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->e:LX/19w;

    .line 1788019
    invoke-static {v2}, LX/19w;->s(LX/19w;)V

    .line 1788020
    iget-object v4, v2, LX/19w;->f:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    iget-boolean v4, v2, LX/19w;->k:Z

    if-nez v4, :cond_2

    .line 1788021
    :cond_0
    const-wide/16 v6, 0x0

    .line 1788022
    :cond_1
    move-wide v2, v6

    .line 1788023
    iput-wide v2, v0, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->j:J

    .line 1788024
    invoke-virtual {v0}, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->a()V

    .line 1788025
    return-void

    .line 1788026
    :cond_2
    const-wide v4, 0x7fffffffffffffffL

    .line 1788027
    iget-object v6, v2, LX/19w;->f:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move-wide v6, v4

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/7Jg;

    .line 1788028
    iget-wide v10, v4, LX/7Jg;->m:J

    cmp-long v5, v10, v6

    if-gez v5, :cond_3

    .line 1788029
    iget-wide v4, v4, LX/7Jg;->m:J

    :goto_1
    move-wide v6, v4

    .line 1788030
    goto :goto_0

    :cond_3
    move-wide v4, v6

    goto :goto_1
.end method
