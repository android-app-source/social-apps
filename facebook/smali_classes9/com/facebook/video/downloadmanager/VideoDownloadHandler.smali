.class public Lcom/facebook/video/downloadmanager/VideoDownloadHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile c:Lcom/facebook/video/downloadmanager/VideoDownloadHandler;


# instance fields
.field private final b:Lcom/facebook/http/common/FbHttpRequestProcessor;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1788874
    const-class v0, Lcom/facebook/video/downloadmanager/VideoDownloadHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/downloadmanager/VideoDownloadHandler;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/http/common/FbHttpRequestProcessor;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1788875
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1788876
    iput-object p1, p0, Lcom/facebook/video/downloadmanager/VideoDownloadHandler;->b:Lcom/facebook/http/common/FbHttpRequestProcessor;

    .line 1788877
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/downloadmanager/VideoDownloadHandler;
    .locals 4

    .prologue
    .line 1788878
    sget-object v0, Lcom/facebook/video/downloadmanager/VideoDownloadHandler;->c:Lcom/facebook/video/downloadmanager/VideoDownloadHandler;

    if-nez v0, :cond_1

    .line 1788879
    const-class v1, Lcom/facebook/video/downloadmanager/VideoDownloadHandler;

    monitor-enter v1

    .line 1788880
    :try_start_0
    sget-object v0, Lcom/facebook/video/downloadmanager/VideoDownloadHandler;->c:Lcom/facebook/video/downloadmanager/VideoDownloadHandler;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1788881
    if-eqz v2, :cond_0

    .line 1788882
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1788883
    new-instance p0, Lcom/facebook/video/downloadmanager/VideoDownloadHandler;

    invoke-static {v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v3

    check-cast v3, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-direct {p0, v3}, Lcom/facebook/video/downloadmanager/VideoDownloadHandler;-><init>(Lcom/facebook/http/common/FbHttpRequestProcessor;)V

    .line 1788884
    move-object v0, p0

    .line 1788885
    sput-object v0, Lcom/facebook/video/downloadmanager/VideoDownloadHandler;->c:Lcom/facebook/video/downloadmanager/VideoDownloadHandler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1788886
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1788887
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1788888
    :cond_1
    sget-object v0, Lcom/facebook/video/downloadmanager/VideoDownloadHandler;->c:Lcom/facebook/video/downloadmanager/VideoDownloadHandler;

    return-object v0

    .line 1788889
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1788890
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static synthetic a(Lcom/facebook/video/downloadmanager/VideoDownloadHandler;Ljava/lang/String;Landroid/net/Uri;JJLX/BTy;)V
    .locals 1

    .prologue
    .line 1788891
    invoke-static/range {p1 .. p7}, Lcom/facebook/video/downloadmanager/VideoDownloadHandler;->a(Ljava/lang/String;Landroid/net/Uri;JJLX/BTy;)V

    return-void
.end method

.method private static a(Ljava/io/File;Lorg/apache/http/client/methods/HttpGet;J)V
    .locals 4

    .prologue
    .line 1788892
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1788893
    :goto_0
    return-void

    .line 1788894
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "bytes="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1788895
    const-string v1, "Range"

    invoke-virtual {p1, v1, v0}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Landroid/net/Uri;JJLX/BTy;)V
    .locals 8

    .prologue
    .line 1788896
    move-object v1, p6

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-wide v6, p4

    :try_start_0
    invoke-virtual/range {v1 .. v7}, LX/BTy;->a(Ljava/lang/String;Landroid/net/Uri;JJ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1788897
    :goto_0
    return-void

    .line 1788898
    :catch_0
    move-exception v0

    .line 1788899
    sget-object v1, Lcom/facebook/video/downloadmanager/VideoDownloadHandler;->a:Ljava/lang/String;

    const-string v2, "Exception notifiying status "

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Exception;LX/BTy;I)V
    .locals 3

    .prologue
    .line 1788900
    :try_start_0
    invoke-virtual {p2, p0, p1, p3}, LX/BTy;->a(Ljava/lang/String;Ljava/lang/Exception;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1788901
    :goto_0
    return-void

    .line 1788902
    :catch_0
    move-exception v0

    .line 1788903
    sget-object v1, Lcom/facebook/video/downloadmanager/VideoDownloadHandler;->a:Ljava/lang/String;

    const-string v2, "Exception notifiying error "

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private b(Landroid/net/Uri;Ljava/lang/String;Ljava/io/File;LX/BTy;J)LX/BUJ;
    .locals 9

    .prologue
    .line 1788904
    :try_start_0
    new-instance v8, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0

    invoke-direct {v8, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    .line 1788905
    invoke-static {p3, v8, p5, p6}, Lcom/facebook/video/downloadmanager/VideoDownloadHandler;->a(Ljava/io/File;Lorg/apache/http/client/methods/HttpGet;J)V

    .line 1788906
    invoke-virtual {v8}, Lorg/apache/http/client/methods/HttpGet;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lorg/apache/http/client/params/HttpClientParams;->setRedirecting(Lorg/apache/http/params/HttpParams;Z)V

    .line 1788907
    new-instance v0, LX/BUI;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move-object v4, p3

    move-object v5, p4

    move-wide v6, p5

    invoke-direct/range {v0 .. v7}, LX/BUI;-><init>(Lcom/facebook/video/downloadmanager/VideoDownloadHandler;Ljava/lang/String;Landroid/net/Uri;Ljava/io/File;LX/BTy;J)V

    .line 1788908
    invoke-static {}, LX/15D;->newBuilder()LX/15E;

    move-result-object v1

    .line 1788909
    iput-object v8, v1, LX/15E;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 1788910
    move-object v1, v1

    .line 1788911
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    .line 1788912
    iput-object v2, v1, LX/15E;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 1788913
    move-object v1, v1

    .line 1788914
    const-class v2, Lcom/facebook/video/downloadmanager/VideoDownloadHandler;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    .line 1788915
    iput-object v2, v1, LX/15E;->c:Ljava/lang/String;

    .line 1788916
    move-object v1, v1

    .line 1788917
    const/4 v2, 0x1

    .line 1788918
    iput-boolean v2, v1, LX/15E;->p:Z

    .line 1788919
    move-object v1, v1

    .line 1788920
    sget-object v2, LX/14P;->RETRY_SAFE:LX/14P;

    .line 1788921
    iput-object v2, v1, LX/15E;->j:LX/14P;

    .line 1788922
    move-object v1, v1

    .line 1788923
    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    .line 1788924
    iput-object v2, v1, LX/15E;->k:Lcom/facebook/http/interfaces/RequestPriority;

    .line 1788925
    move-object v1, v1

    .line 1788926
    iput-object v0, v1, LX/15E;->g:Lorg/apache/http/client/ResponseHandler;

    .line 1788927
    move-object v1, v1

    .line 1788928
    invoke-virtual {v1}, LX/15E;->a()LX/15D;

    move-result-object v1

    .line 1788929
    iget-object v2, p0, Lcom/facebook/video/downloadmanager/VideoDownloadHandler;->b:Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v2, v1}, Lcom/facebook/http/common/FbHttpRequestProcessor;->b(LX/15D;)LX/1j2;

    move-result-object v1

    .line 1788930
    iget-object v2, v1, LX/1j2;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v2, v2

    .line 1788931
    iput-object v2, v0, LX/BUI;->f:Ljava/util/concurrent/Future;

    .line 1788932
    new-instance v0, LX/BUJ;

    invoke-direct {v0, v1}, LX/BUJ;-><init>(LX/1j2;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1788933
    :goto_0
    return-object v0

    .line 1788934
    :catch_0
    move-exception v0

    .line 1788935
    sget-object v1, Lcom/facebook/video/downloadmanager/VideoDownloadHandler;->a:Ljava/lang/String;

    const-string v2, "Exception in starting http request "

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1788936
    const/4 v1, -0x1

    invoke-static {p2, v0, p4, v1}, Lcom/facebook/video/downloadmanager/VideoDownloadHandler;->a(Ljava/lang/String;Ljava/lang/Exception;LX/BTy;I)V

    .line 1788937
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Ljava/lang/String;Ljava/io/File;LX/BTy;J)LX/BUJ;
    .locals 1

    .prologue
    .line 1788938
    invoke-direct/range {p0 .. p6}, Lcom/facebook/video/downloadmanager/VideoDownloadHandler;->b(Landroid/net/Uri;Ljava/lang/String;Ljava/io/File;LX/BTy;J)LX/BUJ;

    move-result-object v0

    return-object v0
.end method
