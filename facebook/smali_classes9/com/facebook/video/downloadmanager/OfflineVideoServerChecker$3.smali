.class public final Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;


# direct methods
.method public constructor <init>(Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;)V
    .locals 0

    .prologue
    .line 1788804
    iput-object p1, p0, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker$3;->a:Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1788805
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker$3;->a:Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;

    invoke-virtual {v0}, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->b()LX/2ZE;

    move-result-object v0

    .line 1788806
    if-nez v0, :cond_0

    .line 1788807
    :goto_0
    return-void

    .line 1788808
    :cond_0
    iget-object v1, p0, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker$3;->a:Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;

    iget-object v1, v1, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->g:LX/28W;

    const-string v2, "networkChangedOfflineVideoServerSync"

    sget-object v3, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v0, v4}, LX/28W;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/List;LX/14U;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1788809
    :catch_0
    move-exception v0

    .line 1788810
    sget-object v1, Lcom/facebook/video/downloadmanager/OfflineVideoServerChecker;->a:Ljava/lang/String;

    const-string v2, "Offline video server sync fail"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
