.class public final Lcom/facebook/video/downloadmanager/DownloadVideoUtils$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/Throwable;

.field public final synthetic b:LX/15V;


# direct methods
.method public constructor <init>(LX/15V;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1788620
    iput-object p1, p0, Lcom/facebook/video/downloadmanager/DownloadVideoUtils$2;->b:LX/15V;

    iput-object p2, p0, Lcom/facebook/video/downloadmanager/DownloadVideoUtils$2;->a:Ljava/lang/Throwable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    .line 1788621
    const/4 v0, 0x0

    .line 1788622
    iget-object v1, p0, Lcom/facebook/video/downloadmanager/DownloadVideoUtils$2;->a:Ljava/lang/Throwable;

    instance-of v1, v1, LX/BTp;

    if-eqz v1, :cond_0

    .line 1788623
    iget-object v0, p0, Lcom/facebook/video/downloadmanager/DownloadVideoUtils$2;->a:Ljava/lang/Throwable;

    check-cast v0, LX/BTp;

    iget-object v0, v0, LX/BTp;->mExceptionCode:LX/BTo;

    .line 1788624
    :cond_0
    iget-object v1, p0, Lcom/facebook/video/downloadmanager/DownloadVideoUtils$2;->b:LX/15V;

    iget-object v1, v1, LX/15V;->a:LX/6WJ;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/video/downloadmanager/DownloadVideoUtils$2;->b:LX/15V;

    iget-object v1, v1, LX/15V;->j:Landroid/app/Activity;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/video/downloadmanager/DownloadVideoUtils$2;->b:LX/15V;

    iget-object v1, v1, LX/15V;->a:LX/6WJ;

    invoke-virtual {v1}, LX/6WJ;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/video/downloadmanager/DownloadVideoUtils$2;->b:LX/15V;

    iget-object v2, v2, LX/15V;->j:Landroid/app/Activity;

    if-ne v1, v2, :cond_2

    :cond_1
    iget-object v1, p0, Lcom/facebook/video/downloadmanager/DownloadVideoUtils$2;->b:LX/15V;

    iget-object v1, v1, LX/15V;->i:LX/BTo;

    if-eq v1, v0, :cond_3

    .line 1788625
    :cond_2
    iget-object v1, p0, Lcom/facebook/video/downloadmanager/DownloadVideoUtils$2;->b:LX/15V;

    iget-object v2, p0, Lcom/facebook/video/downloadmanager/DownloadVideoUtils$2;->b:LX/15V;

    iget-object v3, p0, Lcom/facebook/video/downloadmanager/DownloadVideoUtils$2;->a:Ljava/lang/Throwable;

    .line 1788626
    iget-object v4, v2, LX/15V;->j:Landroid/app/Activity;

    if-nez v4, :cond_5

    .line 1788627
    const/4 v4, 0x0

    .line 1788628
    :goto_0
    move-object v2, v4

    .line 1788629
    iput-object v2, v1, LX/15V;->a:LX/6WJ;

    .line 1788630
    iget-object v1, p0, Lcom/facebook/video/downloadmanager/DownloadVideoUtils$2;->b:LX/15V;

    .line 1788631
    iput-object v0, v1, LX/15V;->i:LX/BTo;

    .line 1788632
    :cond_3
    iget-object v0, p0, Lcom/facebook/video/downloadmanager/DownloadVideoUtils$2;->b:LX/15V;

    iget-object v0, v0, LX/15V;->j:Landroid/app/Activity;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/video/downloadmanager/DownloadVideoUtils$2;->b:LX/15V;

    iget-object v0, v0, LX/15V;->a:LX/6WJ;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/video/downloadmanager/DownloadVideoUtils$2;->b:LX/15V;

    iget-object v0, v0, LX/15V;->a:LX/6WJ;

    invoke-virtual {v0}, LX/6WJ;->isShowing()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1788633
    iget-object v0, p0, Lcom/facebook/video/downloadmanager/DownloadVideoUtils$2;->b:LX/15V;

    iget-object v0, v0, LX/15V;->a:LX/6WJ;

    invoke-virtual {v0}, LX/6WJ;->show()V

    .line 1788634
    :cond_4
    return-void

    :cond_5
    iget-object v4, v2, LX/15V;->j:Landroid/app/Activity;

    const/4 v6, 0x0

    .line 1788635
    instance-of v5, v3, LX/BTp;

    if-eqz v5, :cond_7

    .line 1788636
    check-cast v3, LX/BTp;

    iget-object v5, v3, LX/BTp;->mExceptionCode:LX/BTo;

    .line 1788637
    :goto_1
    new-instance v7, LX/6WI;

    invoke-direct {v7, v4}, LX/6WI;-><init>(Landroid/content/Context;)V

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, LX/6WI;->a(Z)LX/6WI;

    move-result-object v7

    .line 1788638
    if-nez v5, :cond_6

    .line 1788639
    invoke-static {v2}, LX/15V;->e(LX/15V;)I

    move-result v5

    invoke-virtual {v7, v5}, LX/6WI;->a(I)LX/6WI;

    move-result-object v5

    .line 1788640
    invoke-static {v2}, LX/15V;->f(LX/15V;)I

    move-result v7

    invoke-virtual {v5, v7}, LX/6WI;->b(I)LX/6WI;

    move-result-object v5

    .line 1788641
    const-string v7, "OK"

    invoke-virtual {v5, v7, v6}, LX/6WI;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/6WI;

    .line 1788642
    :goto_2
    invoke-virtual {v5}, LX/6WI;->a()LX/6WJ;

    move-result-object v5

    .line 1788643
    move-object v4, v5

    .line 1788644
    goto :goto_0

    .line 1788645
    :cond_6
    sget-object v8, LX/BUC;->a:[I

    invoke-virtual {v5}, LX/BTo;->ordinal()I

    move-result v5

    aget v5, v8, v5

    packed-switch v5, :pswitch_data_0

    .line 1788646
    invoke-static {v2}, LX/15V;->e(LX/15V;)I

    move-result v5

    invoke-virtual {v7, v5}, LX/6WI;->a(I)LX/6WI;

    move-result-object v5

    .line 1788647
    invoke-static {v2}, LX/15V;->f(LX/15V;)I

    move-result v7

    invoke-virtual {v5, v7}, LX/6WI;->b(I)LX/6WI;

    move-result-object v5

    .line 1788648
    const-string v7, "OK"

    invoke-virtual {v5, v7, v6}, LX/6WI;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/6WI;

    goto :goto_2

    .line 1788649
    :pswitch_0
    invoke-static {v2}, LX/15V;->g(LX/15V;)I

    move-result v5

    invoke-virtual {v7, v5}, LX/6WI;->a(I)LX/6WI;

    move-result-object v5

    .line 1788650
    invoke-static {v2}, LX/15V;->h(LX/15V;)I

    move-result v7

    invoke-virtual {v5, v7}, LX/6WI;->b(I)LX/6WI;

    move-result-object v5

    .line 1788651
    const-string v7, "CANCEL"

    invoke-virtual {v5, v7, v6}, LX/6WI;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/6WI;

    .line 1788652
    const-string v6, "REVIEW VIDEOS"

    iget-object v7, v2, LX/15V;->g:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v5, v6, v7}, LX/6WI;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/6WI;

    goto :goto_2

    .line 1788653
    :pswitch_1
    invoke-static {v2}, LX/15V;->g(LX/15V;)I

    move-result v5

    invoke-virtual {v7, v5}, LX/6WI;->a(I)LX/6WI;

    move-result-object v5

    .line 1788654
    invoke-static {v2}, LX/15V;->h(LX/15V;)I

    move-result v7

    invoke-virtual {v5, v7}, LX/6WI;->b(I)LX/6WI;

    move-result-object v5

    .line 1788655
    const-string v7, "Ok"

    invoke-virtual {v5, v7, v6}, LX/6WI;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/6WI;

    goto :goto_2

    .line 1788656
    :pswitch_2
    invoke-static {v2}, LX/15V;->g(LX/15V;)I

    move-result v5

    invoke-virtual {v7, v5}, LX/6WI;->a(I)LX/6WI;

    move-result-object v5

    .line 1788657
    const v7, 0x7f080dc5

    invoke-virtual {v5, v7}, LX/6WI;->b(I)LX/6WI;

    move-result-object v5

    .line 1788658
    const-string v7, "OK"

    invoke-virtual {v5, v7, v6}, LX/6WI;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/6WI;

    goto :goto_2

    :cond_7
    move-object v5, v6

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
