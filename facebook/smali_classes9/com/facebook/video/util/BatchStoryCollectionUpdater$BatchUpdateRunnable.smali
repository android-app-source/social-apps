.class public final Lcom/facebook/video/util/BatchStoryCollectionUpdater$BatchUpdateRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/BV5;


# direct methods
.method public constructor <init>(LX/BV5;)V
    .locals 0

    .prologue
    .line 1790160
    iput-object p1, p0, Lcom/facebook/video/util/BatchStoryCollectionUpdater$BatchUpdateRunnable;->a:LX/BV5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 1790161
    iget-object v0, p0, Lcom/facebook/video/util/BatchStoryCollectionUpdater$BatchUpdateRunnable;->a:LX/BV5;

    iget-object v1, v0, LX/BV5;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 1790162
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/util/BatchStoryCollectionUpdater$BatchUpdateRunnable;->a:LX/BV5;

    iget-object v0, v0, LX/BV5;->e:LX/BV4;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/util/BatchStoryCollectionUpdater$BatchUpdateRunnable;->a:LX/BV5;

    iget-object v0, v0, LX/BV5;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1790163
    iget-object v0, p0, Lcom/facebook/video/util/BatchStoryCollectionUpdater$BatchUpdateRunnable;->a:LX/BV5;

    iget-object v0, v0, LX/BV5;->e:LX/BV4;

    iget-object v2, p0, Lcom/facebook/video/util/BatchStoryCollectionUpdater$BatchUpdateRunnable;->a:LX/BV5;

    iget-object v2, v2, LX/BV5;->d:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v0, v2}, LX/BV4;->a(Ljava/util/Collection;)V

    .line 1790164
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/util/BatchStoryCollectionUpdater$BatchUpdateRunnable;->a:LX/BV5;

    iget-object v0, v0, LX/BV5;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1790165
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
