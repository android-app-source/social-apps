.class public final Lcom/facebook/video/commercialbreak/pubsub/CommercialBreakMessageSubscriber$1$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0lF;

.field public final synthetic b:LX/BSc;


# direct methods
.method public constructor <init>(LX/BSc;LX/0lF;)V
    .locals 0

    .prologue
    .line 1785590
    iput-object p1, p0, Lcom/facebook/video/commercialbreak/pubsub/CommercialBreakMessageSubscriber$1$1;->b:LX/BSc;

    iput-object p2, p0, Lcom/facebook/video/commercialbreak/pubsub/CommercialBreakMessageSubscriber$1$1;->a:LX/0lF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 1785591
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/pubsub/CommercialBreakMessageSubscriber$1$1;->a:LX/0lF;

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    .line 1785592
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/pubsub/CommercialBreakMessageSubscriber$1$1;->b:LX/BSc;

    iget-object v0, v0, LX/BSc;->c:LX/BSe;

    iget-object v1, p0, Lcom/facebook/video/commercialbreak/pubsub/CommercialBreakMessageSubscriber$1$1;->a:LX/0lF;

    invoke-virtual {v1}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/BSe;->a(Ljava/lang/String;)LX/BSa;

    move-result-object v1

    .line 1785593
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/pubsub/CommercialBreakMessageSubscriber$1$1;->b:LX/BSc;

    iget-object v0, v0, LX/BSc;->c:LX/BSe;

    iget-object v0, v0, LX/BSe;->j:LX/3H4;

    iget-object v2, p0, Lcom/facebook/video/commercialbreak/pubsub/CommercialBreakMessageSubscriber$1$1;->b:LX/BSc;

    iget-object v2, v2, LX/BSc;->c:LX/BSe;

    iget-object v2, v2, LX/BSe;->i:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/video/commercialbreak/pubsub/CommercialBreakMessageSubscriber$1$1;->b:LX/BSc;

    iget-boolean v3, v3, LX/BSc;->a:Z

    .line 1785594
    if-nez v1, :cond_1

    .line 1785595
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "commercial_break_skywalker_receive_invalid_message"

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "commercial_break"

    .line 1785596
    iput-object v5, v4, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1785597
    move-object v4, v4

    .line 1785598
    const-string v5, "topic"

    invoke-virtual {v4, v5, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 1785599
    iget-object v5, v0, LX/3H4;->a:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1785600
    :goto_0
    if-eqz v1, :cond_0

    .line 1785601
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/pubsub/CommercialBreakMessageSubscriber$1$1;->b:LX/BSc;

    iget-object v0, v0, LX/BSc;->b:LX/3Fh;

    invoke-virtual {v0, v1}, LX/3Fh;->a(LX/BSa;)V

    .line 1785602
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/pubsub/CommercialBreakMessageSubscriber$1$1;->b:LX/BSc;

    iget-object v0, v0, LX/BSc;->c:LX/BSe;

    iget-object v0, v0, LX/BSe;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1785603
    iget-object v0, p0, Lcom/facebook/video/commercialbreak/pubsub/CommercialBreakMessageSubscriber$1$1;->b:LX/BSc;

    iget-object v0, v0, LX/BSc;->c:LX/BSe;

    iget-object v0, v0, LX/BSe;->g:LX/0kL;

    new-instance v2, LX/27k;

    invoke-virtual {v1}, LX/BSa;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v2}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 1785604
    :cond_0
    return-void

    .line 1785605
    :cond_1
    sget-object v4, LX/BSO;->a:[I

    iget-object v5, v1, LX/BSa;->b:LX/BSZ;

    invoke-virtual {v5}, LX/BSZ;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 1785606
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "commercial_break_skywalker_receive_invalid_message"

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1785607
    :goto_1
    const-string v5, "commercial_break"

    .line 1785608
    iput-object v5, v4, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1785609
    move-object v5, v4

    .line 1785610
    const-string v6, "topic"

    invoke-virtual {v5, v6, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "host_video_id"

    iget-object v7, v1, LX/BSa;->a:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "instream_video_ad_type"

    sget-object v7, LX/3H0;->LIVE:LX/3H0;

    invoke-virtual {v5, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "is_sponsored"

    invoke-virtual {v5, v6, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1785611
    iget-object v5, v0, LX/3H4;->a:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0

    .line 1785612
    :pswitch_0
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "commercial_break_skywalker_receive_intent"

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "commercial_break_length_ms"

    iget-wide v6, v1, LX/BSa;->c:J

    invoke-virtual {v4, v5, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    goto :goto_1

    .line 1785613
    :pswitch_1
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "commercial_break_skywalker_receive_start"

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "commercial_break_start_time_ms"

    iget-wide v6, v1, LX/BSa;->d:J

    invoke-virtual {v4, v5, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
