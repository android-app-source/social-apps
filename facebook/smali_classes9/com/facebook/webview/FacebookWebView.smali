.class public Lcom/facebook/webview/FacebookWebView;
.super Lcom/facebook/webview/BasicWebView;
.source ""

# interfaces
.implements LX/0hY;


# static fields
.field private static final i:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/BWE;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/BWG;

.field public g:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public h:Lcom/facebook/performancelogger/PerformanceLogger;

.field public j:Z

.field public k:Ljava/lang/String;

.field public l:LX/BWU;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1791984
    const-class v0, Lcom/facebook/webview/FacebookWebView;

    sput-object v0, Lcom/facebook/webview/FacebookWebView;->i:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1791982
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/webview/FacebookWebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1791983
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1791980
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/webview/FacebookWebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1791981
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1791978
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/webview/BasicWebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1791979
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/auth/credentials/SessionCookie;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1791968
    if-eqz p2, :cond_1

    .line 1791969
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 1791970
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    .line 1791971
    invoke-virtual {v0}, Landroid/webkit/CookieManager;->flush()V

    move-object v1, v0

    .line 1791972
    :goto_0
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/webkit/CookieManager;->setAcceptCookie(Z)V

    .line 1791973
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/SessionCookie;

    .line 1791974
    invoke-virtual {v0}, Lcom/facebook/auth/credentials/SessionCookie;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Landroid/webkit/CookieManager;->setCookie(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1791975
    :cond_0
    invoke-static {p0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->sync()V

    .line 1791976
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 1791977
    :cond_1
    return-void
.end method

.method private a(Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/performancelogger/PerformanceLogger;LX/BWU;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1791964
    iput-object p1, p0, Lcom/facebook/webview/FacebookWebView;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1791965
    iput-object p2, p0, Lcom/facebook/webview/FacebookWebView;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 1791966
    iput-object p3, p0, Lcom/facebook/webview/FacebookWebView;->l:LX/BWU;

    .line 1791967
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/webview/FacebookWebView;

    invoke-static {v2}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v2}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v1

    check-cast v1, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static {v2}, LX/BWU;->a(LX/0QB;)LX/BWU;

    move-result-object v2

    check-cast v2, LX/BWU;

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/performancelogger/PerformanceLogger;LX/BWU;)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1791962
    iget-object v0, p0, Lcom/facebook/webview/BasicWebView;->b:LX/48V;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "javascript:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, LX/48V;->b(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 1791963
    return-void
.end method

.method public static getWebViewUriRedirector(Lcom/facebook/webview/FacebookWebView;)LX/BWU;
    .locals 1

    .prologue
    .line 1791961
    iget-object v0, p0, Lcom/facebook/webview/FacebookWebView;->l:LX/BWU;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/BWE;
    .locals 1

    .prologue
    .line 1791960
    iget-object v0, p0, Lcom/facebook/webview/FacebookWebView;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BWE;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/BWL;)Ljava/lang/String;
    .locals 9
    .param p2    # LX/BWL;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 1791941
    iget-object v0, p0, Lcom/facebook/webview/FacebookWebView;->f:LX/BWG;

    invoke-virtual {v0, p1, p2}, LX/BWG;->a(Ljava/lang/String;LX/BWL;)Ljava/lang/String;

    move-result-object v6

    .line 1791942
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "__android_injected_function_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1791943
    const-string v0, "javascript:var %1$s = function() { return %2$s;};"

    invoke-static {v0, v7, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1791944
    iget-object v1, p0, Lcom/facebook/webview/BasicWebView;->b:LX/48V;

    invoke-virtual {v1, p0, v0}, LX/48V;->b(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 1791945
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 1791946
    const-string v0, "callId"

    invoke-interface {v5, v0, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1791947
    const-string v0, "exc"

    new-instance v1, LX/BWO;

    const-string v3, "__android_exception"

    invoke-direct {v1, v3}, LX/BWO;-><init>(Ljava/lang/String;)V

    invoke-interface {v5, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1791948
    const-string v0, "retval"

    new-instance v1, LX/BWO;

    const-string v3, "__android_retval"

    invoke-direct {v1, v3}, LX/BWO;-><init>(Ljava/lang/String;)V

    invoke-interface {v5, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1791949
    const-string v0, "fbrpc"

    const-string v1, "facebook"

    const-string v4, "call_return"

    move-object v3, v2

    invoke-static/range {v0 .. v5}, LX/BWP;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/UUID;Ljava/util/UUID;Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v8

    .line 1791950
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 1791951
    if-eqz v0, :cond_0

    .line 1791952
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 1791953
    const-string v0, "callId"

    invoke-interface {v5, v0, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1791954
    const-string v0, "exc"

    new-instance v1, LX/BWO;

    const-string v3, "err"

    invoke-direct {v1, v3}, LX/BWO;-><init>(Ljava/lang/String;)V

    invoke-interface {v5, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1791955
    const-string v0, "fbrpc"

    const-string v1, "facebook"

    const-string v4, "call_return"

    move-object v3, v2

    invoke-static/range {v0 .. v5}, LX/BWP;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/UUID;Ljava/util/UUID;Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    .line 1791956
    const-string v1, "javascript:(function() { var __android_exception = null; var __android_retval; try { __android_retval = %1$s();} catch (err) { window.prompt(%2$s);throw err; }window.prompt(%3$s);%1$s = null;})()"

    invoke-static {v1, v7, v0, v8}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1791957
    :goto_0
    iget-object v1, p0, Lcom/facebook/webview/BasicWebView;->b:LX/48V;

    invoke-virtual {v1, p0, v0}, LX/48V;->b(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 1791958
    return-object v6

    .line 1791959
    :cond_0
    const-string v0, "javascript:(function() { var __android_exception = null; var __android_retval = null; try { __android_retval = %1$s();} catch (err) { __android_exception = true; }window.prompt(%2$s);%1$s = null;})()"

    invoke-static {v0, v7, v8}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1791935
    invoke-super {p0, p1}, Lcom/facebook/webview/BasicWebView;->a(Landroid/content/Context;)V

    .line 1791936
    const-class v0, Lcom/facebook/webview/FacebookWebView;

    invoke-static {v0, p0}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1791937
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/webview/FacebookWebView;->e:Ljava/util/Map;

    .line 1791938
    new-instance v0, LX/BWG;

    sget-object v1, Lcom/facebook/webview/FacebookWebView;->i:Ljava/lang/Class;

    invoke-direct {v0, v1}, LX/BWG;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/facebook/webview/FacebookWebView;->f:LX/BWG;

    .line 1791939
    const-string v0, "fbrpc"

    iget-object v1, p0, Lcom/facebook/webview/FacebookWebView;->f:LX/BWG;

    iget-object v1, v1, LX/BWG;->b:LX/BWF;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWE;)V

    .line 1791940
    return-void
.end method

.method public final a(Landroid/content/Context;LX/BWN;)V
    .locals 1

    .prologue
    .line 1791985
    iget-object v0, p0, Lcom/facebook/webview/FacebookWebView;->f:LX/BWG;

    invoke-virtual {v0, p1, p0, p2}, LX/BWG;->a(Landroid/content/Context;Lcom/facebook/webview/FacebookWebView;LX/BWN;)Z

    .line 1791986
    return-void
.end method

.method public final a(Ljava/lang/String;LX/BWC;)V
    .locals 1

    .prologue
    .line 1791933
    iget-object v0, p0, Lcom/facebook/webview/FacebookWebView;->f:LX/BWG;

    invoke-virtual {v0, p1, p2}, LX/BWG;->a(Ljava/lang/String;LX/BWC;)V

    .line 1791934
    return-void
.end method

.method public final a(Ljava/lang/String;LX/BWE;)V
    .locals 1

    .prologue
    .line 1791929
    iget-object v0, p0, Lcom/facebook/webview/FacebookWebView;->e:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BWE;

    .line 1791930
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/45Y;->a(Z)V

    .line 1791931
    return-void

    .line 1791932
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1791924
    if-eqz p2, :cond_0

    const-string v0, ""

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1791925
    :cond_0
    const-string p2, "null"

    .line 1791926
    :cond_1
    const-string v0, "(function() {var event = document.createEvent(\'Event\');event.initEvent(\'%1$s\', true, true);event.data = \'%2$s\';document.dispatchEvent(event);})();"

    new-instance v1, LX/BWO;

    invoke-direct {v1, p2}, LX/BWO;-><init>(Ljava/lang/String;)V

    invoke-static {v0, p1, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1791927
    invoke-direct {p0, v0}, Lcom/facebook/webview/FacebookWebView;->b(Ljava/lang/String;)V

    .line 1791928
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;LX/BWL;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<*>;",
            "LX/BWL;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1791917
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1791918
    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1791919
    const-string v1, ", "

    sget-object v2, LX/BWG;->a:LX/1jt;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-static {v0, v1, v2, v3}, LX/0YN;->a(Ljava/lang/StringBuilder;Ljava/lang/String;LX/1jt;[Ljava/lang/Object;)V

    .line 1791920
    const-string v1, ");"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1791921
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 1791922
    invoke-virtual {p0, v0, p3}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWL;)Ljava/lang/String;

    .line 1791923
    return-void
.end method

.method public final a(LX/31M;II)Z
    .locals 1

    .prologue
    .line 1791908
    const/4 p1, 0x0

    .line 1791909
    invoke-virtual {p0}, Lcom/facebook/webview/FacebookWebView;->getUrl()Ljava/lang/String;

    move-result-object p2

    .line 1791910
    if-nez p2, :cond_0

    move v0, p1

    .line 1791911
    :goto_0
    move v0, v0

    .line 1791912
    return v0

    .line 1791913
    :cond_0
    sget-object v0, LX/BWT;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :cond_1
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BWQ;

    .line 1791914
    invoke-interface {v0, p2}, LX/BWQ;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1791915
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, p1

    .line 1791916
    goto :goto_0
.end method

.method public destroy()V
    .locals 3

    .prologue
    .line 1791891
    iget-object v0, p0, Lcom/facebook/webview/FacebookWebView;->f:LX/BWG;

    if-eqz v0, :cond_0

    .line 1791892
    iget-object v0, p0, Lcom/facebook/webview/FacebookWebView;->f:LX/BWG;

    iget-object v0, v0, LX/BWG;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1791893
    :cond_0
    :try_start_0
    invoke-super {p0}, Lcom/facebook/webview/BasicWebView;->destroy()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1791894
    :goto_0
    return-void

    .line 1791895
    :catch_0
    move-exception v0

    .line 1791896
    iget-object v1, p0, Lcom/facebook/webview/BasicWebView;->c:LX/03V;

    const-string v2, "webview_destroy_exception"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public getFbSharedPreferences()Lcom/facebook/prefs/shared/FbSharedPreferences;
    .locals 1

    .prologue
    .line 1791907
    iget-object v0, p0, Lcom/facebook/webview/FacebookWebView;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    return-object v0
.end method

.method public getMobilePage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1791906
    iget-object v0, p0, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    return-object v0
.end method

.method public getNetAccessLogger()LX/44G;
    .locals 1

    .prologue
    .line 1791905
    iget-object v0, p0, Lcom/facebook/webview/BasicWebView;->a:LX/44G;

    return-object v0
.end method

.method public setChromeClient(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1791903
    new-instance v0, LX/BWJ;

    invoke-direct {v0, p0}, LX/BWJ;-><init>(Lcom/facebook/webview/FacebookWebView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/webview/FacebookWebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 1791904
    return-void
.end method

.method public setFileChooserChromeClient(LX/BWI;)V
    .locals 1

    .prologue
    .line 1791901
    new-instance v0, LX/BWK;

    invoke-direct {v0, p0, p1}, LX/BWK;-><init>(Lcom/facebook/webview/FacebookWebView;LX/BWI;)V

    invoke-virtual {p0, v0}, Lcom/facebook/webview/FacebookWebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 1791902
    return-void
.end method

.method public setMobilePage(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1791899
    iput-object p1, p0, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    .line 1791900
    return-void
.end method

.method public setSyncFriendsOnDestroy(Z)V
    .locals 0

    .prologue
    .line 1791897
    iput-boolean p1, p0, Lcom/facebook/webview/FacebookWebView;->j:Z

    .line 1791898
    return-void
.end method
