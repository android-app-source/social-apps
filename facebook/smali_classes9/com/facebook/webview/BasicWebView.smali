.class public Lcom/facebook/webview/BasicWebView;
.super LX/4iK;
.source ""


# static fields
.field public static i:Ljava/lang/String;


# instance fields
.field public a:LX/44G;

.field public b:LX/48V;

.field public c:LX/03V;

.field public d:LX/1hm;

.field private e:LX/13t;

.field private f:LX/18b;

.field private g:Ljava/lang/String;

.field private h:LX/63k;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1686761
    const/4 v0, 0x0

    sput-object v0, Lcom/facebook/webview/BasicWebView;->i:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1686759
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/webview/BasicWebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1686760
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1686757
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/webview/BasicWebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1686758
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1686754
    invoke-direct {p0, p1, p2, p3}, LX/4iK;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1686755
    invoke-virtual {p0, p1}, Lcom/facebook/webview/BasicWebView;->a(Landroid/content/Context;)V

    .line 1686756
    return-void
.end method

.method private a(LX/13t;LX/18b;Ljava/lang/String;LX/48V;LX/44G;LX/03V;LX/1hm;LX/63k;)V
    .locals 0
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/webview/CustomUserAgent;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1686745
    iput-object p1, p0, Lcom/facebook/webview/BasicWebView;->e:LX/13t;

    .line 1686746
    iput-object p2, p0, Lcom/facebook/webview/BasicWebView;->f:LX/18b;

    .line 1686747
    iput-object p3, p0, Lcom/facebook/webview/BasicWebView;->g:Ljava/lang/String;

    .line 1686748
    iput-object p4, p0, Lcom/facebook/webview/BasicWebView;->b:LX/48V;

    .line 1686749
    iput-object p5, p0, Lcom/facebook/webview/BasicWebView;->a:LX/44G;

    .line 1686750
    iput-object p6, p0, Lcom/facebook/webview/BasicWebView;->c:LX/03V;

    .line 1686751
    iput-object p7, p0, Lcom/facebook/webview/BasicWebView;->d:LX/1hm;

    .line 1686752
    iput-object p8, p0, Lcom/facebook/webview/BasicWebView;->h:LX/63k;

    .line 1686753
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/webview/BasicWebView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 9

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v8

    move-object v0, p0

    check-cast v0, Lcom/facebook/webview/BasicWebView;

    invoke-static {v8}, LX/13t;->a(LX/0QB;)LX/13t;

    move-result-object v1

    check-cast v1, LX/13t;

    invoke-static {v8}, LX/18b;->a(LX/0QB;)LX/18b;

    move-result-object v2

    check-cast v2, LX/18b;

    invoke-static {v8}, LX/0s0;->a(LX/0QB;)LX/0s0;

    move-result-object v3

    check-cast v3, LX/0s1;

    invoke-static {v3}, LX/0eG;->a(LX/0s1;)Ljava/lang/String;

    move-result-object v3

    move-object v3, v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v8}, LX/48V;->b(LX/0QB;)LX/48V;

    move-result-object v4

    check-cast v4, LX/48V;

    invoke-static {v8}, LX/44G;->b(LX/0QB;)LX/44G;

    move-result-object v5

    check-cast v5, LX/44G;

    invoke-static {v8}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {v8}, LX/1hm;->a(LX/0QB;)LX/1hm;

    move-result-object v7

    check-cast v7, LX/1hm;

    invoke-static {v8}, LX/7VD;->a(LX/0QB;)LX/7VD;

    move-result-object v8

    check-cast v8, LX/63k;

    invoke-direct/range {v0 .. v8}, Lcom/facebook/webview/BasicWebView;->a(LX/13t;LX/18b;Ljava/lang/String;LX/48V;LX/44G;LX/03V;LX/1hm;LX/63k;)V

    return-void
.end method

.method private getAdditionalHttpHeaders()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1686733
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    .line 1686734
    const-string v1, "X-FB-Connection-Type"

    iget-object v2, p0, Lcom/facebook/webview/BasicWebView;->e:LX/13t;

    invoke-virtual {v2}, LX/13t;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1686735
    const-string v1, "x-fb-net-hni"

    iget-object v2, p0, Lcom/facebook/webview/BasicWebView;->f:LX/18b;

    invoke-virtual {v2}, LX/18b;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1686736
    const-string v1, "x-fb-sim-hni"

    iget-object v2, p0, Lcom/facebook/webview/BasicWebView;->f:LX/18b;

    invoke-virtual {v2}, LX/18b;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1686737
    const-string v1, "x-fb-net-sid"

    iget-object v2, p0, Lcom/facebook/webview/BasicWebView;->f:LX/18b;

    invoke-virtual {v2}, LX/18b;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1686738
    iget-object v1, p0, Lcom/facebook/webview/BasicWebView;->d:LX/1hm;

    .line 1686739
    iget-object v2, v1, LX/1hm;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, v1, LX/1hm;->c:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v1, LX/1hm;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1686740
    :cond_0
    iget-object v2, v1, LX/1hm;->d:Ljava/util/Map;

    .line 1686741
    :goto_0
    move-object v1, v2

    .line 1686742
    if-eqz v1, :cond_1

    .line 1686743
    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1686744
    :cond_1
    return-object v0

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1686720
    const-class v0, Lcom/facebook/webview/BasicWebView;

    invoke-static {v0, p0}, Lcom/facebook/webview/BasicWebView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1686721
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/webview/BasicWebView;->setHapticFeedbackEnabled(Z)V

    .line 1686722
    invoke-virtual {p0, v1}, Lcom/facebook/webview/BasicWebView;->setLongClickable(Z)V

    .line 1686723
    new-instance v0, LX/BW7;

    invoke-direct {v0, p0}, LX/BW7;-><init>(Lcom/facebook/webview/BasicWebView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/webview/BasicWebView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1686724
    invoke-virtual {p0}, Lcom/facebook/webview/BasicWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 1686725
    sget-object v1, Lcom/facebook/webview/BasicWebView;->i:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1686726
    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getUserAgentString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/facebook/webview/BasicWebView;->i:Ljava/lang/String;

    .line 1686727
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/facebook/webview/BasicWebView;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/webview/BasicWebView;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    .line 1686728
    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1686729
    :goto_0
    invoke-virtual {p0, p1}, Lcom/facebook/webview/BasicWebView;->setChromeClient(Landroid/content/Context;)V

    .line 1686730
    return-void

    .line 1686731
    :catch_0
    move-exception v0

    .line 1686732
    iget-object v1, p0, Lcom/facebook/webview/BasicWebView;->c:LX/03V;

    const-string v2, "basicwebview_tts_npe"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public getBaseUserAgent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1686719
    sget-object v0, Lcom/facebook/webview/BasicWebView;->i:Ljava/lang/String;

    return-object v0
.end method

.method public loadUrl(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1686716
    invoke-virtual {p0}, Lcom/facebook/webview/BasicWebView;->resumeTimers()V

    .line 1686717
    iget-object v0, p0, Lcom/facebook/webview/BasicWebView;->h:LX/63k;

    invoke-interface {v0, p1}, LX/63k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/facebook/webview/BasicWebView;->getAdditionalHttpHeaders()Ljava/util/Map;

    move-result-object v1

    invoke-super {p0, v0, v1}, LX/4iK;->loadUrl(Ljava/lang/String;Ljava/util/Map;)V

    .line 1686718
    return-void
.end method

.method public final loadUrl(Ljava/lang/String;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1686710
    invoke-virtual {p0}, Lcom/facebook/webview/BasicWebView;->resumeTimers()V

    .line 1686711
    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, LX/0PM;->a(I)Ljava/util/HashMap;

    move-result-object v0

    .line 1686712
    invoke-interface {v0, p2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1686713
    invoke-direct {p0}, Lcom/facebook/webview/BasicWebView;->getAdditionalHttpHeaders()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1686714
    iget-object v1, p0, Lcom/facebook/webview/BasicWebView;->h:LX/63k;

    invoke-interface {v1, p1}, LX/63k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-super {p0, v1, v0}, LX/4iK;->loadUrl(Ljava/lang/String;Ljava/util/Map;)V

    .line 1686715
    return-void
.end method

.method public setChromeClient(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1686708
    new-instance v0, LX/BW8;

    invoke-direct {v0}, LX/BW8;-><init>()V

    invoke-virtual {p0, v0}, Lcom/facebook/webview/BasicWebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 1686709
    return-void
.end method

.method public setUserAgentString(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1686706
    invoke-virtual {p0}, Lcom/facebook/webview/BasicWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    .line 1686707
    return-void
.end method
