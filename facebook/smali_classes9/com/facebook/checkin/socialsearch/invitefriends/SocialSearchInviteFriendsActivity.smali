.class public Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# static fields
.field public static final y:Ljava/lang/String;


# instance fields
.field private A:Landroid/widget/ImageView;

.field public B:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

.field public C:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field private D:Ljava/lang/String;

.field public E:Ljava/lang/String;

.field private F:Z

.field private G:LX/Bbo;

.field public H:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/621",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;>;"
        }
    .end annotation
.end field

.field public I:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/621",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;>;"
        }
    .end annotation
.end field

.field public J:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation
.end field

.field public K:Z

.field public p:LX/8tC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/8RL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/Bbt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/Bbe;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:Landroid/view/inputmethod/InputMethodManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/8yG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private z:Lcom/facebook/ui/titlebar/Fb4aTitleBar;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1801204
    const-class v0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->y:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1801201
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1801202
    return-void
.end method

.method private static a(LX/1y7;)Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;
    .locals 6

    .prologue
    .line 1801203
    new-instance v1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    new-instance v2, Lcom/facebook/user/model/Name;

    invoke-interface {p0}, LX/1y7;->v_()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;)V

    invoke-interface {p0}, LX/1y7;->c()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/1y7;->c()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-instance v3, Lcom/facebook/user/model/UserKey;

    sget-object v4, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-interface {p0}, LX/1y7;->e()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    invoke-direct {v1, v2, v0, v3}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;-><init>(Lcom/facebook/user/model/Name;Ljava/lang/String;Lcom/facebook/user/model/UserKey;)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;LX/8tC;LX/8RL;LX/Bbt;LX/Bbe;Landroid/view/inputmethod/InputMethodManager;LX/8yG;LX/0kL;LX/03V;Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 1801221
    iput-object p1, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->p:LX/8tC;

    iput-object p2, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->q:LX/8RL;

    iput-object p3, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->r:LX/Bbt;

    iput-object p4, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->s:LX/Bbe;

    iput-object p5, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->t:Landroid/view/inputmethod/InputMethodManager;

    iput-object p6, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->u:LX/8yG;

    iput-object p7, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->v:LX/0kL;

    iput-object p8, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->w:LX/03V;

    iput-object p9, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->x:Ljava/lang/Boolean;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 10

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v9

    move-object v0, p0

    check-cast v0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    invoke-static {v9}, LX/8tC;->c(LX/0QB;)LX/8tC;

    move-result-object v1

    check-cast v1, LX/8tC;

    invoke-static {v9}, LX/8RL;->b(LX/0QB;)LX/8RL;

    move-result-object v2

    check-cast v2, LX/8RL;

    invoke-static {v9}, LX/Bbt;->b(LX/0QB;)LX/Bbt;

    move-result-object v3

    check-cast v3, LX/Bbt;

    new-instance v6, LX/Bbe;

    invoke-static {v9}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v9}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    invoke-direct {v6, v4, v5}, LX/Bbe;-><init>(LX/0tX;LX/1Ck;)V

    move-object v4, v6

    check-cast v4, LX/Bbe;

    invoke-static {v9}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v5

    check-cast v5, Landroid/view/inputmethod/InputMethodManager;

    invoke-static {v9}, LX/8yG;->a(LX/0QB;)LX/8yG;

    move-result-object v6

    check-cast v6, LX/8yG;

    invoke-static {v9}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v7

    check-cast v7, LX/0kL;

    invoke-static {v9}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static {v9}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v9

    check-cast v9, Ljava/lang/Boolean;

    invoke-static/range {v0 .. v9}, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->a(Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;LX/8tC;LX/8RL;LX/Bbt;LX/Bbe;Landroid/view/inputmethod/InputMethodManager;LX/8yG;LX/0kL;LX/03V;Ljava/lang/Boolean;)V

    return-void
.end method

.method private static a(Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel;)Z
    .locals 1

    .prologue
    .line 1801205
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel;->j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$FriendsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel;->j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$FriendsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$FriendsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel;->a()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$CategoryModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel;->a()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$CategoryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$CategoryModel;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel;)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 1801206
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 1801207
    invoke-virtual {p1}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel;->a()LX/0Px;

    move-result-object v6

    move v2, v3

    .line 1801208
    :goto_0
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 1801209
    invoke-virtual {v6, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel;

    .line 1801210
    invoke-static {v0}, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->a(Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1801211
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 1801212
    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel;->j()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$FriendsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$FriendsModel;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v4, v3

    :goto_1
    if-ge v4, v9, :cond_0

    invoke-virtual {v8, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1y7;

    .line 1801213
    invoke-static {v1}, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->a(LX/1y7;)Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    move-result-object v1

    invoke-virtual {v7, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1801214
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    .line 1801215
    :cond_0
    new-instance v1, LX/623;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel;->a()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$CategoryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$InviteFriendsDetailsModel$AttachmentsModel$TargetModel$InvitedFriendsInfoModel$InvitableFriendsGroupsModel$CategoryModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    invoke-direct {v1, v0, v4}, LX/623;-><init>(Ljava/lang/String;LX/0Px;)V

    invoke-virtual {v5, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1801216
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1801217
    :cond_2
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->H:LX/0Px;

    .line 1801218
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->p:LX/8tC;

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->H:LX/0Px;

    invoke-virtual {v0, v1}, LX/8tB;->a(Ljava/util/List;)V

    .line 1801219
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->p:LX/8tC;

    const v1, 0x410e41e6

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1801220
    return-void
.end method

.method public static a$redex0(Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;)V
    .locals 4

    .prologue
    .line 1801182
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->J:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1801183
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->u:LX/8yG;

    .line 1801184
    iget-object v1, v0, LX/8yG;->a:LX/0if;

    sget-object v2, LX/0ig;->aA:LX/0ih;

    const-string v3, "friend_unselected"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1801185
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->J:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1801186
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->B:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;Z)V

    .line 1801187
    :goto_0
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->p:LX/8tC;

    const v1, -0x3772cdeb

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1801188
    return-void

    .line 1801189
    :cond_0
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->u:LX/8yG;

    .line 1801190
    iget-object v1, v0, LX/8yG;->a:LX/0if;

    sget-object v2, LX/0ig;->aA:LX/0ih;

    const-string v3, "friend_selected"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1801191
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->J:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1801192
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->B:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;)V

    goto :goto_0
.end method

.method private b()V
    .locals 5

    .prologue
    .line 1801193
    const v0, 0x7f0d186e

    invoke-virtual {p0, v0}, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    .line 1801194
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->p:LX/8tC;

    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->J:Ljava/util/List;

    .line 1801195
    iput-object v2, v1, LX/8tB;->i:Ljava/util/List;

    .line 1801196
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->p:LX/8tC;

    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->q:LX/8RL;

    new-instance v3, LX/Bbu;

    invoke-direct {v3}, LX/Bbu;-><init>()V

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, LX/8tB;->a(LX/8RK;LX/8RE;Z)V

    .line 1801197
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->p:LX/8tC;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1801198
    new-instance v1, LX/Bbf;

    invoke-direct {v1, p0}, LX/Bbf;-><init>(Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1801199
    new-instance v1, LX/Bbg;

    invoke-direct {v1, p0}, LX/Bbg;-><init>(Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1801200
    return-void
.end method

.method private l()V
    .locals 2

    .prologue
    .line 1801111
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->x:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0829fe

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1801112
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->z:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-virtual {v1, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(Ljava/lang/String;)V

    .line 1801113
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->z:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    new-instance v1, LX/Bbh;

    invoke-direct {v1, p0}, LX/Bbh;-><init>(Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 1801114
    invoke-static {p0}, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->m(Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;)V

    .line 1801115
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->z:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    new-instance v1, LX/Bbj;

    invoke-direct {v1, p0}, LX/Bbj;-><init>(Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setActionButtonOnClickListener(LX/107;)V

    .line 1801116
    return-void

    .line 1801117
    :cond_0
    const v0, 0x7f0829fd

    goto :goto_0
.end method

.method public static m(Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;)V
    .locals 3

    .prologue
    .line 1801118
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->z:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    const v2, 0x7f0829ff

    invoke-virtual {p0, v2}, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1801119
    iput-object v2, v0, LX/108;->g:Ljava/lang/String;

    .line 1801120
    move-object v0, v0

    .line 1801121
    const/4 v2, -0x2

    .line 1801122
    iput v2, v0, LX/108;->h:I

    .line 1801123
    move-object v2, v0

    .line 1801124
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->J:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 1801125
    :goto_0
    iput-boolean v0, v2, LX/108;->d:Z

    .line 1801126
    move-object v0, v2

    .line 1801127
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setPrimaryButton(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 1801128
    return-void

    .line 1801129
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private n()V
    .locals 2

    .prologue
    .line 1801130
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->B:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->G:LX/Bbo;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1801131
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->B:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    new-instance v1, LX/Bbk;

    invoke-direct {v1, p0}, LX/Bbk;-><init>(Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1801132
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->A:Landroid/widget/ImageView;

    new-instance v1, LX/Bbl;

    invoke-direct {v1, p0}, LX/Bbl;-><init>(Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1801133
    invoke-static {p0}, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->o(Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;)V

    .line 1801134
    return-void
.end method

.method public static o(Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;)V
    .locals 2

    .prologue
    .line 1801135
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->A:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->J:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1801136
    return-void

    .line 1801137
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private p()V
    .locals 7

    .prologue
    .line 1801138
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->C:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 1801139
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->r:LX/Bbt;

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->D:Ljava/lang/String;

    new-instance v2, LX/Bbm;

    invoke-direct {v2, p0}, LX/Bbm;-><init>(Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;)V

    .line 1801140
    new-instance v3, LX/5IA;

    invoke-direct {v3}, LX/5IA;-><init>()V

    .line 1801141
    const-string v4, "story_id"

    invoke-virtual {v3, v4, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1801142
    const-string v4, "profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1801143
    const-string v4, "profile_pic_media_type"

    iget-object v5, v0, LX/Bbt;->d:LX/0rq;

    invoke-virtual {v5}, LX/0rq;->b()LX/0wF;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 1801144
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    sget-object v4, LX/0zS;->c:LX/0zS;

    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    .line 1801145
    iget-object v4, v0, LX/Bbt;->j:LX/1Ck;

    const-string v5, "fetch_friend_groups"

    iget-object v6, v0, LX/Bbt;->b:LX/0tX;

    invoke-virtual {v6, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    new-instance v6, LX/Bbp;

    invoke-direct {v6, v0, v2}, LX/Bbp;-><init>(LX/Bbt;LX/0TF;)V

    invoke-virtual {v4, v5, v3, v6}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1801146
    iget-boolean v0, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->F:Z

    if-eqz v0, :cond_0

    .line 1801147
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->r:LX/Bbt;

    new-instance v1, LX/Bbn;

    invoke-direct {v1, p0}, LX/Bbn;-><init>(Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;)V

    .line 1801148
    iget-object v2, v0, LX/Bbt;->j:LX/1Ck;

    const-string v3, "fetch_all_friends"

    iget-object v4, v0, LX/Bbt;->e:LX/0TD;

    new-instance v5, LX/Bbq;

    invoke-direct {v5, v0}, LX/Bbq;-><init>(LX/Bbt;)V

    invoke-interface {v4, v5}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance v5, LX/Bbr;

    invoke-direct {v5, v0, v1}, LX/Bbr;-><init>(LX/Bbt;LX/0TF;)V

    invoke-virtual {v2, v3, v4, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1801149
    :cond_0
    return-void
.end method

.method public static q(Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;)V
    .locals 3

    .prologue
    .line 1801150
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->t:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->B:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1801151
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 1801161
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1801162
    invoke-static {p0, p0}, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1801163
    const v0, 0x7f030992

    invoke-virtual {p0, v0}, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->setContentView(I)V

    .line 1801164
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "invite_friends_story_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->D:Ljava/lang/String;

    .line 1801165
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "invite_friends_placelist_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->E:Ljava/lang/String;

    .line 1801166
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "invite_friends_can_search_all_friends"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->F:Z

    .line 1801167
    new-instance v0, LX/Bbo;

    invoke-direct {v0, p0}, LX/Bbo;-><init>(Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;)V

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->G:LX/Bbo;

    .line 1801168
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->J:Ljava/util/List;

    .line 1801169
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->z:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 1801170
    const v0, 0x7f0d12a7

    invoke-virtual {p0, v0}, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->A:Landroid/widget/ImageView;

    .line 1801171
    const v0, 0x7f0d12a5

    invoke-virtual {p0, v0}, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->B:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    .line 1801172
    const v0, 0x7f0d05b0

    invoke-virtual {p0, v0}, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->C:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 1801173
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->l()V

    .line 1801174
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->n()V

    .line 1801175
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->b()V

    .line 1801176
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->p()V

    .line 1801177
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->u:LX/8yG;

    .line 1801178
    iget-boolean v1, v0, LX/8yG;->b:Z

    if-eqz v1, :cond_0

    .line 1801179
    :goto_0
    return-void

    .line 1801180
    :cond_0
    iget-object v1, v0, LX/8yG;->a:LX/0if;

    sget-object v2, LX/0ig;->aA:LX/0ih;

    invoke-virtual {v1, v2}, LX/0if;->a(LX/0ih;)V

    .line 1801181
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/8yG;->b:Z

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 1801152
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->u:LX/8yG;

    invoke-virtual {v0}, LX/8yG;->h()V

    .line 1801153
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 1801154
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x74926a59

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1801155
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->B:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->G:LX/Bbo;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1801156
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->r:LX/Bbt;

    .line 1801157
    iget-object v2, v1, LX/Bbt;->j:LX/1Ck;

    invoke-virtual {v2}, LX/1Ck;->c()V

    .line 1801158
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->p:LX/8tC;

    .line 1801159
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 1801160
    const/16 v1, 0x23

    const v2, -0x2b96f063

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
