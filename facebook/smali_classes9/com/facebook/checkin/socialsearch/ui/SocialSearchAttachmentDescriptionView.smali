.class public Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;

.field private f:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1801408
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1801409
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->a()V

    .line 1801410
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1801411
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1801412
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->a()V

    .line 1801413
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1801426
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1801427
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->a()V

    .line 1801428
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1801414
    const v0, 0x7f03137a    # 1.7423E38f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1801415
    const v0, 0x7f0d2cfa

    invoke-virtual {p0, v0}, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->c:Landroid/widget/TextView;

    .line 1801416
    const v0, 0x7f0d2cfc

    invoke-virtual {p0, v0}, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->b:Landroid/widget/TextView;

    .line 1801417
    const v0, 0x7f0d2d00

    invoke-virtual {p0, v0}, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->a:Landroid/widget/TextView;

    .line 1801418
    const v0, 0x7f0d2cfd

    invoke-virtual {p0, v0}, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->d:Landroid/view/View;

    .line 1801419
    const v0, 0x7f0d2cff

    invoke-virtual {p0, v0}, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->e:Landroid/view/View;

    .line 1801420
    const v0, 0x7f0d2cfe

    invoke-virtual {p0, v0}, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->f:Landroid/widget/TextView;

    .line 1801421
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 1801422
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1801423
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->b:Landroid/widget/TextView;

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a00ab

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1801424
    return-void

    .line 1801425
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a00e6

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 1801401
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->b:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1801402
    return-void

    .line 1801403
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1801404
    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->d:Landroid/view/View;

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1801405
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->b:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const v1, 0x7f021864

    :cond_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 1801406
    return-void

    .line 1801407
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final c(Z)V
    .locals 2

    .prologue
    .line 1801398
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->a:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1801399
    return-void

    .line 1801400
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final d(Z)V
    .locals 2

    .prologue
    .line 1801395
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->e:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1801396
    return-void

    .line 1801397
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final e(Z)V
    .locals 2

    .prologue
    .line 1801392
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->f:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1801393
    return-void

    .line 1801394
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setCenterLocationButtonText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1801390
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1801391
    return-void
.end method

.method public setDescriptionDetails(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1801388
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1801389
    return-void
.end method

.method public setDescriptionHeader(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1801386
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1801387
    return-void
.end method
