.class public Lcom/facebook/checkin/socialsearch/ui/SocialSearchImplicitPlaceListOverlay;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1801459
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1801460
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/ui/SocialSearchImplicitPlaceListOverlay;->a()V

    .line 1801461
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1801462
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1801463
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/ui/SocialSearchImplicitPlaceListOverlay;->a()V

    .line 1801464
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1801465
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1801466
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/ui/SocialSearchImplicitPlaceListOverlay;->a()V

    .line 1801467
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1801468
    const-class v0, Lcom/facebook/checkin/socialsearch/ui/SocialSearchImplicitPlaceListOverlay;

    invoke-static {v0, p0}, Lcom/facebook/checkin/socialsearch/ui/SocialSearchImplicitPlaceListOverlay;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1801469
    const v0, 0x7f031383

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1801470
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/ui/SocialSearchImplicitPlaceListOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/ui/SocialSearchImplicitPlaceListOverlay;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0829d1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1801471
    const v0, 0x7f0d2d12

    invoke-virtual {p0, v0}, Lcom/facebook/checkin/socialsearch/ui/SocialSearchImplicitPlaceListOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 1801472
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1801473
    return-void

    .line 1801474
    :cond_0
    const v0, 0x7f0829d0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/checkin/socialsearch/ui/SocialSearchImplicitPlaceListOverlay;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/checkin/socialsearch/ui/SocialSearchImplicitPlaceListOverlay;

    invoke-static {v0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/ui/SocialSearchImplicitPlaceListOverlay;->a:Ljava/lang/Boolean;

    return-void
.end method
