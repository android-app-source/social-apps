.class public Lcom/facebook/checkin/socialsearch/ui/SocialSearchCallToActionAttachmentView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/20J;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1801452
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1801453
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/ui/SocialSearchCallToActionAttachmentView;->a()V

    .line 1801454
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1801449
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1801450
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/ui/SocialSearchCallToActionAttachmentView;->a()V

    .line 1801451
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1801446
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1801447
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/ui/SocialSearchCallToActionAttachmentView;->a()V

    .line 1801448
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1801441
    const-class v0, Lcom/facebook/checkin/socialsearch/ui/SocialSearchCallToActionAttachmentView;

    invoke-static {v0, p0}, Lcom/facebook/checkin/socialsearch/ui/SocialSearchCallToActionAttachmentView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1801442
    const v0, 0x7f031381

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1801443
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/ui/SocialSearchCallToActionAttachmentView;->a:LX/20J;

    sget-object v1, LX/1Wl;->VISIBLE:LX/1Wl;

    .line 1801444
    iput-object v1, v0, LX/20J;->d:LX/1Wl;

    .line 1801445
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/checkin/socialsearch/ui/SocialSearchCallToActionAttachmentView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/checkin/socialsearch/ui/SocialSearchCallToActionAttachmentView;

    invoke-static {v0}, LX/20J;->b(LX/0QB;)LX/20J;

    move-result-object v0

    check-cast v0, LX/20J;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/ui/SocialSearchCallToActionAttachmentView;->a:LX/20J;

    return-void
.end method


# virtual methods
.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1801438
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1801439
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/ui/SocialSearchCallToActionAttachmentView;->a:LX/20J;

    invoke-virtual {v0, p0, p1}, LX/20J;->a(Landroid/view/View;Landroid/graphics/Canvas;)V

    .line 1801440
    return-void
.end method
