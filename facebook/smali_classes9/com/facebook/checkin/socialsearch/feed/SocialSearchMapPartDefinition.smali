.class public Lcom/facebook/checkin/socialsearch/feed/SocialSearchMapPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Po;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/Bbc;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/maps/FbStaticMapView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/maps/FbStaticMapView;",
            ">;"
        }
    .end annotation
.end field

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/maps/rows/MapPartDefinition;

.field private final c:Lcom/facebook/maps/rows/PrefetchMapPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1800989
    new-instance v0, LX/Bbb;

    invoke-direct {v0}, LX/Bbb;-><init>()V

    sput-object v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchMapPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/maps/rows/MapPartDefinition;Lcom/facebook/maps/rows/PrefetchMapPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1800928
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1800929
    iput-object p1, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchMapPartDefinition;->b:Lcom/facebook/maps/rows/MapPartDefinition;

    .line 1800930
    iput-object p2, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchMapPartDefinition;->c:Lcom/facebook/maps/rows/PrefetchMapPartDefinition;

    .line 1800931
    return-void
.end method

.method private static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;
    .locals 15
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;"
        }
    .end annotation

    .prologue
    .line 1800952
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1800953
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    .line 1800954
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->fe()Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;

    move-result-object v0

    move-object v1, v0

    .line 1800955
    :goto_0
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    .line 1800956
    :goto_1
    if-eqz v0, :cond_5

    .line 1800957
    new-instance v0, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    const-string v2, "social_search"

    invoke-direct {v0, v2}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;-><init>(Ljava/lang/String;)V

    .line 1800958
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;->a()LX/0Px;

    move-result-object v1

    const/high16 v14, 0x3f000000    # 0.5f

    .line 1800959
    new-instance v5, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v5, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 1800960
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLPlaceListItem;

    .line 1800961
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    .line 1800962
    if-eqz v4, :cond_0

    .line 1800963
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->N()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v7

    .line 1800964
    if-eqz v7, :cond_0

    .line 1800965
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->aW()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_1

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->aW()Ljava/lang/String;

    move-result-object v4

    .line 1800966
    :goto_3
    new-instance v8, LX/68O;

    new-instance v9, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLLocation;->a()D

    move-result-wide v10

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLLocation;->b()D

    move-result-wide v12

    invoke-direct {v9, v10, v11, v12, v13}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    invoke-direct {v8, v9, v4, v14, v14}, LX/68O;-><init>(Lcom/facebook/android/maps/model/LatLng;Ljava/lang/String;FF)V

    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1800967
    :cond_1
    const-string v4, "images/places/map/pink-place-dot.png"

    goto :goto_3

    .line 1800968
    :cond_2
    move-object v1, v5

    .line 1800969
    invoke-virtual {v0, v1}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(Ljava/util/List;)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    .line 1800970
    :goto_4
    return-object v0

    .line 1800971
    :cond_3
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0

    .line 1800972
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 1800973
    :cond_5
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->qc()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    .line 1800974
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->qd()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    .line 1800975
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->oy()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v2

    .line 1800976
    invoke-static {v0}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchMapPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLPage;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1800977
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->O()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v1

    const-string v2, "social_search"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->N()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    invoke-static {v1, v2, v0}, LX/3BX;->a(Lcom/facebook/graphql/model/GraphQLGeoRectangle;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLLocation;)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v0

    goto :goto_4

    .line 1800978
    :cond_6
    invoke-static {v1}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchMapPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLPage;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1800979
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPage;->O()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v0

    const-string v2, "social_search"

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPage;->N()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v1

    invoke-static {v0, v2, v1}, LX/3BX;->a(Lcom/facebook/graphql/model/GraphQLGeoRectangle;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLLocation;)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v0

    goto :goto_4

    .line 1800980
    :cond_7
    if-eqz v2, :cond_9

    .line 1800981
    const-string v0, "social_search"

    .line 1800982
    invoke-static {v2}, LX/3BX;->a(Lcom/facebook/graphql/model/GraphQLGeoRectangle;)Landroid/graphics/RectF;

    move-result-object v3

    .line 1800983
    new-instance v1, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    invoke-direct {v1, v0}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;-><init>(Ljava/lang/String;)V

    .line 1800984
    if-eqz v3, :cond_8

    .line 1800985
    invoke-virtual {v1, v3}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(Landroid/graphics/RectF;)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v1

    .line 1800986
    :cond_8
    move-object v0, v1

    .line 1800987
    goto :goto_4

    .line 1800988
    :cond_9
    new-instance v0, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    const-string v1, "social_search"

    invoke-direct {v0, v1}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;-><init>(Ljava/lang/String;)V

    goto :goto_4
.end method

.method public static a(LX/0QB;)Lcom/facebook/checkin/socialsearch/feed/SocialSearchMapPartDefinition;
    .locals 5

    .prologue
    .line 1800941
    const-class v1, Lcom/facebook/checkin/socialsearch/feed/SocialSearchMapPartDefinition;

    monitor-enter v1

    .line 1800942
    :try_start_0
    sget-object v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchMapPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1800943
    sput-object v2, Lcom/facebook/checkin/socialsearch/feed/SocialSearchMapPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1800944
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1800945
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1800946
    new-instance p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchMapPartDefinition;

    invoke-static {v0}, Lcom/facebook/maps/rows/MapPartDefinition;->a(LX/0QB;)Lcom/facebook/maps/rows/MapPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/maps/rows/MapPartDefinition;

    invoke-static {v0}, Lcom/facebook/maps/rows/PrefetchMapPartDefinition;->a(LX/0QB;)Lcom/facebook/maps/rows/PrefetchMapPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/maps/rows/PrefetchMapPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchMapPartDefinition;-><init>(Lcom/facebook/maps/rows/MapPartDefinition;Lcom/facebook/maps/rows/PrefetchMapPartDefinition;)V

    .line 1800947
    move-object v0, p0

    .line 1800948
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1800949
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchMapPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1800950
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1800951
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLPage;)Z
    .locals 1

    .prologue
    .line 1800940
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->O()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->N()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/maps/FbStaticMapView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1800939
    sget-object v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchMapPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1800933
    check-cast p2, LX/Bbc;

    .line 1800934
    iget-object v0, p2, LX/Bbc;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchMapPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v0

    .line 1800935
    iget-object v1, p2, LX/Bbc;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1800936
    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchMapPartDefinition;->b:Lcom/facebook/maps/rows/MapPartDefinition;

    new-instance v3, LX/3BY;

    const/4 v4, 0x0

    iget v5, p2, LX/Bbc;->b:I

    iget v6, p2, LX/Bbc;->c:I

    invoke-direct {v3, v0, v4, v5, v6}, LX/3BY;-><init>(Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;ZII)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1800937
    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchMapPartDefinition;->c:Lcom/facebook/maps/rows/PrefetchMapPartDefinition;

    new-instance v3, LX/3Ba;

    iget v4, p2, LX/Bbc;->b:I

    iget v5, p2, LX/Bbc;->c:I

    invoke-direct {v3, v1, v0, v4, v5}, LX/3Ba;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;II)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1800938
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1800932
    const/4 v0, 0x1

    return v0
.end method
