.class public Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Po;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/BbK;",
        "TE;",
        "Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;",
            ">;"
        }
    .end annotation
.end field

.field private static k:LX/0Xm;


# instance fields
.field public final b:Landroid/content/Context;

.field private final c:Landroid/content/res/Resources;

.field private final d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final e:LX/8xo;

.field public final f:Lcom/facebook/content/SecureContextHelper;

.field private final g:Z

.field private final h:LX/0ad;

.field public final i:LX/8yM;

.field public final j:LX/0kL;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1800668
    new-instance v0, LX/BbG;

    invoke-direct {v0}, LX/BbG;-><init>()V

    sput-object v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;LX/8xo;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/content/SecureContextHelper;Ljava/lang/Boolean;LX/0ad;LX/8yM;LX/0kL;)V
    .locals 1
    .param p6    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1800669
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1800670
    iput-object p1, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->b:Landroid/content/Context;

    .line 1800671
    iput-object p2, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->c:Landroid/content/res/Resources;

    .line 1800672
    iput-object p3, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->e:LX/8xo;

    .line 1800673
    iput-object p4, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 1800674
    iput-object p5, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->f:Lcom/facebook/content/SecureContextHelper;

    .line 1800675
    if-nez p6, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->g:Z

    .line 1800676
    iput-object p7, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->h:LX/0ad;

    .line 1800677
    iput-object p8, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->i:LX/8yM;

    .line 1800678
    iput-object p9, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->j:LX/0kL;

    .line 1800679
    return-void

    .line 1800680
    :cond_0
    invoke-virtual {p6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method private a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ps;)LX/BbK;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/BbK;"
        }
    .end annotation

    .prologue
    .line 1800681
    invoke-static/range {p2 .. p2}, LX/BbT;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v13

    .line 1800682
    if-eqz v13, :cond_0

    .line 1800683
    move-object/from16 v0, p2

    invoke-static {p0, v0}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->j(Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/view/View$OnClickListener;

    move-result-object v3

    .line 1800684
    const v2, 0x7f0d2cfb

    iget-object v4, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v4, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1800685
    const v4, 0x7f0d2cfe

    iget-object v5, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static/range {p2 .. p2}, LX/BbT;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    if-eqz v2, :cond_1

    move-object/from16 v0, p2

    invoke-static {p0, v0}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->k(Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/view/View$OnClickListener;

    move-result-object v2

    :goto_0
    move-object/from16 v0, p1

    invoke-interface {v0, v4, v5, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1800686
    const v2, 0x7f0d2cff

    iget-object v4, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v4, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1800687
    :cond_0
    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-static {p0, v0, v1}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->a(Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ps;)Ljava/lang/CharSequence;

    move-result-object v11

    .line 1800688
    move-object/from16 v0, p2

    invoke-static {p0, v0}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->f(Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v9

    .line 1800689
    move-object/from16 v0, p2

    invoke-static {p0, v0}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->d(Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v10

    .line 1800690
    new-instance v2, LX/BbK;

    if-eqz v9, :cond_2

    const/4 v3, 0x1

    :goto_1
    if-eqz v9, :cond_3

    if-eqz v13, :cond_3

    const/4 v4, 0x1

    :goto_2
    if-eqz v10, :cond_4

    const/4 v5, 0x1

    :goto_3
    invoke-static/range {p2 .. p2}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v6

    if-eqz v11, :cond_5

    const/4 v7, 0x1

    :goto_4
    move-object/from16 v0, p2

    invoke-static {p0, v0}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->e(Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v8

    invoke-static/range {p2 .. p2}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v12

    if-eqz v12, :cond_6

    if-eqz v9, :cond_6

    const/4 v12, 0x1

    :goto_5
    move-object/from16 v0, p2

    invoke-static {p0, v0}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->g(Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v14

    invoke-direct/range {v2 .. v14}, LX/BbK;-><init>(ZZZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;ZZI)V

    return-object v2

    :cond_1
    move-object v2, v3

    .line 1800691
    goto :goto_0

    .line 1800692
    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    :cond_3
    const/4 v4, 0x0

    goto :goto_2

    :cond_4
    const/4 v5, 0x0

    goto :goto_3

    :cond_5
    const/4 v7, 0x0

    goto :goto_4

    :cond_6
    const/4 v12, 0x0

    goto :goto_5
.end method

.method public static a(LX/0QB;)Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;
    .locals 13

    .prologue
    .line 1800693
    const-class v1, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;

    monitor-enter v1

    .line 1800694
    :try_start_0
    sget-object v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1800695
    sput-object v2, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1800696
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1800697
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1800698
    new-instance v3, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-static {v0}, LX/8xo;->b(LX/0QB;)LX/8xo;

    move-result-object v6

    check-cast v6, LX/8xo;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v8

    check-cast v8, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v9

    check-cast v9, Ljava/lang/Boolean;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-static {v0}, LX/8yM;->b(LX/0QB;)LX/8yM;

    move-result-object v11

    check-cast v11, LX/8yM;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v12

    check-cast v12, LX/0kL;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;-><init>(Landroid/content/Context;Landroid/content/res/Resources;LX/8xo;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/content/SecureContextHelper;Ljava/lang/Boolean;LX/0ad;LX/8yM;LX/0kL;)V

    .line 1800699
    move-object v0, v3

    .line 1800700
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1800701
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1800702
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1800703
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ps;)Ljava/lang/CharSequence;
    .locals 3
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1800704
    check-cast p2, LX/1Po;

    invoke-interface {p2}, LX/1Po;->c()LX/1PT;

    move-result-object v1

    .line 1800705
    if-eqz v1, :cond_5

    invoke-interface {v1}, LX/1PT;->a()LX/1Qt;

    move-result-object v1

    sget-object v2, LX/1Qt;->PERMALINK:LX/1Qt;

    if-ne v1, v2, :cond_5

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 1800706
    if-eqz v1, :cond_1

    .line 1800707
    :cond_0
    :goto_1
    return-object v0

    .line 1800708
    :cond_1
    invoke-static {p1}, LX/BbT;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1800709
    invoke-static {p1}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0, p1}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->h(Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_1

    .line 1800710
    :cond_2
    invoke-static {p1}, LX/BbT;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {p1}, LX/BbT;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1800711
    invoke-static {p1}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0, p1}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->h(Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->c:Landroid/content/res/Resources;

    const v1, 0x7f0829e6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1800712
    :cond_4
    invoke-static {p1}, LX/BbT;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1}, LX/BbT;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1800713
    invoke-static {p0}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->c(Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->c:Landroid/content/res/Resources;

    const v1, 0x7f0829e5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1800714
    invoke-static {p0}, LX/BbT;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/BbT;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;)Z
    .locals 5

    .prologue
    .line 1800715
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->h:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget-object v2, LX/0c1;->Off:LX/0c1;

    sget-short v3, LX/5HH;->k:S

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v0

    return v0
.end method

.method private static c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1800716
    invoke-static {p0}, LX/BbT;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/BbT;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;
    .locals 4
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1800717
    invoke-static {p1}, LX/BbT;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p1}, LX/BbT;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1800718
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->c:Landroid/content/res/Resources;

    const v1, 0x7f0829eb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1800719
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, LX/BbT;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1800720
    :cond_0
    :goto_0
    return-object v0

    .line 1800721
    :cond_1
    invoke-static {p1}, LX/BbT;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1}, LX/BbT;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p1}, LX/BbT;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1800722
    invoke-static {p0}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->c(Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->c:Landroid/content/res/Resources;

    const v1, 0x7f0829e9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static e(Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1800630
    invoke-static {p1}, LX/BbT;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, LX/BbT;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1800631
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->c:Landroid/content/res/Resources;

    const v1, 0x7f0829e1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1800632
    :goto_0
    return-object v0

    .line 1800633
    :cond_0
    invoke-static {p1}, LX/BbT;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, LX/BbT;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1800634
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->c:Landroid/content/res/Resources;

    const v1, 0x7f0829e2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1800635
    :cond_1
    invoke-static {p1}, LX/BbT;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p1}, LX/BbT;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1800636
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->c:Landroid/content/res/Resources;

    const v1, 0x7f0829e2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1800637
    :cond_2
    invoke-static {p1}, LX/BbT;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p1}, LX/BbT;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1800638
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->c:Landroid/content/res/Resources;

    const v1, 0x7f0829ea

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1800639
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, LX/BbT;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1800640
    :cond_3
    invoke-static {p1}, LX/BbT;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {p1}, LX/BbT;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {p1}, LX/BbT;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1800641
    invoke-static {p0}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->c(Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->c:Landroid/content/res/Resources;

    const v1, 0x7f0829e5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->c:Landroid/content/res/Resources;

    const v1, 0x7f0829e2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1800642
    :cond_5
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->c:Landroid/content/res/Resources;

    const v1, 0x7f0829e1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private static f(Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1800658
    invoke-static {p1}, LX/BbT;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1800659
    invoke-static {p1}, LX/BbT;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, LX/BbT;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v0

    .line 1800660
    :cond_0
    :goto_0
    return-object v0

    .line 1800661
    :cond_1
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->c:Landroid/content/res/Resources;

    const v1, 0x7f0829e3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1800662
    :cond_2
    invoke-static {p1}, LX/BbT;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {p1}, LX/BbT;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1800663
    invoke-static {p1}, LX/BbT;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1800664
    :cond_3
    invoke-static {p1}, LX/BbT;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {p1}, LX/BbT;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1800665
    :cond_4
    invoke-static {p1}, LX/BbT;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {p1}, LX/BbT;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-static {p1}, LX/BbT;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1800666
    invoke-static {p0}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->c(Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->c:Landroid/content/res/Resources;

    const v1, 0x7f0829e4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1800667
    :cond_5
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->c:Landroid/content/res/Resources;

    const v1, 0x7f0829e3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static g(Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)I
    .locals 1
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 1800723
    iget-boolean v0, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->g:Z

    if-eqz v0, :cond_0

    .line 1800724
    const v0, 0x7f0823d5

    .line 1800725
    :goto_0
    return v0

    .line 1800726
    :cond_0
    invoke-static {p1}, LX/BbT;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1800727
    const v0, 0x7f0823d6

    goto :goto_0

    .line 1800728
    :cond_1
    const v0, 0x7f0823d4

    goto :goto_0
.end method

.method private static h(Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/text/Spanned;
    .locals 8
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "Landroid/text/Spanned;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1800643
    if-eqz p1, :cond_0

    .line 1800644
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1800645
    if-eqz v0, :cond_0

    .line 1800646
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1800647
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 1800648
    :goto_0
    return-object v0

    .line 1800649
    :cond_1
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1800650
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 1800651
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->lX()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fe()Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fe()Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;->a()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fe()Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    move-object v0, v1

    .line 1800652
    goto :goto_0

    .line 1800653
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->lX()I

    move-result v1

    .line 1800654
    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->c:Landroid/content/res/Resources;

    const v3, 0x7f0f0127

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1800655
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fe()Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 1800656
    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->c:Landroid/content/res/Resources;

    const v3, 0x7f0f0128

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1800657
    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->c:Landroid/content/res/Resources;

    const v3, 0x7f0829f1

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v6

    aput-object v0, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_0
.end method

.method private static i(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1800621
    if-eqz p0, :cond_0

    .line 1800622
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1800623
    if-eqz v0, :cond_0

    .line 1800624
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1800625
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 1800626
    :goto_0
    return v0

    .line 1800627
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1800628
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 1800629
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->lX()I

    move-result v0

    if-lez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method private static j(Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/view/View$OnClickListener;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    .line 1800620
    new-instance v0, LX/BbH;

    invoke-direct {v0, p0, p1}, LX/BbH;-><init>(Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    return-object v0
.end method

.method private static k(Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/view/View$OnClickListener;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    .line 1800619
    new-instance v0, LX/BbJ;

    invoke-direct {v0, p0, p1}, LX/BbJ;-><init>(Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    return-object v0
.end method

.method public static l(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1800604
    invoke-static {p0}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1800605
    if-nez v0, :cond_0

    .line 1800606
    const-class v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;

    const-string v2, "Null story on REX edit location CTA"

    invoke-static {v0, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    move v0, v1

    .line 1800607
    :goto_0
    return v0

    .line 1800608
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    .line 1800609
    if-nez v0, :cond_1

    .line 1800610
    const-class v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;

    const-string v2, "Null story ID on REX edit location CTA"

    invoke-static {v0, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    move v0, v1

    .line 1800611
    goto :goto_0

    .line 1800612
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1800613
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1800614
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1800615
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    .line 1800616
    :cond_2
    const-class v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;

    const-string v2, "Null placelist ID on REX edit location CTA"

    invoke-static {v0, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    move v0, v1

    .line 1800617
    goto :goto_0

    .line 1800618
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1800603
    sget-object v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1800602
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Ps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ps;)LX/BbK;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x35105d2c    # -7852394.0f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1800583
    check-cast p2, LX/BbK;

    check-cast p4, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;

    .line 1800584
    iget-object v1, p2, LX/BbK;->g:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->setDescriptionHeader(Ljava/lang/String;)V

    .line 1800585
    iget-boolean v1, p2, LX/BbK;->a:Z

    invoke-virtual {p4, v1}, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->a(Z)V

    .line 1800586
    iget-boolean v1, p2, LX/BbK;->b:Z

    invoke-virtual {p4, v1}, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->b(Z)V

    .line 1800587
    iget-object v1, p2, LX/BbK;->h:Ljava/lang/String;

    iget-boolean v2, p2, LX/BbK;->k:Z

    invoke-virtual {p4, v1, v2}, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->a(Ljava/lang/String;Z)V

    .line 1800588
    iget-boolean v1, p2, LX/BbK;->e:Z

    invoke-virtual {p4, v1}, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->c(Z)V

    .line 1800589
    iget-object v1, p2, LX/BbK;->j:Ljava/lang/CharSequence;

    invoke-virtual {p4, v1}, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->setDescriptionDetails(Ljava/lang/CharSequence;)V

    .line 1800590
    iget-boolean v1, p2, LX/BbK;->f:Z

    if-eqz v1, :cond_0

    .line 1800591
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->e:LX/8xo;

    iget v2, p2, LX/BbK;->l:I

    .line 1800592
    iget-object p1, v1, LX/8xo;->d:LX/0iA;

    sget-object p3, LX/8xo;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class p0, LX/8xl;

    invoke-virtual {p1, p3, p0}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object p1

    .line 1800593
    if-eqz p1, :cond_0

    .line 1800594
    check-cast p1, LX/8xl;

    .line 1800595
    iget-boolean p3, p1, LX/8xl;->c:Z

    move p1, p3

    .line 1800596
    if-nez p1, :cond_0

    .line 1800597
    iget-object p1, v1, LX/8xo;->e:LX/8xm;

    invoke-virtual {p1, p4, v2}, LX/8xm;->a(Landroid/view/View;I)V

    .line 1800598
    :cond_0
    iget-object v1, p2, LX/BbK;->i:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->setCenterLocationButtonText(Ljava/lang/String;)V

    .line 1800599
    iget-boolean v1, p2, LX/BbK;->c:Z

    invoke-virtual {p4, v1}, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->e(Z)V

    .line 1800600
    iget-boolean v1, p2, LX/BbK;->d:Z

    invoke-virtual {p4, v1}, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;->d(Z)V

    .line 1800601
    const/16 v1, 0x1f

    const v2, 0x2276bbb6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1800582
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1800576
    check-cast p2, LX/BbK;

    check-cast p4, Lcom/facebook/checkin/socialsearch/ui/SocialSearchAttachmentDescriptionView;

    .line 1800577
    iget-boolean v0, p2, LX/BbK;->f:Z

    if-eqz v0, :cond_0

    .line 1800578
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->e:LX/8xo;

    .line 1800579
    iget-object p0, v0, LX/8xo;->e:LX/8xm;

    .line 1800580
    invoke-static {p0, p4}, LX/8xm;->b(LX/8xm;Landroid/view/View;)V

    .line 1800581
    :cond_0
    return-void
.end method
