.class public Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/9jH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/8yM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;

.field public v:Z

.field public w:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1800185
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 1800180
    iget-boolean v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;->v:Z

    if-eqz v0, :cond_0

    .line 1800181
    :goto_0
    return-void

    .line 1800182
    :cond_0
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;->w:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 1800183
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;->v:Z

    .line 1800184
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;->q:LX/8yM;

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;->t:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;->u:Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;

    iget-object v2, v2, Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;->a:Ljava/lang/String;

    new-instance v3, LX/Bb2;

    invoke-direct {v3, p0}, LX/Bb2;-><init>(Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;)V

    invoke-virtual {v0, v1, v2, v3}, LX/8yM;->a(Ljava/lang/String;Ljava/lang/String;LX/0TF;)V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;LX/9jH;LX/8yM;LX/0kL;)V
    .locals 0

    .prologue
    .line 1800155
    iput-object p1, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;->p:LX/9jH;

    iput-object p2, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;->q:LX/8yM;

    iput-object p3, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;->r:LX/0kL;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;

    invoke-static {v2}, LX/9jH;->b(LX/0QB;)LX/9jH;

    move-result-object v0

    check-cast v0, LX/9jH;

    invoke-static {v2}, LX/8yM;->b(LX/0QB;)LX/8yM;

    move-result-object v1

    check-cast v1, LX/8yM;

    invoke-static {v2}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v2

    check-cast v2, LX/0kL;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;->a(Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;LX/9jH;LX/8yM;LX/0kL;)V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1800178
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;->p:LX/9jH;

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;->s:Ljava/lang/String;

    invoke-virtual {v0, p0, v1}, LX/9jH;->a(Landroid/app/Activity;Ljava/lang/String;)V

    .line 1800179
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1800166
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1800167
    const v0, 0x7f031379

    invoke-virtual {p0, v0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;->setContentView(I)V

    .line 1800168
    invoke-static {p0, p0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1800169
    const v0, 0x7f0d2cf9

    invoke-virtual {p0, v0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;->w:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 1800170
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "post_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;->s:Ljava/lang/String;

    .line 1800171
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "placelist_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;->t:Ljava/lang/String;

    .line 1800172
    if-eqz p1, :cond_0

    .line 1800173
    const-string v0, "updating_post_location"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;->v:Z

    .line 1800174
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;->v:Z

    if-eqz v0, :cond_1

    .line 1800175
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;->w:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 1800176
    :goto_0
    return-void

    .line 1800177
    :cond_1
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;->b()V

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1800156
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1800157
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 1800158
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;->finish()V

    .line 1800159
    invoke-virtual {p0, v1, v1}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;->overridePendingTransition(II)V

    .line 1800160
    :cond_0
    :goto_0
    return-void

    .line 1800161
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 1800162
    :pswitch_0
    const-string v0, "extra_place"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1800163
    if-eqz v0, :cond_0

    .line 1800164
    invoke-static {v0}, Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;->u:Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;

    .line 1800165
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchAddEditLocationActivity;->a()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x138a
        :pswitch_0
    .end packed-switch
.end method
