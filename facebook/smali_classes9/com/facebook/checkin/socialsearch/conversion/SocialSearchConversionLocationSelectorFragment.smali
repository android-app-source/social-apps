.class public Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionLocationSelectorFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/Bb8;

.field public b:Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;

.field public c:Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionMapView;

.field private d:Landroid/widget/Button;

.field private e:Landroid/widget/Button;

.field public f:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1800442
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1800443
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1800438
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1800439
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1800440
    const-string v1, "place_info"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionLocationSelectorFragment;->b:Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;

    .line 1800441
    return-void
.end method

.method public final onAttach(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1800444
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onAttach(Landroid/content/Context;)V

    .line 1800445
    instance-of v0, p1, LX/Bb8;

    if-nez v0, :cond_0

    .line 1800446
    new-instance v0, Ljava/lang/ClassCastException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement SocialSearchConversionLocationSelectorFragment.Listener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1800447
    :cond_0
    check-cast p1, LX/Bb8;

    iput-object p1, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionLocationSelectorFragment;->a:LX/Bb8;

    .line 1800448
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x268a5383

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1800437
    const v1, 0x7f03137f

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x4ef43988

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x41915a4b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1800434
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1800435
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionLocationSelectorFragment;->a:LX/Bb8;

    .line 1800436
    const/16 v1, 0x2b

    const v2, 0x5429e7dc

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 8
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1800415
    const v0, 0x7f0d2d09

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionMapView;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionLocationSelectorFragment;->c:Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionMapView;

    .line 1800416
    const v0, 0x7f0d2d0d

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionLocationSelectorFragment;->d:Landroid/widget/Button;

    .line 1800417
    const v0, 0x7f0d2d0c

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionLocationSelectorFragment;->e:Landroid/widget/Button;

    .line 1800418
    const v0, 0x7f0d2d0a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionLocationSelectorFragment;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 1800419
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionLocationSelectorFragment;->d:Landroid/widget/Button;

    .line 1800420
    new-instance v1, LX/Bb9;

    invoke-direct {v1, p0}, LX/Bb9;-><init>(Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionLocationSelectorFragment;)V

    move-object v1, v1

    .line 1800421
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1800422
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionLocationSelectorFragment;->e:Landroid/widget/Button;

    .line 1800423
    new-instance v1, LX/BbA;

    invoke-direct {v1, p0}, LX/BbA;-><init>(Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionLocationSelectorFragment;)V

    move-object v1, v1

    .line 1800424
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1800425
    new-instance v2, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    const-string v3, "social_search"

    invoke-direct {v2, v3}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;-><init>(Ljava/lang/String;)V

    .line 1800426
    iget-object v3, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionLocationSelectorFragment;->b:Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;

    iget-object v3, v3, Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;->e:Landroid/graphics/RectF;

    if-eqz v3, :cond_0

    .line 1800427
    iget-object v3, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionLocationSelectorFragment;->b:Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;

    iget-object v3, v3, Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;->e:Landroid/graphics/RectF;

    invoke-virtual {v2, v3}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(Landroid/graphics/RectF;)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    .line 1800428
    :goto_0
    iget-object v3, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionLocationSelectorFragment;->c:Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionMapView;

    invoke-virtual {v3, v2}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionMapView;->setMapOptions(Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)V

    .line 1800429
    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionLocationSelectorFragment;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0829fa

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionLocationSelectorFragment;->b:Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;

    iget-object v7, v7, Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;->b:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1800430
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionLocationSelectorFragment;->a:LX/Bb8;

    invoke-interface {v0}, LX/Bb8;->l()V

    .line 1800431
    return-void

    .line 1800432
    :cond_0
    iget-object v3, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionLocationSelectorFragment;->b:Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;

    iget-wide v4, v3, Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;->c:D

    iget-object v3, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionLocationSelectorFragment;->b:Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;

    iget-wide v6, v3, Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;->d:D

    invoke-virtual {v2, v4, v5, v6, v7}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(DD)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    .line 1800433
    const/16 v3, 0xd

    invoke-virtual {v2, v3}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(I)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    goto :goto_0
.end method
