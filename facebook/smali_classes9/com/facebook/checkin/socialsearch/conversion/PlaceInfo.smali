.class public Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:D

.field public final d:D

.field public final e:Landroid/graphics/RectF;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;DDLandroid/graphics/RectF;)V
    .locals 1

    .prologue
    .line 1800120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1800121
    iput-object p1, p0, Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;->a:Ljava/lang/String;

    .line 1800122
    iput-object p2, p0, Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;->b:Ljava/lang/String;

    .line 1800123
    iput-wide p3, p0, Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;->c:D

    .line 1800124
    iput-wide p5, p0, Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;->d:D

    .line 1800125
    iput-object p7, p0, Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;->e:Landroid/graphics/RectF;

    .line 1800126
    return-void
.end method

.method public static a(Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationInfoFieldsGraphQLModel$PlaceEntitiesModel;)Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;
    .locals 15

    .prologue
    const-wide/16 v6, 0x0

    .line 1800127
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationInfoFieldsGraphQLModel$PlaceEntitiesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v0

    .line 1800128
    new-instance v1, Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;

    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationInfoFieldsGraphQLModel$PlaceEntitiesModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationInfoFieldsGraphQLModel$PlaceEntitiesModel;->d()Ljava/lang/String;

    move-result-object v3

    if-eqz v0, :cond_1

    invoke-interface {v0}, LX/1k1;->a()D

    move-result-wide v4

    :goto_0
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/1k1;->b()D

    move-result-wide v6

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationInfoFieldsGraphQLModel$PlaceEntitiesModel;->k()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationInfoFieldsGraphQLModel$PlaceEntitiesModel$MapBoundingBoxModel;

    move-result-object v0

    .line 1800129
    if-nez v0, :cond_2

    .line 1800130
    const/4 v9, 0x0

    .line 1800131
    :goto_1
    move-object v8, v9

    .line 1800132
    invoke-direct/range {v1 .. v8}, Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;-><init>(Ljava/lang/String;Ljava/lang/String;DDLandroid/graphics/RectF;)V

    return-object v1

    :cond_1
    move-wide v4, v6

    goto :goto_0

    :cond_2
    new-instance v9, Landroid/graphics/RectF;

    invoke-virtual {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationInfoFieldsGraphQLModel$PlaceEntitiesModel$MapBoundingBoxModel;->l()D

    move-result-wide v11

    double-to-float v10, v11

    invoke-virtual {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationInfoFieldsGraphQLModel$PlaceEntitiesModel$MapBoundingBoxModel;->j()D

    move-result-wide v11

    double-to-float v11, v11

    invoke-virtual {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationInfoFieldsGraphQLModel$PlaceEntitiesModel$MapBoundingBoxModel;->a()D

    move-result-wide v13

    double-to-float v12, v13

    invoke-virtual {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationInfoFieldsGraphQLModel$PlaceEntitiesModel$MapBoundingBoxModel;->k()D

    move-result-wide v13

    double-to-float v13, v13

    invoke-direct {v9, v10, v11, v12, v13}, Landroid/graphics/RectF;-><init>(FFFF)V

    goto :goto_1
.end method

.method public static a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;
    .locals 9

    .prologue
    const-wide/16 v6, 0x0

    .line 1800133
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    move-result-object v0

    .line 1800134
    new-instance v1, Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;

    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v3

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;->a()D

    move-result-wide v4

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;->b()D

    move-result-wide v6

    :cond_0
    const/4 v8, 0x0

    invoke-direct/range {v1 .. v8}, Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;-><init>(Ljava/lang/String;Ljava/lang/String;DDLandroid/graphics/RectF;)V

    return-object v1

    :cond_1
    move-wide v4, v6

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1800135
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1800136
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1800137
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1800138
    iget-wide v0, p0, Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;->c:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 1800139
    iget-wide v0, p0, Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;->d:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 1800140
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;->e:Landroid/graphics/RectF;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1800141
    return-void
.end method
