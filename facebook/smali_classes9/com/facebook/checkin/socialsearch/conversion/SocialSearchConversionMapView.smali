.class public Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionMapView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/maps/FbStaticMapView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1800487
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionMapView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1800488
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1800489
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionMapView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1800490
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1800491
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1800492
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionMapView;->a()V

    .line 1800493
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1800494
    const v0, 0x7f031380

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1800495
    const v0, 0x7f0d2d0e

    invoke-virtual {p0, v0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionMapView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/maps/FbStaticMapView;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionMapView;->a:Lcom/facebook/maps/FbStaticMapView;

    .line 1800496
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionMapView;->a:Lcom/facebook/maps/FbStaticMapView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/3BP;->setReportButtonVisibility(I)V

    .line 1800497
    return-void
.end method


# virtual methods
.method public final onLayout(ZIIII)V
    .locals 5

    .prologue
    .line 1800498
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomFrameLayout;->onLayout(ZIIII)V

    .line 1800499
    if-nez p1, :cond_0

    .line 1800500
    :goto_0
    return-void

    .line 1800501
    :cond_0
    new-instance v0, LX/Bbz;

    sub-int v1, p4, p2

    sub-int v2, p5, p3

    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionMapView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a008a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, LX/Bbz;-><init>(III)V

    invoke-virtual {p0, v0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionMapView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public setMapOptions(Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)V
    .locals 1

    .prologue
    .line 1800502
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionMapView;->a:Lcom/facebook/maps/FbStaticMapView;

    invoke-virtual {v0, p1}, LX/3BP;->setMapOptions(Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)V

    .line 1800503
    return-void
.end method
