.class public Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/Bb5;
.implements LX/Bb8;


# static fields
.field public static final x:Ljava/lang/String;

.field public static final y:LX/0Tn;


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field public C:Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;

.field public D:Z

.field public E:Z

.field private F:Z

.field private G:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public p:LX/BbF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/8xw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/9jH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0hy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/BbC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1800366
    const-class v0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->x:Ljava/lang/String;

    .line 1800367
    sget-object v0, LX/0Tm;->e:LX/0Tn;

    const-string v1, "social_search_converted_posts/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->y:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1800368
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1800369
    iput-boolean v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->D:Z

    .line 1800370
    iput-boolean v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->E:Z

    .line 1800371
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->F:Z

    return-void
.end method

.method private static a(Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;LX/BbF;LX/8xw;LX/9jH;LX/0hy;Lcom/facebook/content/SecureContextHelper;LX/03V;LX/BbC;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0

    .prologue
    .line 1800372
    iput-object p1, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->p:LX/BbF;

    iput-object p2, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->q:LX/8xw;

    iput-object p3, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->r:LX/9jH;

    iput-object p4, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->s:LX/0hy;

    iput-object p5, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->t:Lcom/facebook/content/SecureContextHelper;

    iput-object p6, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->u:LX/03V;

    iput-object p7, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->v:LX/BbC;

    iput-object p8, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->w:Lcom/facebook/prefs/shared/FbSharedPreferences;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 9

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v8

    move-object v0, p0

    check-cast v0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;

    new-instance v3, LX/BbF;

    invoke-static {v8}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {v8}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v2

    check-cast v2, LX/1Ck;

    invoke-direct {v3, v1, v2}, LX/BbF;-><init>(LX/0tX;LX/1Ck;)V

    move-object v1, v3

    check-cast v1, LX/BbF;

    invoke-static {v8}, LX/8xw;->b(LX/0QB;)LX/8xw;

    move-result-object v2

    check-cast v2, LX/8xw;

    invoke-static {v8}, LX/9jH;->b(LX/0QB;)LX/9jH;

    move-result-object v3

    check-cast v3, LX/9jH;

    invoke-static {v8}, LX/10P;->a(LX/0QB;)LX/10P;

    move-result-object v4

    check-cast v4, LX/0hy;

    invoke-static {v8}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v8}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {v8}, LX/BbC;->a(LX/0QB;)LX/BbC;

    move-result-object v7

    check-cast v7, LX/BbC;

    invoke-static {v8}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v8

    check-cast v8, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {v0 .. v8}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->a(Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;LX/BbF;LX/8xw;LX/9jH;LX/0hy;Lcom/facebook/content/SecureContextHelper;LX/03V;LX/BbC;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    return-void
.end method

.method public static b(Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$StoryConversionDetailsModel;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1800339
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$StoryConversionDetailsModel;->a()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$StoryConversionDetailsModel;->a()LX/2uF;

    move-result-object v0

    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 1800340
    :goto_0
    return v0

    .line 1800341
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$StoryConversionDetailsModel;->a()LX/2uF;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v4, v0, LX/1vs;->b:I

    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1800342
    const-class v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v3, v4, v1, v0}, LX/15i;->g(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_1
    if-nez v0, :cond_3

    move v0, v2

    :goto_2
    if-eqz v0, :cond_6

    move v0, v1

    .line 1800343
    goto :goto_0

    .line 1800344
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1800345
    :cond_2
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1800346
    goto :goto_1

    .line 1800347
    :cond_3
    const-class v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v3, v4, v1, v0}, LX/15i;->g(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    .line 1800348
    if-eqz v0, :cond_4

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_3
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PLACE_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v0, v3}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v2

    goto :goto_2

    .line 1800349
    :cond_4
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1800350
    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_2

    :cond_6
    move v0, v2

    .line 1800351
    goto :goto_0
.end method

.method private q()V
    .locals 3

    .prologue
    .line 1800373
    new-instance v0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionAcceptFragment;

    invoke-direct {v0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionAcceptFragment;-><init>()V

    move-object v0, v0

    .line 1800374
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    .line 1800375
    const v2, 0x7f0d2d08

    invoke-virtual {v1, v2, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 1800376
    invoke-virtual {v1}, LX/0hH;->b()I

    .line 1800377
    return-void
.end method

.method public static r(Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;)V
    .locals 4

    .prologue
    .line 1800378
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->v:LX/BbC;

    sget-object v1, LX/BbB;->CLICK_CREATE_MAP:LX/BbB;

    const-string v2, "no_suggested_place"

    invoke-virtual {v0, v1, v2}, LX/BbC;->a(LX/BbB;Ljava/lang/String;)V

    .line 1800379
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->C:Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;

    .line 1800380
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1800381
    const-string v2, "place_info"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1800382
    new-instance v2, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionLocationSelectorFragment;

    invoke-direct {v2}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionLocationSelectorFragment;-><init>()V

    .line 1800383
    invoke-virtual {v2, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1800384
    move-object v0, v2

    .line 1800385
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    .line 1800386
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    .line 1800387
    const v2, 0x7f0d2d08

    const-string v3, "location_selector"

    invoke-virtual {v1, v2, v0, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    .line 1800388
    invoke-virtual {v1}, LX/0hH;->c()I

    .line 1800389
    return-void
.end method

.method private s()V
    .locals 6

    .prologue
    .line 1800390
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->p:LX/BbF;

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->z:Ljava/lang/String;

    new-instance v2, LX/Bb6;

    invoke-direct {v2, p0}, LX/Bb6;-><init>(Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;)V

    .line 1800391
    new-instance v3, LX/5IC;

    invoke-direct {v3}, LX/5IC;-><init>()V

    .line 1800392
    const-string v4, "story_id"

    invoke-virtual {v3, v4, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1800393
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    sget-object v4, LX/0zS;->c:LX/0zS;

    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    .line 1800394
    iget-object v4, v0, LX/BbF;->b:LX/1Ck;

    const-string v5, "fetch_story_info_"

    iget-object p0, v0, LX/BbF;->a:LX/0tX;

    invoke-virtual {p0, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    new-instance p0, LX/BbE;

    invoke-direct {p0, v0, v2}, LX/BbE;-><init>(LX/BbF;LX/0TF;)V

    invoke-virtual {v4, v5, v3, p0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1800395
    return-void
.end method

.method private t()V
    .locals 4

    .prologue
    .line 1800396
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->E:Z

    .line 1800397
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->G:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 1800398
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->q:LX/8xw;

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->z:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->C:Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;

    iget-object v2, v2, Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;->a:Ljava/lang/String;

    new-instance v3, LX/Bb7;

    invoke-direct {v3, p0}, LX/Bb7;-><init>(Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;)V

    .line 1800399
    const-string p0, "nux"

    invoke-static {v0, v1, p0, v2, v3}, LX/8xw;->a(LX/8xw;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0TF;)V

    .line 1800400
    return-void
.end method

.method public static u(Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;)V
    .locals 2

    .prologue
    .line 1800401
    new-instance v0, LX/89k;

    invoke-direct {v0}, LX/89k;-><init>()V

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->z:Ljava/lang/String;

    .line 1800402
    iput-object v1, v0, LX/89k;->b:Ljava/lang/String;

    .line 1800403
    move-object v0, v0

    .line 1800404
    invoke-virtual {v0}, LX/89k;->a()Lcom/facebook/ipc/feed/PermalinkStoryIdParams;

    move-result-object v0

    .line 1800405
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->s:LX/0hy;

    invoke-interface {v1, v0}, LX/0hy;->a(Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;

    move-result-object v0

    .line 1800406
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->t:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1800407
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->finish()V

    .line 1800408
    return-void
.end method

.method public static v(Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1800352
    iput-boolean v1, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->F:Z

    .line 1800353
    iget-boolean v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->D:Z

    if-eqz v0, :cond_0

    .line 1800354
    iput-boolean v1, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->D:Z

    .line 1800355
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->v:LX/BbC;

    sget-object v1, LX/BbB;->CLICK_CREATE_MAP:LX/BbB;

    const-string v2, "has_suggested_place"

    invoke-virtual {v0, v1, v2}, LX/BbC;->a(LX/BbB;Ljava/lang/String;)V

    .line 1800356
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->r:LX/9jH;

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->z:Ljava/lang/String;

    invoke-virtual {v0, p0, v1}, LX/9jH;->a(Landroid/app/Activity;Ljava/lang/String;)V

    .line 1800357
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1800358
    iget-boolean v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->F:Z

    if-nez v0, :cond_0

    .line 1800359
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->v:LX/BbC;

    sget-object v1, LX/BbB;->CLICK_CREATE_MAP:LX/BbB;

    const-string v2, "has_suggested_place"

    invoke-virtual {v0, v1, v2}, LX/BbC;->a(LX/BbB;Ljava/lang/String;)V

    .line 1800360
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->r:LX/9jH;

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->z:Ljava/lang/String;

    invoke-virtual {v0, p0, v1}, LX/9jH;->a(Landroid/app/Activity;Ljava/lang/String;)V

    .line 1800361
    :goto_0
    return-void

    .line 1800362
    :cond_0
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->C:Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;

    if-nez v0, :cond_1

    .line 1800363
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->D:Z

    .line 1800364
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->G:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    goto :goto_0

    .line 1800365
    :cond_1
    invoke-static {p0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->r(Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1800305
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->onBackPressed()V

    .line 1800306
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1800268
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1800269
    invoke-static {p0, p0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1800270
    const v0, 0x7f03137e

    invoke-virtual {p0, v0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->setContentView(I)V

    .line 1800271
    const v0, 0x7f0d2cf9

    invoke-virtual {p0, v0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->G:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 1800272
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "post_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->z:Ljava/lang/String;

    .line 1800273
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "entry_point"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->A:Ljava/lang/String;

    .line 1800274
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "funnel_logging_tags"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->B:Ljava/lang/String;

    .line 1800275
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->w:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->y:LX/0Tn;

    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->z:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 1800276
    if-eqz v0, :cond_0

    .line 1800277
    invoke-static {p0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->u(Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;)V

    .line 1800278
    :goto_0
    return-void

    .line 1800279
    :cond_0
    if-nez p1, :cond_5

    .line 1800280
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->q()V

    .line 1800281
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->C:Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;

    if-nez v0, :cond_2

    .line 1800282
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->s()V

    .line 1800283
    :cond_2
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->B:Ljava/lang/String;

    invoke-static {v0}, LX/47C;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v0

    .line 1800284
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->v:LX/BbC;

    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->A:Ljava/lang/String;

    .line 1800285
    iget-boolean v3, v1, LX/BbC;->b:Z

    if-nez v3, :cond_4

    .line 1800286
    iget-object v3, v1, LX/BbC;->a:LX/0if;

    sget-object v4, LX/0ig;->az:LX/0ih;

    invoke-virtual {v3, v4}, LX/0if;->a(LX/0ih;)V

    .line 1800287
    iget-object v3, v1, LX/BbC;->a:LX/0if;

    sget-object v4, LX/0ig;->az:LX/0ih;

    const-string p0, "entry_point=%s"

    invoke-static {p0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v3, v4, p0}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 1800288
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1800289
    iget-object p0, v1, LX/BbC;->a:LX/0if;

    sget-object p1, LX/0ig;->az:LX/0ih;

    invoke-virtual {p0, p1, v3}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    goto :goto_2

    .line 1800290
    :cond_3
    const/4 v3, 0x1

    iput-boolean v3, v1, LX/BbC;->b:Z

    .line 1800291
    :cond_4
    goto :goto_0

    .line 1800292
    :cond_5
    const-string v0, "place_info_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->C:Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;

    .line 1800293
    const-string v0, "waiting_for_fetch_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->D:Z

    .line 1800294
    const-string v0, "has_predicted_location_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->F:Z

    .line 1800295
    const-string v0, "converting_post_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->E:Z

    .line 1800296
    iget-boolean v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->E:Z

    if-eqz v0, :cond_1

    .line 1800297
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->m()V

    goto :goto_1
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 1800298
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->G:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 1800299
    return-void
.end method

.method public final m()V
    .locals 0

    .prologue
    .line 1800300
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->t()V

    .line 1800301
    return-void
.end method

.method public final n()V
    .locals 2

    .prologue
    .line 1800302
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->v:LX/BbC;

    sget-object v1, LX/BbB;->CLICK_EDIT_CITY:LX/BbB;

    invoke-virtual {v0, v1}, LX/BbC;->a(LX/BbB;)V

    .line 1800303
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->r:LX/9jH;

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->z:Ljava/lang/String;

    invoke-virtual {v0, p0, v1}, LX/9jH;->a(Landroid/app/Activity;Ljava/lang/String;)V

    .line 1800304
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1800307
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1800308
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 1800309
    if-nez p2, :cond_0

    const/16 v0, 0x138a

    if-ne p1, v0, :cond_0

    .line 1800310
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->v:LX/BbC;

    sget-object v1, LX/BbB;->CANCEL_EDIT_CITY:LX/BbB;

    invoke-virtual {v0, v1}, LX/BbC;->a(LX/BbB;)V

    .line 1800311
    :cond_0
    :goto_0
    return-void

    .line 1800312
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 1800313
    :pswitch_0
    const-string v0, "extra_place"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1800314
    invoke-static {v0}, Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->C:Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;

    .line 1800315
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->t()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x138a
        :pswitch_0
    .end packed-switch
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 1800316
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d2d08

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 1800317
    instance-of v0, v0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionAcceptFragment;

    if-eqz v0, :cond_0

    .line 1800318
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->v:LX/BbC;

    sget-object v1, LX/BbB;->CANCEL_NUX:LX/BbB;

    invoke-virtual {v0, v1}, LX/BbC;->a(LX/BbB;)V

    .line 1800319
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->v:LX/BbC;

    invoke-virtual {v0}, LX/BbC;->a()V

    .line 1800320
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 1800321
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x44242246

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1800322
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 1800323
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->p:LX/BbF;

    .line 1800324
    iget-object v2, v1, LX/BbF;->b:LX/1Ck;

    invoke-virtual {v2}, LX/1Ck;->c()V

    .line 1800325
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->q:LX/8xw;

    .line 1800326
    iget-object v2, v1, LX/8xw;->b:LX/1Ck;

    invoke-virtual {v2}, LX/1Ck;->c()V

    .line 1800327
    const/16 v1, 0x23

    const v2, -0x453549ae

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x22

    const v2, 0x19e770d3

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1800328
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 1800329
    iget-boolean v1, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->D:Z

    if-eqz v1, :cond_0

    .line 1800330
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->G:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 1800331
    :goto_0
    const v1, 0x14c25049

    invoke-static {v1, v0}, LX/02F;->c(II)V

    return-void

    .line 1800332
    :cond_0
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->G:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1800333
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1800334
    const-string v0, "place_info_state"

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->C:Lcom/facebook/checkin/socialsearch/conversion/PlaceInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1800335
    const-string v0, "waiting_for_fetch_state"

    iget-boolean v1, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->D:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1800336
    const-string v0, "converting_post_state"

    iget-boolean v1, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->E:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1800337
    const-string v0, "has_predicted_location_state"

    iget-boolean v1, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionActivity;->F:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1800338
    return-void
.end method
