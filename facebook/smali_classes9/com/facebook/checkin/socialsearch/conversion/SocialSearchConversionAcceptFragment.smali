.class public Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionAcceptFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Bb5;

.field private c:Landroid/widget/Button;

.field private d:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1800214
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1800215
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1800216
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1800217
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionAcceptFragment;

    invoke-static {v0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionAcceptFragment;->a:Ljava/lang/Boolean;

    .line 1800218
    return-void
.end method

.method public final onAttach(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1800192
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onAttach(Landroid/content/Context;)V

    .line 1800193
    instance-of v0, p1, LX/Bb5;

    if-nez v0, :cond_0

    .line 1800194
    new-instance v0, Ljava/lang/ClassCastException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement SocialSearchConversionAcceptFragment.Listener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1800195
    :cond_0
    check-cast p1, LX/Bb5;

    iput-object p1, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionAcceptFragment;->b:LX/Bb5;

    .line 1800196
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x1522f45f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1800213
    const v1, 0x7f03137c

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x214211e9

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x55fdb60b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1800210
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1800211
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionAcceptFragment;->b:LX/Bb5;

    .line 1800212
    const/16 v1, 0x2b

    const v2, 0x41e69e2b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1800197
    const v0, 0x7f0d2d07

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionAcceptFragment;->c:Landroid/widget/Button;

    .line 1800198
    const v0, 0x7f0d2d06

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionAcceptFragment;->d:Landroid/widget/Button;

    .line 1800199
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionAcceptFragment;->c:Landroid/widget/Button;

    .line 1800200
    new-instance v1, LX/Bb3;

    invoke-direct {v1, p0}, LX/Bb3;-><init>(Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionAcceptFragment;)V

    move-object v1, v1

    .line 1800201
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1800202
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionAcceptFragment;->d:Landroid/widget/Button;

    .line 1800203
    new-instance v1, LX/Bb4;

    invoke-direct {v1, p0}, LX/Bb4;-><init>(Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionAcceptFragment;)V

    move-object v1, v1

    .line 1800204
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1800205
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/conversion/SocialSearchConversionAcceptFragment;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0829f6

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1800206
    const v0, 0x7f0d2d04

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 1800207
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1800208
    return-void

    .line 1800209
    :cond_0
    const v0, 0x7f0829f5

    goto :goto_0
.end method
