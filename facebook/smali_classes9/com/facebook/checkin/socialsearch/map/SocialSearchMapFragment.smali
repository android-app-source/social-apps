.class public Lcom/facebook/checkin/socialsearch/map/SocialSearchMapFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/6Zz;


# static fields
.field public static a:Ljava/lang/String;

.field public static b:Ljava/lang/String;


# instance fields
.field public c:LX/Bbv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/maps/delegate/MapFragmentDelegate;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1801317
    const-string v0, "place_list_id"

    sput-object v0, Lcom/facebook/checkin/socialsearch/map/SocialSearchMapFragment;->a:Ljava/lang/String;

    .line 1801318
    const-string v0, "map_center"

    sput-object v0, Lcom/facebook/checkin/socialsearch/map/SocialSearchMapFragment;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1801319
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/6al;)V
    .locals 6

    .prologue
    .line 1801320
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1801321
    sget-object v1, Lcom/facebook/checkin/socialsearch/map/SocialSearchMapFragment;->b:Ljava/lang/String;

    invoke-static {v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 1801322
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->N()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    .line 1801323
    new-instance v1, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLocation;->a()D

    move-result-wide v2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLocation;->b()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    const/high16 v0, 0x41500000    # 13.0f

    invoke-static {v1, v0}, LX/6aN;->a(Lcom/facebook/android/maps/model/LatLng;F)LX/6aM;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/6al;->a(LX/6aM;)V

    .line 1801324
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1801325
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1801326
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/checkin/socialsearch/map/SocialSearchMapFragment;

    new-instance v1, LX/Bbv;

    invoke-static {v3}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {v3}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object p1

    check-cast p1, LX/0sa;

    invoke-static {v3}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v0

    check-cast v0, LX/0rq;

    invoke-direct {v1, v2, p1, v0}, LX/Bbv;-><init>(LX/0tX;LX/0sa;LX/0rq;)V

    move-object v2, v1

    check-cast v2, LX/Bbv;

    invoke-static {v3}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/Executor;

    iput-object v2, p0, Lcom/facebook/checkin/socialsearch/map/SocialSearchMapFragment;->c:LX/Bbv;

    iput-object v3, p0, Lcom/facebook/checkin/socialsearch/map/SocialSearchMapFragment;->d:Ljava/util/concurrent/Executor;

    .line 1801327
    return-void
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 4

    .prologue
    .line 1801328
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onAttachFragment(Landroid/support/v4/app/Fragment;)V

    .line 1801329
    instance-of v0, p1, Lcom/facebook/maps/FbMapFragmentDelegate;

    if-eqz v0, :cond_0

    .line 1801330
    check-cast p1, Lcom/facebook/maps/delegate/MapFragmentDelegate;

    iput-object p1, p0, Lcom/facebook/checkin/socialsearch/map/SocialSearchMapFragment;->e:Lcom/facebook/maps/delegate/MapFragmentDelegate;

    .line 1801331
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/map/SocialSearchMapFragment;->e:Lcom/facebook/maps/delegate/MapFragmentDelegate;

    invoke-virtual {v0, p0}, Lcom/facebook/maps/delegate/MapFragmentDelegate;->a(LX/6Zz;)V

    .line 1801332
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1801333
    sget-object v1, Lcom/facebook/checkin/socialsearch/map/SocialSearchMapFragment;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1801334
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/map/SocialSearchMapFragment;->c:LX/Bbv;

    .line 1801335
    new-instance v2, LX/5IB;

    invoke-direct {v2}, LX/5IB;-><init>()V

    .line 1801336
    const-string v3, "place_list_id"

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1801337
    const-string v3, "profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v2, v3, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1801338
    const-string v3, "profile_pic_media_type"

    iget-object p1, v1, LX/Bbv;->c:LX/0rq;

    invoke-virtual {p1}, LX/0rq;->b()LX/0wF;

    move-result-object p1

    invoke-virtual {v2, v3, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 1801339
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    sget-object v3, LX/0zS;->c:LX/0zS;

    invoke-virtual {v2, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    .line 1801340
    iget-object v3, v1, LX/Bbv;->a:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    move-object v0, v2

    .line 1801341
    new-instance v1, LX/Bbx;

    invoke-direct {v1, p0}, LX/Bbx;-><init>(Lcom/facebook/checkin/socialsearch/map/SocialSearchMapFragment;)V

    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/map/SocialSearchMapFragment;->d:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1801342
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x4536e584

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1801343
    const v1, 0x7f030f6b

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1801344
    const/16 v2, 0x2b

    const v3, 0x5f45dd2e

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x69a48517

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1801345
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/checkin/socialsearch/map/SocialSearchMapFragment;->e:Lcom/facebook/maps/delegate/MapFragmentDelegate;

    .line 1801346
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1801347
    const/16 v1, 0x2b

    const v2, 0x68729e6e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
