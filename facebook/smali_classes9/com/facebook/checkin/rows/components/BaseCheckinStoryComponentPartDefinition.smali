.class public Lcom/facebook/checkin/rows/components/BaseCheckinStoryComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Ljava/lang/Object;",
        "TE;>;"
    }
.end annotation


# instance fields
.field private final d:LX/Bav;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Bav;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1800032
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1800033
    iput-object p2, p0, Lcom/facebook/checkin/rows/components/BaseCheckinStoryComponentPartDefinition;->d:LX/Bav;

    .line 1800034
    return-void
.end method

.method private a(LX/1De;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1800038
    iget-object v0, p0, Lcom/facebook/checkin/rows/components/BaseCheckinStoryComponentPartDefinition;->d:LX/Bav;

    const/4 v1, 0x0

    .line 1800039
    new-instance v2, LX/Bat;

    invoke-direct {v2, v0}, LX/Bat;-><init>(LX/Bav;)V

    .line 1800040
    iget-object p0, v0, LX/Bav;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Bau;

    .line 1800041
    if-nez p0, :cond_0

    .line 1800042
    new-instance p0, LX/Bau;

    invoke-direct {p0, v0}, LX/Bau;-><init>(LX/Bav;)V

    .line 1800043
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/Bau;->a$redex0(LX/Bau;LX/1De;IILX/Bat;)V

    .line 1800044
    move-object v2, p0

    .line 1800045
    move-object v1, v2

    .line 1800046
    move-object v0, v1

    .line 1800047
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1800037
    invoke-direct {p0, p1}, Lcom/facebook/checkin/rows/components/BaseCheckinStoryComponentPartDefinition;->a(LX/1De;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1800048
    invoke-direct {p0, p1}, Lcom/facebook/checkin/rows/components/BaseCheckinStoryComponentPartDefinition;->a(LX/1De;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1800036
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1800035
    const/4 v0, 0x0

    return-object v0
.end method
