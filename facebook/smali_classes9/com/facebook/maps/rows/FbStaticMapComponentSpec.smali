.class public Lcom/facebook/maps/rows/FbStaticMapComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/MountSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static c:LX/0Xm;


# instance fields
.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0W9;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1874961
    const-class v0, Lcom/facebook/maps/rows/FbStaticMapComponentSpec;

    const-string v1, "newsfeed_map_view"

    const-string v2, "map"

    const-string v3, "native_newsfeed"

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/maps/rows/FbStaticMapComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0W9;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1874958
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1874959
    iput-object p1, p0, Lcom/facebook/maps/rows/FbStaticMapComponentSpec;->b:LX/0Ot;

    .line 1874960
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/maps/rows/FbStaticMapComponentSpec;
    .locals 4

    .prologue
    .line 1874962
    const-class v1, Lcom/facebook/maps/rows/FbStaticMapComponentSpec;

    monitor-enter v1

    .line 1874963
    :try_start_0
    sget-object v0, Lcom/facebook/maps/rows/FbStaticMapComponentSpec;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1874964
    sput-object v2, Lcom/facebook/maps/rows/FbStaticMapComponentSpec;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1874965
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1874966
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1874967
    new-instance v3, Lcom/facebook/maps/rows/FbStaticMapComponentSpec;

    const/16 p0, 0x2be

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, Lcom/facebook/maps/rows/FbStaticMapComponentSpec;-><init>(LX/0Ot;)V

    .line 1874968
    move-object v0, v3

    .line 1874969
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1874970
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/maps/rows/FbStaticMapComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1874971
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1874972
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
