.class public Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;
.super Lcom/facebook/messaging/xma/ui/XMALinearLayout;
.source ""


# instance fields
.field private final b:Landroid/view/LayoutInflater;

.field private final c:Landroid/view/View$OnClickListener;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:I

.field private i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1875720
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1875721
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1875722
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1875723
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 1875724
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/messaging/xma/ui/XMALinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1875725
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->setOrientation(I)V

    .line 1875726
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->setGravity(I)V

    .line 1875727
    if-eqz p2, :cond_0

    .line 1875728
    sget-object v0, LX/03r;->CallToActionContainerView:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1875729
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->h:I

    .line 1875730
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->i:I

    .line 1875731
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1875732
    :cond_0
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->b:Landroid/view/LayoutInflater;

    .line 1875733
    new-instance v0, LX/CJk;

    invoke-direct {v0, p0}, LX/CJk;-><init>(Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;)V

    iput-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->c:Landroid/view/View$OnClickListener;

    .line 1875734
    return-void
.end method

.method private a(Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1875735
    iget-object v0, p1, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->e:LX/4ge;

    if-eqz v0, :cond_2

    sget-object v0, LX/4ge;->PAYMENT:LX/4ge;

    iget-object v1, p1, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->e:LX/4ge;

    invoke-virtual {v0, v1}, LX/4ge;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->l:Lcom/facebook/messaging/business/common/calltoaction/model/CTAPaymentInfo;

    if-eqz v0, :cond_2

    .line 1875736
    iput-boolean v2, p0, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->g:Z

    .line 1875737
    iget-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f030f8d

    invoke-virtual {v0, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1875738
    const v0, 0x7f0d258d

    invoke-static {v1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 1875739
    iget-object v4, p1, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->l:Lcom/facebook/messaging/business/common/calltoaction/model/CTAPaymentInfo;

    iget-object v4, v4, Lcom/facebook/messaging/business/common/calltoaction/model/CTAPaymentInfo;->a:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1875740
    iget v4, p0, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->i:I

    if-eqz v4, :cond_0

    .line 1875741
    iget v4, p0, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->i:I

    invoke-virtual {v0, v4}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 1875742
    :cond_0
    :goto_0
    const v0, 0x7f0d258c

    invoke-static {v1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 1875743
    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setTag(Ljava/lang/Object;)V

    .line 1875744
    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1875745
    invoke-virtual {p1}, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1875746
    iget-object v4, p0, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1875747
    iget-boolean v4, p1, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;->h:Z

    if-nez v4, :cond_3

    :goto_1
    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setEnabled(Z)V

    .line 1875748
    iget v2, p0, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->h:I

    if-eqz v2, :cond_1

    .line 1875749
    iget v2, p0, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->h:I

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 1875750
    :cond_1
    invoke-virtual {p0, v1}, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->addView(Landroid/view/View;)V

    .line 1875751
    return-void

    .line 1875752
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f030f8c

    invoke-virtual {v0, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_3
    move v2, v3

    .line 1875753
    goto :goto_1
.end method

.method private a(II)Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1875754
    iget-boolean v0, p0, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->g:Z

    if-eqz v0, :cond_1

    move v1, v3

    .line 1875755
    :cond_0
    :goto_0
    return v1

    .line 1875756
    :cond_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    move v0, v1

    move v2, v1

    .line 1875757
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->getChildCount()I

    move-result v5

    if-ge v0, v5, :cond_3

    .line 1875758
    invoke-virtual {p0, v0}, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 1875759
    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v6

    if-nez v6, :cond_2

    .line 1875760
    invoke-virtual {p0, v5, p1, p2}, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->measureChild(Landroid/view/View;II)V

    .line 1875761
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v2, v5

    .line 1875762
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1875763
    :cond_3
    if-le v2, v4, :cond_0

    move v1, v3

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/util/List;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;",
            ">;",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1875764
    iput-object p1, p0, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->d:Ljava/util/List;

    .line 1875765
    iput-object p2, p0, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->e:Landroid/net/Uri;

    .line 1875766
    iput-object p3, p0, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->f:Ljava/lang/String;

    .line 1875767
    iput-boolean v1, p0, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->g:Z

    .line 1875768
    iget-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->d:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1875769
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->setVisibility(I)V

    .line 1875770
    :cond_1
    return-void

    .line 1875771
    :cond_2
    invoke-virtual {p0, v1}, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->setVisibility(I)V

    .line 1875772
    invoke-virtual {p0}, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->removeAllViews()V

    .line 1875773
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    .line 1875774
    invoke-direct {p0, v0}, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->a(Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;)V

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 1

    .prologue
    .line 1875775
    invoke-direct {p0, p1, p2}, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->a(II)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 1875776
    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/messaging/business/common/calltoaction/CallToActionContainerView;->setOrientation(I)V

    .line 1875777
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/xma/ui/XMALinearLayout;->onMeasure(II)V

    .line 1875778
    return-void

    .line 1875779
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
