.class public final Lcom/facebook/messaging/business/subscription/instantarticle/graphql/BusinessSubscriptionIAQueriesModels$BusinessSubscriptionMessengerContentSubscribedQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/business/subscription/instantarticle/graphql/BusinessSubscriptionIAQueriesModels$BusinessSubscriptionMessengerContentSubscribedQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1877320
    const-class v0, Lcom/facebook/messaging/business/subscription/instantarticle/graphql/BusinessSubscriptionIAQueriesModels$BusinessSubscriptionMessengerContentSubscribedQueryModel;

    new-instance v1, Lcom/facebook/messaging/business/subscription/instantarticle/graphql/BusinessSubscriptionIAQueriesModels$BusinessSubscriptionMessengerContentSubscribedQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/business/subscription/instantarticle/graphql/BusinessSubscriptionIAQueriesModels$BusinessSubscriptionMessengerContentSubscribedQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1877321
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1877322
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/business/subscription/instantarticle/graphql/BusinessSubscriptionIAQueriesModels$BusinessSubscriptionMessengerContentSubscribedQueryModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1877323
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1877324
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x0

    .line 1877325
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1877326
    invoke-virtual {v1, v0, p2}, LX/15i;->g(II)I

    move-result p0

    .line 1877327
    if-eqz p0, :cond_0

    .line 1877328
    const-string p0, "__type__"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1877329
    invoke-static {v1, v0, p2, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1877330
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v0, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 1877331
    if-eqz p0, :cond_1

    .line 1877332
    const-string p2, "is_viewer_subscribed_to_messenger_content"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1877333
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 1877334
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1877335
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1877336
    check-cast p1, Lcom/facebook/messaging/business/subscription/instantarticle/graphql/BusinessSubscriptionIAQueriesModels$BusinessSubscriptionMessengerContentSubscribedQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/business/subscription/instantarticle/graphql/BusinessSubscriptionIAQueriesModels$BusinessSubscriptionMessengerContentSubscribedQueryModel$Serializer;->a(Lcom/facebook/messaging/business/subscription/instantarticle/graphql/BusinessSubscriptionIAQueriesModels$BusinessSubscriptionMessengerContentSubscribedQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
