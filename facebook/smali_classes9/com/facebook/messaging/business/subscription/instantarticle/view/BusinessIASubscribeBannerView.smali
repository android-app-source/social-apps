.class public Lcom/facebook/messaging/business/subscription/instantarticle/view/BusinessIASubscribeBannerView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private final a:Lcom/facebook/widget/tiles/ThreadTileView;

.field private final b:Lcom/facebook/resources/ui/FbTextView;

.field private final c:Lcom/facebook/resources/ui/FbTextView;

.field private final d:Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1877505
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/messaging/business/subscription/instantarticle/view/BusinessIASubscribeBannerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1877506
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1877507
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/business/subscription/instantarticle/view/BusinessIASubscribeBannerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1877508
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1877509
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1877510
    const v0, 0x7f030212

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1877511
    const v0, 0x7f0d0825

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tiles/ThreadTileView;

    iput-object v0, p0, Lcom/facebook/messaging/business/subscription/instantarticle/view/BusinessIASubscribeBannerView;->a:Lcom/facebook/widget/tiles/ThreadTileView;

    .line 1877512
    const v0, 0x7f0d0826

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/subscription/instantarticle/view/BusinessIASubscribeBannerView;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1877513
    const v0, 0x7f0d0827

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/subscription/instantarticle/view/BusinessIASubscribeBannerView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 1877514
    const v0, 0x7f0d0828

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;

    iput-object v0, p0, Lcom/facebook/messaging/business/subscription/instantarticle/view/BusinessIASubscribeBannerView;->d:Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;

    .line 1877515
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/business/subscription/instantarticle/view/BusinessIASubscribeBannerView;->setClickable(Z)V

    .line 1877516
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;LX/8Vc;Ljava/lang/String;Ljava/lang/String;LX/CKe;)V
    .locals 2

    .prologue
    .line 1877517
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/instantarticle/view/BusinessIASubscribeBannerView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1877518
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/instantarticle/view/BusinessIASubscribeBannerView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1877519
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/instantarticle/view/BusinessIASubscribeBannerView;->a:Lcom/facebook/widget/tiles/ThreadTileView;

    invoke-virtual {v0, p3}, Lcom/facebook/widget/tiles/ThreadTileView;->setThreadTileViewData(LX/8Vc;)V

    .line 1877520
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/instantarticle/view/BusinessIASubscribeBannerView;->d:Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;

    .line 1877521
    iput-object p4, v0, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;->d:Ljava/lang/String;

    .line 1877522
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/instantarticle/view/BusinessIASubscribeBannerView;->d:Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;

    .line 1877523
    iput-object p5, v0, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;->e:Ljava/lang/String;

    .line 1877524
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/instantarticle/view/BusinessIASubscribeBannerView;->d:Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;

    new-instance v1, LX/CKl;

    invoke-direct {v1, p0, p6}, LX/CKl;-><init>(Lcom/facebook/messaging/business/subscription/instantarticle/view/BusinessIASubscribeBannerView;LX/CKe;)V

    .line 1877525
    iput-object v1, v0, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;->f:LX/CKX;

    .line 1877526
    return-void
.end method
