.class public Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/CKT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final c:LX/CKR;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:LX/CKX;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final g:Lcom/facebook/widget/text/BetterTextView;

.field private final h:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1877272
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1877273
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1877270
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1877271
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1877261
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1877262
    new-instance v0, LX/CKU;

    invoke-direct {v0, p0}, LX/CKU;-><init>(Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;)V

    iput-object v0, p0, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;->c:LX/CKR;

    .line 1877263
    const-class v0, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;

    invoke-static {v0, p0}, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1877264
    const v0, 0x7f030218

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1877265
    const v0, 0x7f0d083d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;->g:Lcom/facebook/widget/text/BetterTextView;

    .line 1877266
    const v0, 0x7f0d083e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;->h:Landroid/widget/ImageView;

    .line 1877267
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;->g:Lcom/facebook/widget/text/BetterTextView;

    new-instance v1, LX/CKV;

    invoke-direct {v1, p0}, LX/CKV;-><init>(Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1877268
    sget-object v0, LX/CKW;->SUBSCRIBE:LX/CKW;

    invoke-static {p0, v0}, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;->a$redex0(Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;LX/CKW;)V

    .line 1877269
    return-void
.end method

.method private static a(Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;LX/0wM;LX/CKT;)V
    .locals 0

    .prologue
    .line 1877260
    iput-object p1, p0, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;->a:LX/0wM;

    iput-object p2, p0, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;->b:LX/CKT;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;

    invoke-static {v1}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    invoke-static {v1}, LX/CKT;->b(LX/0QB;)LX/CKT;

    move-result-object v1

    check-cast v1, LX/CKT;

    invoke-static {p0, v0, v1}, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;->a(Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;LX/0wM;LX/CKT;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;LX/CKW;)V
    .locals 0

    .prologue
    .line 1877274
    invoke-direct {p0, p1}, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;->b(LX/CKW;)V

    .line 1877275
    invoke-direct {p0, p1}, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;->c(LX/CKW;)V

    .line 1877276
    invoke-direct {p0, p1}, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;->d(LX/CKW;)V

    .line 1877277
    return-void
.end method

.method private b(LX/CKW;)V
    .locals 2

    .prologue
    .line 1877255
    sget-object v0, LX/CKW;->SUBSCRIBE:LX/CKW;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    .line 1877256
    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;->setEnabled(Z)V

    .line 1877257
    iget-object v1, p0, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setEnabled(Z)V

    .line 1877258
    return-void

    .line 1877259
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(LX/CKW;)V
    .locals 3

    .prologue
    .line 1877239
    iget-object v1, p0, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;->g:Lcom/facebook/widget/text/BetterTextView;

    sget-object v0, LX/CKW;->SUBSCRIBE:LX/CKW;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a0642

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 1877240
    return-void

    .line 1877241
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a0643

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0
.end method

.method private d(LX/CKW;)V
    .locals 4

    .prologue
    .line 1877248
    sget-object v0, LX/CKW;->SUBSCRIBE:LX/CKW;

    if-ne p1, v0, :cond_0

    const v0, 0x7f0a0642

    .line 1877249
    :goto_0
    sget-object v1, LX/CKW;->SUBSCRIBED:LX/CKW;

    if-ne p1, v1, :cond_1

    const v1, 0x7f0207da

    .line 1877250
    :goto_1
    iget-object v2, p0, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;->a:LX/0wM;

    invoke-virtual {p0}, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v2, v1, v0}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1877251
    iget-object v1, p0, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;->h:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1877252
    return-void

    .line 1877253
    :cond_0
    const v0, 0x7f0a0643

    goto :goto_0

    .line 1877254
    :cond_1
    const v1, 0x7f020741

    goto :goto_1
.end method


# virtual methods
.method public setButtonSource(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1877246
    iput-object p1, p0, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;->e:Ljava/lang/String;

    .line 1877247
    return-void
.end method

.method public setSubscribeListener(LX/CKX;)V
    .locals 0

    .prologue
    .line 1877244
    iput-object p1, p0, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;->f:LX/CKX;

    .line 1877245
    return-void
.end method

.method public setSubscribePageId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1877242
    iput-object p1, p0, Lcom/facebook/messaging/business/subscription/common/view/BusinessSubscribeButton;->d:Ljava/lang/String;

    .line 1877243
    return-void
.end method
