.class public final Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$NullStateSubstationQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$NullStateSubstationQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1878472
    const-class v0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$NullStateSubstationQueryModel;

    new-instance v1, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$NullStateSubstationQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$NullStateSubstationQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1878473
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1878474
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$NullStateSubstationQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1878475
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1878476
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1878477
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1878478
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1878479
    if-eqz v2, :cond_0

    .line 1878480
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1878481
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1878482
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1878483
    if-eqz v2, :cond_1

    .line 1878484
    const-string p0, "messenger_content_broadcast_sub_stations"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1878485
    invoke-static {v1, v2, p1, p2}, LX/CLB;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1878486
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1878487
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1878488
    check-cast p1, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$NullStateSubstationQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$NullStateSubstationQueryModel$Serializer;->a(Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$NullStateSubstationQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
