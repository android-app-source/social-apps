.class public final Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x46ac925d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1877911
    const-class v0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1877912
    const-class v0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1877913
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1877914
    return-void
.end method

.method private j()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1877921
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->c(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel;->e:Ljava/util/List;

    .line 1877922
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1877915
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel;->f:Ljava/lang/String;

    .line 1877916
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1877917
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel;->g:Ljava/lang/String;

    .line 1877918
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1877919
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel;->h:Ljava/lang/String;

    .line 1877920
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method private n()Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel$ProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1877895
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel;->i:Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel$ProfilePictureModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel$ProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel$ProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel;->i:Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel$ProfilePictureModel;

    .line 1877896
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel;->i:Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel$ProfilePictureModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1877897
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1877898
    invoke-direct {p0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->c(Ljava/util/List;)I

    move-result v0

    .line 1877899
    invoke-direct {p0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1877900
    invoke-direct {p0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1877901
    invoke-direct {p0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1877902
    invoke-direct {p0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel;->n()Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel$ProfilePictureModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1877903
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1877904
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1877905
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1877906
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1877907
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1877908
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1877909
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1877910
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1877887
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1877888
    invoke-direct {p0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel;->n()Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1877889
    invoke-direct {p0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel;->n()Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel$ProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel$ProfilePictureModel;

    .line 1877890
    invoke-direct {p0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel;->n()Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel$ProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1877891
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel;

    .line 1877892
    iput-object v0, v1, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel;->i:Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel$ProfilePictureModel;

    .line 1877893
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1877894
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1877886
    new-instance v0, LX/CKw;

    invoke-direct {v0, p1}, LX/CKw;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1877885
    invoke-direct {p0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1877883
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1877884
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1877882
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1877879
    new-instance v0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel;

    invoke-direct {v0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$ContentSubscriptionPublisherModel;-><init>()V

    .line 1877880
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1877881
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1877878
    const v0, 0x564d4671

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1877877
    const v0, 0x25d6af

    return v0
.end method
