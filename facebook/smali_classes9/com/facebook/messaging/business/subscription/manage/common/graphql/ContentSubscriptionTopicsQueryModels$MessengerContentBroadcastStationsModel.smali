.class public final Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2cae50aa
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Z

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1877729
    const-class v0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1877728
    const-class v0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1877726
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1877727
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1877668
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1877669
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1877670
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1877720
    iput-boolean p1, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->i:Z

    .line 1877721
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1877722
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1877723
    if-eqz v0, :cond_0

    .line 1877724
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1877725
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1877706
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1877707
    invoke-virtual {p0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1877708
    invoke-virtual {p0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1877709
    invoke-virtual {p0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1877710
    invoke-virtual {p0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1877711
    const/4 v4, 0x6

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1877712
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1877713
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1877714
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1877715
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1877716
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1877717
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1877718
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1877719
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1877703
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1877704
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1877705
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1877702
    new-instance v0, LX/CKq;

    invoke-direct {v0, p1}, LX/CKq;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1877701
    invoke-virtual {p0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1877697
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1877698
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->g:Z

    .line 1877699
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->i:Z

    .line 1877700
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1877691
    const-string v0, "is_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1877692
    invoke-virtual {p0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->n()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1877693
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1877694
    const/4 v0, 0x4

    iput v0, p2, LX/18L;->c:I

    .line 1877695
    :goto_0
    return-void

    .line 1877696
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1877688
    const-string v0, "is_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1877689
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->a(Z)V

    .line 1877690
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1877685
    new-instance v0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;

    invoke-direct {v0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;-><init>()V

    .line 1877686
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1877687
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1877684
    const v0, -0xf5f1ef

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1877683
    const v0, 0x58e74759

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1877681
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->e:Ljava/lang/String;

    .line 1877682
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1877679
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->f:Ljava/lang/String;

    .line 1877680
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 1877677
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1877678
    iget-boolean v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->g:Z

    return v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1877675
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->h:Ljava/lang/String;

    .line 1877676
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Z
    .locals 2

    .prologue
    .line 1877673
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1877674
    iget-boolean v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->i:Z

    return v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1877671
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->j:Ljava/lang/String;

    .line 1877672
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/ContentSubscriptionTopicsQueryModels$MessengerContentBroadcastStationsModel;->j:Ljava/lang/String;

    return-object v0
.end method
