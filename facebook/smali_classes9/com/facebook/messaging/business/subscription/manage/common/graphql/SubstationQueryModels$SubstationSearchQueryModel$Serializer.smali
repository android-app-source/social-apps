.class public final Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$SubstationSearchQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$SubstationSearchQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1878554
    const-class v0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$SubstationSearchQueryModel;

    new-instance v1, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$SubstationSearchQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$SubstationSearchQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1878555
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1878556
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$SubstationSearchQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1878557
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1878558
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1878559
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1878560
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1878561
    if-eqz v2, :cond_0

    .line 1878562
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1878563
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1878564
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1878565
    if-eqz v2, :cond_1

    .line 1878566
    const-string p0, "messenger_content_broadcast_sub_stations_search"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1878567
    invoke-static {v1, v2, p1, p2}, LX/CLA;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1878568
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1878569
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1878570
    check-cast p1, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$SubstationSearchQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$SubstationSearchQueryModel$Serializer;->a(Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$SubstationSearchQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
