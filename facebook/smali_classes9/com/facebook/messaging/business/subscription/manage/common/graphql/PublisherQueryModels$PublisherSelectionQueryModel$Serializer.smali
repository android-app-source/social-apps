.class public final Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1878061
    const-class v0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel;

    new-instance v1, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1878062
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1878063
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1878064
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1878065
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1878066
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1878067
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1878068
    if-eqz v2, :cond_0

    .line 1878069
    const-string p0, "messenger_subscription_publisher"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1878070
    invoke-static {v1, v2, p1, p2}, LX/CL0;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1878071
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1878072
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1878073
    check-cast p1, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel$Serializer;->a(Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
