.class public final Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$SubstationSearchQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x30ff9072
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$SubstationSearchQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$SubstationSearchQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$ContentSubscriptionSubstationModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1878595
    const-class v0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$SubstationSearchQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1878596
    const-class v0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$SubstationSearchQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1878593
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1878594
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1878590
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$SubstationSearchQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1878591
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$SubstationSearchQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1878592
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$SubstationSearchQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1878582
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1878583
    invoke-direct {p0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$SubstationSearchQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1878584
    invoke-virtual {p0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$SubstationSearchQueryModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1878585
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1878586
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1878587
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1878588
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1878589
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$ContentSubscriptionSubstationModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1878580
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$SubstationSearchQueryModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$ContentSubscriptionSubstationModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$SubstationSearchQueryModel;->f:Ljava/util/List;

    .line 1878581
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$SubstationSearchQueryModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1878597
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1878598
    invoke-virtual {p0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$SubstationSearchQueryModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1878599
    invoke-virtual {p0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$SubstationSearchQueryModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1878600
    if-eqz v1, :cond_0

    .line 1878601
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$SubstationSearchQueryModel;

    .line 1878602
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$SubstationSearchQueryModel;->f:Ljava/util/List;

    .line 1878603
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1878604
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1878579
    new-instance v0, LX/CL8;

    invoke-direct {v0, p1}, LX/CL8;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1878577
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1878578
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1878576
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1878573
    new-instance v0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$SubstationSearchQueryModel;

    invoke-direct {v0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/SubstationQueryModels$SubstationSearchQueryModel;-><init>()V

    .line 1878574
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1878575
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1878571
    const v0, -0x7db70821

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1878572
    const v0, 0x252222

    return v0
.end method
