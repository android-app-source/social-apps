.class public final Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x73257281
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel$MessengerSubscriptionPublisherModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1878095
    const-class v0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1878096
    const-class v0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1878097
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1878098
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1878089
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1878090
    invoke-virtual {p0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel;->a()Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel$MessengerSubscriptionPublisherModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1878091
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1878092
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1878093
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1878094
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1878074
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1878075
    invoke-virtual {p0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel;->a()Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel$MessengerSubscriptionPublisherModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1878076
    invoke-virtual {p0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel;->a()Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel$MessengerSubscriptionPublisherModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel$MessengerSubscriptionPublisherModel;

    .line 1878077
    invoke-virtual {p0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel;->a()Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel$MessengerSubscriptionPublisherModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1878078
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel;

    .line 1878079
    iput-object v0, v1, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel;->e:Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel$MessengerSubscriptionPublisherModel;

    .line 1878080
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1878081
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel$MessengerSubscriptionPublisherModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMessengerSubscriptionPublisher"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1878087
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel;->e:Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel$MessengerSubscriptionPublisherModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel$MessengerSubscriptionPublisherModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel$MessengerSubscriptionPublisherModel;

    iput-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel;->e:Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel$MessengerSubscriptionPublisherModel;

    .line 1878088
    iget-object v0, p0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel;->e:Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel$MessengerSubscriptionPublisherModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1878084
    new-instance v0, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel;

    invoke-direct {v0}, Lcom/facebook/messaging/business/subscription/manage/common/graphql/PublisherQueryModels$PublisherSelectionQueryModel;-><init>()V

    .line 1878085
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1878086
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1878083
    const v0, 0x419d56ff

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1878082
    const v0, -0x6747e1ce

    return v0
.end method
