.class public Lcom/facebook/messaging/business/ride/utils/RidePromoShareExtras;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/business/ride/utils/RidePromoShareExtras;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/facebook/ui/media/attachments/MediaResource;

.field private final d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1876316
    new-instance v0, LX/CK1;

    invoke-direct {v0}, LX/CK1;-><init>()V

    sput-object v0, Lcom/facebook/messaging/business/ride/utils/RidePromoShareExtras;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1876317
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1876318
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RidePromoShareExtras;->a:Ljava/lang/String;

    .line 1876319
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RidePromoShareExtras;->b:Ljava/lang/String;

    .line 1876320
    const-class v0, Lcom/facebook/messaging/business/ride/utils/RidePromoShareExtras;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RidePromoShareExtras;->c:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 1876321
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RidePromoShareExtras;->d:Ljava/lang/String;

    .line 1876322
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1876323
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1876324
    iput-object p1, p0, Lcom/facebook/messaging/business/ride/utils/RidePromoShareExtras;->a:Ljava/lang/String;

    .line 1876325
    iput-object p2, p0, Lcom/facebook/messaging/business/ride/utils/RidePromoShareExtras;->b:Ljava/lang/String;

    .line 1876326
    iput-object p3, p0, Lcom/facebook/messaging/business/ride/utils/RidePromoShareExtras;->c:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 1876327
    iput-object p4, p0, Lcom/facebook/messaging/business/ride/utils/RidePromoShareExtras;->d:Ljava/lang/String;

    .line 1876328
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1876329
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1876330
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RidePromoShareExtras;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1876331
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RidePromoShareExtras;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1876332
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RidePromoShareExtras;->c:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1876333
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RidePromoShareExtras;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1876334
    return-void
.end method
