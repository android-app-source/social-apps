.class public Lcom/facebook/messaging/business/ride/utils/RideServiceParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/business/ride/utils/RideServiceParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Lcom/facebook/location/Coordinates;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1876339
    new-instance v0, LX/CK2;

    invoke-direct {v0}, LX/CK2;-><init>()V

    sput-object v0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/CK3;)V
    .locals 1

    .prologue
    .line 1876340
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1876341
    iget-object v0, p1, LX/CK3;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1876342
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->a:Ljava/lang/String;

    .line 1876343
    iget-object v0, p1, LX/CK3;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1876344
    iget-object v0, p1, LX/CK3;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->c:Ljava/lang/String;

    .line 1876345
    iget-object v0, p1, LX/CK3;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->d:Ljava/lang/String;

    .line 1876346
    iget-object v0, p1, LX/CK3;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->e:Ljava/lang/String;

    .line 1876347
    iget-object v0, p1, LX/CK3;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->f:Ljava/lang/String;

    .line 1876348
    iget-object v0, p1, LX/CK3;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->g:Ljava/lang/String;

    .line 1876349
    iget-object v0, p1, LX/CK3;->h:Lcom/facebook/location/Coordinates;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->h:Lcom/facebook/location/Coordinates;

    .line 1876350
    iget-object v0, p1, LX/CK3;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->i:Ljava/lang/String;

    .line 1876351
    iget-object v0, p1, LX/CK3;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->j:Ljava/lang/String;

    .line 1876352
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1876353
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1876354
    const-class v0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    .line 1876355
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->a:Ljava/lang/String;

    .line 1876356
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1876357
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->c:Ljava/lang/String;

    .line 1876358
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->d:Ljava/lang/String;

    .line 1876359
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->e:Ljava/lang/String;

    .line 1876360
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->f:Ljava/lang/String;

    .line 1876361
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->g:Ljava/lang/String;

    .line 1876362
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/location/Coordinates;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->h:Lcom/facebook/location/Coordinates;

    .line 1876363
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->i:Ljava/lang/String;

    .line 1876364
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->j:Ljava/lang/String;

    .line 1876365
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1876366
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1876367
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1876368
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1876369
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1876370
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1876371
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1876372
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1876373
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1876374
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->h:Lcom/facebook/location/Coordinates;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1876375
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1876376
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1876377
    return-void
.end method
