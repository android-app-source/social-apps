.class public Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final j:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/CNI;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/CNJ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/1Mv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Mv",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Lcom/facebook/ui/media/attachments/MediaResourceHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/8tH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private k:LX/CNK;

.field private l:LX/2MK;

.field public m:Lcom/facebook/video/player/InlineVideoView;

.field public n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public o:Landroid/widget/ImageView;

.field public p:Landroid/widget/TextView;

.field private q:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1881582
    const-class v0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;

    sput-object v0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->j:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1881579
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1881580
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->a(Landroid/util/AttributeSet;)V

    .line 1881581
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1881576
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1881577
    invoke-direct {p0, p2}, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->a(Landroid/util/AttributeSet;)V

    .line 1881578
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1881573
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1881574
    invoke-direct {p0, p2}, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->a(Landroid/util/AttributeSet;)V

    .line 1881575
    return-void
.end method

.method public static synthetic a(Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/ui/media/attachments/MediaResource;
    .locals 1

    .prologue
    .line 1881572
    invoke-direct {p0, p1}, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/ui/media/attachments/MediaResource;
    .locals 3

    .prologue
    .line 1881564
    invoke-static {}, Lcom/facebook/ui/media/attachments/MediaResource;->a()LX/5zn;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/5zn;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zn;

    move-result-object v0

    .line 1881565
    iget-object v1, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->l:LX/2MK;

    sget-object v2, LX/2MK;->VIDEO:LX/2MK;

    if-ne v1, v2, :cond_0

    .line 1881566
    iget-object v1, v0, LX/5zn;->d:LX/5zj;

    move-object v1, v1

    .line 1881567
    sget-object v2, LX/5zj;->UNSPECIFIED:LX/5zj;

    if-ne v1, v2, :cond_0

    .line 1881568
    sget-object v1, LX/5zj;->CAMERA:LX/5zj;

    .line 1881569
    iput-object v1, v0, LX/5zn;->d:LX/5zj;

    .line 1881570
    :cond_0
    iget-object v1, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->h:Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    invoke-virtual {v1, v0}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->a(LX/5zn;)V

    .line 1881571
    invoke-virtual {v0}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    return-object v0
.end method

.method private static a(J)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1881560
    long-to-float v0, p0

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 1881561
    div-int/lit8 v1, v0, 0x3c

    .line 1881562
    rem-int/lit8 v0, v0, 0x3c

    .line 1881563
    const-string v2, "%d:%02d"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v2, v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/04g;)V
    .locals 2

    .prologue
    .line 1881555
    iget-object v0, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->m:Lcom/facebook/video/player/InlineVideoView;

    invoke-virtual {v0}, Lcom/facebook/video/player/InlineVideoView;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1881556
    iget-boolean v0, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->q:Z

    if-eqz v0, :cond_0

    .line 1881557
    iget-object v0, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->m:Lcom/facebook/video/player/InlineVideoView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/InlineVideoView;->setVisibility(I)V

    .line 1881558
    iget-object v0, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->m:Lcom/facebook/video/player/InlineVideoView;

    invoke-virtual {v0, p1}, Lcom/facebook/video/player/InlineVideoView;->a(LX/04g;)V

    .line 1881559
    :cond_0
    return-void
.end method

.method private a(Landroid/util/AttributeSet;)V
    .locals 4
    .param p1    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1881583
    const-class v0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1881584
    if-nez p1, :cond_0

    .line 1881585
    :goto_0
    return-void

    .line 1881586
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->MediaSharePreviewPlayableView:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1881587
    invoke-static {}, LX/CNK;->values()[LX/CNK;

    move-result-object v1

    const/16 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->k:LX/CNK;

    .line 1881588
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;LX/0TD;LX/1Ad;LX/03V;Ljava/util/concurrent/ExecutorService;Lcom/facebook/ui/media/attachments/MediaResourceHelper;LX/8tH;)V
    .locals 0

    .prologue
    .line 1881554
    iput-object p1, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->d:LX/0TD;

    iput-object p2, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->e:LX/1Ad;

    iput-object p3, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->f:LX/03V;

    iput-object p4, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->g:Ljava/util/concurrent/ExecutorService;

    iput-object p5, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->h:Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    iput-object p6, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->i:LX/8tH;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;

    invoke-static {v6}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, LX/0TD;

    invoke-static {v6}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v2

    check-cast v2, LX/1Ad;

    invoke-static {v6}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v6}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {v6}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->a(LX/0QB;)Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    move-result-object v5

    check-cast v5, Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    invoke-static {v6}, LX/8tH;->a(LX/0QB;)LX/8tH;

    move-result-object v6

    check-cast v6, LX/8tH;

    invoke-static/range {v0 .. v6}, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->a(Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;LX/0TD;LX/1Ad;LX/03V;Ljava/util/concurrent/ExecutorService;Lcom/facebook/ui/media/attachments/MediaResourceHelper;LX/8tH;)V

    return-void
.end method

.method public static setupAudioPlaceholder(Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 4

    .prologue
    .line 1881550
    const v0, 0x7f0d214a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->o:Landroid/widget/ImageView;

    .line 1881551
    iget-object v0, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->p:Landroid/widget/TextView;

    iget-wide v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->j:J

    invoke-static {v2, v3}, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1881552
    iget-object v0, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->p:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1881553
    return-void
.end method

.method public static setupInlineVideo(Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1881517
    const v0, 0x7f0d1b44

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/InlineVideoView;

    iput-object v0, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->m:Lcom/facebook/video/player/InlineVideoView;

    .line 1881518
    invoke-static {}, Lcom/facebook/video/engine/VideoDataSource;->newBuilder()LX/2oE;

    move-result-object v0

    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    .line 1881519
    iput-object v1, v0, LX/2oE;->a:Landroid/net/Uri;

    .line 1881520
    move-object v0, v0

    .line 1881521
    sget-object v1, LX/097;->FROM_LOCAL_STORAGE:LX/097;

    .line 1881522
    iput-object v1, v0, LX/2oE;->e:LX/097;

    .line 1881523
    move-object v1, v0

    .line 1881524
    iget-boolean v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->n:Z

    if-eqz v0, :cond_0

    sget-object v0, LX/2oF;->MIRROR_HORIZONTALLY:LX/2oF;

    .line 1881525
    :goto_0
    iput-object v0, v1, LX/2oE;->g:LX/2oF;

    .line 1881526
    move-object v0, v1

    .line 1881527
    invoke-virtual {v0}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v0

    .line 1881528
    invoke-static {}, Lcom/facebook/video/engine/VideoPlayerParams;->newBuilder()LX/2oH;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/2oH;->a(Lcom/facebook/video/engine/VideoDataSource;)LX/2oH;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/ui/media/attachments/MediaResource;->b()Ljava/lang/String;

    move-result-object v1

    .line 1881529
    iput-object v1, v0, LX/2oH;->b:Ljava/lang/String;

    .line 1881530
    move-object v0, v0

    .line 1881531
    iget-wide v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->j:J

    long-to-int v1, v2

    .line 1881532
    iput v1, v0, LX/2oH;->c:I

    .line 1881533
    move-object v0, v0

    .line 1881534
    iput-boolean v4, v0, LX/2oH;->g:Z

    .line 1881535
    move-object v0, v0

    .line 1881536
    invoke-virtual {v0}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v0

    .line 1881537
    iget-object v1, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->m:Lcom/facebook/video/player/InlineVideoView;

    invoke-virtual {v1, v0}, Lcom/facebook/video/player/InlineVideoView;->setVideoData(Lcom/facebook/video/engine/VideoPlayerParams;)V

    .line 1881538
    iget-object v0, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->m:Lcom/facebook/video/player/InlineVideoView;

    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v0, v4, v1}, Lcom/facebook/video/player/InlineVideoView;->a(ZLX/04g;)V

    .line 1881539
    iget-object v0, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->m:Lcom/facebook/video/player/InlineVideoView;

    sget-object v1, LX/04D;->MESSENGER_VIDEO_MEDIA_SHARE_VIEW:LX/04D;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/InlineVideoView;->setPlayerOrigin(LX/04D;)V

    .line 1881540
    iput-boolean v4, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->q:Z

    .line 1881541
    sget-object v0, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-direct {p0, v0}, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->a(LX/04g;)V

    .line 1881542
    sget-object v0, LX/CNH;->a:[I

    iget-object v1, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->k:LX/CNK;

    invoke-virtual {v1}, LX/CNK;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1881543
    :goto_1
    iget-object v0, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->i:LX/8tH;

    invoke-virtual {p1}, Lcom/facebook/ui/media/attachments/MediaResource;->c()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, LX/8tH;->a(J)Ljava/lang/String;

    move-result-object v0

    .line 1881544
    iget-object v1, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->p:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1881545
    iget-object v0, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->p:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1881546
    return-void

    .line 1881547
    :cond_0
    sget-object v0, LX/2oF;->NONE:LX/2oF;

    goto :goto_0

    .line 1881548
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->m:Lcom/facebook/video/player/InlineVideoView;

    sget-object v1, LX/7LL;->CENTER_CROP:LX/7LL;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/InlineVideoView;->setScaleType(LX/7LL;)V

    goto :goto_1

    .line 1881549
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->m:Lcom/facebook/video/player/InlineVideoView;

    sget-object v1, LX/7LL;->CENTER_INSIDE:LX/7LL;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/InlineVideoView;->setScaleType(LX/7LL;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static setupOverlayImage(Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 4

    .prologue
    .line 1881498
    const v0, 0x7f0d08f1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1881499
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->o:Landroid/net/Uri;

    if-nez v0, :cond_0

    .line 1881500
    iget-object v0, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1881501
    iget-object v0, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1881502
    :goto_0
    return-void

    .line 1881503
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1881504
    iget-object v1, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v0, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->e:LX/1Ad;

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->o:Landroid/net/Uri;

    invoke-static {v2}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v2

    .line 1881505
    iget-object v3, p1, Lcom/facebook/ui/media/attachments/MediaResource;->f:LX/5zi;

    sget-object p0, LX/5zi;->CAMERA_CORE:LX/5zi;

    if-ne v3, p0, :cond_1

    iget v3, p1, Lcom/facebook/ui/media/attachments/MediaResource;->l:I

    iget p0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->k:I

    if-le v3, p0, :cond_1

    .line 1881506
    const/16 v3, 0x5a

    .line 1881507
    :goto_1
    iget-boolean p0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->n:Z

    if-eqz p0, :cond_3

    if-lez v3, :cond_3

    .line 1881508
    rsub-int v3, v3, 0x168

    move p0, v3

    .line 1881509
    :goto_2
    if-eqz p0, :cond_2

    .line 1881510
    new-instance v3, LX/5zr;

    invoke-direct {v3, p0}, LX/5zr;-><init>(I)V

    .line 1881511
    :goto_3
    move-object v3, v3

    .line 1881512
    iput-object v3, v2, LX/1bX;->j:LX/33B;

    .line 1881513
    move-object v2, v2

    .line 1881514
    invoke-virtual {v2}, LX/1bX;->n()LX/1bf;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    const-class v2, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    goto :goto_0

    .line 1881515
    :cond_1
    iget-object v3, p1, Lcom/facebook/ui/media/attachments/MediaResource;->m:LX/47d;

    invoke-static {v3}, LX/47e;->a(LX/47d;)I

    move-result v3

    goto :goto_1

    .line 1881516
    :cond_2
    const/4 v3, 0x0

    goto :goto_3

    :cond_3
    move p0, v3

    goto :goto_2
.end method


# virtual methods
.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;I)V
    .locals 3

    .prologue
    .line 1881478
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    iput-object v0, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->l:LX/2MK;

    .line 1881479
    iget-object v0, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->l:LX/2MK;

    sget-object v1, LX/2MK;->AUDIO:LX/2MK;

    if-ne v0, v1, :cond_2

    .line 1881480
    invoke-virtual {p0, p2}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1881481
    const v0, 0x7f0d214b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->p:Landroid/widget/TextView;

    .line 1881482
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->p:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1881483
    iget-object v0, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->c:LX/1Mv;

    if-eqz v0, :cond_1

    .line 1881484
    iget-object v0, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->c:LX/1Mv;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/1Mv;->a(Z)V

    .line 1881485
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->c:LX/1Mv;

    .line 1881486
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->d:LX/0TD;

    new-instance v1, LX/CNF;

    invoke-direct {v1, p0, p1}, LX/CNF;-><init>(Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;Lcom/facebook/ui/media/attachments/MediaResource;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1881487
    new-instance v1, LX/CNG;

    invoke-direct {v1, p0}, LX/CNG;-><init>(Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;)V

    .line 1881488
    invoke-static {v0, v1}, LX/1Mv;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)LX/1Mv;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->c:LX/1Mv;

    .line 1881489
    iget-object v2, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->g:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1881490
    return-void

    .line 1881491
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->l:LX/2MK;

    sget-object v1, LX/2MK;->VIDEO:LX/2MK;

    if-ne v0, v1, :cond_0

    .line 1881492
    const v0, 0x7f030d8e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1881493
    const v0, 0x7f0d1b2a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->p:Landroid/widget/TextView;

    goto :goto_0
.end method

.method public setErrorListener(LX/CNI;)V
    .locals 0

    .prologue
    .line 1881496
    iput-object p1, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->a:LX/CNI;

    .line 1881497
    return-void
.end method

.method public setMediaResourceListener(LX/CNJ;)V
    .locals 0

    .prologue
    .line 1881494
    iput-object p1, p0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->b:LX/CNJ;

    .line 1881495
    return-void
.end method
