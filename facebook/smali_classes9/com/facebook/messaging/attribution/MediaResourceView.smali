.class public Lcom/facebook/messaging/attribution/MediaResourceView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation runtime Lcom/facebook/messaging/attachments/IsInlineVideoPlayerSupported;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Lcom/facebook/ui/media/attachments/MediaResource;

.field private e:Lcom/facebook/widget/listview/EmptyListViewItem;

.field private f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1875369
    const-class v0, Lcom/facebook/messaging/attribution/MediaResourceView;

    const-string v1, "media_resource_view"

    const-string v2, "media_resource_view"

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/attribution/MediaResourceView;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1875365
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1875366
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->h:I

    .line 1875367
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/messaging/attribution/MediaResourceView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1875368
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1875361
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1875362
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->h:I

    .line 1875363
    invoke-direct {p0, p1, p2}, Lcom/facebook/messaging/attribution/MediaResourceView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1875364
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1875357
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1875358
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->h:I

    .line 1875359
    invoke-direct {p0, p1, p2}, Lcom/facebook/messaging/attribution/MediaResourceView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1875360
    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    const/16 v3, 0x3c0

    const/4 v4, 0x0

    .line 1875335
    iget-object v0, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->e:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->setVisibility(I)V

    .line 1875336
    iget-object v0, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->d:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    .line 1875337
    invoke-direct {p0}, Lcom/facebook/messaging/attribution/MediaResourceView;->getPreviewType()LX/CJJ;

    move-result-object v1

    .line 1875338
    sget-object v2, LX/CJJ;->PHOTO:LX/CJJ;

    if-ne v1, v2, :cond_1

    .line 1875339
    iget-object v0, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-nez v0, :cond_0

    .line 1875340
    const v0, 0x7f0d1b34

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1875341
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1875342
    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->d:Lcom/facebook/ui/media/attachments/MediaResource;

    iget v1, v1, Lcom/facebook/ui/media/attachments/MediaResource;->k:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->d:Lcom/facebook/ui/media/attachments/MediaResource;

    iget v2, v2, Lcom/facebook/ui/media/attachments/MediaResource;->l:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 1875343
    iget-object v0, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->a:LX/1Ad;

    sget-object v1, Lcom/facebook/messaging/attribution/MediaResourceView;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-direct {p0}, Lcom/facebook/messaging/attribution/MediaResourceView;->getUriForPhotoPreview()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v1

    new-instance v2, LX/1o9;

    invoke-direct {v2, v3, v3}, LX/1o9;-><init>(II)V

    .line 1875344
    iput-object v2, v1, LX/1bX;->c:LX/1o9;

    .line 1875345
    move-object v1, v1

    .line 1875346
    invoke-virtual {v1}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    new-instance v1, LX/CJH;

    invoke-direct {v1, p0}, LX/CJH;-><init>(Lcom/facebook/messaging/attribution/MediaResourceView;)V

    invoke-virtual {v0, v1}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1875347
    iget-object v1, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1875348
    :goto_1
    return-void

    .line 1875349
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_0

    .line 1875350
    :cond_1
    sget-object v2, LX/CJJ;->AUDIO_OR_VIDEO:LX/CJJ;

    if-ne v1, v2, :cond_3

    .line 1875351
    iget-object v0, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->g:Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;

    if-nez v0, :cond_2

    .line 1875352
    const v0, 0x7f0d1b35

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1875353
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;

    iput-object v0, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->g:Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;

    .line 1875354
    :goto_2
    iget-object v0, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->g:Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;

    iget-object v1, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->d:Lcom/facebook/ui/media/attachments/MediaResource;

    const v2, 0x7f030d8a

    invoke-virtual {v0, v1, v2}, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->a(Lcom/facebook/ui/media/attachments/MediaResource;I)V

    goto :goto_1

    .line 1875355
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->g:Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;

    invoke-virtual {v0, v4}, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->setVisibility(I)V

    goto :goto_2

    .line 1875356
    :cond_3
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Unrecognized MediaResource.Type: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 1875301
    const-class v0, Lcom/facebook/messaging/attribution/MediaResourceView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/attribution/MediaResourceView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1875302
    const v0, 0x7f030aa3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1875303
    const v0, 0x7f0d05b0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/EmptyListViewItem;

    iput-object v0, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->e:Lcom/facebook/widget/listview/EmptyListViewItem;

    .line 1875304
    iget-object v0, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->e:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 1875305
    if-eqz p2, :cond_0

    .line 1875306
    sget-object v0, LX/03r;->MediaResourceView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1875307
    const/16 v1, 0x0

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->h:I

    .line 1875308
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1875309
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/messaging/attribution/MediaResourceView;LX/1Ad;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/attribution/MediaResourceView;",
            "LX/1Ad;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1875334
    iput-object p1, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->a:LX/1Ad;

    iput-object p2, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->b:LX/0Or;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/attribution/MediaResourceView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/messaging/attribution/MediaResourceView;

    invoke-static {v1}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v0

    check-cast v0, LX/1Ad;

    const/16 v2, 0x14f7

    invoke-static {v1, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/facebook/messaging/attribution/MediaResourceView;->a(Lcom/facebook/messaging/attribution/MediaResourceView;LX/1Ad;LX/0Or;)V

    return-void
.end method

.method private getPreviewType()LX/CJJ;
    .locals 5

    .prologue
    .line 1875327
    iget-object v0, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->d:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    .line 1875328
    sget-object v1, LX/CJI;->a:[I

    invoke-virtual {v0}, LX/2MK;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1875329
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Unrecognized MediaResource.Type: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1875330
    :pswitch_0
    sget-object v0, LX/CJJ;->PHOTO:LX/CJJ;

    .line 1875331
    :goto_0
    return-object v0

    .line 1875332
    :pswitch_1
    sget-object v0, LX/CJJ;->AUDIO_OR_VIDEO:LX/CJJ;

    goto :goto_0

    .line 1875333
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/CJJ;->AUDIO_OR_VIDEO:LX/CJJ;

    goto :goto_0

    :cond_0
    sget-object v0, LX/CJJ;->PHOTO:LX/CJJ;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getUriForPhotoPreview()Landroid/net/Uri;
    .locals 5

    .prologue
    .line 1875322
    iget-object v0, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->d:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    .line 1875323
    sget-object v1, LX/CJI;->a:[I

    invoke-virtual {v0}, LX/2MK;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1875324
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "MediaResource type does not support thumbnail: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1875325
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->d:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    .line 1875326
    :goto_0
    return-object v0

    :pswitch_2
    iget-object v0, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->d:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->g:Landroid/net/Uri;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 1875318
    iget v0, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->h:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1875319
    iget v0, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->h:I

    const/high16 v1, -0x80000000

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 1875320
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 1875321
    return-void
.end method

.method public setMediaResource(Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 1875310
    iput-object p1, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->d:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 1875311
    iget-object v0, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->d:Lcom/facebook/ui/media/attachments/MediaResource;

    if-eqz v0, :cond_1

    .line 1875312
    invoke-direct {p0}, Lcom/facebook/messaging/attribution/MediaResourceView;->a()V

    .line 1875313
    :cond_0
    :goto_0
    return-void

    .line 1875314
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v0, :cond_2

    .line 1875315
    iget-object v0, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1875316
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->g:Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;

    if-eqz v0, :cond_0

    .line 1875317
    iget-object v0, p0, Lcom/facebook/messaging/attribution/MediaResourceView;->g:Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->setVisibility(I)V

    goto :goto_0
.end method
