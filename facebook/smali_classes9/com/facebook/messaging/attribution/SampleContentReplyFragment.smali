.class public Lcom/facebook/messaging/attribution/SampleContentReplyFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:LX/CJL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private n:LX/7Tc;

.field public o:LX/3if;

.field private p:LX/CJG;

.field public q:Lcom/facebook/ui/media/attachments/MediaResource;

.field public r:LX/CJX;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1875515
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 1875516
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/messaging/attribution/SampleContentReplyFragment;

    invoke-static {p0}, LX/CJL;->a(LX/0QB;)LX/CJL;

    move-result-object p0

    check-cast p0, LX/CJL;

    iput-object p0, p1, Lcom/facebook/messaging/attribution/SampleContentReplyFragment;->m:LX/CJL;

    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x2f94441e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1875491
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1875492
    const-class v0, Lcom/facebook/messaging/attribution/SampleContentReplyFragment;

    invoke-static {v0, p0}, Lcom/facebook/messaging/attribution/SampleContentReplyFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1875493
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1875494
    const-string v2, "media_resource"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    iput-object v0, p0, Lcom/facebook/messaging/attribution/SampleContentReplyFragment;->q:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 1875495
    const/4 v0, 0x0

    const v2, 0x7f0e0569

    invoke-virtual {p0, v0, v2}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 1875496
    const/16 v0, 0x2b

    const v2, 0xb9a65b8

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/high16 v3, 0x3f800000    # 1.0f

    const/16 v0, 0x2a

    const v1, -0x4683368

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1875503
    new-instance v1, LX/CJG;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/CJG;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/messaging/attribution/SampleContentReplyFragment;->p:LX/CJG;

    .line 1875504
    new-instance v1, LX/3if;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/3if;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/messaging/attribution/SampleContentReplyFragment;->o:LX/3if;

    .line 1875505
    iget-object v1, p0, Lcom/facebook/messaging/attribution/SampleContentReplyFragment;->o:LX/3if;

    .line 1875506
    iput v3, v1, LX/3if;->e:F

    .line 1875507
    iget-object v1, p0, Lcom/facebook/messaging/attribution/SampleContentReplyFragment;->o:LX/3if;

    .line 1875508
    iput v3, v1, LX/3if;->d:F

    .line 1875509
    iget-object v1, p0, Lcom/facebook/messaging/attribution/SampleContentReplyFragment;->o:LX/3if;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, LX/3if;->setRecyclerViewBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1875510
    new-instance v1, LX/7Tc;

    iget-object v2, p0, Lcom/facebook/messaging/attribution/SampleContentReplyFragment;->p:LX/CJG;

    invoke-direct {v1, v2}, LX/7Tc;-><init>(Landroid/view/View;)V

    iput-object v1, p0, Lcom/facebook/messaging/attribution/SampleContentReplyFragment;->n:LX/7Tc;

    .line 1875511
    iget-object v1, p0, Lcom/facebook/messaging/attribution/SampleContentReplyFragment;->o:LX/3if;

    iget-object v2, p0, Lcom/facebook/messaging/attribution/SampleContentReplyFragment;->n:LX/7Tc;

    invoke-virtual {v1, v2}, LX/3if;->setAdapter(LX/1OM;)V

    .line 1875512
    iget-object v1, p0, Lcom/facebook/messaging/attribution/SampleContentReplyFragment;->o:LX/3if;

    new-instance v2, LX/CJV;

    invoke-direct {v2, p0}, LX/CJV;-><init>(Lcom/facebook/messaging/attribution/SampleContentReplyFragment;)V

    .line 1875513
    iput-object v2, v1, LX/3if;->p:LX/3iq;

    .line 1875514
    iget-object v1, p0, Lcom/facebook/messaging/attribution/SampleContentReplyFragment;->o:LX/3if;

    const/16 v2, 0x2b

    const v3, -0x67da0ce

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1875497
    invoke-super {p0, p1, p2}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1875498
    iget-object v0, p0, Lcom/facebook/messaging/attribution/SampleContentReplyFragment;->p:LX/CJG;

    new-instance v1, LX/CJW;

    invoke-direct {v1, p0}, LX/CJW;-><init>(Lcom/facebook/messaging/attribution/SampleContentReplyFragment;)V

    .line 1875499
    iput-object v1, v0, LX/CJG;->g:LX/CJ7;

    .line 1875500
    iget-object v0, p0, Lcom/facebook/messaging/attribution/SampleContentReplyFragment;->p:LX/CJG;

    iget-object v1, p0, Lcom/facebook/messaging/attribution/SampleContentReplyFragment;->q:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v0, v1}, LX/CJG;->setMediaResource(Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 1875501
    iget-object v0, p0, Lcom/facebook/messaging/attribution/SampleContentReplyFragment;->p:LX/CJG;

    invoke-virtual {v0}, LX/CJG;->a()V

    .line 1875502
    return-void
.end method
