.class public Lcom/facebook/messaging/attribution/InlineReplyFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# static fields
.field public static final w:[Ljava/lang/String;


# instance fields
.field public A:Lcom/facebook/ui/media/attachments/MediaResource;

.field public B:Landroid/content/Intent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private E:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private F:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private G:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Z

.field private J:J

.field private K:Z

.field public L:Z

.field public M:LX/CJP;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public N:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/3N1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/CJ5;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/CNE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/CJL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/1Ml;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0Or;
    .annotation runtime Lcom/facebook/messaging/attachments/IsInlineVideoPlayerSupported;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private x:LX/7Tc;

.field public y:LX/3if;

.field public z:LX/CJG;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1875240
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.READ_EXTERNAL_STORAGE"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->w:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1875241
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 1875242
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v11

    move-object v1, p1

    check-cast v1, Lcom/facebook/messaging/attribution/InlineReplyFragment;

    invoke-static {v11}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-static {v11}, LX/3N1;->b(LX/0QB;)LX/3N1;

    move-result-object v3

    check-cast v3, LX/3N1;

    invoke-static {v11}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v11}, LX/CJ5;->a(LX/0QB;)LX/CJ5;

    move-result-object v5

    check-cast v5, LX/CJ5;

    invoke-static {v11}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, LX/0TD;

    invoke-static {v11}, LX/CNE;->b(LX/0QB;)LX/CNE;

    move-result-object v7

    check-cast v7, LX/CNE;

    invoke-static {v11}, LX/CJL;->a(LX/0QB;)LX/CJL;

    move-result-object v8

    check-cast v8, LX/CJL;

    invoke-static {v11}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v9

    check-cast v9, LX/1Ml;

    invoke-static {v11}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v10

    check-cast v10, Lcom/facebook/content/SecureContextHelper;

    const/16 p0, 0x14f7

    invoke-static {v11, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    iput-object v2, v1, Lcom/facebook/messaging/attribution/InlineReplyFragment;->m:LX/0Sh;

    iput-object v3, v1, Lcom/facebook/messaging/attribution/InlineReplyFragment;->n:LX/3N1;

    iput-object v4, v1, Lcom/facebook/messaging/attribution/InlineReplyFragment;->o:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v5, v1, Lcom/facebook/messaging/attribution/InlineReplyFragment;->p:LX/CJ5;

    iput-object v6, v1, Lcom/facebook/messaging/attribution/InlineReplyFragment;->q:LX/0TD;

    iput-object v7, v1, Lcom/facebook/messaging/attribution/InlineReplyFragment;->r:LX/CNE;

    iput-object v8, v1, Lcom/facebook/messaging/attribution/InlineReplyFragment;->s:LX/CJL;

    iput-object v9, v1, Lcom/facebook/messaging/attribution/InlineReplyFragment;->t:LX/1Ml;

    iput-object v10, v1, Lcom/facebook/messaging/attribution/InlineReplyFragment;->u:Lcom/facebook/content/SecureContextHelper;

    iput-object v11, v1, Lcom/facebook/messaging/attribution/InlineReplyFragment;->v:LX/0Or;

    return-void
.end method

.method public static l(Lcom/facebook/messaging/attribution/InlineReplyFragment;)V
    .locals 4

    .prologue
    .line 1875213
    iget-object v0, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->B:Landroid/content/Intent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->B:Landroid/content/Intent;

    const-string v1, "IS_URI_COPIED"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1875214
    iget-object v0, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->A:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1875215
    :goto_0
    new-instance v1, LX/CJB;

    invoke-direct {v1, p0}, LX/CJB;-><init>(Lcom/facebook/messaging/attribution/InlineReplyFragment;)V

    move-object v1, v1

    .line 1875216
    iget-object v2, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->q:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1875217
    :goto_1
    return-void

    .line 1875218
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->t:LX/1Ml;

    sget-object v1, Lcom/facebook/messaging/attribution/InlineReplyFragment;->w:[Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/1Ml;->a([Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1875219
    iget-object v0, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->r:LX/CNE;

    iget-object v1, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->A:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 1875220
    iget-object v2, v0, LX/CNE;->c:LX/0TD;

    new-instance v3, LX/CNB;

    invoke-direct {v3, v0, v1}, LX/CNB;-><init>(LX/CNE;Ljava/util/List;)V

    invoke-interface {v2, v3}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 1875221
    goto :goto_0

    .line 1875222
    :cond_1
    const/4 v3, 0x1

    .line 1875223
    new-instance v0, LX/2rN;

    invoke-direct {v0}, LX/2rN;-><init>()V

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08017f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1875224
    iput-object v1, v0, LX/2rN;->a:Ljava/lang/String;

    .line 1875225
    move-object v0, v0

    .line 1875226
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080180

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1875227
    iput-object v1, v0, LX/2rN;->b:Ljava/lang/String;

    .line 1875228
    move-object v0, v0

    .line 1875229
    sget-object v1, LX/0jt;->ALWAYS_SHOW:LX/0jt;

    .line 1875230
    iput-object v1, v0, LX/2rN;->c:LX/0jt;

    .line 1875231
    move-object v0, v0

    .line 1875232
    const/4 v1, 0x0

    .line 1875233
    iput-boolean v1, v0, LX/2rN;->d:Z

    .line 1875234
    move-object v0, v0

    .line 1875235
    invoke-virtual {v0}, LX/2rN;->e()Lcom/facebook/runtimepermissions/RequestPermissionsConfig;

    move-result-object v0

    .line 1875236
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/facebook/messaging/attribution/InlineReplyFragment;->w:[Ljava/lang/String;

    invoke-static {v1, v2, v0}, Lcom/facebook/runtimepermissions/RequestPermissionsActivity;->a(Landroid/content/Context;[Ljava/lang/String;Lcom/facebook/runtimepermissions/RequestPermissionsConfig;)Landroid/content/Intent;

    move-result-object v0

    .line 1875237
    iget-object v1, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->u:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, v3, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1875238
    iput-boolean v3, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->L:Z

    .line 1875239
    goto :goto_1
.end method


# virtual methods
.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    const/high16 v4, 0x1000000

    const/4 v0, 0x1

    const/4 v1, 0x2

    const/16 v2, 0x2a

    const v3, -0x2b90b5ec

    invoke-static {v1, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1875191
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1875192
    iget-object v2, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->B:Landroid/content/Intent;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->B:Landroid/content/Intent;

    const-string v3, "IS_CHAT_HEADS_HARDWARE_ACCELERATION_DISABLED"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1875193
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 1875194
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v0

    .line 1875195
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 1875196
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->I:Z

    if-eqz v0, :cond_2

    .line 1875197
    iget-object v5, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->B:Landroid/content/Intent;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->D:Ljava/lang/String;

    if-nez v5, :cond_4

    .line 1875198
    :cond_2
    :goto_1
    invoke-static {p0}, Lcom/facebook/messaging/attribution/InlineReplyFragment;->l(Lcom/facebook/messaging/attribution/InlineReplyFragment;)V

    .line 1875199
    const v0, 0x2f4e1dfd

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 1875200
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 1875201
    :cond_4
    iget-object v5, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->n:LX/3N1;

    iget-object v6, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->B:Landroid/content/Intent;

    iget-object v7, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->D:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, LX/3N1;->a(Landroid/content/Intent;Ljava/lang/String;)Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    move-result-object v5

    .line 1875202
    if-eqz v5, :cond_2

    .line 1875203
    iget-object v6, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->p:LX/CJ5;

    .line 1875204
    new-instance v8, LX/5Tq;

    invoke-direct {v8}, LX/5Tq;-><init>()V

    move-object v8, v8

    .line 1875205
    const-string v9, "app_fbid"

    iget-object v10, v5, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->b:Ljava/lang/String;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1875206
    const-string v9, "verification_type"

    const-string v10, "OTHER"

    invoke-virtual {v10}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1875207
    const-string v9, "hash_key"

    iget-object v10, v5, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->d:Ljava/lang/String;

    invoke-virtual {v8, v9, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1875208
    invoke-static {v8}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v8

    sget-object v9, LX/0zS;->a:LX/0zS;

    invoke-virtual {v8, v9}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v8

    .line 1875209
    const-wide/32 v10, 0x15180

    invoke-virtual {v8, v10, v11}, LX/0zO;->a(J)LX/0zO;

    move-result-object v8

    .line 1875210
    iget-object v9, v6, LX/CJ5;->a:LX/0tX;

    invoke-virtual {v9, v8}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v8

    .line 1875211
    new-instance v9, LX/CJ4;

    invoke-direct {v9, v6, v5}, LX/CJ4;-><init>(LX/CJ5;Lcom/facebook/messaging/model/attribution/ContentAppAttribution;)V

    invoke-static {v8, v9}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    move-object v5, v8

    .line 1875212
    new-instance v6, LX/CJ9;

    invoke-direct {v6, p0}, LX/CJ9;-><init>(Lcom/facebook/messaging/attribution/InlineReplyFragment;)V

    iget-object v7, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->q:LX/0TD;

    invoke-static {v5, v6, v7}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_1
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    .line 1875243
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 1875244
    const/4 p2, 0x2

    const/4 p1, 0x0

    const/4 v4, 0x1

    .line 1875245
    iput-boolean p1, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->L:Z

    .line 1875246
    const-string v0, "extra_permission_results"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 1875247
    const-string v1, "android.permission.READ_EXTERNAL_STORAGE"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 1875248
    const-string v2, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1875249
    if-eqz v1, :cond_0

    if-nez v0, :cond_2

    .line 1875250
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 1875251
    :goto_0
    return-void

    .line 1875252
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    .line 1875253
    :cond_2
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-nez v2, :cond_3

    .line 1875254
    invoke-static {p0}, Lcom/facebook/messaging/attribution/InlineReplyFragment;->l(Lcom/facebook/messaging/attribution/InlineReplyFragment;)V

    goto :goto_0

    .line 1875255
    :cond_3
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, v4, :cond_4

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v4, :cond_5

    .line 1875256
    :cond_4
    iget-object v2, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->o:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/Cuy;->e:LX/0Tn;

    invoke-interface {v2, v3, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 1875257
    :cond_5
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, p2, :cond_6

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p2, :cond_7

    .line 1875258
    :cond_6
    iget-object v0, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->o:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/Cuy;->e:LX/0Tn;

    invoke-interface {v0, v1, v4}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1875259
    :cond_7
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->d()V

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/16 v0, 0x2a

    const v1, 0x2c5783ed

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1875176
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1875177
    const-class v0, Lcom/facebook/messaging/attribution/InlineReplyFragment;

    invoke-static {v0, p0}, Lcom/facebook/messaging/attribution/InlineReplyFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1875178
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v0

    .line 1875179
    const-string v0, "media_resource"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    iput-object v0, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->A:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 1875180
    const-string v0, "app_id"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->C:Ljava/lang/String;

    .line 1875181
    const-string v0, "app_package"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->D:Ljava/lang/String;

    .line 1875182
    const-string v0, "title"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->E:Ljava/lang/String;

    .line 1875183
    const-string v0, "description"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->F:Ljava/lang/String;

    .line 1875184
    const-string v0, "cancel_label"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->G:Ljava/lang/String;

    .line 1875185
    const-string v0, "reply_intent"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->B:Landroid/content/Intent;

    .line 1875186
    const-string v0, "thread_key"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->H:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1875187
    const-string v0, "is_platform_instance"

    invoke-virtual {v2, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->I:Z

    .line 1875188
    const-string v0, "dialog_id"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->J:J

    .line 1875189
    const v0, 0x7f0e0569

    invoke-virtual {p0, v4, v0}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 1875190
    const/16 v0, 0x2b

    const v2, 0x24ad9bb5

    invoke-static {v5, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/high16 v3, 0x3f800000    # 1.0f

    const/16 v0, 0x2a

    const v1, -0x10a64d8

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1875164
    new-instance v1, LX/CJG;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/CJG;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->z:LX/CJG;

    .line 1875165
    new-instance v1, LX/3if;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/3if;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->y:LX/3if;

    .line 1875166
    iget-object v1, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->y:LX/3if;

    .line 1875167
    iput v3, v1, LX/3if;->e:F

    .line 1875168
    iget-object v1, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->y:LX/3if;

    .line 1875169
    iput v3, v1, LX/3if;->d:F

    .line 1875170
    iget-object v1, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->y:LX/3if;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, LX/3if;->setRecyclerViewBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1875171
    new-instance v1, LX/7Tc;

    iget-object v2, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->z:LX/CJG;

    invoke-direct {v1, v2}, LX/7Tc;-><init>(Landroid/view/View;)V

    iput-object v1, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->x:LX/7Tc;

    .line 1875172
    iget-object v1, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->y:LX/3if;

    iget-object v2, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->x:LX/7Tc;

    invoke-virtual {v1, v2}, LX/3if;->setAdapter(LX/1OM;)V

    .line 1875173
    iget-object v1, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->y:LX/3if;

    new-instance v2, LX/CJ6;

    invoke-direct {v2, p0}, LX/CJ6;-><init>(Lcom/facebook/messaging/attribution/InlineReplyFragment;)V

    .line 1875174
    iput-object v2, v1, LX/3if;->p:LX/3iq;

    .line 1875175
    iget-object v1, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->y:LX/3if;

    const/16 v2, 0x2b

    const v3, 0x74244405

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5b9327df

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1875161
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onPause()V

    .line 1875162
    iget-boolean v1, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->L:Z

    iput-boolean v1, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->K:Z

    .line 1875163
    const/16 v1, 0x2b

    const v2, -0xd6ed80f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x6df9f6a

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1875153
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onResume()V

    .line 1875154
    iget-boolean v1, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->K:Z

    if-eqz v1, :cond_0

    .line 1875155
    iget-object v1, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->t:LX/1Ml;

    sget-object v2, Lcom/facebook/messaging/attribution/InlineReplyFragment;->w:[Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/1Ml;->a([Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1875156
    invoke-static {p0}, Lcom/facebook/messaging/attribution/InlineReplyFragment;->l(Lcom/facebook/messaging/attribution/InlineReplyFragment;)V

    .line 1875157
    :goto_0
    iput-boolean v3, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->L:Z

    .line 1875158
    iput-boolean v3, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->K:Z

    .line 1875159
    :cond_0
    const v1, -0x2e8de640

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void

    .line 1875160
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->d()V

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1875125
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1875126
    const-string v0, "media_resource"

    iget-object v1, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->A:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1875127
    const-string v0, "app_id"

    iget-object v1, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->C:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1875128
    const-string v0, "app_package"

    iget-object v1, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->D:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1875129
    const-string v0, "title"

    iget-object v1, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->E:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1875130
    const-string v0, "description"

    iget-object v1, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->F:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1875131
    const-string v0, "cancel_label"

    iget-object v1, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->G:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1875132
    const-string v0, "reply_intent"

    iget-object v1, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->B:Landroid/content/Intent;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1875133
    const-string v0, "thread_key"

    iget-object v1, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->H:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1875134
    const-string v0, "is_platform_instance"

    iget-boolean v1, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->I:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1875135
    const-string v0, "dialog_id"

    iget-wide v2, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->J:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1875136
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1875137
    invoke-super {p0, p1, p2}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1875138
    iget-object v0, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->z:LX/CJG;

    new-instance v1, LX/CJ8;

    invoke-direct {v1, p0}, LX/CJ8;-><init>(Lcom/facebook/messaging/attribution/InlineReplyFragment;)V

    .line 1875139
    iput-object v1, v0, LX/CJG;->g:LX/CJ7;

    .line 1875140
    iget-object v0, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->z:LX/CJG;

    iget-object v1, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->E:Ljava/lang/String;

    .line 1875141
    iget-object p1, v0, LX/CJG;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p1, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1875142
    iget-object p2, v0, LX/CJG;->e:Lcom/facebook/resources/ui/FbTextView;

    if-nez v1, :cond_0

    const/16 p1, 0x8

    :goto_0
    invoke-virtual {p2, p1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1875143
    iget-object v0, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->z:LX/CJG;

    iget-object v1, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->F:Ljava/lang/String;

    .line 1875144
    iget-object p1, v0, LX/CJG;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p1, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1875145
    iget-object p2, v0, LX/CJG;->f:Lcom/facebook/resources/ui/FbTextView;

    if-nez v1, :cond_1

    const/16 p1, 0x8

    :goto_1
    invoke-virtual {p2, p1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1875146
    iget-object v0, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->z:LX/CJG;

    iget-object v1, p0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->G:Ljava/lang/String;

    .line 1875147
    if-nez v1, :cond_2

    .line 1875148
    iget-object p0, v0, LX/CJG;->c:Lcom/facebook/resources/ui/FbTextView;

    const p1, 0x7f080017

    invoke-virtual {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1875149
    :goto_2
    return-void

    .line 1875150
    :cond_0
    const/4 p1, 0x0

    goto :goto_0

    .line 1875151
    :cond_1
    const/4 p1, 0x0

    goto :goto_1

    .line 1875152
    :cond_2
    iget-object p0, v0, LX/CJG;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method
