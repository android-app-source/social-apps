.class public Lcom/facebook/messaging/attribution/AttributionReportFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/48V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:Lcom/facebook/widget/listview/EmptyListViewItem;

.field public p:Lcom/facebook/webview/FacebookWebView;

.field private q:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1875028
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/messaging/attribution/AttributionReportFragment;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {p0}, LX/48V;->b(LX/0QB;)LX/48V;

    move-result-object p0

    check-cast p0, LX/48V;

    iput-object v1, p1, Lcom/facebook/messaging/attribution/AttributionReportFragment;->m:LX/03V;

    iput-object p0, p1, Lcom/facebook/messaging/attribution/AttributionReportFragment;->n:LX/48V;

    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x4bc25b5e    # 2.5474748E7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1875022
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1875023
    const-class v1, Lcom/facebook/messaging/attribution/AttributionReportFragment;

    invoke-static {v1, p0}, Lcom/facebook/messaging/attribution/AttributionReportFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1875024
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 1875025
    const-string v2, "attachment_fbid"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/messaging/attribution/AttributionReportFragment;->q:Ljava/lang/String;

    .line 1875026
    const/4 v1, 0x0

    const v2, 0x7f0e056a

    invoke-virtual {p0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 1875027
    const/16 v1, 0x2b

    const v2, 0x1262b9c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x33113fb0    # -1.251744E8f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1875021
    const v1, 0x7f030130

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x27ade6c3

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1875012
    invoke-super {p0, p1, p2}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1875013
    const v0, 0x7f0d05e3

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/webview/FacebookWebView;

    iput-object v0, p0, Lcom/facebook/messaging/attribution/AttributionReportFragment;->p:Lcom/facebook/webview/FacebookWebView;

    .line 1875014
    const v0, 0x7f0d05e4

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/EmptyListViewItem;

    iput-object v0, p0, Lcom/facebook/messaging/attribution/AttributionReportFragment;->o:Lcom/facebook/widget/listview/EmptyListViewItem;

    .line 1875015
    iget-object v0, p0, Lcom/facebook/messaging/attribution/AttributionReportFragment;->o:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 1875016
    iget-object v0, p0, Lcom/facebook/messaging/attribution/AttributionReportFragment;->o:Lcom/facebook/widget/listview/EmptyListViewItem;

    const v1, 0x7f080024

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->setMessage(I)V

    .line 1875017
    const-string v0, "https://m.facebook.com/trust/afro/?reportable_ent_token=%s&initial_action=%s&story_location=%s"

    iget-object v1, p0, Lcom/facebook/messaging/attribution/AttributionReportFragment;->q:Ljava/lang/String;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->RESOLVE_PROBLEM:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    const-string v3, "messenger"

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1875018
    iget-object v1, p0, Lcom/facebook/messaging/attribution/AttributionReportFragment;->p:Lcom/facebook/webview/FacebookWebView;

    new-instance v2, LX/CJ0;

    invoke-direct {v2, p0}, LX/CJ0;-><init>(Lcom/facebook/messaging/attribution/AttributionReportFragment;)V

    invoke-virtual {v1, v2}, Lcom/facebook/webview/FacebookWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 1875019
    iget-object v1, p0, Lcom/facebook/messaging/attribution/AttributionReportFragment;->n:LX/48V;

    iget-object v2, p0, Lcom/facebook/messaging/attribution/AttributionReportFragment;->p:Lcom/facebook/webview/FacebookWebView;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/48V;->a(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 1875020
    return-void
.end method
