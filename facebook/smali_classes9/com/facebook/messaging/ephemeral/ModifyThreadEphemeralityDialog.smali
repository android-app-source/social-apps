.class public Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# static fields
.field private static final q:[I

.field private static final r:[I


# instance fields
.field public m:LX/CMf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/CMh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/CMZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/CMe;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:I

.field public t:[I

.field public u:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1880853
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;->q:[I

    .line 1880854
    const/4 v0, 0x1

    new-array v0, v0, [I

    aput v1, v0, v1

    sput-object v0, Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;->r:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0xea60
        0xdbba0
        0x36ee80
        0xdbba00
        0x5265c00
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1880855
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 1880856
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;->s:I

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v4

    check-cast p1, Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;

    new-instance v2, LX/CMf;

    invoke-static {v4}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-direct {v2, v1}, LX/CMf;-><init>(LX/0Zb;)V

    move-object v1, v2

    check-cast v1, LX/CMf;

    invoke-static {v4}, LX/CMh;->b(LX/0QB;)LX/CMh;

    move-result-object v2

    check-cast v2, LX/CMh;

    new-instance v0, LX/CMZ;

    invoke-static {v4}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {v0, v3}, LX/CMZ;-><init>(Landroid/content/res/Resources;)V

    move-object v3, v0

    check-cast v3, LX/CMZ;

    new-instance v0, LX/CMe;

    invoke-static {v4}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v5

    check-cast v5, LX/0aG;

    const-class v6, Landroid/content/Context;

    invoke-interface {v4, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-static {v4}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v7

    check-cast v7, Landroid/content/res/Resources;

    invoke-static {v4}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object p0

    check-cast p0, LX/1Ck;

    invoke-direct {v0, v5, v6, v7, p0}, LX/CMe;-><init>(LX/0aG;Landroid/content/Context;Landroid/content/res/Resources;LX/1Ck;)V

    move-object v4, v0

    check-cast v4, LX/CMe;

    iput-object v1, p1, Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;->m:LX/CMf;

    iput-object v2, p1, Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;->n:LX/CMh;

    iput-object v3, p1, Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;->o:LX/CMZ;

    iput-object v4, p1, Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;->p:LX/CMe;

    return-void
.end method

.method private a()[Ljava/lang/String;
    .locals 12

    .prologue
    .line 1880832
    iget-object v0, p0, Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;->t:[I

    array-length v0, v0

    new-array v1, v0, [Ljava/lang/String;

    .line 1880833
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;->t:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 1880834
    iget-object v2, p0, Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;->o:LX/CMZ;

    iget-object v3, p0, Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;->t:[I

    aget v3, v3, v0

    .line 1880835
    if-gtz v3, :cond_1

    .line 1880836
    iget-object v4, v2, LX/CMZ;->a:Landroid/content/res/Resources;

    const v5, 0x7f0805b5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1880837
    :goto_1
    move-object v2, v4

    .line 1880838
    aput-object v2, v1, v0

    .line 1880839
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1880840
    :cond_0
    return-object v1

    .line 1880841
    :cond_1
    int-to-long v4, v3

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    long-to-int v6, v4

    .line 1880842
    const v4, 0x7f0f0029

    .line 1880843
    int-to-long v8, v3

    const-wide/32 v10, 0xea60

    div-long/2addr v8, v10

    long-to-int v5, v8

    .line 1880844
    if-lez v5, :cond_4

    .line 1880845
    const v4, 0x7f0f0028

    .line 1880846
    :goto_2
    int-to-long v6, v3

    const-wide/32 v8, 0x36ee80

    div-long/2addr v6, v8

    long-to-int v6, v6

    .line 1880847
    if-lez v6, :cond_2

    .line 1880848
    const v4, 0x7f0f0027

    move v5, v6

    .line 1880849
    :cond_2
    int-to-long v6, v3

    const-wide/32 v8, 0x5265c00

    div-long/2addr v6, v8

    long-to-int v6, v6

    .line 1880850
    if-lez v6, :cond_3

    .line 1880851
    const v4, 0x7f0f0026

    move v5, v6

    .line 1880852
    :cond_3
    iget-object v6, v2, LX/CMZ;->a:Landroid/content/res/Resources;

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v6, v4, v5, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_4
    move v5, v6

    goto :goto_2
.end method

.method public static a$redex0(Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;Lcom/facebook/messaging/model/threads/ThreadSummary;I)V
    .locals 10

    .prologue
    .line 1880784
    iget v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->K:I

    if-ne v0, p2, :cond_0

    .line 1880785
    :goto_0
    return-void

    .line 1880786
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;->p:LX/CMe;

    iget-object v1, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget v2, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->K:I

    .line 1880787
    const/4 v3, 0x1

    .line 1880788
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1880789
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 1880790
    new-instance v4, LX/6ih;

    invoke-direct {v4}, LX/6ih;-><init>()V

    .line 1880791
    iput-object v1, v4, LX/6ih;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1880792
    move-object v4, v4

    .line 1880793
    const/4 v5, 0x1

    .line 1880794
    iput-boolean v5, v4, LX/6ih;->n:Z

    .line 1880795
    move-object v4, v4

    .line 1880796
    iput p2, v4, LX/6ih;->o:I

    .line 1880797
    move-object v4, v4

    .line 1880798
    iput v2, v4, LX/6ih;->p:I

    .line 1880799
    move-object v4, v4

    .line 1880800
    invoke-virtual {v4}, LX/6ih;->u()Lcom/facebook/messaging/service/model/ModifyThreadParams;

    move-result-object v4

    .line 1880801
    const-string v5, "modifyThreadParams"

    invoke-virtual {v6, v5, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1880802
    iget-object v4, v0, LX/CMe;->a:LX/0aG;

    const-string v5, "modify_thread"

    sget-object v7, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v8, LX/CMe;

    invoke-static {v8}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v8

    const v9, 0x51bce327

    invoke-static/range {v4 .. v9}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v4

    .line 1880803
    if-eqz v3, :cond_1

    .line 1880804
    new-instance v5, LX/4At;

    iget-object v6, v0, LX/CMe;->b:Landroid/content/Context;

    iget-object v7, v0, LX/CMe;->c:Landroid/content/res/Resources;

    const v8, 0x7f0805b6

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, LX/4At;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-interface {v4, v5}, LX/1MF;->setOperationProgressIndicator(LX/4At;)LX/1MF;

    .line 1880805
    :cond_1
    iget-object v5, v0, LX/CMe;->d:LX/1Ck;

    invoke-interface {v4}, LX/1MF;->start()LX/1ML;

    move-result-object v4

    new-instance v6, LX/CMd;

    invoke-direct {v6, v0}, LX/CMd;-><init>(LX/CMe;)V

    invoke-static {v6}, LX/0Vd;->of(LX/0TF;)LX/0Vd;

    move-result-object v6

    invoke-virtual {v5, v1, v4, v6}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1880806
    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    .line 1880821
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1880822
    const-string v1, "thread_summary_arg"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 1880823
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 1880824
    const-string v2, "analytics_source_arg"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;->u:Ljava/lang/String;

    .line 1880825
    iget-object v1, p0, Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;->m:LX/CMf;

    iget-object v2, p0, Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;->u:Ljava/lang/String;

    .line 1880826
    iget-object v3, v1, LX/CMf;->a:LX/0Zb;

    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "ephemeral_dialog_open"

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "source"

    invoke-virtual {v4, v5, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v3, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1880827
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1880828
    new-instance v2, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/31Y;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0805b4

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v2

    invoke-direct {p0}, Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;->a()[Ljava/lang/String;

    move-result-object v3

    .line 1880829
    iget-object v4, p0, Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;->t:[I

    iget v5, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->K:I

    invoke-static {v4, v5}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v4

    .line 1880830
    if-gez v4, :cond_0

    const/4 v4, -0x1

    :cond_0
    move v4, v4

    .line 1880831
    new-instance v5, LX/CMc;

    invoke-direct {v5, p0}, LX/CMc;-><init>(Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;)V

    invoke-virtual {v2, v3, v4, v5}, LX/0ju;->a([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v2

    const v3, 0x7f080016

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/CMb;

    invoke-direct {v4, p0, v0}, LX/CMb;-><init>(Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    invoke-virtual {v2, v3, v4}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v2, 0x7f080017

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/CMa;

    invoke-direct {v2, p0}, LX/CMa;-><init>(Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x4de0d756

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1880807
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1880808
    const-class v0, Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;

    invoke-static {v0, p0}, Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;->a(Ljava/lang/Class;LX/02k;)V

    .line 1880809
    iget-object v0, p0, Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;->n:LX/CMh;

    .line 1880810
    iget-object v2, v0, LX/CMh;->a:LX/0Uh;

    const/16 v3, 0x14d

    const/4 p1, 0x0

    invoke-virtual {v2, v3, p1}, LX/0Uh;->a(IZ)Z

    move-result v2

    move v2, v2

    .line 1880811
    if-eqz v2, :cond_2

    .line 1880812
    sget-object v2, LX/CMg;->NONE:LX/CMg;

    .line 1880813
    :goto_0
    move-object v2, v2

    .line 1880814
    sget-object v3, LX/CMg;->DISABLE_ONLY:LX/CMg;

    if-ne v2, v3, :cond_1

    const/4 v2, 0x1

    :goto_1
    move v0, v2

    .line 1880815
    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;->r:[I

    :goto_2
    iput-object v0, p0, Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;->t:[I

    .line 1880816
    const v0, 0x27417d83

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 1880817
    :cond_0
    sget-object v0, Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;->q:[I

    goto :goto_2

    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 1880818
    :cond_2
    iget-object v2, v0, LX/CMh;->a:LX/0Uh;

    const/16 v3, 0x14f

    const/4 p1, 0x0

    invoke-virtual {v2, v3, p1}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1880819
    sget-object v2, LX/CMg;->ENABLE_AND_DISABLE:LX/CMg;

    goto :goto_0

    .line 1880820
    :cond_3
    sget-object v2, LX/CMg;->DISABLE_ONLY:LX/CMg;

    goto :goto_0
.end method
