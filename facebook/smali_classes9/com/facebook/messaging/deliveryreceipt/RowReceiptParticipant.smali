.class public Lcom/facebook/messaging/deliveryreceipt/RowReceiptParticipant;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/deliveryreceipt/RowReceiptParticipant;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/facebook/messaging/deliveryreceipt/RowReceiptParticipant;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Lcom/facebook/messaging/model/messages/ParticipantInfo;

.field public final c:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1879545
    new-instance v0, LX/CLo;

    invoke-direct {v0}, LX/CLo;-><init>()V

    sput-object v0, Lcom/facebook/messaging/deliveryreceipt/RowReceiptParticipant;->a:Ljava/util/Comparator;

    .line 1879546
    new-instance v0, LX/CLp;

    invoke-direct {v0}, LX/CLp;-><init>()V

    sput-object v0, Lcom/facebook/messaging/deliveryreceipt/RowReceiptParticipant;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1879536
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1879537
    const-class v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iput-object v0, p0, Lcom/facebook/messaging/deliveryreceipt/RowReceiptParticipant;->b:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 1879538
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/deliveryreceipt/RowReceiptParticipant;->c:J

    .line 1879539
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1879544
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1879543
    const-class v0, Lcom/facebook/messaging/deliveryreceipt/RowReceiptParticipant;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "participantInfo"

    iget-object v2, p0, Lcom/facebook/messaging/deliveryreceipt/RowReceiptParticipant;->b:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "readTimestampMs"

    iget-wide v2, p0, Lcom/facebook/messaging/deliveryreceipt/RowReceiptParticipant;->c:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1879540
    iget-object v0, p0, Lcom/facebook/messaging/deliveryreceipt/RowReceiptParticipant;->b:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 1879541
    iget-wide v0, p0, Lcom/facebook/messaging/deliveryreceipt/RowReceiptParticipant;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1879542
    return-void
.end method
