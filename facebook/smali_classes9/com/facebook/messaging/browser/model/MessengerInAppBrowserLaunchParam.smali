.class public Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;


# instance fields
.field public final b:D

.field public final c:Z

.field public final d:Z

.field public final e:LX/CJa;

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 1875564
    new-instance v0, LX/CJZ;

    invoke-direct {v0}, LX/CJZ;-><init>()V

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    .line 1875565
    iput-wide v2, v0, LX/CJZ;->a:D

    .line 1875566
    move-object v0, v0

    .line 1875567
    sget-object v1, LX/CJa;->UNKNOWN:LX/CJa;

    .line 1875568
    iput-object v1, v0, LX/CJZ;->f:LX/CJa;

    .line 1875569
    move-object v0, v0

    .line 1875570
    new-instance v1, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;

    invoke-direct {v1, v0}, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;-><init>(LX/CJZ;)V

    move-object v0, v1

    .line 1875571
    sput-object v0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->a:Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;

    .line 1875572
    new-instance v0, LX/CJY;

    invoke-direct {v0}, LX/CJY;-><init>()V

    sput-object v0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/CJZ;)V
    .locals 2

    .prologue
    .line 1875528
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1875529
    iget-wide v0, p1, LX/CJZ;->a:D

    iput-wide v0, p0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->b:D

    .line 1875530
    iget-boolean v0, p1, LX/CJZ;->b:Z

    iput-boolean v0, p0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->c:Z

    .line 1875531
    iget-boolean v0, p1, LX/CJZ;->c:Z

    iput-boolean v0, p0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->d:Z

    .line 1875532
    iget-object v0, p1, LX/CJZ;->f:LX/CJa;

    const-string v1, "source type must not be null"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CJa;

    iput-object v0, p0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->e:LX/CJa;

    .line 1875533
    iget-object v0, p1, LX/CJZ;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->f:Ljava/lang/String;

    .line 1875534
    iget-object v0, p1, LX/CJZ;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->g:Ljava/lang/String;

    .line 1875535
    iget-object v0, p1, LX/CJZ;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->h:Ljava/lang/String;

    .line 1875536
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1875553
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1875554
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->b:D

    .line 1875555
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->c:Z

    .line 1875556
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->d:Z

    .line 1875557
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CJa;->valueOf(Ljava/lang/String;)LX/CJa;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->e:LX/CJa;

    .line 1875558
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->f:Ljava/lang/String;

    .line 1875559
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->g:Ljava/lang/String;

    .line 1875560
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->h:Ljava/lang/String;

    .line 1875561
    return-void

    :cond_0
    move v0, v2

    .line 1875562
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1875563
    goto :goto_1
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1875552
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1875548
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;

    if-nez v1, :cond_1

    .line 1875549
    :cond_0
    :goto_0
    return v0

    .line 1875550
    :cond_1
    check-cast p1, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;

    .line 1875551
    iget-wide v2, p0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->b:D

    iget-wide v4, p1, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->b:D

    cmpl-double v1, v2, v4

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->c:Z

    iget-boolean v2, p1, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->c:Z

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->f:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->f:Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->g:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->g:Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->d:Z

    iget-boolean v2, p1, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->d:Z

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->e:LX/CJa;

    iget-object v2, p1, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->e:LX/CJa;

    invoke-virtual {v1, v2}, LX/CJa;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->h:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->h:Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 1875547
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->b:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->e:LX/CJa;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->h:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1875537
    iget-wide v4, p0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->b:D

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeDouble(D)V

    .line 1875538
    iget-boolean v0, p0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->c:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1875539
    iget-boolean v0, p0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->d:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1875540
    iget-object v0, p0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->e:LX/CJa;

    invoke-virtual {v0}, LX/CJa;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1875541
    iget-object v0, p0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1875542
    iget-object v0, p0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1875543
    iget-object v0, p0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1875544
    return-void

    :cond_0
    move v0, v2

    .line 1875545
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1875546
    goto :goto_1
.end method
