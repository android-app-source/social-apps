.class public Lcom/facebook/messaging/media/photoquality/PhotoQuality;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/media/photoquality/PhotoQuality;",
            ">;"
        }
    .end annotation
.end field

.field public static a:Lcom/facebook/messaging/media/photoquality/PhotoQuality;


# instance fields
.field public final b:I

.field public final c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1881077
    new-instance v0, Lcom/facebook/messaging/media/photoquality/PhotoQuality;

    const/16 v1, 0x3c0

    invoke-direct {v0, v1}, Lcom/facebook/messaging/media/photoquality/PhotoQuality;-><init>(I)V

    sput-object v0, Lcom/facebook/messaging/media/photoquality/PhotoQuality;->a:Lcom/facebook/messaging/media/photoquality/PhotoQuality;

    .line 1881078
    new-instance v0, LX/CMw;

    invoke-direct {v0}, LX/CMw;-><init>()V

    sput-object v0, Lcom/facebook/messaging/media/photoquality/PhotoQuality;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 1881087
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1881088
    iput p1, p0, Lcom/facebook/messaging/media/photoquality/PhotoQuality;->b:I

    .line 1881089
    const/high16 v0, -0x43800000    # -0.015625f

    add-int/lit16 v1, p1, -0x3c0

    int-to-float v1, v1

    mul-float/2addr v0, v1

    const/high16 v1, 0x42aa0000    # 85.0f

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 1881090
    const/16 v1, 0x37

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1881091
    const/16 v1, 0x5f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1881092
    move v0, v0

    .line 1881093
    iput v0, p0, Lcom/facebook/messaging/media/photoquality/PhotoQuality;->c:I

    .line 1881094
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1881083
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1881084
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/media/photoquality/PhotoQuality;->b:I

    .line 1881085
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/media/photoquality/PhotoQuality;->c:I

    .line 1881086
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1881095
    iget v0, p0, Lcom/facebook/messaging/media/photoquality/PhotoQuality;->b:I

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1881082
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1881079
    iget v0, p0, Lcom/facebook/messaging/media/photoquality/PhotoQuality;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1881080
    iget v0, p0, Lcom/facebook/messaging/media/photoquality/PhotoQuality;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1881081
    return-void
.end method
