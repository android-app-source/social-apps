.class public Lcom/facebook/messaging/media/photoquality/PhotoQualityCacheItemSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/media/photoquality/PhotoQualityCacheItem;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1881162
    const-class v0, Lcom/facebook/messaging/media/photoquality/PhotoQualityCacheItem;

    new-instance v1, Lcom/facebook/messaging/media/photoquality/PhotoQualityCacheItemSerializer;

    invoke-direct {v1}, Lcom/facebook/messaging/media/photoquality/PhotoQualityCacheItemSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1881163
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1881161
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/media/photoquality/PhotoQualityCacheItem;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1881164
    if-nez p0, :cond_0

    .line 1881165
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1881166
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1881167
    invoke-static {p0, p1, p2}, Lcom/facebook/messaging/media/photoquality/PhotoQualityCacheItemSerializer;->b(Lcom/facebook/messaging/media/photoquality/PhotoQualityCacheItem;LX/0nX;LX/0my;)V

    .line 1881168
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1881169
    return-void
.end method

.method private static b(Lcom/facebook/messaging/media/photoquality/PhotoQualityCacheItem;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1881156
    const-string v0, "threadKey"

    iget-object v1, p0, Lcom/facebook/messaging/media/photoquality/PhotoQualityCacheItem;->threadKey:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1881157
    const-string v0, "resolution"

    iget-object v1, p0, Lcom/facebook/messaging/media/photoquality/PhotoQualityCacheItem;->resolution:Ljava/lang/Integer;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1881158
    const-string v0, "thumbnailResolution"

    iget-object v1, p0, Lcom/facebook/messaging/media/photoquality/PhotoQualityCacheItem;->thumbnailResolution:Ljava/lang/Integer;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1881159
    const-string v0, "expirationTimeMs"

    iget-object v1, p0, Lcom/facebook/messaging/media/photoquality/PhotoQualityCacheItem;->expirationTimeMs:Ljava/lang/Long;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1881160
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1881155
    check-cast p1, Lcom/facebook/messaging/media/photoquality/PhotoQualityCacheItem;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/media/photoquality/PhotoQualityCacheItemSerializer;->a(Lcom/facebook/messaging/media/photoquality/PhotoQualityCacheItem;LX/0nX;LX/0my;)V

    return-void
.end method
