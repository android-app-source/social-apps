.class public Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile c:Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1880005
    const-class v0, Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;

    const-string v1, "emoji_popup"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1880002
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1880003
    iput-object p1, p0, Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;->b:LX/0Or;

    .line 1880004
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;
    .locals 4

    .prologue
    .line 1879989
    sget-object v0, Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;->c:Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;

    if-nez v0, :cond_1

    .line 1879990
    const-class v1, Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;

    monitor-enter v1

    .line 1879991
    :try_start_0
    sget-object v0, Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;->c:Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1879992
    if-eqz v2, :cond_0

    .line 1879993
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1879994
    new-instance v3, Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;

    const/16 p0, 0x509

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;-><init>(LX/0Or;)V

    .line 1879995
    move-object v0, v3

    .line 1879996
    sput-object v0, Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;->c:Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1879997
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1879998
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1879999
    :cond_1
    sget-object v0, Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;->c:Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;

    return-object v0

    .line 1880000
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1880001
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)LX/CLu;
    .locals 4

    .prologue
    .line 1879982
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1879983
    new-instance v1, Lcom/facebook/drawee/view/DraweeView;

    invoke-direct {v1, v0}, Lcom/facebook/drawee/view/DraweeView;-><init>(Landroid/content/Context;)V

    .line 1879984
    const v2, 0x7f0211a4

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/view/DraweeView;->setBackgroundResource(I)V

    .line 1879985
    const v2, 0x7f0809e1

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1879986
    new-instance v0, LX/CLv;

    .line 1879987
    sget-object v2, Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;->a:Lcom/facebook/common/callercontext/CallerContext;

    move-object v2, v2

    .line 1879988
    iget-object v3, p0, Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;->b:LX/0Or;

    invoke-direct {v0, v1, v2, v3}, LX/CLv;-><init>(Lcom/facebook/drawee/view/DraweeView;Lcom/facebook/common/callercontext/CallerContext;LX/0Or;)V

    return-object v0
.end method
