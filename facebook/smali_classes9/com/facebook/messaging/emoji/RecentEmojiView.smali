.class public Lcom/facebook/messaging/emoji/RecentEmojiView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Ljava/lang/Class;


# instance fields
.field public b:LX/0aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/CM4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/6e7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/CM6;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:LX/CM3;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/1Mv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Mv",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/7kc;

.field public j:Landroid/support/v7/widget/RecyclerView;

.field public k:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1880663
    const-class v0, Lcom/facebook/messaging/emoji/RecentEmojiView;

    sput-object v0, Lcom/facebook/messaging/emoji/RecentEmojiView;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/7kc;)V
    .locals 10

    .prologue
    .line 1880664
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1880665
    iput-object p2, p0, Lcom/facebook/messaging/emoji/RecentEmojiView;->i:LX/7kc;

    .line 1880666
    const-class v0, Lcom/facebook/messaging/emoji/RecentEmojiView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/emoji/RecentEmojiView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1880667
    const v0, 0x7f030472

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1880668
    const v0, 0x7f0d0d5e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/messaging/emoji/RecentEmojiView;->j:Landroid/support/v7/widget/RecyclerView;

    .line 1880669
    const v0, 0x7f0d0d5d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/messaging/emoji/RecentEmojiView;->k:Lcom/facebook/resources/ui/FbTextView;

    .line 1880670
    new-instance v0, LX/3wu;

    invoke-virtual {p0}, Lcom/facebook/messaging/emoji/RecentEmojiView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/messaging/emoji/RecentEmojiView;->i:LX/7kc;

    .line 1880671
    iget v3, v2, LX/7kc;->a:I

    move v2, v3

    .line 1880672
    invoke-direct {v0, v1, v2}, LX/3wu;-><init>(Landroid/content/Context;I)V

    .line 1880673
    iget-object v1, p0, Lcom/facebook/messaging/emoji/RecentEmojiView;->j:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1880674
    iget-object v3, p0, Lcom/facebook/messaging/emoji/RecentEmojiView;->e:LX/6e7;

    .line 1880675
    iget-object v4, v3, LX/6e7;->a:LX/0Px;

    move-object v3, v4

    .line 1880676
    if-eqz v3, :cond_0

    .line 1880677
    invoke-static {p0, v3}, Lcom/facebook/messaging/emoji/RecentEmojiView;->a$redex0(Lcom/facebook/messaging/emoji/RecentEmojiView;LX/0Px;)V

    .line 1880678
    :goto_0
    return-void

    .line 1880679
    :cond_0
    iget-object v4, p0, Lcom/facebook/messaging/emoji/RecentEmojiView;->b:LX/0aG;

    const-string v5, "fetch_recent_emoji"

    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    sget-object v7, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v8, Lcom/facebook/messaging/emoji/RecentEmojiView;

    invoke-static {v8}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v8

    const v9, -0xac8280e

    invoke-static/range {v4 .. v9}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v4

    invoke-interface {v4}, LX/1MF;->start()LX/1ML;

    move-result-object v4

    .line 1880680
    new-instance v5, LX/CMT;

    invoke-direct {v5, p0}, LX/CMT;-><init>(Lcom/facebook/messaging/emoji/RecentEmojiView;)V

    .line 1880681
    invoke-static {v4, v5}, LX/1Mv;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)LX/1Mv;

    move-result-object v6

    iput-object v6, p0, Lcom/facebook/messaging/emoji/RecentEmojiView;->h:LX/1Mv;

    .line 1880682
    iget-object v6, p0, Lcom/facebook/messaging/emoji/RecentEmojiView;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1880683
    goto :goto_0
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/messaging/emoji/RecentEmojiView;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v1

    check-cast v1, LX/0aG;

    const-class v2, LX/CM4;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/CM4;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/6e7;->a(LX/0QB;)LX/6e7;

    move-result-object p0

    check-cast p0, LX/6e7;

    iput-object v1, p1, Lcom/facebook/messaging/emoji/RecentEmojiView;->b:LX/0aG;

    iput-object v2, p1, Lcom/facebook/messaging/emoji/RecentEmojiView;->c:LX/CM4;

    iput-object v3, p1, Lcom/facebook/messaging/emoji/RecentEmojiView;->d:Ljava/util/concurrent/ExecutorService;

    iput-object p0, p1, Lcom/facebook/messaging/emoji/RecentEmojiView;->e:LX/6e7;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/emoji/RecentEmojiView;LX/0Px;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ui/emoji/model/Emoji;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1880684
    iget-object v0, p0, Lcom/facebook/messaging/emoji/RecentEmojiView;->g:LX/CM3;

    if-nez v0, :cond_0

    .line 1880685
    iget-object v0, p0, Lcom/facebook/messaging/emoji/RecentEmojiView;->c:LX/CM4;

    iget-object v1, p0, Lcom/facebook/messaging/emoji/RecentEmojiView;->i:LX/7kc;

    invoke-virtual {v0, v1}, LX/CM4;->a(LX/7kc;)LX/CM3;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/emoji/RecentEmojiView;->g:LX/CM3;

    .line 1880686
    iget-object v0, p0, Lcom/facebook/messaging/emoji/RecentEmojiView;->g:LX/CM3;

    iget-object v1, p0, Lcom/facebook/messaging/emoji/RecentEmojiView;->f:LX/CM6;

    .line 1880687
    iput-object v1, v0, LX/CM3;->j:LX/CM6;

    .line 1880688
    iget-object v0, p0, Lcom/facebook/messaging/emoji/RecentEmojiView;->j:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/messaging/emoji/RecentEmojiView;->g:LX/CM3;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1880689
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/emoji/RecentEmojiView;->g:LX/CM3;

    invoke-virtual {v0, p1}, LX/CM3;->a(LX/0Px;)V

    .line 1880690
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1880691
    iget-object v0, p0, Lcom/facebook/messaging/emoji/RecentEmojiView;->j:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 1880692
    iget-object v0, p0, Lcom/facebook/messaging/emoji/RecentEmojiView;->k:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1880693
    :goto_0
    return-void

    .line 1880694
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/emoji/RecentEmojiView;->k:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1880695
    iget-object v0, p0, Lcom/facebook/messaging/emoji/RecentEmojiView;->j:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x21796408

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1880696
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 1880697
    iget-object v1, p0, Lcom/facebook/messaging/emoji/RecentEmojiView;->h:LX/1Mv;

    if-eqz v1, :cond_0

    .line 1880698
    iget-object v1, p0, Lcom/facebook/messaging/emoji/RecentEmojiView;->h:LX/1Mv;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/1Mv;->a(Z)V

    .line 1880699
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/messaging/emoji/RecentEmojiView;->h:LX/1Mv;

    .line 1880700
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x11233964

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
