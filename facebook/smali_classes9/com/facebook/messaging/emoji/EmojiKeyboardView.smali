.class public Lcom/facebook/messaging/emoji/EmojiKeyboardView;
.super Lcom/facebook/messaging/tabbedpager/TabbedPager;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public a:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/CMH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1zU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/CMM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CML;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/CMN;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private i:LX/7kc;

.field private j:I

.field private k:LX/CMG;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:LX/CM7;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1880118
    invoke-direct {p0, p1}, Lcom/facebook/messaging/tabbedpager/TabbedPager;-><init>(Landroid/content/Context;)V

    .line 1880119
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1880120
    iput-object v0, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->h:LX/0Ot;

    .line 1880121
    invoke-direct {p0}, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->d()V

    .line 1880122
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1880240
    invoke-direct {p0, p1, p2}, Lcom/facebook/messaging/tabbedpager/TabbedPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1880241
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1880242
    iput-object v0, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->h:LX/0Ot;

    .line 1880243
    invoke-direct {p0}, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->d()V

    .line 1880244
    return-void
.end method

.method private a(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/7HE;",
            ">;)",
            "Ljava/util/List",
            "<",
            "LX/7HE;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1880235
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7HE;

    .line 1880236
    iget-object v1, v0, LX/7HE;->d:LX/7HD;

    move-object v0, v1

    .line 1880237
    sget-object v1, LX/7HD;->RECENTLY_USED:LX/7HD;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->g:LX/CMN;

    invoke-virtual {v0}, LX/CMN;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1880238
    const/4 v0, 0x1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {p1, v0, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    .line 1880239
    :cond_0
    return-object p1
.end method

.method private static a(Lcom/facebook/messaging/emoji/EmojiKeyboardView;Lcom/facebook/common/callercontexttagger/AnalyticsTagger;LX/CMH;LX/1zU;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/CMM;LX/0Ot;LX/CMN;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/emoji/EmojiKeyboardView;",
            "Lcom/facebook/common/callercontexttagger/AnalyticsTagger;",
            "LX/CMH;",
            "LX/1zU;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/CMM;",
            "LX/0Ot",
            "<",
            "LX/CML;",
            ">;",
            "LX/CMN;",
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1880234
    iput-object p1, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->a:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    iput-object p2, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->b:LX/CMH;

    iput-object p3, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->c:LX/1zU;

    iput-object p4, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p5, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->e:LX/CMM;

    iput-object p6, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->f:LX/0Ot;

    iput-object p7, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->g:LX/CMN;

    iput-object p8, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->h:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 10

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v8

    move-object v0, p0

    check-cast v0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;

    invoke-static {v8}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(LX/0QB;)Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    move-result-object v1

    check-cast v1, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    const-class v2, LX/CMH;

    invoke-interface {v8, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/CMH;

    invoke-static {v8}, LX/1zU;->a(LX/0QB;)LX/1zU;

    move-result-object v3

    check-cast v3, LX/1zU;

    invoke-static {v8}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    new-instance v5, LX/CMM;

    invoke-direct {v5}, LX/CMM;-><init>()V

    move-object v5, v5

    move-object v5, v5

    check-cast v5, LX/CMM;

    const/16 v6, 0x275a

    invoke-static {v8, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v8}, LX/CMN;->b(LX/0QB;)LX/CMN;

    move-result-object v7

    check-cast v7, LX/CMN;

    const/16 v9, 0x542

    invoke-static {v8, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static/range {v0 .. v8}, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->a(Lcom/facebook/messaging/emoji/EmojiKeyboardView;Lcom/facebook/common/callercontexttagger/AnalyticsTagger;LX/CMH;LX/1zU;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/CMM;LX/0Ot;LX/CMN;LX/0Ot;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/emoji/EmojiKeyboardView;Lcom/facebook/ui/emoji/model/Emoji;)V
    .locals 1

    .prologue
    .line 1880230
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->performHapticFeedback(I)Z

    .line 1880231
    iget-object v0, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->g:LX/CMN;

    invoke-virtual {v0}, LX/CMN;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1880232
    invoke-direct {p0, p1}, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->b(Lcom/facebook/ui/emoji/model/Emoji;)V

    .line 1880233
    :cond_0
    return-void
.end method

.method private b(Lcom/facebook/ui/emoji/model/Emoji;)V
    .locals 6

    .prologue
    .line 1880226
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1880227
    const-string v0, "emoji"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1880228
    iget-object v0, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0aG;

    const-string v1, "update_recent_emoji"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v4, Lcom/facebook/messaging/emoji/EmojiKeyboardView;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, 0x7bd3f7f6

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    .line 1880229
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 1880216
    const-class v0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1880217
    invoke-virtual {p0}, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1880218
    invoke-virtual {p0}, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f010370

    invoke-static {v0, v1}, LX/0WH;->b(Landroid/content/Context;I)LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1880219
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->e:LX/CMM;

    new-instance v1, LX/CM5;

    invoke-direct {v1, p0}, LX/CM5;-><init>(Lcom/facebook/messaging/emoji/EmojiKeyboardView;)V

    .line 1880220
    iput-object v1, v0, LX/CMM;->c:LX/CM5;

    .line 1880221
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->setShowEndTabButton(Z)V

    .line 1880222
    iget-object v0, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->e:LX/CMM;

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->setEndTabButtonOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1880223
    invoke-virtual {p0}, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0801ed

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->setEndTabButtonContentDescription(Ljava/lang/String;)V

    .line 1880224
    const-string v1, "emoji_popup"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {p0, v1, v2}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(Landroid/view/View;Ljava/lang/String;Ljava/lang/Class;)V

    .line 1880225
    return-void
.end method

.method private e()Z
    .locals 1

    .prologue
    .line 1880215
    iget-object v0, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->i:LX/7kc;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()V
    .locals 6

    .prologue
    .line 1880202
    const-string v0, "loadAndSetEmojisForBackside"

    const v1, 0x335b1e10

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1880203
    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->a()V

    .line 1880204
    iget-object v0, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->b:LX/CMH;

    iget-object v1, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->i:LX/7kc;

    .line 1880205
    new-instance v5, LX/CMG;

    invoke-static {v0}, LX/3Rl;->a(LX/0QB;)LX/3Rl;

    move-result-object v2

    check-cast v2, LX/3Rl;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    const-class v4, LX/CM4;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/CM4;

    invoke-direct {v5, v2, v3, v4, v1}, LX/CMG;-><init>(LX/3Rl;Landroid/content/Context;LX/CM4;LX/7kc;)V

    .line 1880206
    move-object v0, v5

    .line 1880207
    iput-object v0, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->k:LX/CMG;

    .line 1880208
    iget-object v0, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->k:LX/CMG;

    new-instance v1, LX/CM6;

    invoke-direct {v1, p0}, LX/CM6;-><init>(Lcom/facebook/messaging/emoji/EmojiKeyboardView;)V

    .line 1880209
    iput-object v1, v0, LX/CMG;->f:LX/CM6;

    .line 1880210
    iget-object v0, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->k:LX/CMG;

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->setAdapter(LX/8CX;)V

    .line 1880211
    invoke-direct {p0}, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->h()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->setItems(Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1880212
    const v0, -0x60cbbd14

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1880213
    return-void

    .line 1880214
    :catchall_0
    move-exception v0

    const v1, 0x7cb64b6e

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private g()Z
    .locals 2

    .prologue
    .line 1880199
    iget-object v0, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->k:LX/CMG;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->k:LX/CMG;

    .line 1880200
    iget-object v1, v0, LX/CMG;->h:Ljava/lang/String;

    move-object v0, v1

    .line 1880201
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/CMI;->c:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/7HE;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1880167
    iget-object v0, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->g:LX/CMN;

    invoke-virtual {v0}, LX/CMN;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1880168
    iget-object v0, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CML;

    invoke-virtual {v0}, LX/CML;->a()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->j:I

    .line 1880169
    iget-object v0, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->c:LX/1zU;

    iget v1, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->j:I

    .line 1880170
    iget-object v2, v0, LX/1zU;->f:LX/1zH;

    invoke-virtual {v2}, LX/1zH;->a()Ljava/util/List;

    move-result-object v2

    .line 1880171
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1880172
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7HE;

    .line 1880173
    iget-object v5, v2, LX/7HE;->d:LX/7HD;

    move-object v5, v5

    .line 1880174
    sget-object v6, LX/7HD;->RECENTLY_USED:LX/7HD;

    if-ne v5, v6, :cond_0

    .line 1880175
    invoke-virtual {v3, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1880176
    :cond_0
    iget v5, v2, LX/7HE;->a:I

    move v5, v5

    .line 1880177
    iget v6, v2, LX/7HE;->b:I

    move v6, v6

    .line 1880178
    iget-object v7, v2, LX/7HE;->c:Ljava/util/List;

    move-object v2, v7

    .line 1880179
    invoke-static {v1}, LX/1zU;->c(I)Z

    move-result v7

    if-nez v7, :cond_3

    .line 1880180
    :goto_1
    move-object v2, v2

    .line 1880181
    invoke-static {v5, v6, v2}, LX/7HE;->a(IILjava/util/List;)LX/7HE;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1880182
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v0, v2

    .line 1880183
    :goto_2
    invoke-direct {p0, v0}, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 1880184
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->c:LX/1zU;

    .line 1880185
    iget-object v1, v0, LX/1zU;->f:LX/1zH;

    invoke-virtual {v1}, LX/1zH;->a()Ljava/util/List;

    move-result-object v1

    move-object v0, v1

    .line 1880186
    goto :goto_2

    .line 1880187
    :cond_3
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 1880188
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/ui/emoji/model/Emoji;

    .line 1880189
    invoke-virtual {v0, v7}, LX/1zU;->a(Lcom/facebook/ui/emoji/model/Emoji;)Z

    move-result v10

    if-nez v10, :cond_4

    .line 1880190
    invoke-virtual {v8, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    .line 1880191
    :cond_4
    new-instance v10, Lcom/facebook/ui/emoji/model/Emoji;

    iget-object v11, v0, LX/1zU;->f:LX/1zH;

    .line 1880192
    iget v12, v7, Lcom/facebook/ui/emoji/model/Emoji;->b:I

    move v12, v12

    .line 1880193
    const/4 v13, -0x1

    invoke-virtual {v11, v12, v1, v13}, LX/1zH;->a(III)I

    move-result v11

    .line 1880194
    iget v12, v7, Lcom/facebook/ui/emoji/model/Emoji;->b:I

    move v12, v12

    .line 1880195
    iget v13, v7, Lcom/facebook/ui/emoji/model/Emoji;->d:I

    move v7, v13

    .line 1880196
    invoke-direct {v10, v11, v12, v1, v7}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    .line 1880197
    invoke-virtual {v8, v10}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    .line 1880198
    :cond_5
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto :goto_1
.end method

.method private i()Z
    .locals 2

    .prologue
    .line 1880166
    invoke-direct {p0}, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->g:LX/CMN;

    invoke-virtual {v0}, LX/CMN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->j:I

    iget-object v0, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CML;

    invoke-virtual {v0}, LX/CML;->a()I

    move-result v0

    if-eq v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final c()V
    .locals 1

    .prologue
    .line 1880163
    invoke-direct {p0}, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1880164
    invoke-direct {p0}, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->f()V

    .line 1880165
    :cond_0
    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2c

    const v1, 0x3b498b4d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1880155
    iget-object v1, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->k:LX/CMG;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->k:LX/CMG;

    .line 1880156
    iget-object v2, v1, LX/CMG;->h:Ljava/lang/String;

    move-object v1, v2

    .line 1880157
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1880158
    iget-object v1, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/CMI;->c:LX/0Tn;

    iget-object v3, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->k:LX/CMG;

    .line 1880159
    iget-object v5, v3, LX/CMG;->h:Ljava/lang/String;

    move-object v3, v5

    .line 1880160
    invoke-interface {v1, v2, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 1880161
    :cond_0
    invoke-super {p0}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->onDetachedFromWindow()V

    .line 1880162
    const/16 v1, 0x2d

    const v2, 0x43e8cd7d

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    .line 1880145
    iget-object v0, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->k:LX/CMG;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->k:LX/CMG;

    const/4 v2, 0x1

    .line 1880146
    iget-object v1, v0, LX/CMG;->e:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1880147
    iget-object v1, v0, LX/CMG;->e:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CM3;

    .line 1880148
    invoke-virtual {v1}, LX/CM3;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    .line 1880149
    :goto_0
    move v0, v1

    .line 1880150
    if-eqz v0, :cond_1

    .line 1880151
    const/4 v0, 0x1

    .line 1880152
    :goto_1
    return v0

    :cond_1
    invoke-super {p0, p1}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v1, v0, LX/CMG;->g:Lcom/facebook/messaging/emoji/RecentEmojiView;

    if-eqz v1, :cond_3

    iget-object v1, v0, LX/CMG;->g:Lcom/facebook/messaging/emoji/RecentEmojiView;

    .line 1880153
    iget-object v3, v1, Lcom/facebook/messaging/emoji/RecentEmojiView;->g:LX/CM3;

    if-eqz v3, :cond_4

    iget-object v3, v1, Lcom/facebook/messaging/emoji/RecentEmojiView;->g:LX/CM3;

    invoke-virtual {v3}, LX/CM3;->d()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x1

    :goto_2
    move v1, v3

    .line 1880154
    if-eqz v1, :cond_3

    move v1, v2

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    :cond_4
    const/4 v3, 0x0

    goto :goto_2
.end method

.method public final onLayout(ZIIII)V
    .locals 3

    .prologue
    .line 1880140
    invoke-super/range {p0 .. p5}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->onLayout(ZIIII)V

    .line 1880141
    invoke-direct {p0}, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1880142
    iget-object v0, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/CMI;->c:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1880143
    invoke-virtual {p0, v0}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->a(Ljava/lang/String;)V

    .line 1880144
    :cond_0
    return-void
.end method

.method public final onMeasure(II)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1880127
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 1880128
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 1880129
    if-lez v2, :cond_0

    if-lez v3, :cond_0

    .line 1880130
    invoke-direct {p0}, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->e()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 1880131
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1880132
    new-instance v5, LX/7kd;

    new-instance v6, LX/CLx;

    invoke-direct {v6}, LX/CLx;-><init>()V

    invoke-direct {v5, v4, v6}, LX/7kd;-><init>(Landroid/content/res/Resources;LX/7kb;)V

    const v6, 0x7f0b025b

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {v5, v2, v3, v1}, LX/7kd;->a(IIZ)LX/7kc;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->i:LX/7kc;

    .line 1880133
    if-eqz v0, :cond_0

    .line 1880134
    iget-boolean v0, p0, Lcom/facebook/messaging/tabbedpager/TabbedPager;->n:Z

    move v0, v0

    .line 1880135
    if-nez v0, :cond_0

    .line 1880136
    invoke-direct {p0}, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->f()V

    .line 1880137
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->onMeasure(II)V

    .line 1880138
    return-void

    :cond_1
    move v0, v1

    .line 1880139
    goto :goto_0
.end method

.method public setBackspaceVisible(Z)V
    .locals 0

    .prologue
    .line 1880125
    invoke-virtual {p0, p1}, Lcom/facebook/messaging/tabbedpager/TabbedPager;->setShowEndTabButton(Z)V

    .line 1880126
    return-void
.end method

.method public setEmojiPickerListener(LX/CM7;)V
    .locals 0

    .prologue
    .line 1880123
    iput-object p1, p0, Lcom/facebook/messaging/emoji/EmojiKeyboardView;->l:LX/CM7;

    .line 1880124
    return-void
.end method
