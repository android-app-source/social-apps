.class public Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/CME;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1zU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:I

.field public d:LX/CMP;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1880647
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1880648
    invoke-direct {p0}, Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;->a()V

    .line 1880649
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1880644
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1880645
    invoke-direct {p0}, Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;->a()V

    .line 1880646
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1880641
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1880642
    invoke-direct {p0}, Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;->a()V

    .line 1880643
    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    .line 1880623
    const-class v0, Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1880624
    const v0, 0x7f030af3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1880625
    invoke-virtual {p0}, Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1880626
    iget-object v1, p0, Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;->a:LX/CME;

    const v2, 0x7f0a019a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 1880627
    const/4 v3, 0x0

    const v4, 0x3e99999a    # 0.3f

    invoke-static {v2, v4}, LX/47Z;->b(IF)I

    move-result v4

    invoke-static {v1, v3, v2, v4}, LX/CME;->a(LX/CME;III)V

    .line 1880628
    iget-object v1, p0, Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;->a:LX/CME;

    invoke-direct {p0}, Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;->b()LX/0Px;

    move-result-object v2

    .line 1880629
    iget-object v3, v1, LX/CME;->g:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 1880630
    iget-object v3, v1, LX/CME;->g:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1880631
    invoke-virtual {v1}, LX/1OM;->notifyDataSetChanged()V

    .line 1880632
    iget-object v1, p0, Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;->a:LX/CME;

    new-instance v2, LX/CMR;

    invoke-direct {v2, p0}, LX/CMR;-><init>(Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;)V

    .line 1880633
    iput-object v2, v1, LX/CME;->m:LX/CMR;

    .line 1880634
    const v1, 0x7f0b0262

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;->c:I

    .line 1880635
    const v0, 0x7f0d04e7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    .line 1880636
    new-instance v1, LX/3wu;

    invoke-virtual {p0}, Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x3

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, LX/3wu;-><init>(Landroid/content/Context;IIZ)V

    .line 1880637
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1880638
    new-instance v1, LX/CMS;

    invoke-direct {v1, p0}, LX/CMS;-><init>(Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 1880639
    iget-object v1, p0, Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;->a:LX/CME;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1880640
    return-void
.end method

.method private static a(Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;LX/CME;LX/1zU;)V
    .locals 0

    .prologue
    .line 1880622
    iput-object p1, p0, Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;->a:LX/CME;

    iput-object p2, p0, Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;->b:LX/1zU;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;

    new-instance p1, LX/CME;

    invoke-static {v1}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-static {v1}, Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;->a(LX/0QB;)Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;

    invoke-static {v1}, LX/1zC;->a(LX/0QB;)LX/1zC;

    move-result-object v3

    check-cast v3, LX/1zC;

    invoke-direct {p1, v0, v2, v3}, LX/CME;-><init>(Landroid/content/res/Resources;Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;LX/1zC;)V

    move-object v0, p1

    check-cast v0, LX/CME;

    invoke-static {v1}, LX/1zU;->a(LX/0QB;)LX/1zU;

    move-result-object v1

    check-cast v1, LX/1zU;

    invoke-static {p0, v0, v1}, Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;->a(Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;LX/CME;LX/1zU;)V

    return-void
.end method

.method private b()LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/ui/emoji/model/Emoji;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1880618
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1880619
    iget-object v1, p0, Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;->b:LX/1zU;

    const v2, 0x1f44d

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/1zU;->a(II)Lcom/facebook/ui/emoji/model/Emoji;

    move-result-object v1

    .line 1880620
    iget-object v2, p0, Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;->b:LX/1zU;

    invoke-virtual {v2, v1}, LX/1zU;->b(Lcom/facebook/ui/emoji/model/Emoji;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1880621
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getSelectedEmojiColor()I
    .locals 1

    .prologue
    .line 1880613
    iget-object v0, p0, Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;->a:LX/CME;

    .line 1880614
    iget-object p0, v0, LX/CME;->l:Lcom/facebook/ui/emoji/model/Emoji;

    move-object v0, p0

    .line 1880615
    if-eqz v0, :cond_0

    .line 1880616
    iget p0, v0, Lcom/facebook/ui/emoji/model/Emoji;->c:I

    move v0, p0

    .line 1880617
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public setOnEmojiClickListener(LX/CMP;)V
    .locals 0

    .prologue
    .line 1880606
    iput-object p1, p0, Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;->d:LX/CMP;

    .line 1880607
    return-void
.end method

.method public setSelectedEmojiColor(I)V
    .locals 2

    .prologue
    .line 1880608
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 1880609
    iget-object v0, p0, Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;->a:LX/CME;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/CME;->a(Lcom/facebook/ui/emoji/model/Emoji;)V

    .line 1880610
    :goto_0
    return-void

    .line 1880611
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;->b:LX/1zU;

    const v1, 0x1f44d

    invoke-virtual {v0, v1, p1}, LX/1zU;->a(II)Lcom/facebook/ui/emoji/model/Emoji;

    move-result-object v0

    .line 1880612
    iget-object v1, p0, Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;->a:LX/CME;

    invoke-virtual {v1, v0}, LX/CME;->a(Lcom/facebook/ui/emoji/model/Emoji;)V

    goto :goto_0
.end method
