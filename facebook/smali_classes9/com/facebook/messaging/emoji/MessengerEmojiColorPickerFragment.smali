.class public Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerFragment;
.super Lcom/facebook/messaging/widget/dialog/SlidingSheetDialogFragment;
.source ""


# instance fields
.field public m:LX/CML;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private n:Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1880588
    invoke-direct {p0}, Lcom/facebook/messaging/widget/dialog/SlidingSheetDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3757ca44

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1880589
    invoke-super {p0, p1}, Lcom/facebook/messaging/widget/dialog/SlidingSheetDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1880590
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerFragment;

    invoke-static {v1}, LX/CML;->a(LX/0QB;)LX/CML;

    move-result-object v1

    check-cast v1, LX/CML;

    iput-object v1, p0, Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerFragment;->m:LX/CML;

    .line 1880591
    const/16 v1, 0x2b

    const v2, -0x4b57eb8d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x55a3ae83

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1880592
    const v1, 0x7f030af2

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x76df5c83

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1880593
    const v0, 0x7f0d1bb9

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;

    iput-object v0, p0, Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerFragment;->n:Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;

    .line 1880594
    iget-object v0, p0, Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerFragment;->n:Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;

    iget-object v1, p0, Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerFragment;->m:LX/CML;

    invoke-virtual {v1}, LX/CML;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;->setSelectedEmojiColor(I)V

    .line 1880595
    iget-object v0, p0, Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerFragment;->n:Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;

    new-instance v1, LX/CMQ;

    invoke-direct {v1, p0}, LX/CMQ;-><init>(Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerFragment;)V

    .line 1880596
    iput-object v1, v0, Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerView;->d:LX/CMP;

    .line 1880597
    return-void
.end method
