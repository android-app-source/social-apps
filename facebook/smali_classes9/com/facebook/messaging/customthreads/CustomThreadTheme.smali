.class public Lcom/facebook/messaging/customthreads/CustomThreadTheme;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/customthreads/CustomThreadTheme;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:I

.field public final b:I

.field private final c:I

.field public final d:I

.field public final e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1878899
    new-instance v0, LX/CLT;

    invoke-direct {v0}, LX/CLT;-><init>()V

    sput-object v0, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/CLU;)V
    .locals 1

    .prologue
    .line 1878900
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1878901
    iget v0, p1, LX/CLU;->a:I

    move v0, v0

    .line 1878902
    iput v0, p0, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->a:I

    .line 1878903
    iget v0, p1, LX/CLU;->b:I

    move v0, v0

    .line 1878904
    iput v0, p0, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->b:I

    .line 1878905
    iget v0, p1, LX/CLU;->c:I

    move v0, v0

    .line 1878906
    iput v0, p0, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->c:I

    .line 1878907
    iget v0, p1, LX/CLU;->d:I

    move v0, v0

    .line 1878908
    iput v0, p0, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->d:I

    .line 1878909
    iget v0, p1, LX/CLU;->e:I

    move v0, v0

    .line 1878910
    iput v0, p0, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->e:I

    .line 1878911
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1878912
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1878913
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->a:I

    .line 1878914
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->b:I

    .line 1878915
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->c:I

    .line 1878916
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->d:I

    .line 1878917
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->e:I

    .line 1878918
    return-void
.end method

.method public static newBuilder()LX/CLU;
    .locals 1

    .prologue
    .line 1878919
    new-instance v0, LX/CLU;

    invoke-direct {v0}, LX/CLU;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1878920
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1878921
    iget v0, p0, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1878922
    iget v0, p0, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1878923
    iget v0, p0, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1878924
    iget v0, p0, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1878925
    iget v0, p0, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1878926
    return-void
.end method
