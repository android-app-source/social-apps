.class public Lcom/facebook/messaging/customthreads/CustomSmsTheme;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/customthreads/CustomSmsTheme;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1878889
    new-instance v0, LX/CLS;

    invoke-direct {v0}, LX/CLS;-><init>()V

    sput-object v0, Lcom/facebook/messaging/customthreads/CustomSmsTheme;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1878892
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1878893
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/customthreads/CustomSmsTheme;->a:I

    .line 1878894
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1878895
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1878890
    iget v0, p0, Lcom/facebook/messaging/customthreads/CustomSmsTheme;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1878891
    return-void
.end method
