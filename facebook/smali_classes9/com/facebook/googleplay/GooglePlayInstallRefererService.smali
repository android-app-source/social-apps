.class public Lcom/facebook/googleplay/GooglePlayInstallRefererService;
.super LX/1ZN;
.source ""


# static fields
.field private static final c:Ljava/lang/String;

.field private static d:LX/1ql;


# instance fields
.field public a:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/CFf;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1863883
    const-class v0, Lcom/facebook/googleplay/GooglePlayInstallRefererService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/googleplay/GooglePlayInstallRefererService;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1863881
    const-string v0, "GooglePlayInstallRefererService"

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 1863882
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;LX/1r1;)V
    .locals 2

    .prologue
    .line 1863872
    sget-object v0, Lcom/facebook/googleplay/GooglePlayInstallRefererService;->d:LX/1ql;

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    .line 1863873
    const/4 v0, 0x1

    sget-object v1, Lcom/facebook/googleplay/GooglePlayInstallRefererService;->c:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, LX/1r1;->a(ILjava/lang/String;)LX/1ql;

    move-result-object v0

    sput-object v0, Lcom/facebook/googleplay/GooglePlayInstallRefererService;->d:LX/1ql;

    .line 1863874
    :cond_0
    sget-object v0, Lcom/facebook/googleplay/GooglePlayInstallRefererService;->d:LX/1ql;

    invoke-virtual {v0}, LX/1ql;->c()V

    .line 1863875
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1863876
    invoke-virtual {v0, p1}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 1863877
    const-class v1, Lcom/facebook/googleplay/GooglePlayInstallRefererService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1863878
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1863879
    :goto_0
    return-void

    .line 1863880
    :catch_0
    sget-object v0, Lcom/facebook/googleplay/GooglePlayInstallRefererService;->d:LX/1ql;

    invoke-virtual {v0}, LX/1ql;->d()V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/googleplay/GooglePlayInstallRefererService;LX/03V;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/googleplay/GooglePlayInstallRefererService;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Ljava/util/Set",
            "<",
            "LX/CFf;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1863871
    iput-object p1, p0, Lcom/facebook/googleplay/GooglePlayInstallRefererService;->a:LX/03V;

    iput-object p2, p0, Lcom/facebook/googleplay/GooglePlayInstallRefererService;->b:Ljava/util/Set;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/googleplay/GooglePlayInstallRefererService;

    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    new-instance v2, LX/0U8;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v3

    new-instance p1, LX/CFh;

    invoke-direct {p1, v1}, LX/CFh;-><init>(LX/0QB;)V

    invoke-direct {v2, v3, p1}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v1, v2

    invoke-static {p0, v0, v1}, Lcom/facebook/googleplay/GooglePlayInstallRefererService;->a(Lcom/facebook/googleplay/GooglePlayInstallRefererService;LX/03V;Ljava/util/Set;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1863867
    invoke-static {p1}, Lcom/facebook/googleplay/GooglePlayInstallRefererService;->b(Ljava/lang/String;)LX/0P1;

    move-result-object v1

    .line 1863868
    iget-object v0, p0, Lcom/facebook/googleplay/GooglePlayInstallRefererService;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CFf;

    .line 1863869
    invoke-interface {v0, v1}, LX/CFf;->a(LX/0P1;)V

    goto :goto_0

    .line 1863870
    :cond_0
    return-void
.end method

.method private static b(Ljava/lang/String;)LX/0P1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1863862
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    invoke-static {p0}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->encodedQuery(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 1863863
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    .line 1863864
    invoke-virtual {v1}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1863865
    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    .line 1863866
    :cond_0
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x24

    const v2, 0x23621d0

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1863851
    :try_start_0
    const-string v0, "referrer"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1863852
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onHandleIntent: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1863853
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1863854
    invoke-direct {p0, v0}, Lcom/facebook/googleplay/GooglePlayInstallRefererService;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1863855
    :cond_0
    sget-object v0, Lcom/facebook/googleplay/GooglePlayInstallRefererService;->d:LX/1ql;

    if-eqz v0, :cond_1

    .line 1863856
    sget-object v0, Lcom/facebook/googleplay/GooglePlayInstallRefererService;->d:LX/1ql;

    invoke-virtual {v0}, LX/1ql;->d()V

    .line 1863857
    :goto_0
    const v0, -0x64cd2cab    # -1.47921E-22f

    invoke-static {v0, v1}, LX/02F;->d(II)V

    return-void

    .line 1863858
    :cond_1
    iget-object v0, p0, Lcom/facebook/googleplay/GooglePlayInstallRefererService;->a:LX/03V;

    sget-object v2, Lcom/facebook/googleplay/GooglePlayInstallRefererService;->c:Ljava/lang/String;

    const-string v3, "wakelock is null and cannot be released"

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1863859
    :catchall_0
    move-exception v0

    sget-object v2, Lcom/facebook/googleplay/GooglePlayInstallRefererService;->d:LX/1ql;

    if-eqz v2, :cond_2

    .line 1863860
    sget-object v2, Lcom/facebook/googleplay/GooglePlayInstallRefererService;->d:LX/1ql;

    invoke-virtual {v2}, LX/1ql;->d()V

    .line 1863861
    :goto_1
    const v2, -0x357a4ec8    # -4380828.0f

    invoke-static {v2, v1}, LX/02F;->d(II)V

    throw v0

    :cond_2
    iget-object v2, p0, Lcom/facebook/googleplay/GooglePlayInstallRefererService;->a:LX/03V;

    sget-object v3, Lcom/facebook/googleplay/GooglePlayInstallRefererService;->c:Ljava/lang/String;

    const-string v4, "wakelock is null and cannot be released"

    invoke-virtual {v2, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x58be420a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1863848
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 1863849
    invoke-static {p0, p0}, Lcom/facebook/googleplay/GooglePlayInstallRefererService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1863850
    const/16 v1, 0x25

    const v2, 0x125b47e5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
