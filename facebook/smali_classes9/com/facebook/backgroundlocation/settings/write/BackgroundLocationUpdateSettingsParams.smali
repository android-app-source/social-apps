.class public Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1799345
    new-instance v0, LX/Bac;

    invoke-direct {v0}, LX/Bac;-><init>()V

    sput-object v0, Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0am;LX/0am;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1799346
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1799347
    iput-object p1, p0, Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;->a:LX/0am;

    .line 1799348
    iput-object p2, p0, Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;->b:LX/0am;

    .line 1799349
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1799350
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1799351
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 1799352
    if-nez v0, :cond_0

    .line 1799353
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;->a:LX/0am;

    .line 1799354
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;->b:LX/0am;

    .line 1799355
    return-void

    .line 1799356
    :cond_0
    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;->a:LX/0am;

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;
    .locals 3

    .prologue
    .line 1799357
    new-instance v0, Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    invoke-static {p0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;-><init>(LX/0am;LX/0am;)V

    return-object v0
.end method

.method public static a(ZLjava/lang/String;)Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;
    .locals 3

    .prologue
    .line 1799358
    new-instance v0, Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;-><init>(LX/0am;LX/0am;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1799359
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1799360
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1799361
    iget-object v0, p0, Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1799362
    return-void

    .line 1799363
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
