.class public Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Po;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/3ZK;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final c:LX/C51;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/C51;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1848188
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1848189
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;->a:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    .line 1848190
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 1848191
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;->c:LX/C51;

    .line 1848192
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;
    .locals 6

    .prologue
    .line 1848177
    const-class v1, Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;

    monitor-enter v1

    .line 1848178
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1848179
    sput-object v2, Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1848180
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1848181
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1848182
    new-instance p0, Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    const-class v5, LX/C51;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/C51;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;-><init>(Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/C51;)V

    .line 1848183
    move-object v0, p0

    .line 1848184
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1848185
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1848186
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1848187
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 1848176
    sget-object v0, LX/3ZK;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1848164
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Ps;

    const/4 v4, 0x0

    .line 1848165
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;->c:LX/C51;

    check-cast p3, LX/1Po;

    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v1

    const-string v2, "throwbackFeedFooter"

    const-string v3, "goodwill_throwback_permalink_ufi"

    invoke-virtual {v0, p2, v1, v2, v3}, LX/C51;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;Ljava/lang/String;Ljava/lang/String;)LX/C50;

    move-result-object v0

    .line 1848166
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;->a:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    invoke-interface {p1, v1, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1848167
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/throwback/ThrowbackFeedFooterPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1848168
    return-object v4
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x63e409b3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1848172
    check-cast p4, LX/3ZK;

    .line 1848173
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, LX/3ZK;->setHasBottomDivider(Z)V

    .line 1848174
    const/4 v1, 0x1

    invoke-virtual {p4, v1}, LX/3ZK;->setHasButton(Z)V

    .line 1848175
    const/16 v1, 0x1f

    const v2, -0x18da25df

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1848169
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1848170
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1848171
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/214;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    return v0
.end method
