.class public Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedHeaderPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedBannerPartDefinition;

.field private final b:Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitlePartDefinition;

.field private final c:Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBirthdayTitlePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedBannerPartDefinition;Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitlePartDefinition;Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBirthdayTitlePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1848054
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1848055
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedHeaderPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedBannerPartDefinition;

    .line 1848056
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedHeaderPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitlePartDefinition;

    .line 1848057
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedHeaderPartDefinition;->c:Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBirthdayTitlePartDefinition;

    .line 1848058
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedHeaderPartDefinition;
    .locals 6

    .prologue
    .line 1848035
    const-class v1, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedHeaderPartDefinition;

    monitor-enter v1

    .line 1848036
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedHeaderPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1848037
    sput-object v2, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedHeaderPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1848038
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1848039
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1848040
    new-instance p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedBannerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedBannerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedBannerPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitlePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBirthdayTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBirthdayTitlePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBirthdayTitlePartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedHeaderPartDefinition;-><init>(Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedBannerPartDefinition;Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitlePartDefinition;Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBirthdayTitlePartDefinition;)V

    .line 1848041
    move-object v0, p0

    .line 1848042
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1848043
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1848044
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1848045
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1848050
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1848051
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedHeaderPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedBannerPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1848052
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedHeaderPartDefinition;->c:Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBirthdayTitlePartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedHeaderPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitlePartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 1848053
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 1848046
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1848047
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1848048
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 1848049
    const-string v1, "single_photo_ipb"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "instagram_photo"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "birthday_ipb"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "status_update_ipb"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "link_ipb"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "video"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "reshare_photo"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "reshare_status_update"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "reshare_video"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "reshare_multi_photo"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "multi_photo"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
