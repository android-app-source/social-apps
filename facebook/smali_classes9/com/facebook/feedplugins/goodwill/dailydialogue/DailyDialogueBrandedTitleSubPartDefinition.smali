.class public Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitleSubPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:LX/1Ua;

.field private static e:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:LX/1Uf;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1848059
    invoke-static {}, LX/1UY;->f()LX/1UY;

    move-result-object v0

    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitleSubPartDefinition;->a:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1Uf;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1848060
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1848061
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitleSubPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 1848062
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitleSubPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 1848063
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitleSubPartDefinition;->d:LX/1Uf;

    .line 1848064
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitleSubPartDefinition;
    .locals 6

    .prologue
    .line 1848065
    const-class v1, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitleSubPartDefinition;

    monitor-enter v1

    .line 1848066
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitleSubPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1848067
    sput-object v2, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitleSubPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1848068
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1848069
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1848070
    new-instance p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitleSubPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v5

    check-cast v5, LX/1Uf;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitleSubPartDefinition;-><init>(Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1Uf;)V

    .line 1848071
    move-object v0, p0

    .line 1848072
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1848073
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitleSubPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1848074
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1848075
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1848076
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v6, 0x0

    .line 1848077
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1848078
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 1848079
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitleSubPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitleSubPartDefinition;->a:LX/1Ua;

    const v4, 0x7f0207fa

    const/4 v5, -0x1

    invoke-direct {v2, p2, v3, v4, v5}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1848080
    const v2, 0x7f0d0bdc

    iget-object v3, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitleSubPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-interface {p1, v2, v3, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1848081
    const v1, 0x7f0d0bdd

    iget-object v2, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitleSubPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedTitleSubPartDefinition;->d:LX/1Uf;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {v0}, LX/1eD;->c(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/1eE;

    move-result-object v0

    const/4 v4, 0x1

    invoke-virtual {v3, v0, v4, v6}, LX/1Uf;->a(LX/1eE;ZLX/0lF;)Landroid/text/Spannable;

    move-result-object v0

    :goto_1
    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1848082
    return-object v6

    .line 1848083
    :cond_0
    const-string v1, ""

    goto :goto_0

    .line 1848084
    :cond_1
    const-string v0, ""

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x563c8724

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1848085
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, Ljava/lang/Void;

    check-cast p3, LX/1Ps;

    .line 1848086
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;->a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V

    .line 1848087
    const-string v2, "birthday_ipb"

    .line 1848088
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1848089
    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 1848090
    if-eqz v1, :cond_0

    .line 1848091
    const v1, 0x7f0d0bdc

    invoke-virtual {p4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1848092
    const/high16 v2, 0x41800000    # 16.0f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 1848093
    const v1, 0x7f0d0bdd

    invoke-virtual {p4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1848094
    const/high16 v2, 0x41600000    # 14.0f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 1848095
    :cond_0
    const/16 v1, 0x1f

    const v2, 0x94a56b8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
