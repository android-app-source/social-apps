.class public Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pe;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static e:LX/0Xm;


# instance fields
.field public final b:LX/1nu;

.field public final c:LX/1DR;

.field public final d:LX/C4p;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1847854
    const-class v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentSpec;

    const-string v1, "daily_dialogue"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/C4p;LX/1nu;LX/1DR;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1847855
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1847856
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentSpec;->d:LX/C4p;

    .line 1847857
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentSpec;->b:LX/1nu;

    .line 1847858
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentSpec;->c:LX/1DR;

    .line 1847859
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentSpec;
    .locals 6

    .prologue
    .line 1847860
    const-class v1, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentSpec;

    monitor-enter v1

    .line 1847861
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentSpec;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1847862
    sput-object v2, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentSpec;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1847863
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1847864
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1847865
    new-instance p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentSpec;

    invoke-static {v0}, LX/C4p;->a(LX/0QB;)LX/C4p;

    move-result-object v3

    check-cast v3, LX/C4p;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v4

    check-cast v4, LX/1nu;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v5

    check-cast v5, LX/1DR;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentSpec;-><init>(LX/C4p;LX/1nu;LX/1DR;)V

    .line 1847866
    move-object v0, p0

    .line 1847867
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1847868
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1847869
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1847870
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ps;)LX/1Dg;
    .locals 9
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1Ps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1847871
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1847872
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1847873
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-static {v1}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v1

    .line 1847874
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    if-eqz v1, :cond_1

    .line 1847875
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentSpec;->d:LX/C4p;

    const/4 v1, 0x0

    .line 1847876
    new-instance v3, LX/C4o;

    invoke-direct {v3, v0}, LX/C4o;-><init>(LX/C4p;)V

    .line 1847877
    iget-object p0, v0, LX/C4p;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/C4n;

    .line 1847878
    if-nez p0, :cond_0

    .line 1847879
    new-instance p0, LX/C4n;

    invoke-direct {p0, v0}, LX/C4n;-><init>(LX/C4p;)V

    .line 1847880
    :cond_0
    invoke-static {p0, p1, v1, v1, v3}, LX/C4n;->a$redex0(LX/C4n;LX/1De;IILX/C4o;)V

    .line 1847881
    move-object v3, p0

    .line 1847882
    move-object v1, v3

    .line 1847883
    move-object v0, v1

    .line 1847884
    iget-object v1, v0, LX/C4n;->a:LX/C4o;

    iput-object p2, v1, LX/C4o;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1847885
    iget-object v1, v0, LX/C4n;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    .line 1847886
    move-object v0, v0

    .line 1847887
    check-cast p3, LX/1Pe;

    .line 1847888
    iget-object v1, v0, LX/C4n;->a:LX/C4o;

    iput-object p3, v1, LX/C4o;->b:LX/1Pe;

    .line 1847889
    iget-object v1, v0, LX/C4n;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    .line 1847890
    move-object v0, v0

    .line 1847891
    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    move-object v0, v0

    .line 1847892
    :goto_0
    invoke-interface {v2, v0}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0

    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 1847893
    iget-object v3, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentSpec;->b:LX/1nu;

    invoke-virtual {v3, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v3

    sget-object v4, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v4}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentSpec;->c:LX/1DR;

    invoke-virtual {v4}, LX/1DR;->a()I

    move-result v4

    invoke-interface {v3, v4}, LX/1Di;->g(I)LX/1Di;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentSpec;->c:LX/1DR;

    invoke-virtual {v4}, LX/1DR;->a()I

    move-result v4

    .line 1847894
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v5

    int-to-double v5, v5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v7

    int-to-double v7, v7

    div-double/2addr v5, v7

    .line 1847895
    int-to-double v7, v4

    mul-double/2addr v5, v7

    double-to-int v5, v5

    move v4, v5

    .line 1847896
    invoke-interface {v3, v4}, LX/1Di;->o(I)LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1847897
    goto :goto_0
.end method
