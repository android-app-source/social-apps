.class public Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentFbVideoView;
.super LX/2oW;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1847791
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentFbVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1847792
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1847789
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentFbVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1847790
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1847785
    invoke-direct {p0, p1, p2, p3}, LX/2oW;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1847786
    const/4 v0, 0x1

    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 1847787
    sget-object v0, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setOriginalPlayReason(LX/04g;)V

    .line 1847788
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "LX/0Px",
            "<+",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1847784
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;D)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 1847755
    new-instance v0, LX/2oE;

    invoke-direct {v0}, LX/2oE;-><init>()V

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1847756
    iput-object v1, v0, LX/2oE;->a:Landroid/net/Uri;

    .line 1847757
    move-object v0, v0

    .line 1847758
    sget-object v1, LX/097;->FROM_STREAM:LX/097;

    .line 1847759
    iput-object v1, v0, LX/2oE;->e:LX/097;

    .line 1847760
    move-object v0, v0

    .line 1847761
    invoke-virtual {v0}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v0

    .line 1847762
    new-instance v1, LX/2oH;

    invoke-direct {v1}, LX/2oH;-><init>()V

    invoke-virtual {v1, v0}, LX/2oH;->a(Lcom/facebook/video/engine/VideoDataSource;)LX/2oH;

    move-result-object v0

    .line 1847763
    iput-boolean v2, v0, LX/2oH;->o:Z

    .line 1847764
    move-object v0, v0

    .line 1847765
    iput-boolean v2, v0, LX/2oH;->g:Z

    .line 1847766
    move-object v0, v0

    .line 1847767
    iput-object p2, v0, LX/2oH;->b:Ljava/lang/String;

    .line 1847768
    move-object v0, v0

    .line 1847769
    invoke-virtual {v0}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v0

    .line 1847770
    new-instance v1, LX/2pZ;

    invoke-direct {v1}, LX/2pZ;-><init>()V

    .line 1847771
    iput-object v0, v1, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1847772
    move-object v0, v1

    .line 1847773
    iput-wide p3, v0, LX/2pZ;->e:D

    .line 1847774
    move-object v0, v0

    .line 1847775
    invoke-virtual {v0}, LX/2pZ;->b()LX/2pa;

    move-result-object v0

    .line 1847776
    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 1847777
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1847782
    sget-object v0, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 1847783
    return-void
.end method

.method public getDefaultPlayerOrigin()LX/04D;
    .locals 1

    .prologue
    .line 1847781
    sget-object v0, LX/04D;->CULTURAL_MOMENTS_SHARE:LX/04D;

    return-object v0
.end method

.method public getDefaultPlayerType()LX/04G;
    .locals 1

    .prologue
    .line 1847780
    sget-object v0, LX/04G;->INLINE_PLAYER:LX/04G;

    return-object v0
.end method

.method public final jj_()V
    .locals 1

    .prologue
    .line 1847778
    sget-object v0, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 1847779
    return-void
.end method
