.class public Lcom/facebook/feedplugins/richtextpicker/RichTextPickerStyleComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static e:LX/0Xm;


# instance fields
.field public final b:LX/1Ad;

.field private final c:LX/1Uo;

.field public final d:LX/CC2;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1857422
    const-class v0, Lcom/facebook/feedplugins/richtextpicker/RichTextPickerStyleComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/richtextpicker/RichTextPickerStyleComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1Ad;LX/1Uo;LX/CC2;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1857423
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1857424
    iput-object p1, p0, Lcom/facebook/feedplugins/richtextpicker/RichTextPickerStyleComponentSpec;->b:LX/1Ad;

    .line 1857425
    iput-object p2, p0, Lcom/facebook/feedplugins/richtextpicker/RichTextPickerStyleComponentSpec;->c:LX/1Uo;

    .line 1857426
    iput-object p3, p0, Lcom/facebook/feedplugins/richtextpicker/RichTextPickerStyleComponentSpec;->d:LX/CC2;

    .line 1857427
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/richtextpicker/RichTextPickerStyleComponentSpec;
    .locals 6

    .prologue
    .line 1857428
    const-class v1, Lcom/facebook/feedplugins/richtextpicker/RichTextPickerStyleComponentSpec;

    monitor-enter v1

    .line 1857429
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/richtextpicker/RichTextPickerStyleComponentSpec;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1857430
    sput-object v2, Lcom/facebook/feedplugins/richtextpicker/RichTextPickerStyleComponentSpec;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1857431
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1857432
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1857433
    new-instance p0, Lcom/facebook/feedplugins/richtextpicker/RichTextPickerStyleComponentSpec;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v3

    check-cast v3, LX/1Ad;

    invoke-static {v0}, LX/1qZ;->b(LX/0QB;)LX/1Uo;

    move-result-object v4

    check-cast v4, LX/1Uo;

    invoke-static {v0}, LX/CC2;->b(LX/0QB;)LX/CC2;

    move-result-object v5

    check-cast v5, LX/CC2;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/richtextpicker/RichTextPickerStyleComponentSpec;-><init>(LX/1Ad;LX/1Uo;LX/CC2;)V

    .line 1857434
    move-object v0, p0

    .line 1857435
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1857436
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/richtextpicker/RichTextPickerStyleComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1857437
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1857438
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
