.class public final Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x560ba062
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1857771
    const-class v0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1857770
    const-class v0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1857768
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1857769
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1857765
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1857766
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1857767
    return-void
.end method

.method private a(Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;)V
    .locals 3
    .param p1    # Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1857759
    iput-object p1, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel;->f:Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;

    .line 1857760
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1857761
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1857762
    if-eqz v0, :cond_0

    .line 1857763
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 1857764
    :cond_0
    return-void
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1857757
    iget-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel;->g:Ljava/lang/String;

    .line 1857758
    iget-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1857747
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1857748
    invoke-virtual {p0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1857749
    invoke-virtual {p0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel;->k()Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1857750
    invoke-direct {p0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1857751
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1857752
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1857753
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1857754
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1857755
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1857756
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1857739
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1857740
    invoke-virtual {p0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel;->k()Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1857741
    invoke-virtual {p0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel;->k()Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;

    .line 1857742
    invoke-virtual {p0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel;->k()Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1857743
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel;

    .line 1857744
    iput-object v0, v1, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel;->f:Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;

    .line 1857745
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1857746
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1857772
    new-instance v0, LX/CCL;

    invoke-direct {v0, p1}, LX/CCL;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1857738
    invoke-virtual {p0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1857736
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1857737
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1857733
    const-string v0, "text_format_metadata"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1857734
    check-cast p2, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;

    invoke-direct {p0, p2}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel;->a(Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;)V

    .line 1857735
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1857732
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1857729
    new-instance v0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel;

    invoke-direct {v0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel;-><init>()V

    .line 1857730
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1857731
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1857728
    const v0, 0x30f08c4e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1857727
    const v0, 0x4c808d5

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1857725
    iget-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel;->e:Ljava/lang/String;

    .line 1857726
    iget-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1857723
    iget-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel;->f:Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;

    iput-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel;->f:Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;

    .line 1857724
    iget-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel;->f:Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;

    return-object v0
.end method
