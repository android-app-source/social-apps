.class public final Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5b5b96c0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel$BackgroundImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1857679
    const-class v0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1857680
    const-class v0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1857681
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1857682
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1857683
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1857684
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1857685
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 11

    .prologue
    .line 1857686
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1857687
    invoke-virtual {p0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1857688
    invoke-virtual {p0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1857689
    invoke-virtual {p0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1857690
    invoke-virtual {p0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->l()Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel$BackgroundImageModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1857691
    invoke-virtual {p0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1857692
    invoke-virtual {p0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->n()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1857693
    invoke-virtual {p0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1857694
    invoke-virtual {p0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->p()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1857695
    invoke-virtual {p0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->q()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1857696
    invoke-virtual {p0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->r()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1857697
    const/16 v10, 0xa

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1857698
    const/4 v10, 0x0

    invoke-virtual {p1, v10, v0}, LX/186;->b(II)V

    .line 1857699
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1857700
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1857701
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1857702
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1857703
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1857704
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1857705
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1857706
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1857707
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1857708
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1857709
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1857710
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1857711
    invoke-virtual {p0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->l()Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel$BackgroundImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1857712
    invoke-virtual {p0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->l()Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel$BackgroundImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel$BackgroundImageModel;

    .line 1857713
    invoke-virtual {p0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->l()Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel$BackgroundImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1857714
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;

    .line 1857715
    iput-object v0, v1, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->h:Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel$BackgroundImageModel;

    .line 1857716
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1857717
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1857718
    iget-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->e:Ljava/lang/String;

    .line 1857719
    iget-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1857720
    new-instance v0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;

    invoke-direct {v0}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;-><init>()V

    .line 1857721
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1857722
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1857677
    const v0, 0x7614f619

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1857678
    const v0, 0x6e7daaf3

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1857675
    iget-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->f:Ljava/lang/String;

    .line 1857676
    iget-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1857673
    iget-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->g:Ljava/lang/String;

    .line 1857674
    iget-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel$BackgroundImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1857671
    iget-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->h:Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel$BackgroundImageModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel$BackgroundImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel$BackgroundImageModel;

    iput-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->h:Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel$BackgroundImageModel;

    .line 1857672
    iget-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->h:Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel$BackgroundImageModel;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1857669
    iget-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->i:Ljava/lang/String;

    .line 1857670
    iget-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1857667
    iget-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->j:Ljava/lang/String;

    .line 1857668
    iget-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1857665
    iget-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->k:Ljava/lang/String;

    .line 1857666
    iget-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1857663
    iget-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->l:Ljava/lang/String;

    .line 1857664
    iget-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1857661
    iget-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->m:Ljava/lang/String;

    .line 1857662
    iget-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1857659
    iget-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->n:Ljava/lang/String;

    .line 1857660
    iget-object v0, p0, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;->n:Ljava/lang/String;

    return-object v0
.end method
