.class public final Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherProcessor$2;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/auth/viewercontext/ViewerContext;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3iF;


# direct methods
.method public constructor <init>(LX/3iF;)V
    .locals 0

    .prologue
    .line 1860072
    iput-object p1, p0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherProcessor$2;->a:LX/3iF;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCancel(Ljava/util/concurrent/CancellationException;)V
    .locals 1

    .prologue
    .line 1860073
    invoke-super {p0, p1}, LX/0Vd;->onCancel(Ljava/util/concurrent/CancellationException;)V

    .line 1860074
    iget-object v0, p0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherProcessor$2;->a:LX/3iF;

    invoke-static {v0, p1}, LX/3iF;->a$redex0(LX/3iF;Ljava/lang/Throwable;)V

    .line 1860075
    return-void
.end method

.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1860076
    iget-object v0, p0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherProcessor$2;->a:LX/3iF;

    invoke-static {v0, p1}, LX/3iF;->a$redex0(LX/3iF;Ljava/lang/Throwable;)V

    .line 1860077
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 1860078
    check-cast p1, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1860079
    if-nez p1, :cond_0

    .line 1860080
    iget-object v0, p0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherProcessor$2;->a:LX/3iF;

    iget-object v0, v0, LX/3iF;->i:LX/03V;

    sget-object v1, LX/3iF;->a:Ljava/lang/String;

    const-string v2, "Fetched ViewerContext is null!"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1860081
    iget-object v0, p0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherProcessor$2;->a:LX/3iF;

    .line 1860082
    invoke-static {v0}, LX/3iF;->e$redex0(LX/3iF;)V

    .line 1860083
    :goto_0
    return-void

    .line 1860084
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/voiceswitcher/VoiceSwitcherProcessor$2;->a:LX/3iF;

    .line 1860085
    iget-object v3, v0, LX/3iF;->m:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v3}, LX/3iF;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1860086
    iget-object v3, v0, LX/3iF;->i:LX/03V;

    sget-object v4, LX/3iF;->a:Ljava/lang/String;

    const-string v5, "Current feedback id is Empty or null!"

    invoke-virtual {v3, v4, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1860087
    invoke-static {v0}, LX/3iF;->e$redex0(LX/3iF;)V

    .line 1860088
    :goto_1
    goto :goto_0

    .line 1860089
    :cond_1
    iget-object v3, v0, LX/3iF;->b:LX/3iK;

    iget-object v4, v0, LX/3iF;->m:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1860090
    iget-object v5, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v5

    .line 1860091
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v4

    const-class v5, LX/3iF;

    invoke-static {v5}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v6

    const/4 v7, 0x1

    new-instance v8, LX/CDm;

    invoke-direct {v8, v0, p1}, LX/CDm;-><init>(LX/3iF;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    move-object v5, p1

    invoke-virtual/range {v3 .. v8}, LX/3iK;->a(Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/common/callercontext/CallerContext;ZLX/451;)V

    goto :goto_1
.end method
