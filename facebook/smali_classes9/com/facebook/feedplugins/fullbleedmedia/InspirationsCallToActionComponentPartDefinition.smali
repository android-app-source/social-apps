.class public Lcom/facebook/feedplugins/fullbleedmedia/InspirationsCallToActionComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/C4b;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/C4X;

.field private final e:LX/C4a;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/C4a;LX/C4X;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1847671
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1847672
    iput-object p2, p0, Lcom/facebook/feedplugins/fullbleedmedia/InspirationsCallToActionComponentPartDefinition;->e:LX/C4a;

    .line 1847673
    iput-object p3, p0, Lcom/facebook/feedplugins/fullbleedmedia/InspirationsCallToActionComponentPartDefinition;->d:LX/C4X;

    .line 1847674
    return-void
.end method

.method private a(LX/1De;LX/C4b;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/C4b;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1847652
    iget-object v0, p0, Lcom/facebook/feedplugins/fullbleedmedia/InspirationsCallToActionComponentPartDefinition;->e:LX/C4a;

    const/4 v1, 0x0

    .line 1847653
    new-instance v2, LX/C4Z;

    invoke-direct {v2, v0}, LX/C4Z;-><init>(LX/C4a;)V

    .line 1847654
    sget-object v3, LX/C4a;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/C4Y;

    .line 1847655
    if-nez v3, :cond_0

    .line 1847656
    new-instance v3, LX/C4Y;

    invoke-direct {v3}, LX/C4Y;-><init>()V

    .line 1847657
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/C4Y;->a$redex0(LX/C4Y;LX/1De;IILX/C4Z;)V

    .line 1847658
    move-object v2, v3

    .line 1847659
    move-object v1, v2

    .line 1847660
    move-object v0, v1

    .line 1847661
    iget-object v1, p2, LX/C4b;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1847662
    iget-object v2, v0, LX/C4Y;->a:LX/C4Z;

    iput-object v1, v2, LX/C4Z;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1847663
    iget-object v2, v0, LX/C4Y;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1847664
    move-object v1, v0

    .line 1847665
    iget-object v2, p0, Lcom/facebook/feedplugins/fullbleedmedia/InspirationsCallToActionComponentPartDefinition;->d:LX/C4X;

    iget-object v0, p2, LX/C4b;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1847666
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 1847667
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/C4X;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/0Px;

    move-result-object v0

    .line 1847668
    iget-object v2, v1, LX/C4Y;->a:LX/C4Z;

    iput-object v0, v2, LX/C4Z;->b:LX/0Px;

    .line 1847669
    move-object v0, v1

    .line 1847670
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/fullbleedmedia/InspirationsCallToActionComponentPartDefinition;
    .locals 6

    .prologue
    .line 1847635
    const-class v1, Lcom/facebook/feedplugins/fullbleedmedia/InspirationsCallToActionComponentPartDefinition;

    monitor-enter v1

    .line 1847636
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/fullbleedmedia/InspirationsCallToActionComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1847637
    sput-object v2, Lcom/facebook/feedplugins/fullbleedmedia/InspirationsCallToActionComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1847638
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1847639
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1847640
    new-instance p0, Lcom/facebook/feedplugins/fullbleedmedia/InspirationsCallToActionComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/C4a;->a(LX/0QB;)LX/C4a;

    move-result-object v4

    check-cast v4, LX/C4a;

    invoke-static {v0}, LX/C4X;->b(LX/0QB;)LX/C4X;

    move-result-object v5

    check-cast v5, LX/C4X;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/fullbleedmedia/InspirationsCallToActionComponentPartDefinition;-><init>(Landroid/content/Context;LX/C4a;LX/C4X;)V

    .line 1847641
    move-object v0, p0

    .line 1847642
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1847643
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/fullbleedmedia/InspirationsCallToActionComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1847644
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1847645
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1847651
    check-cast p2, LX/C4b;

    invoke-direct {p0, p1, p2}, Lcom/facebook/feedplugins/fullbleedmedia/InspirationsCallToActionComponentPartDefinition;->a(LX/1De;LX/C4b;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1847650
    check-cast p2, LX/C4b;

    invoke-direct {p0, p1, p2}, Lcom/facebook/feedplugins/fullbleedmedia/InspirationsCallToActionComponentPartDefinition;->a(LX/1De;LX/C4b;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 1847648
    check-cast p1, LX/C4b;

    .line 1847649
    iget-object v0, p0, Lcom/facebook/feedplugins/fullbleedmedia/InspirationsCallToActionComponentPartDefinition;->d:LX/C4X;

    iget-object v1, p1, LX/C4b;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p1, LX/C4b;->b:LX/1PT;

    iget-object v3, p1, LX/C4b;->c:Landroid/content/Context;

    invoke-virtual {v0, v1, v2, v3}, LX/C4X;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1847646
    check-cast p1, LX/C4b;

    .line 1847647
    iget-object v0, p1, LX/C4b;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
