.class public Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pe;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static final b:Ljava/lang/String;

.field private static n:LX/0Xm;


# instance fields
.field public final c:LX/1xN;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1xN",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final d:LX/361;

.field public final e:LX/1VD;

.field public final f:LX/C4M;

.field private final g:LX/03V;

.field public final h:LX/2mn;

.field public final i:LX/2mq;

.field private final j:LX/0qn;

.field private final k:LX/C9E;

.field public final l:LX/1nu;

.field public final m:LX/1qa;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1847396
    const-class v0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->b:Ljava/lang/String;

    .line 1847397
    const-class v0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1xN;LX/361;LX/1nu;LX/1VD;LX/C4M;LX/03V;LX/2mn;LX/2mq;LX/0qn;LX/C9E;LX/1qa;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1847398
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1847399
    iput-object p1, p0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->c:LX/1xN;

    .line 1847400
    iput-object p2, p0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->d:LX/361;

    .line 1847401
    iput-object p3, p0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->l:LX/1nu;

    .line 1847402
    iput-object p5, p0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->f:LX/C4M;

    .line 1847403
    iput-object p4, p0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->e:LX/1VD;

    .line 1847404
    iput-object p6, p0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->g:LX/03V;

    .line 1847405
    iput-object p7, p0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->h:LX/2mn;

    .line 1847406
    iput-object p8, p0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->i:LX/2mq;

    .line 1847407
    iput-object p9, p0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->j:LX/0qn;

    .line 1847408
    iput-object p10, p0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->k:LX/C9E;

    .line 1847409
    iput-object p11, p0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->m:LX/1qa;

    .line 1847410
    return-void
.end method

.method public static a(Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1Di;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1Di;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1847411
    invoke-static {p2}, LX/182;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 1847412
    if-eqz v3, :cond_0

    .line 1847413
    iget-object v0, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1847414
    if-nez v0, :cond_1

    :cond_0
    move-object v0, v2

    .line 1847415
    :goto_0
    return-object v0

    .line 1847416
    :cond_1
    iget-object v0, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1847417
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v4

    .line 1847418
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1847419
    sget-object v5, LX/C4W;->a:[I

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ordinal()I

    move-result v0

    aget v0, v5, v0

    packed-switch v0, :pswitch_data_0

    .line 1847420
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1847421
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->c:LX/1xN;

    invoke-virtual {v0, p1}, LX/1xN;->c(LX/1De;)LX/22O;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/22O;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/22O;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/22O;->a(LX/1Pb;)LX/22O;

    move-result-object v0

    sget-object v1, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/22O;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/22O;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    move-object v0, v0

    .line 1847422
    goto :goto_0

    .line 1847423
    :pswitch_1
    invoke-static {p0, p2}, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->a(Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1847424
    iget-object v1, p0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->m:LX/1qa;

    .line 1847425
    iget-object v0, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1847426
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    sget-object v2, LX/26P;->Video:LX/26P;

    invoke-virtual {v1, v0, v2}, LX/1qa;->b(Lcom/facebook/graphql/model/GraphQLMedia;LX/26P;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 1847427
    iget-object v1, p0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->l:LX/1nu;

    invoke-virtual {v1, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "file://"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v0

    sget-object v1, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v0

    check-cast p3, LX/1Pp;

    invoke-virtual {v0, p3}, LX/1nw;->a(LX/1Pp;)LX/1nw;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    move-object v0, v0

    .line 1847428
    goto/16 :goto_0

    .line 1847429
    :cond_2
    iget-object v1, p0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->i:LX/2mq;

    .line 1847430
    iget-object v0, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1847431
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1, v0}, LX/2mq;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)F

    move-result v0

    .line 1847432
    iget-object v1, p0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->h:LX/2mn;

    invoke-virtual {v1, v3, v0}, LX/2mn;->c(Lcom/facebook/feed/rows/core/props/FeedProps;F)LX/3FO;

    move-result-object v0

    .line 1847433
    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 1847434
    new-instance v2, LX/3EE;

    const/4 v4, -0x1

    new-instance v5, LX/CDf;

    iget v6, v0, LX/3FO;->c:I

    iget v7, v0, LX/3FO;->d:I

    const/4 p2, 0x0

    invoke-direct {v5, v6, v7, p2}, LX/CDf;-><init>(III)V

    invoke-static {v5}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5, v1}, LX/3EE;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;ILX/0am;Ljava/util/concurrent/atomic/AtomicReference;)V

    .line 1847435
    iget-object v1, p0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->d:LX/361;

    invoke-virtual {v1, p1}, LX/361;->c(LX/1De;)LX/CDK;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/CDK;->a(LX/3EE;)LX/CDK;

    move-result-object v1

    check-cast p3, LX/1Pe;

    invoke-virtual {v1, p3}, LX/CDK;->a(LX/1Pe;)LX/CDK;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    iget v0, v0, LX/3FO;->b:I

    invoke-interface {v1, v0}, LX/1Di;->o(I)LX/1Di;

    move-result-object v0

    move-object v0, v0

    .line 1847436
    goto/16 :goto_0

    .line 1847437
    :cond_3
    iget-object v0, p0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->g:LX/03V;

    sget-object v1, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->b:Ljava/lang/String;

    const-string v3, "Unsupported media type"

    invoke-virtual {v0, v1, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    .line 1847438
    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;
    .locals 15

    .prologue
    .line 1847439
    const-class v1, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;

    monitor-enter v1

    .line 1847440
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->n:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1847441
    sput-object v2, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->n:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1847442
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1847443
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1847444
    new-instance v3, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;

    invoke-static {v0}, LX/1xN;->a(LX/0QB;)LX/1xN;

    move-result-object v4

    check-cast v4, LX/1xN;

    invoke-static {v0}, LX/361;->a(LX/0QB;)LX/361;

    move-result-object v5

    check-cast v5, LX/361;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v6

    check-cast v6, LX/1nu;

    invoke-static {v0}, LX/1VD;->a(LX/0QB;)LX/1VD;

    move-result-object v7

    check-cast v7, LX/1VD;

    invoke-static {v0}, LX/C4M;->a(LX/0QB;)LX/C4M;

    move-result-object v8

    check-cast v8, LX/C4M;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static {v0}, LX/2mn;->a(LX/0QB;)LX/2mn;

    move-result-object v10

    check-cast v10, LX/2mn;

    invoke-static {v0}, LX/2mq;->a(LX/0QB;)LX/2mq;

    move-result-object v11

    check-cast v11, LX/2mq;

    invoke-static {v0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v12

    check-cast v12, LX/0qn;

    invoke-static {v0}, LX/C9E;->b(LX/0QB;)LX/C9E;

    move-result-object v13

    check-cast v13, LX/C9E;

    invoke-static {v0}, LX/1qa;->a(LX/0QB;)LX/1qa;

    move-result-object v14

    check-cast v14, LX/1qa;

    invoke-direct/range {v3 .. v14}, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;-><init>(LX/1xN;LX/361;LX/1nu;LX/1VD;LX/C4M;LX/03V;LX/2mn;LX/2mq;LX/0qn;LX/C9E;LX/1qa;)V

    .line 1847445
    move-object v0, v3

    .line 1847446
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1847447
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1847448
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1847449
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/1Dh;I)V
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1847450
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 1847451
    :goto_0
    return-void

    .line 1847452
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 1847453
    :pswitch_0
    const/4 v0, 0x7

    :goto_1
    move v0, v0

    .line 1847454
    const v1, 0x7f0b00d5

    invoke-interface {p0, v0, v1}, LX/1Dh;->u(II)LX/1Dh;

    goto :goto_0

    .line 1847455
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_1

    .line 1847456
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1847457
    iget-object v1, p0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->j:LX/0qn;

    .line 1847458
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1847459
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v0}, LX/0qn;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedPhotoStoryComponentSpec;->k:LX/C9E;

    .line 1847460
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1847461
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v0}, LX/C9E;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1847462
    if-eq p0, v0, :cond_0

    const/4 v1, 0x2

    if-ne p0, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(I)Z
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 1847463
    if-eq p0, v0, :cond_0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
