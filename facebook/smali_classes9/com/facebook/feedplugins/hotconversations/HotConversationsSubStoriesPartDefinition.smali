.class public Lcom/facebook/feedplugins/hotconversations/HotConversationsSubStoriesPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/hotconversations/HotConversationSubStoryPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/hotconversations/HotConversationSubStoryPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1852968
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1852969
    iput-object p1, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationsSubStoriesPartDefinition;->a:Lcom/facebook/feedplugins/hotconversations/HotConversationSubStoryPartDefinition;

    .line 1852970
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/hotconversations/HotConversationsSubStoriesPartDefinition;
    .locals 4

    .prologue
    .line 1852957
    const-class v1, Lcom/facebook/feedplugins/hotconversations/HotConversationsSubStoriesPartDefinition;

    monitor-enter v1

    .line 1852958
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/hotconversations/HotConversationsSubStoriesPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1852959
    sput-object v2, Lcom/facebook/feedplugins/hotconversations/HotConversationsSubStoriesPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1852960
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1852961
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1852962
    new-instance p0, Lcom/facebook/feedplugins/hotconversations/HotConversationsSubStoriesPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/hotconversations/HotConversationSubStoryPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/hotconversations/HotConversationSubStoryPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/hotconversations/HotConversationSubStoryPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/hotconversations/HotConversationsSubStoriesPartDefinition;-><init>(Lcom/facebook/feedplugins/hotconversations/HotConversationSubStoryPartDefinition;)V

    .line 1852963
    move-object v0, p0

    .line 1852964
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1852965
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/hotconversations/HotConversationsSubStoriesPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1852966
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1852967
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 1852943
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v8, 0x0

    .line 1852944
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1852945
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1852946
    invoke-static {v0}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v2

    .line 1852947
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1852948
    invoke-virtual {p2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    .line 1852949
    iget-object v5, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationsSubStoriesPartDefinition;->a:Lcom/facebook/feedplugins/hotconversations/HotConversationSubStoryPartDefinition;

    new-instance v6, LX/C33;

    sget-object v7, LX/C34;->HOT_CONVERSATION:LX/C34;

    .line 1852950
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1852951
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v6, v8, v7, v4, v0}, LX/C33;-><init>(Landroid/view/View$OnClickListener;LX/C34;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)V

    invoke-virtual {p1, v5, v6}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1852952
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1852953
    :cond_0
    return-object v8
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1852954
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1852955
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1852956
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aP()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
