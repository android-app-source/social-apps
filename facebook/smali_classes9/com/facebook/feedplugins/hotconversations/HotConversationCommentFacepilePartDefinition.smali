.class public Lcom/facebook/feedplugins/hotconversations/HotConversationCommentFacepilePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/C33;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        "LX/C2i;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:LX/0ad;

.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:LX/C31;

.field private final d:Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;

.field private final e:Lcom/facebook/feedplugins/commentfacepile/CommentFacepileBarPartDefinition;


# direct methods
.method public constructor <init>(LX/0ad;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/C31;Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;Lcom/facebook/feedplugins/commentfacepile/CommentFacepileBarPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1852791
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1852792
    iput-object p1, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationCommentFacepilePartDefinition;->a:LX/0ad;

    .line 1852793
    iput-object p2, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationCommentFacepilePartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 1852794
    iput-object p3, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationCommentFacepilePartDefinition;->c:LX/C31;

    .line 1852795
    iput-object p4, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationCommentFacepilePartDefinition;->d:Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;

    .line 1852796
    iput-object p5, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationCommentFacepilePartDefinition;->e:Lcom/facebook/feedplugins/commentfacepile/CommentFacepileBarPartDefinition;

    .line 1852797
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/hotconversations/HotConversationCommentFacepilePartDefinition;
    .locals 13

    .prologue
    .line 1852774
    const-class v1, Lcom/facebook/feedplugins/hotconversations/HotConversationCommentFacepilePartDefinition;

    monitor-enter v1

    .line 1852775
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/hotconversations/HotConversationCommentFacepilePartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1852776
    sput-object v2, Lcom/facebook/feedplugins/hotconversations/HotConversationCommentFacepilePartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1852777
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1852778
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1852779
    new-instance v3, Lcom/facebook/feedplugins/hotconversations/HotConversationCommentFacepilePartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/C31;->b(LX/0QB;)LX/C31;

    move-result-object v6

    check-cast v6, LX/C31;

    invoke-static {v0}, Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;

    .line 1852780
    new-instance v11, Lcom/facebook/feedplugins/commentfacepile/CommentFacepileBarPartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    .line 1852781
    new-instance v12, LX/C2k;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v9

    check-cast v9, Landroid/content/res/Resources;

    invoke-static {v0}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v10

    check-cast v10, LX/154;

    invoke-direct {v12, v9, v10}, LX/C2k;-><init>(Landroid/content/res/Resources;LX/154;)V

    .line 1852782
    move-object v9, v12

    .line 1852783
    check-cast v9, LX/C2k;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v10

    check-cast v10, Landroid/content/res/Resources;

    invoke-direct {v11, v8, v9, v10}, Lcom/facebook/feedplugins/commentfacepile/CommentFacepileBarPartDefinition;-><init>(LX/0ad;LX/C2k;Landroid/content/res/Resources;)V

    .line 1852784
    move-object v8, v11

    .line 1852785
    check-cast v8, Lcom/facebook/feedplugins/commentfacepile/CommentFacepileBarPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/hotconversations/HotConversationCommentFacepilePartDefinition;-><init>(LX/0ad;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/C31;Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;Lcom/facebook/feedplugins/commentfacepile/CommentFacepileBarPartDefinition;)V

    .line 1852786
    move-object v0, v3

    .line 1852787
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1852788
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/hotconversations/HotConversationCommentFacepilePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1852789
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1852790
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/C2i;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1852773
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationCommentFacepilePartDefinition;->e:Lcom/facebook/feedplugins/commentfacepile/CommentFacepileBarPartDefinition;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/commentfacepile/CommentFacepileBarPartDefinition;->a()LX/1Cz;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1852766
    check-cast p2, LX/C33;

    .line 1852767
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationCommentFacepilePartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    iget-object v1, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationCommentFacepilePartDefinition;->c:LX/C31;

    iget-object v2, p2, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/C33;->b:LX/C34;

    invoke-virtual {v1, v2, v3}, LX/C31;->c(Lcom/facebook/feed/rows/core/props/FeedProps;LX/C34;)LX/1X6;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1852768
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationCommentFacepilePartDefinition;->d:Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1852769
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationCommentFacepilePartDefinition;->e:Lcom/facebook/feedplugins/commentfacepile/CommentFacepileBarPartDefinition;

    iget-object v1, p2, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1852770
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 1852771
    check-cast p1, LX/C33;

    const/4 v0, 0x0

    .line 1852772
    sget-object v1, LX/C34;->HOT_CONVERSATION:LX/C34;

    iget-object v2, p1, LX/C33;->b:LX/C34;

    invoke-virtual {v1, v2}, LX/C34;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationCommentFacepilePartDefinition;->a:LX/0ad;

    sget-short v2, LX/0fe;->C:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
