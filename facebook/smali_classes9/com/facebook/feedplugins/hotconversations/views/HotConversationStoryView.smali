.class public Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1852996
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1852997
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1852971
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1852972
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1852973
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1852974
    iput-object p1, p0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;->d:Landroid/content/Context;

    .line 1852975
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;->setOrientation(I)V

    .line 1852976
    const v0, 0x7f03089f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1852977
    const v0, 0x7f0d090e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;->a:Landroid/widget/TextView;

    .line 1852978
    const v0, 0x7f0d1647

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;->b:Landroid/widget/TextView;

    .line 1852979
    const v0, 0x7f0d1648

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;->c:Landroid/widget/TextView;

    .line 1852980
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;->a:Landroid/widget/TextView;

    invoke-static {}, LX/8tu;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1852981
    return-void
.end method


# virtual methods
.method public setHeaderText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1852982
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1852983
    return-void
.end method

.method public setHeaderTextAppearance(I)V
    .locals 2

    .prologue
    .line 1852984
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;->d:Landroid/content/Context;

    invoke-virtual {v0, v1, p1}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 1852985
    return-void
.end method

.method public setMaxMessageLines(I)V
    .locals 1

    .prologue
    .line 1852986
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 1852987
    return-void
.end method

.method public setMessageText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1852988
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1852989
    return-void
.end method

.method public setTimestampTextView(Ljava/lang/CharSequence;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1852990
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 1852991
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;->a:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;->a:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1852992
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1852993
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1852994
    :goto_0
    return-void

    .line 1852995
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;->b:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
