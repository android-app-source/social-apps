.class public Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private a:Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;

.field private b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1853030
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1853031
    invoke-direct {p0, p1}, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;->a(Landroid/content/Context;)V

    .line 1853032
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1853027
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1853028
    invoke-direct {p0, p1}, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;->a(Landroid/content/Context;)V

    .line 1853029
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1852998
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1852999
    invoke-direct {p0, p1}, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;->a(Landroid/content/Context;)V

    .line 1853000
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1853022
    const v0, 0x7f03089e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1853023
    const v0, 0x7f0d1644

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;

    iput-object v0, p0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;->a:Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;

    .line 1853024
    const v0, 0x7f0d1646

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1853025
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1d6f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;->c:I

    .line 1853026
    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 9

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    .line 1853014
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;->a:Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/FrameLayout$LayoutParams;

    .line 1853015
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/widget/FrameLayout$LayoutParams;

    .line 1853016
    iget v0, p0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;->c:I

    iget v1, v7, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v0, v1

    iget v1, v7, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int v3, v0, v1

    .line 1853017
    iget-object v1, p0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;->a:Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;

    const/4 v5, 0x0

    move-object v0, p0

    move v2, p1

    move v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 1853018
    iget v0, p0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;->c:I

    iget-object v1, p0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;->a:Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;

    invoke-virtual {v1}, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;->getMeasuredHeight()I

    move-result v1

    iget v2, v7, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v3, v7, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v2, v3

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1853019
    iget-object v1, p0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v1, v2, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->measure(II)V

    .line 1853020
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iget-object v1, p0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;->a:Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;

    invoke-virtual {v1}, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;->getPaddingTop()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    iget v2, v6, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v1, v2

    iget v2, v6, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v1, v2

    invoke-static {v1, p2}, Landroid/view/View;->resolveSize(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;->setMeasuredDimension(II)V

    .line 1853021
    return-void
.end method

.method public setStoryHeaderText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1853012
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;->a:Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;

    invoke-virtual {v0, p1}, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;->setHeaderText(Ljava/lang/CharSequence;)V

    .line 1853013
    return-void
.end method

.method public setStoryHeaderTextAppearance(I)V
    .locals 1

    .prologue
    .line 1853010
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;->a:Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;

    invoke-virtual {v0, p1}, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;->setHeaderTextAppearance(I)V

    .line 1853011
    return-void
.end method

.method public setStoryImageUri(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 1853008
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1853009
    return-void
.end method

.method public setStoryMessageText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1853006
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;->a:Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;

    invoke-virtual {v0, p1}, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;->setMessageText(Ljava/lang/CharSequence;)V

    .line 1853007
    return-void
.end method

.method public setStoryOverlayImage(LX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1853003
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {p1, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1853004
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {p0}, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020cce

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1af;->f(Landroid/graphics/drawable/Drawable;)V

    .line 1853005
    :cond_0
    return-void
.end method

.method public setTimestampText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1853001
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;->a:Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;

    invoke-virtual {v0, p1}, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;->setTimestampTextView(Ljava/lang/CharSequence;)V

    .line 1853002
    return-void
.end method
