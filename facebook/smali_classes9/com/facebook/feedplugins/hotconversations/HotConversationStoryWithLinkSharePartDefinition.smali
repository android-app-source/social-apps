.class public Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithLinkSharePartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/C33;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithBorderPartDefinition;

.field private final b:Lcom/facebook/feedplugins/hotconversations/HotConversationLinkSharePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithBorderPartDefinition;Lcom/facebook/feedplugins/hotconversations/HotConversationLinkSharePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1852899
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1852900
    iput-object p1, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithLinkSharePartDefinition;->a:Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithBorderPartDefinition;

    .line 1852901
    iput-object p2, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithLinkSharePartDefinition;->b:Lcom/facebook/feedplugins/hotconversations/HotConversationLinkSharePartDefinition;

    .line 1852902
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithLinkSharePartDefinition;
    .locals 5

    .prologue
    .line 1852903
    const-class v1, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithLinkSharePartDefinition;

    monitor-enter v1

    .line 1852904
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithLinkSharePartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1852905
    sput-object v2, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithLinkSharePartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1852906
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1852907
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1852908
    new-instance p0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithLinkSharePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithBorderPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithBorderPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithBorderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/hotconversations/HotConversationLinkSharePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/hotconversations/HotConversationLinkSharePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/hotconversations/HotConversationLinkSharePartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithLinkSharePartDefinition;-><init>(Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithBorderPartDefinition;Lcom/facebook/feedplugins/hotconversations/HotConversationLinkSharePartDefinition;)V

    .line 1852909
    move-object v0, p0

    .line 1852910
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1852911
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithLinkSharePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1852912
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1852913
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1852914
    check-cast p2, LX/C33;

    .line 1852915
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithLinkSharePartDefinition;->a:Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithBorderPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1852916
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithLinkSharePartDefinition;->b:Lcom/facebook/feedplugins/hotconversations/HotConversationLinkSharePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1852917
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1852918
    check-cast p1, LX/C33;

    .line 1852919
    invoke-static {p1}, LX/C3N;->b(LX/C33;)Z

    move-result v0

    return v0
.end method
