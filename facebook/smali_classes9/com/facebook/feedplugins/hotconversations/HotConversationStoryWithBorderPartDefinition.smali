.class public Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithBorderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/C33;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        "Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final b:Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;

.field private final c:Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1852894
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1852895
    iput-object p1, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithBorderPartDefinition;->b:Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;

    .line 1852896
    iput-object p2, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithBorderPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 1852897
    iput-object p3, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithBorderPartDefinition;->c:Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;

    .line 1852898
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithBorderPartDefinition;
    .locals 6

    .prologue
    .line 1852883
    const-class v1, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithBorderPartDefinition;

    monitor-enter v1

    .line 1852884
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithBorderPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1852885
    sput-object v2, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithBorderPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1852886
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1852887
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1852888
    new-instance p0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithBorderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithBorderPartDefinition;-><init>(Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;)V

    .line 1852889
    move-object v0, p0

    .line 1852890
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1852891
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithBorderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1852892
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1852893
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1852882
    sget-object v0, Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1852877
    check-cast p2, LX/C33;

    .line 1852878
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithBorderPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    iget-object v1, p2, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/C33;->b:LX/C34;

    invoke-static {v1, v2}, LX/C31;->b(Lcom/facebook/feed/rows/core/props/FeedProps;LX/C34;)LX/1X6;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1852879
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithBorderPartDefinition;->b:Lcom/facebook/feedplugins/hotconversations/BaseHotConversationPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1852880
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationStoryWithBorderPartDefinition;->c:Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1852881
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1852876
    const/4 v0, 0x1

    return v0
.end method
