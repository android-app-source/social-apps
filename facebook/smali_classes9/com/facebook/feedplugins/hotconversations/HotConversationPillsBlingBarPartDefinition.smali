.class public Lcom/facebook/feedplugins/hotconversations/HotConversationPillsBlingBarPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/C33;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        "Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final b:LX/C31;

.field private final c:LX/39G;

.field private final d:Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;


# direct methods
.method public constructor <init>(LX/39G;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/C31;Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1852856
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1852857
    iput-object p1, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationPillsBlingBarPartDefinition;->c:LX/39G;

    .line 1852858
    iput-object p2, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationPillsBlingBarPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 1852859
    iput-object p3, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationPillsBlingBarPartDefinition;->b:LX/C31;

    .line 1852860
    iput-object p4, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationPillsBlingBarPartDefinition;->d:Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;

    .line 1852861
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/hotconversations/HotConversationPillsBlingBarPartDefinition;
    .locals 7

    .prologue
    .line 1852837
    const-class v1, Lcom/facebook/feedplugins/hotconversations/HotConversationPillsBlingBarPartDefinition;

    monitor-enter v1

    .line 1852838
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/hotconversations/HotConversationPillsBlingBarPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1852839
    sput-object v2, Lcom/facebook/feedplugins/hotconversations/HotConversationPillsBlingBarPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1852840
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1852841
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1852842
    new-instance p0, Lcom/facebook/feedplugins/hotconversations/HotConversationPillsBlingBarPartDefinition;

    invoke-static {v0}, LX/39G;->a(LX/0QB;)LX/39G;

    move-result-object v3

    check-cast v3, LX/39G;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/C31;->b(LX/0QB;)LX/C31;

    move-result-object v5

    check-cast v5, LX/C31;

    invoke-static {v0}, Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/hotconversations/HotConversationPillsBlingBarPartDefinition;-><init>(LX/39G;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/C31;Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;)V

    .line 1852843
    move-object v0, p0

    .line 1852844
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1852845
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/hotconversations/HotConversationPillsBlingBarPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1852846
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1852847
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1852854
    sget-object v0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->a:LX/1Cz;

    move-object v0, v0

    .line 1852855
    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1852862
    check-cast p2, LX/C33;

    .line 1852863
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationPillsBlingBarPartDefinition;->d:Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1852864
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationPillsBlingBarPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    iget-object v1, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationPillsBlingBarPartDefinition;->b:LX/C31;

    iget-object v2, p2, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/C33;->b:LX/C34;

    invoke-virtual {v1, v2, v3}, LX/C31;->c(Lcom/facebook/feed/rows/core/props/FeedProps;LX/C34;)LX/1X6;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1852865
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0xe95f8fc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1852849
    check-cast p1, LX/C33;

    check-cast p4, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    .line 1852850
    iget-object v1, p1, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1852851
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v2

    .line 1852852
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2, p4}, LX/39G;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/Integer;Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;)V

    .line 1852853
    const/16 v1, 0x1f

    const v2, -0x2a10f8fa

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1852848
    const/4 v0, 0x1

    return v0
.end method
