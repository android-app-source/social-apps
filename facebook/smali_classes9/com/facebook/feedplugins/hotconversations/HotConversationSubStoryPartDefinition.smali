.class public Lcom/facebook/feedplugins/hotconversations/HotConversationSubStoryPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/C33;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/hotconversations/HotConversationStorySelector;

.field private final b:Lcom/facebook/feedplugins/hotconversations/HotConversationBlingBarSelector;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/hotconversations/HotConversationStorySelector;Lcom/facebook/feedplugins/hotconversations/HotConversationBlingBarSelector;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1852920
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1852921
    iput-object p1, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationSubStoryPartDefinition;->a:Lcom/facebook/feedplugins/hotconversations/HotConversationStorySelector;

    .line 1852922
    iput-object p2, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationSubStoryPartDefinition;->b:Lcom/facebook/feedplugins/hotconversations/HotConversationBlingBarSelector;

    .line 1852923
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/hotconversations/HotConversationSubStoryPartDefinition;
    .locals 8

    .prologue
    .line 1852924
    const-class v1, Lcom/facebook/feedplugins/hotconversations/HotConversationSubStoryPartDefinition;

    monitor-enter v1

    .line 1852925
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/hotconversations/HotConversationSubStoryPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1852926
    sput-object v2, Lcom/facebook/feedplugins/hotconversations/HotConversationSubStoryPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1852927
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1852928
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1852929
    new-instance v5, Lcom/facebook/feedplugins/hotconversations/HotConversationSubStoryPartDefinition;

    .line 1852930
    new-instance v3, Lcom/facebook/feedplugins/hotconversations/HotConversationStorySelector;

    const/16 v4, 0x1fc1

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v6, 0x95e

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x95d

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 p0, 0x1fc2

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v6, v7, p0}, Lcom/facebook/feedplugins/hotconversations/HotConversationStorySelector;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 1852931
    move-object v3, v3

    .line 1852932
    check-cast v3, Lcom/facebook/feedplugins/hotconversations/HotConversationStorySelector;

    invoke-static {v0}, Lcom/facebook/feedplugins/hotconversations/HotConversationBlingBarSelector;->a(LX/0QB;)Lcom/facebook/feedplugins/hotconversations/HotConversationBlingBarSelector;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/hotconversations/HotConversationBlingBarSelector;

    invoke-direct {v5, v3, v4}, Lcom/facebook/feedplugins/hotconversations/HotConversationSubStoryPartDefinition;-><init>(Lcom/facebook/feedplugins/hotconversations/HotConversationStorySelector;Lcom/facebook/feedplugins/hotconversations/HotConversationBlingBarSelector;)V

    .line 1852933
    move-object v0, v5

    .line 1852934
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1852935
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/hotconversations/HotConversationSubStoryPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1852936
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1852937
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1852938
    check-cast p2, LX/C33;

    .line 1852939
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationSubStoryPartDefinition;->a:Lcom/facebook/feedplugins/hotconversations/HotConversationStorySelector;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1852940
    iget-object v0, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationSubStoryPartDefinition;->b:Lcom/facebook/feedplugins/hotconversations/HotConversationBlingBarSelector;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1852941
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1852942
    const/4 v0, 0x1

    return v0
.end method
