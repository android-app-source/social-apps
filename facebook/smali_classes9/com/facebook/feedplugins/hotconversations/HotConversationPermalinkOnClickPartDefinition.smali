.class public Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/C33;",
        "Landroid/view/View$OnClickListener;",
        "LX/1PW;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/1nB;

.field public final b:Lcom/facebook/content/SecureContextHelper;

.field public final c:LX/C3a;


# direct methods
.method public constructor <init>(LX/1nB;Lcom/facebook/content/SecureContextHelper;LX/C3a;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1852808
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1852809
    iput-object p1, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;->a:LX/1nB;

    .line 1852810
    iput-object p2, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;->b:Lcom/facebook/content/SecureContextHelper;

    .line 1852811
    iput-object p3, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;->c:LX/C3a;

    .line 1852812
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;
    .locals 6

    .prologue
    .line 1852813
    const-class v1, Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;

    monitor-enter v1

    .line 1852814
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1852815
    sput-object v2, Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1852816
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1852817
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1852818
    new-instance p0, Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v3

    check-cast v3, LX/1nB;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/C3a;->b(LX/0QB;)LX/C3a;

    move-result-object v5

    check-cast v5, LX/C3a;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;-><init>(LX/1nB;Lcom/facebook/content/SecureContextHelper;LX/C3a;)V

    .line 1852819
    move-object v0, p0

    .line 1852820
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1852821
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1852822
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1852823
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1852824
    check-cast p2, LX/C33;

    .line 1852825
    iget-object v0, p2, LX/C33;->a:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 1852826
    iget-object v0, p2, LX/C33;->a:Landroid/view/View$OnClickListener;

    .line 1852827
    :goto_0
    return-object v0

    .line 1852828
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;->a:LX/1nB;

    iget-object v0, p2, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1852829
    iget-object p1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p1

    .line 1852830
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v0}, LX/1nB;->a(Lcom/facebook/graphql/model/GraphQLStory;)Landroid/content/Intent;

    move-result-object v1

    .line 1852831
    new-instance v0, LX/C8U;

    invoke-direct {v0, p0, p2, v1}, LX/C8U;-><init>(Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;LX/C33;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x14d41eba

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1852832
    check-cast p2, Landroid/view/View$OnClickListener;

    .line 1852833
    invoke-virtual {p4, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1852834
    const/16 v1, 0x1f

    const v2, 0x7a88fe2a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1852835
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1852836
    return-void
.end method
