.class public Lcom/facebook/feedplugins/video/SearchResultsRichVideoAttachmentView;
.super Lcom/facebook/feedplugins/video/RichVideoAttachmentView;
.source ""


# instance fields
.field public g:LX/CDi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1858699
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedplugins/video/SearchResultsRichVideoAttachmentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1858700
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1858697
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feedplugins/video/SearchResultsRichVideoAttachmentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1858698
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1858689
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1858690
    const-class v0, Lcom/facebook/feedplugins/video/SearchResultsRichVideoAttachmentView;

    invoke-static {v0, p0}, Lcom/facebook/feedplugins/video/SearchResultsRichVideoAttachmentView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1858691
    const v0, 0x7f0311f8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1858692
    iget-object v0, p0, Lcom/facebook/feedplugins/video/SearchResultsRichVideoAttachmentView;->g:LX/CDi;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 1858693
    new-instance p3, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;

    const-class p1, Landroid/content/Context;

    invoke-interface {v0, p1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/content/Context;

    invoke-static {v0}, LX/3Gf;->b(LX/0QB;)Ljava/lang/Boolean;

    move-result-object p2

    check-cast p2, Ljava/lang/Boolean;

    invoke-direct {p3, p1, v1, p2}, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;-><init>(Landroid/content/Context;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 1858694
    move-object v0, p3

    .line 1858695
    iput-object v0, p0, Lcom/facebook/feedplugins/video/SearchResultsRichVideoAttachmentView;->a:LX/3Ge;

    .line 1858696
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feedplugins/video/SearchResultsRichVideoAttachmentView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/feedplugins/video/SearchResultsRichVideoAttachmentView;

    const-class v1, LX/CDi;

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/CDi;

    iput-object v0, p0, Lcom/facebook/feedplugins/video/SearchResultsRichVideoAttachmentView;->g:LX/CDi;

    return-void
.end method
