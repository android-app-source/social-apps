.class public Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;
.super LX/3Ge;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final m:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final n:Landroid/content/Context;

.field private o:Lcom/facebook/video/player/plugins/VideoPlugin;

.field private p:LX/7NM;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1860021
    const-class v0, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->m:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 1
    .param p2    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/video/player/IsVideoSpecDisplayEnabled;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1859985
    invoke-direct {p0}, LX/3Ge;-><init>()V

    .line 1859986
    iput-object p1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    .line 1859987
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->k:Z

    .line 1859988
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->a:LX/0Px;

    .line 1859989
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1859990
    invoke-virtual {p0}, LX/3Ge;->a()V

    .line 1859991
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/video/player/RichVideoPlayer;)LX/3J8;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1859992
    const-class v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1859993
    :goto_0
    const-class v3, Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-virtual {p1, v3}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 1859994
    :goto_1
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    .line 1859995
    sget-object v0, LX/3J8;->LIVE_360_VIDEO:LX/3J8;

    .line 1859996
    :goto_2
    return-object v0

    :cond_0
    move v0, v2

    .line 1859997
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1859998
    goto :goto_1

    .line 1859999
    :cond_2
    if-eqz v0, :cond_3

    .line 1860000
    sget-object v0, LX/3J8;->LIVE_VIDEO:LX/3J8;

    goto :goto_2

    .line 1860001
    :cond_3
    invoke-super {p0, p1}, LX/3Ge;->a(Lcom/facebook/video/player/RichVideoPlayer;)LX/3J8;

    move-result-object v0

    goto :goto_2
.end method

.method public final a(Lcom/facebook/video/player/RichVideoPlayer;LX/3J8;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1860002
    invoke-virtual {p0, p1}, LX/3Ge;->a(Lcom/facebook/video/player/RichVideoPlayer;)LX/3J8;

    move-result-object v2

    .line 1860003
    sget-object v3, LX/CDh;->a:[I

    invoke-virtual {v2}, LX/3J8;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 1860004
    if-ne p2, v2, :cond_0

    move v0, v1

    :cond_0
    :goto_0
    return v0

    .line 1860005
    :pswitch_0
    sget-object v2, LX/3J8;->REGULAR_VIDEO:LX/3J8;

    if-eq p2, v2, :cond_1

    sget-object v2, LX/3J8;->PREVIOUSLY_LIVE_VIDEO:LX/3J8;

    if-eq p2, v2, :cond_1

    sget-object v2, LX/3J8;->PURPLE_RAIN_VIDEO:LX/3J8;

    if-eq p2, v2, :cond_1

    sget-object v2, LX/3J8;->ANIMATED_GIF_VIDEO:LX/3J8;

    if-ne p2, v2, :cond_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 1860006
    :pswitch_1
    sget-object v2, LX/3J8;->REGULAR_360_VIDEO:LX/3J8;

    if-eq p2, v2, :cond_2

    sget-object v2, LX/3J8;->PREVIOUSLY_LIVE_360_VIDEO:LX/3J8;

    if-ne p2, v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final c()LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1860007
    new-instance v0, Lcom/facebook/video/player/plugins/VideoPlugin;

    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->o:Lcom/facebook/video/player/plugins/VideoPlugin;

    .line 1860008
    new-instance v0, LX/7NM;

    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/7NM;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->p:LX/7NM;

    .line 1860009
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/player/plugins/CoverImagePlugin;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    sget-object v3, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->m:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v1, v2, v3}, Lcom/facebook/video/player/plugins/CoverImagePlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    .line 1860010
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1860011
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->o:Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/Bwt;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/Bwt;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1860012
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->o:Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->p:LX/7NM;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final f()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1860013
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->p:LX/7NM;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/3I7;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/3I7;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/Video360Plugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/3Ie;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/3Ie;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final g()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1860014
    invoke-virtual {p0}, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->e()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final h()LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1860015
    iget-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->o:Lcom/facebook/video/player/plugins/VideoPlugin;

    if-nez v0, :cond_0

    .line 1860016
    new-instance v0, Lcom/facebook/video/player/plugins/VideoPlugin;

    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->o:Lcom/facebook/video/player/plugins/VideoPlugin;

    .line 1860017
    :cond_0
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->o:Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/player/plugins/CoverImagePlugin;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    sget-object v3, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->m:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v1, v2, v3}, Lcom/facebook/video/player/plugins/CoverImagePlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/3Ig;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/3Ig;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1860018
    invoke-virtual {p0}, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->e()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1860019
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    iget-object v1, p0, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->p:LX/7NM;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/3I7;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/3I7;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/Video360Plugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/Video360NuxAnimationPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v2, p0, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->n:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final k()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1860020
    invoke-virtual {p0}, Lcom/facebook/feedplugins/video/richvideoplayer/SearchResultsInlineRichVideoPlayerPluginSelector;->f()LX/0Px;

    move-result-object v0

    return-object v0
.end method
