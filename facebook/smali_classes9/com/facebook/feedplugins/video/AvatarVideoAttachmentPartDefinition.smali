.class public Lcom/facebook/feedplugins/video/AvatarVideoAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/3VT;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static k:LX/0Xm;


# instance fields
.field public final b:LX/1Uj;

.field private final c:LX/1qb;

.field private final d:Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition",
            "<",
            "LX/3VT;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

.field private final f:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition",
            "<TE;",
            "LX/3VT;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

.field private final h:Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

.field private final i:Lcom/facebook/feedplugins/attachments/SmallImagePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/SmallImagePartDefinition",
            "<",
            "LX/3VT;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition",
            "<TE;",
            "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1858521
    const-class v0, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;

    const-string v1, "newsfeed_angora_attachment_view"

    const-string v2, "small_photo"

    const-string v3, "native_newsfeed"

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/video/AvatarVideoAttachmentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1qb;LX/1Uj;Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;Lcom/facebook/feedplugins/attachments/SmallImagePartDefinition;Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1858562
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1858563
    iput-object p1, p0, Lcom/facebook/feedplugins/video/AvatarVideoAttachmentPartDefinition;->c:LX/1qb;

    .line 1858564
    iput-object p2, p0, Lcom/facebook/feedplugins/video/AvatarVideoAttachmentPartDefinition;->b:LX/1Uj;

    .line 1858565
    iput-object p3, p0, Lcom/facebook/feedplugins/video/AvatarVideoAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;

    .line 1858566
    iput-object p4, p0, Lcom/facebook/feedplugins/video/AvatarVideoAttachmentPartDefinition;->e:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    .line 1858567
    iput-object p5, p0, Lcom/facebook/feedplugins/video/AvatarVideoAttachmentPartDefinition;->f:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    .line 1858568
    iput-object p6, p0, Lcom/facebook/feedplugins/video/AvatarVideoAttachmentPartDefinition;->g:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    .line 1858569
    iput-object p7, p0, Lcom/facebook/feedplugins/video/AvatarVideoAttachmentPartDefinition;->h:Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    .line 1858570
    iput-object p8, p0, Lcom/facebook/feedplugins/video/AvatarVideoAttachmentPartDefinition;->i:Lcom/facebook/feedplugins/attachments/SmallImagePartDefinition;

    .line 1858571
    iput-object p9, p0, Lcom/facebook/feedplugins/video/AvatarVideoAttachmentPartDefinition;->j:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    .line 1858572
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/video/AvatarVideoAttachmentPartDefinition;
    .locals 13

    .prologue
    .line 1858551
    const-class v1, Lcom/facebook/feedplugins/video/AvatarVideoAttachmentPartDefinition;

    monitor-enter v1

    .line 1858552
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/video/AvatarVideoAttachmentPartDefinition;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1858553
    sput-object v2, Lcom/facebook/feedplugins/video/AvatarVideoAttachmentPartDefinition;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1858554
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1858555
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1858556
    new-instance v3, Lcom/facebook/feedplugins/video/AvatarVideoAttachmentPartDefinition;

    invoke-static {v0}, LX/1qb;->a(LX/0QB;)LX/1qb;

    move-result-object v4

    check-cast v4, LX/1qb;

    invoke-static {v0}, LX/1Uj;->a(LX/0QB;)LX/1Uj;

    move-result-object v5

    check-cast v5, LX/1Uj;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/SmallImagePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/SmallImagePartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/feedplugins/attachments/SmallImagePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    move-result-object v12

    check-cast v12, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/feedplugins/video/AvatarVideoAttachmentPartDefinition;-><init>(LX/1qb;LX/1Uj;Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;Lcom/facebook/feedplugins/attachments/SmallImagePartDefinition;Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;)V

    .line 1858557
    move-object v0, v3

    .line 1858558
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1858559
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/video/AvatarVideoAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1858560
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1858561
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 1858573
    sget-object v0, LX/3VT;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1858523
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v7, 0x0

    .line 1858524
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1858525
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1858526
    const v1, 0x7f0d0539

    iget-object v2, p0, Lcom/facebook/feedplugins/video/AvatarVideoAttachmentPartDefinition;->j:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    new-instance v3, LX/3EE;

    const/4 v4, -0x1

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v5

    new-instance v6, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v6}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    invoke-direct {v3, p2, v4, v5, v6}, LX/3EE;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;ILX/0am;Ljava/util/concurrent/atomic/AtomicReference;)V

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1858527
    const v1, 0x7f0d052c

    iget-object v2, p0, Lcom/facebook/feedplugins/video/AvatarVideoAttachmentPartDefinition;->e:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    .line 1858528
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/SpannableStringBuilder;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    .line 1858529
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1858530
    iget-object v4, p0, Lcom/facebook/feedplugins/video/AvatarVideoAttachmentPartDefinition;->b:LX/1Uj;

    invoke-virtual {v4}, LX/1Uj;->a()Landroid/text/style/MetricAffectingSpan;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    const/16 p3, 0x11

    invoke-virtual {v3, v4, v5, v6, p3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1858531
    :cond_0
    move-object v3, v3

    .line 1858532
    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1858533
    const v1, 0x7f0d052d

    iget-object v2, p0, Lcom/facebook/feedplugins/video/AvatarVideoAttachmentPartDefinition;->e:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    .line 1858534
    const/4 v3, 0x0

    .line 1858535
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->y()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 1858536
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->y()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/SpannableStringBuilder;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    .line 1858537
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 1858538
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    .line 1858539
    if-eqz v3, :cond_3

    .line 1858540
    invoke-static {v3}, LX/1qb;->a(Landroid/text/SpannableStringBuilder;)V

    .line 1858541
    invoke-virtual {v3, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1858542
    :cond_2
    :goto_0
    move-object v3, v3

    .line 1858543
    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1858544
    iget-object v1, p0, Lcom/facebook/feedplugins/video/AvatarVideoAttachmentPartDefinition;->f:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    invoke-interface {p1, v1, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1858545
    iget-object v1, p0, Lcom/facebook/feedplugins/video/AvatarVideoAttachmentPartDefinition;->h:Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    invoke-interface {p1, v1, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1858546
    iget-object v1, p0, Lcom/facebook/feedplugins/video/AvatarVideoAttachmentPartDefinition;->g:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    new-instance v2, LX/2ya;

    invoke-direct {v2, p2}, LX/2ya;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1858547
    iget-object v1, p0, Lcom/facebook/feedplugins/video/AvatarVideoAttachmentPartDefinition;->i:Lcom/facebook/feedplugins/attachments/SmallImagePartDefinition;

    new-instance v2, LX/36R;

    invoke-static {v0}, LX/36S;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    sget-object v3, Lcom/facebook/feedplugins/video/AvatarVideoAttachmentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v2, v0, v3}, LX/36R;-><init>(Lcom/facebook/graphql/model/GraphQLImage;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1858548
    iget-object v0, p0, Lcom/facebook/feedplugins/video/AvatarVideoAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;

    invoke-interface {p1, v0, v7}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1858549
    return-object v7

    .line 1858550
    :cond_3
    invoke-static {v4}, Landroid/text/SpannableStringBuilder;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1858522
    const/4 v0, 0x1

    return v0
.end method
