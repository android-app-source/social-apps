.class public Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionInlineVideoView;
.super LX/2oW;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final m:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private n:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1858023
    const-class v0, Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionInlineVideoView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionInlineVideoView;->m:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1858021
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionInlineVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1858022
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1858019
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionInlineVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1858020
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1858013
    invoke-direct {p0, p1, p2, p3}, LX/2oW;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1858014
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionInlineVideoView;->n:Ljava/lang/String;

    .line 1858015
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setShouldCropToFit(Z)V

    .line 1858016
    sget-object v0, LX/04D;->SAVED_REMINDER:LX/04D;

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerOrigin(LX/04D;)V

    .line 1858017
    sget-object v0, LX/04G;->INLINE_PLAYER:LX/04G;

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerType(LX/04G;)V

    .line 1858018
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "LX/0Px",
            "<+",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1858002
    new-instance v0, Lcom/facebook/video/player/plugins/CoverImagePlugin;

    sget-object v1, Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionInlineVideoView;->m:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v0, p1, v1}, Lcom/facebook/video/player/plugins/CoverImagePlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    new-instance v1, LX/2pM;

    invoke-direct {v1, p1}, LX/2pM;-><init>(Landroid/content/Context;)V

    new-instance v2, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-direct {v2, p1}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;-><init>(Landroid/content/Context;)V

    new-instance v3, LX/7NY;

    invoke-direct {v3, p1}, LX/7NY;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1, v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/video/engine/VideoPlayerParams;LX/0P1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/video/engine/VideoPlayerParams;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "+",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1858003
    iget-object v0, p0, Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionInlineVideoView;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionInlineVideoView;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1858004
    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 1858005
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionInlineVideoView;->n:Ljava/lang/String;

    .line 1858006
    :cond_0
    iget-object v0, p1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/feedplugins/saved/rows/ui/SavedCollectionInlineVideoView;->n:Ljava/lang/String;

    .line 1858007
    new-instance v0, LX/2pZ;

    invoke-direct {v0}, LX/2pZ;-><init>()V

    .line 1858008
    iput-object p1, v0, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1858009
    move-object v0, v0

    .line 1858010
    invoke-virtual {v0, p2}, LX/2pZ;->a(LX/0P1;)LX/2pZ;

    move-result-object v0

    invoke-virtual {v0}, LX/2pZ;->b()LX/2pa;

    move-result-object v0

    .line 1858011
    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 1858012
    return-void
.end method
