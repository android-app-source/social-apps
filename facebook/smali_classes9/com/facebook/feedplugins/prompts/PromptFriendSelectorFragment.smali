.class public Lcom/facebook/feedplugins/prompts/PromptFriendSelectorFragment;
.super Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;
.source ""


# static fields
.field private static final w:Ljava/lang/String;


# instance fields
.field public u:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CAt;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private x:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1855780
    const-class v0, Lcom/facebook/feedplugins/prompts/PromptFriendSelectorFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/prompts/PromptFriendSelectorFragment;->w:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1855771
    invoke-direct {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/feedplugins/prompts/PromptFriendSelectorFragment;

    const/16 p0, 0x20b1

    invoke-static {v1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    iput-object p0, p1, Lcom/facebook/feedplugins/prompts/PromptFriendSelectorFragment;->u:LX/0Ot;

    iput-object v1, p1, Lcom/facebook/feedplugins/prompts/PromptFriendSelectorFragment;->v:LX/03V;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1855772
    const-class v0, Lcom/facebook/feedplugins/prompts/PromptFriendSelectorFragment;

    invoke-static {v0, p0}, Lcom/facebook/feedplugins/prompts/PromptFriendSelectorFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1855773
    invoke-super {p0, p1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(Landroid/os/Bundle;)V

    .line 1855774
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1855775
    if-eqz v0, :cond_0

    .line 1855776
    sget-object v1, LX/CAr;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/feedplugins/prompts/PromptFriendSelectorFragment;->x:Ljava/lang/String;

    .line 1855777
    :cond_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feedplugins/prompts/PromptFriendSelectorFragment;->x:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1855778
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedplugins/prompts/PromptFriendSelectorFragment;->v:LX/03V;

    sget-object v1, Lcom/facebook/feedplugins/prompts/PromptFriendSelectorFragment;->w:Ljava/lang/String;

    const-string v2, "FriendVote inviter launched without specifying Prompt ID"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1855779
    :cond_2
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x15471e6e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1855763
    invoke-super {p0, p1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1855764
    const v1, 0x7f020cc3

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082a3d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1855765
    iget-object v3, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->K:Landroid/view/ViewStub;

    if-nez v3, :cond_0

    .line 1855766
    :goto_0
    const/16 v1, 0x2b

    const v2, 0x43262875

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1855767
    :cond_0
    iget-object v3, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->K:Landroid/view/ViewStub;

    invoke-virtual {v3}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v5

    .line 1855768
    const v3, 0x7f0d131f

    invoke-virtual {v5, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1855769
    const v3, 0x7f0d1320

    invoke-virtual {v5, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1855770
    const v3, 0x7f0d1321

    invoke-virtual {v5, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    new-instance p1, LX/BXK;

    invoke-direct {p1, p0, v5}, LX/BXK;-><init>(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;Landroid/view/View;)V

    invoke-virtual {v3, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final y()V
    .locals 8

    .prologue
    .line 1855750
    iget-object v0, p0, Lcom/facebook/feedplugins/prompts/PromptFriendSelectorFragment;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CAt;

    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->o()LX/0Px;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feedplugins/prompts/PromptFriendSelectorFragment;->x:Ljava/lang/String;

    .line 1855751
    new-instance v3, LX/CAw;

    invoke-direct {v3}, LX/CAw;-><init>()V

    move-object v4, v3

    .line 1855752
    new-instance v5, LX/4Ic;

    invoke-direct {v5}, LX/4Ic;-><init>()V

    iget-object v3, v0, LX/CAt;->d:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1855753
    const-string v6, "actor_id"

    invoke-virtual {v5, v6, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1855754
    move-object v3, v5

    .line 1855755
    const-string v5, "production_prompt_id"

    invoke-virtual {v3, v5, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1855756
    move-object v3, v3

    .line 1855757
    const-string v5, "user_ids"

    invoke-virtual {v3, v5, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1855758
    move-object v3, v3

    .line 1855759
    const-string v5, "input"

    invoke-virtual {v4, v5, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1855760
    invoke-static {v4}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    .line 1855761
    iget-object v4, v0, LX/CAt;->e:LX/1Ck;

    const-string v5, "key_send_social_prompt_invite"

    iget-object v6, v0, LX/CAt;->f:LX/0tX;

    sget-object v7, LX/3Fz;->b:LX/3Fz;

    invoke-virtual {v6, v3, v7}, LX/0tX;->a(LX/399;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    new-instance v6, LX/CAs;

    invoke-direct {v6, v0, p0}, LX/CAs;-><init>(LX/CAt;Lcom/facebook/feedplugins/prompts/PromptFriendSelectorFragment;)V

    invoke-virtual {v4, v5, v3, v6}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1855762
    return-void
.end method
