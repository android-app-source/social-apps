.class public final Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3fc960d1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel$ProductionPromptModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel$ViewerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1856057
    const-class v0, Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1856086
    const-class v0, Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1856089
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1856090
    return-void
.end method

.method private a()Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel$ProductionPromptModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1856087
    iget-object v0, p0, Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel;->e:Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel$ProductionPromptModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel$ProductionPromptModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel$ProductionPromptModel;

    iput-object v0, p0, Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel;->e:Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel$ProductionPromptModel;

    .line 1856088
    iget-object v0, p0, Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel;->e:Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel$ProductionPromptModel;

    return-object v0
.end method

.method private j()Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel$ViewerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1856076
    iget-object v0, p0, Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel;->f:Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel$ViewerModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel$ViewerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel$ViewerModel;

    iput-object v0, p0, Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel;->f:Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel$ViewerModel;

    .line 1856077
    iget-object v0, p0, Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel;->f:Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel$ViewerModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1856078
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1856079
    invoke-direct {p0}, Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel;->a()Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel$ProductionPromptModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1856080
    invoke-direct {p0}, Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel;->j()Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel$ViewerModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1856081
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1856082
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1856083
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1856084
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1856085
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1856063
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1856064
    invoke-direct {p0}, Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel;->a()Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel$ProductionPromptModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1856065
    invoke-direct {p0}, Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel;->a()Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel$ProductionPromptModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel$ProductionPromptModel;

    .line 1856066
    invoke-direct {p0}, Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel;->a()Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel$ProductionPromptModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1856067
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel;

    .line 1856068
    iput-object v0, v1, Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel;->e:Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel$ProductionPromptModel;

    .line 1856069
    :cond_0
    invoke-direct {p0}, Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel;->j()Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel$ViewerModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1856070
    invoke-direct {p0}, Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel;->j()Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel$ViewerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel$ViewerModel;

    .line 1856071
    invoke-direct {p0}, Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel;->j()Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel$ViewerModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1856072
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel;

    .line 1856073
    iput-object v0, v1, Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel;->f:Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel$ViewerModel;

    .line 1856074
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1856075
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1856060
    new-instance v0, Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel;

    invoke-direct {v0}, Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel;-><init>()V

    .line 1856061
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1856062
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1856059
    const v0, -0x40c161f6

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1856058
    const v0, 0xb0da76b

    return v0
.end method
