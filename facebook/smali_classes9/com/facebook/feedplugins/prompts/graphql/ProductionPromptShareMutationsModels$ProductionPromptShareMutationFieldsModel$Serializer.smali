.class public final Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1855945
    const-class v0, Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel;

    new-instance v1, Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1855946
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1855944
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1855947
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1855948
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1855949
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1855950
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1855951
    if-eqz v2, :cond_0

    .line 1855952
    const-string p0, "production_prompt"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1855953
    invoke-static {v1, v2, p1}, LX/CB1;->a(LX/15i;ILX/0nX;)V

    .line 1855954
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1855955
    if-eqz v2, :cond_1

    .line 1855956
    const-string p0, "viewer"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1855957
    invoke-static {v1, v2, p1, p2}, LX/CB3;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1855958
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1855959
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1855943
    check-cast p1, Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel$Serializer;->a(Lcom/facebook/feedplugins/prompts/graphql/ProductionPromptShareMutationsModels$ProductionPromptShareMutationFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
