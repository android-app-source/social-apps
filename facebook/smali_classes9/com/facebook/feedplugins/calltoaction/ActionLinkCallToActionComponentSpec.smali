.class public Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static d:LX/0Xm;


# instance fields
.field private final b:LX/1nu;

.field public final c:LX/C2K;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1844291
    const-class v0, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionComponentSpec;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/C2K;LX/1nu;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1844292
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1844293
    iput-object p1, p0, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionComponentSpec;->c:LX/C2K;

    .line 1844294
    iput-object p2, p0, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionComponentSpec;->b:LX/1nu;

    .line 1844295
    return-void
.end method

.method public static a(Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionComponentSpec;LX/1De;LX/1Pp;LX/1dc;Landroid/net/Uri;)LX/1Di;
    .locals 4
    .param p2    # LX/1Pp;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/1dc;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "TE;",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;",
            "Landroid/net/Uri;",
            ")",
            "LX/1Di;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1844296
    if-nez p3, :cond_0

    if-nez p4, :cond_0

    .line 1844297
    const/4 v0, 0x0

    .line 1844298
    :goto_0
    return-object v0

    .line 1844299
    :cond_0
    if-eqz p4, :cond_1

    .line 1844300
    iget-object v0, p0, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionComponentSpec;->b:LX/1nu;

    invoke-virtual {v0, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v0

    sget-object v1, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v0

    invoke-virtual {v0, p4}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1nw;->a(LX/1Pp;)LX/1nw;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    .line 1844301
    :goto_1
    const v1, 0x7f0b1c8c

    invoke-interface {v0, v1}, LX/1Di;->i(I)LX/1Di;

    move-result-object v1

    const v2, 0x7f0b1c8c

    invoke-interface {v1, v2}, LX/1Di;->q(I)LX/1Di;

    move-result-object v1

    const/4 v2, 0x5

    const v3, 0x7f0b1c8d

    invoke-interface {v1, v2, v3}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    .line 1844302
    const v2, -0x40fde068

    const/4 v3, 0x0

    invoke-static {p1, v2, v3}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v2

    move-object v2, v2

    .line 1844303
    invoke-interface {v1, v2}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    goto :goto_0

    .line 1844304
    :cond_1
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionComponentSpec;
    .locals 5

    .prologue
    .line 1844305
    const-class v1, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionComponentSpec;

    monitor-enter v1

    .line 1844306
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionComponentSpec;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1844307
    sput-object v2, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionComponentSpec;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1844308
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1844309
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1844310
    new-instance p0, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionComponentSpec;

    invoke-static {v0}, LX/C2K;->a(LX/0QB;)LX/C2K;

    move-result-object v3

    check-cast v3, LX/C2K;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v4

    check-cast v4, LX/1nu;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionComponentSpec;-><init>(LX/C2K;LX/1nu;)V

    .line 1844311
    move-object v0, p0

    .line 1844312
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1844313
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1844314
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1844315
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
