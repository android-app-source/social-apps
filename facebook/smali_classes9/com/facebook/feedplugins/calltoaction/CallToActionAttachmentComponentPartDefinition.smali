.class public Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pm;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;"
    }
.end annotation


# instance fields
.field private final d:LX/C2T;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/C2T;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1844366
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1844367
    iput-object p2, p0, Lcom/facebook/feedplugins/calltoaction/CallToActionAttachmentComponentPartDefinition;->d:LX/C2T;

    .line 1844368
    return-void
.end method


# virtual methods
.method public final synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1844369
    const/4 v0, 0x0

    move-object v0, v0

    .line 1844370
    return-object v0
.end method

.method public final synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1844364
    const/4 v0, 0x0

    move-object v0, v0

    .line 1844365
    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1844359
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1844360
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1844361
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1844362
    invoke-static {v0}, LX/2v7;->e(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1844363
    goto :goto_1

    :goto_0
    return v0

    :cond_0
    :goto_1
    const/4 v0, 0x0

    goto :goto_0
.end method
