.class public Lcom/facebook/feedplugins/quickpromotion/QuickPromotionNativeTemplateFeedUnitPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLQuickPromotionNativeTemplateFeedUnit;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final d:LX/1V0;

.field public final e:LX/3j4;

.field public final f:LX/CBN;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1V0;LX/3j4;LX/CBN;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1856292
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1856293
    iput-object p2, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionNativeTemplateFeedUnitPartDefinition;->d:LX/1V0;

    .line 1856294
    iput-object p3, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionNativeTemplateFeedUnitPartDefinition;->e:LX/3j4;

    .line 1856295
    iput-object p4, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionNativeTemplateFeedUnitPartDefinition;->f:LX/CBN;

    .line 1856296
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLQuickPromotionNativeTemplateFeedUnit;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1856279
    new-instance v0, LX/CBK;

    invoke-direct {v0, p0, p2, p3}, LX/CBK;-><init>(Lcom/facebook/feedplugins/quickpromotion/QuickPromotionNativeTemplateFeedUnitPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)V

    .line 1856280
    iget-object v1, p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionNativeTemplateFeedUnitPartDefinition;->d:LX/1V0;

    check-cast p3, LX/1Ps;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->e:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/quickpromotion/QuickPromotionNativeTemplateFeedUnitPartDefinition;
    .locals 7

    .prologue
    .line 1856281
    const-class v1, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionNativeTemplateFeedUnitPartDefinition;

    monitor-enter v1

    .line 1856282
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionNativeTemplateFeedUnitPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1856283
    sput-object v2, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionNativeTemplateFeedUnitPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1856284
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1856285
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1856286
    new-instance p0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionNativeTemplateFeedUnitPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v4

    check-cast v4, LX/1V0;

    invoke-static {v0}, LX/3j4;->a(LX/0QB;)LX/3j4;

    move-result-object v5

    check-cast v5, LX/3j4;

    invoke-static {v0}, LX/CBN;->a(LX/0QB;)LX/CBN;

    move-result-object v6

    check-cast v6, LX/CBN;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionNativeTemplateFeedUnitPartDefinition;-><init>(Landroid/content/Context;LX/1V0;LX/3j4;LX/CBN;)V

    .line 1856287
    move-object v0, p0

    .line 1856288
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1856289
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionNativeTemplateFeedUnitPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1856290
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1856291
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1856278
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionNativeTemplateFeedUnitPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1856277
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/quickpromotion/QuickPromotionNativeTemplateFeedUnitPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1856272
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1856273
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1856274
    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionNativeTemplateFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionNativeTemplateFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1856275
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1856276
    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuickPromotionNativeTemplateFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuickPromotionNativeTemplateFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNativeTemplateView;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
