.class public Lcom/facebook/feedplugins/attachments/CouponAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pn;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/35o;",
        ":",
        "LX/35q;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;TV;>;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static h:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition",
            "<",
            "Lcom/facebook/attachments/angora/AngoraAttachmentView;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition",
            "<TE;",
            "Lcom/facebook/attachments/angora/AngoraAttachmentView;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

.field private final g:Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1836523
    const-class v0, Lcom/facebook/feedplugins/attachments/CouponAttachmentPartDefinition;

    const-string v1, "newsfeed_angora_attachment_view"

    const-string v2, "native_newsfeed"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/CouponAttachmentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1836504
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1836505
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/CouponAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;

    .line 1836506
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/CouponAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;

    .line 1836507
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/CouponAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    .line 1836508
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/CouponAttachmentPartDefinition;->e:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    .line 1836509
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/CouponAttachmentPartDefinition;->f:Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    .line 1836510
    iput-object p6, p0, Lcom/facebook/feedplugins/attachments/CouponAttachmentPartDefinition;->g:Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;

    .line 1836511
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/CouponAttachmentPartDefinition;
    .locals 10

    .prologue
    .line 1836512
    const-class v1, Lcom/facebook/feedplugins/attachments/CouponAttachmentPartDefinition;

    monitor-enter v1

    .line 1836513
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/CouponAttachmentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1836514
    sput-object v2, Lcom/facebook/feedplugins/attachments/CouponAttachmentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1836515
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1836516
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1836517
    new-instance v3, Lcom/facebook/feedplugins/attachments/CouponAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/attachments/CouponAttachmentPartDefinition;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;)V

    .line 1836518
    move-object v0, v3

    .line 1836519
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1836520
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/CouponAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1836521
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1836522
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 1836503
    sget-object v0, Lcom/facebook/attachments/angora/AngoraAttachmentView;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1836489
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v5, 0x0

    .line 1836490
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1836491
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1836492
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/CouponAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;

    new-instance v2, LX/2yZ;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LX/2yZ;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1836493
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/CouponAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    invoke-interface {p1, v1, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1836494
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/CouponAttachmentPartDefinition;->f:Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    invoke-interface {p1, v1, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1836495
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/CouponAttachmentPartDefinition;->e:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    new-instance v2, LX/2ya;

    invoke-direct {v2, p2}, LX/2ya;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1836496
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-nez v1, :cond_2

    .line 1836497
    :cond_0
    const/4 v1, 0x0

    .line 1836498
    :goto_0
    move-object v0, v1

    .line 1836499
    if-eqz v0, :cond_1

    .line 1836500
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/CouponAttachmentPartDefinition;->g:Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;

    new-instance v2, LX/36Q;

    sget-object v3, Lcom/facebook/feedplugins/attachments/CouponAttachmentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v4, 0x3ff745d1

    invoke-direct {v2, v0, v3, v4}, LX/36Q;-><init>(Lcom/facebook/graphql/model/GraphQLImage;Lcom/facebook/common/callercontext/CallerContext;F)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1836501
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/CouponAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;

    invoke-interface {p1, v0, v5}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1836502
    return-object v5

    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1836488
    const/4 v0, 0x1

    return v0
.end method
