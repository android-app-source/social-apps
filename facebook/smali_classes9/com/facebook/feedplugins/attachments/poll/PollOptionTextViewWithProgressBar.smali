.class public Lcom/facebook/feedplugins/attachments/poll/PollOptionTextViewWithProgressBar;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1842246
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 1842247
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1842248
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1842249
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1842250
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1842251
    return-void
.end method


# virtual methods
.method public declared-synchronized setProgress(F)V
    .locals 2

    .prologue
    .line 1842252
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/feedplugins/attachments/poll/PollOptionTextViewWithProgressBar;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    instance-of v0, v0, Landroid/graphics/drawable/LayerDrawable;

    if-eqz v0, :cond_0

    .line 1842253
    invoke-virtual {p0}, Lcom/facebook/feedplugins/attachments/poll/PollOptionTextViewWithProgressBar;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    .line 1842254
    const v1, 0x7f0d31ee

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1842255
    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/graphics/drawable/ClipDrawable;

    if-eqz v1, :cond_0

    .line 1842256
    const v1, 0x461c4000    # 10000.0f

    mul-float/2addr v1, p1

    float-to-int v1, v1

    .line 1842257
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    .line 1842258
    invoke-virtual {p0}, Lcom/facebook/feedplugins/attachments/poll/PollOptionTextViewWithProgressBar;->drawableStateChanged()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1842259
    :cond_0
    monitor-exit p0

    return-void

    .line 1842260
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
