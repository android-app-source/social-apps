.class public Lcom/facebook/feedplugins/attachments/poll/PollAddOptionComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/C1C;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final d:LX/C1A;

.field private final e:LX/1V0;

.field private final f:LX/C17;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/C1A;LX/1V0;LX/C17;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1842016
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1842017
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/poll/PollAddOptionComponentPartDefinition;->d:LX/C1A;

    .line 1842018
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/poll/PollAddOptionComponentPartDefinition;->e:LX/1V0;

    .line 1842019
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/poll/PollAddOptionComponentPartDefinition;->f:LX/C17;

    .line 1842020
    return-void
.end method

.method private a(LX/1De;LX/C1C;LX/1Pn;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/C1C;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1842021
    iget-object v0, p2, LX/C1C;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->ba()Z

    move-result v0

    iget-object v1, p2, LX/C1C;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/poll/PollAddOptionComponentPartDefinition;->f:LX/C17;

    invoke-static {v0, v1, v2}, LX/C1G;->a(ZLcom/facebook/feed/rows/core/props/FeedProps;LX/C17;)LX/1X6;

    move-result-object v1

    .line 1842022
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/poll/PollAddOptionComponentPartDefinition;->d:LX/C1A;

    const/4 v2, 0x0

    .line 1842023
    new-instance v3, LX/C19;

    invoke-direct {v3, v0}, LX/C19;-><init>(LX/C1A;)V

    .line 1842024
    iget-object v4, v0, LX/C1A;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/C18;

    .line 1842025
    if-nez v4, :cond_0

    .line 1842026
    new-instance v4, LX/C18;

    invoke-direct {v4, v0}, LX/C18;-><init>(LX/C1A;)V

    .line 1842027
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/C18;->a$redex0(LX/C18;LX/1De;IILX/C19;)V

    .line 1842028
    move-object v3, v4

    .line 1842029
    move-object v2, v3

    .line 1842030
    move-object v0, v2

    .line 1842031
    iget-object v2, p2, LX/C1C;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1842032
    iget-object v3, v0, LX/C18;->a:LX/C19;

    iput-object v2, v3, LX/C19;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1842033
    iget-object v3, v0, LX/C18;->e:Ljava/util/BitSet;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 1842034
    move-object v2, v0

    .line 1842035
    move-object v0, p3

    check-cast v0, LX/1Po;

    .line 1842036
    iget-object v3, v2, LX/C18;->a:LX/C19;

    iput-object v0, v3, LX/C19;->b:LX/1Po;

    .line 1842037
    iget-object v3, v2, LX/C18;->e:Ljava/util/BitSet;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 1842038
    move-object v0, v2

    .line 1842039
    iget-object v2, p2, LX/C1C;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v2

    .line 1842040
    iget-object v3, v0, LX/C18;->a:LX/C19;

    iput-object v2, v3, LX/C19;->c:Ljava/lang/String;

    .line 1842041
    iget-object v3, v0, LX/C18;->e:Ljava/util/BitSet;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 1842042
    move-object v0, v0

    .line 1842043
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 1842044
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/poll/PollAddOptionComponentPartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/poll/PollAddOptionComponentPartDefinition;
    .locals 7

    .prologue
    .line 1842045
    const-class v1, Lcom/facebook/feedplugins/attachments/poll/PollAddOptionComponentPartDefinition;

    monitor-enter v1

    .line 1842046
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/poll/PollAddOptionComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1842047
    sput-object v2, Lcom/facebook/feedplugins/attachments/poll/PollAddOptionComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1842048
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1842049
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1842050
    new-instance p0, Lcom/facebook/feedplugins/attachments/poll/PollAddOptionComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/C1A;->a(LX/0QB;)LX/C1A;

    move-result-object v4

    check-cast v4, LX/C1A;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-static {v0}, LX/C17;->a(LX/0QB;)LX/C17;

    move-result-object v6

    check-cast v6, LX/C17;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/attachments/poll/PollAddOptionComponentPartDefinition;-><init>(Landroid/content/Context;LX/C1A;LX/1V0;LX/C17;)V

    .line 1842051
    move-object v0, p0

    .line 1842052
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1842053
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/poll/PollAddOptionComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1842054
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1842055
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1842056
    check-cast p2, LX/C1C;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/poll/PollAddOptionComponentPartDefinition;->a(LX/1De;LX/C1C;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1842057
    check-cast p2, LX/C1C;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/poll/PollAddOptionComponentPartDefinition;->a(LX/1De;LX/C1C;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1842058
    check-cast p1, LX/C1C;

    .line 1842059
    iget-boolean v0, p1, LX/C1C;->a:Z

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1842060
    check-cast p1, LX/C1C;

    .line 1842061
    iget-object v0, p1, LX/C1C;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1WF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method
