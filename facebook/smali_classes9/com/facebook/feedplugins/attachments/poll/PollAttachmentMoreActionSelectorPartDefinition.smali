.class public Lcom/facebook/feedplugins/attachments/poll/PollAttachmentMoreActionSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/C1F;",
        "Ljava/lang/Void;",
        "LX/1Po;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/attachments/poll/PollSeeMoreComponentPartDefinition;

.field private final b:Lcom/facebook/feedplugins/attachments/poll/PollAddOptionComponentPartDefinition;

.field private final c:LX/C17;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/poll/PollSeeMoreComponentPartDefinition;Lcom/facebook/feedplugins/attachments/poll/PollAddOptionComponentPartDefinition;LX/C17;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1842189
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1842190
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/poll/PollAttachmentMoreActionSelectorPartDefinition;->a:Lcom/facebook/feedplugins/attachments/poll/PollSeeMoreComponentPartDefinition;

    .line 1842191
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/poll/PollAttachmentMoreActionSelectorPartDefinition;->b:Lcom/facebook/feedplugins/attachments/poll/PollAddOptionComponentPartDefinition;

    .line 1842192
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/poll/PollAttachmentMoreActionSelectorPartDefinition;->c:LX/C17;

    .line 1842193
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/poll/PollAttachmentMoreActionSelectorPartDefinition;
    .locals 6

    .prologue
    .line 1842194
    const-class v1, Lcom/facebook/feedplugins/attachments/poll/PollAttachmentMoreActionSelectorPartDefinition;

    monitor-enter v1

    .line 1842195
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/poll/PollAttachmentMoreActionSelectorPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1842196
    sput-object v2, Lcom/facebook/feedplugins/attachments/poll/PollAttachmentMoreActionSelectorPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1842197
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1842198
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1842199
    new-instance p0, Lcom/facebook/feedplugins/attachments/poll/PollAttachmentMoreActionSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/poll/PollSeeMoreComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/poll/PollSeeMoreComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/attachments/poll/PollSeeMoreComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/poll/PollAddOptionComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/poll/PollAddOptionComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/attachments/poll/PollAddOptionComponentPartDefinition;

    invoke-static {v0}, LX/C17;->a(LX/0QB;)LX/C17;

    move-result-object v5

    check-cast v5, LX/C17;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/attachments/poll/PollAttachmentMoreActionSelectorPartDefinition;-><init>(Lcom/facebook/feedplugins/attachments/poll/PollSeeMoreComponentPartDefinition;Lcom/facebook/feedplugins/attachments/poll/PollAddOptionComponentPartDefinition;LX/C17;)V

    .line 1842200
    move-object v0, p0

    .line 1842201
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1842202
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/poll/PollAttachmentMoreActionSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1842203
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1842204
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 1842205
    check-cast p2, LX/C1F;

    check-cast p3, LX/1Po;

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 1842206
    iget-object v0, p2, LX/C1F;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->gJ()Lcom/facebook/graphql/enums/GraphQLQuestionPollAnswersState;

    move-result-object v0

    .line 1842207
    iget-object v2, p2, LX/C1F;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->ba()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "OPEN"

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLQuestionPollAnswersState;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v2, v1

    .line 1842208
    :goto_0
    iget v4, p2, LX/C1F;->a:I

    if-lez v4, :cond_2

    :goto_1
    iget-object v3, p2, LX/C1F;->f:Ljava/lang/String;

    iget-object v4, p2, LX/C1F;->g:Ljava/lang/String;

    move-object v5, p3

    .line 1842209
    check-cast v5, LX/1Po;

    const/4 v7, 0x0

    .line 1842210
    invoke-interface {v5}, LX/1Po;->c()LX/1PT;

    move-result-object v8

    invoke-interface {v8}, LX/1PT;->a()LX/1Qt;

    move-result-object v8

    sget-object p3, LX/1Qt;->PERMALINK:LX/1Qt;

    invoke-virtual {v8, p3}, LX/1Qt;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    move-object v3, v7

    .line 1842211
    :cond_0
    :goto_2
    move-object v0, v3

    .line 1842212
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/poll/PollAttachmentMoreActionSelectorPartDefinition;->a:Lcom/facebook/feedplugins/attachments/poll/PollSeeMoreComponentPartDefinition;

    new-instance v3, LX/C1O;

    iget-object v4, p2, LX/C1F;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-boolean v5, p2, LX/C1F;->d:Z

    invoke-direct {v3, v0, v4, v5}, LX/C1O;-><init>(Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;Z)V

    invoke-static {p1, v1, v3}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/poll/PollAttachmentMoreActionSelectorPartDefinition;->b:Lcom/facebook/feedplugins/attachments/poll/PollAddOptionComponentPartDefinition;

    new-instance v3, LX/C1C;

    iget-object v4, p2, LX/C1F;->e:Lcom/facebook/graphql/model/GraphQLNode;

    iget-object v5, p2, LX/C1F;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {v3, v2, v4, v5}, LX/C1C;-><init>(ZLcom/facebook/graphql/model/GraphQLNode;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {v0, v1, v3}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 1842213
    const/4 v0, 0x0

    return-object v0

    :cond_1
    move v2, v3

    .line 1842214
    goto :goto_0

    :cond_2
    move v1, v3

    .line 1842215
    goto :goto_1

    .line 1842216
    :cond_3
    if-nez v1, :cond_0

    .line 1842217
    if-eqz v2, :cond_4

    move-object v3, v4

    .line 1842218
    goto :goto_2

    :cond_4
    move-object v3, v7

    .line 1842219
    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1842220
    const/4 v0, 0x1

    return v0
.end method
