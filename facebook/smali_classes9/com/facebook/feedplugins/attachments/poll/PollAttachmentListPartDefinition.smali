.class public Lcom/facebook/feedplugins/attachments/poll/PollAttachmentListPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;

.field public final c:LX/C1W;

.field private final d:LX/C17;

.field private final e:Lcom/facebook/feedplugins/attachments/poll/PollAttachmentMoreActionSelectorPartDefinition;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;LX/C17;Lcom/facebook/feedplugins/attachments/poll/PollAttachmentMoreActionSelectorPartDefinition;LX/C1W;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1842167
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1842168
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/poll/PollAttachmentListPartDefinition;->a:Landroid/content/Context;

    .line 1842169
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/poll/PollAttachmentListPartDefinition;->b:Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;

    .line 1842170
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/poll/PollAttachmentListPartDefinition;->c:LX/C1W;

    .line 1842171
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/poll/PollAttachmentListPartDefinition;->d:LX/C17;

    .line 1842172
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/poll/PollAttachmentListPartDefinition;->e:Lcom/facebook/feedplugins/attachments/poll/PollAttachmentMoreActionSelectorPartDefinition;

    .line 1842173
    return-void
.end method

.method private static a(LX/0Px;)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLQuestionOption;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1842174
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v3, :cond_0

    invoke-virtual {p0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLQuestionOption;

    .line 1842175
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->n()Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 1842176
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->n()Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLQuestionOptionVotersConnection;->a()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1842177
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 1842178
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private static a(Lcom/facebook/feedplugins/attachments/poll/PollAttachmentListPartDefinition;Lcom/facebook/graphql/model/GraphQLQuestionOption;Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/view/View$OnClickListener;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLQuestionOption;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    .line 1842129
    new-instance v0, LX/C1D;

    invoke-direct {v0, p0, p1, p2}, LX/C1D;-><init>(Lcom/facebook/feedplugins/attachments/poll/PollAttachmentListPartDefinition;Lcom/facebook/graphql/model/GraphQLQuestionOption;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/poll/PollAttachmentListPartDefinition;
    .locals 9

    .prologue
    .line 1842156
    const-class v1, Lcom/facebook/feedplugins/attachments/poll/PollAttachmentListPartDefinition;

    monitor-enter v1

    .line 1842157
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/poll/PollAttachmentListPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1842158
    sput-object v2, Lcom/facebook/feedplugins/attachments/poll/PollAttachmentListPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1842159
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1842160
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1842161
    new-instance v3, Lcom/facebook/feedplugins/attachments/poll/PollAttachmentListPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;

    invoke-static {v0}, LX/C17;->a(LX/0QB;)LX/C17;

    move-result-object v6

    check-cast v6, LX/C17;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/poll/PollAttachmentMoreActionSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/poll/PollAttachmentMoreActionSelectorPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/attachments/poll/PollAttachmentMoreActionSelectorPartDefinition;

    invoke-static {v0}, LX/C1W;->a(LX/0QB;)LX/C1W;

    move-result-object v8

    check-cast v8, LX/C1W;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/attachments/poll/PollAttachmentListPartDefinition;-><init>(Landroid/content/Context;Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;LX/C17;Lcom/facebook/feedplugins/attachments/poll/PollAttachmentMoreActionSelectorPartDefinition;LX/C1W;)V

    .line 1842162
    move-object v0, v3

    .line 1842163
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1842164
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/poll/PollAttachmentListPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1842165
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1842166
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/1RF;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ps;)Ljava/lang/Void;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/multirow/api/MultiRowSubParts",
            "<TE;>;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "Ljava/lang/Void;"
        }
    .end annotation

    .prologue
    .line 1842132
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    move-object v13, v2

    check-cast v13, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1842133
    invoke-virtual {v13}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->fS()Lcom/facebook/graphql/model/GraphQLQuestionOptionsConnection;

    move-result-object v2

    if-nez v2, :cond_1

    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v2

    move-object v11, v2

    .line 1842134
    :goto_0
    invoke-virtual {v13}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->ba()Z

    move-result v10

    .line 1842135
    invoke-static {v11}, Lcom/facebook/feedplugins/attachments/poll/PollAttachmentListPartDefinition;->a(LX/0Px;)I

    move-result v5

    move-object/from16 v2, p3

    .line 1842136
    check-cast v2, LX/1Po;

    invoke-interface {v2}, LX/1Po;->c()LX/1PT;

    move-result-object v2

    invoke-interface {v2}, LX/1PT;->a()LX/1Qt;

    move-result-object v2

    sget-object v3, LX/1Qt;->PERMALINK:LX/1Qt;

    if-ne v2, v3, :cond_2

    const/4 v2, 0x1

    .line 1842137
    :goto_1
    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feedplugins/attachments/poll/PollAttachmentListPartDefinition;->d:LX/C17;

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v2, v0, v1}, LX/C17;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PW;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_0
    const/4 v2, 0x1

    .line 1842138
    :goto_2
    if-eqz v2, :cond_4

    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v2

    move v12, v2

    .line 1842139
    :goto_3
    const/4 v2, 0x0

    move v14, v2

    :goto_4
    if-ge v14, v12, :cond_7

    .line 1842140
    invoke-virtual {v11, v14}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLQuestionOption;

    .line 1842141
    const-class v2, LX/C1E;

    invoke-virtual {v13}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->hM()Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLQuestionResponseMethod;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v7

    check-cast v7, LX/C1E;

    .line 1842142
    if-eqz v10, :cond_5

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v4, v1}, Lcom/facebook/feedplugins/attachments/poll/PollAttachmentListPartDefinition;->a(Lcom/facebook/feedplugins/attachments/poll/PollAttachmentListPartDefinition;Lcom/facebook/graphql/model/GraphQLQuestionOption;Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/view/View$OnClickListener;

    move-result-object v8

    .line 1842143
    :goto_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/facebook/feedplugins/attachments/poll/PollAttachmentListPartDefinition;->b:Lcom/facebook/feedplugins/attachments/poll/PollOptionItemPartDefinition;

    new-instance v2, LX/C1H;

    if-nez v14, :cond_6

    const/4 v6, 0x1

    :goto_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/feedplugins/attachments/poll/PollAttachmentListPartDefinition;->d:LX/C17;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/facebook/feedplugins/attachments/poll/PollAttachmentListPartDefinition;->a:Landroid/content/Context;

    const v16, 0x7f0805e4

    move/from16 v0, v16

    invoke-virtual {v9, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v4, v9}, LX/C17;->a(Lcom/facebook/graphql/model/GraphQLQuestionOption;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v9

    move-object/from16 v3, p2

    invoke-direct/range {v2 .. v10}, LX/C1H;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLQuestionOption;IZLX/C1E;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Z)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1842144
    add-int/lit8 v2, v14, 0x1

    move v14, v2

    goto :goto_4

    .line 1842145
    :cond_1
    invoke-virtual {v13}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->fS()Lcom/facebook/graphql/model/GraphQLQuestionOptionsConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuestionOptionsConnection;->a()LX/0Px;

    move-result-object v2

    move-object v11, v2

    goto/16 :goto_0

    .line 1842146
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 1842147
    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    .line 1842148
    :cond_4
    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v2

    const/4 v3, 0x3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    move v12, v2

    goto :goto_3

    .line 1842149
    :cond_5
    const/4 v8, 0x0

    goto :goto_5

    .line 1842150
    :cond_6
    const/4 v6, 0x0

    goto :goto_6

    .line 1842151
    :cond_7
    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v2

    sub-int v7, v2, v12

    .line 1842152
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feedplugins/attachments/poll/PollAttachmentListPartDefinition;->a:Landroid/content/Context;

    const v3, 0x7f081a48

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 1842153
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feedplugins/attachments/poll/PollAttachmentListPartDefinition;->a:Landroid/content/Context;

    const v3, 0x7f081a4d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 1842154
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feedplugins/attachments/poll/PollAttachmentListPartDefinition;->e:Lcom/facebook/feedplugins/attachments/poll/PollAttachmentMoreActionSelectorPartDefinition;

    new-instance v6, LX/C1F;

    invoke-virtual {v13}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v9

    move-object/from16 v8, p2

    invoke-direct/range {v6 .. v12}, LX/C1F;-><init>(ILcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLNode;ZLjava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1842155
    const/4 v2, 0x0

    return-object v2
.end method


# virtual methods
.method public final bridge synthetic a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1842131
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Ps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/poll/PollAttachmentListPartDefinition;->a(LX/1RF;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ps;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1842130
    const/4 v0, 0x1

    return v0
.end method
