.class public Lcom/facebook/feedplugins/attachments/poll/PollSeeMoreComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/C1O;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final d:LX/C1M;

.field private final e:LX/1V0;

.field private final f:LX/C17;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/C1M;LX/1V0;LX/C17;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1842343
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1842344
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/poll/PollSeeMoreComponentPartDefinition;->d:LX/C1M;

    .line 1842345
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/poll/PollSeeMoreComponentPartDefinition;->e:LX/1V0;

    .line 1842346
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/poll/PollSeeMoreComponentPartDefinition;->f:LX/C17;

    .line 1842347
    return-void
.end method

.method private a(LX/1De;LX/C1O;LX/1Pn;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/C1O;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1842365
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/poll/PollSeeMoreComponentPartDefinition;->d:LX/C1M;

    const/4 v1, 0x0

    .line 1842366
    new-instance v2, LX/C1L;

    invoke-direct {v2, v0}, LX/C1L;-><init>(LX/C1M;)V

    .line 1842367
    sget-object v3, LX/C1M;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/C1K;

    .line 1842368
    if-nez v3, :cond_0

    .line 1842369
    new-instance v3, LX/C1K;

    invoke-direct {v3}, LX/C1K;-><init>()V

    .line 1842370
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/C1K;->a$redex0(LX/C1K;LX/1De;IILX/C1L;)V

    .line 1842371
    move-object v2, v3

    .line 1842372
    move-object v1, v2

    .line 1842373
    move-object v0, v1

    .line 1842374
    iget-object v1, v0, LX/C1K;->a:LX/C1L;

    iput-object p2, v1, LX/C1L;->a:LX/C1O;

    .line 1842375
    iget-object v1, v0, LX/C1K;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1842376
    move-object v0, v0

    .line 1842377
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 1842378
    iget-boolean v1, p2, LX/C1O;->c:Z

    iget-object v2, p2, LX/C1O;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, Lcom/facebook/feedplugins/attachments/poll/PollSeeMoreComponentPartDefinition;->f:LX/C17;

    invoke-static {v1, v2, v3}, LX/C1G;->a(ZLcom/facebook/feed/rows/core/props/FeedProps;LX/C17;)LX/1X6;

    move-result-object v1

    .line 1842379
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/poll/PollSeeMoreComponentPartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/poll/PollSeeMoreComponentPartDefinition;
    .locals 7

    .prologue
    .line 1842354
    const-class v1, Lcom/facebook/feedplugins/attachments/poll/PollSeeMoreComponentPartDefinition;

    monitor-enter v1

    .line 1842355
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/poll/PollSeeMoreComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1842356
    sput-object v2, Lcom/facebook/feedplugins/attachments/poll/PollSeeMoreComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1842357
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1842358
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1842359
    new-instance p0, Lcom/facebook/feedplugins/attachments/poll/PollSeeMoreComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/C1M;->a(LX/0QB;)LX/C1M;

    move-result-object v4

    check-cast v4, LX/C1M;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-static {v0}, LX/C17;->a(LX/0QB;)LX/C17;

    move-result-object v6

    check-cast v6, LX/C17;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/attachments/poll/PollSeeMoreComponentPartDefinition;-><init>(Landroid/content/Context;LX/C1M;LX/1V0;LX/C17;)V

    .line 1842360
    move-object v0, p0

    .line 1842361
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1842362
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/poll/PollSeeMoreComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1842363
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1842364
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1842353
    check-cast p2, LX/C1O;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/poll/PollSeeMoreComponentPartDefinition;->a(LX/1De;LX/C1O;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1842352
    check-cast p2, LX/C1O;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/poll/PollSeeMoreComponentPartDefinition;->a(LX/1De;LX/C1O;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1842350
    check-cast p1, LX/C1O;

    .line 1842351
    iget-object v0, p1, LX/C1O;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1842348
    check-cast p1, LX/C1O;

    .line 1842349
    iget-object v0, p1, LX/C1O;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1WF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method
