.class public Lcom/facebook/feedplugins/attachments/AlbumAttachmentPageComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static e:LX/0Xm;


# instance fields
.field public final b:LX/1nu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1nu",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final c:LX/1qa;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BaD;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1835945
    const-class v0, Lcom/facebook/feedplugins/attachments/AlbumAttachmentPageComponentSpec;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/AlbumAttachmentPageComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;LX/1qa;LX/0Ot;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1nu;",
            "LX/1qa;",
            "LX/0Ot",
            "<",
            "LX/BaD;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1835946
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1835947
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/AlbumAttachmentPageComponentSpec;->b:LX/1nu;

    .line 1835948
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/AlbumAttachmentPageComponentSpec;->c:LX/1qa;

    .line 1835949
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/AlbumAttachmentPageComponentSpec;->d:LX/0Ot;

    .line 1835950
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AlbumAttachmentPageComponentSpec;
    .locals 6

    .prologue
    .line 1835951
    const-class v1, Lcom/facebook/feedplugins/attachments/AlbumAttachmentPageComponentSpec;

    monitor-enter v1

    .line 1835952
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/AlbumAttachmentPageComponentSpec;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1835953
    sput-object v2, Lcom/facebook/feedplugins/attachments/AlbumAttachmentPageComponentSpec;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1835954
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1835955
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1835956
    new-instance v5, Lcom/facebook/feedplugins/attachments/AlbumAttachmentPageComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v3

    check-cast v3, LX/1nu;

    invoke-static {v0}, LX/1qa;->a(LX/0QB;)LX/1qa;

    move-result-object v4

    check-cast v4, LX/1qa;

    const/16 p0, 0x179e

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, Lcom/facebook/feedplugins/attachments/AlbumAttachmentPageComponentSpec;-><init>(LX/1nu;LX/1qa;LX/0Ot;)V

    .line 1835957
    move-object v0, v5

    .line 1835958
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1835959
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/AlbumAttachmentPageComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1835960
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1835961
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
