.class public Lcom/facebook/feedplugins/attachments/BirthdayAvatarAttachmentSelector;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Po;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;

.field private final b:Lcom/facebook/feedplugins/attachments/BirthdayActionComponentPartDefinition;

.field private final c:Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;Lcom/facebook/feedplugins/attachments/BirthdayActionComponentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1836431
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1836432
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/BirthdayAvatarAttachmentSelector;->a:Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;

    .line 1836433
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/BirthdayAvatarAttachmentSelector;->c:Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;

    .line 1836434
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/BirthdayAvatarAttachmentSelector;->b:Lcom/facebook/feedplugins/attachments/BirthdayActionComponentPartDefinition;

    .line 1836435
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/BirthdayAvatarAttachmentSelector;
    .locals 6

    .prologue
    .line 1836436
    const-class v1, Lcom/facebook/feedplugins/attachments/BirthdayAvatarAttachmentSelector;

    monitor-enter v1

    .line 1836437
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/BirthdayAvatarAttachmentSelector;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1836438
    sput-object v2, Lcom/facebook/feedplugins/attachments/BirthdayAvatarAttachmentSelector;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1836439
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1836440
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1836441
    new-instance p0, Lcom/facebook/feedplugins/attachments/BirthdayAvatarAttachmentSelector;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/BirthdayActionComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/BirthdayActionComponentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/attachments/BirthdayActionComponentPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/attachments/BirthdayAvatarAttachmentSelector;-><init>(Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;Lcom/facebook/feedplugins/attachments/BirthdayActionComponentPartDefinition;)V

    .line 1836442
    move-object v0, p0

    .line 1836443
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1836444
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/BirthdayAvatarAttachmentSelector;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1836445
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1836446
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1836447
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Po;

    const/4 v2, 0x0

    .line 1836448
    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    invoke-interface {v0}, LX/1PT;->a()LX/1Qt;

    move-result-object v0

    sget-object v1, LX/1Qt;->FEED:LX/1Qt;

    invoke-virtual {v0, v1}, LX/1Qt;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/BirthdayAvatarAttachmentSelector;->c:Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/BirthdayAvatarAttachmentSelector;->b:Lcom/facebook/feedplugins/attachments/BirthdayActionComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1836449
    :cond_0
    :goto_0
    return-object v2

    .line 1836450
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/BirthdayAvatarAttachmentSelector;->a:Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1836451
    const/4 v0, 0x1

    return v0
.end method
