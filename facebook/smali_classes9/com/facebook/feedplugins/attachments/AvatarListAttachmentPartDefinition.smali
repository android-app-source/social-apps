.class public Lcom/facebook/feedplugins/attachments/AvatarListAttachmentPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/35o;",
        ":",
        "LX/35q;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition",
            "<",
            "LX/1Pf;",
            "TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1836082
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1836083
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/AvatarListAttachmentPartDefinition;->a:Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;

    .line 1836084
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AvatarListAttachmentPartDefinition;
    .locals 4

    .prologue
    .line 1836085
    const-class v1, Lcom/facebook/feedplugins/attachments/AvatarListAttachmentPartDefinition;

    monitor-enter v1

    .line 1836086
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/AvatarListAttachmentPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1836087
    sput-object v2, Lcom/facebook/feedplugins/attachments/AvatarListAttachmentPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1836088
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1836089
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1836090
    new-instance p0, Lcom/facebook/feedplugins/attachments/AvatarListAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/attachments/AvatarListAttachmentPartDefinition;-><init>(Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;)V

    .line 1836091
    move-object v0, p0

    .line 1836092
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1836093
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/AvatarListAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1836094
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1836095
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1836096
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1836097
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1836098
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1836099
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1836100
    iget-object v4, p0, Lcom/facebook/feedplugins/attachments/AvatarListAttachmentPartDefinition;->a:Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;

    invoke-virtual {p2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->b(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1836101
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1836102
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1836103
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1836104
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1836105
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1836106
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
