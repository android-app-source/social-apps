.class public final Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$FacecastScheduledLiveUnsubscribeMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xc8429ee
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$FacecastScheduledLiveUnsubscribeMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$FacecastScheduledLiveUnsubscribeMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1843806
    const-class v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$FacecastScheduledLiveUnsubscribeMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1843807
    const-class v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$FacecastScheduledLiveUnsubscribeMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1843786
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1843787
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1843803
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1843804
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1843805
    return-void
.end method

.method private a()Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1843801
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$FacecastScheduledLiveUnsubscribeMutationModel;->e:Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;

    iput-object v0, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$FacecastScheduledLiveUnsubscribeMutationModel;->e:Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;

    .line 1843802
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$FacecastScheduledLiveUnsubscribeMutationModel;->e:Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1843808
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1843809
    invoke-direct {p0}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$FacecastScheduledLiveUnsubscribeMutationModel;->a()Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1843810
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1843811
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1843812
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1843813
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1843793
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1843794
    invoke-direct {p0}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$FacecastScheduledLiveUnsubscribeMutationModel;->a()Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1843795
    invoke-direct {p0}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$FacecastScheduledLiveUnsubscribeMutationModel;->a()Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;

    .line 1843796
    invoke-direct {p0}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$FacecastScheduledLiveUnsubscribeMutationModel;->a()Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1843797
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$FacecastScheduledLiveUnsubscribeMutationModel;

    .line 1843798
    iput-object v0, v1, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$FacecastScheduledLiveUnsubscribeMutationModel;->e:Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;

    .line 1843799
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1843800
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1843790
    new-instance v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$FacecastScheduledLiveUnsubscribeMutationModel;

    invoke-direct {v0}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$FacecastScheduledLiveUnsubscribeMutationModel;-><init>()V

    .line 1843791
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1843792
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1843789
    const v0, -0x76041210

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1843788
    const v0, -0x2596c7c7

    return v0
.end method
