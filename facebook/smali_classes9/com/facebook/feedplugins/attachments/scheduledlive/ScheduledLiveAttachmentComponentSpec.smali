.class public Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static k:LX/0Xm;


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/2g9;

.field public final d:LX/C1k;

.field public final e:LX/C1v;

.field public final f:LX/Abd;

.field public final g:LX/Abg;

.field private final h:LX/C28;

.field public final i:LX/AVV;

.field public final j:LX/3Hc;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1842919
    const-class v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;

    const-string v1, "scheduled_live"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/2g9;LX/C1k;LX/C1v;LX/Abd;LX/C28;LX/Abg;LX/AVV;LX/3Hc;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/2g9;",
            "LX/C1k;",
            "LX/C1v;",
            "LX/Abd;",
            "LX/C28;",
            "LX/Abg;",
            "LX/AVV;",
            "LX/3Hc;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1843090
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1843091
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->b:LX/0Or;

    .line 1843092
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->c:LX/2g9;

    .line 1843093
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->d:LX/C1k;

    .line 1843094
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->e:LX/C1v;

    .line 1843095
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->f:LX/Abd;

    .line 1843096
    iput-object p6, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->h:LX/C28;

    .line 1843097
    iput-object p7, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->g:LX/Abg;

    .line 1843098
    iput-object p8, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->i:LX/AVV;

    .line 1843099
    iput-object p9, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->j:LX/3Hc;

    .line 1843100
    return-void
.end method

.method private static a(Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pq;Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)LX/1Dg;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;",
            "Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ")",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1843038
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1843039
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    .line 1843040
    if-nez v4, :cond_0

    .line 1843041
    const/4 v3, 0x0

    .line 1843042
    :goto_0
    return-object v3

    .line 1843043
    :cond_0
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMedia;->s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v4

    .line 1843044
    if-nez v4, :cond_1

    .line 1843045
    const/4 v3, 0x0

    goto :goto_0

    .line 1843046
    :cond_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->g:LX/Abg;

    invoke-virtual {v5}, LX/Abg;->c()LX/Abe;

    move-result-object v13

    .line 1843047
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SCHEDULED_CANCELED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v4, v5, :cond_7

    const/4 v4, 0x1

    .line 1843048
    :goto_1
    invoke-static/range {p4 .. p4}, LX/Abd;->e(Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)Ljava/lang/String;

    move-result-object v5

    .line 1843049
    if-nez v5, :cond_2

    .line 1843050
    invoke-static/range {p4 .. p4}, LX/Abd;->d(Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)Ljava/lang/String;

    move-result-object v5

    .line 1843051
    :cond_2
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->f:LX/Abd;

    move-object/from16 v0, p4

    invoke-virtual {v6, v0}, LX/Abd;->a(Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)Z

    move-result v14

    .line 1843052
    const v6, -0x6818e089

    invoke-static {v3, v6}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v6

    .line 1843053
    if-eqz v14, :cond_8

    .line 1843054
    invoke-virtual/range {p5 .. p5}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v8

    .line 1843055
    const/4 v7, 0x0

    .line 1843056
    invoke-static/range {p1 .. p1}, LX/C1g;->e(LX/1De;)LX/1dQ;

    move-result-object v6

    move-object v10, v6

    move v11, v7

    move-object v12, v8

    .line 1843057
    :goto_2
    const/4 v6, 0x0

    .line 1843058
    if-nez v4, :cond_4

    sget-object v7, LX/Abe;->PRELOBBY:LX/Abe;

    if-eq v13, v7, :cond_3

    sget-object v7, LX/Abe;->RUNNING_LATE:LX/Abe;

    if-ne v13, v7, :cond_4

    .line 1843059
    :cond_3
    invoke-static/range {p1 .. p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    sget-object v7, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v6, v7}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v6

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, LX/1ne;->j(I)LX/1ne;

    move-result-object v6

    invoke-virtual/range {p4 .. p4}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->C()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    sget-object v7, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v6, v7}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v6

    const/4 v7, -0x1

    invoke-virtual {v6, v7}, LX/1ne;->m(I)LX/1ne;

    move-result-object v6

    const v7, 0x7f0b0050

    invoke-virtual {v6, v7}, LX/1ne;->q(I)LX/1ne;

    move-result-object v6

    .line 1843060
    :cond_4
    invoke-static/range {p1 .. p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v7

    sget-object v8, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v7, v8}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v7

    const/4 v8, 0x5

    invoke-virtual {v7, v8}, LX/1ne;->j(I)LX/1ne;

    move-result-object v7

    const/4 v8, -0x1

    invoke-virtual {v7, v8}, LX/1ne;->m(I)LX/1ne;

    move-result-object v7

    const v8, 0x7f0b0050

    invoke-virtual {v7, v8}, LX/1ne;->q(I)LX/1ne;

    move-result-object v15

    .line 1843061
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v3

    .line 1843062
    if-eqz v4, :cond_a

    .line 1843063
    invoke-virtual/range {p4 .. p4}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->p()Ljava/lang/String;

    move-result-object v3

    .line 1843064
    sget-object v7, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v15, v7}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    .line 1843065
    :cond_5
    :goto_3
    invoke-virtual {v15, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    .line 1843066
    const/4 v9, 0x0

    .line 1843067
    const/4 v8, 0x0

    .line 1843068
    const/4 v3, 0x0

    .line 1843069
    const/4 v7, 0x0

    .line 1843070
    if-nez v4, :cond_d

    sget-object v4, LX/Abe;->TIMED_OUT:LX/Abe;

    if-eq v13, v4, :cond_d

    .line 1843071
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->c:LX/2g9;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, LX/2g9;->c(LX/1De;)LX/2gA;

    move-result-object v4

    const/16 v7, 0x202

    invoke-virtual {v4, v7}, LX/2gA;->h(I)LX/2gA;

    move-result-object v4

    invoke-virtual {v4, v12}, LX/2gA;->a(Ljava/lang/CharSequence;)LX/2gA;

    move-result-object v4

    invoke-virtual {v4, v11}, LX/2gA;->k(I)LX/2gA;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    invoke-interface {v4, v10}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v4

    const/4 v7, 0x1

    invoke-interface {v4, v7}, LX/1Di;->b(I)LX/1Di;

    move-result-object v4

    const/4 v7, 0x1

    const v8, 0x7f0b0f3e

    invoke-interface {v4, v7, v8}, LX/1Di;->c(II)LX/1Di;

    move-result-object v8

    .line 1843072
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->d:LX/C1k;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, LX/C1k;->c(LX/1De;)LX/C1i;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, LX/C1i;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C1i;

    move-result-object v4

    move-object/from16 v0, p4

    invoke-virtual {v4, v0}, LX/C1i;->a(Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)LX/C1i;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, LX/C1i;->a(LX/1Pq;)LX/C1i;

    move-result-object v7

    .line 1843073
    invoke-virtual/range {p4 .. p4}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->B()Z

    move-result v4

    if-eqz v4, :cond_6

    sget-object v4, LX/Abe;->PRELOBBY:LX/Abe;

    if-ne v13, v4, :cond_6

    .line 1843074
    invoke-static/range {p1 .. p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v4}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v3

    invoke-virtual/range {p4 .. p4}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->v()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, LX/1ne;->m(I)LX/1ne;

    move-result-object v3

    const v4, 0x7f0b004e

    invoke-virtual {v3, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    .line 1843075
    :cond_6
    invoke-static/range {p1 .. p1}, LX/C1g;->e(LX/1De;)LX/1dQ;

    move-result-object v4

    move-object/from16 v16, v4

    move-object v4, v3

    move-object/from16 v3, v16

    .line 1843076
    :goto_4
    invoke-static/range {p1 .. p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v9

    if-eqz v14, :cond_c

    :goto_5
    invoke-interface {v9, v3}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v3

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p4

    invoke-static {v0, v1, v2}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->a(Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;LX/1De;Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)LX/1up;

    move-result-object v9

    invoke-interface {v3, v9}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v9

    invoke-static/range {p1 .. p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v10, 0x1

    invoke-interface {v3, v10}, LX/1Dh;->C(I)LX/1Dh;

    move-result-object v3

    const/16 v10, 0x8

    const/4 v11, 0x0

    invoke-interface {v3, v10, v11}, LX/1Dh;->w(II)LX/1Dh;

    move-result-object v3

    const/4 v10, 0x2

    invoke-interface {v3, v10}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/4 v10, 0x2

    invoke-interface {v3, v10}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v10

    invoke-static/range {p1 .. p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1Ad;

    invoke-virtual {v3, v5}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v3

    sget-object v5, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v5}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v3

    invoke-virtual {v3}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v3

    invoke-virtual {v11, v3}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v3

    invoke-static {}, LX/4Ab;->e()LX/4Ab;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/1up;->a(LX/4Ab;)LX/1up;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v5, 0x7f0b0f3b

    invoke-interface {v3, v5}, LX/1Di;->i(I)LX/1Di;

    move-result-object v3

    const v5, 0x7f0b0f3b

    invoke-interface {v3, v5}, LX/1Di;->q(I)LX/1Di;

    move-result-object v3

    const/4 v5, 0x4

    const v11, 0x7f0b0f3c

    invoke-interface {v3, v5, v11}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    invoke-interface {v10, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static/range {p1 .. p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const/high16 v10, 0x3f800000    # 1.0f

    invoke-interface {v5, v10}, LX/1Dh;->f(F)LX/1Dh;

    move-result-object v5

    const/4 v10, 0x6

    const v11, 0x7f0b0f3d

    invoke-interface {v5, v10, v11}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v7}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v6}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v15}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v8}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v9, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    goto/16 :goto_0

    .line 1843077
    :cond_7
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 1843078
    :cond_8
    invoke-virtual/range {p4 .. p4}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->l()Z

    move-result v7

    if-eqz v7, :cond_9

    .line 1843079
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bw()Ljava/lang/String;

    move-result-object v7

    .line 1843080
    const v6, 0x7f0207da

    .line 1843081
    :goto_6
    invoke-static/range {p1 .. p1}, LX/C1g;->d(LX/1De;)LX/1dQ;

    move-result-object v8

    move-object v10, v8

    move v11, v6

    move-object v12, v7

    goto/16 :goto_2

    .line 1843082
    :cond_9
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v7

    .line 1843083
    const v6, 0x7f0207ad

    goto :goto_6

    .line 1843084
    :cond_a
    sget-object v7, LX/Abe;->RUNNING_LATE:LX/Abe;

    if-ne v13, v7, :cond_b

    .line 1843085
    invoke-virtual/range {p4 .. p4}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->x()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_3

    .line 1843086
    :cond_b
    sget-object v7, LX/Abe;->TIMED_OUT:LX/Abe;

    if-ne v13, v7, :cond_5

    .line 1843087
    invoke-virtual/range {p4 .. p4}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->r()Ljava/lang/String;

    move-result-object v3

    .line 1843088
    sget-object v7, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v15, v7}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    goto/16 :goto_3

    .line 1843089
    :cond_c
    const/4 v3, 0x0

    goto/16 :goto_5

    :cond_d
    move-object v4, v3

    move-object v3, v7

    move-object v7, v8

    move-object v8, v9

    goto/16 :goto_4
.end method

.method public static a(Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;LX/1De;Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)LX/1up;
    .locals 3

    .prologue
    .line 1843033
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    .line 1843034
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->f:LX/Abd;

    invoke-virtual {v1, p2}, LX/Abd;->f(Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)LX/1bf;

    move-result-object v1

    .line 1843035
    sget-object v2, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    .line 1843036
    invoke-virtual {v0}, LX/1Ad;->o()LX/1Ad;

    .line 1843037
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v0

    const v2, 0x3fe38e39

    invoke-virtual {v0, v2}, LX/1up;->c(F)LX/1up;

    move-result-object v0

    sget-object v2, LX/1Up;->g:LX/1Up;

    invoke-virtual {v0, v2}, LX/1up;->b(LX/1Up;)LX/1up;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;
    .locals 13

    .prologue
    .line 1843019
    const-class v1, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;

    monitor-enter v1

    .line 1843020
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1843021
    sput-object v2, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1843022
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1843023
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1843024
    new-instance v3, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;

    const/16 v4, 0x509

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0}, LX/2g9;->a(LX/0QB;)LX/2g9;

    move-result-object v5

    check-cast v5, LX/2g9;

    invoke-static {v0}, LX/C1k;->a(LX/0QB;)LX/C1k;

    move-result-object v6

    check-cast v6, LX/C1k;

    invoke-static {v0}, LX/C1v;->a(LX/0QB;)LX/C1v;

    move-result-object v7

    check-cast v7, LX/C1v;

    invoke-static {v0}, LX/Abd;->a(LX/0QB;)LX/Abd;

    move-result-object v8

    check-cast v8, LX/Abd;

    .line 1843025
    new-instance v11, LX/C28;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v9

    check-cast v9, LX/0tX;

    invoke-static {v0}, LX/1si;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-direct {v11, v9, v10}, LX/C28;-><init>(LX/0tX;Ljava/lang/String;)V

    .line 1843026
    move-object v9, v11

    .line 1843027
    check-cast v9, LX/C28;

    invoke-static {v0}, LX/Abg;->b(LX/0QB;)LX/Abg;

    move-result-object v10

    check-cast v10, LX/Abg;

    invoke-static {v0}, LX/AVV;->a(LX/0QB;)LX/AVV;

    move-result-object v11

    check-cast v11, LX/AVV;

    invoke-static {v0}, LX/3Hc;->b(LX/0QB;)LX/3Hc;

    move-result-object v12

    check-cast v12, LX/3Hc;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;-><init>(LX/0Or;LX/2g9;LX/C1k;LX/C1v;LX/Abd;LX/C28;LX/Abg;LX/AVV;LX/3Hc;)V

    .line 1843028
    move-object v0, v3

    .line 1843029
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1843030
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1843031
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1843032
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pq;LX/1np;)LX/1Dg;
    .locals 11
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1Pq;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;",
            "LX/1np",
            "<",
            "Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;",
            ">;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1842966
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1842967
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1842968
    invoke-static {v0}, LX/Abd;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    move-result-object v1

    .line 1842969
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    .line 1842970
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->R()Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-result-object v3

    if-eqz v3, :cond_0

    if-nez v2, :cond_1

    .line 1842971
    :cond_0
    const/4 v0, 0x0

    .line 1842972
    :goto_0
    return-object v0

    .line 1842973
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->R()Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-result-object v1

    .line 1842974
    iput-object v1, p4, LX/1np;->a:Ljava/lang/Object;

    .line 1842975
    const v1, -0x2fcaae6

    invoke-static {v0, v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v5

    .line 1842976
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->g:LX/Abg;

    .line 1842977
    iget-object v0, p4, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1842978
    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    invoke-virtual {v1, v0}, LX/Abg;->a(Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)V

    .line 1842979
    iget-object v0, p4, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1842980
    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->s()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1842981
    iget-object v0, p4, LX/1np;->a:Ljava/lang/Object;

    move-object v4, v0

    .line 1842982
    check-cast v4, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-static/range {v0 .. v5}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->a(Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pq;Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)LX/1Dg;

    move-result-object v0

    goto :goto_0

    .line 1842983
    :cond_2
    iget-object v0, p4, LX/1np;->a:Ljava/lang/Object;

    move-object v4, v0

    .line 1842984
    check-cast v4, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    const/4 p4, 0x2

    const/4 v7, 0x0

    const/4 p3, 0x1

    const/4 p2, 0x0

    .line 1842985
    iget-object v6, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v6, v6

    .line 1842986
    check-cast v6, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1842987
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v8

    .line 1842988
    if-eqz v8, :cond_3

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_4

    .line 1842989
    :cond_3
    :goto_1
    move-object v0, v7

    .line 1842990
    goto :goto_0

    .line 1842991
    :cond_4
    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v9

    sget-object v10, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v9, v10}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v9

    const/4 v10, 0x5

    invoke-virtual {v9, v10}, LX/1ne;->j(I)LX/1ne;

    move-result-object v9

    const/4 v10, -0x1

    invoke-virtual {v9, v10}, LX/1ne;->m(I)LX/1ne;

    move-result-object v9

    const v10, 0x7f0b0050

    invoke-virtual {v9, v10}, LX/1ne;->q(I)LX/1ne;

    move-result-object v9

    sget-object v10, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v9, v10}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v10

    .line 1842992
    iget-object v9, v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->g:LX/Abg;

    invoke-virtual {v9}, LX/Abg;->c()LX/Abe;

    move-result-object v9

    .line 1842993
    sget-object p0, LX/Abe;->TIMED_OUT:LX/Abe;

    if-ne v9, p0, :cond_7

    .line 1842994
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->r()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v10, v6}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    .line 1842995
    sget-object v6, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v10, v6}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    .line 1842996
    :goto_2
    iget-object v6, v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->c:LX/2g9;

    invoke-virtual {v6, v1}, LX/2g9;->c(LX/1De;)LX/2gA;

    move-result-object v6

    const/16 v9, 0x202

    invoke-virtual {v6, v9}, LX/2gA;->h(I)LX/2gA;

    move-result-object v6

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, LX/2gA;->a(Ljava/lang/CharSequence;)LX/2gA;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    invoke-static {v1}, LX/C1g;->e(LX/1De;)LX/1dQ;

    move-result-object v9

    invoke-interface {v6, v9}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v6

    invoke-interface {v6, p4}, LX/1Di;->b(I)LX/1Di;

    move-result-object v6

    const v9, 0x7f0b0f3e

    invoke-interface {v6, p3, v9}, LX/1Di;->c(II)LX/1Di;

    move-result-object v9

    .line 1842997
    iget-object v6, v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->e:LX/C1v;

    const/4 p0, 0x0

    .line 1842998
    new-instance p1, LX/C1u;

    invoke-direct {p1, v6}, LX/C1u;-><init>(LX/C1v;)V

    .line 1842999
    sget-object v5, LX/C1v;->a:LX/0Zi;

    invoke-virtual {v5}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/C1t;

    .line 1843000
    if-nez v5, :cond_5

    .line 1843001
    new-instance v5, LX/C1t;

    invoke-direct {v5}, LX/C1t;-><init>()V

    .line 1843002
    :cond_5
    invoke-static {v5, v1, p0, p0, p1}, LX/C1t;->a$redex0(LX/C1t;LX/1De;IILX/C1u;)V

    .line 1843003
    move-object p1, v5

    .line 1843004
    move-object p0, p1

    .line 1843005
    move-object v6, p0

    .line 1843006
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v8

    .line 1843007
    iget-object p0, v6, LX/C1t;->a:LX/C1u;

    iput-object v8, p0, LX/C1u;->a:Ljava/lang/String;

    .line 1843008
    iget-object p0, v6, LX/C1t;->d:Ljava/util/BitSet;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Ljava/util/BitSet;->set(I)V

    .line 1843009
    move-object v6, v6

    .line 1843010
    iget-object v8, v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->j:LX/3Hc;

    .line 1843011
    iget-object p0, v6, LX/C1t;->a:LX/C1u;

    iput-object v8, p0, LX/C1u;->b:LX/3Hc;

    .line 1843012
    iget-object p0, v6, LX/C1t;->d:Ljava/util/BitSet;

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Ljava/util/BitSet;->set(I)V

    .line 1843013
    move-object v6, v6

    .line 1843014
    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    invoke-interface {v6, p3}, LX/1Di;->c(I)LX/1Di;

    move-result-object v6

    invoke-interface {v6, p3, p2}, LX/1Di;->k(II)LX/1Di;

    move-result-object v6

    invoke-interface {v6, p2, p2}, LX/1Di;->k(II)LX/1Di;

    move-result-object v6

    const v8, 0x7f0b0060

    invoke-interface {v6, p2, v8}, LX/1Di;->c(II)LX/1Di;

    move-result-object v6

    const v8, 0x7f0b0f40

    invoke-interface {v6, p3, v8}, LX/1Di;->c(II)LX/1Di;

    move-result-object v8

    .line 1843015
    iget-object v6, v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->f:LX/Abd;

    invoke-virtual {v6, v4}, LX/Abd;->a(Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)Z

    move-result p0

    .line 1843016
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p1

    if-eqz p0, :cond_8

    invoke-static {v1}, LX/C1g;->e(LX/1De;)LX/1dQ;

    move-result-object v6

    :goto_3
    invoke-interface {p1, v6}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v6

    invoke-static {v0, v1, v4}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->a(Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;LX/1De;Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)LX/1up;

    move-result-object p1

    invoke-interface {v6, p1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object p1

    if-eqz p0, :cond_9

    move-object v6, v8

    :goto_4
    invoke-interface {p1, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    iget-object v8, v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->d:LX/C1k;

    invoke-virtual {v8, v1}, LX/C1k;->c(LX/1De;)LX/C1i;

    move-result-object v8

    invoke-virtual {v8, v2}, LX/C1i;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C1i;

    move-result-object v8

    invoke-virtual {v8, v4}, LX/C1i;->a(Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)LX/C1i;

    move-result-object v8

    invoke-virtual {v8, v3}, LX/C1i;->a(LX/1Pq;)LX/C1i;

    move-result-object v8

    invoke-interface {v6, v8}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v6

    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v8

    invoke-interface {v8, p3}, LX/1Dh;->C(I)LX/1Dh;

    move-result-object v8

    const/16 p1, 0x8

    invoke-interface {v8, p1, p2}, LX/1Dh;->w(II)LX/1Dh;

    move-result-object v8

    invoke-interface {v8, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v8

    invoke-interface {v8, p4}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v8

    invoke-interface {v8, p3}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v8

    invoke-interface {v8, v10}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v8

    if-eqz p0, :cond_6

    move-object v7, v9

    :cond_6
    invoke-interface {v8, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v7

    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v7

    goto/16 :goto_1

    .line 1843017
    :cond_7
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v10, v6}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    goto/16 :goto_2

    :cond_8
    move-object v6, v7

    .line 1843018
    goto :goto_3

    :cond_9
    move-object v6, v7

    goto :goto_4
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)V
    .locals 13

    .prologue
    .line 1842920
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentSpec;->h:LX/C28;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->l()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->k()Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x1

    .line 1842921
    if-eqz v0, :cond_1

    .line 1842922
    new-instance v3, LX/C1y;

    invoke-direct {v3}, LX/C1y;-><init>()V

    move-object v3, v3

    .line 1842923
    const-string v4, "input"

    invoke-static {v1, v2}, LX/C28;->a(LX/C28;Ljava/lang/String;)LX/4KF;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1842924
    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    .line 1842925
    new-instance v4, LX/C21;

    invoke-direct {v4}, LX/C21;-><init>()V

    invoke-static {v0, v2}, LX/C28;->b(ZLjava/lang/String;)Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;

    move-result-object v5

    .line 1842926
    iput-object v5, v4, LX/C21;->a:Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;

    .line 1842927
    move-object v4, v4

    .line 1842928
    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 1842929
    new-instance v7, LX/186;

    const/16 v8, 0x80

    invoke-direct {v7, v8}, LX/186;-><init>(I)V

    .line 1842930
    iget-object v8, v4, LX/C21;->a:Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;

    invoke-static {v7, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1842931
    invoke-virtual {v7, v11}, LX/186;->c(I)V

    .line 1842932
    invoke-virtual {v7, v10, v8}, LX/186;->b(II)V

    .line 1842933
    invoke-virtual {v7}, LX/186;->d()I

    move-result v8

    .line 1842934
    invoke-virtual {v7, v8}, LX/186;->d(I)V

    .line 1842935
    invoke-virtual {v7}, LX/186;->e()[B

    move-result-object v7

    invoke-static {v7}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v8

    .line 1842936
    invoke-virtual {v8, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1842937
    new-instance v7, LX/15i;

    move-object v10, v9

    move-object v12, v9

    invoke-direct/range {v7 .. v12}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1842938
    new-instance v8, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$FacecastScheduledLiveSubscribeMutationModel;

    invoke-direct {v8, v7}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$FacecastScheduledLiveSubscribeMutationModel;-><init>(LX/15i;)V

    .line 1842939
    move-object v4, v8

    .line 1842940
    invoke-virtual {v3, v4}, LX/399;->a(LX/0jT;)LX/399;

    .line 1842941
    iput-boolean v6, v3, LX/399;->d:Z

    .line 1842942
    iget-object v4, v1, LX/C28;->a:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1842943
    :goto_1
    return-void

    .line 1842944
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1842945
    :cond_1
    new-instance v3, LX/C1z;

    invoke-direct {v3}, LX/C1z;-><init>()V

    move-object v3, v3

    .line 1842946
    const-string v4, "input"

    invoke-static {v1, v2}, LX/C28;->a(LX/C28;Ljava/lang/String;)LX/4KF;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1842947
    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    .line 1842948
    new-instance v4, LX/C22;

    invoke-direct {v4}, LX/C22;-><init>()V

    invoke-static {v0, v2}, LX/C28;->b(ZLjava/lang/String;)Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;

    move-result-object v5

    .line 1842949
    iput-object v5, v4, LX/C22;->a:Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;

    .line 1842950
    move-object v4, v4

    .line 1842951
    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 1842952
    new-instance v7, LX/186;

    const/16 v8, 0x80

    invoke-direct {v7, v8}, LX/186;-><init>(I)V

    .line 1842953
    iget-object v8, v4, LX/C22;->a:Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;

    invoke-static {v7, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1842954
    invoke-virtual {v7, v11}, LX/186;->c(I)V

    .line 1842955
    invoke-virtual {v7, v10, v8}, LX/186;->b(II)V

    .line 1842956
    invoke-virtual {v7}, LX/186;->d()I

    move-result v8

    .line 1842957
    invoke-virtual {v7, v8}, LX/186;->d(I)V

    .line 1842958
    invoke-virtual {v7}, LX/186;->e()[B

    move-result-object v7

    invoke-static {v7}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v8

    .line 1842959
    invoke-virtual {v8, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1842960
    new-instance v7, LX/15i;

    move-object v10, v9

    move-object v12, v9

    invoke-direct/range {v7 .. v12}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1842961
    new-instance v8, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$FacecastScheduledLiveUnsubscribeMutationModel;

    invoke-direct {v8, v7}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$FacecastScheduledLiveUnsubscribeMutationModel;-><init>(LX/15i;)V

    .line 1842962
    move-object v4, v8

    .line 1842963
    invoke-virtual {v3, v4}, LX/399;->a(LX/0jT;)LX/399;

    .line 1842964
    iput-boolean v6, v3, LX/399;->d:Z

    .line 1842965
    iget-object v4, v1, LX/C28;->a:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_1
.end method
