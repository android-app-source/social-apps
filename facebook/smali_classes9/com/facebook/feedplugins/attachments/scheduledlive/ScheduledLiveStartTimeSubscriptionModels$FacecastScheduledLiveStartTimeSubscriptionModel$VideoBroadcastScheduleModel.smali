.class public final Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5b8e1397
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:J


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1843383
    const-class v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1843384
    const-class v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1843385
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1843386
    return-void
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 1843387
    iput-wide p1, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->k:J

    .line 1843388
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1843389
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1843390
    if-eqz v0, :cond_0

    .line 1843391
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2, p1, p2}, LX/15i;->b(IIJ)V

    .line 1843392
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1843393
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->e:Ljava/lang/String;

    .line 1843394
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1843395
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1843396
    if-eqz v0, :cond_0

    .line 1843397
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1843398
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1843399
    iput-boolean p1, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->g:Z

    .line 1843400
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1843401
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1843402
    if-eqz v0, :cond_0

    .line 1843403
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1843404
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1843318
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->h:Ljava/lang/String;

    .line 1843319
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1843320
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1843321
    if-eqz v0, :cond_0

    .line 1843322
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1843323
    :cond_0
    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1843405
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->i:Ljava/lang/String;

    .line 1843406
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1843407
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1843408
    if-eqz v0, :cond_0

    .line 1843409
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1843410
    :cond_0
    return-void
.end method

.method private d(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1843411
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->j:Ljava/lang/String;

    .line 1843412
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1843413
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1843414
    if-eqz v0, :cond_0

    .line 1843415
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1843416
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1843417
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1843418
    invoke-virtual {p0}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1843419
    invoke-virtual {p0}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1843420
    invoke-virtual {p0}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1843421
    invoke-virtual {p0}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1843422
    invoke-virtual {p0}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1843423
    const/4 v5, 0x7

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1843424
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1843425
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1843426
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1843427
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1843428
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1843429
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1843430
    const/4 v1, 0x6

    iget-wide v2, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->k:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1843431
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1843432
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1843433
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1843434
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1843435
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1843436
    new-instance v0, LX/C1p;

    invoke-direct {v0, p1}, LX/C1p;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1843382
    invoke-virtual {p0}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 1843437
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1843438
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->g:Z

    .line 1843439
    const/4 v0, 0x6

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->k:J

    .line 1843440
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 2

    .prologue
    .line 1843356
    const-string v0, "formatted_start_time"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1843357
    invoke-virtual {p0}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1843358
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1843359
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 1843360
    :goto_0
    return-void

    .line 1843361
    :cond_0
    const-string v0, "is_rescheduled"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1843362
    invoke-virtual {p0}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->l()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1843363
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1843364
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1843365
    :cond_1
    const-string v0, "rescheduled_endscreen_body"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1843366
    invoke-virtual {p0}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->m()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1843367
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1843368
    const/4 v0, 0x3

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1843369
    :cond_2
    const-string v0, "rescheduled_endscreen_title"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1843370
    invoke-virtual {p0}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->n()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1843371
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1843372
    const/4 v0, 0x4

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1843373
    :cond_3
    const-string v0, "rescheduled_heading"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1843374
    invoke-virtual {p0}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->o()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1843375
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1843376
    const/4 v0, 0x5

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1843377
    :cond_4
    const-string v0, "start_time"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1843378
    invoke-virtual {p0}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->p()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1843379
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1843380
    const/4 v0, 0x6

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1843381
    :cond_5
    invoke-virtual {p2}, LX/18L;->a()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 1843343
    const-string v0, "formatted_start_time"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1843344
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->a(Ljava/lang/String;)V

    .line 1843345
    :cond_0
    :goto_0
    return-void

    .line 1843346
    :cond_1
    const-string v0, "is_rescheduled"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1843347
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->a(Z)V

    goto :goto_0

    .line 1843348
    :cond_2
    const-string v0, "rescheduled_endscreen_body"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1843349
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 1843350
    :cond_3
    const-string v0, "rescheduled_endscreen_title"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1843351
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 1843352
    :cond_4
    const-string v0, "rescheduled_heading"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1843353
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 1843354
    :cond_5
    const-string v0, "start_time"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1843355
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->a(J)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1843340
    new-instance v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;

    invoke-direct {v0}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;-><init>()V

    .line 1843341
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1843342
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1843339
    const v0, 0x4e847a86

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1843338
    const v0, 0x56118b5d    # 4.0006937E13f

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1843336
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->e:Ljava/lang/String;

    .line 1843337
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1843334
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->f:Ljava/lang/String;

    .line 1843335
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 1843332
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1843333
    iget-boolean v0, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->g:Z

    return v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1843330
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->h:Ljava/lang/String;

    .line 1843331
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1843328
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->i:Ljava/lang/String;

    .line 1843329
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1843326
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->j:Ljava/lang/String;

    .line 1843327
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final p()J
    .locals 2

    .prologue
    .line 1843324
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1843325
    iget-wide v0, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->k:J

    return-wide v0
.end method
