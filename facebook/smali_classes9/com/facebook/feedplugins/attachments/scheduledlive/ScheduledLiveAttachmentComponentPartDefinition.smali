.class public Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final d:LX/1b4;

.field private final e:LX/C1g;

.field private final f:LX/1V0;

.field private final g:LX/C1d;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1b4;LX/C1g;LX/1V0;LX/C1d;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1842904
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1842905
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentPartDefinition;->d:LX/1b4;

    .line 1842906
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentPartDefinition;->e:LX/C1g;

    .line 1842907
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentPartDefinition;->f:LX/1V0;

    .line 1842908
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentPartDefinition;->g:LX/C1d;

    .line 1842909
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1842885
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentPartDefinition;->e:LX/C1g;

    const/4 v1, 0x0

    .line 1842886
    new-instance v2, LX/C1f;

    invoke-direct {v2, v0}, LX/C1f;-><init>(LX/C1g;)V

    .line 1842887
    iget-object v3, v0, LX/C1g;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/C1e;

    .line 1842888
    if-nez v3, :cond_0

    .line 1842889
    new-instance v3, LX/C1e;

    invoke-direct {v3, v0}, LX/C1e;-><init>(LX/C1g;)V

    .line 1842890
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/C1e;->a$redex0(LX/C1e;LX/1De;IILX/C1f;)V

    .line 1842891
    move-object v2, v3

    .line 1842892
    move-object v1, v2

    .line 1842893
    move-object v0, v1

    .line 1842894
    iget-object v1, v0, LX/C1e;->a:LX/C1f;

    iput-object p2, v1, LX/C1f;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1842895
    iget-object v1, v0, LX/C1e;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1842896
    move-object v1, v0

    .line 1842897
    move-object v0, p3

    check-cast v0, LX/1Pq;

    .line 1842898
    iget-object v2, v1, LX/C1e;->a:LX/C1f;

    iput-object v0, v2, LX/C1f;->b:LX/1Pq;

    .line 1842899
    iget-object v2, v1, LX/C1e;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1842900
    move-object v0, v1

    .line 1842901
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 1842902
    new-instance v1, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    sget-object v3, LX/1Ua;->e:LX/1Ua;

    const v4, 0x7f020a48

    const/4 v5, -0x1

    invoke-direct {v1, v2, v3, v4, v5}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    .line 1842903
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentPartDefinition;
    .locals 9

    .prologue
    .line 1842874
    const-class v1, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentPartDefinition;

    monitor-enter v1

    .line 1842875
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1842876
    sput-object v2, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1842877
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1842878
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1842879
    new-instance v3, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1b4;->b(LX/0QB;)LX/1b4;

    move-result-object v5

    check-cast v5, LX/1b4;

    invoke-static {v0}, LX/C1g;->a(LX/0QB;)LX/C1g;

    move-result-object v6

    check-cast v6, LX/C1g;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v7

    check-cast v7, LX/1V0;

    invoke-static {v0}, LX/C1d;->b(LX/0QB;)LX/C1d;

    move-result-object v8

    check-cast v8, LX/C1d;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentPartDefinition;-><init>(Landroid/content/Context;LX/1b4;LX/C1g;LX/1V0;LX/C1d;)V

    .line 1842880
    move-object v0, v3

    .line 1842881
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1842882
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1842883
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1842884
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/1dV;",
            "TE;",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1842871
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(Ljava/lang/Object;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V

    .line 1842872
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentPartDefinition;->g:LX/C1d;

    invoke-virtual {v0, p1}, LX/C1d;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1842873
    return-void
.end method

.method private b(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/1dV;",
            "TE;",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1842865
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/components/feed/ComponentPartDefinition;->b(Ljava/lang/Object;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V

    .line 1842866
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentPartDefinition;->g:LX/C1d;

    invoke-virtual {v0, p1}, LX/C1d;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1842867
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1842870
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1842910
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V
    .locals 0

    .prologue
    .line 1842869
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x39ae231

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1842868
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/1dV;

    check-cast p3, LX/1Pn;

    check-cast p4, Lcom/facebook/components/feed/FeedComponentView;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V

    const/16 v1, 0x1f

    const v2, -0x759dec46

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1842862
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentPartDefinition;->d:LX/1b4;

    .line 1842863
    iget-object v1, v0, LX/1b4;->b:LX/0Uh;

    const/16 p0, 0x35f

    const/4 p1, 0x0

    invoke-virtual {v1, p0, p1}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 1842864
    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1842860
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1842861
    invoke-static {p1}, LX/1WF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic b(Ljava/lang/Object;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V
    .locals 0

    .prologue
    .line 1842859
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentPartDefinition;->b(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V

    return-void
.end method

.method public final bridge synthetic b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1842858
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/1dV;

    check-cast p3, LX/1Pn;

    check-cast p4, Lcom/facebook/components/feed/FeedComponentView;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentPartDefinition;->b(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V

    return-void
.end method
