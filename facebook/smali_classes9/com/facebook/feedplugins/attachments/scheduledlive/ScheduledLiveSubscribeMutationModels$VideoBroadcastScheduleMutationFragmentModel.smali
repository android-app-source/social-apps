.class public final Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4d6555ad
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1843869
    const-class v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1843870
    const-class v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1843871
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1843872
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1843842
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1843843
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1843844
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1843873
    iput-boolean p1, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;->f:Z

    .line 1843874
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1843875
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1843876
    if-eqz v0, :cond_0

    .line 1843877
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1843878
    :cond_0
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1843879
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;->e:Ljava/lang/String;

    .line 1843880
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Z
    .locals 2

    .prologue
    .line 1843881
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1843882
    iget-boolean v0, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;->f:Z

    return v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1843859
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1843860
    invoke-direct {p0}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1843861
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1843862
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1843863
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;->f:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1843864
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1843865
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1843866
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1843867
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1843868
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1843858
    new-instance v0, LX/C24;

    invoke-direct {v0, p1}, LX/C24;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1843857
    invoke-direct {p0}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1843854
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1843855
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;->f:Z

    .line 1843856
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1843848
    const-string v0, "is_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1843849
    invoke-direct {p0}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;->k()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1843850
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1843851
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 1843852
    :goto_0
    return-void

    .line 1843853
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1843845
    const-string v0, "is_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1843846
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;->a(Z)V

    .line 1843847
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1843839
    new-instance v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;

    invoke-direct {v0}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveSubscribeMutationModels$VideoBroadcastScheduleMutationFragmentModel;-><init>()V

    .line 1843840
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1843841
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1843838
    const v0, -0x20b5f52c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1843837
    const v0, 0x56118b5d    # 4.0006937E13f

    return v0
.end method
