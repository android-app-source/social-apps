.class public Lcom/facebook/feedplugins/attachments/meme/MemeAttachmentComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/C0i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/C0i",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:LX/1V0;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/C0i;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1841362
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1841363
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/meme/MemeAttachmentComponentPartDefinition;->d:LX/C0i;

    .line 1841364
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/meme/MemeAttachmentComponentPartDefinition;->e:LX/1V0;

    .line 1841365
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1841344
    new-instance v0, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    sget-object v2, LX/1Ua;->a:LX/1Ua;

    const v3, 0x7f020a3c

    const/4 v4, -0x1

    invoke-direct {v0, v1, v2, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    .line 1841345
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/meme/MemeAttachmentComponentPartDefinition;->d:LX/C0i;

    const/4 v2, 0x0

    .line 1841346
    new-instance v3, LX/C0h;

    invoke-direct {v3, v1}, LX/C0h;-><init>(LX/C0i;)V

    .line 1841347
    iget-object v4, v1, LX/C0i;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/C0g;

    .line 1841348
    if-nez v4, :cond_0

    .line 1841349
    new-instance v4, LX/C0g;

    invoke-direct {v4, v1}, LX/C0g;-><init>(LX/C0i;)V

    .line 1841350
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/C0g;->a$redex0(LX/C0g;LX/1De;IILX/C0h;)V

    .line 1841351
    move-object v3, v4

    .line 1841352
    move-object v2, v3

    .line 1841353
    move-object v1, v2

    .line 1841354
    iget-object v2, v1, LX/C0g;->a:LX/C0h;

    iput-object p2, v2, LX/C0h;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1841355
    iget-object v2, v1, LX/C0g;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1841356
    move-object v1, v1

    .line 1841357
    iget-object v2, v1, LX/C0g;->a:LX/C0h;

    iput-object p3, v2, LX/C0h;->b:LX/1Pb;

    .line 1841358
    iget-object v2, v1, LX/C0g;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1841359
    move-object v1, v1

    .line 1841360
    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 1841361
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/meme/MemeAttachmentComponentPartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v0, v1}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/meme/MemeAttachmentComponentPartDefinition;
    .locals 6

    .prologue
    .line 1841373
    const-class v1, Lcom/facebook/feedplugins/attachments/meme/MemeAttachmentComponentPartDefinition;

    monitor-enter v1

    .line 1841374
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/meme/MemeAttachmentComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1841375
    sput-object v2, Lcom/facebook/feedplugins/attachments/meme/MemeAttachmentComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1841376
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1841377
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1841378
    new-instance p0, Lcom/facebook/feedplugins/attachments/meme/MemeAttachmentComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/C0i;->a(LX/0QB;)LX/C0i;

    move-result-object v4

    check-cast v4, LX/C0i;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/attachments/meme/MemeAttachmentComponentPartDefinition;-><init>(Landroid/content/Context;LX/C0i;LX/1V0;)V

    .line 1841379
    move-object v0, p0

    .line 1841380
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1841381
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/meme/MemeAttachmentComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1841382
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1841383
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1841371
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/meme/MemeAttachmentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1841372
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/meme/MemeAttachmentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1841368
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1841369
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1841370
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/1VO;->q(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1841366
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1841367
    invoke-static {p1}, LX/1WF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method
