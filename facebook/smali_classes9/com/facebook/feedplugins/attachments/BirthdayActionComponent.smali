.class public Lcom/facebook/feedplugins/attachments/BirthdayActionComponent;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Bxr;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "Lcom/facebook/feedplugins/attachments/BirthdayActionComponent",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Bxr;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1836148
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1836149
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/feedplugins/attachments/BirthdayActionComponent;->b:LX/0Zi;

    .line 1836150
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/BirthdayActionComponent;->a:LX/0Ot;

    .line 1836151
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/BirthdayActionComponent;
    .locals 4

    .prologue
    .line 1836152
    const-class v1, Lcom/facebook/feedplugins/attachments/BirthdayActionComponent;

    monitor-enter v1

    .line 1836153
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/BirthdayActionComponent;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1836154
    sput-object v2, Lcom/facebook/feedplugins/attachments/BirthdayActionComponent;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1836155
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1836156
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1836157
    new-instance v3, Lcom/facebook/feedplugins/attachments/BirthdayActionComponent;

    const/16 p0, 0x1e14

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, Lcom/facebook/feedplugins/attachments/BirthdayActionComponent;-><init>(LX/0Ot;)V

    .line 1836158
    move-object v0, v3

    .line 1836159
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1836160
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/BirthdayActionComponent;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1836161
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1836162
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1836163
    const v0, 0xaf306b7

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1836164
    check-cast p2, LX/Bxp;

    .line 1836165
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/BirthdayActionComponent;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bxr;

    iget-object v1, p2, LX/Bxp;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/Bxp;->b:LX/1Pn;

    const/4 p2, 0x0

    .line 1836166
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1836167
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1836168
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    iget-object v4, v0, LX/Bxr;->b:LX/Ap5;

    invoke-virtual {v4, p1}, LX/Ap5;->c(LX/1De;)LX/Ap4;

    move-result-object v4

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4, p0}, LX/Ap4;->a(Ljava/lang/CharSequence;)LX/Ap4;

    move-result-object p0

    iget-object v4, v0, LX/Bxr;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/36S;

    invoke-static {v1}, LX/36S;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/Ap4;->b(Ljava/lang/CharSequence;)LX/Ap4;

    move-result-object p0

    iget-object v4, v0, LX/Bxr;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/36S;

    invoke-virtual {v4, v1}, LX/36S;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/text/Spannable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/Ap4;->c(Ljava/lang/CharSequence;)LX/Ap4;

    move-result-object v4

    check-cast v2, LX/1Po;

    invoke-virtual {v4, v2}, LX/Ap4;->a(LX/1Po;)LX/Ap4;

    move-result-object p0

    .line 1836169
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1836170
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1836171
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->bC()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->bC()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->bC()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->bC()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1836172
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->bC()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1836173
    :goto_0
    move-object v4, v2

    .line 1836174
    invoke-virtual {p0, v4}, LX/Ap4;->a(Landroid/net/Uri;)LX/Ap4;

    move-result-object v4

    invoke-virtual {v4, p2}, LX/Ap4;->h(I)LX/Ap4;

    move-result-object v4

    iget-object p0, v0, LX/Bxr;->c:LX/ApL;

    invoke-virtual {p0, p1}, LX/ApL;->c(LX/1De;)LX/ApK;

    move-result-object p0

    .line 1836175
    invoke-static {v3}, LX/36S;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p2

    if-eqz p2, :cond_2

    invoke-static {v3}, LX/36S;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_2

    .line 1836176
    invoke-static {v3}, LX/36S;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p2

    .line 1836177
    :goto_1
    move-object v3, p2

    .line 1836178
    invoke-virtual {p0, v3}, LX/ApK;->a(Landroid/net/Uri;)LX/ApK;

    move-result-object v3

    const/4 p0, 0x2

    invoke-virtual {v3, p0}, LX/ApK;->h(I)LX/ApK;

    move-result-object v3

    invoke-static {p1}, Lcom/facebook/common/callercontext/CallerContext;->a(Landroid/content/Context;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object p0

    .line 1836179
    iget-object p2, v3, LX/ApK;->a:Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;

    iput-object p0, p2, Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 1836180
    move-object v3, v3

    .line 1836181
    iget-object p0, v4, LX/Ap4;->a:Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;

    invoke-virtual {v3}, LX/1X5;->d()LX/1X1;

    move-result-object p2

    iput-object p2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->k:LX/1X1;

    .line 1836182
    move-object v3, v4

    .line 1836183
    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    .line 1836184
    const v4, 0xaf306b7

    const/4 p0, 0x0

    invoke-static {p1, v4, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 1836185
    invoke-interface {v3, v4}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v3

    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, LX/Bxr;->d:LX/Bxu;

    const/4 v5, 0x0

    .line 1836186
    new-instance p0, LX/Bxs;

    invoke-direct {p0, v4}, LX/Bxs;-><init>(LX/Bxu;)V

    .line 1836187
    iget-object p2, v4, LX/Bxu;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/Bxt;

    .line 1836188
    if-nez p2, :cond_0

    .line 1836189
    new-instance p2, LX/Bxt;

    invoke-direct {p2, v4}, LX/Bxt;-><init>(LX/Bxu;)V

    .line 1836190
    :cond_0
    invoke-static {p2, p1, v5, v5, p0}, LX/Bxt;->a$redex0(LX/Bxt;LX/1De;IILX/Bxs;)V

    .line 1836191
    move-object p0, p2

    .line 1836192
    move-object v5, p0

    .line 1836193
    move-object v4, v5

    .line 1836194
    iget-object v5, v4, LX/Bxt;->a:LX/Bxs;

    iput-object v1, v5, LX/Bxs;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1836195
    iget-object v5, v4, LX/Bxt;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v5, p0}, Ljava/util/BitSet;->set(I)V

    .line 1836196
    move-object v4, v4

    .line 1836197
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    .line 1836198
    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1836199
    return-object v0

    :cond_1
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_2
    const/4 p2, 0x0

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1836200
    invoke-static {}, LX/1dS;->b()V

    .line 1836201
    iget v0, p1, LX/1dQ;->b:I

    .line 1836202
    packed-switch v0, :pswitch_data_0

    .line 1836203
    :goto_0
    return-object v2

    .line 1836204
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1836205
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1836206
    check-cast v1, LX/Bxp;

    .line 1836207
    iget-object v3, p0, Lcom/facebook/feedplugins/attachments/BirthdayActionComponent;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Bxr;

    iget-object p1, v1, LX/Bxp;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1836208
    iget-object p2, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p2, p2

    .line 1836209
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object p2

    .line 1836210
    sget-object p0, LX/0ax;->bE:Ljava/lang/String;

    invoke-static {p0, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 1836211
    iget-object p0, v3, LX/Bxr;->e:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {p0, v1, p2}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 1836212
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0xaf306b7
        :pswitch_0
    .end packed-switch
.end method
