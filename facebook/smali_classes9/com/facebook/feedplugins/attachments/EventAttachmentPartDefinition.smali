.class public Lcom/facebook/feedplugins/attachments/EventAttachmentPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoViewPartDefinition;

.field private final b:Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoComponentPartDefinition;

.field private final c:Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;

.field private final d:Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewComponentPartDefinition;

.field private final e:Lcom/facebook/feedplugins/attachments/EventTicketAttachmentFooterViewComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoViewPartDefinition;Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoComponentPartDefinition;Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewComponentPartDefinition;Lcom/facebook/feedplugins/attachments/EventTicketAttachmentFooterViewComponentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1836858
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1836859
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentPartDefinition;->a:Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoViewPartDefinition;

    .line 1836860
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoComponentPartDefinition;

    .line 1836861
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;

    .line 1836862
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewComponentPartDefinition;

    .line 1836863
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentPartDefinition;->e:Lcom/facebook/feedplugins/attachments/EventTicketAttachmentFooterViewComponentPartDefinition;

    .line 1836864
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/EventAttachmentPartDefinition;
    .locals 9

    .prologue
    .line 1836865
    const-class v1, Lcom/facebook/feedplugins/attachments/EventAttachmentPartDefinition;

    monitor-enter v1

    .line 1836866
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/EventAttachmentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1836867
    sput-object v2, Lcom/facebook/feedplugins/attachments/EventAttachmentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1836868
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1836869
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1836870
    new-instance v3, Lcom/facebook/feedplugins/attachments/EventAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoViewPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoViewPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoViewPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoComponentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewComponentPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentFooterViewComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/EventTicketAttachmentFooterViewComponentPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentFooterViewComponentPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/attachments/EventAttachmentPartDefinition;-><init>(Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoViewPartDefinition;Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoComponentPartDefinition;Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewComponentPartDefinition;Lcom/facebook/feedplugins/attachments/EventTicketAttachmentFooterViewComponentPartDefinition;)V

    .line 1836871
    move-object v0, v3

    .line 1836872
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1836873
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/EventAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1836874
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1836875
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1836876
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1836877
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoComponentPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentPartDefinition;->a:Lcom/facebook/feedplugins/attachments/EventAttachmentPhotoViewPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 1836878
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentPartDefinition;->e:Lcom/facebook/feedplugins/attachments/EventTicketAttachmentFooterViewComponentPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/EventAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/attachments/EventAttachmentFooterViewComponentPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 1836879
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1836880
    const/4 v0, 0x1

    return v0
.end method
