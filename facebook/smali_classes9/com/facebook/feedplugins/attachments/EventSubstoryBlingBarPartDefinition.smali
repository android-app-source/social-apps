.class public Lcom/facebook/feedplugins/attachments/EventSubstoryBlingBarPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Po;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/Anm;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1837032
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1837033
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/EventSubstoryBlingBarPartDefinition;->a:Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;

    .line 1837034
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/EventSubstoryBlingBarPartDefinition;
    .locals 4

    .prologue
    .line 1837039
    const-class v1, Lcom/facebook/feedplugins/attachments/EventSubstoryBlingBarPartDefinition;

    monitor-enter v1

    .line 1837040
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/EventSubstoryBlingBarPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1837041
    sput-object v2, Lcom/facebook/feedplugins/attachments/EventSubstoryBlingBarPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1837042
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1837043
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1837044
    new-instance p0, Lcom/facebook/feedplugins/attachments/EventSubstoryBlingBarPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/attachments/EventSubstoryBlingBarPartDefinition;-><init>(Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;)V

    .line 1837045
    move-object v0, p0

    .line 1837046
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1837047
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/EventSubstoryBlingBarPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1837048
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1837049
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1837035
    new-instance v0, LX/ByI;

    invoke-direct {v0, p0}, LX/ByI;-><init>(Lcom/facebook/feedplugins/attachments/EventSubstoryBlingBarPartDefinition;)V

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1837036
    check-cast p2, LX/Anm;

    .line 1837037
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/EventSubstoryBlingBarPartDefinition;->a:Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;

    new-instance v1, LX/Anm;

    iget-object v2, p2, LX/Anm;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, LX/Anm;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Z)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1837038
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x4ee31e5    # 5.5999352E-36f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1837028
    check-cast p4, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;

    .line 1837029
    const v1, 0x7f0a00d5

    invoke-virtual {p4, v1}, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->setBackgroundResource(I)V

    .line 1837030
    const/16 v1, 0x1f

    const v2, -0x6c3b9ff7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1837031
    const/4 v0, 0x1

    return v0
.end method
