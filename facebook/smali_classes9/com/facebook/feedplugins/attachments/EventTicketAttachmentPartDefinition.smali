.class public Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Boolean;",
        "TE;",
        "LX/3VX;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static l:LX/0Xm;


# instance fields
.field public final b:LX/1qb;

.field private final c:LX/6RZ;

.field public final d:Landroid/content/Context;

.field public final e:LX/0Zb;

.field public final f:LX/1xP;

.field private final g:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final h:Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

.field private final i:Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition",
            "<",
            "LX/3VX;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

.field private final k:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1837247
    const-class v0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;

    const-string v1, "newsfeed_angora_attachment_view"

    const-string v2, "native_newsfeed"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1qb;LX/6RZ;Landroid/content/Context;LX/0Zb;LX/1xP;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1837235
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1837236
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;->b:LX/1qb;

    .line 1837237
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;->c:LX/6RZ;

    .line 1837238
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;->d:Landroid/content/Context;

    .line 1837239
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;->e:LX/0Zb;

    .line 1837240
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;->f:LX/1xP;

    .line 1837241
    iput-object p6, p0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;->g:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    .line 1837242
    iput-object p7, p0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;->h:Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    .line 1837243
    iput-object p8, p0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;->i:Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;

    .line 1837244
    iput-object p9, p0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;->j:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    .line 1837245
    iput-object p10, p0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;->k:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 1837246
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;
    .locals 14

    .prologue
    .line 1837224
    const-class v1, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;

    monitor-enter v1

    .line 1837225
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1837226
    sput-object v2, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1837227
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1837228
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1837229
    new-instance v3, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;

    invoke-static {v0}, LX/1qb;->a(LX/0QB;)LX/1qb;

    move-result-object v4

    check-cast v4, LX/1qb;

    invoke-static {v0}, LX/6RZ;->a(LX/0QB;)LX/6RZ;

    move-result-object v5

    check-cast v5, LX/6RZ;

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    invoke-static {v0}, LX/1xP;->a(LX/0QB;)LX/1xP;

    move-result-object v8

    check-cast v8, LX/1xP;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    move-result-object v12

    check-cast v12, Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v13

    check-cast v13, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-direct/range {v3 .. v13}, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;-><init>(LX/1qb;LX/6RZ;Landroid/content/Context;LX/0Zb;LX/1xP;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V

    .line 1837230
    move-object v0, v3

    .line 1837231
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1837232
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1837233
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1837234
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/CharSequence;
    .locals 10
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 1837248
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v6

    .line 1837249
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v4, v5

    move-object v1, v3

    move-object v2, v3

    :goto_0
    if-ge v4, v7, :cond_0

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 1837250
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    if-eqz v8, :cond_5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v8

    const v9, -0x6001edea

    if-ne v8, v9, :cond_5

    .line 1837251
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v0

    .line 1837252
    if-eqz v0, :cond_5

    .line 1837253
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEvent;->bn()J

    move-result-wide v8

    invoke-static {v8, v9}, LX/5O7;->c(J)Ljava/util/Date;

    move-result-object v1

    .line 1837254
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEvent;->Z()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    invoke-static {v0}, LX/Bnh;->a(Lcom/facebook/graphql/model/GraphQLPlace;)Ljava/lang/String;

    move-result-object v0

    .line 1837255
    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-object v2, v1

    move-object v1, v0

    goto :goto_0

    .line 1837256
    :cond_0
    if-eqz v2, :cond_2

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;->c:LX/6RZ;

    invoke-virtual {v0, v2}, LX/6RZ;->i(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 1837257
    :goto_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1837258
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081a42

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object v1, v4, v0

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1837259
    :cond_1
    :goto_3
    return-object v0

    :cond_2
    move-object v0, v3

    .line 1837260
    goto :goto_2

    .line 1837261
    :cond_3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1837262
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    move-object v0, v1

    .line 1837263
    goto :goto_3

    :cond_4
    move-object v0, v3

    .line 1837264
    goto :goto_3

    :cond_5
    move-object v0, v1

    move-object v1, v2

    goto :goto_1
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 1837223
    sget-object v0, LX/3VX;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1837182
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const v5, 0x3ff745d1

    .line 1837183
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1837184
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1837185
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;->h:Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    invoke-interface {p1, v1, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1837186
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;->g:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    new-instance v2, LX/2ya;

    invoke-direct {v2, p2}, LX/2ya;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1837187
    const p3, 0x3ff745d1

    .line 1837188
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-static {v0}, LX/1VO;->t(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;->b:LX/1qb;

    invoke-static {v0}, LX/1VO;->t(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v1, v2, p3}, LX/1qb;->a(Lcom/facebook/graphql/model/GraphQLImage;F)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1837189
    invoke-static {v0}, LX/1VO;->t(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1837190
    :goto_0
    move-object v1, v1

    .line 1837191
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;->b:LX/1qb;

    invoke-virtual {v2, v1, v5}, LX/1qb;->a(Lcom/facebook/graphql/model/GraphQLImage;F)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1837192
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;->i:Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;

    new-instance v3, LX/36Q;

    sget-object v4, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v3, v1, v4, v5}, LX/36Q;-><init>(Lcom/facebook/graphql/model/GraphQLImage;Lcom/facebook/common/callercontext/CallerContext;F)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1837193
    :cond_0
    const v1, 0x7f0d0e95

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;->j:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1837194
    const v1, 0x7f0d0e96

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;->j:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    invoke-direct {p0, v0}, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1837195
    const v1, 0x7f0d0e97

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;->j:Lcom/facebook/multirow/parts/TextOrHiddenPartDefinition;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1837196
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1837197
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1837198
    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1837199
    :cond_1
    const/4 v0, 0x0

    .line 1837200
    :goto_2
    move-object v0, v0

    .line 1837201
    const v1, 0x7f0d0e98

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;->k:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 1837202
    if-nez v0, :cond_8

    .line 1837203
    const/4 v3, 0x0

    .line 1837204
    :goto_3
    move-object v3, v3

    .line 1837205
    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1837206
    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_4
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 1837207
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 1837208
    :cond_3
    const/4 v0, 0x0

    goto :goto_4

    .line 1837209
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v3

    .line 1837210
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_5
    if-ge v2, v4, :cond_6

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 1837211
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    if-eqz v6, :cond_5

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v6

    const v7, -0x6001edea

    if-ne v6, v7, :cond_5

    .line 1837212
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v1

    .line 1837213
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEvent;->E()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v6

    if-eqz v6, :cond_5

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEvent;->E()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v6

    if-eqz v6, :cond_5

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEvent;->E()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;->b:LX/1qb;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEvent;->E()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    invoke-virtual {v6, v7, p3}, LX/1qb;->a(Lcom/facebook/graphql/model/GraphQLImage;F)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1837214
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEvent;->E()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    goto/16 :goto_0

    .line 1837215
    :cond_5
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_5

    .line 1837216
    :cond_6
    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_7
    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    :cond_8
    new-instance v3, LX/ByO;

    invoke-direct {v3, p0, p2, v0}, LX/ByO;-><init>(Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)V

    goto/16 :goto_3
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x2ae7ac76

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1837218
    check-cast p2, Ljava/lang/Boolean;

    check-cast p4, LX/3VX;

    .line 1837219
    iget-object v1, p4, LX/3VX;->d:Lcom/facebook/resources/ui/FbButton;

    move-object v2, v1

    .line 1837220
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 1837221
    const/16 v1, 0x1f

    const v2, 0x11ca2ea6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1837222
    :cond_0
    const/16 v1, 0x8

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1837217
    const/4 v0, 0x1

    return v0
.end method
