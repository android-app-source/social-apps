.class public Lcom/facebook/feedplugins/attachments/PollAttachmentSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/2yX;",
        ":",
        "LX/2yW;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/QuestionAttachmentPartDefinition",
            "<",
            "LX/1Pf;",
            "TV;>;>;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/poll/PollAttachmentListPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/poll/PollAttachmentListPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/QuestionAttachmentPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1837620
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1837621
    move-object v0, p1

    .line 1837622
    iput-object v0, p0, Lcom/facebook/feedplugins/attachments/PollAttachmentSelectorPartDefinition;->b:LX/0Ot;

    .line 1837623
    move-object v0, p2

    .line 1837624
    iput-object v0, p0, Lcom/facebook/feedplugins/attachments/PollAttachmentSelectorPartDefinition;->a:LX/0Ot;

    .line 1837625
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/PollAttachmentSelectorPartDefinition;
    .locals 5

    .prologue
    .line 1837626
    const-class v1, Lcom/facebook/feedplugins/attachments/PollAttachmentSelectorPartDefinition;

    monitor-enter v1

    .line 1837627
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/PollAttachmentSelectorPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1837628
    sput-object v2, Lcom/facebook/feedplugins/attachments/PollAttachmentSelectorPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1837629
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1837630
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1837631
    new-instance v3, Lcom/facebook/feedplugins/attachments/PollAttachmentSelectorPartDefinition;

    const/16 v4, 0x1e89

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x1e2e

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, Lcom/facebook/feedplugins/attachments/PollAttachmentSelectorPartDefinition;-><init>(LX/0Ot;LX/0Ot;)V

    .line 1837632
    move-object v0, v3

    .line 1837633
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1837634
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/PollAttachmentSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1837635
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1837636
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1837637
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1837638
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/PollAttachmentSelectorPartDefinition;->b:LX/0Ot;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/PollAttachmentSelectorPartDefinition;->a:LX/0Ot;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    .line 1837639
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1837640
    const/4 v0, 0x1

    return v0
.end method
