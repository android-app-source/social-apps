.class public Lcom/facebook/feedplugins/attachments/BirthdayActionComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:Lcom/facebook/feedplugins/attachments/BirthdayActionComponent;

.field private final e:LX/1V0;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/feedplugins/attachments/BirthdayActionComponent;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1836247
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1836248
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/BirthdayActionComponentPartDefinition;->d:Lcom/facebook/feedplugins/attachments/BirthdayActionComponent;

    .line 1836249
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/BirthdayActionComponentPartDefinition;->e:LX/1V0;

    .line 1836250
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1836229
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/BirthdayActionComponentPartDefinition;->d:Lcom/facebook/feedplugins/attachments/BirthdayActionComponent;

    const/4 v1, 0x0

    .line 1836230
    new-instance v2, LX/Bxp;

    invoke-direct {v2, v0}, LX/Bxp;-><init>(Lcom/facebook/feedplugins/attachments/BirthdayActionComponent;)V

    .line 1836231
    iget-object v3, v0, Lcom/facebook/feedplugins/attachments/BirthdayActionComponent;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Bxq;

    .line 1836232
    if-nez v3, :cond_0

    .line 1836233
    new-instance v3, LX/Bxq;

    invoke-direct {v3, v0}, LX/Bxq;-><init>(Lcom/facebook/feedplugins/attachments/BirthdayActionComponent;)V

    .line 1836234
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/Bxq;->a$redex0(LX/Bxq;LX/1De;IILX/Bxp;)V

    .line 1836235
    move-object v2, v3

    .line 1836236
    move-object v1, v2

    .line 1836237
    move-object v0, v1

    .line 1836238
    iget-object v1, v0, LX/Bxq;->a:LX/Bxp;

    iput-object p2, v1, LX/Bxp;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1836239
    iget-object v1, v0, LX/Bxq;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1836240
    move-object v0, v0

    .line 1836241
    iget-object v1, v0, LX/Bxq;->a:LX/Bxp;

    iput-object p3, v1, LX/Bxp;->b:LX/1Pn;

    .line 1836242
    iget-object v1, v0, LX/Bxq;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1836243
    move-object v0, v0

    .line 1836244
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 1836245
    new-instance v1, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    sget-object v3, LX/1Ua;->n:LX/1Ua;

    const v4, 0x7f020137

    const/4 v5, -0x1

    invoke-direct {v1, v2, v3, v4, v5}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    .line 1836246
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/BirthdayActionComponentPartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/BirthdayActionComponentPartDefinition;
    .locals 6

    .prologue
    .line 1836218
    const-class v1, Lcom/facebook/feedplugins/attachments/BirthdayActionComponentPartDefinition;

    monitor-enter v1

    .line 1836219
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/BirthdayActionComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1836220
    sput-object v2, Lcom/facebook/feedplugins/attachments/BirthdayActionComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1836221
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1836222
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1836223
    new-instance p0, Lcom/facebook/feedplugins/attachments/BirthdayActionComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/BirthdayActionComponent;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/BirthdayActionComponent;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/attachments/BirthdayActionComponent;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/attachments/BirthdayActionComponentPartDefinition;-><init>(Landroid/content/Context;Lcom/facebook/feedplugins/attachments/BirthdayActionComponent;LX/1V0;)V

    .line 1836224
    move-object v0, p0

    .line 1836225
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1836226
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/BirthdayActionComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1836227
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1836228
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1836251
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/BirthdayActionComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1836217
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/BirthdayActionComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 1836213
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 1836214
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1836215
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1836216
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/components/feed/ComponentPartDefinition;->c:LX/0ad;

    sget-short v2, LX/0wi;->d:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/components/feed/ComponentPartDefinition;->c:LX/0ad;

    sget-short v2, LX/0wi;->e:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
