.class public Lcom/facebook/feedplugins/attachments/PhotoLayoutPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/drawee/view/DraweeView;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1837581
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x59bbae15

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1837582
    check-cast p1, Ljava/lang/Integer;

    check-cast p4, Lcom/facebook/drawee/view/DraweeView;

    .line 1837583
    invoke-virtual {p4}, Lcom/facebook/drawee/view/DraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 1837584
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1837585
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1837586
    invoke-virtual {p4, v1}, Lcom/facebook/drawee/view/DraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1837587
    const/16 v1, 0x1f

    const v2, -0x6e88a2ee

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1837588
    check-cast p4, Lcom/facebook/drawee/view/DraweeView;

    const/4 v1, -0x1

    .line 1837589
    invoke-virtual {p4}, Lcom/facebook/drawee/view/DraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1837590
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1837591
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1837592
    invoke-virtual {p4, v0}, Lcom/facebook/drawee/view/DraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1837593
    return-void
.end method
