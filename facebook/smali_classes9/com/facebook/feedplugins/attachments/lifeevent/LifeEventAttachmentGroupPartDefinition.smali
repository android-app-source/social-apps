.class public Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderIconPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderIconPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderTitleTextComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderTitleTextComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderSubtitleTextComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderSubtitleTextComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderUnderSubtitleTextComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderUnderSubtitleTextComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentDescriptionComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentDescriptionComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/1VL;

.field private final g:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderIconPartDefinition;Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderTitleTextComponentPartDefinition;Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderSubtitleTextComponentPartDefinition;Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderUnderSubtitleTextComponentPartDefinition;Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentDescriptionComponentPartDefinition;LX/1VL;Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1838194
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1838195
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentGroupPartDefinition;->a:Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderIconPartDefinition;

    .line 1838196
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentGroupPartDefinition;->b:Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderTitleTextComponentPartDefinition;

    .line 1838197
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentGroupPartDefinition;->c:Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderSubtitleTextComponentPartDefinition;

    .line 1838198
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentGroupPartDefinition;->d:Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderUnderSubtitleTextComponentPartDefinition;

    .line 1838199
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentGroupPartDefinition;->e:Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentDescriptionComponentPartDefinition;

    .line 1838200
    iput-object p6, p0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentGroupPartDefinition;->f:LX/1VL;

    .line 1838201
    iput-object p7, p0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentGroupPartDefinition;->g:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;

    .line 1838202
    iput-object p8, p0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentGroupPartDefinition;->h:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    .line 1838203
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentGroupPartDefinition;
    .locals 12

    .prologue
    .line 1838204
    const-class v1, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentGroupPartDefinition;

    monitor-enter v1

    .line 1838205
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentGroupPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1838206
    sput-object v2, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentGroupPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1838207
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1838208
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1838209
    new-instance v3, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderIconPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderIconPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderIconPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderTitleTextComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderTitleTextComponentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderTitleTextComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderSubtitleTextComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderSubtitleTextComponentPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderSubtitleTextComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderUnderSubtitleTextComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderUnderSubtitleTextComponentPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderUnderSubtitleTextComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentDescriptionComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentDescriptionComponentPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentDescriptionComponentPartDefinition;

    invoke-static {v0}, LX/1VL;->b(LX/0QB;)LX/1VL;

    move-result-object v9

    check-cast v9, LX/1VL;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentGroupPartDefinition;-><init>(Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderIconPartDefinition;Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderTitleTextComponentPartDefinition;Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderSubtitleTextComponentPartDefinition;Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderUnderSubtitleTextComponentPartDefinition;Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentDescriptionComponentPartDefinition;LX/1VL;Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;)V

    .line 1838210
    move-object v0, v3

    .line 1838211
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1838212
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1838213
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1838214
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1838215
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1838216
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1838217
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1838218
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentGroupPartDefinition;->a:Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderIconPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1838219
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentGroupPartDefinition;->b:Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderTitleTextComponentPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1838220
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentGroupPartDefinition;->c:Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderSubtitleTextComponentPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1838221
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentGroupPartDefinition;->d:Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderUnderSubtitleTextComponentPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1838222
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentGroupPartDefinition;->e:Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentDescriptionComponentPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1838223
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1838224
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentGroupPartDefinition;->f:LX/1VL;

    invoke-virtual {v1, v0}, LX/1VL;->f(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1838225
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentGroupPartDefinition;->h:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1838226
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 1838227
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentGroupPartDefinition;->g:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1838228
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1838229
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1838230
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1838231
    invoke-static {v0}, LX/1VO;->h(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1838232
    const/4 v0, 0x0

    .line 1838233
    :goto_0
    return v0

    .line 1838234
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-nez v1, :cond_2

    .line 1838235
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 1838236
    :cond_2
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentGroupPartDefinition;->f:LX/1VL;

    invoke-virtual {v1, v0}, LX/1VL;->f(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1838237
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentGroupPartDefinition;->h:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    invoke-virtual {v0, p1}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    goto :goto_0

    .line 1838238
    :cond_3
    invoke-static {p1}, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    goto :goto_0
.end method
