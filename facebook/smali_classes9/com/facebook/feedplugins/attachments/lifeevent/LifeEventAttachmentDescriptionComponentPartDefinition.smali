.class public Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentDescriptionComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/1V0;

.field private final e:LX/BzG;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/BzG",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/BzG;LX/1V0;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1838105
    invoke-direct {p0, p3}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1838106
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentDescriptionComponentPartDefinition;->e:LX/BzG;

    .line 1838107
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentDescriptionComponentPartDefinition;->d:LX/1V0;

    .line 1838108
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1838109
    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1838110
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentDescriptionComponentPartDefinition;->e:LX/BzG;

    const/4 v2, 0x0

    .line 1838111
    new-instance v3, LX/BzF;

    invoke-direct {v3, v0}, LX/BzF;-><init>(LX/BzG;)V

    .line 1838112
    iget-object v4, v0, LX/BzG;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/BzE;

    .line 1838113
    if-nez v4, :cond_0

    .line 1838114
    new-instance v4, LX/BzE;

    invoke-direct {v4, v0}, LX/BzE;-><init>(LX/BzG;)V

    .line 1838115
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/BzE;->a$redex0(LX/BzE;LX/1De;IILX/BzF;)V

    .line 1838116
    move-object v3, v4

    .line 1838117
    move-object v2, v3

    .line 1838118
    move-object v0, v2

    .line 1838119
    iget-object v2, v0, LX/BzE;->a:LX/BzF;

    iput-object p2, v2, LX/BzF;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1838120
    iget-object v2, v0, LX/BzE;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1838121
    move-object v0, v0

    .line 1838122
    iget-object v2, v0, LX/BzE;->a:LX/BzF;

    iput-object v1, v2, LX/BzF;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1838123
    iget-object v2, v0, LX/BzE;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1838124
    move-object v2, v0

    .line 1838125
    move-object v0, p3

    check-cast v0, LX/1Pq;

    .line 1838126
    iget-object v3, v2, LX/BzE;->a:LX/BzF;

    iput-object v0, v3, LX/BzF;->c:LX/1Pq;

    .line 1838127
    iget-object v3, v2, LX/BzE;->e:Ljava/util/BitSet;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 1838128
    move-object v0, v2

    .line 1838129
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 1838130
    new-instance v2, LX/1X6;

    invoke-static {}, LX/1UY;->g()LX/1UY;

    move-result-object v3

    const/high16 v4, 0x40800000    # 4.0f

    .line 1838131
    iput v4, v3, LX/1UY;->b:F

    .line 1838132
    move-object v3, v3

    .line 1838133
    const/high16 v4, 0x41000000    # 8.0f

    .line 1838134
    iput v4, v3, LX/1UY;->c:F

    .line 1838135
    move-object v3, v3

    .line 1838136
    invoke-virtual {v3}, LX/1UY;->i()LX/1Ua;

    move-result-object v3

    invoke-direct {v2, v1, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 1838137
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentDescriptionComponentPartDefinition;->d:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentDescriptionComponentPartDefinition;
    .locals 6

    .prologue
    .line 1838138
    const-class v1, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentDescriptionComponentPartDefinition;

    monitor-enter v1

    .line 1838139
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentDescriptionComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1838140
    sput-object v2, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentDescriptionComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1838141
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1838142
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1838143
    new-instance p0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentDescriptionComponentPartDefinition;

    invoke-static {v0}, LX/BzG;->a(LX/0QB;)LX/BzG;

    move-result-object v3

    check-cast v3, LX/BzG;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v4

    check-cast v4, LX/1V0;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentDescriptionComponentPartDefinition;-><init>(LX/BzG;LX/1V0;Landroid/content/Context;)V

    .line 1838144
    move-object v0, p0

    .line 1838145
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1838146
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentDescriptionComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1838147
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1838148
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1838149
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentDescriptionComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1838150
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentDescriptionComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1838151
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1838152
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1838153
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1838154
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 1838155
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1838156
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1838157
    invoke-static {p1}, LX/1WF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 1838158
    sget-object v0, Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
