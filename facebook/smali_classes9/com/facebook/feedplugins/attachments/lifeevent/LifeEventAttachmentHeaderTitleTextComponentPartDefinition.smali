.class public Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderTitleTextComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/1V0;

.field private final e:LX/BzL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/BzL",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1V0;LX/BzL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1838347
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1838348
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderTitleTextComponentPartDefinition;->e:LX/BzL;

    .line 1838349
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderTitleTextComponentPartDefinition;->d:LX/1V0;

    .line 1838350
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1838311
    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1838312
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderTitleTextComponentPartDefinition;->e:LX/BzL;

    invoke-virtual {v0, p1}, LX/BzL;->c(LX/1De;)LX/BzJ;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/BzJ;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/BzJ;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/BzJ;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/BzJ;

    move-result-object v2

    move-object v0, p3

    check-cast v0, LX/1Pq;

    invoke-virtual {v2, v0}, LX/BzJ;->a(LX/1Pq;)LX/BzJ;

    move-result-object v2

    .line 1838313
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1838314
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const-string v3, "Title"

    invoke-static {v0, v3}, LX/BzD;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/BzJ;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/BzJ;

    move-result-object v0

    const v2, 0x7f0b0052

    invoke-virtual {v0, v2}, LX/BzJ;->h(I)LX/BzJ;

    move-result-object v0

    const/high16 v2, -0x1000000

    .line 1838315
    iget-object v3, v0, LX/BzJ;->a:LX/BzK;

    iput v2, v3, LX/BzK;->h:I

    .line 1838316
    iget-object v3, v0, LX/BzJ;->e:Ljava/util/BitSet;

    const/4 v4, 0x7

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 1838317
    move-object v0, v0

    .line 1838318
    const-string v2, "Title"

    invoke-virtual {v0, v2}, LX/BzJ;->b(Ljava/lang/String;)LX/BzJ;

    move-result-object v0

    sget-object v2, LX/1eK;->TITLE:LX/1eK;

    invoke-virtual {v0, v2}, LX/BzJ;->a(LX/1eK;)LX/BzJ;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 1838319
    new-instance v2, LX/1X6;

    invoke-static {}, LX/1UY;->g()LX/1UY;

    move-result-object v3

    const/high16 v4, 0x40800000    # 4.0f

    .line 1838320
    iput v4, v3, LX/1UY;->b:F

    .line 1838321
    move-object v3, v3

    .line 1838322
    const/4 v4, 0x0

    .line 1838323
    iput v4, v3, LX/1UY;->c:F

    .line 1838324
    move-object v3, v3

    .line 1838325
    invoke-virtual {v3}, LX/1UY;->i()LX/1Ua;

    move-result-object v3

    invoke-direct {v2, v1, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 1838326
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderTitleTextComponentPartDefinition;->d:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderTitleTextComponentPartDefinition;
    .locals 6

    .prologue
    .line 1838336
    const-class v1, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderTitleTextComponentPartDefinition;

    monitor-enter v1

    .line 1838337
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderTitleTextComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1838338
    sput-object v2, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderTitleTextComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1838339
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1838340
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1838341
    new-instance p0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderTitleTextComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v4

    check-cast v4, LX/1V0;

    invoke-static {v0}, LX/BzL;->a(LX/0QB;)LX/BzL;

    move-result-object v5

    check-cast v5, LX/BzL;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderTitleTextComponentPartDefinition;-><init>(Landroid/content/Context;LX/1V0;LX/BzL;)V

    .line 1838342
    move-object v0, p0

    .line 1838343
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1838344
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderTitleTextComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1838345
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1838346
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1838335
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderTitleTextComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1838334
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentHeaderTitleTextComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1838330
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1838331
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1838332
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const-string v1, "Title"

    invoke-static {v0, v1}, LX/BzD;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 1838333
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1838328
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1838329
    invoke-static {p1}, LX/1WF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 1838327
    sget-object v0, Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
