.class public Lcom/facebook/feedplugins/attachments/NoteAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pn;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/2yX;",
        ":",
        "LX/35q;",
        ":",
        "LX/2yW;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;TV;>;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static i:LX/0Xm;


# instance fields
.field public final b:LX/1Uj;

.field private final c:LX/1qb;

.field private final d:Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final g:Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

.field private final h:Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1837540
    const-class v0, Lcom/facebook/feedplugins/attachments/NoteAttachmentPartDefinition;

    const-string v1, "newsfeed_angora_attachment_view"

    const-string v2, "large_photo"

    const-string v3, "native_newsfeed"

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/NoteAttachmentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1qb;LX/1Uj;Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1837572
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1837573
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/NoteAttachmentPartDefinition;->c:LX/1qb;

    .line 1837574
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/NoteAttachmentPartDefinition;->b:LX/1Uj;

    .line 1837575
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/NoteAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;

    .line 1837576
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/NoteAttachmentPartDefinition;->e:Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;

    .line 1837577
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/NoteAttachmentPartDefinition;->f:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    .line 1837578
    iput-object p6, p0, Lcom/facebook/feedplugins/attachments/NoteAttachmentPartDefinition;->g:Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    .line 1837579
    iput-object p7, p0, Lcom/facebook/feedplugins/attachments/NoteAttachmentPartDefinition;->h:Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;

    .line 1837580
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/NoteAttachmentPartDefinition;
    .locals 11

    .prologue
    .line 1837561
    const-class v1, Lcom/facebook/feedplugins/attachments/NoteAttachmentPartDefinition;

    monitor-enter v1

    .line 1837562
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/NoteAttachmentPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1837563
    sput-object v2, Lcom/facebook/feedplugins/attachments/NoteAttachmentPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1837564
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1837565
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1837566
    new-instance v3, Lcom/facebook/feedplugins/attachments/NoteAttachmentPartDefinition;

    invoke-static {v0}, LX/1qb;->a(LX/0QB;)LX/1qb;

    move-result-object v4

    check-cast v4, LX/1qb;

    invoke-static {v0}, LX/1Uj;->a(LX/0QB;)LX/1Uj;

    move-result-object v5

    check-cast v5, LX/1Uj;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feedplugins/attachments/NoteAttachmentPartDefinition;-><init>(LX/1qb;LX/1Uj;Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;)V

    .line 1837567
    move-object v0, v3

    .line 1837568
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1837569
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/NoteAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1837570
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1837571
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 1837560
    sget-object v0, Lcom/facebook/attachments/angora/AngoraAttachmentView;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 1837542
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v6, 0x0

    const v5, 0x3ff745d1

    .line 1837543
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1837544
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1837545
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/NoteAttachmentPartDefinition;->e:Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;

    new-instance v2, LX/2yZ;

    .line 1837546
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1837547
    const-string v3, ""

    invoke-static {v3}, Landroid/text/SpannableStringBuilder;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    .line 1837548
    :goto_0
    move-object v3, v3

    .line 1837549
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/SpannableStringBuilder;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    :goto_1
    move-object v4, v4

    .line 1837550
    invoke-direct {v2, v3, v4}, LX/2yZ;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1837551
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/NoteAttachmentPartDefinition;->g:Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    invoke-interface {p1, v1, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1837552
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/NoteAttachmentPartDefinition;->f:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    new-instance v2, LX/2ya;

    invoke-direct {v2, p2}, LX/2ya;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1837553
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    :goto_2
    move-object v0, v1

    .line 1837554
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/NoteAttachmentPartDefinition;->c:LX/1qb;

    invoke-virtual {v1, v0, v5}, LX/1qb;->a(Lcom/facebook/graphql/model/GraphQLImage;F)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1837555
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/NoteAttachmentPartDefinition;->h:Lcom/facebook/feedplugins/attachments/LargeImagePartDefinition;

    new-instance v2, LX/36Q;

    sget-object v3, Lcom/facebook/feedplugins/attachments/NoteAttachmentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v2, v0, v3, v5}, LX/36Q;-><init>(Lcom/facebook/graphql/model/GraphQLImage;Lcom/facebook/common/callercontext/CallerContext;F)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1837556
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/NoteAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;

    invoke-interface {p1, v0, v6}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1837557
    return-object v6

    .line 1837558
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/SpannableStringBuilder;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    .line 1837559
    iget-object v4, p0, Lcom/facebook/feedplugins/attachments/NoteAttachmentPartDefinition;->b:LX/1Uj;

    invoke-virtual {v4}, LX/1Uj;->a()Landroid/text/style/MetricAffectingSpan;

    move-result-object v4

    const/4 v7, 0x0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    const/16 p3, 0x11

    invoke-virtual {v3, v4, v7, v8, p3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1837541
    const/4 v0, 0x1

    return v0
.end method
