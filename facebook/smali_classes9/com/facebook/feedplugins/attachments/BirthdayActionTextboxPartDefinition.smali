.class public Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Po;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/3Wj;",
        ">;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field public final a:LX/0ad;

.field public final b:Landroid/content/Context;

.field public final c:Landroid/app/Activity;

.field public final d:LX/1Kf;

.field private final e:Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

.field private final f:Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;

.field public final g:LX/6VI;

.field public final h:LX/CEF;


# direct methods
.method public constructor <init>(LX/0ad;Landroid/content/Context;Landroid/app/Activity;LX/1Kf;Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;LX/6VI;LX/CEF;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1836417
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1836418
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;->a:LX/0ad;

    .line 1836419
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;->b:Landroid/content/Context;

    .line 1836420
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;->c:Landroid/app/Activity;

    .line 1836421
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;->d:LX/1Kf;

    .line 1836422
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;->e:Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    .line 1836423
    iput-object p6, p0, Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;->f:Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;

    .line 1836424
    iput-object p7, p0, Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;->g:LX/6VI;

    .line 1836425
    iput-object p8, p0, Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;->h:LX/CEF;

    .line 1836426
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;
    .locals 12

    .prologue
    .line 1836390
    const-class v1, Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;

    monitor-enter v1

    .line 1836391
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1836392
    sput-object v2, Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1836393
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1836394
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1836395
    new-instance v3, Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {v0}, LX/0kU;->b(LX/0QB;)Landroid/app/Activity;

    move-result-object v6

    check-cast v6, Landroid/app/Activity;

    invoke-static {v0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v7

    check-cast v7, LX/1Kf;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;

    invoke-static {v0}, LX/6VI;->b(LX/0QB;)LX/6VI;

    move-result-object v10

    check-cast v10, LX/6VI;

    invoke-static {v0}, LX/CEF;->b(LX/0QB;)LX/CEF;

    move-result-object v11

    check-cast v11, LX/CEF;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;-><init>(LX/0ad;Landroid/content/Context;Landroid/app/Activity;LX/1Kf;Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;LX/6VI;LX/CEF;)V

    .line 1836396
    move-object v0, v3

    .line 1836397
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1836398
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1836399
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1836400
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/3Wj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1836416
    sget-object v0, LX/3Wj;->c:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1836427
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1836428
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;->e:Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1836429
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;->f:Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1836430
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x241732ae

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1836405
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p4, LX/3Wj;

    .line 1836406
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1836407
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1836408
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;->a:LX/0ad;

    sget-char v4, LX/0wi;->c:C

    iget-object p2, p0, Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;->b:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const p3, 0x7f081a58

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-interface {v2, v4, p2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p4, v2}, LX/3Wj;->setActionTextBoxLabel(Ljava/lang/String;)V

    .line 1836409
    const/4 v2, 0x0

    invoke-virtual {p4, v2}, LX/3Wj;->setActionTextBoxVisibility(I)V

    .line 1836410
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v1

    .line 1836411
    new-instance v4, LX/Bxw;

    invoke-direct {v4, p0, v2, v1}, LX/Bxw;-><init>(Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v4

    .line 1836412
    invoke-virtual {p4, v1}, LX/3Wj;->setActionTextBoxOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1836413
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;->g:LX/6VI;

    .line 1836414
    iget-object v2, v1, LX/6VI;->a:LX/0Zb;

    sget-object v4, LX/6VH;->IMPRESSION:LX/6VH;

    invoke-static {v1, v4}, LX/6VI;->a(LX/6VI;LX/6VH;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v2, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1836415
    const/16 v1, 0x1f

    const v2, -0x115c28fe

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 1836401
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 1836402
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1836403
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1836404
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;->a:LX/0ad;

    sget-short v2, LX/0wi;->d:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/BirthdayActionTextboxPartDefinition;->a:LX/0ad;

    sget-short v2, LX/0wi;->e:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1836384
    check-cast p4, LX/3Wj;

    const/4 v1, 0x0

    .line 1836385
    invoke-virtual {p4, v1}, LX/3Wj;->setActionTextBoxLabel(Ljava/lang/String;)V

    .line 1836386
    const/16 v0, 0x8

    invoke-virtual {p4, v0}, LX/3Wj;->setActionTextBoxVisibility(I)V

    .line 1836387
    invoke-virtual {p4, v1}, LX/3Wj;->setActionTextBoxOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1836388
    invoke-virtual {p4}, LX/35n;->a()V

    .line 1836389
    return-void
.end method
