.class public final Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/1xF;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

.field public b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/7Dj;

.field public d:Landroid/view/View$OnClickListener;

.field public e:LX/2oV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2oV",
            "<",
            "LX/8xF;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/1aZ;

.field public g:Lcom/facebook/common/callercontext/CallerContext;

.field public final synthetic h:LX/1xF;


# direct methods
.method public constructor <init>(LX/1xF;)V
    .locals 1

    .prologue
    .line 1841805
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->h:LX/1xF;

    .line 1841806
    move-object v0, p1

    .line 1841807
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1841808
    sget-object v0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    iput-object v0, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->g:Lcom/facebook/common/callercontext/CallerContext;

    .line 1841809
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1841810
    const-string v0, "SphericalPhotoAttachmentComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1841811
    if-ne p0, p1, :cond_1

    .line 1841812
    :cond_0
    :goto_0
    return v0

    .line 1841813
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1841814
    goto :goto_0

    .line 1841815
    :cond_3
    check-cast p1, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;

    .line 1841816
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1841817
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1841818
    if-eq v2, v3, :cond_0

    .line 1841819
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->a:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->a:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    iget-object v3, p1, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->a:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    invoke-virtual {v2, v3}, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1841820
    goto :goto_0

    .line 1841821
    :cond_5
    iget-object v2, p1, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->a:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    if-nez v2, :cond_4

    .line 1841822
    :cond_6
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1841823
    goto :goto_0

    .line 1841824
    :cond_8
    iget-object v2, p1, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_7

    .line 1841825
    :cond_9
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->c:LX/7Dj;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->c:LX/7Dj;

    iget-object v3, p1, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->c:LX/7Dj;

    invoke-virtual {v2, v3}, LX/7Dj;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 1841826
    goto :goto_0

    .line 1841827
    :cond_b
    iget-object v2, p1, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->c:LX/7Dj;

    if-nez v2, :cond_a

    .line 1841828
    :cond_c
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->d:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->d:Landroid/view/View$OnClickListener;

    iget-object v3, p1, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 1841829
    goto :goto_0

    .line 1841830
    :cond_e
    iget-object v2, p1, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->d:Landroid/view/View$OnClickListener;

    if-nez v2, :cond_d

    .line 1841831
    :cond_f
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->e:LX/2oV;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->e:LX/2oV;

    iget-object v3, p1, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->e:LX/2oV;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    .line 1841832
    goto :goto_0

    .line 1841833
    :cond_11
    iget-object v2, p1, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->e:LX/2oV;

    if-nez v2, :cond_10

    .line 1841834
    :cond_12
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->f:LX/1aZ;

    if-eqz v2, :cond_14

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->f:LX/1aZ;

    iget-object v3, p1, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->f:LX/1aZ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    :cond_13
    move v0, v1

    .line 1841835
    goto/16 :goto_0

    .line 1841836
    :cond_14
    iget-object v2, p1, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->f:LX/1aZ;

    if-nez v2, :cond_13

    .line 1841837
    :cond_15
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->g:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_16

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->g:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p1, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->g:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1841838
    goto/16 :goto_0

    .line 1841839
    :cond_16
    iget-object v2, p1, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->g:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
