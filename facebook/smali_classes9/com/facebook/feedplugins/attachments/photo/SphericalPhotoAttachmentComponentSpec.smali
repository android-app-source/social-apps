.class public Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/MountSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pv;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static d:LX/0Xm;


# instance fields
.field public final b:LX/1AV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1AV",
            "<",
            "LX/8xF;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0yc;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1841885
    const-class v0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentSpec;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1AV;LX/0yc;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1841886
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1841887
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentSpec;->b:LX/1AV;

    .line 1841888
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentSpec;->c:LX/0yc;

    .line 1841889
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentSpec;
    .locals 5

    .prologue
    .line 1841890
    const-class v1, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentSpec;

    monitor-enter v1

    .line 1841891
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentSpec;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1841892
    sput-object v2, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentSpec;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1841893
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1841894
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1841895
    new-instance p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentSpec;

    invoke-static {v0}, LX/1AV;->a(LX/0QB;)LX/1AV;

    move-result-object v3

    check-cast v3, LX/1AV;

    invoke-static {v0}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v4

    check-cast v4, LX/0yc;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentSpec;-><init>(LX/1AV;LX/0yc;)V

    .line 1841896
    move-object v0, p0

    .line 1841897
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1841898
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1841899
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1841900
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
