.class public Lcom/facebook/feedplugins/attachments/VideoViewCountTextPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/resources/ui/FbTextView;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final b:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/parts/TextPartDefinition;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1837983
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1837984
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/VideoViewCountTextPartDefinition;->a:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 1837985
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/VideoViewCountTextPartDefinition;->b:Landroid/content/res/Resources;

    .line 1837986
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/VideoViewCountTextPartDefinition;
    .locals 5

    .prologue
    .line 1837987
    const-class v1, Lcom/facebook/feedplugins/attachments/VideoViewCountTextPartDefinition;

    monitor-enter v1

    .line 1837988
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/VideoViewCountTextPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1837989
    sput-object v2, Lcom/facebook/feedplugins/attachments/VideoViewCountTextPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1837990
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1837991
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1837992
    new-instance p0, Lcom/facebook/feedplugins/attachments/VideoViewCountTextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/attachments/VideoViewCountTextPartDefinition;-><init>(Lcom/facebook/multirow/parts/TextPartDefinition;Landroid/content/res/Resources;)V

    .line 1837993
    move-object v0, p0

    .line 1837994
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1837995
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/VideoViewCountTextPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1837996
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1837997
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1837998
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1837999
    invoke-static {p2}, LX/1VO;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1838000
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v3, 0x4ed245b

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1838001
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->aO()I

    move-result v0

    .line 1838002
    iget-object v3, p0, Lcom/facebook/feedplugins/attachments/VideoViewCountTextPartDefinition;->a:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v4, p0, Lcom/facebook/feedplugins/attachments/VideoViewCountTextPartDefinition;->b:Landroid/content/res/Resources;

    const v5, 0x7f0f006f

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v1, v2

    invoke-virtual {v4, v5, v0, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v3, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1838003
    const/4 v0, 0x0

    return-object v0

    :cond_0
    move v0, v2

    .line 1838004
    goto :goto_0
.end method
