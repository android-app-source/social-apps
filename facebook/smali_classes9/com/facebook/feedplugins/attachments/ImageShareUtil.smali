.class public Lcom/facebook/feedplugins/attachments/ImageShareUtil;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1837306
    const-class v0, Lcom/facebook/feedplugins/attachments/ImageShareUtil;

    const-string v1, "newsfeed_image_share_view"

    const-string v2, "static_image"

    const-string v3, "native_newsfeed"

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/ImageShareUtil;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1837307
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLImage;)F
    .locals 4

    .prologue
    const/high16 v0, 0x40000000    # 2.0f

    const v1, 0x3f2aaaab

    .line 1837308
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v2

    if-nez v2, :cond_1

    .line 1837309
    const v0, 0x3faaaaab

    .line 1837310
    :cond_0
    :goto_0
    return v0

    .line 1837311
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 1837312
    cmpl-float v3, v2, v0

    if-gtz v3, :cond_0

    .line 1837313
    cmpg-float v0, v2, v1

    if-gez v0, :cond_2

    move v0, v1

    .line 1837314
    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public static a(LX/1Ad;Lcom/facebook/graphql/model/GraphQLImage;)LX/1aZ;
    .locals 2

    .prologue
    .line 1837315
    invoke-virtual {p0}, LX/1Ad;->o()LX/1Ad;

    move-result-object v0

    sget-object v1, Lcom/facebook/feedplugins/attachments/ImageShareUtil;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-static {p1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->p()LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;
    .locals 1

    .prologue
    .line 1837316
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 1837317
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;
    .locals 1

    .prologue
    .line 1837318
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    return-object v0
.end method
