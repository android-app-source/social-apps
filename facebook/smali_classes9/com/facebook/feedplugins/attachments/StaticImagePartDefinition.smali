.class public Lcom/facebook/feedplugins/attachments/StaticImagePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        "LX/Bym;",
        "LX/1PW;",
        "LX/ByP;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/1Ad;


# direct methods
.method public constructor <init>(LX/1Ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1837816
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1837817
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/StaticImagePartDefinition;->a:LX/1Ad;

    .line 1837818
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/StaticImagePartDefinition;
    .locals 4

    .prologue
    .line 1837805
    const-class v1, Lcom/facebook/feedplugins/attachments/StaticImagePartDefinition;

    monitor-enter v1

    .line 1837806
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/StaticImagePartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1837807
    sput-object v2, Lcom/facebook/feedplugins/attachments/StaticImagePartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1837808
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1837809
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1837810
    new-instance p0, Lcom/facebook/feedplugins/attachments/StaticImagePartDefinition;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v3

    check-cast v3, LX/1Ad;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/attachments/StaticImagePartDefinition;-><init>(LX/1Ad;)V

    .line 1837811
    move-object v0, p0

    .line 1837812
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1837813
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/StaticImagePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1837814
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1837815
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1837819
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1837820
    invoke-static {p2}, Lcom/facebook/feedplugins/attachments/ImageShareUtil;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 1837821
    new-instance v1, LX/Bym;

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/StaticImagePartDefinition;->a:LX/1Ad;

    invoke-static {v2, v0}, Lcom/facebook/feedplugins/attachments/ImageShareUtil;->a(LX/1Ad;Lcom/facebook/graphql/model/GraphQLImage;)LX/1aZ;

    move-result-object v2

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/ImageShareUtil;->a(Lcom/facebook/graphql/model/GraphQLImage;)F

    move-result v0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->aq()Z

    move-result v3

    invoke-direct {v1, v2, v0, v3}, LX/Bym;-><init>(LX/1aZ;FZ)V

    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x22ab1e12

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1837799
    check-cast p2, LX/Bym;

    check-cast p4, LX/ByP;

    .line 1837800
    iget v1, p2, LX/Bym;->b:F

    invoke-virtual {p4, v1}, LX/ByP;->setAspectRatio(F)V

    .line 1837801
    iget-object v1, p2, LX/Bym;->a:LX/1aZ;

    invoke-virtual {p4, v1}, LX/ByP;->setImageController(LX/1aZ;)V

    .line 1837802
    iget-boolean v1, p2, LX/Bym;->c:Z

    if-eqz v1, :cond_0

    sget-object v1, LX/6Wv;->READY_TO_PLAY:LX/6Wv;

    :goto_0
    invoke-virtual {p4, v1}, LX/ByP;->setPlayButtonState(LX/6Wv;)V

    .line 1837803
    const/16 v1, 0x1f

    const v2, 0x3baf059

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1837804
    :cond_0
    sget-object v1, LX/6Wv;->HIDDEN:LX/6Wv;

    goto :goto_0
.end method
