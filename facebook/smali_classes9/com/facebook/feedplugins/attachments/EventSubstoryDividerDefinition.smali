.class public Lcom/facebook/feedplugins/attachments/EventSubstoryDividerDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<TP;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1837052
    new-instance v0, LX/ByJ;

    invoke-direct {v0}, LX/ByJ;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/attachments/EventSubstoryDividerDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1837055
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1837056
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/EventSubstoryDividerDefinition;
    .locals 3

    .prologue
    .line 1837057
    const-class v1, Lcom/facebook/feedplugins/attachments/EventSubstoryDividerDefinition;

    monitor-enter v1

    .line 1837058
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/EventSubstoryDividerDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1837059
    sput-object v2, Lcom/facebook/feedplugins/attachments/EventSubstoryDividerDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1837060
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1837061
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1837062
    new-instance v0, Lcom/facebook/feedplugins/attachments/EventSubstoryDividerDefinition;

    invoke-direct {v0}, Lcom/facebook/feedplugins/attachments/EventSubstoryDividerDefinition;-><init>()V

    .line 1837063
    move-object v0, v0

    .line 1837064
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1837065
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/EventSubstoryDividerDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1837066
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1837067
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 1837054
    sget-object v0, Lcom/facebook/feedplugins/attachments/EventSubstoryDividerDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;)Z"
        }
    .end annotation

    .prologue
    .line 1837053
    const/4 v0, 0x1

    return v0
.end method
