.class public Lcom/facebook/feedplugins/attachments/list/ListAttachmentItemPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private a:LX/1WL;


# direct methods
.method public constructor <init>(LX/1WL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1841213
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1841214
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/list/ListAttachmentItemPartDefinition;->a:LX/1WL;

    .line 1841215
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/list/ListAttachmentItemPartDefinition;
    .locals 4

    .prologue
    .line 1841216
    const-class v1, Lcom/facebook/feedplugins/attachments/list/ListAttachmentItemPartDefinition;

    monitor-enter v1

    .line 1841217
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/list/ListAttachmentItemPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1841218
    sput-object v2, Lcom/facebook/feedplugins/attachments/list/ListAttachmentItemPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1841219
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1841220
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1841221
    new-instance p0, Lcom/facebook/feedplugins/attachments/list/ListAttachmentItemPartDefinition;

    invoke-static {v0}, LX/1WG;->a(LX/0QB;)LX/1WL;

    move-result-object v3

    check-cast v3, LX/1WL;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/attachments/list/ListAttachmentItemPartDefinition;-><init>(LX/1WL;)V

    .line 1841222
    move-object v0, p0

    .line 1841223
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1841224
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/list/ListAttachmentItemPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1841225
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1841226
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1841227
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v5, 0x0

    .line 1841228
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1841229
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1841230
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v2

    .line 1841231
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 1841232
    iget-object v4, p0, Lcom/facebook/feedplugins/attachments/list/ListAttachmentItemPartDefinition;->a:LX/1WL;

    invoke-interface {v4, v0}, LX/1WL;->a(Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)LX/1RB;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    .line 1841233
    if-eqz v0, :cond_1

    .line 1841234
    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1841235
    :cond_0
    return-object v5

    .line 1841236
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1841237
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1841238
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1841239
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1841240
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
