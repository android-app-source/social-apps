.class public Lcom/facebook/feedplugins/attachments/PhotoPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/Bye;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/drawee/view/GenericDraweeView;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1837600
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1837601
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/PhotoPartDefinition;
    .locals 3

    .prologue
    .line 1837602
    const-class v1, Lcom/facebook/feedplugins/attachments/PhotoPartDefinition;

    monitor-enter v1

    .line 1837603
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/PhotoPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1837604
    sput-object v2, Lcom/facebook/feedplugins/attachments/PhotoPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1837605
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1837606
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1837607
    new-instance v0, Lcom/facebook/feedplugins/attachments/PhotoPartDefinition;

    invoke-direct {v0}, Lcom/facebook/feedplugins/attachments/PhotoPartDefinition;-><init>()V

    .line 1837608
    move-object v0, v0

    .line 1837609
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1837610
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/PhotoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1837611
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1837612
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x11f08935    # -1.1099992E28f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1837613
    check-cast p1, LX/Bye;

    check-cast p4, Lcom/facebook/drawee/view/GenericDraweeView;

    .line 1837614
    iget v1, p1, LX/Bye;->b:F

    invoke-virtual {p4, v1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 1837615
    iget-object v1, p1, LX/Bye;->a:LX/1aZ;

    invoke-virtual {p4, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1837616
    const/16 v1, 0x1f

    const v2, 0x467f55

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1837617
    check-cast p4, Lcom/facebook/drawee/view/GenericDraweeView;

    .line 1837618
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1837619
    return-void
.end method
