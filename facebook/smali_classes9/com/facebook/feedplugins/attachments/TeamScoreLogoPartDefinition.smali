.class public Lcom/facebook/feedplugins/attachments/TeamScoreLogoPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/Byl;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "LX/ByU;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1837870
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/TeamScoreLogoPartDefinition;
    .locals 3

    .prologue
    .line 1837871
    const-class v1, Lcom/facebook/feedplugins/attachments/TeamScoreLogoPartDefinition;

    monitor-enter v1

    .line 1837872
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/TeamScoreLogoPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1837873
    sput-object v2, Lcom/facebook/feedplugins/attachments/TeamScoreLogoPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1837874
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1837875
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1837876
    new-instance v0, Lcom/facebook/feedplugins/attachments/TeamScoreLogoPartDefinition;

    invoke-direct {v0}, Lcom/facebook/feedplugins/attachments/TeamScoreLogoPartDefinition;-><init>()V

    .line 1837877
    move-object v0, v0

    .line 1837878
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1837879
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/TeamScoreLogoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1837880
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1837881
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x3b58f75e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1837882
    check-cast p1, LX/Byl;

    check-cast p4, LX/ByU;

    .line 1837883
    iget v1, p1, LX/Byl;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 1837884
    iget-object v2, p4, LX/ByU;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1837885
    iget-object v1, p1, LX/Byl;->b:Ljava/lang/String;

    iget-object v2, p1, LX/Byl;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 1837886
    iget-object p0, p4, LX/ByU;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {p0, p1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1837887
    const/16 v1, 0x1f

    const v2, 0x745d49c4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
