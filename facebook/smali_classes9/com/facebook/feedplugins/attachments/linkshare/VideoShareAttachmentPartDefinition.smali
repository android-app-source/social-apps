.class public Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/35o;",
        ":",
        "LX/35r;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition",
            "<TE;TV;>;"
        }
    .end annotation
.end field

.field private final b:LX/2sO;

.field private final c:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition",
            "<TE;TV;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoPartDefinition",
            "<TE;TV;>;"
        }
    .end annotation
.end field

.field private final e:I


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;LX/2sO;Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1840799
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1840800
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentPartDefinition;->a:Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    .line 1840801
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentPartDefinition;->b:LX/2sO;

    .line 1840802
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    .line 1840803
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoPartDefinition;

    .line 1840804
    const v0, 0x7f0b00c4

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentPartDefinition;->e:I

    .line 1840805
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentPartDefinition;
    .locals 9

    .prologue
    .line 1840772
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentPartDefinition;

    monitor-enter v1

    .line 1840773
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1840774
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1840775
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1840776
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1840777
    new-instance v3, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    invoke-static {v0}, LX/2sO;->a(LX/0QB;)LX/2sO;

    move-result-object v7

    check-cast v7, LX/2sO;

    invoke-static {v0}, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentPartDefinition;-><init>(Landroid/content/res/Resources;Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;LX/2sO;Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;)V

    .line 1840778
    move-object v0, v3

    .line 1840779
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1840780
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1840781
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1840782
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 1840798
    sget-object v0, Lcom/facebook/attachments/angora/AngoraAttachmentView;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1840790
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1840791
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentPartDefinition;->a:Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1840792
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentPartDefinition;->b:LX/2sO;

    invoke-virtual {v0, p2}, LX/2sO;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1840793
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1840794
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoPartDefinition;

    new-instance v2, LX/C0G;

    .line 1840795
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1840796
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget v3, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentPartDefinition;->e:I

    invoke-direct {v2, v0, v3}, LX/C0G;-><init>(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1840797
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x185c8de6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1840784
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1840785
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1840786
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    .line 1840787
    :goto_0
    check-cast p4, LX/35r;

    invoke-interface {p4, v1}, LX/35r;->setSidePhotoPlayIconVisibility(I)V

    .line 1840788
    const/16 v1, 0x1f

    const v2, -0x4224e31c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1840789
    :cond_0
    const/16 v1, 0x8

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1840783
    const/4 v0, 0x1

    return v0
.end method
