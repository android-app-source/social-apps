.class public Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareQuoteOnlyGroupDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareWithVerticalBarPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareWithVerticalBarPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1840081
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1840082
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareQuoteOnlyGroupDefinition;->a:Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareWithVerticalBarPartDefinition;

    .line 1840083
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareQuoteOnlyGroupDefinition;
    .locals 4

    .prologue
    .line 1840084
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareQuoteOnlyGroupDefinition;

    monitor-enter v1

    .line 1840085
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareQuoteOnlyGroupDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1840086
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareQuoteOnlyGroupDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1840087
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1840088
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1840089
    new-instance p0, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareQuoteOnlyGroupDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareWithVerticalBarPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareWithVerticalBarPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareWithVerticalBarPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareQuoteOnlyGroupDefinition;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareWithVerticalBarPartDefinition;)V

    .line 1840090
    move-object v0, p0

    .line 1840091
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1840092
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareQuoteOnlyGroupDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1840093
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1840094
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1840095
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1840096
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareQuoteOnlyGroupDefinition;->a:Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareWithVerticalBarPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1840097
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1840098
    const/4 v0, 0x1

    return v0
.end method
