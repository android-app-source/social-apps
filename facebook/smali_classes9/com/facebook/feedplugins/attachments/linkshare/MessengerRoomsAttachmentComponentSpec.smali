.class public Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pp;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field public final b:LX/1qb;

.field public final c:LX/2sO;

.field public final d:LX/1nu;

.field public final e:LX/36X;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1839573
    const-class v0, Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1qb;LX/2sO;LX/1nu;LX/36X;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1839574
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1839575
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentSpec;->b:LX/1qb;

    .line 1839576
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentSpec;->c:LX/2sO;

    .line 1839577
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentSpec;->d:LX/1nu;

    .line 1839578
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentSpec;->e:LX/36X;

    .line 1839579
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentSpec;
    .locals 7

    .prologue
    .line 1839580
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentSpec;

    monitor-enter v1

    .line 1839581
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentSpec;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1839582
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentSpec;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1839583
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1839584
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1839585
    new-instance p0, Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentSpec;

    invoke-static {v0}, LX/1qb;->a(LX/0QB;)LX/1qb;

    move-result-object v3

    check-cast v3, LX/1qb;

    invoke-static {v0}, LX/2sO;->a(LX/0QB;)LX/2sO;

    move-result-object v4

    check-cast v4, LX/2sO;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v5

    check-cast v5, LX/1nu;

    invoke-static {v0}, LX/36X;->a(LX/0QB;)LX/36X;

    move-result-object v6

    check-cast v6, LX/36X;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentSpec;-><init>(LX/1qb;LX/2sO;LX/1nu;LX/36X;)V

    .line 1839586
    move-object v0, p0

    .line 1839587
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1839588
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1839589
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1839590
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
