.class public Lcom/facebook/feedplugins/attachments/linkshare/FollowShareAttachmentSelector;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowPageShareAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowPageShareAttachmentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowUserShareAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowUserShareAttachmentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowUserShareAttachmentPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowPageShareAttachmentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1838925
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1838926
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/FollowShareAttachmentSelector;->b:Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowUserShareAttachmentPartDefinition;

    .line 1838927
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/FollowShareAttachmentSelector;->a:Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowPageShareAttachmentPartDefinition;

    .line 1838928
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/FollowShareAttachmentSelector;
    .locals 5

    .prologue
    .line 1838914
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/FollowShareAttachmentSelector;

    monitor-enter v1

    .line 1838915
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/FollowShareAttachmentSelector;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1838916
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/FollowShareAttachmentSelector;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1838917
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1838918
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1838919
    new-instance p0, Lcom/facebook/feedplugins/attachments/linkshare/FollowShareAttachmentSelector;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowUserShareAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowUserShareAttachmentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowUserShareAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowPageShareAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowPageShareAttachmentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowPageShareAttachmentPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/attachments/linkshare/FollowShareAttachmentSelector;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowUserShareAttachmentPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowPageShareAttachmentPartDefinition;)V

    .line 1838920
    move-object v0, p0

    .line 1838921
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1838922
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/FollowShareAttachmentSelector;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1838923
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1838924
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1838932
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1838933
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/FollowShareAttachmentSelector;->a:Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowPageShareAttachmentPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/FollowShareAttachmentSelector;->b:Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowUserShareAttachmentPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 1838934
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1838929
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1838930
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1838931
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/2yg;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    return v0
.end method
