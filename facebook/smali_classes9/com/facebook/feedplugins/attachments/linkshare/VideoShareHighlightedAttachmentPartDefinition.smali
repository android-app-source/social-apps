.class public Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pr;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/35o;",
        ":",
        "LX/35q;",
        ":",
        "LX/35r;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition",
            "<TE;TV;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition",
            "<TE;TV;>;"
        }
    .end annotation
.end field

.field private final c:LX/2y3;

.field private final d:Lcom/facebook/feedplugins/attachments/linkshare/PlayIconPartDefinition;

.field private final e:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition",
            "<TE;TV;>;"
        }
    .end annotation
.end field

.field private final f:LX/2sO;

.field private final g:LX/0tK;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;LX/2y3;Lcom/facebook/feedplugins/attachments/linkshare/PlayIconPartDefinition;Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;LX/2sO;LX/0tK;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1840806
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1840807
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentPartDefinition;->a:Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    .line 1840808
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;

    .line 1840809
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentPartDefinition;->c:LX/2y3;

    .line 1840810
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/attachments/linkshare/PlayIconPartDefinition;

    .line 1840811
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentPartDefinition;->e:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    .line 1840812
    iput-object p6, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentPartDefinition;->f:LX/2sO;

    .line 1840813
    iput-object p7, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentPartDefinition;->g:LX/0tK;

    .line 1840814
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentPartDefinition;
    .locals 11

    .prologue
    .line 1840815
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentPartDefinition;

    monitor-enter v1

    .line 1840816
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1840817
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1840818
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1840819
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1840820
    new-instance v3, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;

    invoke-static {v0}, LX/2y3;->a(LX/0QB;)LX/2y3;

    move-result-object v6

    check-cast v6, LX/2y3;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/PlayIconPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/PlayIconPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/attachments/linkshare/PlayIconPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    invoke-static {v0}, LX/2sO;->a(LX/0QB;)LX/2sO;

    move-result-object v9

    check-cast v9, LX/2sO;

    invoke-static {v0}, LX/0tK;->a(LX/0QB;)LX/0tK;

    move-result-object v10

    check-cast v10, LX/0tK;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentPartDefinition;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;LX/2y3;Lcom/facebook/feedplugins/attachments/linkshare/PlayIconPartDefinition;Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;LX/2sO;LX/0tK;)V

    .line 1840821
    move-object v0, v3

    .line 1840822
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1840823
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1840824
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1840825
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 1840826
    sget-object v0, Lcom/facebook/attachments/angora/AngoraAttachmentView;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1840827
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1840828
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1840829
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1840830
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentPartDefinition;->a:Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    invoke-interface {p1, v1, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1840831
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;

    invoke-interface {p1, v1, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1840832
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/attachments/linkshare/PlayIconPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1840833
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentPartDefinition;->f:LX/2sO;

    invoke-virtual {v0, p2}, LX/2sO;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1840834
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentPartDefinition;->e:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1840835
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1840836
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v0, 0x0

    .line 1840837
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentPartDefinition;->g:LX/0tK;

    invoke-virtual {v1, v0}, LX/0tK;->b(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1840838
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentPartDefinition;->c:LX/2y3;

    .line 1840839
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1840840
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/2y3;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v0

    goto :goto_0
.end method
