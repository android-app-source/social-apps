.class public Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowPageShareAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pf;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/2yV;",
        "TE;",
        "LX/3WL;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;

.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final c:LX/20i;

.field public final d:LX/1Ck;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/25G;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/20i;LX/1Ck;LX/0Ot;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            "Lcom/facebook/graphql/model/GraphQLActorCache;",
            "LX/1Ck;",
            "LX/0Ot",
            "<",
            "LX/25G;",
            ">;",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1841098
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1841099
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowPageShareAttachmentPartDefinition;->a:Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;

    .line 1841100
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowPageShareAttachmentPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 1841101
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowPageShareAttachmentPartDefinition;->c:LX/20i;

    .line 1841102
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowPageShareAttachmentPartDefinition;->d:LX/1Ck;

    .line 1841103
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowPageShareAttachmentPartDefinition;->e:LX/0Ot;

    .line 1841104
    iput-object p6, p0, Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowPageShareAttachmentPartDefinition;->f:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 1841105
    return-void
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2yV;LX/1Pf;)Landroid/view/View$OnClickListener;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/2yV;",
            "TE;)",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    .line 1841106
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v0

    .line 1841107
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1841108
    new-instance v0, LX/C0d;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LX/C0d;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowPageShareAttachmentPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/2yV;LX/1Pf;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowPageShareAttachmentPartDefinition;
    .locals 10

    .prologue
    .line 1841109
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowPageShareAttachmentPartDefinition;

    monitor-enter v1

    .line 1841110
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowPageShareAttachmentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1841111
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowPageShareAttachmentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1841112
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1841113
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1841114
    new-instance v3, Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowPageShareAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/20i;->a(LX/0QB;)LX/20i;

    move-result-object v6

    check-cast v6, LX/20i;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v7

    check-cast v7, LX/1Ck;

    const/16 v8, 0x476

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowPageShareAttachmentPartDefinition;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/20i;LX/1Ck;LX/0Ot;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 1841115
    move-object v0, v3

    .line 1841116
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1841117
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowPageShareAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1841118
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1841119
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 1841120
    sget-object v0, LX/3WL;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1841121
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    .line 1841122
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1841123
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1841124
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowPageShareAttachmentPartDefinition;->f:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    sget-object v4, LX/1Ua;->m:LX/1Ua;

    const v5, 0x7f020a41

    const/4 v6, -0x1

    invoke-direct {v2, v3, v4, v5, v6}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1841125
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowPageShareAttachmentPartDefinition;->a:Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1841126
    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 1841127
    new-instance v0, LX/2yU;

    invoke-direct {v0, p2}, LX/2yU;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-static {p2}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-interface {p3, v0, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2yV;

    .line 1841128
    iget-object v2, v0, LX/2yV;->a:LX/C0a;

    move-object v2, v2

    .line 1841129
    if-nez v2, :cond_0

    .line 1841130
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->az()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2yV;->a(Lcom/facebook/graphql/model/GraphQLProfile;)V

    .line 1841131
    :cond_0
    const v1, 0x7f0d11df

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowPageShareAttachmentPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-direct {p0, p2, v0, p3}, Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowPageShareAttachmentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2yV;LX/1Pf;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1841132
    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x10c9af4c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1841133
    check-cast p2, LX/2yV;

    check-cast p4, LX/3WL;

    .line 1841134
    iget-object v1, p2, LX/2yV;->a:LX/C0a;

    move-object v1, v1

    .line 1841135
    invoke-virtual {p4, v1}, LX/3WL;->setButtonState(LX/C0a;)V

    .line 1841136
    const/16 v1, 0x1f

    const v2, 0x39fac63

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1841137
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1841138
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1841139
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1841140
    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->az()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x25d6af

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
