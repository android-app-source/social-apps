.class public Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowUserShareAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pf;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/2yV;",
        "TE;",
        "LX/3WL;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;

.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1841157
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1841158
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowUserShareAttachmentPartDefinition;->a:Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;

    .line 1841159
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowUserShareAttachmentPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 1841160
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowUserShareAttachmentPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 1841161
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowUserShareAttachmentPartDefinition;
    .locals 6

    .prologue
    .line 1841171
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowUserShareAttachmentPartDefinition;

    monitor-enter v1

    .line 1841172
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowUserShareAttachmentPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1841173
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowUserShareAttachmentPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1841174
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1841175
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1841176
    new-instance p0, Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowUserShareAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowUserShareAttachmentPartDefinition;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 1841177
    move-object v0, p0

    .line 1841178
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1841179
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowUserShareAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1841180
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1841181
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 1841170
    sget-object v0, LX/3WL;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1841182
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    .line 1841183
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1841184
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1841185
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowUserShareAttachmentPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    sget-object v4, LX/1Ua;->m:LX/1Ua;

    const v5, 0x7f020a41

    const/4 v6, -0x1

    invoke-direct {v2, v3, v4, v5, v6}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1841186
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowUserShareAttachmentPartDefinition;->a:Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1841187
    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 1841188
    new-instance v0, LX/2yU;

    invoke-direct {v0, p2}, LX/2yU;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-static {p2}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-interface {p3, v0, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2yV;

    .line 1841189
    iget-object v2, v0, LX/2yV;->a:LX/C0a;

    move-object v2, v2

    .line 1841190
    if-nez v2, :cond_0

    .line 1841191
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->az()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2yV;->a(Lcom/facebook/graphql/model/GraphQLProfile;)V

    .line 1841192
    :cond_0
    const v1, 0x7f0d11df

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowUserShareAttachmentPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 1841193
    new-instance v3, LX/C0e;

    invoke-direct {v3, p0, p2, v0, p3}, LX/C0e;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowUserShareAttachmentPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/2yV;LX/1Pf;)V

    move-object v3, v3

    .line 1841194
    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1841195
    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x12b62546

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1841166
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/2yV;

    check-cast p4, LX/3WL;

    .line 1841167
    iget-object v1, p2, LX/2yV;->a:LX/C0a;

    move-object v1, v1

    .line 1841168
    invoke-virtual {p4, v1}, LX/3WL;->setButtonState(LX/C0a;)V

    .line 1841169
    const/16 v1, 0x1f

    const v2, 0x2ca6689c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1841162
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1841163
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1841164
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1841165
    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->az()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x285feb

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
