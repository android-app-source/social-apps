.class public Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pf;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/3WL;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static h:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/FbDraweePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/multirow/parts/FbDraweePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final e:Landroid/content/Context;

.field public final f:LX/1nG;

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1841033
    const-class v0, Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/1nG;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/multirow/parts/TextPartDefinition;",
            "Lcom/facebook/multirow/parts/FbDraweePartDefinition;",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            "LX/1nG;",
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1841034
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1841035
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;->e:Landroid/content/Context;

    .line 1841036
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 1841037
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;->c:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 1841038
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 1841039
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;->f:LX/1nG;

    .line 1841040
    iput-object p6, p0, Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;->g:LX/0Or;

    .line 1841041
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1841042
    new-instance v0, LX/C0Z;

    invoke-direct {v0, p0, p1}, LX/C0Z;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;
    .locals 10

    .prologue
    .line 1841043
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;

    monitor-enter v1

    .line 1841044
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1841045
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1841046
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1841047
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1841048
    new-instance v3, Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/1nG;->a(LX/0QB;)LX/1nG;

    move-result-object v8

    check-cast v8, LX/1nG;

    const/16 v9, 0xbc6

    invoke-static {v0, v9}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;-><init>(Landroid/content/Context;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/1nG;LX/0Or;)V

    .line 1841049
    move-object v0, v3

    .line 1841050
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1841051
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1841052
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1841053
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1841054
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1841055
    invoke-static {p2}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 1841056
    new-instance v2, Landroid/text/style/StyleSpan;

    const/4 v0, 0x1

    invoke-direct {v2, v0}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 1841057
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->an()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1841058
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->an()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1841059
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->an()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v0

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->c()I

    move-result v0

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    const/16 v5, 0x21

    invoke-virtual {v3, v2, v0, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1841060
    :cond_0
    const v0, 0x7f0d11de

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v0, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1841061
    const v0, 0x7f0d11dd

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;->c:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a()LX/2f8;

    move-result-object v3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->az()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v1

    sget-object v3, Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1841062
    iput-object v3, v1, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 1841063
    move-object v1, v1

    .line 1841064
    invoke-virtual {v1}, LX/2f8;->a()LX/2f9;

    move-result-object v1

    invoke-interface {p1, v0, v2, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1841065
    const v0, 0x7f0d11de

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-direct {p0, p2}, Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1841066
    const v0, 0x7f0d11dd

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-direct {p0, p2}, Lcom/facebook/feedplugins/attachments/linkshare/follow/BaseFollowShareAttachmentPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1841067
    const/4 v0, 0x0

    return-object v0
.end method
