.class public Lcom/facebook/feedplugins/attachments/linkshare/InstagramAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/35o;",
        ":",
        "LX/35q;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;TV;>;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static e:LX/0Xm;


# instance fields
.field public final b:LX/1qb;

.field private final c:Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition",
            "<TE;",
            "Lcom/facebook/attachments/angora/AngoraAttachmentView;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition",
            "<TE;",
            "Lcom/facebook/attachments/angora/AngoraAttachmentView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1838944
    const-class v0, Lcom/facebook/feedplugins/attachments/linkshare/InstagramAttachmentPartDefinition;

    const-string v1, "newsfeed_angora_attachment_view"

    const-string v2, "large_square_photo"

    const-string v3, "native_newsfeed"

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/linkshare/InstagramAttachmentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1qb;Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;LX/2yH;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1838945
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1838946
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/InstagramAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    .line 1838947
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/InstagramAttachmentPartDefinition;->b:LX/1qb;

    .line 1838948
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/InstagramAttachmentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    const/high16 v1, 0x3f800000    # 1.0f

    .line 1838949
    new-instance v2, LX/Bza;

    invoke-direct {v2, p0}, LX/Bza;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/InstagramAttachmentPartDefinition;)V

    move-object v2, v2

    .line 1838950
    invoke-virtual {p3, v0, v1, v2}, LX/2yH;->a(Lcom/facebook/common/callercontext/CallerContext;FLX/2yA;)Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/InstagramAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;

    .line 1838951
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/InstagramAttachmentPartDefinition;
    .locals 6

    .prologue
    .line 1838952
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/InstagramAttachmentPartDefinition;

    monitor-enter v1

    .line 1838953
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/InstagramAttachmentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1838954
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/InstagramAttachmentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1838955
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1838956
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1838957
    new-instance p0, Lcom/facebook/feedplugins/attachments/linkshare/InstagramAttachmentPartDefinition;

    invoke-static {v0}, LX/1qb;->a(LX/0QB;)LX/1qb;

    move-result-object v3

    check-cast v3, LX/1qb;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    const-class v5, LX/2yH;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/2yH;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/attachments/linkshare/InstagramAttachmentPartDefinition;-><init>(LX/1qb;Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;LX/2yH;)V

    .line 1838958
    move-object v0, p0

    .line 1838959
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1838960
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/InstagramAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1838961
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1838962
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 1838963
    sget-object v0, Lcom/facebook/attachments/angora/AngoraAttachmentView;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1838964
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1838965
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/InstagramAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1838966
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/InstagramAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1838967
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1838968
    const/4 v0, 0x1

    return v0
.end method
