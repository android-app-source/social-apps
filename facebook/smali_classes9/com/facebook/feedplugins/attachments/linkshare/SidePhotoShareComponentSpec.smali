.class public Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:I
    .annotation build Landroid/support/annotation/DimenRes;
    .end annotation
.end field

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static g:LX/0Xm;


# instance fields
.field public final c:LX/1nu;

.field private final d:LX/1qa;

.field public final e:Landroid/content/res/Resources;

.field public final f:LX/1WM;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1840288
    const v0, 0x7f0b00c5

    sput v0, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareComponentSpec;->a:I

    .line 1840289
    const-class v0, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareComponentSpec;

    const-string v1, "newsfeed_angora_attachment_view"

    const-string v2, "small_photo"

    const-string v3, "native_newsfeed"

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareComponentSpec;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;Landroid/content/res/Resources;LX/1WM;LX/1qa;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1840290
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1840291
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareComponentSpec;->c:LX/1nu;

    .line 1840292
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareComponentSpec;->d:LX/1qa;

    .line 1840293
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareComponentSpec;->e:Landroid/content/res/Resources;

    .line 1840294
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareComponentSpec;->f:LX/1WM;

    .line 1840295
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareComponentSpec;
    .locals 7

    .prologue
    .line 1840296
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareComponentSpec;

    monitor-enter v1

    .line 1840297
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareComponentSpec;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1840298
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareComponentSpec;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1840299
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1840300
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1840301
    new-instance p0, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v3

    check-cast v3, LX/1nu;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/1WM;->a(LX/0QB;)LX/1WM;

    move-result-object v5

    check-cast v5, LX/1WM;

    invoke-static {v0}, LX/1qa;->a(LX/0QB;)LX/1qa;

    move-result-object v6

    check-cast v6, LX/1qa;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareComponentSpec;-><init>(LX/1nu;Landroid/content/res/Resources;LX/1WM;LX/1qa;)V

    .line 1840302
    move-object v0, p0

    .line 1840303
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1840304
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1840305
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1840306
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
