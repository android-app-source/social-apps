.class public Lcom/facebook/feedplugins/attachments/linkshare/LinkCoverPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/Bzo;",
        "LX/Bzp;",
        "LX/1PW;",
        "LX/Cko;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/03V;

.field private final b:LX/Ckj;

.field public final c:LX/0ad;


# direct methods
.method public constructor <init>(LX/03V;LX/Ckj;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1839258
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1839259
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/LinkCoverPartDefinition;->a:LX/03V;

    .line 1839260
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/LinkCoverPartDefinition;->b:LX/Ckj;

    .line 1839261
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/linkshare/LinkCoverPartDefinition;->c:LX/0ad;

    .line 1839262
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/LinkCoverPartDefinition;
    .locals 6

    .prologue
    .line 1839385
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/LinkCoverPartDefinition;

    monitor-enter v1

    .line 1839386
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/LinkCoverPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1839387
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/LinkCoverPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1839388
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1839389
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1839390
    new-instance p0, Lcom/facebook/feedplugins/attachments/linkshare/LinkCoverPartDefinition;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v0}, LX/Ckj;->a(LX/0QB;)LX/Ckj;

    move-result-object v4

    check-cast v4, LX/Ckj;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/attachments/linkshare/LinkCoverPartDefinition;-><init>(LX/03V;LX/Ckj;LX/0ad;)V

    .line 1839391
    move-object v0, p0

    .line 1839392
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1839393
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/LinkCoverPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1839394
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1839395
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/Bzo;LX/Bzp;LX/Cko;)V
    .locals 11

    .prologue
    .line 1839304
    iget-object v0, p2, LX/Bzp;->a:LX/CkL;

    if-eqz v0, :cond_1

    if-eqz p3, :cond_1

    invoke-virtual {p3}, LX/Cko;->getMeasuredWidth()I

    move-result v0

    if-lez v0, :cond_1

    invoke-static {p1}, Lcom/facebook/feedplugins/attachments/linkshare/LinkCoverPartDefinition;->b(LX/Bzo;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p2, LX/Bzp;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-static {p1}, Lcom/facebook/feedplugins/attachments/linkshare/LinkCoverPartDefinition;->b(LX/Bzo;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p2, LX/Bzp;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1839305
    iget-object v0, p2, LX/Bzp;->a:LX/CkL;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1839306
    if-eqz p3, :cond_0

    iget-object v0, p2, LX/Bzp;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/facebook/feedplugins/attachments/linkshare/LinkCoverPartDefinition;->b(LX/Bzo;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p2, LX/Bzp;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1839307
    :cond_0
    :goto_0
    return-void

    .line 1839308
    :cond_1
    if-nez p3, :cond_c

    .line 1839309
    :cond_2
    :goto_1
    goto :goto_0

    .line 1839310
    :cond_3
    invoke-virtual {p3}, LX/Cko;->getMeasuredWidth()I

    move-result v0

    .line 1839311
    iget-object v1, p2, LX/Bzp;->a:LX/CkL;

    iget-object v2, p2, LX/Bzp;->b:Ljava/lang/String;

    .line 1839312
    iget p0, v1, LX/CkL;->t:I

    if-ne v0, p0, :cond_8

    if-eqz v2, :cond_8

    iget-object p0, v1, LX/CkL;->i:Ljava/lang/String;

    if-eqz p0, :cond_8

    iget-object p0, v1, LX/CkL;->i:Ljava/lang/String;

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    const/4 p0, 0x1

    :goto_2
    move v1, p0

    .line 1839313
    if-nez v1, :cond_4

    .line 1839314
    iget-object v1, p2, LX/Bzp;->a:LX/CkL;

    iget-object v2, p2, LX/Bzp;->b:Ljava/lang/String;

    const/high16 p0, 0x40000000    # 2.0f

    .line 1839315
    iput v0, v1, LX/CkL;->t:I

    .line 1839316
    iput-object v2, v1, LX/CkL;->i:Ljava/lang/String;

    .line 1839317
    int-to-float v3, v0

    .line 1839318
    sget v4, LX/CkK;->a:F

    div-float v4, v3, v4

    .line 1839319
    sget v5, LX/CkK;->b:F

    div-float v5, v4, v5

    .line 1839320
    sget v6, LX/CkK;->c:F

    div-float v6, v3, v6

    .line 1839321
    sget v7, LX/CkK;->f:F

    mul-float/2addr v7, v5

    sub-float v7, v4, v7

    .line 1839322
    sget v8, LX/CkK;->d:F

    mul-float/2addr v8, v6

    sub-float v8, v3, v8

    div-float/2addr v8, p0

    .line 1839323
    sget v9, LX/CkK;->e:F

    mul-float/2addr v9, v5

    mul-float v10, v7, p0

    add-float/2addr v9, v10

    .line 1839324
    iput v3, v1, LX/CkL;->w:F

    .line 1839325
    iput v9, v1, LX/CkL;->x:F

    .line 1839326
    sget-object v3, LX/Ckb;->DESCRIPTION:LX/Ckb;

    sget v9, LX/CkK;->d:F

    mul-float/2addr v9, v6

    mul-float v10, v5, p0

    invoke-static {v1, v3, v9, v10}, LX/CkL;->a(LX/CkL;LX/Ckb;FF)V

    .line 1839327
    sget-object v3, LX/Ckb;->BYLINE:LX/Ckb;

    sget v9, LX/CkK;->d:F

    mul-float/2addr v9, v6

    invoke-static {v1, v3, v9, v5}, LX/CkL;->a(LX/CkL;LX/Ckb;FF)V

    .line 1839328
    sget-object v3, LX/Ckb;->BYLINE_AREA:LX/Ckb;

    sget v9, LX/CkK;->d:F

    mul-float/2addr v9, v6

    invoke-static {v1, v3, v9, v5}, LX/CkL;->a(LX/CkL;LX/Ckb;FF)V

    .line 1839329
    sget-object v3, LX/Ckb;->HEADLINE:LX/Ckb;

    sget v9, LX/CkK;->d:F

    mul-float/2addr v9, v6

    mul-float v10, p0, v5

    invoke-static {v1, v3, v9, v10}, LX/CkL;->a(LX/CkL;LX/Ckb;FF)V

    .line 1839330
    sget-object v3, LX/Ckb;->OVERLAY:LX/Ckb;

    sget v9, LX/CkK;->d:F

    mul-float/2addr v9, v6

    invoke-static {v1, v3, v9, v5}, LX/CkL;->a(LX/CkL;LX/Ckb;FF)V

    .line 1839331
    sget-object v3, LX/Ckb;->SOURCE_IMAGE:LX/Ckb;

    invoke-static {v1, v3, v6, v5}, LX/CkL;->a(LX/CkL;LX/Ckb;FF)V

    .line 1839332
    sget-object v3, LX/Ckb;->COVER_IMAGE:LX/Ckb;

    sget v9, LX/CkK;->d:F

    mul-float/2addr v9, v6

    invoke-static {v1, v3, v9, v4}, LX/CkL;->a(LX/CkL;LX/Ckb;FF)V

    .line 1839333
    sget-object v3, LX/Ckb;->COVER_VIDEO:LX/Ckb;

    sget v9, LX/CkK;->d:F

    mul-float/2addr v9, v6

    invoke-static {v1, v3, v9, v4}, LX/CkL;->a(LX/CkL;LX/Ckb;FF)V

    .line 1839334
    iget-object v3, v1, LX/CkL;->h:LX/CkX;

    .line 1839335
    iput v6, v3, LX/CkX;->g:F

    .line 1839336
    iget-object v3, v1, LX/CkL;->h:LX/CkX;

    .line 1839337
    iput v5, v3, LX/CkX;->h:F

    .line 1839338
    iget-object v3, v1, LX/CkL;->h:LX/CkX;

    .line 1839339
    iput v7, v3, LX/CkX;->a:F

    .line 1839340
    iget-object v3, v1, LX/CkL;->h:LX/CkX;

    .line 1839341
    iput v8, v3, LX/CkX;->b:F

    .line 1839342
    :cond_4
    iget-boolean v0, p3, LX/Cko;->c:Z

    move v0, v0

    .line 1839343
    if-eqz v0, :cond_5

    .line 1839344
    invoke-virtual {p3}, LX/Cko;->removeAllViews()V

    .line 1839345
    const v0, 0x7f0309f3

    invoke-virtual {p3, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1839346
    const v0, 0x7f0d1932

    invoke-virtual {p3, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;

    iput-object v0, p3, LX/Cko;->b:Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;

    .line 1839347
    const/4 v0, 0x0

    iput-object v0, p3, LX/Cko;->a:Lcom/facebook/richdocument/linkcovers/view/LinkCoverBasicView;

    .line 1839348
    const/4 v0, 0x0

    iput-boolean v0, p3, LX/Cko;->c:Z

    .line 1839349
    :cond_5
    iget-object v0, p3, LX/Cko;->b:Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;

    move-object v1, v0

    .line 1839350
    iget-object v0, p2, LX/Bzp;->a:LX/CkL;

    invoke-virtual {v1, v0}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->a(LX/CkL;)V

    .line 1839351
    invoke-static {p1}, Lcom/facebook/feedplugins/attachments/linkshare/LinkCoverPartDefinition;->c(LX/Bzo;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->setImageParams(Landroid/net/Uri;)V

    .line 1839352
    iget-object v0, p1, LX/Bzo;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->setTitleText(Ljava/lang/String;)V

    .line 1839353
    iget-object v0, p1, LX/Bzo;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-object v0, p1, LX/Bzo;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {v1, v0}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->setDescriptionText(Ljava/lang/String;)V

    .line 1839354
    iget-object v0, v1, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->d:Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;

    move-object v0, v0

    .line 1839355
    iget-object v1, p1, LX/Bzo;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;->setAuthorText(Ljava/lang/String;)V

    .line 1839356
    iget-object v1, p2, LX/Bzp;->a:LX/CkL;

    .line 1839357
    iget-object v2, v1, LX/CkL;->l:LX/CkO;

    if-eqz v2, :cond_a

    iget-object v2, v1, LX/CkL;->l:LX/CkO;

    .line 1839358
    iget-boolean v1, v2, LX/CkO;->f:Z

    move v2, v1

    .line 1839359
    if-eqz v2, :cond_a

    const/4 v2, 0x1

    :goto_4
    move v1, v2

    .line 1839360
    if-eqz v1, :cond_7

    .line 1839361
    iget-object v1, p2, LX/Bzp;->a:LX/CkL;

    .line 1839362
    iget-object v2, v1, LX/CkL;->l:LX/CkO;

    if-eqz v2, :cond_6

    iget-object v2, v1, LX/CkL;->l:LX/CkO;

    .line 1839363
    iget-boolean p1, v2, LX/CkO;->f:Z

    move v2, p1

    .line 1839364
    if-nez v2, :cond_b

    .line 1839365
    :cond_6
    const/4 v2, 0x0

    .line 1839366
    :goto_5
    move-object v1, v2

    .line 1839367
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p2, LX/Bzp;->a:LX/CkL;

    invoke-virtual {v2}, LX/CkL;->m()Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;->a(Landroid/net/Uri;I)V

    .line 1839368
    :cond_7
    goto/16 :goto_0

    :cond_8
    const/4 p0, 0x0

    goto/16 :goto_2

    .line 1839369
    :cond_9
    const-string v0, ""

    goto :goto_3

    :cond_a
    const/4 v2, 0x0

    goto :goto_4

    :cond_b
    iget-object v2, v1, LX/CkL;->l:LX/CkO;

    iget-object v2, v2, LX/CkO;->g:Ljava/lang/String;

    goto :goto_5

    .line 1839370
    :cond_c
    iget-boolean v0, p3, LX/Cko;->c:Z

    move v0, v0

    .line 1839371
    if-nez v0, :cond_d

    .line 1839372
    invoke-virtual {p3}, LX/Cko;->b()V

    .line 1839373
    :cond_d
    iget-object v0, p3, LX/Cko;->a:Lcom/facebook/richdocument/linkcovers/view/LinkCoverBasicView;

    move-object v0, v0

    .line 1839374
    if-eqz v0, :cond_2

    .line 1839375
    iget-object v1, p1, LX/Bzo;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_e

    .line 1839376
    iget-object v1, v0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverBasicView;->c:Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;

    move-object v1, v1

    .line 1839377
    if-eqz v1, :cond_e

    .line 1839378
    iget-object v1, v0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverBasicView;->c:Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;

    move-object v1, v1

    .line 1839379
    iget-object v2, p1, LX/Bzo;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;->setAuthorText(Ljava/lang/String;)V

    .line 1839380
    :cond_e
    iget-object v1, p1, LX/Bzo;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverBasicView;->setTitleText(Ljava/lang/String;)V

    .line 1839381
    iget-object v1, p1, LX/Bzo;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->y()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverBasicView;->setDescriptionText(Ljava/lang/String;)V

    .line 1839382
    invoke-static {p1}, Lcom/facebook/feedplugins/attachments/linkshare/LinkCoverPartDefinition;->c(LX/Bzo;)Landroid/net/Uri;

    move-result-object v1

    .line 1839383
    if-eqz v1, :cond_2

    .line 1839384
    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverBasicView;->setImageParams(Landroid/net/Uri;)V

    goto/16 :goto_1
.end method

.method private a(LX/Bzp;Lcom/facebook/graphql/model/GraphQLInstantArticle;LX/Bzo;)V
    .locals 5

    .prologue
    .line 1839275
    const/4 v0, 0x0

    .line 1839276
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/LinkCoverPartDefinition;->c:LX/0ad;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/LinkCoverPartDefinition;->c:LX/0ad;

    sget-short v2, LX/2yD;->y:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/LinkCoverPartDefinition;->c:LX/0ad;

    sget-short v2, LX/2yD;->z:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    move v0, v0

    .line 1839277
    if-nez v0, :cond_2

    .line 1839278
    :cond_1
    :goto_0
    return-void

    .line 1839279
    :cond_2
    iget-object v0, p1, LX/Bzp;->b:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1839280
    new-instance v0, LX/CkV;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->j()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/CkV;-><init>(Ljava/lang/String;)V

    .line 1839281
    new-instance v1, LX/CkW;

    invoke-direct {v1, v0}, LX/CkW;-><init>(LX/CkV;)V

    move-object v0, v1

    .line 1839282
    const/4 v1, 0x0

    iput-object v1, p1, LX/Bzp;->b:Ljava/lang/String;

    .line 1839283
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/LinkCoverPartDefinition;->b:LX/Ckj;

    new-instance v2, LX/Bzn;

    invoke-direct {v2, p0, p1, p2, p3}, LX/Bzn;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/LinkCoverPartDefinition;LX/Bzp;Lcom/facebook/graphql/model/GraphQLInstantArticle;LX/Bzo;)V

    .line 1839284
    iget-object v3, v1, LX/Ckj;->e:LX/CkM;

    .line 1839285
    iget-object v4, v3, LX/CkM;->a:LX/0aq;

    .line 1839286
    iget-object p0, v0, LX/CkW;->a:Ljava/lang/String;

    move-object p0, p0

    .line 1839287
    invoke-virtual {v4, p0}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/CkL;

    .line 1839288
    move-object v3, v4

    .line 1839289
    if-eqz v3, :cond_3

    .line 1839290
    invoke-virtual {v2, v3}, LX/Bzm;->a(LX/CkL;)V

    .line 1839291
    :goto_1
    goto :goto_0

    .line 1839292
    :cond_3
    new-instance v3, LX/Ckh;

    invoke-direct {v3, v1, v0, v2}, LX/Ckh;-><init>(LX/Ckj;LX/CkW;LX/Bzm;)V

    .line 1839293
    iget-object p0, v1, LX/Ckj;->c:LX/0tX;

    .line 1839294
    new-instance v4, LX/8aq;

    invoke-direct {v4}, LX/8aq;-><init>()V

    move-object v4, v4

    .line 1839295
    const-string p1, "instantArticleId"

    .line 1839296
    iget-object p2, v0, LX/CkW;->a:Ljava/lang/String;

    move-object p2, p2

    .line 1839297
    invoke-virtual {v4, p1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    check-cast v4, LX/8aq;

    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    .line 1839298
    iget-object p0, v1, LX/Ckj;->d:LX/1Ck;

    .line 1839299
    iget-object p1, v0, LX/CkW;->a:Ljava/lang/String;

    move-object p1, p1

    .line 1839300
    new-instance p2, LX/Cki;

    iget-object p3, v1, LX/Ckj;->b:LX/03V;

    .line 1839301
    iget-object v2, v0, LX/CkW;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1839302
    invoke-direct {p2, p3, v2, v3}, LX/Cki;-><init>(LX/03V;Ljava/lang/String;LX/CkU;)V

    invoke-virtual {p0, p1, v4, p2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1839303
    goto :goto_1
.end method

.method public static b(LX/Bzo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1839273
    iget-object v0, p0, LX/Bzo;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->ek()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v0

    .line 1839274
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInstantArticle;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c(LX/Bzo;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1839271
    iget-object v0, p0, LX/Bzo;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/1VO;->u(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v0

    .line 1839272
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1839264
    check-cast p2, LX/Bzo;

    .line 1839265
    iget-object v0, p2, LX/Bzo;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->ek()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v0

    .line 1839266
    new-instance v1, LX/Bzp;

    invoke-direct {v1}, LX/Bzp;-><init>()V

    .line 1839267
    if-nez v0, :cond_0

    .line 1839268
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/LinkCoverPartDefinition;->a:LX/03V;

    iget-object v2, p2, LX/Bzo;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Story attachment without instant article "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p2, LX/Bzo;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v4}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->E_()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1839269
    :goto_0
    return-object v1

    .line 1839270
    :cond_0
    invoke-direct {p0, v1, v0, p2}, Lcom/facebook/feedplugins/attachments/linkshare/LinkCoverPartDefinition;->a(LX/Bzp;Lcom/facebook/graphql/model/GraphQLInstantArticle;LX/Bzo;)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x5316d198

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1839263
    check-cast p1, LX/Bzo;

    check-cast p2, LX/Bzp;

    check-cast p4, LX/Cko;

    invoke-direct {p0, p1, p2, p4}, Lcom/facebook/feedplugins/attachments/linkshare/LinkCoverPartDefinition;->a(LX/Bzo;LX/Bzp;LX/Cko;)V

    const/16 v1, 0x1f

    const v2, -0x5d98edb7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
