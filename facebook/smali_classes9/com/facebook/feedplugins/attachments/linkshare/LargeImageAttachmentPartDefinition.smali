.class public Lcom/facebook/feedplugins/attachments/linkshare/LargeImageAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pr;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/35o;",
        ":",
        "LX/35q;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:LX/2y3;

.field private final b:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition",
            "<TE;TV;>;"
        }
    .end annotation
.end field

.field private final c:LX/2sO;

.field private final d:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition",
            "<TE;TV;>;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition",
            "<TE;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2y3;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;LX/2sO;Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1839096
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1839097
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/linkshare/LargeImageAttachmentPartDefinition;->e:Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    .line 1839098
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/LargeImageAttachmentPartDefinition;->a:LX/2y3;

    .line 1839099
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/LargeImageAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;

    .line 1839100
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/linkshare/LargeImageAttachmentPartDefinition;->c:LX/2sO;

    .line 1839101
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/linkshare/LargeImageAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    .line 1839102
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/LargeImageAttachmentPartDefinition;
    .locals 9

    .prologue
    .line 1839103
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/LargeImageAttachmentPartDefinition;

    monitor-enter v1

    .line 1839104
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/LargeImageAttachmentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1839105
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/LargeImageAttachmentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1839106
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1839107
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1839108
    new-instance v3, Lcom/facebook/feedplugins/attachments/linkshare/LargeImageAttachmentPartDefinition;

    invoke-static {v0}, LX/2y3;->a(LX/0QB;)LX/2y3;

    move-result-object v4

    check-cast v4, LX/2y3;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;

    invoke-static {v0}, LX/2sO;->a(LX/0QB;)LX/2sO;

    move-result-object v6

    check-cast v6, LX/2sO;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/attachments/linkshare/LargeImageAttachmentPartDefinition;-><init>(LX/2y3;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;LX/2sO;Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;)V

    .line 1839109
    move-object v0, v3

    .line 1839110
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1839111
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/LargeImageAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1839112
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1839113
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 1839114
    sget-object v0, Lcom/facebook/attachments/angora/AngoraAttachmentView;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1839115
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1839116
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1839117
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1839118
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/LargeImageAttachmentPartDefinition;->e:Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    invoke-interface {p1, v1, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1839119
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 1839120
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/LargeImageAttachmentPartDefinition;->a:LX/2y3;

    invoke-virtual {v1, v0}, LX/2y3;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1839121
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/LargeImageAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1839122
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/LargeImageAttachmentPartDefinition;->c:LX/2sO;

    invoke-virtual {v0, p2}, LX/2sO;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1839123
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/LargeImageAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1839124
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1839125
    const/4 v0, 0x1

    return v0
.end method
