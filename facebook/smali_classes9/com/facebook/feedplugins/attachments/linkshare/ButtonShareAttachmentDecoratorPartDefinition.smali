.class public Lcom/facebook/feedplugins/attachments/linkshare/ButtonShareAttachmentDecoratorPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/35o;",
        ":",
        "LX/35r;",
        ":",
        "LX/35q;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# instance fields
.field private final a:LX/1Nt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Nt",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;*TE;TV;>;"
        }
    .end annotation
.end field

.field private final b:LX/2sO;

.field private final c:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition",
            "<TE;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Nt;LX/2sO;Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;)V
    .locals 0
    .param p1    # LX/1Nt;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Nt",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;*TE;TV;>;",
            "LX/2sO;",
            "Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1838779
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1838780
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/ButtonShareAttachmentDecoratorPartDefinition;->a:LX/1Nt;

    .line 1838781
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/ButtonShareAttachmentDecoratorPartDefinition;->b:LX/2sO;

    .line 1838782
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/linkshare/ButtonShareAttachmentDecoratorPartDefinition;->c:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    .line 1838783
    return-void
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2sO;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/2sO;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 1838773
    invoke-virtual {p1, p0}, LX/2sO;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 1838778
    sget-object v0, Lcom/facebook/attachments/angora/AngoraAttachmentView;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1838784
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1838785
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/ButtonShareAttachmentDecoratorPartDefinition;->a:LX/1Nt;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1838786
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/ButtonShareAttachmentDecoratorPartDefinition;->c:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1838787
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1838776
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1838777
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/ButtonShareAttachmentDecoratorPartDefinition;->b:LX/2sO;

    invoke-static {p1, v0}, Lcom/facebook/feedplugins/attachments/linkshare/ButtonShareAttachmentDecoratorPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2sO;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1838774
    check-cast p4, LX/2yW;

    invoke-interface {p4}, LX/2yW;->a()V

    .line 1838775
    return-void
.end method
