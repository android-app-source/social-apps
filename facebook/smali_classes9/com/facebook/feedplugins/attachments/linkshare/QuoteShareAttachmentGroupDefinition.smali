.class public Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareAttachmentGroupDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareWithVerticalBarPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/QuoteAttachmentImageFormatSelector;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareWithVerticalBarPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/QuoteAttachmentImageFormatSelector;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1840061
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1840062
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareAttachmentGroupDefinition;->a:LX/0Ot;

    .line 1840063
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareAttachmentGroupDefinition;->b:LX/0Ot;

    .line 1840064
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareAttachmentGroupDefinition;
    .locals 5

    .prologue
    .line 1840070
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareAttachmentGroupDefinition;

    monitor-enter v1

    .line 1840071
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareAttachmentGroupDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1840072
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareAttachmentGroupDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1840073
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1840074
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1840075
    new-instance v3, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareAttachmentGroupDefinition;

    const/16 v4, 0x811

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x1e5b

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareAttachmentGroupDefinition;-><init>(LX/0Ot;LX/0Ot;)V

    .line 1840076
    move-object v0, v3

    .line 1840077
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1840078
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareAttachmentGroupDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1840079
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1840080
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1840066
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1840067
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareAttachmentGroupDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1840068
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareAttachmentGroupDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1840069
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1840065
    const/4 v0, 0x1

    return v0
.end method
