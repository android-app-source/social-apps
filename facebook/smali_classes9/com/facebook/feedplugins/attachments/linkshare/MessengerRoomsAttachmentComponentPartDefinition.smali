.class public Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/Bzs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Bzs",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:LX/1V0;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Bzs;LX/1V0;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1839568
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1839569
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentPartDefinition;->d:LX/Bzs;

    .line 1839570
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentPartDefinition;->e:LX/1V0;

    .line 1839571
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1839549
    invoke-static {p2}, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X6;

    move-result-object v1

    .line 1839550
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentPartDefinition;->d:LX/Bzs;

    const/4 v2, 0x0

    .line 1839551
    new-instance v3, LX/Bzr;

    invoke-direct {v3, v0}, LX/Bzr;-><init>(LX/Bzs;)V

    .line 1839552
    iget-object v4, v0, LX/Bzs;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Bzq;

    .line 1839553
    if-nez v4, :cond_0

    .line 1839554
    new-instance v4, LX/Bzq;

    invoke-direct {v4, v0}, LX/Bzq;-><init>(LX/Bzs;)V

    .line 1839555
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/Bzq;->a$redex0(LX/Bzq;LX/1De;IILX/Bzr;)V

    .line 1839556
    move-object v3, v4

    .line 1839557
    move-object v2, v3

    .line 1839558
    move-object v0, v2

    .line 1839559
    iget-object v2, v0, LX/Bzq;->a:LX/Bzr;

    iput-object p2, v2, LX/Bzr;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1839560
    iget-object v2, v0, LX/Bzq;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1839561
    move-object v2, v0

    .line 1839562
    move-object v0, p3

    check-cast v0, LX/1Po;

    .line 1839563
    iget-object v3, v2, LX/Bzq;->a:LX/Bzr;

    iput-object v0, v3, LX/Bzr;->b:LX/1Po;

    .line 1839564
    iget-object v3, v2, LX/Bzq;->e:Ljava/util/BitSet;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 1839565
    move-object v0, v2

    .line 1839566
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 1839567
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentPartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentPartDefinition;
    .locals 6

    .prologue
    .line 1839538
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentPartDefinition;

    monitor-enter v1

    .line 1839539
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1839540
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1839541
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1839542
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1839543
    new-instance p0, Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/Bzs;->a(LX/0QB;)LX/Bzs;

    move-result-object v4

    check-cast v4, LX/Bzs;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentPartDefinition;-><init>(Landroid/content/Context;LX/Bzs;LX/1V0;)V

    .line 1839544
    move-object v0, p0

    .line 1839545
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1839546
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1839547
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1839548
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1839572
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1839534
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/linkshare/MessengerRoomsAttachmentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1839537
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1839535
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1839536
    invoke-static {p1}, LX/1WF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method
