.class public Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareAttachmentPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pt;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/2yY;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/C0H;",
        "LX/1aZ;",
        "TE;TV;>;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static g:LX/0Xm;


# instance fields
.field private final b:LX/1Ad;

.field private final c:LX/1qa;

.field private final d:LX/1f2;

.field private final e:LX/1WM;

.field private final f:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1840239
    const-class v0, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareAttachmentPartDefinition;

    const-string v1, "newsfeed_angora_attachment_view"

    const-string v2, "small_photo"

    const-string v3, "native_newsfeed"

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareAttachmentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1qa;LX/1Ad;LX/1f2;LX/1WM;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1840232
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1840233
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareAttachmentPartDefinition;->c:LX/1qa;

    .line 1840234
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareAttachmentPartDefinition;->b:LX/1Ad;

    .line 1840235
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareAttachmentPartDefinition;->d:LX/1f2;

    .line 1840236
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareAttachmentPartDefinition;->e:LX/1WM;

    .line 1840237
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareAttachmentPartDefinition;->f:Landroid/content/res/Resources;

    .line 1840238
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareAttachmentPartDefinition;
    .locals 9

    .prologue
    .line 1840240
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareAttachmentPartDefinition;

    monitor-enter v1

    .line 1840241
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareAttachmentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1840242
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareAttachmentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1840243
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1840244
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1840245
    new-instance v3, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareAttachmentPartDefinition;

    invoke-static {v0}, LX/1qa;->a(LX/0QB;)LX/1qa;

    move-result-object v4

    check-cast v4, LX/1qa;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v5

    check-cast v5, LX/1Ad;

    invoke-static {v0}, LX/1f2;->b(LX/0QB;)LX/1f2;

    move-result-object v6

    check-cast v6, LX/1f2;

    invoke-static {v0}, LX/1WM;->a(LX/0QB;)LX/1WM;

    move-result-object v7

    check-cast v7, LX/1WM;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v8

    check-cast v8, Landroid/content/res/Resources;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareAttachmentPartDefinition;-><init>(LX/1qa;LX/1Ad;LX/1f2;LX/1WM;Landroid/content/res/Resources;)V

    .line 1840246
    move-object v0, v3

    .line 1840247
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1840248
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1840249
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1840250
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1840222
    check-cast p2, LX/C0H;

    check-cast p3, LX/1Pt;

    const/4 v0, 0x0

    .line 1840223
    iget-object v1, p2, LX/C0H;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1840224
    :cond_0
    :goto_0
    return-object v0

    .line 1840225
    :cond_1
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareAttachmentPartDefinition;->f:Landroid/content/res/Resources;

    iget v2, p2, LX/C0H;->b:I

    int-to-float v2, v2

    invoke-static {v1, v2}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v1

    .line 1840226
    iget-object v2, p2, LX/C0H;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-static {v2, v1}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLMedia;I)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1840227
    if-eqz v1, :cond_0

    .line 1840228
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareAttachmentPartDefinition;->e:LX/1WM;

    iget-object v3, p2, LX/C0H;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1WM;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1840229
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareAttachmentPartDefinition;->d:LX/1f2;

    iget-object v2, p2, LX/C0H;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareAttachmentPartDefinition;->b:LX/1Ad;

    invoke-virtual {v0, v2, v1, v3}, LX/1f2;->a(Lcom/facebook/graphql/model/GraphQLMedia;Lcom/facebook/graphql/model/GraphQLImage;LX/1Ad;)LX/1bf;

    move-result-object v0

    .line 1840230
    sget-object v1, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareAttachmentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {p3, v0, v1}, LX/1Pt;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1840231
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareAttachmentPartDefinition;->b:LX/1Ad;

    sget-object v1, Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareAttachmentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x4ef7022b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1840218
    check-cast p2, LX/1aZ;

    .line 1840219
    if-eqz p2, :cond_0

    .line 1840220
    check-cast p4, LX/2yY;

    invoke-interface {p4, p2}, LX/2yY;->setSideImageController(LX/1aZ;)V

    .line 1840221
    :cond_0
    const/16 v1, 0x1f

    const v2, 0x73d9f165

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1840216
    check-cast p4, LX/2yY;

    const/4 v0, 0x0

    invoke-interface {p4, v0}, LX/2yY;->setSideImageController(LX/1aZ;)V

    .line 1840217
    return-void
.end method
