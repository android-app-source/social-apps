.class public Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static e:LX/0Xm;


# instance fields
.field public b:LX/1qb;

.field public c:LX/C0D;

.field public d:LX/1nu;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1840607
    const-class v0, Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareComponentSpec;

    const-string v1, "newsfeed_angora_attachment_view"

    const-string v2, "small_photo"

    const-string v3, "native_newsfeed"

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1qb;LX/C0D;LX/1nu;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1840584
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1840585
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareComponentSpec;->b:LX/1qb;

    .line 1840586
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareComponentSpec;->c:LX/C0D;

    .line 1840587
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareComponentSpec;->d:LX/1nu;

    .line 1840588
    return-void
.end method

.method public static a(Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareComponentSpec;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;Z)LX/1ne;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;Z)",
            "LX/1ne;"
        }
    .end annotation

    .prologue
    .line 1840589
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v0

    .line 1840590
    if-eqz p3, :cond_0

    iget-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareComponentSpec;->b:LX/1qb;

    .line 1840591
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1840592
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {p1, v1}, LX/1qb;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    :goto_0
    move-object v1, v1

    .line 1840593
    invoke-virtual {v0, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    const v1, 0x7f0b1064

    invoke-virtual {v0, v1}, LX/1ne;->q(I)LX/1ne;

    move-result-object v0

    const v1, 0x7f0a0161

    invoke-virtual {v0, v1}, LX/1ne;->n(I)LX/1ne;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, LX/1ne;->j(I)LX/1ne;

    move-result-object v0

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, LX/1ne;->h(F)LX/1ne;

    move-result-object v0

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareComponentSpec;->b:LX/1qb;

    .line 1840594
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1840595
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {p1, v1}, LX/1qb;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/Spannable;

    move-result-object v1

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareComponentSpec;
    .locals 6

    .prologue
    .line 1840596
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareComponentSpec;

    monitor-enter v1

    .line 1840597
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareComponentSpec;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1840598
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareComponentSpec;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1840599
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1840600
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1840601
    new-instance p0, Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareComponentSpec;

    invoke-static {v0}, LX/1qb;->a(LX/0QB;)LX/1qb;

    move-result-object v3

    check-cast v3, LX/1qb;

    invoke-static {v0}, LX/C0D;->b(LX/0QB;)LX/C0D;

    move-result-object v4

    check-cast v4, LX/C0D;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v5

    check-cast v5, LX/1nu;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareComponentSpec;-><init>(LX/1qb;LX/C0D;LX/1nu;)V

    .line 1840602
    move-object v0, p0

    .line 1840603
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1840604
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1840605
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1840606
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
