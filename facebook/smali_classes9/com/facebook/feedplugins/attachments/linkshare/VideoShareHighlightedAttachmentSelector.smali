.class public Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentSelector;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/35o;",
        ":",
        "LX/35q;",
        ":",
        "LX/35r;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentPartDefinition",
            "<TE;TV;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentPartDefinition",
            "<TE;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1840841
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1840842
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentSelector;->b:Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentPartDefinition;

    .line 1840843
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentSelector;->a:Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentPartDefinition;

    .line 1840844
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentSelector;
    .locals 5

    .prologue
    .line 1840849
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentSelector;

    monitor-enter v1

    .line 1840850
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentSelector;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1840851
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentSelector;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1840852
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1840853
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1840854
    new-instance p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentSelector;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentSelector;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentPartDefinition;)V

    .line 1840855
    move-object v0, p0

    .line 1840856
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1840857
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentSelector;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1840858
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1840859
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1840860
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1840861
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentSelector;->a:Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentSelector;->b:Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 1840862
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1840845
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1840846
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1840847
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1840848
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
