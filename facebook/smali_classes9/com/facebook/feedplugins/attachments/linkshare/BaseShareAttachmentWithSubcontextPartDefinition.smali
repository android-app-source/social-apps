.class public Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentWithSubcontextPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/2yX;",
        ":",
        "LX/2yW;",
        ":",
        "LX/3Vx;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/String;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition",
            "<TE;TV;>;"
        }
    .end annotation
.end field

.field private final b:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1838745
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1838746
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentWithSubcontextPartDefinition;->a:Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    .line 1838747
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentWithSubcontextPartDefinition;->b:Landroid/content/res/Resources;

    .line 1838748
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentWithSubcontextPartDefinition;
    .locals 5

    .prologue
    .line 1838749
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentWithSubcontextPartDefinition;

    monitor-enter v1

    .line 1838750
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentWithSubcontextPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1838751
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentWithSubcontextPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1838752
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1838753
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1838754
    new-instance p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentWithSubcontextPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentWithSubcontextPartDefinition;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;Landroid/content/res/Resources;)V

    .line 1838755
    move-object v0, p0

    .line 1838756
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1838757
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentWithSubcontextPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1838758
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1838759
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 1838760
    sget-object v0, LX/3WP;->c:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1838761
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1838762
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentWithSubcontextPartDefinition;->a:Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1838763
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1838764
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/2v7;->k(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentWithSubcontextPartDefinition;->b:Landroid/content/res/Resources;

    invoke-static {v0}, LX/2v7;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x640a4665

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1838765
    check-cast p2, Ljava/lang/String;

    .line 1838766
    check-cast p4, LX/3Vx;

    invoke-interface {p4, p2}, LX/3Vx;->setSubcontextText(Ljava/lang/CharSequence;)V

    .line 1838767
    const/16 v1, 0x1f

    const v2, -0x64ba7777

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1838768
    const/4 v0, 0x1

    move v0, v0

    .line 1838769
    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1838770
    move-object v0, p4

    check-cast v0, LX/3Vx;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/3Vx;->setSubcontextText(Ljava/lang/CharSequence;)V

    .line 1838771
    check-cast p4, LX/2yW;

    invoke-interface {p4}, LX/2yW;->a()V

    .line 1838772
    return-void
.end method
