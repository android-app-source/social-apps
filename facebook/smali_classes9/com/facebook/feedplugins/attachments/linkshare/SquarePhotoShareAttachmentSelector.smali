.class public Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareAttachmentSelector;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;*",
            "LX/1Ps;",
            "*>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/feedplugins/attachments/linkshare/LargeSquarePhotoAttachmentPartDefinition;

.field private final c:Lcom/facebook/feedplugins/attachments/linkshare/SmallPhotoShareAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/SmallPhotoShareAttachmentPartDefinition",
            "<",
            "LX/1Pf;",
            "*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/linkshare/LargeSquarePhotoAttachmentPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/SmallPhotoShareAttachmentPartDefinition;LX/BzU;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1840442
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1840443
    new-instance v2, Lcom/facebook/feedplugins/attachments/linkshare/ButtonShareAttachmentDecoratorPartDefinition;

    invoke-static {p3}, LX/2sO;->a(LX/0QB;)LX/2sO;

    move-result-object v0

    check-cast v0, LX/2sO;

    invoke-static {p3}, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    invoke-direct {v2, p2, v0, v1}, Lcom/facebook/feedplugins/attachments/linkshare/ButtonShareAttachmentDecoratorPartDefinition;-><init>(LX/1Nt;LX/2sO;Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;)V

    .line 1840444
    move-object v0, v2

    .line 1840445
    iput-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareAttachmentSelector;->a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    .line 1840446
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareAttachmentSelector;->b:Lcom/facebook/feedplugins/attachments/linkshare/LargeSquarePhotoAttachmentPartDefinition;

    .line 1840447
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareAttachmentSelector;->c:Lcom/facebook/feedplugins/attachments/linkshare/SmallPhotoShareAttachmentPartDefinition;

    .line 1840448
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareAttachmentSelector;
    .locals 6

    .prologue
    .line 1840449
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareAttachmentSelector;

    monitor-enter v1

    .line 1840450
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareAttachmentSelector;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1840451
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareAttachmentSelector;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1840452
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1840453
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1840454
    new-instance p0, Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareAttachmentSelector;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/LargeSquarePhotoAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/LargeSquarePhotoAttachmentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/attachments/linkshare/LargeSquarePhotoAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/SmallPhotoShareAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/SmallPhotoShareAttachmentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/attachments/linkshare/SmallPhotoShareAttachmentPartDefinition;

    const-class v5, LX/BzU;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/BzU;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareAttachmentSelector;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/LargeSquarePhotoAttachmentPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/SmallPhotoShareAttachmentPartDefinition;LX/BzU;)V

    .line 1840455
    move-object v0, p0

    .line 1840456
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1840457
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareAttachmentSelector;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1840458
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1840459
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1840460
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1840461
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareAttachmentSelector;->a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareAttachmentSelector;->b:Lcom/facebook/feedplugins/attachments/linkshare/LargeSquarePhotoAttachmentPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/SquarePhotoShareAttachmentSelector;->c:Lcom/facebook/feedplugins/attachments/linkshare/SmallPhotoShareAttachmentPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 1840462
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1840463
    const/4 v0, 0x1

    move v0, v0

    .line 1840464
    return v0
.end method
