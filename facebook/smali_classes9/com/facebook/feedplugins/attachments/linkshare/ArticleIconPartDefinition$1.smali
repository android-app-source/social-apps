.class public final Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:Landroid/view/View;

.field public final synthetic c:Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;Landroid/view/View;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1838632
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition$1;->c:Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;

    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition$1;->a:Landroid/view/View;

    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition$1;->b:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 1838633
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1838634
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition$1;->c:Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;

    iget-boolean v1, v1, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->q:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition$1;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1838635
    :cond_0
    :goto_0
    return-void

    .line 1838636
    :cond_1
    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition$1;->c:Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->p:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->a()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1e

    div-int/lit16 v1, v1, 0xa0

    if-lt v0, v1, :cond_0

    .line 1838637
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition$1;->b:Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition$1;->c:Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->o:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1838638
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition$1;->c:Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition$1;->c:Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;

    iget-wide v2, v2, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->j:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    .line 1838639
    const/4 v1, 0x2

    if-ge v0, v1, :cond_2

    .line 1838640
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition$1;->c:Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition$1;->b:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->b$redex0(Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;Landroid/view/View;)V

    goto :goto_0

    .line 1838641
    :cond_2
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition$1;->c:Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;

    .line 1838642
    iget-object v2, v1, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->o:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget v3, v1, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->k:I

    sub-int v3, v2, v3

    .line 1838643
    iget-object v2, v1, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->o:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    iget v4, v1, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->l:I

    sub-int/2addr v2, v4

    .line 1838644
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v4

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v5

    if-le v4, v5, :cond_5

    .line 1838645
    :goto_1
    mul-int/lit16 v2, v2, 0x3e8

    div-int/2addr v2, v0

    .line 1838646
    iget v3, v1, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->m:I

    sub-int v3, v2, v3

    mul-int/lit16 v3, v3, 0x3e8

    div-int/2addr v3, v0

    iput v3, v1, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->n:I

    .line 1838647
    iput v2, v1, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->m:I

    .line 1838648
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition$1;->c:Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;

    const/4 v1, 0x1

    .line 1838649
    iget v2, v0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->m:I

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    iget-object v3, v0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->p:LX/0hB;

    invoke-virtual {v3}, LX/0hB;->a()I

    move-result v3

    div-int/2addr v2, v3

    .line 1838650
    if-gtz v2, :cond_3

    if-le v2, v1, :cond_6

    iget v2, v0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->m:I

    int-to-float v2, v2

    invoke-static {v2}, Ljava/lang/Math;->signum(F)F

    move-result v2

    iget v3, v0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->n:I

    int-to-float v3, v3

    invoke-static {v3}, Ljava/lang/Math;->signum(F)F

    move-result v3

    mul-float/2addr v2, v3

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_6

    .line 1838651
    :cond_3
    :goto_2
    move v0, v1

    .line 1838652
    if-eqz v0, :cond_4

    .line 1838653
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition$1;->c:Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition$1;->b:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->b$redex0(Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;Landroid/view/View;)V

    goto/16 :goto_0

    .line 1838654
    :cond_4
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition$1;->c:Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->f:LX/0hs;

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition$1;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/0ht;->a(Landroid/view/View;)V

    .line 1838655
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition$1;->c:Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->i:LX/0i1;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition$1;->c:Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->a:LX/0iA;

    if-eqz v0, :cond_0

    .line 1838656
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition$1;->c:Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->a:LX/0iA;

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition$1;->c:Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->i:LX/0i1;

    invoke-virtual {v0, v1}, LX/0iA;->a(LX/0i1;)V

    .line 1838657
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition$1;->c:Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->a:LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition$1;->c:Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->i:LX/0i1;

    invoke-interface {v1}, LX/0i1;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    move v2, v3

    goto :goto_1

    :cond_6
    const/4 v1, 0x0

    goto :goto_2
.end method
