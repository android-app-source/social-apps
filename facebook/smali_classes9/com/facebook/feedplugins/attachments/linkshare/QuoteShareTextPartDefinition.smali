.class public Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareTextPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/C0C;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1840110
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1840111
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareTextPartDefinition;
    .locals 3

    .prologue
    .line 1840112
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareTextPartDefinition;

    monitor-enter v1

    .line 1840113
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareTextPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1840114
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareTextPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1840115
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1840116
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1840117
    new-instance v0, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareTextPartDefinition;

    invoke-direct {v0}, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareTextPartDefinition;-><init>()V

    .line 1840118
    move-object v0, v0

    .line 1840119
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1840120
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareTextPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1840121
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1840122
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1840109
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x7026a8e8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1840104
    check-cast p1, LX/C0C;

    check-cast p2, Ljava/lang/Void;

    check-cast p4, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

    .line 1840105
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;->a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V

    .line 1840106
    iget-object v1, p1, LX/C0C;->a:Ljava/lang/CharSequence;

    invoke-virtual {p4, v1}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1840107
    iget v1, p1, LX/C0C;->b:I

    invoke-virtual {p4, v1}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->setMaxLines(I)V

    .line 1840108
    const/16 v1, 0x1f

    const v2, 0x67ed0492

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final bridge synthetic b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1840103
    return-void
.end method
