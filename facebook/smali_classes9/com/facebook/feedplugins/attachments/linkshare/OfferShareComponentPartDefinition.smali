.class public Lcom/facebook/feedplugins/attachments/linkshare/OfferShareComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/1Pm;",
        ">;"
    }
.end annotation


# instance fields
.field private final d:LX/C09;

.field private final e:LX/1V0;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/C09;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1840015
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1840016
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/OfferShareComponentPartDefinition;->d:LX/C09;

    .line 1840017
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/linkshare/OfferShareComponentPartDefinition;->e:LX/1V0;

    .line 1840018
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/1Pm;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1839997
    invoke-static {p2}, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X6;

    move-result-object v0

    .line 1839998
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/OfferShareComponentPartDefinition;->d:LX/C09;

    const/4 v2, 0x0

    .line 1839999
    new-instance v3, LX/C08;

    invoke-direct {v3, v1}, LX/C08;-><init>(LX/C09;)V

    .line 1840000
    sget-object v4, LX/C09;->a:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/C07;

    .line 1840001
    if-nez v4, :cond_0

    .line 1840002
    new-instance v4, LX/C07;

    invoke-direct {v4}, LX/C07;-><init>()V

    .line 1840003
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/C07;->a$redex0(LX/C07;LX/1De;IILX/C08;)V

    .line 1840004
    move-object v3, v4

    .line 1840005
    move-object v2, v3

    .line 1840006
    move-object v1, v2

    .line 1840007
    iget-object v2, v1, LX/C07;->a:LX/C08;

    iput-object p2, v2, LX/C08;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1840008
    iget-object v2, v1, LX/C07;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1840009
    move-object v1, v1

    .line 1840010
    iget-object v2, v1, LX/C07;->a:LX/C08;

    iput-object p3, v2, LX/C08;->b:LX/1Pm;

    .line 1840011
    iget-object v2, v1, LX/C07;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1840012
    move-object v1, v1

    .line 1840013
    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 1840014
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/linkshare/OfferShareComponentPartDefinition;->e:LX/1V0;

    invoke-virtual {v2, p1, p3, v0, v1}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1839996
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/linkshare/OfferShareComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1840019
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/linkshare/OfferShareComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1839995
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1839993
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1839994
    invoke-static {p1}, LX/1WF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method
