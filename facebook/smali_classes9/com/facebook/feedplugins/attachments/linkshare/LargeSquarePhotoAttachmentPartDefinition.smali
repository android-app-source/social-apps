.class public Lcom/facebook/feedplugins/attachments/linkshare/LargeSquarePhotoAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/3VZ;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition",
            "<TE;",
            "LX/3VZ;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareAttachmentPartDefinition",
            "<TE;",
            "LX/3VZ;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/35i;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/35i;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/SidePhotoShareAttachmentPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1839214
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1839215
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/linkshare/LargeSquarePhotoAttachmentPartDefinition;->a:LX/0Ot;

    .line 1839216
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/LargeSquarePhotoAttachmentPartDefinition;->c:LX/0Ot;

    .line 1839217
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/linkshare/LargeSquarePhotoAttachmentPartDefinition;->b:LX/0Ot;

    .line 1839218
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/LargeSquarePhotoAttachmentPartDefinition;
    .locals 6

    .prologue
    .line 1839219
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/LargeSquarePhotoAttachmentPartDefinition;

    monitor-enter v1

    .line 1839220
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/LargeSquarePhotoAttachmentPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1839221
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/LargeSquarePhotoAttachmentPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1839222
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1839223
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1839224
    new-instance v3, Lcom/facebook/feedplugins/attachments/linkshare/LargeSquarePhotoAttachmentPartDefinition;

    const/16 v4, 0x7ff

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x80b

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0x1e61

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, p0}, Lcom/facebook/feedplugins/attachments/linkshare/LargeSquarePhotoAttachmentPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 1839225
    move-object v0, v3

    .line 1839226
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1839227
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/LargeSquarePhotoAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1839228
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1839229
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/3VZ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1839230
    sget-object v0, LX/3VZ;->j:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1839231
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1839232
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/LargeSquarePhotoAttachmentPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1839233
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/LargeSquarePhotoAttachmentPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v3, LX/C0H;

    .line 1839234
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1839235
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/linkshare/LargeSquarePhotoAttachmentPartDefinition;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/35i;

    invoke-virtual {v2, p2}, LX/35i;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v2

    invoke-direct {v3, v1, v2}, LX/C0H;-><init>(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)V

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1839236
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1839237
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1839238
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/LargeSquarePhotoAttachmentPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/35i;

    invoke-virtual {v0, p1}, LX/35i;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
