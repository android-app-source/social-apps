.class public Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/1Pm;",
        ">;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final d:LX/C0T;

.field private final e:LX/1V0;

.field public final f:LX/1V4;

.field public g:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/C0T;LX/1V0;LX/1V4;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1840711
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1840712
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentComponentPartDefinition;->d:LX/C0T;

    .line 1840713
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentComponentPartDefinition;->e:LX/1V0;

    .line 1840714
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentComponentPartDefinition;->f:LX/1V4;

    .line 1840715
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/1Pm;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1840716
    invoke-static {p2}, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X6;

    move-result-object v0

    .line 1840717
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentComponentPartDefinition;->d:LX/C0T;

    const/4 v2, 0x0

    .line 1840718
    new-instance v3, LX/C0S;

    invoke-direct {v3, v1}, LX/C0S;-><init>(LX/C0T;)V

    .line 1840719
    sget-object v4, LX/C0T;->a:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/C0R;

    .line 1840720
    if-nez v4, :cond_0

    .line 1840721
    new-instance v4, LX/C0R;

    invoke-direct {v4}, LX/C0R;-><init>()V

    .line 1840722
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/C0R;->a$redex0(LX/C0R;LX/1De;IILX/C0S;)V

    .line 1840723
    move-object v3, v4

    .line 1840724
    move-object v2, v3

    .line 1840725
    move-object v1, v2

    .line 1840726
    iget-object v2, v1, LX/C0R;->a:LX/C0S;

    iput-object p2, v2, LX/C0S;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1840727
    iget-object v2, v1, LX/C0R;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1840728
    move-object v1, v1

    .line 1840729
    iget-object v2, v1, LX/C0R;->a:LX/C0S;

    iput-object p3, v2, LX/C0S;->b:LX/1Pm;

    .line 1840730
    iget-object v2, v1, LX/C0R;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1840731
    move-object v1, v1

    .line 1840732
    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 1840733
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentComponentPartDefinition;->e:LX/1V0;

    invoke-virtual {v2, p1, p3, v0, v1}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentComponentPartDefinition;
    .locals 7

    .prologue
    .line 1840734
    const-class v1, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentComponentPartDefinition;

    monitor-enter v1

    .line 1840735
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1840736
    sput-object v2, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1840737
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1840738
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1840739
    new-instance p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/C0T;->a(LX/0QB;)LX/C0T;

    move-result-object v4

    check-cast v4, LX/C0T;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-static {v0}, LX/1V4;->a(LX/0QB;)LX/1V4;

    move-result-object v6

    check-cast v6, LX/1V4;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentComponentPartDefinition;-><init>(Landroid/content/Context;LX/C0T;LX/1V0;LX/1V4;)V

    .line 1840740
    move-object v0, p0

    .line 1840741
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1840742
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1840743
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1840744
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1840745
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1840746
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1840747
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentComponentPartDefinition;->g:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 1840748
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentComponentPartDefinition;->f:LX/1V4;

    invoke-virtual {v0}, LX/1V4;->c()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentComponentPartDefinition;->g:Ljava/lang/Boolean;

    .line 1840749
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareAttachmentComponentPartDefinition;->g:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    move v0, v0

    .line 1840750
    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1840751
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1840752
    invoke-static {p1}, LX/1WF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 1840753
    sget-object v0, Lcom/facebook/components/feed/ComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method
