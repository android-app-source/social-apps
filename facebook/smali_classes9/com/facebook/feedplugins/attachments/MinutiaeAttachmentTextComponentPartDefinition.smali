.class public Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentTextComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/Byd;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final d:LX/1WE;

.field private final e:LX/1V0;

.field private final f:F

.field private final g:F

.field private final h:LX/Byb;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1WE;LX/1V0;LX/0ad;LX/Byb;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1837500
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1837501
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentTextComponentPartDefinition;->d:LX/1WE;

    .line 1837502
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentTextComponentPartDefinition;->e:LX/1V0;

    .line 1837503
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentTextComponentPartDefinition;->h:LX/Byb;

    .line 1837504
    sget v0, LX/0wi;->l:F

    const/high16 v1, 0x40000000    # 2.0f

    invoke-interface {p4, v0, v1}, LX/0ad;->a(FF)F

    move-result v0

    iput v0, p0, Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentTextComponentPartDefinition;->f:F

    .line 1837505
    sget v0, LX/0wi;->k:F

    const/high16 v1, 0x41000000    # 8.0f

    invoke-interface {p4, v0, v1}, LX/0ad;->a(FF)F

    move-result v0

    iput v0, p0, Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentTextComponentPartDefinition;->g:F

    .line 1837506
    return-void
.end method

.method private a(LX/1De;LX/Byd;LX/1Pn;)LX/1X1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/Byd;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1837507
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentTextComponentPartDefinition;->d:LX/1WE;

    invoke-virtual {v0, p1}, LX/1WE;->c(LX/1De;)LX/1XG;

    move-result-object v0

    invoke-virtual {p2}, LX/Byd;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1XG;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1XG;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/1XG;->a(LX/1Pn;)LX/1XG;

    move-result-object v0

    const v1, 0x7f0b004e

    invoke-virtual {v0, v1}, LX/1XG;->h(I)LX/1XG;

    move-result-object v0

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, LX/1XG;->a(Landroid/graphics/Typeface;)LX/1XG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentTextComponentPartDefinition;->h:LX/Byb;

    .line 1837508
    new-instance v4, LX/Bya;

    const-class v2, Landroid/content/Context;

    invoke-interface {v1, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {v1}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v3

    check-cast v3, LX/1Uf;

    const/16 v5, 0x2eb

    invoke-static {v1, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-direct {v4, p2, v2, v3, v5}, LX/Bya;-><init>(LX/Byd;Landroid/content/Context;LX/1Uf;LX/0Or;)V

    .line 1837509
    move-object v1, v4

    .line 1837510
    iget-object v2, v0, LX/1XG;->a:LX/1XE;

    iput-object v1, v2, LX/1XE;->i:LX/1xz;

    .line 1837511
    move-object v0, v0

    .line 1837512
    sget-object v1, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v0, v1}, LX/1XG;->a(Landroid/text/Layout$Alignment;)LX/1XG;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 1837513
    new-instance v1, LX/1X6;

    invoke-virtual {p2}, LX/Byd;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    invoke-static {}, LX/1UY;->g()LX/1UY;

    move-result-object v3

    iget v4, p0, Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentTextComponentPartDefinition;->f:F

    .line 1837514
    iput v4, v3, LX/1UY;->b:F

    .line 1837515
    move-object v3, v3

    .line 1837516
    iget v4, p0, Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentTextComponentPartDefinition;->g:F

    .line 1837517
    iput v4, v3, LX/1UY;->c:F

    .line 1837518
    move-object v3, v3

    .line 1837519
    invoke-virtual {v3}, LX/1UY;->i()LX/1Ua;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 1837520
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentTextComponentPartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentTextComponentPartDefinition;
    .locals 9

    .prologue
    .line 1837521
    const-class v1, Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentTextComponentPartDefinition;

    monitor-enter v1

    .line 1837522
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentTextComponentPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1837523
    sput-object v2, Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentTextComponentPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1837524
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1837525
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1837526
    new-instance v3, Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentTextComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1WE;->a(LX/0QB;)LX/1WE;

    move-result-object v5

    check-cast v5, LX/1WE;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v6

    check-cast v6, LX/1V0;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    const-class v8, LX/Byb;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/Byb;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentTextComponentPartDefinition;-><init>(Landroid/content/Context;LX/1WE;LX/1V0;LX/0ad;LX/Byb;)V

    .line 1837527
    move-object v0, v3

    .line 1837528
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1837529
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentTextComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1837530
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1837531
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1837532
    check-cast p2, LX/Byd;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentTextComponentPartDefinition;->a(LX/1De;LX/Byd;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1837533
    check-cast p2, LX/Byd;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentTextComponentPartDefinition;->a(LX/1De;LX/Byd;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1837534
    check-cast p1, LX/Byd;

    .line 1837535
    iget-object v0, p1, LX/Byd;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/Byd;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1837536
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1837537
    check-cast p1, LX/Byd;

    .line 1837538
    invoke-virtual {p1}, LX/Byd;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-static {v0}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 1837539
    sget-object v0, Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
