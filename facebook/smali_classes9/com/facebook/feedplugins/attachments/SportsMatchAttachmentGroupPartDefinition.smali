.class public Lcom/facebook/feedplugins/attachments/SportsMatchAttachmentGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/attachments/SportsMatchPhotoPartDefinition;

.field private final b:Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;

.field private final c:Lcom/facebook/feedplugins/attachments/SportsMatchFooterTextPartDefinition;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/SportsMatchPhotoPartDefinition;Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;Lcom/facebook/feedplugins/attachments/SportsMatchFooterTextPartDefinition;LX/0Or;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/feedplugins/attachments/IsSportsAttachmentModuleEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedplugins/attachments/SportsMatchPhotoPartDefinition;",
            "Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;",
            "Lcom/facebook/feedplugins/attachments/SportsMatchFooterTextPartDefinition;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1837765
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1837766
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/SportsMatchAttachmentGroupPartDefinition;->a:Lcom/facebook/feedplugins/attachments/SportsMatchPhotoPartDefinition;

    .line 1837767
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/SportsMatchAttachmentGroupPartDefinition;->b:Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;

    .line 1837768
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/SportsMatchAttachmentGroupPartDefinition;->c:Lcom/facebook/feedplugins/attachments/SportsMatchFooterTextPartDefinition;

    .line 1837769
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/SportsMatchAttachmentGroupPartDefinition;->d:LX/0Or;

    .line 1837770
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/SportsMatchAttachmentGroupPartDefinition;
    .locals 7

    .prologue
    .line 1837771
    const-class v1, Lcom/facebook/feedplugins/attachments/SportsMatchAttachmentGroupPartDefinition;

    monitor-enter v1

    .line 1837772
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/SportsMatchAttachmentGroupPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1837773
    sput-object v2, Lcom/facebook/feedplugins/attachments/SportsMatchAttachmentGroupPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1837774
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1837775
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1837776
    new-instance v6, Lcom/facebook/feedplugins/attachments/SportsMatchAttachmentGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/SportsMatchPhotoPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/SportsMatchPhotoPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/attachments/SportsMatchPhotoPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/SportsMatchFooterTextPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/SportsMatchFooterTextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/attachments/SportsMatchFooterTextPartDefinition;

    const/16 p0, 0x14b7

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v6, v3, v4, v5, p0}, Lcom/facebook/feedplugins/attachments/SportsMatchAttachmentGroupPartDefinition;-><init>(Lcom/facebook/feedplugins/attachments/SportsMatchPhotoPartDefinition;Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;Lcom/facebook/feedplugins/attachments/SportsMatchFooterTextPartDefinition;LX/0Or;)V

    .line 1837777
    move-object v0, v6

    .line 1837778
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1837779
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/SportsMatchAttachmentGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1837780
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1837781
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1837782
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1837783
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/SportsMatchAttachmentGroupPartDefinition;->a:Lcom/facebook/feedplugins/attachments/SportsMatchPhotoPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1837784
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/SportsMatchAttachmentGroupPartDefinition;->b:Lcom/facebook/feedplugins/attachments/SportsMatchScorePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1837785
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/SportsMatchAttachmentGroupPartDefinition;->c:Lcom/facebook/feedplugins/attachments/SportsMatchFooterTextPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1837786
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1837787
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/SportsMatchAttachmentGroupPartDefinition;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
