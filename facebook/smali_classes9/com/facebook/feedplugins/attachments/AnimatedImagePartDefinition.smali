.class public Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/Bxo;",
        "LX/1PW;",
        "LX/ByP;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static g:LX/0Xm;


# instance fields
.field public final b:LX/1Ad;

.field public final c:LX/0Zb;

.field public final d:LX/0SG;

.field private final e:LX/17V;

.field private final f:LX/2mt;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1836060
    const-class v0, Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;

    const-string v1, "newsfeed_image_share_view"

    const-string v2, "animated_image"

    const-string v3, "native_newsfeed"

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1Ad;LX/0Zb;LX/0SG;LX/17V;LX/2mt;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1836053
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1836054
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;->b:LX/1Ad;

    .line 1836055
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;->c:LX/0Zb;

    .line 1836056
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;->d:LX/0SG;

    .line 1836057
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;->e:LX/17V;

    .line 1836058
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;->f:LX/2mt;

    .line 1836059
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;
    .locals 9

    .prologue
    .line 1836042
    const-class v1, Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;

    monitor-enter v1

    .line 1836043
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1836044
    sput-object v2, Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1836045
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1836046
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1836047
    new-instance v3, Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v4

    check-cast v4, LX/1Ad;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-static {v0}, LX/17V;->b(LX/0QB;)LX/17V;

    move-result-object v7

    check-cast v7, LX/17V;

    invoke-static {v0}, LX/2mt;->a(LX/0QB;)LX/2mt;

    move-result-object v8

    check-cast v8, LX/2mt;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;-><init>(LX/1Ad;LX/0Zb;LX/0SG;LX/17V;LX/2mt;)V

    .line 1836048
    move-object v0, v3

    .line 1836049
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1836050
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1836051
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1836052
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;LX/Bxo;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Bxo;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1836061
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 1836062
    if-eqz v0, :cond_1

    invoke-static {v0}, LX/181;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    .line 1836063
    :goto_0
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;->f:LX/2mt;

    invoke-virtual {v1, p2}, LX/2mt;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "native_newsfeed"

    .line 1836064
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0lF;->e()I

    move-result v3

    if-eqz v3, :cond_0

    if-nez v1, :cond_4

    .line 1836065
    :cond_0
    const/4 v3, 0x0

    .line 1836066
    :goto_1
    move-object v0, v3

    .line 1836067
    if-nez v0, :cond_2

    .line 1836068
    :goto_2
    return-void

    .line 1836069
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1836070
    :cond_2
    invoke-static {v0}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1836071
    iget-object v1, p1, LX/Bxo;->h:LX/ByP;

    invoke-static {v0, v1}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/view/View;)V

    .line 1836072
    :cond_3
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;->c:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_2

    .line 1836073
    :cond_4
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p2, "inline_play_gif"

    invoke-direct {v3, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p2, "url"

    .line 1836074
    iput-object p2, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->d:Ljava/lang/String;

    .line 1836075
    move-object v3, v3

    .line 1836076
    iput-object v1, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->e:Ljava/lang/String;

    .line 1836077
    move-object v3, v3

    .line 1836078
    const-string p2, "tracking"

    invoke-virtual {v3, p2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 1836079
    iput-object v2, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1836080
    move-object v3, v3

    .line 1836081
    goto :goto_1
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1836024
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1836025
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1836026
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1836027
    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/ImageShareUtil;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1836028
    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/ImageShareUtil;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 1836029
    new-instance v2, LX/Bxo;

    iget-object v3, p0, Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;->b:LX/1Ad;

    invoke-static {v3, v1}, Lcom/facebook/feedplugins/attachments/ImageShareUtil;->a(LX/1Ad;Lcom/facebook/graphql/model/GraphQLImage;)LX/1aZ;

    move-result-object v3

    invoke-static {v1}, Lcom/facebook/feedplugins/attachments/ImageShareUtil;->a(Lcom/facebook/graphql/model/GraphQLImage;)F

    move-result v4

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v5

    .line 1836030
    iget-object v6, v5, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v6, v6

    .line 1836031
    check-cast v6, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1836032
    new-instance v7, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "animated_image_session"

    invoke-direct {v7, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "tracking"

    invoke-static {v5}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object p3

    invoke-virtual {v7, p1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p1, "story_graphql_id"

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v7, p1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p1, "story_legacy_api_post_id"

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, p1, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "native_newsfeed"

    .line 1836033
    iput-object v7, v6, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1836034
    move-object v6, v6

    .line 1836035
    move-object v5, v6

    .line 1836036
    sget-object v6, LX/Bxn;->LOAD_ON_CLICK:LX/Bxn;

    invoke-direct {v2, v3, v4, v5, v6}, LX/Bxo;-><init>(LX/1aZ;FLcom/facebook/analytics/logger/HoneyClientEvent;LX/Bxn;)V

    .line 1836037
    new-instance v3, LX/Bxl;

    invoke-direct {v3, p0, v2}, LX/Bxl;-><init>(Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;LX/Bxo;)V

    move-object v3, v3

    .line 1836038
    iget-object v4, p0, Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;->b:LX/1Ad;

    invoke-virtual {v4}, LX/1Ad;->o()LX/1Ad;

    move-result-object v4

    sget-object v5, Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v5}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v4

    invoke-static {v0}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v4

    invoke-static {v1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v5

    invoke-static {v5}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/1Ae;->d(Ljava/lang/Object;)LX/1Ae;

    move-result-object v4

    check-cast v4, LX/1Ad;

    invoke-virtual {v4, v3}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v4

    check-cast v4, LX/1Ad;

    invoke-virtual {v4}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v4

    move-object v0, v4

    .line 1836039
    new-instance v1, LX/Bxk;

    invoke-direct {v1, p0, v2, v0, p2}, LX/Bxk;-><init>(Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;LX/Bxo;LX/1aZ;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    move-object v0, v1

    .line 1836040
    iput-object v0, v2, LX/Bxo;->b:Landroid/view/View$OnClickListener;

    .line 1836041
    return-object v2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 6

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x25d6b767

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1836002
    check-cast p2, LX/Bxo;

    check-cast p4, LX/ByP;

    .line 1836003
    iget-object v4, p2, LX/Bxo;->f:LX/Bxn;

    iput-object v4, p2, LX/Bxo;->g:LX/Bxn;

    .line 1836004
    iput-object p4, p2, LX/Bxo;->h:LX/ByP;

    .line 1836005
    iget-object v4, p2, LX/Bxo;->h:LX/ByP;

    iget v5, p2, LX/Bxo;->c:F

    invoke-virtual {v4, v5}, LX/ByP;->setAspectRatio(F)V

    .line 1836006
    iget-object v4, p2, LX/Bxo;->h:LX/ByP;

    iget-object v5, p2, LX/Bxo;->a:LX/1aZ;

    invoke-virtual {v4, v5}, LX/ByP;->setImageController(LX/1aZ;)V

    .line 1836007
    iget-object v4, p2, LX/Bxo;->g:LX/Bxn;

    sget-object v5, LX/Bxn;->LOAD_ON_CLICK:LX/Bxn;

    if-ne v4, v5, :cond_0

    .line 1836008
    iget-object v4, p2, LX/Bxo;->h:LX/ByP;

    sget-object v5, LX/6Wv;->READY_TO_PLAY:LX/6Wv;

    invoke-virtual {v4, v5}, LX/ByP;->setPlayButtonState(LX/6Wv;)V

    .line 1836009
    :goto_0
    iget-object v4, p2, LX/Bxo;->h:LX/ByP;

    iget-object v5, p2, LX/Bxo;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, LX/ByP;->setOnImageClickListener(Landroid/view/View$OnClickListener;)V

    .line 1836010
    iget-object v4, p0, Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;->d:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    iput-wide v4, p2, LX/Bxo;->e:J

    .line 1836011
    const/16 v1, 0x1f

    const v2, 0x1fcd87d9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1836012
    :cond_0
    iget-object v4, p2, LX/Bxo;->h:LX/ByP;

    sget-object v5, LX/6Wv;->LOADING:LX/6Wv;

    invoke-virtual {v4, v5}, LX/ByP;->setPlayButtonState(LX/6Wv;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 9

    .prologue
    .line 1836013
    check-cast p2, LX/Bxo;

    const/4 v2, 0x0

    .line 1836014
    iget-object v0, p2, LX/Bxo;->h:LX/ByP;

    invoke-virtual {v0, v2}, LX/ByP;->setOnImageClickListener(Landroid/view/View$OnClickListener;)V

    .line 1836015
    iget-object v0, p2, LX/Bxo;->h:LX/ByP;

    sget-object v1, LX/6Wv;->HIDDEN:LX/6Wv;

    invoke-virtual {v0, v1}, LX/ByP;->setPlayButtonState(LX/6Wv;)V

    .line 1836016
    iput-object v2, p2, LX/Bxo;->h:LX/ByP;

    .line 1836017
    iget-object v3, p2, LX/Bxo;->d:Lcom/facebook/analytics/logger/HoneyClientEvent;

    if-nez v3, :cond_0

    .line 1836018
    :goto_0
    return-void

    .line 1836019
    :cond_0
    iget-object v3, p2, LX/Bxo;->g:LX/Bxn;

    sget-object v4, LX/Bxn;->LOAD_ON_CLICK:LX/Bxn;

    if-eq v3, v4, :cond_1

    const/4 v3, 0x1

    .line 1836020
    :goto_1
    iget-object v4, p2, LX/Bxo;->d:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "was_tapped"

    invoke-virtual {v4, v5, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1836021
    iget-object v3, p2, LX/Bxo;->d:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "time_spent"

    iget-object v5, p0, Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;->d:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v5

    iget-wide v7, p2, LX/Bxo;->e:J

    sub-long/2addr v5, v7

    invoke-virtual {v3, v4, v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1836022
    iget-object v3, p0, Lcom/facebook/feedplugins/attachments/AnimatedImagePartDefinition;->c:LX/0Zb;

    iget-object v4, p2, LX/Bxo;->d:Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-interface {v3, v4}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0

    .line 1836023
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method
