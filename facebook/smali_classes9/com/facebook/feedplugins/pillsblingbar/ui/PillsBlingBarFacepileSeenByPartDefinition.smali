.class public Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarFacepileSeenByPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        "LX/CAG;",
        "TE;",
        "Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0if;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0if;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "Lcom/facebook/funnellogger/FunnelLogger;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1855073
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1855074
    iput-object p1, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarFacepileSeenByPartDefinition;->a:LX/0Ot;

    .line 1855075
    iput-object p2, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarFacepileSeenByPartDefinition;->b:LX/0Ot;

    .line 1855076
    iput-object p3, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarFacepileSeenByPartDefinition;->c:LX/0if;

    .line 1855077
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarFacepileSeenByPartDefinition;
    .locals 6

    .prologue
    .line 1855078
    const-class v1, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarFacepileSeenByPartDefinition;

    monitor-enter v1

    .line 1855079
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarFacepileSeenByPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1855080
    sput-object v2, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarFacepileSeenByPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1855081
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1855082
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1855083
    new-instance v4, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarFacepileSeenByPartDefinition;

    const/16 v3, 0xbc6

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v3, 0x455

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v3

    check-cast v3, LX/0if;

    invoke-direct {v4, v5, p0, v3}, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarFacepileSeenByPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0if;)V

    .line 1855084
    move-object v0, v4

    .line 1855085
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1855086
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarFacepileSeenByPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1855087
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1855088
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1855089
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStory;

    check-cast p3, LX/1Po;

    .line 1855090
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 1855091
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->K()Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->K()Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSeenByConnection;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->K()Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSeenByConnection;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 1855092
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->K()Lcom/facebook/graphql/model/GraphQLSeenByConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSeenByConnection;->j()LX/0Px;

    move-result-object v0

    .line 1855093
    :goto_1
    new-instance v1, LX/CAG;

    .line 1855094
    new-instance v2, LX/CAF;

    invoke-direct {v2, p0, p2, p3}, LX/CAF;-><init>(Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarFacepileSeenByPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;LX/1Po;)V

    move-object v2, v2

    .line 1855095
    invoke-direct {v1, v0, v2}, LX/CAG;-><init>(LX/0Px;Landroid/view/View$OnClickListener;)V

    return-object v1

    .line 1855096
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1855097
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 8
    .annotation build Lcom/facebook/infer/annotation/IgnoreAllocations;
    .end annotation

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x1753355

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1855098
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    check-cast p2, LX/CAG;

    check-cast p4, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    .line 1855099
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->A()I

    move-result v4

    iget-object v5, p2, LX/CAG;->b:LX/0Px;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p4, v4, v5, v6}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->a(ILX/0Px;Ljava/lang/String;)V

    .line 1855100
    iget-object v4, p2, LX/CAG;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v4}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setSeenByFacepileClickListener(Landroid/view/View$OnClickListener;)V

    .line 1855101
    const v4, 0x7f0d008e

    invoke-virtual {p4, v4}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setId(I)V

    .line 1855102
    iget-object v4, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarFacepileSeenByPartDefinition;->c:LX/0if;

    sget-object v5, LX/0ig;->aH:LX/0ih;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v6

    invoke-virtual {v4, v5, v6, v7}, LX/0if;->a(LX/0ih;J)V

    .line 1855103
    const/16 v1, 0x1f

    const v2, -0x354fcc42    # -5773791.0f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4
    .annotation build Lcom/facebook/infer/annotation/IgnoreAllocations;
    .end annotation

    .prologue
    .line 1855104
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    check-cast p4, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 1855105
    invoke-virtual {p4, v1, v0, v0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->a(ILX/0Px;Ljava/lang/String;)V

    .line 1855106
    invoke-virtual {p4, v0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setSeenByFacepileClickListener(Landroid/view/View$OnClickListener;)V

    .line 1855107
    invoke-virtual {p4, v1}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setId(I)V

    .line 1855108
    iget-object v0, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarFacepileSeenByPartDefinition;->c:LX/0if;

    sget-object v1, LX/0ig;->aH:LX/0ih;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, LX/0if;->c(LX/0ih;J)V

    .line 1855109
    return-void
.end method
