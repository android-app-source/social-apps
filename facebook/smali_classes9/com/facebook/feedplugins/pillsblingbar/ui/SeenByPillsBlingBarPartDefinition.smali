.class public Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByPillsBlingBarPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarPartDefinition;

.field private final b:Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarSeenByBinderPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarPartDefinition;Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarSeenByBinderPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1855168
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1855169
    iput-object p1, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByPillsBlingBarPartDefinition;->a:Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarPartDefinition;

    .line 1855170
    iput-object p2, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByPillsBlingBarPartDefinition;->b:Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarSeenByBinderPartDefinition;

    .line 1855171
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByPillsBlingBarPartDefinition;
    .locals 5

    .prologue
    .line 1855179
    const-class v1, Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByPillsBlingBarPartDefinition;

    monitor-enter v1

    .line 1855180
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByPillsBlingBarPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1855181
    sput-object v2, Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByPillsBlingBarPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1855182
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1855183
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1855184
    new-instance p0, Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByPillsBlingBarPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarSeenByBinderPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarSeenByBinderPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarSeenByBinderPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByPillsBlingBarPartDefinition;-><init>(Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarPartDefinition;Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarSeenByBinderPartDefinition;)V

    .line 1855185
    move-object v0, p0

    .line 1855186
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1855187
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByPillsBlingBarPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1855188
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1855189
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 1855190
    sget-object v0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1855173
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1855174
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1855175
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1855176
    iget-object v1, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByPillsBlingBarPartDefinition;->a:Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarPartDefinition;

    invoke-interface {p1, v1, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1855177
    iget-object v1, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByPillsBlingBarPartDefinition;->b:Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarSeenByBinderPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1855178
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1855172
    const/4 v0, 0x1

    return v0
.end method
