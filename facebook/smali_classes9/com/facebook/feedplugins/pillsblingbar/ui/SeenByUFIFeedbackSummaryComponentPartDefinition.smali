.class public Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummaryComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final d:LX/0if;

.field private final e:LX/CAM;

.field private final f:LX/1V0;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1XK;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/CAE;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0if;LX/CAM;LX/1V0;LX/0Ot;LX/CAE;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/funnellogger/FunnelLogger;",
            "LX/CAM;",
            "LX/1V0;",
            "LX/0Ot",
            "<",
            "LX/1XK;",
            ">;",
            "LX/CAE;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1855314
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1855315
    iput-object p2, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummaryComponentPartDefinition;->d:LX/0if;

    .line 1855316
    iput-object p3, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummaryComponentPartDefinition;->e:LX/CAM;

    .line 1855317
    iput-object p4, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummaryComponentPartDefinition;->f:LX/1V0;

    .line 1855318
    iput-object p5, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummaryComponentPartDefinition;->g:LX/0Ot;

    .line 1855319
    iput-object p6, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummaryComponentPartDefinition;->h:LX/CAE;

    .line 1855320
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pe;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1855293
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1855294
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1855295
    iget-object v1, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummaryComponentPartDefinition;->e:LX/CAM;

    const/4 v2, 0x0

    .line 1855296
    new-instance v3, LX/CAL;

    invoke-direct {v3, v1}, LX/CAL;-><init>(LX/CAM;)V

    .line 1855297
    sget-object v4, LX/CAM;->a:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/CAK;

    .line 1855298
    if-nez v4, :cond_0

    .line 1855299
    new-instance v4, LX/CAK;

    invoke-direct {v4}, LX/CAK;-><init>()V

    .line 1855300
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/CAK;->a$redex0(LX/CAK;LX/1De;IILX/CAL;)V

    .line 1855301
    move-object v3, v4

    .line 1855302
    move-object v2, v3

    .line 1855303
    move-object v1, v2

    .line 1855304
    iget-object v2, v1, LX/CAK;->a:LX/CAL;

    iput-object v0, v2, LX/CAL;->a:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1855305
    iget-object v2, v1, LX/CAK;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1855306
    move-object v1, v1

    .line 1855307
    move-object v0, p3

    check-cast v0, LX/1Po;

    .line 1855308
    iget-object v2, v1, LX/CAK;->a:LX/CAL;

    iput-object v0, v2, LX/CAL;->b:LX/1Po;

    .line 1855309
    iget-object v2, v1, LX/CAK;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1855310
    move-object v0, v1

    .line 1855311
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 1855312
    iget-object v2, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummaryComponentPartDefinition;->f:LX/1V0;

    move-object v0, p3

    check-cast v0, LX/1Ps;

    new-instance v3, LX/1X6;

    sget-object v4, LX/1Ua;->p:LX/1Ua;

    invoke-direct {v3, p2, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-virtual {v2, p1, v0, v3, v1}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v1

    .line 1855313
    iget-object v0, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummaryComponentPartDefinition;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1XK;

    invoke-virtual {v0, p1}, LX/1XK;->c(LX/1De;)LX/1XM;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1XM;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1XM;

    move-result-object v0

    check-cast p3, LX/1Po;

    invoke-virtual {v0, p3}, LX/1XM;->a(LX/1Po;)LX/1XM;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, LX/1XM;->a(Z)LX/1XM;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/1XM;->a(LX/1X1;)LX/1XM;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummaryComponentPartDefinition;
    .locals 10

    .prologue
    .line 1855269
    const-class v1, Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummaryComponentPartDefinition;

    monitor-enter v1

    .line 1855270
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummaryComponentPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1855271
    sput-object v2, Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummaryComponentPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1855272
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1855273
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1855274
    new-instance v3, Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummaryComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v5

    check-cast v5, LX/0if;

    invoke-static {v0}, LX/CAM;->a(LX/0QB;)LX/CAM;

    move-result-object v6

    check-cast v6, LX/CAM;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v7

    check-cast v7, LX/1V0;

    const/16 v8, 0x853

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/CAE;->a(LX/0QB;)LX/CAE;

    move-result-object v9

    check-cast v9, LX/CAE;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummaryComponentPartDefinition;-><init>(Landroid/content/Context;LX/0if;LX/CAM;LX/1V0;LX/0Ot;LX/CAE;)V

    .line 1855275
    move-object v0, v3

    .line 1855276
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1855277
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummaryComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1855278
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1855279
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dV;LX/1Pe;Lcom/facebook/components/feed/FeedComponentView;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1dV;",
            "TE;",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1855326
    check-cast p3, LX/1Pn;

    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(Ljava/lang/Object;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V

    .line 1855327
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1855328
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1855329
    iget-object v1, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummaryComponentPartDefinition;->d:LX/0if;

    sget-object v2, LX/0ig;->aG:LX/0ih;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, LX/0if;->a(LX/0ih;J)V

    .line 1855330
    return-void
.end method

.method private b(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dV;LX/1Pe;Lcom/facebook/components/feed/FeedComponentView;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1dV;",
            "TE;",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1855321
    check-cast p3, LX/1Pn;

    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/components/feed/ComponentPartDefinition;->b(Ljava/lang/Object;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V

    .line 1855322
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1855323
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1855324
    iget-object v1, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummaryComponentPartDefinition;->d:LX/0if;

    sget-object v2, LX/0ig;->aG:LX/0ih;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, LX/0if;->c(LX/0ih;J)V

    .line 1855325
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1855291
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pe;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummaryComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pe;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1855292
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pe;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummaryComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pe;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V
    .locals 0

    .prologue
    .line 1855290
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pe;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummaryComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dV;LX/1Pe;Lcom/facebook/components/feed/FeedComponentView;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x271c47b7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1855289
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/1dV;

    check-cast p3, LX/1Pe;

    check-cast p4, Lcom/facebook/components/feed/FeedComponentView;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummaryComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dV;LX/1Pe;Lcom/facebook/components/feed/FeedComponentView;)V

    const/16 v1, 0x1f

    const v2, -0x285ab5f2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1855284
    iget-object v0, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummaryComponentPartDefinition;->h:LX/CAE;

    .line 1855285
    iget-object v1, v0, LX/CAE;->b:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 1855286
    iget-object v1, v0, LX/CAE;->a:LX/0ad;

    sget-short p0, LX/CAD;->a:S

    const/4 p1, 0x0

    invoke-interface {v1, p0, p1}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, LX/CAE;->b:Ljava/lang/Boolean;

    .line 1855287
    :cond_0
    iget-object v1, v0, LX/CAE;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v0, v1

    .line 1855288
    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1855282
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1855283
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic b(Ljava/lang/Object;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V
    .locals 0

    .prologue
    .line 1855281
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pe;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummaryComponentPartDefinition;->b(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dV;LX/1Pe;Lcom/facebook/components/feed/FeedComponentView;)V

    return-void
.end method

.method public final bridge synthetic b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1855280
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/1dV;

    check-cast p3, LX/1Pe;

    check-cast p4, Lcom/facebook/components/feed/FeedComponentView;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/feedplugins/pillsblingbar/ui/SeenByUFIFeedbackSummaryComponentPartDefinition;->b(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dV;LX/1Pe;Lcom/facebook/components/feed/FeedComponentView;)V

    return-void
.end method
