.class public Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarSeenByBinderPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        "LX/CAI;",
        "TE;",
        "Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0if;

.field public final b:LX/CAJ;


# direct methods
.method public constructor <init>(LX/CAJ;LX/0if;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1855116
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1855117
    iput-object p1, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarSeenByBinderPartDefinition;->b:LX/CAJ;

    .line 1855118
    iput-object p2, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarSeenByBinderPartDefinition;->a:LX/0if;

    .line 1855119
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarSeenByBinderPartDefinition;
    .locals 5

    .prologue
    .line 1855120
    const-class v1, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarSeenByBinderPartDefinition;

    monitor-enter v1

    .line 1855121
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarSeenByBinderPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1855122
    sput-object v2, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarSeenByBinderPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1855123
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1855124
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1855125
    new-instance p0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarSeenByBinderPartDefinition;

    invoke-static {v0}, LX/CAJ;->a(LX/0QB;)LX/CAJ;

    move-result-object v3

    check-cast v3, LX/CAJ;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v4

    check-cast v4, LX/0if;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarSeenByBinderPartDefinition;-><init>(LX/CAJ;LX/0if;)V

    .line 1855126
    move-object v0, p0

    .line 1855127
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1855128
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarSeenByBinderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1855129
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1855130
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1855131
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStory;

    check-cast p3, LX/1Po;

    .line 1855132
    new-instance v0, LX/CAI;

    .line 1855133
    new-instance v1, LX/CAH;

    invoke-direct {v1, p0, p2, p3}, LX/CAH;-><init>(Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarSeenByBinderPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;LX/1Po;)V

    move-object v1, v1

    .line 1855134
    invoke-direct {v0, v1}, LX/CAI;-><init>(Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 8

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x33ec9df4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1855135
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    check-cast p2, LX/CAI;

    check-cast p4, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    .line 1855136
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->A()I

    move-result v4

    invoke-virtual {p4, v4}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setSeenByCount(I)V

    .line 1855137
    iget-object v4, p2, LX/CAI;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v4}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setSeenByClickListener(Landroid/view/View$OnClickListener;)V

    .line 1855138
    const v4, 0x7f0d008e

    invoke-virtual {p4, v4}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setId(I)V

    .line 1855139
    iget-object v4, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarSeenByBinderPartDefinition;->a:LX/0if;

    sget-object v5, LX/0ig;->aG:LX/0ih;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v6

    invoke-virtual {v4, v5, v6, v7}, LX/0if;->a(LX/0ih;J)V

    .line 1855140
    const/16 v1, 0x1f

    const v2, 0x4f5c3ef3    # 3.69511296E9f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    .line 1855141
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    check-cast p4, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    .line 1855142
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setSeenByCount(I)V

    .line 1855143
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setSeenByClickListener(Landroid/view/View$OnClickListener;)V

    .line 1855144
    iget-object v0, p0, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarSeenByBinderPartDefinition;->a:LX/0if;

    sget-object v1, LX/0ig;->aG:LX/0ih;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, LX/0if;->c(LX/0ih;J)V

    .line 1855145
    return-void
.end method
