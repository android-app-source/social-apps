.class public final Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterBinderUtil$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/3lC;

.field public final synthetic b:Landroid/view/View;

.field public final synthetic c:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final synthetic d:LX/20l;

.field public final synthetic e:LX/20m;

.field public final synthetic f:Ljava/lang/String;

.field public final synthetic g:LX/0iA;


# direct methods
.method public constructor <init>(LX/3lC;Landroid/view/View;Lcom/facebook/graphql/model/GraphQLFeedback;LX/20l;LX/20m;Ljava/lang/String;LX/0iA;)V
    .locals 0

    .prologue
    .line 1846822
    iput-object p1, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterBinderUtil$1;->a:LX/3lC;

    iput-object p2, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterBinderUtil$1;->b:Landroid/view/View;

    iput-object p3, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterBinderUtil$1;->c:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object p4, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterBinderUtil$1;->d:LX/20l;

    iput-object p5, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterBinderUtil$1;->e:LX/20m;

    iput-object p6, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterBinderUtil$1;->f:Ljava/lang/String;

    iput-object p7, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterBinderUtil$1;->g:LX/0iA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1846823
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterBinderUtil$1;->a:LX/3lC;

    iget-object v1, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterBinderUtil$1;->b:Landroid/view/View;

    iget-object v2, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterBinderUtil$1;->c:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0, v1, v2}, LX/3lC;->a(Landroid/view/View;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1846824
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterBinderUtil$1;->d:LX/20l;

    .line 1846825
    iget-object v1, v0, LX/20l;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/1zs;->e:LX/0Tn;

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 1846826
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterBinderUtil$1;->e:LX/20m;

    iget-object v1, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterBinderUtil$1;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterBinderUtil$1;->c:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/9BS;->FOOTER:LX/9BS;

    iget-object v4, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterBinderUtil$1;->a:LX/3lC;

    invoke-virtual {v4}, LX/3lC;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/20m;->a(Ljava/lang/String;Ljava/lang/String;LX/9BS;Ljava/lang/String;)V

    .line 1846827
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterBinderUtil$1;->g:LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterBinderUtil$1;->a:LX/3lC;

    invoke-virtual {v1}, LX/3lC;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 1846828
    return-void
.end method
