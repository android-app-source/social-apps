.class public Lcom/facebook/feedplugins/spannable/SpannablePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/1xz;",
        "Ljava/lang/Boolean;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1858116
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1858117
    iput-object p1, p0, Lcom/facebook/feedplugins/spannable/SpannablePartDefinition;->a:Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;

    .line 1858118
    iput-object p2, p0, Lcom/facebook/feedplugins/spannable/SpannablePartDefinition;->b:Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;

    .line 1858119
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/spannable/SpannablePartDefinition;
    .locals 5

    .prologue
    .line 1858120
    const-class v1, Lcom/facebook/feedplugins/spannable/SpannablePartDefinition;

    monitor-enter v1

    .line 1858121
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/spannable/SpannablePartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1858122
    sput-object v2, Lcom/facebook/feedplugins/spannable/SpannablePartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1858123
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1858124
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1858125
    new-instance p0, Lcom/facebook/feedplugins/spannable/SpannablePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/spannable/SpannablePartDefinition;-><init>(Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;)V

    .line 1858126
    move-object v0, p0

    .line 1858127
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1858128
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/spannable/SpannablePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1858129
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1858130
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1858131
    check-cast p2, LX/1xz;

    .line 1858132
    invoke-interface {p2}, LX/1xz;->d()LX/1nq;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 1858133
    :goto_0
    if-eqz v0, :cond_1

    .line 1858134
    iget-object v1, p0, Lcom/facebook/feedplugins/spannable/SpannablePartDefinition;->a:Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;

    invoke-interface {p1, v1, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1858135
    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 1858136
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1858137
    :cond_1
    iget-object v1, p0, Lcom/facebook/feedplugins/spannable/SpannablePartDefinition;->b:Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;

    invoke-interface {p1, v1, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x24cae049

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1858138
    check-cast p2, Ljava/lang/Boolean;

    .line 1858139
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1858140
    instance-of v1, p4, Lcom/facebook/fbui/widget/text/TextLayoutView;

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1858141
    :goto_0
    const/16 v1, 0x1f

    const v2, -0x53acc99

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1858142
    :cond_0
    instance-of v1, p4, Lcom/facebook/widget/text/BetterTextView;

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    goto :goto_0
.end method
