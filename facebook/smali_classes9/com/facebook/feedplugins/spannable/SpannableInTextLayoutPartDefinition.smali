.class public Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/1xz;",
        "LX/CCd;",
        "TE;",
        "Lcom/facebook/fbui/widget/text/TextLayoutView;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/1DR;

.field private final b:LX/1xu;

.field public final c:LX/03V;

.field public final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1DR;LX/1xu;LX/03V;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1858110
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1858111
    iput-object p1, p0, Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;->a:LX/1DR;

    .line 1858112
    iput-object p2, p0, Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;->b:LX/1xu;

    .line 1858113
    iput-object p3, p0, Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;->c:LX/03V;

    .line 1858114
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;->d:Ljava/util/Set;

    .line 1858115
    return-void
.end method

.method public static a(LX/1xz;LX/1Pr;I)Landroid/text/Layout;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1xz;",
            "TE;I)",
            "Landroid/text/Layout;"
        }
    .end annotation

    .prologue
    .line 1858076
    invoke-interface {p0}, LX/1xz;->d()LX/1nq;

    move-result-object v0

    .line 1858077
    invoke-virtual {v0, p2}, LX/1nq;->a(I)LX/1nq;

    .line 1858078
    invoke-interface {p0}, LX/1xz;->a()LX/1KL;

    move-result-object v1

    invoke-interface {p0}, LX/1xz;->c()LX/0jW;

    move-result-object p2

    invoke-interface {p1, v1, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1yT;

    .line 1858079
    iget-object v1, v1, LX/1yT;->a:Landroid/text/Spannable;

    move-object v1, v1

    .line 1858080
    invoke-virtual {v0, v1}, LX/1nq;->a(Ljava/lang/CharSequence;)LX/1nq;

    .line 1858081
    invoke-virtual {v0}, LX/1nq;->c()Landroid/text/Layout;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;
    .locals 6

    .prologue
    .line 1858082
    const-class v1, Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;

    monitor-enter v1

    .line 1858083
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1858084
    sput-object v2, Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1858085
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1858086
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1858087
    new-instance p0, Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v3

    check-cast v3, LX/1DR;

    invoke-static {v0}, LX/1xu;->a(LX/0QB;)LX/1xu;

    move-result-object v4

    check-cast v4, LX/1xu;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;-><init>(LX/1DR;LX/1xu;LX/03V;)V

    .line 1858088
    move-object v0, p0

    .line 1858089
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1858090
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1858091
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1858092
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1858093
    check-cast p2, LX/1xz;

    check-cast p3, LX/1Pr;

    .line 1858094
    iget-object v0, p0, Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;->b:LX/1xu;

    invoke-virtual {v0, p2, p3}, LX/1xu;->a(LX/1xz;LX/1Pr;)V

    .line 1858095
    invoke-interface {p2}, LX/1xz;->e()I

    move-result v2

    .line 1858096
    new-instance v0, LX/CCd;

    invoke-static {p2, p3, v2}, Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;->a(LX/1xz;LX/1Pr;I)Landroid/text/Layout;

    move-result-object v1

    .line 1858097
    new-instance v3, LX/CCc;

    invoke-direct {v3, p0, p2}, LX/CCc;-><init>(Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;LX/1xz;)V

    move-object v3, v3

    .line 1858098
    new-instance v4, LX/CCb;

    invoke-direct {v4, p0, p2, p3}, LX/CCb;-><init>(Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;LX/1xz;LX/1Pr;)V

    .line 1858099
    invoke-interface {p2}, LX/1xz;->a()LX/1KL;

    move-result-object v5

    invoke-interface {p2}, LX/1xz;->c()LX/0jW;

    move-result-object p0

    invoke-interface {p3, v5, p0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1yT;

    .line 1858100
    iget-object v5, v5, LX/1yT;->c:LX/2nR;

    move-object v5, v5

    .line 1858101
    invoke-direct/range {v0 .. v5}, LX/CCd;-><init>(Landroid/text/Layout;ILX/34L;LX/5Of;LX/2nR;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x3a06e4e7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1858102
    check-cast p2, LX/CCd;

    check-cast p4, Lcom/facebook/fbui/widget/text/TextLayoutView;

    .line 1858103
    iget-object v1, p2, LX/CCd;->b:Landroid/text/Layout;

    iget-object v2, p2, LX/CCd;->d:LX/5Of;

    iget-object p0, p2, LX/CCd;->c:LX/34L;

    invoke-virtual {p4, v1, v2, p0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->a(Landroid/text/Layout;LX/5Of;LX/34L;)V

    .line 1858104
    iget-object v1, p2, LX/CCd;->e:LX/2nR;

    if-eqz v1, :cond_0

    .line 1858105
    iget-object v1, p2, LX/CCd;->e:LX/2nR;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/text/TextLayoutView;->setAttachDetachListener(LX/2nR;)V

    .line 1858106
    :cond_0
    const/16 v1, 0x1f

    const v2, -0x4f66dd28

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1858107
    check-cast p4, Lcom/facebook/fbui/widget/text/TextLayoutView;

    .line 1858108
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->setAttachDetachListener(LX/2nR;)V

    .line 1858109
    return-void
.end method
