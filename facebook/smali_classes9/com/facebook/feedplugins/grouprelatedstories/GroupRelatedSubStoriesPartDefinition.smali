.class public Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedSubStoriesPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/hotconversations/HotConversationSubStoryPartDefinition;

.field private final b:Lcom/facebook/feedplugins/condensedstory/CondensedStoryPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/hotconversations/HotConversationSubStoryPartDefinition;Lcom/facebook/feedplugins/condensedstory/CondensedStoryPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1852472
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1852473
    iput-object p2, p0, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedSubStoriesPartDefinition;->b:Lcom/facebook/feedplugins/condensedstory/CondensedStoryPartDefinition;

    .line 1852474
    iput-object p1, p0, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedSubStoriesPartDefinition;->a:Lcom/facebook/feedplugins/hotconversations/HotConversationSubStoryPartDefinition;

    .line 1852475
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedSubStoriesPartDefinition;
    .locals 5

    .prologue
    .line 1852461
    const-class v1, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedSubStoriesPartDefinition;

    monitor-enter v1

    .line 1852462
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedSubStoriesPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1852463
    sput-object v2, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedSubStoriesPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1852464
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1852465
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1852466
    new-instance p0, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedSubStoriesPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/hotconversations/HotConversationSubStoryPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/hotconversations/HotConversationSubStoryPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/hotconversations/HotConversationSubStoryPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/condensedstory/CondensedStoryPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/condensedstory/CondensedStoryPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/condensedstory/CondensedStoryPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedSubStoriesPartDefinition;-><init>(Lcom/facebook/feedplugins/hotconversations/HotConversationSubStoryPartDefinition;Lcom/facebook/feedplugins/condensedstory/CondensedStoryPartDefinition;)V

    .line 1852467
    move-object v0, p0

    .line 1852468
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1852469
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedSubStoriesPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1852470
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1852471
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 1852448
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v8, 0x0

    .line 1852449
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1852450
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1Wr;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;

    .line 1852451
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->k()LX/0Px;

    move-result-object v3

    .line 1852452
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnitItem;

    .line 1852453
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    invoke-static {v5}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v5

    .line 1852454
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnitItem;->c()Ljava/lang/String;

    move-result-object v1

    .line 1852455
    iput-object v1, v5, LX/23u;->aM:Ljava/lang/String;

    .line 1852456
    invoke-virtual {v5}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1852457
    new-instance v5, LX/C33;

    sget-object v6, LX/C34;->GROUP_RELATED_STORIES:LX/C34;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->c()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v8, v6, v1, v7}, LX/C33;-><init>(Landroid/view/View$OnClickListener;LX/C34;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)V

    .line 1852458
    iget-object v1, p0, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedSubStoriesPartDefinition;->b:Lcom/facebook/feedplugins/condensedstory/CondensedStoryPartDefinition;

    invoke-static {p1, v1, v5}, LX/1RG;->a(LX/1RF;Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    iget-object v6, p0, Lcom/facebook/feedplugins/grouprelatedstories/GroupRelatedSubStoriesPartDefinition;->a:Lcom/facebook/feedplugins/hotconversations/HotConversationSubStoryPartDefinition;

    invoke-virtual {v1, v6, v5}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 1852459
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1852460
    :cond_0
    return-object v8
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1852440
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1852441
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1852442
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1852443
    invoke-static {v0}, LX/1Wr;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object p0

    .line 1852444
    instance-of p1, p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;

    if-eqz p1, :cond_1

    .line 1852445
    check-cast p0, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupRelatedStoriesFeedUnit;->k()LX/0Px;

    move-result-object p0

    .line 1852446
    :goto_0
    move-object v0, p0

    .line 1852447
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method
