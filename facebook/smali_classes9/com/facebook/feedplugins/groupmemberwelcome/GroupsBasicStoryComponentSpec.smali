.class public Lcom/facebook/feedplugins/groupmemberwelcome/GroupsBasicStoryComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pb;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static g:LX/0Xm;


# instance fields
.field private final b:LX/1VD;

.field public final c:LX/1nu;

.field public final d:LX/1nA;

.field public final e:LX/1zC;

.field public final f:LX/C4M;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1852415
    const-class v0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupsBasicStoryComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupsBasicStoryComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;LX/1VD;LX/C4M;LX/1nA;LX/1zC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1852416
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1852417
    iput-object p1, p0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupsBasicStoryComponentSpec;->c:LX/1nu;

    .line 1852418
    iput-object p2, p0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupsBasicStoryComponentSpec;->b:LX/1VD;

    .line 1852419
    iput-object p3, p0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupsBasicStoryComponentSpec;->f:LX/C4M;

    .line 1852420
    iput-object p4, p0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupsBasicStoryComponentSpec;->d:LX/1nA;

    .line 1852421
    iput-object p5, p0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupsBasicStoryComponentSpec;->e:LX/1zC;

    .line 1852422
    return-void
.end method

.method public static a(Lcom/facebook/feedplugins/groupmemberwelcome/GroupsBasicStoryComponentSpec;LX/1De;LX/1Pn;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X4;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "TE;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/1X4;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 1852423
    iget-object v0, p0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupsBasicStoryComponentSpec;->b:LX/1VD;

    invoke-virtual {v0, p1}, LX/1VD;->c(LX/1De;)LX/1X4;

    move-result-object v0

    check-cast p2, LX/1Pb;

    invoke-virtual {v0, p2}, LX/1X4;->a(LX/1Pb;)LX/1X4;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/1X4;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X4;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/1X4;->g(Z)LX/1X4;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/1X4;->d(Z)LX/1X4;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/1X4;->e(Z)LX/1X4;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/1X4;->c(Z)LX/1X4;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/1X4;->a(Z)LX/1X4;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, LX/1X4;->i(I)LX/1X4;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/groupmemberwelcome/GroupsBasicStoryComponentSpec;
    .locals 9

    .prologue
    .line 1852424
    const-class v1, Lcom/facebook/feedplugins/groupmemberwelcome/GroupsBasicStoryComponentSpec;

    monitor-enter v1

    .line 1852425
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupsBasicStoryComponentSpec;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1852426
    sput-object v2, Lcom/facebook/feedplugins/groupmemberwelcome/GroupsBasicStoryComponentSpec;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1852427
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1852428
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1852429
    new-instance v3, Lcom/facebook/feedplugins/groupmemberwelcome/GroupsBasicStoryComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v4

    check-cast v4, LX/1nu;

    invoke-static {v0}, LX/1VD;->a(LX/0QB;)LX/1VD;

    move-result-object v5

    check-cast v5, LX/1VD;

    invoke-static {v0}, LX/C4M;->a(LX/0QB;)LX/C4M;

    move-result-object v6

    check-cast v6, LX/C4M;

    invoke-static {v0}, LX/1nA;->a(LX/0QB;)LX/1nA;

    move-result-object v7

    check-cast v7, LX/1nA;

    invoke-static {v0}, LX/1zC;->a(LX/0QB;)LX/1zC;

    move-result-object v8

    check-cast v8, LX/1zC;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/groupmemberwelcome/GroupsBasicStoryComponentSpec;-><init>(LX/1nu;LX/1VD;LX/C4M;LX/1nA;LX/1zC;)V

    .line 1852430
    move-object v0, v3

    .line 1852431
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1852432
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupsBasicStoryComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1852433
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1852434
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1852435
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aP()I

    move-result v1

    if-lez v1, :cond_1

    .line 1852436
    :cond_0
    :goto_0
    return v0

    .line 1852437
    :cond_1
    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 1852438
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v1, v2, v3}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 1

    .prologue
    .line 1852439
    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-static {v0}, LX/1VO;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    return v0
.end method
