.class public Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomePhotoGridSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/MountSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static e:LX/0Xm;


# instance fields
.field private final b:LX/1Ad;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/23R;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1g8;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1852133
    const-class v0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomePhotoGridSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomePhotoGridSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1Ad;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ad;",
            "LX/0Ot",
            "<",
            "LX/23R;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1g8;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1852134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1852135
    iput-object p1, p0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomePhotoGridSpec;->b:LX/1Ad;

    .line 1852136
    iput-object p2, p0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomePhotoGridSpec;->c:LX/0Ot;

    .line 1852137
    iput-object p3, p0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomePhotoGridSpec;->d:LX/0Ot;

    .line 1852138
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomePhotoGridSpec;
    .locals 6

    .prologue
    .line 1852139
    const-class v1, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomePhotoGridSpec;

    monitor-enter v1

    .line 1852140
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomePhotoGridSpec;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1852141
    sput-object v2, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomePhotoGridSpec;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1852142
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1852143
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1852144
    new-instance v4, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomePhotoGridSpec;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v3

    check-cast v3, LX/1Ad;

    const/16 v5, 0xf2f

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0xb2d

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, v5, p0}, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomePhotoGridSpec;-><init>(LX/1Ad;LX/0Ot;LX/0Ot;)V

    .line 1852145
    move-object v0, v4

    .line 1852146
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1852147
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomePhotoGridSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1852148
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1852149
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/fig/mediagrid/FigMediaGrid;Lcom/facebook/graphql/model/GraphQLGroup;)V
    .locals 10
    .param p3    # Lcom/facebook/graphql/model/GraphQLGroup;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param

    .prologue
    .line 1852150
    invoke-static {p3}, LX/C84;->b(Lcom/facebook/graphql/model/GraphQLGroup;)LX/0Px;

    move-result-object v5

    .line 1852151
    if-eqz v5, :cond_0

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1852152
    :cond_0
    :goto_0
    return-void

    .line 1852153
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1852154
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 1852155
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 1852156
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v7

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v7, :cond_2

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1852157
    iget-object v1, p0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomePhotoGridSpec;->b:LX/1Ad;

    sget-object v8, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomePhotoGridSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v8}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v1

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f080072

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1852158
    iput-object v8, v1, LX/1Ae;->p:Ljava/lang/String;

    .line 1852159
    invoke-virtual {v1}, LX/1Ae;->n()LX/1Ae;

    move-result-object v9

    move-object v1, v9

    .line 1852160
    check-cast v1, LX/1Ad;

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    .line 1852161
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1852162
    invoke-static {v0}, LX/5k9;->a(Lcom/facebook/graphql/model/GraphQLMedia;)LX/5kD;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1852163
    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1852164
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1852165
    :cond_2
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v0

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 p1, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 1852166
    const/4 v1, 0x5

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1852167
    const/4 v1, 0x0

    :goto_2
    move-object v0, v1

    .line 1852168
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {p2, v1, v0}, Lcom/facebook/fig/mediagrid/FigMediaGrid;->a(LX/0Px;LX/6WP;)Lcom/facebook/fig/mediagrid/FigMediaGrid;

    .line 1852169
    const-class v0, LX/23W;

    new-instance v1, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;-><init>(LX/0Px;)V

    invoke-static {v0, v1}, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a(Ljava/lang/Class;Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;)Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    move-result-object v3

    .line 1852170
    new-instance v0, LX/C7z;

    move-object v1, p0

    move-object v2, p3

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, LX/C7z;-><init>(Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomePhotoGridSpec;Lcom/facebook/graphql/model/GraphQLGroup;Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;LX/0Pz;LX/0Px;Lcom/facebook/fig/mediagrid/FigMediaGrid;)V

    invoke-virtual {p2, v0}, Lcom/facebook/fig/mediagrid/FigMediaGrid;->a(LX/6WM;)Lcom/facebook/fig/mediagrid/FigMediaGrid;

    goto/16 :goto_0

    .line 1852171
    :pswitch_0
    invoke-static {}, LX/6WP;->newBuilder()LX/6WO;

    move-result-object v1

    .line 1852172
    iput v2, v1, LX/6WO;->b:F

    .line 1852173
    move-object v1, v1

    .line 1852174
    invoke-virtual {v1, v8, v8, v7, v7}, LX/6WO;->a(IIII)LX/6WO;

    move-result-object v1

    invoke-virtual {v1}, LX/6WO;->a()LX/6WP;

    move-result-object v1

    goto :goto_2

    .line 1852175
    :pswitch_1
    invoke-static {}, LX/6WP;->newBuilder()LX/6WO;

    move-result-object v1

    const/high16 v2, 0x3fc00000    # 1.5f

    .line 1852176
    iput v2, v1, LX/6WO;->b:F

    .line 1852177
    move-object v1, v1

    .line 1852178
    invoke-virtual {v1, v8, v8, v7, v7}, LX/6WO;->a(IIII)LX/6WO;

    move-result-object v1

    invoke-virtual {v1, v8, v7, v7, v7}, LX/6WO;->a(IIII)LX/6WO;

    move-result-object v1

    invoke-virtual {v1}, LX/6WO;->a()LX/6WP;

    move-result-object v1

    goto :goto_2

    .line 1852179
    :pswitch_2
    invoke-static {}, LX/6WP;->newBuilder()LX/6WO;

    move-result-object v1

    .line 1852180
    iput v2, v1, LX/6WO;->b:F

    .line 1852181
    move-object v1, v1

    .line 1852182
    invoke-virtual {v1, v8, v8, v9, v9}, LX/6WO;->a(IIII)LX/6WO;

    move-result-object v1

    invoke-virtual {v1, v9, v8, v7, v7}, LX/6WO;->a(IIII)LX/6WO;

    move-result-object v1

    invoke-virtual {v1, v9, v7, v7, v7}, LX/6WO;->a(IIII)LX/6WO;

    move-result-object v1

    invoke-virtual {v1}, LX/6WO;->a()LX/6WP;

    move-result-object v1

    goto :goto_2

    .line 1852183
    :pswitch_3
    invoke-static {}, LX/6WP;->newBuilder()LX/6WO;

    move-result-object v1

    .line 1852184
    iput v2, v1, LX/6WO;->b:F

    .line 1852185
    move-object v1, v1

    .line 1852186
    invoke-virtual {v1, v8, v8, v7, v7}, LX/6WO;->a(IIII)LX/6WO;

    move-result-object v1

    invoke-virtual {v1, v7, v8, v7, v7}, LX/6WO;->a(IIII)LX/6WO;

    move-result-object v1

    invoke-virtual {v1, v8, v7, v7, v7}, LX/6WO;->a(IIII)LX/6WO;

    move-result-object v1

    invoke-virtual {v1, v7, v7, v7, v7}, LX/6WO;->a(IIII)LX/6WO;

    move-result-object v1

    invoke-virtual {v1}, LX/6WO;->a()LX/6WP;

    move-result-object v1

    goto :goto_2

    .line 1852187
    :pswitch_4
    invoke-static {}, LX/6WP;->newBuilder()LX/6WO;

    move-result-object v1

    .line 1852188
    iput v2, v1, LX/6WO;->b:F

    .line 1852189
    move-object v1, v1

    .line 1852190
    invoke-virtual {v1, v8, v8, p1, p1}, LX/6WO;->a(IIII)LX/6WO;

    move-result-object v1

    invoke-virtual {v1, p1, v8, p1, p1}, LX/6WO;->a(IIII)LX/6WO;

    move-result-object v1

    invoke-virtual {v1, v8, p1, v9, v9}, LX/6WO;->a(IIII)LX/6WO;

    move-result-object v1

    invoke-virtual {v1, v9, p1, v9, v9}, LX/6WO;->a(IIII)LX/6WO;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2, p1, v9, v9}, LX/6WO;->a(IIII)LX/6WO;

    move-result-object v1

    invoke-virtual {v1}, LX/6WO;->a()LX/6WP;

    move-result-object v1

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
