.class public Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeEndCardSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final e:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/1nu;

.field public final b:LX/8yV;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1g8;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1851743
    const-class v0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeEndCardSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeEndCardSpec;->e:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;LX/8yV;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1nu;",
            "LX/8yV;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1g8;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1851744
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1851745
    iput-object p1, p0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeEndCardSpec;->a:LX/1nu;

    .line 1851746
    iput-object p2, p0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeEndCardSpec;->b:LX/8yV;

    .line 1851747
    iput-object p3, p0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeEndCardSpec;->c:LX/0Ot;

    .line 1851748
    iput-object p4, p0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeEndCardSpec;->d:LX/0Ot;

    .line 1851749
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeEndCardSpec;
    .locals 7

    .prologue
    .line 1851750
    const-class v1, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeEndCardSpec;

    monitor-enter v1

    .line 1851751
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeEndCardSpec;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1851752
    sput-object v2, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeEndCardSpec;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1851753
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1851754
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1851755
    new-instance v5, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeEndCardSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v3

    check-cast v3, LX/1nu;

    invoke-static {v0}, LX/8yV;->a(LX/0QB;)LX/8yV;

    move-result-object v4

    check-cast v4, LX/8yV;

    const/16 v6, 0x2eb

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p0, 0xb2d

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v5, v3, v4, v6, p0}, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeEndCardSpec;-><init>(LX/1nu;LX/8yV;LX/0Ot;LX/0Ot;)V

    .line 1851756
    move-object v0, v5

    .line 1851757
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1851758
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomeEndCardSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1851759
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1851760
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
