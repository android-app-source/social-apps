.class public Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderDataPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        "LX/C3z;",
        "LX/1PW;",
        "LX/C3y;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1846666
    const-class v0, Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderDataPartDefinition;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderDataPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1846667
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1846668
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderDataPartDefinition;
    .locals 3

    .prologue
    .line 1846669
    const-class v1, Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderDataPartDefinition;

    monitor-enter v1

    .line 1846670
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderDataPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1846671
    sput-object v2, Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderDataPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1846672
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1846673
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1846674
    new-instance v0, Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderDataPartDefinition;

    invoke-direct {v0}, Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderDataPartDefinition;-><init>()V

    .line 1846675
    move-object v0, v0

    .line 1846676
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1846677
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderDataPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1846678
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1846679
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1846680
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v2, 0x0

    .line 1846681
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v5

    .line 1846682
    new-instance v0, LX/C3z;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    :cond_0
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/C3z;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;Lcom/facebook/graphql/model/GraphQLTextWithEntities;Lcom/facebook/graphql/model/GraphQLImage;Lcom/facebook/graphql/model/GraphQLImage;)V

    return-object v0

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x5b6ea6e6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1846683
    check-cast p2, LX/C3z;

    check-cast p4, LX/C3y;

    .line 1846684
    iget-object v1, p2, LX/C3z;->d:Lcom/facebook/graphql/model/GraphQLImage;

    const/4 v2, 0x0

    sget-object p0, Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderDataPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p4, v1, v2, p0}, LX/C3y;->a(Lcom/facebook/graphql/model/GraphQLImage;ZLcom/facebook/common/callercontext/CallerContext;)V

    .line 1846685
    iget-object v1, p2, LX/C3z;->a:Ljava/lang/String;

    iget-object v2, p2, LX/C3z;->b:Ljava/lang/CharSequence;

    iget-object p0, p2, LX/C3z;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {p4, v1, v2, p0}, LX/C3y;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)V

    .line 1846686
    iget-object v1, p2, LX/C3z;->e:Lcom/facebook/graphql/model/GraphQLImage;

    sget-object v2, Lcom/facebook/feedplugins/facebookvoice/PersonalHighlightHeaderDataPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p4, v1, v2}, LX/C3y;->a(Lcom/facebook/graphql/model/GraphQLImage;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1846687
    const/4 v1, 0x1

    .line 1846688
    iget-object p0, p4, LX/C3y;->l:Landroid/view/View;

    if-eqz v1, :cond_0

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1846689
    const/16 v1, 0x1f

    const v2, -0x142b7589

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1846690
    :cond_0
    const/16 v2, 0x8

    goto :goto_0
.end method
