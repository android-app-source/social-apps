.class public final Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/C2s;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:Lcom/facebook/common/callercontext/CallerContext;

.field public d:Z

.field public e:Z

.field public f:LX/C2l;

.field public final synthetic g:LX/C2s;


# direct methods
.method public constructor <init>(LX/C2s;)V
    .locals 1

    .prologue
    .line 1844704
    iput-object p1, p0, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;->g:LX/C2s;

    .line 1844705
    move-object v0, p1

    .line 1844706
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1844707
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1844680
    const-string v0, "SingleProductImageComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1844681
    if-ne p0, p1, :cond_1

    .line 1844682
    :cond_0
    :goto_0
    return v0

    .line 1844683
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1844684
    goto :goto_0

    .line 1844685
    :cond_3
    check-cast p1, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;

    .line 1844686
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1844687
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1844688
    if-eq v2, v3, :cond_0

    .line 1844689
    iget v2, p0, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;->a:I

    iget v3, p1, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1844690
    goto :goto_0

    .line 1844691
    :cond_4
    iget-object v2, p0, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 1844692
    goto :goto_0

    .line 1844693
    :cond_6
    iget-object v2, p1, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 1844694
    :cond_7
    iget-object v2, p0, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p1, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    .line 1844695
    goto :goto_0

    .line 1844696
    :cond_9
    iget-object v2, p1, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    if-nez v2, :cond_8

    .line 1844697
    :cond_a
    iget-boolean v2, p0, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;->d:Z

    iget-boolean v3, p1, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;->d:Z

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 1844698
    goto :goto_0

    .line 1844699
    :cond_b
    iget-boolean v2, p0, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;->e:Z

    iget-boolean v3, p1, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;->e:Z

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 1844700
    goto :goto_0

    .line 1844701
    :cond_c
    iget-object v2, p0, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;->f:LX/C2l;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;->f:LX/C2l;

    iget-object v3, p1, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;->f:LX/C2l;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1844702
    goto :goto_0

    .line 1844703
    :cond_d
    iget-object v2, p1, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;->f:LX/C2l;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
