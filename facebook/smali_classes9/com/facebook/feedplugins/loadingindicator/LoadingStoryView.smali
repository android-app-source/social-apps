.class public Lcom/facebook/feedplugins/loadingindicator/LoadingStoryView;
.super Landroid/widget/ImageView;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1853478
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1853479
    invoke-direct {p0}, Lcom/facebook/feedplugins/loadingindicator/LoadingStoryView;->a()V

    .line 1853480
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1853486
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1853487
    invoke-direct {p0}, Lcom/facebook/feedplugins/loadingindicator/LoadingStoryView;->a()V

    .line 1853488
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1853483
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1853484
    invoke-direct {p0}, Lcom/facebook/feedplugins/loadingindicator/LoadingStoryView;->a()V

    .line 1853485
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1853481
    new-instance v0, LX/C8x;

    invoke-virtual {p0}, Lcom/facebook/feedplugins/loadingindicator/LoadingStoryView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/C8x;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/loadingindicator/LoadingStoryView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1853482
    return-void
.end method
