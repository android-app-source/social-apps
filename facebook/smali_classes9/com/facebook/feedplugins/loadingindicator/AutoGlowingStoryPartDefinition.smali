.class public Lcom/facebook/feedplugins/loadingindicator/AutoGlowingStoryPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "E::",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<TT;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1853313
    new-instance v0, LX/C8p;

    invoke-direct {v0}, LX/C8p;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/loadingindicator/AutoGlowingStoryPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1853291
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1853292
    iput-object p1, p0, Lcom/facebook/feedplugins/loadingindicator/AutoGlowingStoryPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 1853293
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/loadingindicator/AutoGlowingStoryPartDefinition;
    .locals 4

    .prologue
    .line 1853302
    const-class v1, Lcom/facebook/feedplugins/loadingindicator/AutoGlowingStoryPartDefinition;

    monitor-enter v1

    .line 1853303
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/loadingindicator/AutoGlowingStoryPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1853304
    sput-object v2, Lcom/facebook/feedplugins/loadingindicator/AutoGlowingStoryPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1853305
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1853306
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1853307
    new-instance p0, Lcom/facebook/feedplugins/loadingindicator/AutoGlowingStoryPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/loadingindicator/AutoGlowingStoryPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 1853308
    move-object v0, p0

    .line 1853309
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1853310
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/loadingindicator/AutoGlowingStoryPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1853311
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1853312
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Landroid/content/Context;Landroid/view/ViewGroup;)Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;
    .locals 3
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1853298
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0307b1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;

    .line 1853299
    const/4 v1, 0x1

    .line 1853300
    iput-boolean v1, v0, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;->d:Z

    .line 1853301
    return-object v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1853297
    sget-object v0, Lcom/facebook/feedplugins/loadingindicator/AutoGlowingStoryPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1853295
    iget-object v0, p0, Lcom/facebook/feedplugins/loadingindicator/AutoGlowingStoryPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    sget-object v1, LX/C8w;->a:LX/1X6;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1853296
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 1853294
    const/4 v0, 0x1

    return v0
.end method
