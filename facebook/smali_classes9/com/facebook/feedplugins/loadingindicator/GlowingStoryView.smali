.class public Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/feedplugins/loadingindicator/LoadingStoryView;

.field private b:Landroid/animation/AnimatorSet;

.field private c:LX/3Ii;

.field public d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1853348
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1853349
    invoke-direct {p0}, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;->e()V

    .line 1853350
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1853354
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1853355
    invoke-direct {p0}, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;->e()V

    .line 1853356
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1853351
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1853352
    invoke-direct {p0}, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;->e()V

    .line 1853353
    return-void
.end method

.method private e()V
    .locals 1

    .prologue
    .line 1853333
    const v0, 0x7f0307b0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1853334
    const v0, 0x7f0d147b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/loadingindicator/LoadingStoryView;

    iput-object v0, p0, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;->a:Lcom/facebook/feedplugins/loadingindicator/LoadingStoryView;

    .line 1853335
    invoke-direct {p0}, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;->f()Landroid/animation/AnimatorSet;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;->b:Landroid/animation/AnimatorSet;

    .line 1853336
    return-void
.end method

.method private f()Landroid/animation/AnimatorSet;
    .locals 8

    .prologue
    const-wide/16 v6, 0x1f4

    const/4 v4, 0x2

    .line 1853337
    iget-object v0, p0, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;->a:Lcom/facebook/feedplugins/loadingindicator/LoadingStoryView;

    const-string v1, "alpha"

    new-array v2, v4, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1853338
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1853339
    iget-object v1, p0, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;->a:Lcom/facebook/feedplugins/loadingindicator/LoadingStoryView;

    const-string v2, "alpha"

    new-array v3, v4, [F

    fill-array-data v3, :array_1

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v1, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 1853340
    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1853341
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1853342
    new-array v3, v4, [Landroid/animation/Animator;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v1, v3, v0

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    .line 1853343
    invoke-virtual {v2, v6, v7}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    .line 1853344
    new-instance v0, LX/C8q;

    invoke-direct {v0, p0, v2}, LX/C8q;-><init>(Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;Landroid/animation/AnimatorSet;)V

    iput-object v0, p0, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;->c:LX/3Ii;

    .line 1853345
    return-object v2

    .line 1853346
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3ee66666    # 0.45f
    .end array-data

    .line 1853347
    :array_1
    .array-data 4
        0x3ee66666    # 0.45f
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1853329
    iget-object v0, p0, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;->b:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isStarted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1853330
    iget-object v0, p0, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;->b:Landroid/animation/AnimatorSet;

    iget-object v1, p0, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;->c:LX/3Ii;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1853331
    iget-object v0, p0, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;->b:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 1853332
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1853326
    iget-object v0, p0, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;->b:Landroid/animation/AnimatorSet;

    iget-object v1, p0, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;->c:LX/3Ii;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->removeListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1853327
    iget-object v0, p0, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;->b:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 1853328
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x44c12551

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1853322
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 1853323
    iget-boolean v1, p0, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;->d:Z

    if-eqz v1, :cond_0

    .line 1853324
    invoke-virtual {p0}, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;->a()V

    .line 1853325
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x6c57279b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x334441b0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1853319
    invoke-virtual {p0}, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;->b()V

    .line 1853320
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 1853321
    const/16 v1, 0x2d

    const v2, -0x26f923c0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setAutoPlay(Z)V
    .locals 0

    .prologue
    .line 1853317
    iput-boolean p1, p0, Lcom/facebook/feedplugins/loadingindicator/GlowingStoryView;->d:Z

    .line 1853318
    return-void
.end method
