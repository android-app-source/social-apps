.class public Lcom/facebook/feedplugins/loadingindicator/LoadingCoverPhotoPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Ljava/lang/Float;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
            ">;"
        }
    .end annotation
.end field

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1853360
    new-instance v0, LX/C8r;

    invoke-direct {v0}, LX/C8r;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/loadingindicator/LoadingCoverPhotoPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1853361
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1853362
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/loadingindicator/LoadingCoverPhotoPartDefinition;
    .locals 3

    .prologue
    .line 1853363
    const-class v1, Lcom/facebook/feedplugins/loadingindicator/LoadingCoverPhotoPartDefinition;

    monitor-enter v1

    .line 1853364
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/loadingindicator/LoadingCoverPhotoPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1853365
    sput-object v2, Lcom/facebook/feedplugins/loadingindicator/LoadingCoverPhotoPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1853366
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1853367
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1853368
    new-instance v0, Lcom/facebook/feedplugins/loadingindicator/LoadingCoverPhotoPartDefinition;

    invoke-direct {v0}, Lcom/facebook/feedplugins/loadingindicator/LoadingCoverPhotoPartDefinition;-><init>()V

    .line 1853369
    move-object v0, v0

    .line 1853370
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1853371
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/loadingindicator/LoadingCoverPhotoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1853372
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1853373
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1853374
    sget-object v0, Lcom/facebook/feedplugins/loadingindicator/LoadingCoverPhotoPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x6f6e4a4a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1853375
    check-cast p1, Ljava/lang/Float;

    check-cast p4, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1853376
    new-instance v1, Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    invoke-virtual {p4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p0, 0x7f021af6

    invoke-virtual {v2, p0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const/16 p0, 0x3e8

    invoke-direct {v1, v2, p0}, Lcom/facebook/drawee/drawable/AutoRotateDrawable;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 1853377
    new-instance v2, LX/1Uo;

    invoke-virtual {p4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-direct {v2, p0}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 1853378
    iput-object v1, v2, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 1853379
    move-object v1, v2

    .line 1853380
    invoke-virtual {v1}, LX/1Uo;->u()LX/1af;

    move-result-object v1

    invoke-virtual {p4, v1}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 1853381
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p4, v1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 1853382
    const/16 v1, 0x1f

    const v2, -0x6230f4d2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1853383
    const/4 v0, 0x1

    return v0
.end method
