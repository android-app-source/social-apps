.class public Lcom/facebook/feedplugins/links/ThirdPartyNativeAttachmentPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/C8n;",
        "LX/C8o;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/0Uh;

.field private final b:Lcom/facebook/feedplugins/links/ArticlePrefetchingPartDefinition;

.field public final c:LX/39V;

.field private final d:Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;


# direct methods
.method public constructor <init>(LX/0Uh;Lcom/facebook/feedplugins/links/ArticlePrefetchingPartDefinition;LX/39V;Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1853256
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1853257
    iput-object p1, p0, Lcom/facebook/feedplugins/links/ThirdPartyNativeAttachmentPartDefinition;->a:LX/0Uh;

    .line 1853258
    iput-object p2, p0, Lcom/facebook/feedplugins/links/ThirdPartyNativeAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/links/ArticlePrefetchingPartDefinition;

    .line 1853259
    iput-object p3, p0, Lcom/facebook/feedplugins/links/ThirdPartyNativeAttachmentPartDefinition;->c:LX/39V;

    .line 1853260
    iput-object p4, p0, Lcom/facebook/feedplugins/links/ThirdPartyNativeAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;

    .line 1853261
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/links/ThirdPartyNativeAttachmentPartDefinition;
    .locals 7

    .prologue
    .line 1853262
    const-class v1, Lcom/facebook/feedplugins/links/ThirdPartyNativeAttachmentPartDefinition;

    monitor-enter v1

    .line 1853263
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/links/ThirdPartyNativeAttachmentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1853264
    sput-object v2, Lcom/facebook/feedplugins/links/ThirdPartyNativeAttachmentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1853265
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1853266
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1853267
    new-instance p0, Lcom/facebook/feedplugins/links/ThirdPartyNativeAttachmentPartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v0}, Lcom/facebook/feedplugins/links/ArticlePrefetchingPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/links/ArticlePrefetchingPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/links/ArticlePrefetchingPartDefinition;

    invoke-static {v0}, LX/39V;->a(LX/0QB;)LX/39V;

    move-result-object v5

    check-cast v5, LX/39V;

    invoke-static {v0}, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/links/ThirdPartyNativeAttachmentPartDefinition;-><init>(LX/0Uh;Lcom/facebook/feedplugins/links/ArticlePrefetchingPartDefinition;LX/39V;Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;)V

    .line 1853268
    move-object v0, p0

    .line 1853269
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1853270
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/links/ThirdPartyNativeAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1853271
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1853272
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(LX/C8n;Landroid/view/View;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1853273
    iget-object v0, p0, LX/C8n;->d:LX/2yF;

    invoke-interface {v0, p1}, LX/2yF;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1853274
    check-cast p2, LX/C8n;

    check-cast p3, LX/1Pq;

    .line 1853275
    iget-object v0, p0, Lcom/facebook/feedplugins/links/ThirdPartyNativeAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/links/ArticlePrefetchingPartDefinition;

    iget-object v1, p2, LX/C8n;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1853276
    iget-object v0, p0, Lcom/facebook/feedplugins/links/ThirdPartyNativeAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;

    new-instance v1, LX/2yf;

    iget-object v2, p2, LX/C8n;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/C8n;->e:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, LX/2yf;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Z)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1853277
    new-instance v0, LX/C8m;

    invoke-direct {v0, p0, p2, p3}, LX/C8m;-><init>(Lcom/facebook/feedplugins/links/ThirdPartyNativeAttachmentPartDefinition;LX/C8n;LX/1Pq;)V

    .line 1853278
    new-instance v1, LX/C8o;

    invoke-direct {v1, v0}, LX/C8o;-><init>(Landroid/view/View$OnClickListener;)V

    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x7e51a161

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1853279
    check-cast p1, LX/C8n;

    check-cast p2, LX/C8o;

    .line 1853280
    invoke-static {p1, p4}, Lcom/facebook/feedplugins/links/ThirdPartyNativeAttachmentPartDefinition;->b(LX/C8n;Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    .line 1853281
    if-eqz v1, :cond_0

    .line 1853282
    iget-object v2, p2, LX/C8o;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1853283
    :cond_0
    const/16 v1, 0x1f

    const v2, 0x84278b7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1853284
    check-cast p1, LX/C8n;

    .line 1853285
    invoke-static {p1, p4}, Lcom/facebook/feedplugins/links/ThirdPartyNativeAttachmentPartDefinition;->b(LX/C8n;Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    .line 1853286
    if-eqz v0, :cond_0

    .line 1853287
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1853288
    :cond_0
    return-void
.end method
