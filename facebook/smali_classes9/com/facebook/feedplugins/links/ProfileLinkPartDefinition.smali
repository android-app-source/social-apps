.class public Lcom/facebook/feedplugins/links/ProfileLinkPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/facebook/flatbuffers/Flattenable;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/3V2;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/C8k",
        "<TT;>;",
        "Landroid/view/View$OnClickListener;",
        "LX/1PW;",
        "TV;>;"
    }
.end annotation


# instance fields
.field public final a:LX/AmS;


# direct methods
.method public constructor <init>(LX/AmS;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1853231
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1853232
    iput-object p1, p0, Lcom/facebook/feedplugins/links/ProfileLinkPartDefinition;->a:LX/AmS;

    .line 1853233
    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1853234
    check-cast p2, LX/C8k;

    .line 1853235
    new-instance v0, LX/C8j;

    invoke-direct {v0, p0, p2}, LX/C8j;-><init>(Lcom/facebook/feedplugins/links/ProfileLinkPartDefinition;LX/C8k;)V

    move-object v0, v0

    .line 1853236
    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x198cd5b0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1853237
    check-cast p2, Landroid/view/View$OnClickListener;

    .line 1853238
    check-cast p4, LX/3V2;

    invoke-interface {p4, p2}, LX/3V2;->setProfileImageOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1853239
    const/16 v1, 0x1f

    const v2, -0x6185ec63

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1853240
    check-cast p4, LX/3V2;

    const/4 v0, 0x0

    invoke-interface {p4, v0}, LX/3V2;->setProfileImageOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1853241
    return-void
.end method
