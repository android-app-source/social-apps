.class public final Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveySubmitMutationModels$InlineSurveySubmitMutationFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xc1c6122
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveySubmitMutationModels$InlineSurveySubmitMutationFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveySubmitMutationModels$InlineSurveySubmitMutationFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1850114
    const-class v0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveySubmitMutationModels$InlineSurveySubmitMutationFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1850131
    const-class v0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveySubmitMutationModels$InlineSurveySubmitMutationFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1850132
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1850133
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1850123
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveySubmitMutationModels$InlineSurveySubmitMutationFieldsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveySubmitMutationModels$InlineSurveySubmitMutationFieldsModel;->e:Ljava/lang/String;

    .line 1850124
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveySubmitMutationModels$InlineSurveySubmitMutationFieldsModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1850125
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1850126
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveySubmitMutationModels$InlineSurveySubmitMutationFieldsModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1850127
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1850128
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1850129
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1850130
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1850120
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1850121
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1850122
    return-object p0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1850117
    new-instance v0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveySubmitMutationModels$InlineSurveySubmitMutationFieldsModel;

    invoke-direct {v0}, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveySubmitMutationModels$InlineSurveySubmitMutationFieldsModel;-><init>()V

    .line 1850118
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1850119
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1850116
    const v0, -0x216c94a3

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1850115
    const v0, -0x9a8dede

    return v0
.end method
