.class public Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/components/InlineSurveyGroupComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<+",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/C6r;

.field private final e:LX/C6x;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/C6r;LX/C6x;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1850478
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1850479
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/components/InlineSurveyGroupComponentPartDefinition;->d:LX/C6r;

    .line 1850480
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/components/InlineSurveyGroupComponentPartDefinition;->e:LX/C6x;

    .line 1850481
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "LX/1Pf;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1850462
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/components/InlineSurveyGroupComponentPartDefinition;->d:LX/C6r;

    const/4 v1, 0x0

    .line 1850463
    new-instance v2, LX/C6q;

    invoke-direct {v2, v0}, LX/C6q;-><init>(LX/C6r;)V

    .line 1850464
    sget-object p0, LX/C6r;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/C6p;

    .line 1850465
    if-nez p0, :cond_0

    .line 1850466
    new-instance p0, LX/C6p;

    invoke-direct {p0}, LX/C6p;-><init>()V

    .line 1850467
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/C6p;->a$redex0(LX/C6p;LX/1De;IILX/C6q;)V

    .line 1850468
    move-object v2, p0

    .line 1850469
    move-object v1, v2

    .line 1850470
    move-object v0, v1

    .line 1850471
    iget-object v1, v0, LX/C6p;->a:LX/C6q;

    iput-object p2, v1, LX/C6q;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1850472
    iget-object v1, v0, LX/C6p;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1850473
    move-object v0, v0

    .line 1850474
    iget-object v1, v0, LX/C6p;->a:LX/C6q;

    iput-object p3, v1, LX/C6q;->b:LX/1Pf;

    .line 1850475
    iget-object v1, v0, LX/C6p;->d:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1850476
    move-object v0, v0

    .line 1850477
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/components/InlineSurveyGroupComponentPartDefinition;
    .locals 7

    .prologue
    .line 1850448
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/components/InlineSurveyGroupComponentPartDefinition;

    monitor-enter v1

    .line 1850449
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/components/InlineSurveyGroupComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1850450
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/components/InlineSurveyGroupComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1850451
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1850452
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1850453
    new-instance v6, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/components/InlineSurveyGroupComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/C6r;->a(LX/0QB;)LX/C6r;

    move-result-object v4

    check-cast v4, LX/C6r;

    .line 1850454
    new-instance p0, LX/C6x;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-direct {p0, v5}, LX/C6x;-><init>(LX/0Uh;)V

    .line 1850455
    move-object v5, p0

    .line 1850456
    check-cast v5, LX/C6x;

    invoke-direct {v6, v3, v4, v5}, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/components/InlineSurveyGroupComponentPartDefinition;-><init>(Landroid/content/Context;LX/C6r;LX/C6x;)V

    .line 1850457
    move-object v0, v6

    .line 1850458
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1850459
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/components/InlineSurveyGroupComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1850460
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1850461
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1850447
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/components/InlineSurveyGroupComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1850446
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/components/InlineSurveyGroupComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1850440
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1850441
    invoke-static {p1}, LX/1VY;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1850442
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1850443
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-static {v0}, LX/0x1;->d(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/components/InlineSurveyGroupComponentPartDefinition;->e:LX/C6x;

    .line 1850444
    iget-object v1, v0, LX/C6x;->a:LX/0Uh;

    const/16 p0, 0x3e5

    const/4 p1, 0x0

    invoke-virtual {v1, p0, p1}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 1850445
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1850438
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1850439
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
