.class public Lcom/facebook/feedplugins/graphqlstory/page/GraphQLStoryPageTextPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "LX/1PW;",
        "Landroid/widget/TextView;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/3Do;


# direct methods
.method public constructor <init>(LX/3Do;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1850862
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1850863
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/page/GraphQLStoryPageTextPartDefinition;->a:LX/3Do;

    .line 1850864
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/page/GraphQLStoryPageTextPartDefinition;
    .locals 4

    .prologue
    .line 1850865
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/page/GraphQLStoryPageTextPartDefinition;

    monitor-enter v1

    .line 1850866
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/page/GraphQLStoryPageTextPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1850867
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/page/GraphQLStoryPageTextPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1850868
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1850869
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1850870
    new-instance p0, Lcom/facebook/feedplugins/graphqlstory/page/GraphQLStoryPageTextPartDefinition;

    const-class v3, LX/3Do;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/3Do;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/graphqlstory/page/GraphQLStoryPageTextPartDefinition;-><init>(LX/3Do;)V

    .line 1850871
    move-object v0, p0

    .line 1850872
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1850873
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/page/GraphQLStoryPageTextPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1850874
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1850875
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1850876
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1850877
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/page/GraphQLStoryPageTextPartDefinition;->a:LX/3Do;

    invoke-virtual {v0, p2}, LX/3Do;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/2tm;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x5e8eb112

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1850878
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, Landroid/view/View$OnClickListener;

    check-cast p4, Landroid/widget/TextView;

    .line 1850879
    const/4 v1, 0x3

    invoke-virtual {p4, v1}, Landroid/widget/TextView;->setLines(I)V

    .line 1850880
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1850881
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/1z5;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1850882
    invoke-virtual {p4, p2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1850883
    const/16 v1, 0x1f

    const v2, 0x7ada08f7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1850884
    check-cast p4, Landroid/widget/TextView;

    .line 1850885
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1850886
    return-void
.end method
