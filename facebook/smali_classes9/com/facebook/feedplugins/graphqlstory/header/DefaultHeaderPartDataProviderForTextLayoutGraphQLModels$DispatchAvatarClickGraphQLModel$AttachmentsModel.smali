.class public final Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x60ebb8e7
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel$ActionLinksModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel$TargetModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1713561
    const-class v0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1713560
    const-class v0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1713558
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1713559
    return-void
.end method

.method private j()Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel$TargetModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1713523
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel;->g:Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel$TargetModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel$TargetModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel$TargetModel;

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel;->g:Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel$TargetModel;

    .line 1713524
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel;->g:Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel$TargetModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1713548
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1713549
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1713550
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->c(Ljava/util/List;)I

    move-result v1

    .line 1713551
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel;->j()Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel$TargetModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1713552
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1713553
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1713554
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1713555
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1713556
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1713557
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getActionLinks"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel$ActionLinksModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1713546
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel$ActionLinksModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel;->e:Ljava/util/List;

    .line 1713547
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1713533
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1713534
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1713535
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1713536
    if-eqz v1, :cond_2

    .line 1713537
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel;

    .line 1713538
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 1713539
    :goto_0
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel;->j()Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel$TargetModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1713540
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel;->j()Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel$TargetModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel$TargetModel;

    .line 1713541
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel;->j()Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel$TargetModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1713542
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel;

    .line 1713543
    iput-object v0, v1, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel;->g:Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel$TargetModel;

    .line 1713544
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1713545
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final b()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1713531
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->c(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel;->f:Ljava/util/List;

    .line 1713532
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1713528
    new-instance v0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel;

    invoke-direct {v0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel;-><init>()V

    .line 1713529
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1713530
    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel$TargetModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1713527
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel;->j()Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel$TargetModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1713526
    const v0, 0x6c45c52e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1713525
    const v0, -0x4b900828

    return v0
.end method
