.class public final Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$ProfilePictureModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/6R4;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/1Fb;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2b81edc6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$ProfilePictureModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$ProfilePictureModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1713303
    const-class v0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$ProfilePictureModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1713308
    const-class v0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$ProfilePictureModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1713306
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1713307
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1713304
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1713305
    iget v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$ProfilePictureModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1713283
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1713284
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$ProfilePictureModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1713285
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1713286
    iget v1, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$ProfilePictureModel;->e:I

    invoke-virtual {p1, v2, v1, v2}, LX/186;->a(III)V

    .line 1713287
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1713288
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$ProfilePictureModel;->g:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1713289
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1713290
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1713300
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1713301
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1713302
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1713309
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1713310
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$ProfilePictureModel;->e:I

    .line 1713311
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$ProfilePictureModel;->g:I

    .line 1713312
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1713297
    new-instance v0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$ProfilePictureModel;

    invoke-direct {v0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$ProfilePictureModel;-><init>()V

    .line 1713298
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1713299
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1713295
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$ProfilePictureModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$ProfilePictureModel;->f:Ljava/lang/String;

    .line 1713296
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$ProfilePictureModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 1713293
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1713294
    iget v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$ProfilePictureModel;->g:I

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1713292
    const v0, 0x1fbbbf93

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1713291
    const v0, 0x437b93b

    return v0
.end method
