.class public final Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x18224226
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1713666
    const-class v0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1713667
    const-class v0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1713668
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1713669
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1713670
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1713671
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1713672
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1713673
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;->f:Ljava/util/List;

    .line 1713674
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1713675
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1713676
    if-eqz v0, :cond_0

    .line 1713677
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/util/List;)V

    .line 1713678
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1713679
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1713680
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1713681
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;->c()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1713682
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1713683
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1713684
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1713685
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1713686
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1713687
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1713688
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1713689
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;->e:Ljava/util/List;

    .line 1713690
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1713652
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1713653
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1713654
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1713655
    if-eqz v1, :cond_0

    .line 1713656
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;

    .line 1713657
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;->e:Ljava/util/List;

    .line 1713658
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;->c()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1713659
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;->c()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1713660
    if-eqz v1, :cond_1

    .line 1713661
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;

    .line 1713662
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;->f:Ljava/util/List;

    .line 1713663
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1713664
    if-nez v0, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1713665
    new-instance v0, LX/AoC;

    invoke-direct {v0, p1}, LX/AoC;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1713650
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1713651
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1713647
    const-string v0, "attachments"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1713648
    check-cast p2, Ljava/util/List;

    invoke-direct {p0, p2}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;->a(Ljava/util/List;)V

    .line 1713649
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1713646
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1713643
    new-instance v0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;

    invoke-direct {v0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;-><init>()V

    .line 1713644
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1713645
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1713641
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;->g:Ljava/lang/String;

    .line 1713642
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAttachments"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1713639
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;->f:Ljava/util/List;

    .line 1713640
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1713637
    const v0, -0x79ea36c7

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1713638
    const v0, 0x4c808d5

    return v0
.end method
