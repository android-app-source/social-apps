.class public final Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/1yE;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/36O;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5b640a20
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1713392
    const-class v0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1713391
    const-class v0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1713389
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1713390
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 4

    .prologue
    .line 1713382
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->g:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1713383
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1713384
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1713385
    if-eqz v0, :cond_0

    .line 1713386
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x2

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1713387
    :cond_0
    return-void

    .line 1713388
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1713380
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->f:Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->f:Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    .line 1713381
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->f:Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    return-object v0
.end method

.method private k()Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$ProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1713378
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->j:Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$ProfilePictureModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$ProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$ProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->j:Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$ProfilePictureModel;

    .line 1713379
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->j:Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$ProfilePictureModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 1713360
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1713361
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->c()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1713362
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->j()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1713363
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->d()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1713364
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1713365
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->v_()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1713366
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->k()Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$ProfilePictureModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1713367
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1713368
    const/4 v7, 0x7

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1713369
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 1713370
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1713371
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1713372
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1713373
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1713374
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1713375
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1713376
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1713377
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1713347
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1713348
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->j()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1713349
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->j()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    .line 1713350
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->j()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1713351
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;

    .line 1713352
    iput-object v0, v1, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->f:Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    .line 1713353
    :cond_0
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->k()Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1713354
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->k()Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$ProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$ProfilePictureModel;

    .line 1713355
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->k()Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$ProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1713356
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;

    .line 1713357
    iput-object v0, v1, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->j:Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$ProfilePictureModel;

    .line 1713358
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1713359
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1713346
    new-instance v0, LX/AoA;

    invoke-direct {v0, p1}, LX/AoA;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1713345
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1713339
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1713340
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->d()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1713341
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1713342
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    .line 1713343
    :goto_0
    return-void

    .line 1713344
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1713393
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1713394
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {p0, p2}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 1713395
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1713320
    new-instance v0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;

    invoke-direct {v0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;-><init>()V

    .line 1713321
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1713322
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1713323
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->k:Ljava/lang/String;

    .line 1713324
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1713325
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1713326
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1713327
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final synthetic cp_()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1713328
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->k()Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$ProfilePictureModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1713329
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->g:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->g:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1713330
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->g:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1713331
    const v0, 0x2a10b1c0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1713332
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->h:Ljava/lang/String;

    .line 1713333
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1713334
    const v0, 0x3c2b9d5

    return v0
.end method

.method public final synthetic p()LX/6R4;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1713335
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->k()Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel$ProfilePictureModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic q()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1713336
    invoke-direct {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->j()Lcom/facebook/directinstall/util/DirectInstallApplicationUtilsGraphQLModels$GetNativeAppDetailsAppStoreApplicationGraphQLModel;

    move-result-object v0

    return-object v0
.end method

.method public final v_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1713337
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->i:Ljava/lang/String;

    .line 1713338
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->i:Ljava/lang/String;

    return-object v0
.end method
