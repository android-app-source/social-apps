.class public final Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1713604
    const-class v0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;

    new-instance v1, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1713605
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1713603
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1713562
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1713563
    const/4 v2, 0x0

    .line 1713564
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_8

    .line 1713565
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1713566
    :goto_0
    move v1, v2

    .line 1713567
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1713568
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1713569
    new-instance v1, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;

    invoke-direct {v1}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;-><init>()V

    .line 1713570
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1713571
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1713572
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1713573
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1713574
    :cond_0
    return-object v1

    .line 1713575
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1713576
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, p0, :cond_7

    .line 1713577
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1713578
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1713579
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v5, :cond_2

    .line 1713580
    const-string p0, "actors"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1713581
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1713582
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, p0, :cond_3

    .line 1713583
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, p0, :cond_3

    .line 1713584
    invoke-static {p1, v0}, LX/AoF;->b(LX/15w;LX/186;)I

    move-result v5

    .line 1713585
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1713586
    :cond_3
    invoke-static {v4, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v4

    move v4, v4

    .line 1713587
    goto :goto_1

    .line 1713588
    :cond_4
    const-string p0, "attachments"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1713589
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1713590
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, p0, :cond_5

    .line 1713591
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, p0, :cond_5

    .line 1713592
    invoke-static {p1, v0}, LX/AoI;->b(LX/15w;LX/186;)I

    move-result v5

    .line 1713593
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1713594
    :cond_5
    invoke-static {v3, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 1713595
    goto :goto_1

    .line 1713596
    :cond_6
    const-string p0, "cache_id"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1713597
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto/16 :goto_1

    .line 1713598
    :cond_7
    const/4 v5, 0x3

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1713599
    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1713600
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1713601
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1713602
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_8
    move v1, v2

    move v3, v2

    move v4, v2

    goto/16 :goto_1
.end method
