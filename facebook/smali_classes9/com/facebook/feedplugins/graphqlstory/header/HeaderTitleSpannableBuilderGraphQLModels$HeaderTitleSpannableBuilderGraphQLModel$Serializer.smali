.class public final Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1714031
    const-class v0, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel;

    new-instance v1, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1714032
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1714033
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1714034
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1714035
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1714036
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1714037
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1714038
    if-eqz v2, :cond_1

    .line 1714039
    const-string v3, "actors"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1714040
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1714041
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_0

    .line 1714042
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1, p2}, LX/AoR;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1714043
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1714044
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1714045
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1714046
    if-eqz v2, :cond_2

    .line 1714047
    const-string v3, "cache_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1714048
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1714049
    :cond_2
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1714050
    if-eqz v2, :cond_3

    .line 1714051
    const-string v3, "to"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1714052
    invoke-static {v1, v2, p1, p2}, LX/AoU;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1714053
    :cond_3
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1714054
    if-eqz v2, :cond_4

    .line 1714055
    const-string v3, "tracking"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1714056
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1714057
    :cond_4
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1714058
    if-eqz v2, :cond_5

    .line 1714059
    const-string v3, "via"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1714060
    invoke-static {v1, v2, p1, p2}, LX/AoV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1714061
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1714062
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1714063
    check-cast p1, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$Serializer;->a(Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel;LX/0nX;LX/0my;)V

    return-void
.end method
