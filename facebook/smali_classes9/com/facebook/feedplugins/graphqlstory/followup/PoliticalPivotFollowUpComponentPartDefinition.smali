.class public Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotFollowUpComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final d:LX/1eA;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C5V;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/C5c;

.field private final g:Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotTooltipPartDefinition;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/C5c;Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotTooltipPartDefinition;LX/1eA;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/C5V;",
            ">;",
            "LX/C5c;",
            "Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotTooltipPartDefinition;",
            "LX/1eA;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1848352
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1848353
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotFollowUpComponentPartDefinition;->e:LX/0Ot;

    .line 1848354
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotFollowUpComponentPartDefinition;->f:LX/C5c;

    .line 1848355
    iput-object p4, p0, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotFollowUpComponentPartDefinition;->g:Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotTooltipPartDefinition;

    .line 1848356
    iput-object p5, p0, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotFollowUpComponentPartDefinition;->d:LX/1eA;

    .line 1848357
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1848358
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotFollowUpComponentPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C5V;

    const/4 v1, 0x0

    .line 1848359
    new-instance v2, LX/C5U;

    invoke-direct {v2, v0}, LX/C5U;-><init>(LX/C5V;)V

    .line 1848360
    iget-object p0, v0, LX/C5V;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/C5T;

    .line 1848361
    if-nez p0, :cond_0

    .line 1848362
    new-instance p0, LX/C5T;

    invoke-direct {p0, v0}, LX/C5T;-><init>(LX/C5V;)V

    .line 1848363
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/C5T;->a$redex0(LX/C5T;LX/1De;IILX/C5U;)V

    .line 1848364
    move-object v2, p0

    .line 1848365
    move-object v1, v2

    .line 1848366
    move-object v0, v1

    .line 1848367
    check-cast p3, LX/1Pq;

    .line 1848368
    iget-object v1, v0, LX/C5T;->a:LX/C5U;

    iput-object p3, v1, LX/C5U;->a:LX/1Pq;

    .line 1848369
    iget-object v1, v0, LX/C5T;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1848370
    move-object v0, v0

    .line 1848371
    iget-object v1, v0, LX/C5T;->a:LX/C5U;

    iput-object p2, v1, LX/C5U;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1848372
    iget-object v1, v0, LX/C5T;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1848373
    move-object v0, v0

    .line 1848374
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1dV;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1dV;"
        }
    .end annotation

    .prologue
    .line 1848375
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotFollowUpComponentPartDefinition;->f:LX/C5c;

    .line 1848376
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1848377
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v4, 0x0

    .line 1848378
    sget-object v2, LX/C5c;->a:Ljava/util/Set;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1848379
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(LX/1aD;Ljava/lang/Object;LX/1Pn;)LX/1dV;

    move-result-object v0

    return-object v0

    .line 1848380
    :cond_0
    sget-object v2, LX/C5b;->RENDER:LX/C5b;

    invoke-static {v1, v2, v0, v4, v4}, LX/C5c;->a(LX/C5c;LX/C5b;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLPage;Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;)V

    .line 1848381
    sget-object v2, LX/C5c;->a:Ljava/util/Set;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotFollowUpComponentPartDefinition;
    .locals 9

    .prologue
    .line 1848383
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotFollowUpComponentPartDefinition;

    monitor-enter v1

    .line 1848384
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotFollowUpComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1848385
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotFollowUpComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1848386
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1848387
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1848388
    new-instance v3, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotFollowUpComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    const/16 v5, 0x1f6b

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/C5c;->b(LX/0QB;)LX/C5c;

    move-result-object v6

    check-cast v6, LX/C5c;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotTooltipPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotTooltipPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotTooltipPartDefinition;

    invoke-static {v0}, LX/1eA;->a(LX/0QB;)LX/1eA;

    move-result-object v8

    check-cast v8, LX/1eA;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotFollowUpComponentPartDefinition;-><init>(Landroid/content/Context;LX/0Ot;LX/C5c;Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotTooltipPartDefinition;LX/1eA;)V

    .line 1848389
    move-object v0, v3

    .line 1848390
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1848391
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotFollowUpComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1848392
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1848393
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1848382
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotFollowUpComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1848351
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotFollowUpComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1Pn;)LX/1dV;
    .locals 1

    .prologue
    .line 1848334
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotFollowUpComponentPartDefinition;->a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1dV;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1848350
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotFollowUpComponentPartDefinition;->a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1dV;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1848346
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1848347
    invoke-super {p0, p1, p2}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(LX/1aD;Ljava/lang/Object;)V

    .line 1848348
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotFollowUpComponentPartDefinition;->g:Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotTooltipPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1848349
    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1848337
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1848338
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1848339
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1848340
    invoke-static {v0}, LX/1Wr;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object p1

    instance-of p1, p1, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;

    move v0, p1

    .line 1848341
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotFollowUpComponentPartDefinition;->d:LX/1eA;

    .line 1848342
    iget-object v1, v0, LX/1eA;->b:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 1848343
    iget-object v1, v0, LX/1eA;->a:LX/0ad;

    sget-short p0, LX/C5Z;->d:S

    const/4 p1, 0x0

    invoke-interface {v1, p0, p1}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, LX/1eA;->b:Ljava/lang/Boolean;

    .line 1848344
    :cond_0
    iget-object v1, v0, LX/1eA;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v0, v1

    .line 1848345
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1848335
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1848336
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
