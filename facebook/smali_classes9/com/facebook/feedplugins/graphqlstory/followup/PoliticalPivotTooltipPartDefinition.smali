.class public Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotTooltipPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/1dh;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "LX/C58;",
        "LX/1PW;",
        "TV;>;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/1eA;

.field public final c:LX/0iA;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0iA;LX/1eA;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1848402
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1848403
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotTooltipPartDefinition;->a:Landroid/content/Context;

    .line 1848404
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotTooltipPartDefinition;->c:LX/0iA;

    .line 1848405
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotTooltipPartDefinition;->b:LX/1eA;

    .line 1848406
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotTooltipPartDefinition;
    .locals 6

    .prologue
    .line 1848407
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotTooltipPartDefinition;

    monitor-enter v1

    .line 1848408
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotTooltipPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1848409
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotTooltipPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1848410
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1848411
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1848412
    new-instance p0, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotTooltipPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v4

    check-cast v4, LX/0iA;

    invoke-static {v0}, LX/1eA;->a(LX/0QB;)LX/1eA;

    move-result-object v5

    check-cast v5, LX/1eA;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotTooltipPartDefinition;-><init>(Landroid/content/Context;LX/0iA;LX/1eA;)V

    .line 1848413
    move-object v0, p0

    .line 1848414
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1848415
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotTooltipPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1848416
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1848417
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1848418
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1848419
    const/4 v1, 0x0

    .line 1848420
    if-eqz p2, :cond_0

    .line 1848421
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1848422
    if-eqz v0, :cond_0

    .line 1848423
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1848424
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1Wr;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 1848425
    :goto_0
    move-object v0, v0

    .line 1848426
    new-instance v1, LX/C58;

    invoke-direct {v1, v0}, LX/C58;-><init>(LX/0hs;)V

    return-object v1

    .line 1848427
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotTooltipPartDefinition;->c:LX/0iA;

    sget-object p1, LX/C59;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class p3, LX/C59;

    invoke-virtual {v0, p1, p3}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    .line 1848428
    if-nez v0, :cond_2

    move-object v0, v1

    .line 1848429
    goto :goto_0

    .line 1848430
    :cond_2
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotTooltipPartDefinition;->c:LX/0iA;

    invoke-virtual {v1}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v1

    invoke-interface {v0}, LX/0i1;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 1848431
    new-instance v0, LX/0hs;

    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotTooltipPartDefinition;->a:Landroid/content/Context;

    const/4 p1, 0x2

    invoke-direct {v0, v1, p1}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 1848432
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotTooltipPartDefinition;->b:LX/1eA;

    iget-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotTooltipPartDefinition;->a:Landroid/content/Context;

    const p3, 0x7f08298f

    invoke-virtual {p1, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 1848433
    iget-object p3, v1, LX/1eA;->h:Ljava/lang/String;

    if-nez p3, :cond_3

    .line 1848434
    iget-object p3, v1, LX/1eA;->a:LX/0ad;

    sget-char p0, LX/C5Z;->g:C

    invoke-interface {p3, p0, p1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object p3

    iput-object p3, v1, LX/1eA;->h:Ljava/lang/String;

    .line 1848435
    :cond_3
    iget-object p3, v1, LX/1eA;->h:Ljava/lang/String;

    move-object v1, p3

    .line 1848436
    invoke-virtual {v0, v1}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 1848437
    const/4 v1, -0x1

    .line 1848438
    iput v1, v0, LX/0hs;->t:I

    .line 1848439
    sget-object v1, LX/3AV;->ABOVE:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 1848440
    move-object v0, v0

    .line 1848441
    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x74d5bb78

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1848442
    check-cast p2, LX/C58;

    .line 1848443
    iget-object v1, p2, LX/C58;->a:LX/0hs;

    if-eqz v1, :cond_0

    .line 1848444
    new-instance v1, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotTooltipPartDefinition$1;

    invoke-direct {v1, p0, p4, p2}, Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotTooltipPartDefinition$1;-><init>(Lcom/facebook/feedplugins/graphqlstory/followup/PoliticalPivotTooltipPartDefinition;Landroid/view/View;LX/C58;)V

    invoke-virtual {p4, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 1848445
    :cond_0
    const/16 v1, 0x1f

    const v2, -0x24522d30

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
