.class public Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field public static final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static k:LX/0Xm;


# instance fields
.field private final c:LX/1nu;

.field public final d:LX/3iV;

.field public final e:LX/1eA;

.field public final f:LX/1vg;

.field private final g:LX/1nG;

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/1zL;

.field public final j:LX/C5c;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1849055
    const-class v0, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1849056
    new-instance v0, LX/C5W;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LX/C5W;-><init>(I)V

    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;->b:Landroid/util/SparseArray;

    return-void
.end method

.method public constructor <init>(LX/1nu;LX/1nG;LX/0Ot;LX/1zL;LX/3iV;LX/1vg;LX/C5c;LX/1eA;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1nu;",
            "LX/1nG;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "Lcom/facebook/ufiservices/util/LinkifySpanFactory;",
            "LX/3iV;",
            "LX/1vg;",
            "LX/C5c;",
            "LX/1eA;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1849057
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1849058
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;->c:LX/1nu;

    .line 1849059
    iput-object p5, p0, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;->d:LX/3iV;

    .line 1849060
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;->h:LX/0Ot;

    .line 1849061
    iput-object p6, p0, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;->f:LX/1vg;

    .line 1849062
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;->g:LX/1nG;

    .line 1849063
    iput-object p4, p0, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;->i:LX/1zL;

    .line 1849064
    iput-object p7, p0, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;->j:LX/C5c;

    .line 1849065
    iput-object p8, p0, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;->e:LX/1eA;

    .line 1849066
    return-void
.end method

.method public static a(Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;LX/1De;Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/1Dg;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;",
            "TE;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
            ")",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1849067
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;->l()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v7

    .line 1849068
    if-eqz v7, :cond_0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1849069
    :cond_0
    const/4 v0, 0x0

    .line 1849070
    :goto_0
    return-object v0

    .line 1849071
    :cond_1
    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    .line 1849072
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 1849073
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    move-object v6, v0

    .line 1849074
    :goto_1
    new-instance v3, LX/C5Y;

    invoke-direct {v3, p2}, LX/C5Y;-><init>(Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;)V

    move-object v0, p3

    .line 1849075
    check-cast v0, LX/1Pr;

    .line 1849076
    iget-object v1, p4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1849077
    check-cast v1, LX/0jW;

    invoke-interface {v0, v3, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    .line 1849078
    if-eqz v8, :cond_3

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;->k()Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move-object v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;->a(Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;LX/1De;Ljava/lang/String;LX/C5Y;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/CharSequence;

    move-result-object v0

    move-object v2, v0

    .line 1849079
    :goto_2
    if-eqz p5, :cond_2

    invoke-virtual {p5}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1849080
    :cond_2
    const v0, 0x7f082990

    invoke-virtual {p1, v0}, LX/1De;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1849081
    :goto_3
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v3, 0x2

    invoke-interface {v1, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/4 v3, 0x2

    invoke-interface {v1, v3}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    const v3, 0x7f0a00d5

    invoke-interface {v1, v3}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v1

    iget-object v3, p0, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;->c:LX/1nu;

    invoke-virtual {v3, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v3

    invoke-virtual {v3, v6}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v3

    sget-object v4, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v4}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v3, v4}, LX/1Di;->b(I)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b1ccf

    invoke-interface {v3, v4}, LX/1Di;->i(I)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b1ccf

    invoke-interface {v3, v4}, LX/1Di;->q(I)LX/1Di;

    move-result-object v3

    const/4 v4, 0x6

    const v5, 0x7f0b1cd0

    invoke-interface {v3, v4, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    const/4 v4, 0x1

    const v5, 0x7f0b1cd1

    invoke-interface {v3, v4, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    invoke-static {p1, v7, p2}, LX/C5V;->a(LX/1De;Lcom/facebook/graphql/model/GraphQLPage;Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;)LX/1dQ;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v3

    invoke-interface {v1, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v4, 0x0

    invoke-interface {v1, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-interface {v1, v4}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v5, 0x2

    invoke-interface {v1, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    const v6, 0x7f0b0050

    invoke-virtual {v5, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    const v6, 0x7f0a00ab

    invoke-virtual {v5, v6}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, LX/1ne;->t(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const/4 v6, 0x5

    const v9, 0x7f0b1cd2

    invoke-interface {v5, v6, v9}, LX/1Di;->g(II)LX/1Di;

    move-result-object v5

    invoke-static {p1, v7, p2}, LX/C5V;->a(LX/1De;Lcom/facebook/graphql/model/GraphQLPage;Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;)LX/1dQ;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v5

    invoke-interface {v1, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPage;->I()Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x0

    :goto_4
    invoke-interface {v5, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v4, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const v2, 0x7f0b0050

    invoke-virtual {v1, v2}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    const v2, 0x7f0a00a8

    invoke-virtual {v1, v2}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 v2, 0x5

    const v5, 0x7f0b1cd3

    invoke-interface {v1, v2, v5}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const/4 v5, 0x3

    if-eqz v8, :cond_6

    const v1, 0x7f0b1cd3

    :goto_5
    invoke-interface {v2, v5, v1}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    invoke-interface {v4, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    if-eqz v8, :cond_7

    const/4 v0, 0x0

    :goto_6
    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v3, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    goto/16 :goto_0

    .line 1849082
    :cond_3
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;->k()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    goto/16 :goto_2

    .line 1849083
    :cond_4
    invoke-virtual {p5}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 1849084
    :cond_5
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v1

    const v6, 0x7f021a25

    invoke-virtual {v1, v6}, LX/1o5;->h(I)LX/1o5;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    goto :goto_4

    :cond_6
    const v1, 0x7f0b1cd4

    goto :goto_5

    :cond_7
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    const v2, 0x7f0b004e

    invoke-virtual {v0, v2}, LX/1ne;->q(I)LX/1ne;

    move-result-object v0

    const v2, 0x7f0a00e6

    invoke-virtual {v0, v2}, LX/1ne;->n(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const/4 v2, 0x3

    const v4, 0x7f0b1cd3

    invoke-interface {v0, v2, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v0

    .line 1849085
    const v2, 0x46a57ddc

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v7, v4, v5

    const/4 v5, 0x1

    aput-object p2, v4, v5

    invoke-static {p1, v2, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v2

    move-object v2, v2

    .line 1849086
    invoke-interface {v0, v2}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v0

    goto :goto_6

    :cond_8
    move-object v6, v0

    goto/16 :goto_1
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;
    .locals 12

    .prologue
    .line 1849087
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;

    monitor-enter v1

    .line 1849088
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1849089
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1849090
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1849091
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1849092
    new-instance v3, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v4

    check-cast v4, LX/1nu;

    invoke-static {v0}, LX/1nG;->a(LX/0QB;)LX/1nG;

    move-result-object v5

    check-cast v5, LX/1nG;

    const/16 v6, 0xbc6

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/1zL;->a(LX/0QB;)LX/1zL;

    move-result-object v7

    check-cast v7, LX/1zL;

    invoke-static {v0}, LX/3iV;->a(LX/0QB;)LX/3iV;

    move-result-object v8

    check-cast v8, LX/3iV;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v9

    check-cast v9, LX/1vg;

    invoke-static {v0}, LX/C5c;->b(LX/0QB;)LX/C5c;

    move-result-object v10

    check-cast v10, LX/C5c;

    invoke-static {v0}, LX/1eA;->a(LX/0QB;)LX/1eA;

    move-result-object v11

    check-cast v11, LX/1eA;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;-><init>(LX/1nu;LX/1nG;LX/0Ot;LX/1zL;LX/3iV;LX/1vg;LX/C5c;LX/1eA;)V

    .line 1849093
    move-object v0, v3

    .line 1849094
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1849095
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1849096
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1849097
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;LX/1De;Ljava/lang/String;LX/C5Y;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/CharSequence;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Ljava/lang/String;",
            "LX/C5Y;",
            "TE;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .prologue
    const/16 v5, 0x46

    const/4 v4, 0x0

    .line 1849098
    const v0, 0x7f08298e

    invoke-virtual {p1, v0}, LX/1De;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v0

    .line 1849099
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;->i:LX/1zL;

    .line 1849100
    new-instance v2, LX/C5X;

    invoke-direct {v2, p0, p4, p3, p5}, LX/C5X;-><init>(Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;LX/1Pq;LX/C5Y;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    move-object v2, v2

    .line 1849101
    invoke-virtual {v1, v2}, LX/1zL;->a(Landroid/view/View$OnClickListener;)Landroid/text/style/CharacterStyle;

    move-result-object v1

    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v2

    const/16 v3, 0x21

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1849102
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v5, :cond_0

    .line 1849103
    invoke-virtual {p2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f080023

    invoke-virtual {p1, v2}, LX/1De;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 1849104
    :cond_0
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/CharSequence;

    aput-object p2, v1, v4

    const/4 v2, 0x1

    const-string v3, " "

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object v0, v1, v2

    invoke-static {v1}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method
