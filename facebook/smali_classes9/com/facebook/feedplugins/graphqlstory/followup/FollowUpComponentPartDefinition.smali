.class public Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pk;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final d:LX/1eA;

.field private final e:LX/C5R;

.field private final f:LX/1LV;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1eA;LX/C5R;LX/1LV;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1848280
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1848281
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpComponentPartDefinition;->d:LX/1eA;

    .line 1848282
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpComponentPartDefinition;->e:LX/C5R;

    .line 1848283
    iput-object p4, p0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpComponentPartDefinition;->f:LX/1LV;

    .line 1848284
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1848263
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpComponentPartDefinition;->e:LX/C5R;

    const/4 v1, 0x0

    .line 1848264
    new-instance v2, LX/C5Q;

    invoke-direct {v2, v0}, LX/C5Q;-><init>(LX/C5R;)V

    .line 1848265
    iget-object p0, v0, LX/C5R;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/C5P;

    .line 1848266
    if-nez p0, :cond_0

    .line 1848267
    new-instance p0, LX/C5P;

    invoke-direct {p0, v0}, LX/C5P;-><init>(LX/C5R;)V

    .line 1848268
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/C5P;->a$redex0(LX/C5P;LX/1De;IILX/C5Q;)V

    .line 1848269
    move-object v2, p0

    .line 1848270
    move-object v1, v2

    .line 1848271
    move-object v0, v1

    .line 1848272
    iget-object v1, v0, LX/C5P;->a:LX/C5Q;

    iput-object p2, v1, LX/C5Q;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1848273
    iget-object v1, v0, LX/C5P;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1848274
    move-object v0, v0

    .line 1848275
    check-cast p3, LX/1Pq;

    .line 1848276
    iget-object v1, v0, LX/C5P;->a:LX/C5Q;

    iput-object p3, v1, LX/C5Q;->a:LX/1Pq;

    .line 1848277
    iget-object v1, v0, LX/C5P;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1848278
    move-object v0, v0

    .line 1848279
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpComponentPartDefinition;
    .locals 7

    .prologue
    .line 1848252
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpComponentPartDefinition;

    monitor-enter v1

    .line 1848253
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1848254
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1848255
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1848256
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1848257
    new-instance p0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1eA;->a(LX/0QB;)LX/1eA;

    move-result-object v4

    check-cast v4, LX/1eA;

    invoke-static {v0}, LX/C5R;->a(LX/0QB;)LX/C5R;

    move-result-object v5

    check-cast v5, LX/C5R;

    invoke-static {v0}, LX/1LV;->a(LX/0QB;)LX/1LV;

    move-result-object v6

    check-cast v6, LX/1LV;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpComponentPartDefinition;-><init>(Landroid/content/Context;LX/1eA;LX/C5R;LX/1LV;)V

    .line 1848258
    move-object v0, p0

    .line 1848259
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1848260
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1848261
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1848262
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1dV;",
            "TE;",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1848247
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(Ljava/lang/Object;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V

    .line 1848248
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1848249
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1Wr;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    .line 1848250
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpComponentPartDefinition;->f:LX/1LV;

    invoke-interface {v0}, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;->I_()I

    move-result v2

    invoke-virtual {v1, v0, v2}, LX/1LV;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    .line 1848251
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1848246
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1848221
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V
    .locals 0

    .prologue
    .line 1848245
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x672c3e47

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1848244
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/1dV;

    check-cast p3, LX/1Pn;

    check-cast p4, Lcom/facebook/components/feed/FeedComponentView;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V

    const/16 v1, 0x1f

    const v2, 0x41681797

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    .line 1848224
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x1

    .line 1848225
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1848226
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1Wr;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 1848227
    instance-of v2, v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpComponentPartDefinition;->d:LX/1eA;

    .line 1848228
    iget-object v3, v2, LX/1eA;->c:Ljava/lang/Boolean;

    if-nez v3, :cond_0

    .line 1848229
    iget-object v3, v2, LX/1eA;->a:LX/0ad;

    sget-short v4, LX/C5Z;->b:S

    const/4 p1, 0x0

    invoke-interface {v3, v4, p1}, LX/0ad;->a(SZ)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v2, LX/1eA;->c:Ljava/lang/Boolean;

    .line 1848230
    :cond_0
    iget-object v3, v2, LX/1eA;->c:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    move v2, v3

    .line 1848231
    if-eqz v2, :cond_1

    move v0, v1

    .line 1848232
    :goto_0
    return v0

    .line 1848233
    :cond_1
    instance-of v2, v0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpComponentPartDefinition;->d:LX/1eA;

    invoke-virtual {v2}, LX/1eA;->f()Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 1848234
    goto :goto_0

    .line 1848235
    :cond_2
    instance-of v2, v0, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/followup/FollowUpComponentPartDefinition;->d:LX/1eA;

    .line 1848236
    iget-object v3, v2, LX/1eA;->e:Ljava/lang/Boolean;

    if-nez v3, :cond_3

    .line 1848237
    iget-object v3, v2, LX/1eA;->a:LX/0ad;

    sget-short v4, LX/C5Z;->c:S

    const/4 p0, 0x0

    invoke-interface {v3, v4, p0}, LX/0ad;->a(SZ)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v2, LX/1eA;->e:Ljava/lang/Boolean;

    .line 1848238
    :cond_3
    iget-object v3, v2, LX/1eA;->e:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    move v2, v3

    .line 1848239
    if-eqz v2, :cond_4

    move v0, v1

    .line 1848240
    goto :goto_0

    .line 1848241
    :cond_4
    instance-of v2, v0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    if-nez v2, :cond_5

    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;

    if-eqz v0, :cond_6

    :cond_5
    move v0, v1

    .line 1848242
    goto :goto_0

    .line 1848243
    :cond_6
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1848222
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1848223
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
