.class public Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field public static final c:Ljava/lang/String;

.field private static f:LX/0Xm;


# instance fields
.field public final d:LX/36i;

.field public final e:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1849750
    const-class v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponentSpec;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 1849751
    const-class v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponentSpec;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponentSpec;->c:Ljava/lang/String;

    .line 1849752
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponentSpec;->b:Lcom/facebook/common/callercontext/CallerContext;

    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/36i;LX/03V;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1849753
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1849754
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponentSpec;->d:LX/36i;

    .line 1849755
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponentSpec;->e:LX/03V;

    .line 1849756
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponentSpec;
    .locals 5

    .prologue
    .line 1849757
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponentSpec;

    monitor-enter v1

    .line 1849758
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponentSpec;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1849759
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponentSpec;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1849760
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1849761
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1849762
    new-instance p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponentSpec;

    invoke-static {v0}, LX/36i;->a(LX/0QB;)LX/36i;

    move-result-object v3

    check-cast v3, LX/36i;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponentSpec;-><init>(LX/36i;LX/03V;)V

    .line 1849763
    move-object v0, p0

    .line 1849764
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1849765
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1849766
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1849767
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;Lcom/facebook/common/callercontext/CallerContext;Z)LX/1Dg;
    .locals 9
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1Pn;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;TE;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Z)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1849768
    iget-object v6, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponentSpec;->d:LX/36i;

    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponentSpec;->d:LX/36i;

    .line 1849769
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1849770
    check-cast v1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1849771
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3

    .line 1849772
    :cond_0
    const/4 v1, 0x0

    .line 1849773
    :goto_0
    move-object v4, v1

    .line 1849774
    const/16 v5, 0x3c

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, LX/36i;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;Ljava/lang/CharSequence;I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v6, p1, v0}, LX/36i;->a(Landroid/content/Context;Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v6

    .line 1849775
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x4

    if-eqz p5, :cond_1

    const v0, 0x7f0b1056

    :goto_1
    invoke-interface {v1, v2, v0}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v8

    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponentSpec;->d:LX/36i;

    const v4, 0x7f0b08f9

    const/4 v5, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    invoke-virtual/range {v0 .. v5}, LX/36i;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/common/callercontext/CallerContext;II)LX/1Di;

    move-result-object v0

    invoke-interface {v8, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {v0, v2}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v2

    if-nez v6, :cond_2

    const/4 v0, 0x0

    :goto_2
    invoke-interface {v2, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v7, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    .line 1849776
    const v1, -0x315d436

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v1

    move-object v1, v1

    .line 1849777
    invoke-interface {v0, v1}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    invoke-static {p1, v6}, LX/36i;->a(LX/1De;Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-interface {v0, v3}, LX/1Di;->a(F)LX/1Di;

    move-result-object v0

    const/4 v3, 0x1

    const/4 v4, -0x1

    invoke-interface {v0, v3, v4}, LX/1Di;->d(II)LX/1Di;

    move-result-object v0

    goto :goto_2

    .line 1849778
    :cond_3
    invoke-static {v1}, LX/36l;->a(Lcom/facebook/graphql/model/GraphQLComment;)Ljava/lang/String;

    move-result-object v3

    .line 1849779
    if-nez v3, :cond_6

    .line 1849780
    iget-object v3, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponentSpec;->e:LX/03V;

    sget-object v4, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponentSpec;->c:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v2, "null comment author, id: "

    invoke-direct {v5, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_5

    :cond_4
    const-string v2, "null"

    :goto_3
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1849781
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 1849782
    :cond_5
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 1849783
    :cond_6
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1849784
    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1849785
    new-instance v4, Landroid/text/style/StyleSpan;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/4 v5, 0x0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v7, 0x11

    invoke-virtual {v2, v4, v5, v3, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1849786
    const-string v3, " "

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1849787
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-object v1, v2

    .line 1849788
    goto/16 :goto_0
.end method
