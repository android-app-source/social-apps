.class public final Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/21g;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:Lcom/facebook/common/callercontext/CallerContext;

.field public d:Z

.field public e:LX/1EO;

.field public final synthetic f:LX/21g;


# direct methods
.method public constructor <init>(LX/21g;)V
    .locals 1

    .prologue
    .line 1849722
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;->f:LX/21g;

    .line 1849723
    move-object v0, p1

    .line 1849724
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1849725
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    iput-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 1849726
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1849727
    const-string v0, "CompressedInlineCommentComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1849728
    if-ne p0, p1, :cond_1

    .line 1849729
    :cond_0
    :goto_0
    return v0

    .line 1849730
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1849731
    goto :goto_0

    .line 1849732
    :cond_3
    check-cast p1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;

    .line 1849733
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1849734
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1849735
    if-eq v2, v3, :cond_0

    .line 1849736
    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1849737
    goto :goto_0

    .line 1849738
    :cond_5
    iget-object v2, p1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1849739
    :cond_6
    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;->b:LX/1Pn;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;->b:LX/1Pn;

    iget-object v3, p1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;->b:LX/1Pn;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1849740
    goto :goto_0

    .line 1849741
    :cond_8
    iget-object v2, p1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;->b:LX/1Pn;

    if-nez v2, :cond_7

    .line 1849742
    :cond_9
    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 1849743
    goto :goto_0

    .line 1849744
    :cond_b
    iget-object v2, p1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    if-nez v2, :cond_a

    .line 1849745
    :cond_c
    iget-boolean v2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;->d:Z

    iget-boolean v3, p1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;->d:Z

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 1849746
    goto :goto_0

    .line 1849747
    :cond_d
    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;->e:LX/1EO;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;->e:LX/1EO;

    iget-object v3, p1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;->e:LX/1EO;

    invoke-virtual {v2, v3}, LX/1EO;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1849748
    goto :goto_0

    .line 1849749
    :cond_e
    iget-object v2, p1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;->e:LX/1EO;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
