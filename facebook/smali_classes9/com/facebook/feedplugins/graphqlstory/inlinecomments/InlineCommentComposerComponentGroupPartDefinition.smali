.class public Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:LX/0tN;

.field private final c:LX/1VF;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0tN;LX/1VF;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "LX/0tN;",
            "LX/1VF;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1849670
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1849671
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentGroupPartDefinition;->a:Landroid/content/res/Resources;

    .line 1849672
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentGroupPartDefinition;->b:LX/0tN;

    .line 1849673
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentGroupPartDefinition;->c:LX/1VF;

    .line 1849674
    iput-object p4, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentGroupPartDefinition;->d:LX/0Ot;

    .line 1849675
    return-void
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1849676
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    .line 1849677
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentGroupPartDefinition;->a:Landroid/content/res/Resources;

    invoke-static {p2, v0, p3}, LX/35G;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/res/Resources;LX/1Pr;)LX/35L;

    move-result-object v1

    .line 1849678
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentGroupPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1849679
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1849668
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1849669
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentGroupPartDefinition;->b:LX/0tN;

    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentGroupPartDefinition;->c:LX/1VF;

    invoke-static {v0, v1, p1}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition;->a(LX/0tN;LX/1VF;Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
