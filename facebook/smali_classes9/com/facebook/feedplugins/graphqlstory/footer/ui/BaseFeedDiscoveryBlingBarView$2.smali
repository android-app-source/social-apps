.class public final Lcom/facebook/feedplugins/graphqlstory/footer/ui/BaseFeedDiscoveryBlingBarView$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/3Vr;


# direct methods
.method public constructor <init>(LX/3Vr;)V
    .locals 0

    .prologue
    .line 1849458
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/ui/BaseFeedDiscoveryBlingBarView$2;->a:LX/3Vr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 1849459
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/ui/BaseFeedDiscoveryBlingBarView$2;->a:LX/3Vr;

    iget-object v0, v0, LX/3Vr;->k:LX/C5w;

    sget-object v1, LX/C5w;->DISABLED:LX/C5w;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/ui/BaseFeedDiscoveryBlingBarView$2;->a:LX/3Vr;

    iget-object v0, v0, LX/3Vr;->j:LX/C60;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/ui/BaseFeedDiscoveryBlingBarView$2;->a:LX/3Vr;

    iget-object v0, v0, LX/3Vr;->j:LX/C60;

    .line 1849460
    iget-object v1, v0, LX/C60;->d:LX/C5z;

    move-object v0, v1

    .line 1849461
    sget-object v1, LX/C5z;->SHOWING_BLING_BAR:LX/C5z;

    if-ne v0, v1, :cond_1

    .line 1849462
    :cond_0
    :goto_0
    return-void

    .line 1849463
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/ui/BaseFeedDiscoveryBlingBarView$2;->a:LX/3Vr;

    iget-object v0, v0, LX/3Vr;->j:LX/C60;

    invoke-virtual {v0}, LX/C60;->c()Z

    move-result v0

    .line 1849464
    if-nez v0, :cond_2

    .line 1849465
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/ui/BaseFeedDiscoveryBlingBarView$2;->a:LX/3Vr;

    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/ui/BaseFeedDiscoveryBlingBarView$2;->a:LX/3Vr;

    iget-object v1, v1, LX/3Vr;->a:LX/0tJ;

    invoke-virtual {v1}, LX/0tJ;->d()I

    move-result v1

    .line 1849466
    invoke-static {v0, v1}, LX/3Vr;->b$redex0(LX/3Vr;I)V

    .line 1849467
    goto :goto_0

    .line 1849468
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/ui/BaseFeedDiscoveryBlingBarView$2;->a:LX/3Vr;

    iget-object v0, v0, LX/3Vr;->b:LX/C5g;

    if-eqz v0, :cond_0

    .line 1849469
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/ui/BaseFeedDiscoveryBlingBarView$2;->a:LX/3Vr;

    iget-object v0, v0, LX/3Vr;->b:LX/C5g;

    .line 1849470
    iget-object v1, v0, LX/C5g;->c:Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryBlingBarPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryBlingBarPartDefinition;->b:LX/1WR;

    iget-object v2, v0, LX/C5g;->a:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1849471
    const-string p0, "saw_return_to_bling_bar"

    invoke-static {v1, v2, p0}, LX/1WR;->a(LX/1WR;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V

    .line 1849472
    iget-object v1, v0, LX/C5g;->b:LX/C5h;

    const/4 v2, 0x1

    .line 1849473
    iput-boolean v2, v1, LX/C5h;->b:Z

    .line 1849474
    goto :goto_0
.end method
