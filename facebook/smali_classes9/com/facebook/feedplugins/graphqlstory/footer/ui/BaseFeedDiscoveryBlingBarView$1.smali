.class public final Lcom/facebook/feedplugins/graphqlstory/footer/ui/BaseFeedDiscoveryBlingBarView$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/3Vr;


# direct methods
.method public constructor <init>(LX/3Vr;)V
    .locals 0

    .prologue
    .line 1849439
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/ui/BaseFeedDiscoveryBlingBarView$1;->a:LX/3Vr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 1849440
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/ui/BaseFeedDiscoveryBlingBarView$1;->a:LX/3Vr;

    iget-object v0, v0, LX/3Vr;->k:LX/C5w;

    sget-object v1, LX/C5w;->DISABLED:LX/C5w;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/ui/BaseFeedDiscoveryBlingBarView$1;->a:LX/3Vr;

    iget-object v0, v0, LX/3Vr;->j:LX/C60;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/ui/BaseFeedDiscoveryBlingBarView$1;->a:LX/3Vr;

    iget-object v0, v0, LX/3Vr;->j:LX/C60;

    .line 1849441
    iget-object v1, v0, LX/C60;->d:LX/C5z;

    move-object v0, v1

    .line 1849442
    sget-object v1, LX/C5z;->SHOWING_REAL_TIME_ACTIVITY:LX/C5z;

    if-ne v0, v1, :cond_2

    .line 1849443
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/ui/BaseFeedDiscoveryBlingBarView$1;->a:LX/3Vr;

    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/ui/BaseFeedDiscoveryBlingBarView$1;->a:LX/3Vr;

    iget-object v1, v1, LX/3Vr;->a:LX/0tJ;

    invoke-virtual {v1}, LX/0tJ;->c()I

    move-result v1

    .line 1849444
    invoke-static {v0, v1}, LX/3Vr;->b$redex0(LX/3Vr;I)V

    .line 1849445
    :cond_1
    :goto_0
    return-void

    .line 1849446
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/ui/BaseFeedDiscoveryBlingBarView$1;->a:LX/3Vr;

    iget-object v0, v0, LX/3Vr;->j:LX/C60;

    invoke-virtual {v0}, LX/C60;->c()Z

    move-result v0

    .line 1849447
    if-eqz v0, :cond_3

    .line 1849448
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/ui/BaseFeedDiscoveryBlingBarView$1;->a:LX/3Vr;

    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/ui/BaseFeedDiscoveryBlingBarView$1;->a:LX/3Vr;

    iget-object v1, v1, LX/3Vr;->a:LX/0tJ;

    invoke-virtual {v1}, LX/0tJ;->c()I

    move-result v1

    .line 1849449
    invoke-static {v0, v1}, LX/3Vr;->b$redex0(LX/3Vr;I)V

    .line 1849450
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/ui/BaseFeedDiscoveryBlingBarView$1;->a:LX/3Vr;

    iget-object v0, v0, LX/3Vr;->b:LX/C5g;

    if-eqz v0, :cond_1

    .line 1849451
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/ui/BaseFeedDiscoveryBlingBarView$1;->a:LX/3Vr;

    iget-object v0, v0, LX/3Vr;->b:LX/C5g;

    .line 1849452
    iget-object v1, v0, LX/C5g;->c:Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryBlingBarPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryBlingBarPartDefinition;->b:LX/1WR;

    iget-object p0, v0, LX/C5g;->a:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1849453
    const-string v0, "saw_recent_activity"

    invoke-static {v1, p0, v0}, LX/1WR;->a(LX/1WR;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V

    .line 1849454
    goto :goto_0

    .line 1849455
    :cond_3
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/ui/BaseFeedDiscoveryBlingBarView$1;->a:LX/3Vr;

    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/ui/BaseFeedDiscoveryBlingBarView$1;->a:LX/3Vr;

    iget-object v1, v1, LX/3Vr;->a:LX/0tJ;

    invoke-virtual {v1}, LX/0tJ;->d()I

    move-result v1

    .line 1849456
    invoke-static {v0, v1}, LX/3Vr;->a$redex0(LX/3Vr;I)V

    .line 1849457
    goto :goto_0
.end method
