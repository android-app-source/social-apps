.class public Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pg;",
        ":",
        "LX/1Pe;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "LX/C5n;",
        "TE;",
        "Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;",
        ">;"
    }
.end annotation


# static fields
.field private static m:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;

.field private final b:Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition",
            "<",
            "Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/14w;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AnQ;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/20p;

.field private final f:LX/03V;

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/215;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/20n;

.field private final j:LX/0bH;

.field private final k:LX/217;

.field private final l:LX/1WR;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;LX/14w;LX/03V;LX/0Ot;LX/20p;LX/20n;LX/217;LX/0Ot;LX/0Or;LX/0bH;LX/1WR;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;",
            "Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;",
            "LX/14w;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Ot",
            "<",
            "LX/AnQ;",
            ">;",
            "LX/20p;",
            "LX/20n;",
            "LX/217;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;",
            "LX/0Or",
            "<",
            "LX/215;",
            ">;",
            "LX/0bH;",
            "LX/1WR;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1849297
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1849298
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;->a:Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;

    .line 1849299
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    .line 1849300
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;->c:LX/14w;

    .line 1849301
    iput-object p4, p0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;->f:LX/03V;

    .line 1849302
    iput-object p5, p0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;->d:LX/0Ot;

    .line 1849303
    iput-object p6, p0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;->e:LX/20p;

    .line 1849304
    iput-object p7, p0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;->i:LX/20n;

    .line 1849305
    iput-object p8, p0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;->k:LX/217;

    .line 1849306
    iput-object p9, p0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;->h:LX/0Ot;

    .line 1849307
    iput-object p10, p0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;->g:LX/0Or;

    .line 1849308
    iput-object p11, p0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;->j:LX/0bH;

    .line 1849309
    iput-object p12, p0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;->l:LX/1WR;

    .line 1849310
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;
    .locals 3

    .prologue
    .line 1849289
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;

    monitor-enter v1

    .line 1849290
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;->m:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1849291
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;->m:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1849292
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1849293
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;->b(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1849294
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1849295
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1849296
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;LX/1Pg;LX/20X;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/view/View;",
            "TE;",
            "LX/20X;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1849277
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1849278
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1849279
    const/4 v4, 0x1

    .line 1849280
    sget-object v1, LX/C5m;->a:[I

    invoke-virtual {p4}, LX/20X;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1849281
    :goto_0
    return-void

    .line 1849282
    :pswitch_0
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;->k:LX/217;

    check-cast p3, LX/1Po;

    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AnQ;

    iget-object v3, p0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;->l:LX/1WR;

    invoke-static {p1, v1, v2, v0, v3}, LX/212;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/217;LX/1PT;LX/AnQ;LX/1WR;)V

    goto :goto_0

    .line 1849283
    :pswitch_1
    invoke-static {v0}, LX/14w;->q(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    move-object v0, p3

    .line 1849284
    check-cast v0, LX/1Pe;

    invoke-interface {v0, v1}, LX/1Pe;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p3

    .line 1849285
    check-cast v0, LX/1Pe;

    invoke-interface {v0, v1}, LX/1Pe;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 1849286
    const/4 v4, 0x0

    .line 1849287
    :cond_0
    check-cast p3, LX/1Po;

    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;->i:LX/20n;

    iget-object v5, p0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;->l:LX/1WR;

    move-object v0, p2

    move-object v1, p1

    invoke-static/range {v0 .. v5}, LX/212;->a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;LX/20n;ZLX/1WR;)V

    goto :goto_0

    .line 1849288
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0gh;

    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;->e:LX/20p;

    check-cast p3, LX/1Po;

    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;->j:LX/0bH;

    iget-object v6, p0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;->l:LX/1WR;

    move-object v0, p2

    move-object v3, p1

    invoke-static/range {v0 .. v6}, LX/212;->a(Landroid/view/View;LX/0gh;LX/20p;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;LX/0bH;LX/1WR;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static b(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;
    .locals 13

    .prologue
    .line 1849255
    new-instance v0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    move-result-object v2

    check-cast v2, Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    invoke-static {p0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v3

    check-cast v3, LX/14w;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    const/16 v5, 0x1d6c

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {p0}, LX/20p;->a(LX/0QB;)LX/20p;

    move-result-object v6

    check-cast v6, LX/20p;

    invoke-static {p0}, LX/20n;->a(LX/0QB;)LX/20n;

    move-result-object v7

    check-cast v7, LX/20n;

    invoke-static {p0}, LX/217;->a(LX/0QB;)LX/217;

    move-result-object v8

    check-cast v8, LX/217;

    const/16 v9, 0x97

    invoke-static {p0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x13a4

    invoke-static {p0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-static {p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v11

    check-cast v11, LX/0bH;

    invoke-static {p0}, LX/1WR;->a(LX/0QB;)LX/1WR;

    move-result-object v12

    check-cast v12, LX/1WR;

    invoke-direct/range {v0 .. v12}, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;-><init>(Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;LX/14w;LX/03V;LX/0Ot;LX/20p;LX/20n;LX/217;LX/0Ot;LX/0Or;LX/0bH;LX/1WR;)V

    .line 1849256
    return-object v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 1849311
    sget-object v0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1849269
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pg;

    .line 1849270
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1849271
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    move-object v1, p3

    .line 1849272
    check-cast v1, LX/1Pr;

    new-instance v3, LX/213;

    move-object v2, p3

    check-cast v2, LX/1Po;

    invoke-interface {v2}, LX/1Po;->c()LX/1PT;

    move-result-object v2

    invoke-direct {v3, v0, v2}, LX/213;-><init>(Lcom/facebook/graphql/model/GraphQLStory;LX/1PT;)V

    invoke-interface {v1, v3, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/EnumMap;

    .line 1849273
    new-instance v2, LX/C5l;

    invoke-direct {v2, p0, p2, p3}, LX/C5l;-><init>(Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pg;)V

    move-object v2, v2

    .line 1849274
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->w()Z

    move-result v3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->y()Z

    move-result v4

    invoke-static {v0}, LX/214;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    iget-object v5, p0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;->g:LX/0Or;

    invoke-static {v3, v4, v0, v1, v5}, LX/212;->a(ZZZLjava/util/EnumMap;LX/0Or;)V

    .line 1849275
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    new-instance v3, LX/20d;

    iget-object v4, p0, Lcom/facebook/feedplugins/graphqlstory/footer/LiveVideoFooterPartDefinition;->a:Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;

    check-cast p3, LX/1Pr;

    invoke-virtual {v4, p2, p3}, Lcom/facebook/feedplugins/graphqlstory/footer/DefaultFooterPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pr;)LX/1Wi;

    move-result-object v4

    invoke-direct {v3, p2, v4}, LX/20d;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Wi;)V

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1849276
    new-instance v0, LX/C5n;

    invoke-direct {v0, v1, v2}, LX/C5n;-><init>(Ljava/util/EnumMap;LX/20Z;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x41a8bde6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1849264
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/C5n;

    check-cast p4, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;

    .line 1849265
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1849266
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1849267
    iget-object v2, p2, LX/C5n;->a:Ljava/util/EnumMap;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    iget-object p0, p2, LX/C5n;->b:LX/20Z;

    invoke-static {p4, v2, v1, p0}, LX/212;->a(LX/1wK;Ljava/util/EnumMap;Lcom/facebook/graphql/model/GraphQLFeedback;LX/20Z;)V

    .line 1849268
    const/16 v1, 0x1f

    const v2, 0xc380df6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1849260
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1849261
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1849262
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1849263
    invoke-static {v0}, LX/14w;->o(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, LX/14w;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1849257
    check-cast p4, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;

    .line 1849258
    invoke-virtual {p4}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->a()V

    .line 1849259
    return-void
.end method
