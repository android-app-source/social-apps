.class public final Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterAnimator$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/C5o;

.field public final synthetic b:[I

.field public final synthetic c:I

.field public final synthetic d:J

.field public final synthetic e:LX/C67;

.field public final synthetic f:LX/C5p;


# direct methods
.method public constructor <init>(LX/C5p;LX/C5o;[IIJLX/C67;)V
    .locals 1

    .prologue
    .line 1849312
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterAnimator$1;->f:LX/C5p;

    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterAnimator$1;->a:LX/C5o;

    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterAnimator$1;->b:[I

    iput p4, p0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterAnimator$1;->c:I

    iput-wide p5, p0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterAnimator$1;->d:J

    iput-object p7, p0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterAnimator$1;->e:LX/C67;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 1849313
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterAnimator$1;->a:LX/C5o;

    iget-object v0, v0, LX/C5o;->b:Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterAnimator$1;->b:[I

    invoke-virtual {v0, v1}, Landroid/view/View;->getLocationInWindow([I)V

    .line 1849314
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterAnimator$1;->b:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    iget v1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterAnimator$1;->c:I

    if-ge v0, v1, :cond_0

    .line 1849315
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterAnimator$1;->a:LX/C5o;

    iget-object v0, v0, LX/C5o;->a:LX/1Wt;

    invoke-virtual {v0}, LX/1Wt;->e()V

    .line 1849316
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterAnimator$1;->f:LX/C5p;

    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterAnimator$1;->a:LX/C5o;

    iget-object v1, v1, LX/C5o;->a:LX/1Wt;

    iget-wide v2, p0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterAnimator$1;->d:J

    invoke-static {v0, p0, v1, v2, v3}, LX/C5p;->a$redex0(LX/C5p;Ljava/lang/Runnable;LX/1Wt;J)V

    .line 1849317
    :goto_0
    return-void

    .line 1849318
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterAnimator$1;->e:LX/C67;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterAnimator$1;->e:LX/C67;

    .line 1849319
    iget-boolean v1, v0, LX/C67;->b:Z

    move v0, v1

    .line 1849320
    if-eqz v0, :cond_1

    .line 1849321
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterAnimator$1;->a:LX/C5o;

    iget-object v0, v0, LX/C5o;->a:LX/1Wt;

    .line 1849322
    const/4 v1, 0x0

    iput-object v1, v0, LX/1Wt;->b:Ljava/lang/ref/WeakReference;

    .line 1849323
    sget-object v1, LX/1Wu;->WAITING_FOR_INTERACTION_STOP:LX/1Wu;

    iput-object v1, v0, LX/1Wt;->a:LX/1Wu;

    .line 1849324
    goto :goto_0

    .line 1849325
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterAnimator$1;->f:LX/C5p;

    iget-object v0, v0, LX/C5p;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Rg;

    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterAnimator$1;->a:LX/C5o;

    iget-object v1, v1, LX/C5o;->b:Landroid/view/View;

    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterAnimator$1;->a:LX/C5o;

    iget-wide v2, v2, LX/C5o;->e:J

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterAnimator$1;->a:LX/C5o;

    iget v5, v5, LX/C5o;->c:I

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, LX/1Rg;->a(Landroid/view/View;JIILandroid/animation/Animator$AnimatorListener;)LX/8sj;

    move-result-object v0

    .line 1849326
    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/8sj;->a(F)Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 1849327
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterAnimator$1;->a:LX/C5o;

    iget-object v0, v0, LX/C5o;->a:LX/1Wt;

    .line 1849328
    const/4 v1, 0x0

    iput-object v1, v0, LX/1Wt;->b:Ljava/lang/ref/WeakReference;

    .line 1849329
    invoke-static {v0}, LX/1Wt;->g(LX/1Wt;)V

    .line 1849330
    sget-object v1, LX/1Wu;->PLAYED:LX/1Wu;

    iput-object v1, v0, LX/1Wt;->a:LX/1Wu;

    .line 1849331
    goto :goto_0
.end method
