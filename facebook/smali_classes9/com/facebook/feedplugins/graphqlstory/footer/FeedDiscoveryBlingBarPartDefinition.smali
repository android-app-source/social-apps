.class public Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryBlingBarPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/3Vs;",
        "E::",
        "LX/1Po;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1PV;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "LX/C5j;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/C61;

.field public final b:LX/1WR;

.field public final c:LX/1EN;


# direct methods
.method public constructor <init>(LX/C61;LX/1WR;LX/1EN;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1849239
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1849240
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryBlingBarPartDefinition;->a:LX/C61;

    .line 1849241
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryBlingBarPartDefinition;->b:LX/1WR;

    .line 1849242
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryBlingBarPartDefinition;->c:LX/1EN;

    .line 1849243
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryBlingBarPartDefinition;
    .locals 6

    .prologue
    .line 1849228
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryBlingBarPartDefinition;

    monitor-enter v1

    .line 1849229
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryBlingBarPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1849230
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryBlingBarPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1849231
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1849232
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1849233
    new-instance p0, Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryBlingBarPartDefinition;

    const-class v3, LX/C61;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/C61;

    invoke-static {v0}, LX/1WR;->a(LX/0QB;)LX/1WR;

    move-result-object v4

    check-cast v4, LX/1WR;

    invoke-static {v0}, LX/1EN;->a(LX/0QB;)LX/1EN;

    move-result-object v5

    check-cast v5, LX/1EN;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryBlingBarPartDefinition;-><init>(LX/C61;LX/1WR;LX/1EN;)V

    .line 1849234
    move-object v0, p0

    .line 1849235
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1849236
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryBlingBarPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1849237
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1849238
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 1849204
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Po;

    .line 1849205
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1849206
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    move-object v1, p3

    .line 1849207
    check-cast v1, LX/1Pr;

    invoke-static {v0}, LX/1WT;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v2

    .line 1849208
    new-instance v3, LX/C5i;

    invoke-direct {v3, v2}, LX/C5i;-><init>(Ljava/lang/String;)V

    move-object v2, v3

    .line 1849209
    invoke-interface {v1, v2, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/C5h;

    .line 1849210
    new-instance v2, LX/C5d;

    invoke-direct {v2, p0, v1}, LX/C5d;-><init>(Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryBlingBarPartDefinition;LX/C5h;)V

    .line 1849211
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->H()Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;

    move-result-object v0

    .line 1849212
    iget-object v3, p0, Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryBlingBarPartDefinition;->a:LX/C61;

    .line 1849213
    iget-object v4, v1, LX/C5h;->a:LX/C5z;

    move-object v4, v4

    .line 1849214
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->a()Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityActorsConnection;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityActorsConnection;->a()LX/0Px;

    move-result-object v5

    .line 1849215
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1849216
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result p1

    const/4 v6, 0x0

    move v7, v6

    :goto_0
    if-ge v7, p1, :cond_0

    invoke-virtual {v5, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/graphql/model/GraphQLActor;

    .line 1849217
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v8, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1849218
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_0

    .line 1849219
    :cond_0
    invoke-static {v8}, LX/0R9;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    move-object v5, v6

    .line 1849220
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v3, v4, v2, v5, v0}, LX/C61;->a(LX/C5z;LX/C5d;Ljava/util/List;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/C60;

    move-result-object v2

    .line 1849221
    new-instance v3, LX/C5e;

    invoke-direct {v3, p0, v2}, LX/C5e;-><init>(Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryBlingBarPartDefinition;LX/C60;)V

    .line 1849222
    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    .line 1849223
    new-instance v4, LX/C5f;

    invoke-direct {v4, p0, p2, v0}, LX/C5f;-><init>(Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryBlingBarPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;)V

    move-object v4, v4

    .line 1849224
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1849225
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1849226
    new-instance v5, LX/C5g;

    invoke-direct {v5, p0, v0, v1}, LX/C5g;-><init>(Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryBlingBarPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;LX/C5h;)V

    move-object v5, v5

    .line 1849227
    new-instance v0, LX/C5j;

    invoke-direct/range {v0 .. v5}, LX/C5j;-><init>(LX/C5h;LX/C60;LX/5Oj;Landroid/view/View$OnClickListener;LX/C5g;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x7e80509

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1849191
    check-cast p2, LX/C5j;

    check-cast p3, LX/1Po;

    .line 1849192
    move-object v1, p4

    check-cast v1, LX/3Vs;

    iget-object v2, p2, LX/C5j;->e:LX/C5g;

    invoke-interface {v1, v2}, LX/3Vs;->setAnimationEventListener(LX/C5g;)V

    .line 1849193
    iget-object v1, p2, LX/C5j;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v1, p4

    .line 1849194
    check-cast v1, LX/3Vs;

    iget-object v2, p2, LX/C5j;->a:LX/C60;

    invoke-interface {v1, v2}, LX/3Vs;->setAnimationController(LX/C60;)V

    .line 1849195
    check-cast p3, LX/1PV;

    iget-object v1, p2, LX/C5j;->b:LX/5Oj;

    invoke-interface {p3, v1}, LX/1PV;->a(LX/5Oj;)V

    .line 1849196
    iget-object v1, p2, LX/C5j;->a:LX/C60;

    .line 1849197
    iget-object v2, v1, LX/C60;->d:LX/C5z;

    move-object v1, v2

    .line 1849198
    sget-object v2, LX/C5z;->SHOWING_BLING_BAR:LX/C5z;

    if-ne v1, v2, :cond_0

    iget-object v1, p2, LX/C5j;->c:LX/C5h;

    .line 1849199
    iget-boolean v2, v1, LX/C5h;->b:Z

    move v1, v2

    .line 1849200
    if-nez v1, :cond_0

    .line 1849201
    check-cast p4, LX/3Vs;

    invoke-interface {p4}, LX/3Vs;->f()V

    .line 1849202
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x6982bdb3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1849203
    :cond_0
    check-cast p4, LX/3Vs;

    invoke-interface {p4}, LX/3Vs;->g()V

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1849184
    check-cast p2, LX/C5j;

    check-cast p3, LX/1Po;

    const/4 v1, 0x0

    .line 1849185
    invoke-virtual {p4, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v0, p4

    .line 1849186
    check-cast v0, LX/3Vs;

    invoke-interface {v0, v1}, LX/3Vs;->setAnimationEventListener(LX/C5g;)V

    move-object v0, p4

    .line 1849187
    check-cast v0, LX/3Vs;

    invoke-interface {v0}, LX/3Vs;->e()V

    .line 1849188
    check-cast p4, LX/3Vs;

    invoke-interface {p4}, LX/3Vs;->h()V

    .line 1849189
    check-cast p3, LX/1PV;

    iget-object v0, p2, LX/C5j;->b:LX/5Oj;

    invoke-interface {p3, v0}, LX/1PV;->b(LX/5Oj;)V

    .line 1849190
    return-void
.end method
