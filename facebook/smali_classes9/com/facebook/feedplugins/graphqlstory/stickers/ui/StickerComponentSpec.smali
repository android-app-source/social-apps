.class public Lcom/facebook/feedplugins/graphqlstory/stickers/ui/StickerComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field public static final c:LX/1Ai;

.field private static d:LX/0Xm;


# instance fields
.field public final b:LX/1nu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1nu",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1850981
    const-class v0, Lcom/facebook/feedplugins/graphqlstory/stickers/ui/StickerComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/stickers/ui/StickerComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1850982
    new-instance v0, LX/C7A;

    invoke-direct {v0}, LX/C7A;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/stickers/ui/StickerComponentSpec;->c:LX/1Ai;

    return-void
.end method

.method public constructor <init>(LX/1nu;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1850983
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1850984
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/stickers/ui/StickerComponentSpec;->b:LX/1nu;

    .line 1850985
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/stickers/ui/StickerComponentSpec;
    .locals 4

    .prologue
    .line 1850986
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/stickers/ui/StickerComponentSpec;

    monitor-enter v1

    .line 1850987
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/stickers/ui/StickerComponentSpec;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1850988
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/stickers/ui/StickerComponentSpec;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1850989
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1850990
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1850991
    new-instance p0, Lcom/facebook/feedplugins/graphqlstory/stickers/ui/StickerComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v3

    check-cast v3, LX/1nu;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/graphqlstory/stickers/ui/StickerComponentSpec;-><init>(LX/1nu;)V

    .line 1850992
    move-object v0, p0

    .line 1850993
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1850994
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/stickers/ui/StickerComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1850995
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1850996
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
