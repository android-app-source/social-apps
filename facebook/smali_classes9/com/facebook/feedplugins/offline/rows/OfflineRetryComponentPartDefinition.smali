.class public Lcom/facebook/feedplugins/offline/rows/OfflineRetryComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final d:LX/1V4;

.field private final e:LX/C9m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/C9m",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:LX/C9t;

.field private final g:LX/1V0;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1V4;LX/C9m;LX/C9t;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1854640
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1854641
    iput-object p2, p0, Lcom/facebook/feedplugins/offline/rows/OfflineRetryComponentPartDefinition;->d:LX/1V4;

    .line 1854642
    iput-object p3, p0, Lcom/facebook/feedplugins/offline/rows/OfflineRetryComponentPartDefinition;->e:LX/C9m;

    .line 1854643
    iput-object p4, p0, Lcom/facebook/feedplugins/offline/rows/OfflineRetryComponentPartDefinition;->f:LX/C9t;

    .line 1854644
    iput-object p5, p0, Lcom/facebook/feedplugins/offline/rows/OfflineRetryComponentPartDefinition;->g:LX/1V0;

    .line 1854645
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1854646
    iget-object v1, p0, Lcom/facebook/feedplugins/offline/rows/OfflineRetryComponentPartDefinition;->g:LX/1V0;

    move-object v0, p3

    check-cast v0, LX/1Ps;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->r:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    iget-object v3, p0, Lcom/facebook/feedplugins/offline/rows/OfflineRetryComponentPartDefinition;->e:LX/C9m;

    invoke-virtual {v3, p1}, LX/C9m;->c(LX/1De;)LX/C9j;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/C9j;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C9j;

    move-result-object v3

    invoke-virtual {v3, p3}, LX/C9j;->a(LX/1Pn;)LX/C9j;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->d()LX/1X1;

    move-result-object v3

    invoke-virtual {v1, p1, v0, v2, v3}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/offline/rows/OfflineRetryComponentPartDefinition;
    .locals 9

    .prologue
    .line 1854628
    const-class v1, Lcom/facebook/feedplugins/offline/rows/OfflineRetryComponentPartDefinition;

    monitor-enter v1

    .line 1854629
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/offline/rows/OfflineRetryComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1854630
    sput-object v2, Lcom/facebook/feedplugins/offline/rows/OfflineRetryComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1854631
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1854632
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1854633
    new-instance v3, Lcom/facebook/feedplugins/offline/rows/OfflineRetryComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1V4;->a(LX/0QB;)LX/1V4;

    move-result-object v5

    check-cast v5, LX/1V4;

    invoke-static {v0}, LX/C9m;->a(LX/0QB;)LX/C9m;

    move-result-object v6

    check-cast v6, LX/C9m;

    invoke-static {v0}, LX/C9t;->b(LX/0QB;)LX/C9t;

    move-result-object v7

    check-cast v7, LX/C9t;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v8

    check-cast v8, LX/1V0;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/offline/rows/OfflineRetryComponentPartDefinition;-><init>(Landroid/content/Context;LX/1V4;LX/C9m;LX/C9t;LX/1V0;)V

    .line 1854634
    move-object v0, v3

    .line 1854635
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1854636
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/offline/rows/OfflineRetryComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1854637
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1854638
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1854639
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/offline/rows/OfflineRetryComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1854627
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/offline/rows/OfflineRetryComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1854619
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1854620
    iget-object v1, p0, Lcom/facebook/feedplugins/offline/rows/OfflineRetryComponentPartDefinition;->f:LX/C9t;

    .line 1854621
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1854622
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v0}, LX/C9t;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feedplugins/offline/rows/OfflineRetryComponentPartDefinition;->d:LX/1V4;

    .line 1854623
    iget-object v1, v0, LX/1V4;->b:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 1854624
    iget-object v1, v0, LX/1V4;->a:LX/0ad;

    sget-short p0, LX/1Dd;->d:S

    const/4 p1, 0x0

    invoke-interface {v1, p0, p1}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, LX/1V4;->b:Ljava/lang/Boolean;

    .line 1854625
    :cond_0
    iget-object v1, v0, LX/1V4;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v0, v1

    .line 1854626
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1854617
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1854618
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
