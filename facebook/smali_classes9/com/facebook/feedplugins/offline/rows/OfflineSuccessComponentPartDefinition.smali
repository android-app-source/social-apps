.class public Lcom/facebook/feedplugins/offline/rows/OfflineSuccessComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/C9z;

.field private final e:LX/CA2;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/C9z;LX/CA2;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1854849
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1854850
    iput-object p2, p0, Lcom/facebook/feedplugins/offline/rows/OfflineSuccessComponentPartDefinition;->d:LX/C9z;

    .line 1854851
    iput-object p3, p0, Lcom/facebook/feedplugins/offline/rows/OfflineSuccessComponentPartDefinition;->e:LX/CA2;

    .line 1854852
    return-void
.end method

.method private a(LX/1De;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1854820
    iget-object v0, p0, Lcom/facebook/feedplugins/offline/rows/OfflineSuccessComponentPartDefinition;->d:LX/C9z;

    const/4 v1, 0x0

    .line 1854821
    new-instance v2, LX/C9y;

    invoke-direct {v2, v0}, LX/C9y;-><init>(LX/C9z;)V

    .line 1854822
    sget-object p0, LX/C9z;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/C9x;

    .line 1854823
    if-nez p0, :cond_0

    .line 1854824
    new-instance p0, LX/C9x;

    invoke-direct {p0}, LX/C9x;-><init>()V

    .line 1854825
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/C9x;->a$redex0(LX/C9x;LX/1De;IILX/C9y;)V

    .line 1854826
    move-object v2, p0

    .line 1854827
    move-object v1, v2

    .line 1854828
    move-object v0, v1

    .line 1854829
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/offline/rows/OfflineSuccessComponentPartDefinition;
    .locals 6

    .prologue
    .line 1854838
    const-class v1, Lcom/facebook/feedplugins/offline/rows/OfflineSuccessComponentPartDefinition;

    monitor-enter v1

    .line 1854839
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/offline/rows/OfflineSuccessComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1854840
    sput-object v2, Lcom/facebook/feedplugins/offline/rows/OfflineSuccessComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1854841
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1854842
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1854843
    new-instance p0, Lcom/facebook/feedplugins/offline/rows/OfflineSuccessComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/C9z;->a(LX/0QB;)LX/C9z;

    move-result-object v4

    check-cast v4, LX/C9z;

    invoke-static {v0}, LX/CA2;->b(LX/0QB;)LX/CA2;

    move-result-object v5

    check-cast v5, LX/CA2;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/offline/rows/OfflineSuccessComponentPartDefinition;-><init>(Landroid/content/Context;LX/C9z;LX/CA2;)V

    .line 1854844
    move-object v0, p0

    .line 1854845
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1854846
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/offline/rows/OfflineSuccessComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1854847
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1854848
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1854837
    invoke-direct {p0, p1}, Lcom/facebook/feedplugins/offline/rows/OfflineSuccessComponentPartDefinition;->a(LX/1De;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1854836
    invoke-direct {p0, p1}, Lcom/facebook/feedplugins/offline/rows/OfflineSuccessComponentPartDefinition;->a(LX/1De;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1854832
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1854833
    iget-object v1, p0, Lcom/facebook/feedplugins/offline/rows/OfflineSuccessComponentPartDefinition;->e:LX/CA2;

    .line 1854834
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1854835
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v0}, LX/CA2;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1854830
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1854831
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
