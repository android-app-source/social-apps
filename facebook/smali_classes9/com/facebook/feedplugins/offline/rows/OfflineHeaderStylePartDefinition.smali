.class public Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Ps;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/CA4;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static final c:LX/1Ua;

.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public final b:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1854212
    sget-object v0, LX/1Ua;->r:LX/1Ua;

    sput-object v0, Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;->c:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1854213
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1854214
    iput-object p1, p0, Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 1854215
    iput-object p2, p0, Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;->b:Landroid/content/res/Resources;

    .line 1854216
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;
    .locals 5

    .prologue
    .line 1854217
    const-class v1, Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;

    monitor-enter v1

    .line 1854218
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1854219
    sput-object v2, Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1854220
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1854221
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1854222
    new-instance p0, Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Landroid/content/res/Resources;)V

    .line 1854223
    move-object v0, p0

    .line 1854224
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1854225
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1854226
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1854227
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/1Po;)Z
    .locals 2

    .prologue
    .line 1854228
    invoke-interface {p0}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    invoke-interface {v0}, LX/1PT;->a()LX/1Qt;

    move-result-object v0

    sget-object v1, LX/1Qt;->PERMALINK:LX/1Qt;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1854229
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Po;

    const/4 v3, 0x0

    .line 1854230
    invoke-static {p3}, Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;->a(LX/1Po;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1854231
    :goto_0
    return-object v3

    .line 1854232
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;->c:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 6

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x43e0f094

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1854233
    check-cast p3, LX/1Po;

    const/4 v4, 0x0

    .line 1854234
    check-cast p4, LX/CA4;

    invoke-interface {p4}, LX/CA4;->getOfflineHeaderView()Landroid/view/View;

    move-result-object p1

    .line 1854235
    invoke-static {p3}, Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;->a(LX/1Po;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1854236
    const v1, 0x7f0218ac

    move v2, v4

    move v5, v1

    .line 1854237
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1854238
    invoke-virtual {v1, v4, v2, v4, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 1854239
    invoke-virtual {p1, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1854240
    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    .line 1854241
    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    .line 1854242
    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v4

    .line 1854243
    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result p2

    .line 1854244
    invoke-virtual {p1, v5}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1854245
    invoke-virtual {p1, v1, v2, v4, p2}, Landroid/view/View;->setPadding(IIII)V

    .line 1854246
    const/16 v1, 0x1f

    const v2, 0x1665b6e0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1854247
    :cond_0
    const v2, 0x7f020ac9

    .line 1854248
    iget-object v1, p0, Lcom/facebook/feedplugins/offline/rows/OfflineHeaderStylePartDefinition;->b:Landroid/content/res/Resources;

    const v5, 0x7f0b1088

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    move v5, v2

    move v2, v1

    goto :goto_0
.end method
