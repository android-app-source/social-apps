.class public Lcom/facebook/feedplugins/offline/rows/MediaUploadRetryComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final d:LX/C9J;

.field private final e:LX/C9m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/C9m",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:LX/1V0;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/C9J;LX/C9m;LX/1V0;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1853905
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1853906
    iput-object p2, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadRetryComponentPartDefinition;->d:LX/C9J;

    .line 1853907
    iput-object p3, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadRetryComponentPartDefinition;->e:LX/C9m;

    .line 1853908
    iput-object p4, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadRetryComponentPartDefinition;->f:LX/1V0;

    .line 1853909
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1853910
    iget-object v1, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadRetryComponentPartDefinition;->f:LX/1V0;

    move-object v0, p3

    check-cast v0, LX/1Ps;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->r:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    iget-object v3, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadRetryComponentPartDefinition;->e:LX/C9m;

    invoke-virtual {v3, p1}, LX/C9m;->c(LX/1De;)LX/C9j;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/C9j;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C9j;

    move-result-object v3

    invoke-virtual {v3, p3}, LX/C9j;->a(LX/1Pn;)LX/C9j;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->d()LX/1X1;

    move-result-object v3

    invoke-virtual {v1, p1, v0, v2, v3}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/offline/rows/MediaUploadRetryComponentPartDefinition;
    .locals 8

    .prologue
    .line 1853911
    const-class v1, Lcom/facebook/feedplugins/offline/rows/MediaUploadRetryComponentPartDefinition;

    monitor-enter v1

    .line 1853912
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/offline/rows/MediaUploadRetryComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1853913
    sput-object v2, Lcom/facebook/feedplugins/offline/rows/MediaUploadRetryComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1853914
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1853915
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1853916
    new-instance v7, Lcom/facebook/feedplugins/offline/rows/MediaUploadRetryComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    .line 1853917
    new-instance p0, LX/C9J;

    invoke-static {v0}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a(LX/0QB;)Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-static {v0}, LX/16I;->a(LX/0QB;)LX/16I;

    move-result-object v5

    check-cast v5, LX/16I;

    invoke-static {v0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v6

    check-cast v6, LX/0qn;

    invoke-direct {p0, v4, v5, v6}, LX/C9J;-><init>(Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;LX/16I;LX/0qn;)V

    .line 1853918
    move-object v4, p0

    .line 1853919
    check-cast v4, LX/C9J;

    invoke-static {v0}, LX/C9m;->a(LX/0QB;)LX/C9m;

    move-result-object v5

    check-cast v5, LX/C9m;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v6

    check-cast v6, LX/1V0;

    invoke-direct {v7, v3, v4, v5, v6}, Lcom/facebook/feedplugins/offline/rows/MediaUploadRetryComponentPartDefinition;-><init>(Landroid/content/Context;LX/C9J;LX/C9m;LX/1V0;)V

    .line 1853920
    move-object v0, v7

    .line 1853921
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1853922
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/offline/rows/MediaUploadRetryComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1853923
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1853924
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1853925
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/offline/rows/MediaUploadRetryComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1853926
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/offline/rows/MediaUploadRetryComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 1853927
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1853928
    iget-object v1, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadRetryComponentPartDefinition;->d:LX/C9J;

    .line 1853929
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1853930
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v2, 0x0

    .line 1853931
    if-nez v0, :cond_1

    .line 1853932
    :cond_0
    :goto_0
    move v0, v2

    .line 1853933
    return v0

    .line 1853934
    :cond_1
    invoke-static {v0}, LX/17E;->m(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result p0

    if-nez p0, :cond_2

    invoke-static {v0}, LX/17E;->q(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1853935
    :cond_2
    iget-object p0, v1, LX/C9J;->c:LX/0qn;

    invoke-virtual {p0, v0}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object p0

    .line 1853936
    sget-object p1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->DELETED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq p0, p1, :cond_0

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq p0, p1, :cond_0

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq p0, p1, :cond_0

    .line 1853937
    iget-object p0, v1, LX/C9J;->a:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->d(Ljava/lang/String;)Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object p0

    .line 1853938
    if-eqz p0, :cond_0

    .line 1853939
    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/PendingStory;->g()Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object p0

    .line 1853940
    if-eqz p0, :cond_3

    iget-boolean p0, p0, Lcom/facebook/composer/publish/common/ErrorDetails;->isRetriable:Z

    if-eqz p0, :cond_0

    :cond_3
    iget-object p0, v1, LX/C9J;->b:LX/16I;

    invoke-virtual {p0}, LX/16I;->a()Z

    move-result p0

    if-nez p0, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1853941
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1853942
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
