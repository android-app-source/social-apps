.class public Lcom/facebook/feedplugins/offline/rows/MediaUploadTimedOutComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final d:LX/C9P;

.field private final e:LX/C9M;

.field private final f:LX/1V0;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/C9P;LX/C9M;LX/1V0;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1854084
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1854085
    iput-object p2, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadTimedOutComponentPartDefinition;->d:LX/C9P;

    .line 1854086
    iput-object p3, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadTimedOutComponentPartDefinition;->e:LX/C9M;

    .line 1854087
    iput-object p4, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadTimedOutComponentPartDefinition;->f:LX/1V0;

    .line 1854088
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1854024
    iget-object v0, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadTimedOutComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->r:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    iget-object v2, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadTimedOutComponentPartDefinition;->e:LX/C9M;

    const/4 v3, 0x0

    .line 1854025
    new-instance v4, LX/C9L;

    invoke-direct {v4, v2}, LX/C9L;-><init>(LX/C9M;)V

    .line 1854026
    sget-object p0, LX/C9M;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/C9K;

    .line 1854027
    if-nez p0, :cond_0

    .line 1854028
    new-instance p0, LX/C9K;

    invoke-direct {p0}, LX/C9K;-><init>()V

    .line 1854029
    :cond_0
    invoke-static {p0, p1, v3, v3, v4}, LX/C9K;->a$redex0(LX/C9K;LX/1De;IILX/C9L;)V

    .line 1854030
    move-object v4, p0

    .line 1854031
    move-object v3, v4

    .line 1854032
    move-object v2, v3

    .line 1854033
    iget-object v3, v2, LX/C9K;->a:LX/C9L;

    iput-object p2, v3, LX/C9L;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1854034
    iget-object v3, v2, LX/C9K;->d:Ljava/util/BitSet;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 1854035
    move-object v2, v2

    .line 1854036
    invoke-virtual {v2}, LX/1X5;->d()LX/1X1;

    move-result-object v2

    invoke-virtual {v0, p1, p3, v1, v2}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/offline/rows/MediaUploadTimedOutComponentPartDefinition;
    .locals 8

    .prologue
    .line 1854068
    const-class v1, Lcom/facebook/feedplugins/offline/rows/MediaUploadTimedOutComponentPartDefinition;

    monitor-enter v1

    .line 1854069
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/offline/rows/MediaUploadTimedOutComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1854070
    sput-object v2, Lcom/facebook/feedplugins/offline/rows/MediaUploadTimedOutComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1854071
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1854072
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1854073
    new-instance v7, Lcom/facebook/feedplugins/offline/rows/MediaUploadTimedOutComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    .line 1854074
    new-instance v4, LX/C9P;

    invoke-direct {v4}, LX/C9P;-><init>()V

    .line 1854075
    const/16 v5, 0x110

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1d69

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p0, 0x2cc

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 1854076
    iput-object v5, v4, LX/C9P;->a:LX/0Ot;

    iput-object v6, v4, LX/C9P;->b:LX/0Ot;

    iput-object p0, v4, LX/C9P;->c:LX/0Ot;

    .line 1854077
    move-object v4, v4

    .line 1854078
    check-cast v4, LX/C9P;

    invoke-static {v0}, LX/C9M;->a(LX/0QB;)LX/C9M;

    move-result-object v5

    check-cast v5, LX/C9M;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v6

    check-cast v6, LX/1V0;

    invoke-direct {v7, v3, v4, v5, v6}, Lcom/facebook/feedplugins/offline/rows/MediaUploadTimedOutComponentPartDefinition;-><init>(Landroid/content/Context;LX/C9P;LX/C9M;LX/1V0;)V

    .line 1854079
    move-object v0, v7

    .line 1854080
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1854081
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/offline/rows/MediaUploadTimedOutComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1854082
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1854083
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1854067
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/offline/rows/MediaUploadTimedOutComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1854066
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/offline/rows/MediaUploadTimedOutComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 1854039
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1854040
    iget-object v1, p0, Lcom/facebook/feedplugins/offline/rows/MediaUploadTimedOutComponentPartDefinition;->d:LX/C9P;

    .line 1854041
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1854042
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v3, 0x0

    .line 1854043
    if-nez v0, :cond_0

    move v2, v3

    .line 1854044
    :goto_0
    move v0, v2

    .line 1854045
    return v0

    .line 1854046
    :cond_0
    invoke-static {v0}, LX/17E;->m(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v0}, LX/17E;->q(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-nez v2, :cond_1

    move v2, v3

    .line 1854047
    goto :goto_0

    .line 1854048
    :cond_1
    iget-object v2, v1, LX/C9P;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/16I;

    invoke-virtual {v2}, LX/16I;->a()Z

    move-result v2

    if-nez v2, :cond_2

    move v2, v3

    .line 1854049
    goto :goto_0

    .line 1854050
    :cond_2
    iget-object v2, v1, LX/C9P;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0qn;

    invoke-virtual {v2, v0}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v2

    .line 1854051
    sget-object p0, LX/C9O;->a:[I

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->ordinal()I

    move-result v2

    aget v2, p0, v2

    packed-switch v2, :pswitch_data_0

    move v2, v3

    .line 1854052
    goto :goto_0

    .line 1854053
    :pswitch_0
    iget-object v2, v1, LX/C9P;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/9A1;

    .line 1854054
    invoke-static {v0}, LX/9A1;->h(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object p0

    .line 1854055
    if-nez p0, :cond_5

    .line 1854056
    const/4 p0, 0x0

    .line 1854057
    :goto_1
    move v2, p0

    .line 1854058
    if-nez v2, :cond_4

    .line 1854059
    iget-object v2, v1, LX/C9P;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/9A1;

    .line 1854060
    invoke-static {v0}, LX/9A1;->h(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object p0

    .line 1854061
    if-eqz p0, :cond_3

    .line 1854062
    invoke-static {v2, p0}, LX/9A1;->b(LX/9A1;Ljava/lang/String;)V

    .line 1854063
    :cond_3
    move v2, v3

    .line 1854064
    goto :goto_0

    .line 1854065
    :cond_4
    const/4 v2, 0x1

    goto :goto_0

    :cond_5
    iget-object p1, v2, LX/9A1;->c:Ljava/util/Set;

    invoke-interface {p1, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1854037
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1854038
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
