.class public Lcom/facebook/feedplugins/articlechaining/components/ArticleChainingItemComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pm;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static g:LX/0Xm;


# instance fields
.field public final b:LX/1DR;

.field public final c:LX/1nu;

.field public final d:LX/Bx8;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17V;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/BxX;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1834926
    const-class v0, Lcom/facebook/feedplugins/articlechaining/components/ArticleChainingItemComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/articlechaining/components/ArticleChainingItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1DR;LX/1nu;LX/Bx8;LX/0Ot;LX/BxX;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1DR;",
            "LX/1nu;",
            "LX/Bx8;",
            "LX/0Ot",
            "<",
            "LX/17V;",
            ">;",
            "LX/BxX;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1834927
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1834928
    iput-object p1, p0, Lcom/facebook/feedplugins/articlechaining/components/ArticleChainingItemComponentSpec;->b:LX/1DR;

    .line 1834929
    iput-object p2, p0, Lcom/facebook/feedplugins/articlechaining/components/ArticleChainingItemComponentSpec;->c:LX/1nu;

    .line 1834930
    iput-object p3, p0, Lcom/facebook/feedplugins/articlechaining/components/ArticleChainingItemComponentSpec;->d:LX/Bx8;

    .line 1834931
    iput-object p4, p0, Lcom/facebook/feedplugins/articlechaining/components/ArticleChainingItemComponentSpec;->e:LX/0Ot;

    .line 1834932
    iput-object p5, p0, Lcom/facebook/feedplugins/articlechaining/components/ArticleChainingItemComponentSpec;->f:LX/BxX;

    .line 1834933
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/articlechaining/components/ArticleChainingItemComponentSpec;
    .locals 9

    .prologue
    .line 1834934
    const-class v1, Lcom/facebook/feedplugins/articlechaining/components/ArticleChainingItemComponentSpec;

    monitor-enter v1

    .line 1834935
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/articlechaining/components/ArticleChainingItemComponentSpec;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1834936
    sput-object v2, Lcom/facebook/feedplugins/articlechaining/components/ArticleChainingItemComponentSpec;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1834937
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1834938
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1834939
    new-instance v3, Lcom/facebook/feedplugins/articlechaining/components/ArticleChainingItemComponentSpec;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v4

    check-cast v4, LX/1DR;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v5

    check-cast v5, LX/1nu;

    invoke-static {v0}, LX/Bx8;->a(LX/0QB;)LX/Bx8;

    move-result-object v6

    check-cast v6, LX/Bx8;

    const/16 v7, 0x8b

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/BxX;->a(LX/0QB;)LX/BxX;

    move-result-object v8

    check-cast v8, LX/BxX;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/articlechaining/components/ArticleChainingItemComponentSpec;-><init>(LX/1DR;LX/1nu;LX/Bx8;LX/0Ot;LX/BxX;)V

    .line 1834940
    move-object v0, v3

    .line 1834941
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1834942
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/articlechaining/components/ArticleChainingItemComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1834943
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1834944
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
