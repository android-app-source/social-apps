.class public Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;
.super LX/AnF;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final e:Lcom/facebook/common/callercontext/CallerContext;

.field private static final f:LX/2eR;

.field private static q:LX/0Xm;


# instance fields
.field private final g:LX/1qa;

.field private final h:LX/17V;

.field public final i:LX/BxX;

.field private final j:LX/1Ad;

.field private final k:LX/0Uh;

.field public final l:LX/5up;

.field public final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final n:LX/1Kf;

.field public final o:LX/0wM;

.field private final p:Landroid/view/View$OnClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1835489
    const-class v0, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 1835490
    new-instance v0, LX/BxL;

    invoke-direct {v0}, LX/BxL;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->f:LX/2eR;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1qa;LX/0hB;LX/17V;LX/1DR;LX/1Ad;LX/BxX;LX/0Uh;LX/5up;LX/0Ot;LX/1Kf;LX/0wM;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/1qa;",
            "LX/0hB;",
            "LX/17V;",
            "LX/1DR;",
            "LX/1Ad;",
            "LX/BxX;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/5up;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/1Kf;",
            "LX/0wM;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1835477
    invoke-direct {p0, p1, p3, p5}, LX/AnF;-><init>(Landroid/content/Context;LX/0hB;LX/1DR;)V

    .line 1835478
    new-instance v0, LX/BxM;

    invoke-direct {v0, p0}, LX/BxM;-><init>(Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;)V

    iput-object v0, p0, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->p:Landroid/view/View$OnClickListener;

    .line 1835479
    iput-object p2, p0, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->g:LX/1qa;

    .line 1835480
    iput-object p4, p0, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->h:LX/17V;

    .line 1835481
    iput-object p6, p0, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->j:LX/1Ad;

    .line 1835482
    iput-object p7, p0, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->i:LX/BxX;

    .line 1835483
    iput-object p8, p0, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->k:LX/0Uh;

    .line 1835484
    iput-object p9, p0, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->l:LX/5up;

    .line 1835485
    iput-object p10, p0, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->m:LX/0Ot;

    .line 1835486
    iput-object p11, p0, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->n:LX/1Kf;

    .line 1835487
    iput-object p12, p0, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->o:LX/0wM;

    .line 1835488
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;
    .locals 3

    .prologue
    .line 1835469
    const-class v1, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;

    monitor-enter v1

    .line 1835470
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->q:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1835471
    sput-object v2, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->q:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1835472
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1835473
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->b(LX/0QB;)Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1835474
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1835475
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1835476
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1835460
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1835461
    move-object v7, v0

    check-cast v7, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1835462
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p2}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v3

    const-string v4, "native_newsfeed"

    const/4 v5, 0x0

    invoke-static {v7}, LX/1VO;->z(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v1 .. v6}, LX/17V;->a(Ljava/lang/String;ZLX/0lF;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1835463
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1835464
    if-eqz p1, :cond_0

    if-eqz v1, :cond_0

    .line 1835465
    const v2, 0x7f0d006a

    invoke-virtual {p1, v2, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1835466
    const v0, 0x7f0d0073

    invoke-virtual {p1, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1835467
    iget-object v0, p0, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->p:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1835468
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/BxZ;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1835410
    iget-object v0, p0, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->k:LX/0Uh;

    const/16 v1, 0x11

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1835411
    :cond_0
    :goto_0
    return-void

    .line 1835412
    :cond_1
    iget-object v0, p0, LX/AnF;->d:LX/1PW;

    if-eqz v0, :cond_0

    .line 1835413
    iget-object v0, p0, LX/AnF;->d:LX/1PW;

    check-cast v0, LX/1Pr;

    new-instance v1, LX/BxW;

    invoke-direct {v1, p1}, LX/BxW;-><init>(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    invoke-interface {v0, v1}, LX/1Pr;->a(LX/1KL;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BxV;

    .line 1835414
    iget-object v1, v0, LX/BxV;->a:Ljava/lang/Boolean;

    move-object v1, v1

    .line 1835415
    if-nez v1, :cond_4

    .line 1835416
    const v1, -0x3625f733

    invoke-static {p1, v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v3

    .line 1835417
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1835418
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->U()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->U()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->kT()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 1835419
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->U()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->kT()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v1

    .line 1835420
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVABLE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eq v1, v3, :cond_0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eq v1, v3, :cond_0

    .line 1835421
    :cond_2
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eq v1, v3, :cond_3

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->ARCHIVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v1, v3, :cond_8

    :cond_3
    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v0, v1}, LX/BxV;->a(Z)V

    .line 1835422
    :cond_4
    iget-object v1, p2, LX/BxZ;->h:Landroid/view/View;

    if-nez v1, :cond_5

    .line 1835423
    iget-object v1, p2, LX/BxZ;->f:Landroid/view/ViewStub;

    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    iput-object v1, p2, LX/BxZ;->h:Landroid/view/View;

    .line 1835424
    :cond_5
    iget-object v1, p2, LX/BxZ;->h:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1835425
    iget-object v1, p0, LX/AnF;->d:LX/1PW;

    instance-of v1, v1, LX/1Po;

    if-eqz v1, :cond_9

    .line 1835426
    const v1, 0x7f0219c9

    const v2, -0x958e80

    .line 1835427
    iget-object v3, p0, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->o:LX/0wM;

    invoke-virtual {v3, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    move-object v1, v3

    .line 1835428
    const v2, 0x7f080fd9

    invoke-virtual {p2, v1, v2}, LX/BxZ;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 1835429
    new-instance v1, LX/BxN;

    invoke-direct {v1, p0, p1}, LX/BxN;-><init>(Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    invoke-virtual {p2, v1}, LX/BxZ;->b(Landroid/view/View$OnClickListener;)V

    .line 1835430
    :goto_2
    iget-object v1, v0, LX/BxV;->a:Ljava/lang/Boolean;

    move-object v1, v1

    .line 1835431
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1835432
    iget-object v1, p2, LX/BxZ;->h:Landroid/view/View;

    if-eqz v1, :cond_6

    .line 1835433
    iget-object v1, p2, LX/BxZ;->e:LX/Bxa;

    const v2, 0x7f0d05ba

    .line 1835434
    invoke-virtual {v1, v2}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v3

    move-object v1, v3

    .line 1835435
    check-cast v1, Lcom/facebook/fbui/glyph/GlyphView;

    .line 1835436
    iget-object v2, p2, LX/BxZ;->e:LX/Bxa;

    invoke-virtual {v2}, LX/Bxa;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00d2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1835437
    iget-object v1, p2, LX/BxZ;->e:LX/Bxa;

    const v2, 0x7f0d05bb

    .line 1835438
    invoke-virtual {v1, v2}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v3

    move-object v1, v3

    .line 1835439
    check-cast v1, Landroid/widget/TextView;

    .line 1835440
    const v2, 0x7f080d28

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1835441
    :cond_6
    :goto_3
    new-instance v1, LX/BxO;

    invoke-direct {v1, p0, p1, v0}, LX/BxO;-><init>(Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/BxV;)V

    .line 1835442
    iget-object v0, p2, LX/BxZ;->h:Landroid/view/View;

    if-eqz v0, :cond_7

    .line 1835443
    iget-object v0, p2, LX/BxZ;->e:LX/Bxa;

    const v2, 0x7f0d05b9

    .line 1835444
    invoke-virtual {v0, v2}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p2

    move-object v0, p2

    .line 1835445
    check-cast v0, Landroid/widget/LinearLayout;

    .line 1835446
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1835447
    :cond_7
    goto/16 :goto_0

    :cond_8
    move v1, v2

    .line 1835448
    goto/16 :goto_1

    .line 1835449
    :cond_9
    invoke-virtual {p2, v2}, LX/BxZ;->a(Z)V

    goto :goto_2

    .line 1835450
    :cond_a
    iget-object v1, p2, LX/BxZ;->h:Landroid/view/View;

    if-eqz v1, :cond_b

    .line 1835451
    iget-object v1, p2, LX/BxZ;->e:LX/Bxa;

    const v2, 0x7f0d05ba

    .line 1835452
    invoke-virtual {v1, v2}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v3

    move-object v1, v3

    .line 1835453
    check-cast v1, Lcom/facebook/fbui/glyph/GlyphView;

    .line 1835454
    iget-object v2, p2, LX/BxZ;->e:LX/Bxa;

    invoke-virtual {v2}, LX/Bxa;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00e5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1835455
    iget-object v1, p2, LX/BxZ;->e:LX/Bxa;

    const v2, 0x7f0d05bb

    .line 1835456
    invoke-virtual {v1, v2}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v3

    move-object v1, v3

    .line 1835457
    check-cast v1, Landroid/widget/TextView;

    .line 1835458
    const v2, 0x7f080d27

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1835459
    :cond_b
    goto :goto_3
.end method

.method private static b(LX/0QB;)Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;
    .locals 13

    .prologue
    .line 1835408
    new-instance v0, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/1qa;->a(LX/0QB;)LX/1qa;

    move-result-object v2

    check-cast v2, LX/1qa;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v3

    check-cast v3, LX/0hB;

    invoke-static {p0}, LX/17V;->b(LX/0QB;)LX/17V;

    move-result-object v4

    check-cast v4, LX/17V;

    invoke-static {p0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v5

    check-cast v5, LX/1DR;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v6

    check-cast v6, LX/1Ad;

    invoke-static {p0}, LX/BxX;->a(LX/0QB;)LX/BxX;

    move-result-object v7

    check-cast v7, LX/BxX;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-static {p0}, LX/5up;->a(LX/0QB;)LX/5up;

    move-result-object v9

    check-cast v9, LX/5up;

    const/16 v10, 0x259

    invoke-static {p0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {p0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v11

    check-cast v11, LX/1Kf;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v12

    check-cast v12, LX/0wM;

    invoke-direct/range {v0 .. v12}, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;-><init>(Landroid/content/Context;LX/1qa;LX/0hB;LX/17V;LX/1DR;LX/1Ad;LX/BxX;LX/0Uh;LX/5up;LX/0Ot;LX/1Kf;LX/0wM;)V

    .line 1835409
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            ">;)",
            "LX/162;"
        }
    .end annotation

    .prologue
    .line 1835403
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1835404
    check-cast v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    .line 1835405
    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;

    if-eqz v0, :cond_0

    .line 1835406
    invoke-static {p1}, LX/181;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    .line 1835407
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()LX/2eR;
    .locals 1

    .prologue
    .line 1835491
    sget-object v0, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->f:LX/2eR;

    return-object v0
.end method

.method public final a(LX/1PW;)V
    .locals 4

    .prologue
    .line 1835399
    invoke-super {p0, p1}, LX/AnF;->a(LX/1PW;)V

    .line 1835400
    if-eqz p1, :cond_0

    instance-of v0, p1, LX/1Po;

    if-nez v0, :cond_0

    .line 1835401
    iget-object v0, p0, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "article_chaining_no_feed_environment"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Environment does not have feed list type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/AnF;->d:LX/1PW;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1835402
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;LX/99X;LX/AnC;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v1, 0x0

    .line 1835358
    move-object v0, p1

    check-cast v0, LX/Bxa;

    .line 1835359
    iget-object v2, v0, LX/Bxa;->a:LX/BxZ;

    move-object v2, v2

    .line 1835360
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1835361
    invoke-static {p5}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;LX/0Px;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 1835362
    invoke-static {p2}, LX/1VO;->z(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 1835363
    :goto_0
    iget-object v5, v2, LX/BxZ;->g:Landroid/view/View;

    if-eqz v0, :cond_4

    const/4 v4, 0x0

    :goto_1
    invoke-virtual {v5, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1835364
    iget-object v0, v2, LX/BxZ;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1835365
    invoke-virtual {p0}, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->f()I

    move-result v4

    iput v4, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1835366
    invoke-virtual {p0}, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->e()I

    move-result v4

    iput v4, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1835367
    iget-object v4, v2, LX/BxZ;->a:Landroid/view/View;

    invoke-virtual {v4, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1835368
    invoke-virtual {p0, p1, p3}, LX/AnF;->a(Landroid/view/View;LX/99X;)V

    .line 1835369
    iget-object v0, v2, LX/BxZ;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v4, p0, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->g:LX/1qa;

    iget-object v5, p0, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->j:LX/1Ad;

    sget-object v6, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->e:Lcom/facebook/common/callercontext/CallerContext;

    const/16 p4, 0x8

    .line 1835370
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p1

    if-nez p1, :cond_5

    .line 1835371
    invoke-virtual {v0, p4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1835372
    :goto_2
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v0

    .line 1835373
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1835374
    iget-object v4, v2, LX/BxZ;->c:Landroid/widget/TextView;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1835375
    iget-object v4, v2, LX/BxZ;->c:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1835376
    :goto_3
    invoke-direct {p0, p2, v2}, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/BxZ;)V

    .line 1835377
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 1835378
    :goto_4
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 1835379
    iget-object v4, v2, LX/BxZ;->d:Landroid/widget/TextView;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1835380
    iget-object v1, v2, LX/BxZ;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1835381
    :goto_5
    iget-object v0, v2, LX/BxZ;->a:Landroid/view/View;

    invoke-direct {p0, v0, v3}, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1835382
    iget-object v0, v2, LX/BxZ;->c:Landroid/widget/TextView;

    invoke-direct {p0, v0, v3}, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1835383
    iget-object v0, v2, LX/BxZ;->d:Landroid/widget/TextView;

    invoke-direct {p0, v0, v3}, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1835384
    iget-object v0, p0, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->i:LX/BxX;

    sget-object v1, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p2, v1}, LX/BxX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1835385
    return-void

    :cond_0
    move v0, v1

    .line 1835386
    goto/16 :goto_0

    .line 1835387
    :cond_1
    iget-object v0, v2, LX/BxZ;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    .line 1835388
    :cond_2
    const-string v0, ""

    goto :goto_4

    .line 1835389
    :cond_3
    iget-object v0, v2, LX/BxZ;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_5

    .line 1835390
    :cond_4
    const/16 v4, 0x8

    goto/16 :goto_1

    .line 1835391
    :cond_5
    const p1, 0x3ff745d1

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 1835392
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p1

    sget-object p3, LX/26P;->Share:LX/26P;

    invoke-virtual {v4, p1, p3}, LX/1qa;->b(Lcom/facebook/graphql/model/GraphQLMedia;LX/26P;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p3

    .line 1835393
    if-eqz p3, :cond_6

    .line 1835394
    const/4 p1, 0x0

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1835395
    invoke-virtual {v5, v6}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object p1

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object p4

    invoke-virtual {p1, p4}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object p1

    check-cast p1, LX/1Ad;

    invoke-static {p3}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/1bf;

    move-result-object p3

    invoke-virtual {p1, p3}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object p1

    check-cast p1, LX/1Ad;

    invoke-virtual {p1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object p1

    .line 1835396
    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1835397
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 1835398
    :cond_6
    invoke-virtual {v0, p4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto/16 :goto_2
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/widget/TextView;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            ">;",
            "Landroid/widget/TextView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1835354
    iget-object v0, p0, LX/AnF;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1835355
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1835356
    check-cast v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    const v2, 0x7f0f0068

    invoke-static {v1, v0, p2, v2}, LX/AnB;->a(Landroid/content/res/Resources;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;Landroid/widget/TextView;I)V

    .line 1835357
    return-void
.end method

.method public final c()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1835353
    const-class v0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;

    return-object v0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 1835352
    iget-object v0, p0, LX/AnF;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b08e7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public final e()I
    .locals 5

    .prologue
    .line 1835347
    iget-object v0, p0, LX/AnF;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1835348
    invoke-virtual {p0}, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->f()I

    move-result v0

    invoke-static {v0}, LX/AnB;->a(I)I

    move-result v0

    const v2, 0x7f0b08e9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v0, v2

    .line 1835349
    iget-object v2, p0, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->k:LX/0Uh;

    const/16 v3, 0x11

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1835350
    const v2, 0x7f0b0631

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 1835351
    :cond_0
    return v0
.end method

.method public final f()I
    .locals 4

    .prologue
    .line 1835346
    iget-object v0, p0, LX/AnF;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b08dd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iget-object v1, p0, LX/AnF;->b:LX/0hB;

    invoke-virtual {p0}, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->d()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/feedplugins/articlechaining/controllers/ArticleChainingViewController;->g()I

    move-result v3

    invoke-static {v1, v2, v3}, LX/AnB;->a(LX/0hB;II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method public final g()I
    .locals 2

    .prologue
    .line 1835345
    iget-object v0, p0, LX/AnF;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b08e8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method
