.class public final Lcom/facebook/feedplugins/articlechaining/protocol/ArticleHideSuggestionMutationModels$ArticleHideSuggestionMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xaee9268
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feedplugins/articlechaining/protocol/ArticleHideSuggestionMutationModels$ArticleHideSuggestionMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feedplugins/articlechaining/protocol/ArticleHideSuggestionMutationModels$ArticleHideSuggestionMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1835548
    const-class v0, Lcom/facebook/feedplugins/articlechaining/protocol/ArticleHideSuggestionMutationModels$ArticleHideSuggestionMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1835549
    const-class v0, Lcom/facebook/feedplugins/articlechaining/protocol/ArticleHideSuggestionMutationModels$ArticleHideSuggestionMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1835550
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1835551
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1835552
    iget-object v0, p0, Lcom/facebook/feedplugins/articlechaining/protocol/ArticleHideSuggestionMutationModels$ArticleHideSuggestionMutationModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/articlechaining/protocol/ArticleHideSuggestionMutationModels$ArticleHideSuggestionMutationModel;->e:Ljava/lang/String;

    .line 1835553
    iget-object v0, p0, Lcom/facebook/feedplugins/articlechaining/protocol/ArticleHideSuggestionMutationModels$ArticleHideSuggestionMutationModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1835554
    iget-object v0, p0, Lcom/facebook/feedplugins/articlechaining/protocol/ArticleHideSuggestionMutationModels$ArticleHideSuggestionMutationModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/articlechaining/protocol/ArticleHideSuggestionMutationModels$ArticleHideSuggestionMutationModel;->f:Ljava/lang/String;

    .line 1835555
    iget-object v0, p0, Lcom/facebook/feedplugins/articlechaining/protocol/ArticleHideSuggestionMutationModels$ArticleHideSuggestionMutationModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1835556
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1835557
    invoke-direct {p0}, Lcom/facebook/feedplugins/articlechaining/protocol/ArticleHideSuggestionMutationModels$ArticleHideSuggestionMutationModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1835558
    invoke-direct {p0}, Lcom/facebook/feedplugins/articlechaining/protocol/ArticleHideSuggestionMutationModels$ArticleHideSuggestionMutationModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1835559
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1835560
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1835561
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1835562
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1835563
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1835564
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1835565
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1835566
    return-object p0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1835567
    new-instance v0, Lcom/facebook/feedplugins/articlechaining/protocol/ArticleHideSuggestionMutationModels$ArticleHideSuggestionMutationModel;

    invoke-direct {v0}, Lcom/facebook/feedplugins/articlechaining/protocol/ArticleHideSuggestionMutationModels$ArticleHideSuggestionMutationModel;-><init>()V

    .line 1835568
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1835569
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1835570
    const v0, -0x6e5ce789

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1835571
    const v0, 0x5c980311

    return v0
.end method
