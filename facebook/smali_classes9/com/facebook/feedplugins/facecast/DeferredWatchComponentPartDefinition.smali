.class public Lcom/facebook/feedplugins/facecast/DeferredWatchComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final d:LX/CDD;

.field private final e:LX/0tQ;

.field public final f:LX/1V7;

.field private final g:LX/1V0;

.field private final h:LX/0kb;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1V7;LX/1V0;LX/0tQ;LX/CDD;LX/0kb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1846808
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1846809
    iput-object p4, p0, Lcom/facebook/feedplugins/facecast/DeferredWatchComponentPartDefinition;->e:LX/0tQ;

    .line 1846810
    iput-object p5, p0, Lcom/facebook/feedplugins/facecast/DeferredWatchComponentPartDefinition;->d:LX/CDD;

    .line 1846811
    iput-object p2, p0, Lcom/facebook/feedplugins/facecast/DeferredWatchComponentPartDefinition;->f:LX/1V7;

    .line 1846812
    iput-object p3, p0, Lcom/facebook/feedplugins/facecast/DeferredWatchComponentPartDefinition;->g:LX/1V0;

    .line 1846813
    iput-object p6, p0, Lcom/facebook/feedplugins/facecast/DeferredWatchComponentPartDefinition;->h:LX/0kb;

    .line 1846814
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1846761
    invoke-static {p2}, Lcom/facebook/feedplugins/facecast/DeferredWatchComponentPartDefinition;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1846762
    iget-object v1, p0, Lcom/facebook/feedplugins/facecast/DeferredWatchComponentPartDefinition;->d:LX/CDD;

    const/4 v2, 0x0

    .line 1846763
    new-instance v3, LX/CDB;

    invoke-direct {v3, v1}, LX/CDB;-><init>(LX/CDD;)V

    .line 1846764
    sget-object v4, LX/CDD;->a:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/CDA;

    .line 1846765
    if-nez v4, :cond_0

    .line 1846766
    new-instance v4, LX/CDA;

    invoke-direct {v4}, LX/CDA;-><init>()V

    .line 1846767
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/CDA;->a$redex0(LX/CDA;LX/1De;IILX/CDB;)V

    .line 1846768
    move-object v3, v4

    .line 1846769
    move-object v2, v3

    .line 1846770
    move-object v1, v2

    .line 1846771
    iget-object v2, v1, LX/CDA;->a:LX/CDB;

    iput-object v0, v2, LX/CDB;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1846772
    iget-object v2, v1, LX/CDA;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1846773
    move-object v0, v1

    .line 1846774
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 1846775
    iget-object v1, p0, Lcom/facebook/feedplugins/facecast/DeferredWatchComponentPartDefinition;->g:LX/1V0;

    check-cast p3, LX/1Ps;

    .line 1846776
    iget-object v2, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 1846777
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1846778
    invoke-static {p2}, Lcom/facebook/feedplugins/facecast/DeferredWatchComponentPartDefinition;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 1846779
    invoke-static {v2}, LX/2v7;->i(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {v2}, LX/2v7;->f(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    iget-object v2, p0, Lcom/facebook/feedplugins/facecast/DeferredWatchComponentPartDefinition;->f:LX/1V7;

    invoke-virtual {v2}, LX/1V7;->i()LX/1Ua;

    move-result-object v2

    .line 1846780
    :goto_0
    new-instance v4, LX/1X6;

    invoke-direct {v4, v3, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    move-object v2, v4

    .line 1846781
    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0

    .line 1846782
    :cond_2
    iget-object v2, p0, Lcom/facebook/feedplugins/facecast/DeferredWatchComponentPartDefinition;->f:LX/1V7;

    invoke-virtual {v2}, LX/1V7;->j()LX/1Ua;

    move-result-object v2

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/facecast/DeferredWatchComponentPartDefinition;
    .locals 10

    .prologue
    .line 1846797
    const-class v1, Lcom/facebook/feedplugins/facecast/DeferredWatchComponentPartDefinition;

    monitor-enter v1

    .line 1846798
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/facecast/DeferredWatchComponentPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1846799
    sput-object v2, Lcom/facebook/feedplugins/facecast/DeferredWatchComponentPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1846800
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1846801
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1846802
    new-instance v3, Lcom/facebook/feedplugins/facecast/DeferredWatchComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v5

    check-cast v5, LX/1V7;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v6

    check-cast v6, LX/1V0;

    invoke-static {v0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v7

    check-cast v7, LX/0tQ;

    invoke-static {v0}, LX/CDD;->a(LX/0QB;)LX/CDD;

    move-result-object v8

    check-cast v8, LX/CDD;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v9

    check-cast v9, LX/0kb;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/facecast/DeferredWatchComponentPartDefinition;-><init>(Landroid/content/Context;LX/1V7;LX/1V0;LX/0tQ;LX/CDD;LX/0kb;)V

    .line 1846803
    move-object v0, v3

    .line 1846804
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1846805
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/facecast/DeferredWatchComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1846806
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1846807
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static d(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1846793
    invoke-static {p0}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1846794
    if-nez v0, :cond_0

    .line 1846795
    const/4 v0, 0x0

    .line 1846796
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, LX/182;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1846815
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/facecast/DeferredWatchComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1846792
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/facecast/DeferredWatchComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1846785
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1846786
    invoke-static {p1}, Lcom/facebook/feedplugins/facecast/DeferredWatchComponentPartDefinition;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1846787
    iget-object v0, p0, Lcom/facebook/feedplugins/facecast/DeferredWatchComponentPartDefinition;->e:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->y()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 1846788
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1846789
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1846790
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1846791
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    invoke-static {v0}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLStorySaveInfo;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feedplugins/facecast/DeferredWatchComponentPartDefinition;->e:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/facecast/DeferredWatchComponentPartDefinition;->h:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->v()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1846783
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1846784
    invoke-static {p1}, LX/1WF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method
