.class public Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/3VG;


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/TextView;

.field private final g:Landroid/view/View;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1712935
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1712936
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 1712921
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1712922
    const v0, 0x7f0301a0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1712923
    const v0, 0x7f0d0701

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->d:Landroid/widget/TextView;

    .line 1712924
    const v0, 0x7f0d0702

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->e:Landroid/widget/TextView;

    .line 1712925
    const v0, 0x7f0d0704

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->g:Landroid/view/View;

    .line 1712926
    const v0, 0x7f0d0703

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->f:Landroid/widget/TextView;

    .line 1712927
    invoke-virtual {p0}, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1712928
    const v1, 0x7f0810d0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->h:Ljava/lang/String;

    .line 1712929
    const v1, 0x7f0810d1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->i:Ljava/lang/String;

    .line 1712930
    const v1, 0x7f0810d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->j:Ljava/lang/String;

    .line 1712931
    const v1, 0x7f0810d3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->k:Ljava/lang/String;

    .line 1712932
    const v1, 0x7f0810d4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->l:Ljava/lang/String;

    .line 1712933
    const v1, 0x7f0810d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->m:Ljava/lang/String;

    .line 1712934
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1712917
    iget-object v0, p0, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080fc6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1712918
    iget-object v0, p0, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->d:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1712919
    iget-object v0, p0, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->e:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1712920
    return-void
.end method

.method private a(III)V
    .locals 3

    .prologue
    .line 1712909
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    if-nez p3, :cond_0

    .line 1712910
    invoke-direct {p0}, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->a()V

    .line 1712911
    :goto_0
    iput p1, p0, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->a:I

    .line 1712912
    iput p2, p0, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->b:I

    .line 1712913
    return-void

    .line 1712914
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->i:Ljava/lang/String;

    invoke-static {v0, p1, v1, v2}, LX/Ano;->a(Landroid/widget/TextView;ILjava/lang/String;Ljava/lang/String;)V

    .line 1712915
    iget-object v0, p0, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->j:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->k:Ljava/lang/String;

    invoke-static {v0, p2, v1, v2}, LX/Ano;->a(Landroid/widget/TextView;ILjava/lang/String;Ljava/lang/String;)V

    .line 1712916
    iget-object v0, p0, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->l:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->m:Ljava/lang/String;

    invoke-static {v0, p3, v1, v2}, LX/Ano;->a(Landroid/widget/TextView;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public getContainerView()Landroid/view/View;
    .locals 1

    .prologue
    .line 1712937
    iget-object v0, p0, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->g:Landroid/view/View;

    return-object v0
.end method

.method public getLikersCountView()Landroid/view/View;
    .locals 1

    .prologue
    .line 1712908
    iget-object v0, p0, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->d:Landroid/widget/TextView;

    return-object v0
.end method

.method public setComments(I)V
    .locals 2

    .prologue
    .line 1712906
    iget v0, p0, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->a:I

    iget v1, p0, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->c:I

    invoke-direct {p0, v0, p1, v1}, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->a(III)V

    .line 1712907
    return-void
.end method

.method public setHeight(I)V
    .locals 0

    .prologue
    .line 1712905
    return-void
.end method

.method public setIsExpanded(Z)V
    .locals 0

    .prologue
    .line 1712904
    return-void
.end method

.method public setLikes(I)V
    .locals 2

    .prologue
    .line 1712902
    iget v0, p0, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->b:I

    iget v1, p0, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->c:I

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->a(III)V

    .line 1712903
    return-void
.end method

.method public setShares(I)V
    .locals 2

    .prologue
    .line 1712900
    iget v0, p0, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->a:I

    iget v1, p0, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->b:I

    invoke-direct {p0, v0, v1, p1}, Lcom/facebook/feedplugins/base/blingbar/ui/ConstantHeightBlingBarView;->a(III)V

    .line 1712901
    return-void
.end method
