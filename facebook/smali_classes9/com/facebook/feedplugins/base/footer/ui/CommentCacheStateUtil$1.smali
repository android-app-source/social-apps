.class public final Lcom/facebook/feedplugins/base/footer/ui/CommentCacheStateUtil$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/1wK;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic c:LX/20e;


# direct methods
.method public constructor <init>(LX/20e;LX/1wK;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0

    .prologue
    .line 1713152
    iput-object p1, p0, Lcom/facebook/feedplugins/base/footer/ui/CommentCacheStateUtil$1;->c:LX/20e;

    iput-object p2, p0, Lcom/facebook/feedplugins/base/footer/ui/CommentCacheStateUtil$1;->a:LX/1wK;

    iput-object p3, p0, Lcom/facebook/feedplugins/base/footer/ui/CommentCacheStateUtil$1;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    .line 1713153
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/CommentCacheStateUtil$1;->c:LX/20e;

    iget-object v1, p0, Lcom/facebook/feedplugins/base/footer/ui/CommentCacheStateUtil$1;->a:LX/1wK;

    iget-object v2, p0, Lcom/facebook/feedplugins/base/footer/ui/CommentCacheStateUtil$1;->c:LX/20e;

    iget-object v2, v2, LX/20e;->a:LX/20f;

    iget-object v3, p0, Lcom/facebook/feedplugins/base/footer/ui/CommentCacheStateUtil$1;->b:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1713154
    iget-object v4, v2, LX/20f;->b:LX/0tp;

    invoke-static {v3}, LX/16z;->n(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0tp;->a(Ljava/lang/Boolean;Ljava/lang/Boolean;)LX/0uj;

    move-result-object v4

    .line 1713155
    iget-object v5, v2, LX/20f;->a:LX/0si;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, LX/0uk;->a(Ljava/lang/String;)LX/0zO;

    move-result-object v6

    invoke-virtual {v4}, LX/0uk;->c()I

    move-result v4

    int-to-long v8, v4

    invoke-virtual {v6, v8, v9}, LX/0zO;->a(J)LX/0zO;

    move-result-object v4

    sget-object v6, LX/0zS;->b:LX/0zS;

    invoke-virtual {v4, v6}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v4

    invoke-interface {v5, v4}, LX/0sj;->d(LX/0zO;)Z

    move-result v4

    move v2, v4

    .line 1713156
    iget-object v3, v0, LX/20e;->c:Landroid/os/Handler;

    new-instance v4, Lcom/facebook/feedplugins/base/footer/ui/CommentCacheStateUtil$2;

    invoke-direct {v4, v0, v1, v2}, Lcom/facebook/feedplugins/base/footer/ui/CommentCacheStateUtil$2;-><init>(LX/20e;LX/1wK;Z)V

    const v5, 0x5400ba90

    invoke-static {v3, v4, v5}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1713157
    return-void
.end method
