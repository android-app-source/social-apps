.class public Lcom/facebook/feedplugins/base/footer/ui/MisinformationShareDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# static fields
.field public static m:Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public n:LX/Anb;

.field public o:LX/Ao2;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1713198
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 1713199
    sget-object v0, LX/Ao2;->OUTSIDE_OR_BACK:LX/Ao2;

    iput-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/MisinformationShareDialogFragment;->o:LX/Ao2;

    .line 1713200
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/feedplugins/base/footer/ui/MisinformationShareDialogFragment;

    invoke-static {p0}, LX/Anb;->a(LX/0QB;)LX/Anb;

    move-result-object p0

    check-cast p0, LX/Anb;

    iput-object p0, p1, Lcom/facebook/feedplugins/base/footer/ui/MisinformationShareDialogFragment;->n:LX/Anb;

    return-void
.end method

.method public static synthetic b(Lcom/facebook/feedplugins/base/footer/ui/MisinformationShareDialogFragment;)Z
    .locals 1

    .prologue
    .line 1713197
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->S_()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 1713188
    new-instance v0, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/31Y;-><init>(Landroid/content/Context;)V

    .line 1713189
    const-class v1, Lcom/facebook/feedplugins/base/footer/ui/MisinformationShareDialogFragment;

    invoke-static {v1, p0}, Lcom/facebook/feedplugins/base/footer/ui/MisinformationShareDialogFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1713190
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 1713191
    const-string v2, "title"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 1713192
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 1713193
    const-string v2, "message"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 1713194
    const-string v1, "Cancel"

    new-instance v2, LX/Ao0;

    invoke-direct {v2, p0}, LX/Ao0;-><init>(Lcom/facebook/feedplugins/base/footer/ui/MisinformationShareDialogFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1713195
    const-string v1, "Continue"

    new-instance v2, LX/Ao1;

    invoke-direct {v2, p0}, LX/Ao1;-><init>(Lcom/facebook/feedplugins/base/footer/ui/MisinformationShareDialogFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1713196
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    return-object v0
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 3

    .prologue
    .line 1713183
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/MisinformationShareDialogFragment;->o:LX/Ao2;

    sget-object v1, LX/Ao2;->OUTSIDE_OR_BACK:LX/Ao2;

    if-ne v0, v1, :cond_0

    .line 1713184
    iget-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/MisinformationShareDialogFragment;->n:LX/Anb;

    sget-object v1, LX/0ig;->bg:LX/0ih;

    const-string v2, "cancel"

    invoke-virtual {v0, v1, v2}, LX/Anb;->a(LX/0ih;Ljava/lang/String;)V

    .line 1713185
    :cond_0
    sget-object v0, LX/Ao2;->OUTSIDE_OR_BACK:LX/Ao2;

    iput-object v0, p0, Lcom/facebook/feedplugins/base/footer/ui/MisinformationShareDialogFragment;->o:LX/Ao2;

    .line 1713186
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 1713187
    return-void
.end method
