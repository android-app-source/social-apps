.class public Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/C33;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/condensedstory/CondensedStoryComponentPartDefinition;

.field private final b:Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkComponentPartDefinition;

.field private final c:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/condensedstory/CondensedStoryComponentPartDefinition;Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1844924
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1844925
    iput-object p1, p0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkPartDefinition;->a:Lcom/facebook/feedplugins/condensedstory/CondensedStoryComponentPartDefinition;

    .line 1844926
    iput-object p2, p0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkPartDefinition;->b:Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkComponentPartDefinition;

    .line 1844927
    iput-object p3, p0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkPartDefinition;->c:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    .line 1844928
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkPartDefinition;
    .locals 6

    .prologue
    .line 1844929
    const-class v1, Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkPartDefinition;

    monitor-enter v1

    .line 1844930
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1844931
    sput-object v2, Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1844932
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1844933
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1844934
    new-instance p0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/condensedstory/CondensedStoryComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/condensedstory/CondensedStoryComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/condensedstory/CondensedStoryComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkPartDefinition;-><init>(Lcom/facebook/feedplugins/condensedstory/CondensedStoryComponentPartDefinition;Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;)V

    .line 1844935
    move-object v0, p0

    .line 1844936
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1844937
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1844938
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1844939
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1844920
    check-cast p2, LX/C33;

    .line 1844921
    iget-object v0, p0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkPartDefinition;->a:Lcom/facebook/feedplugins/condensedstory/CondensedStoryComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1844922
    iget-object v0, p0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkPartDefinition;->b:Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkComponentPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryBoxedLinkPartDefinition;->c:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    iget-object v2, p2, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0, v1, v2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 1844923
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1844918
    check-cast p1, LX/C33;

    .line 1844919
    sget-object v0, LX/C34;->COMPACT_GROUPS_FEED:LX/C34;

    iget-object v1, p1, LX/C33;->b:LX/C34;

    if-eq v0, v1, :cond_0

    sget-object v0, LX/C34;->GROUP_RELATED_STORIES:LX/C34;

    iget-object v1, p1, LX/C33;->b:LX/C34;

    if-ne v0, v1, :cond_1

    :cond_0
    invoke-static {p1}, LX/C3N;->b(LX/C33;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
