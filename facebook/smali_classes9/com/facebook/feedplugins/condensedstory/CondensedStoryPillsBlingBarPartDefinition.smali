.class public Lcom/facebook/feedplugins/condensedstory/CondensedStoryPillsBlingBarPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/C33;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        "Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final b:LX/C31;

.field private final c:LX/39G;

.field public final d:LX/C3B;


# direct methods
.method public constructor <init>(LX/39G;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/C31;LX/C3B;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1844993
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1844994
    iput-object p1, p0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryPillsBlingBarPartDefinition;->c:LX/39G;

    .line 1844995
    iput-object p2, p0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryPillsBlingBarPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 1844996
    iput-object p3, p0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryPillsBlingBarPartDefinition;->b:LX/C31;

    .line 1844997
    iput-object p4, p0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryPillsBlingBarPartDefinition;->d:LX/C3B;

    .line 1844998
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/condensedstory/CondensedStoryPillsBlingBarPartDefinition;
    .locals 7

    .prologue
    .line 1844982
    const-class v1, Lcom/facebook/feedplugins/condensedstory/CondensedStoryPillsBlingBarPartDefinition;

    monitor-enter v1

    .line 1844983
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryPillsBlingBarPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1844984
    sput-object v2, Lcom/facebook/feedplugins/condensedstory/CondensedStoryPillsBlingBarPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1844985
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1844986
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1844987
    new-instance p0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryPillsBlingBarPartDefinition;

    invoke-static {v0}, LX/39G;->a(LX/0QB;)LX/39G;

    move-result-object v3

    check-cast v3, LX/39G;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/C31;->b(LX/0QB;)LX/C31;

    move-result-object v5

    check-cast v5, LX/C31;

    invoke-static {v0}, LX/C3B;->b(LX/0QB;)LX/C3B;

    move-result-object v6

    check-cast v6, LX/C3B;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/condensedstory/CondensedStoryPillsBlingBarPartDefinition;-><init>(LX/39G;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/C31;LX/C3B;)V

    .line 1844988
    move-object v0, p0

    .line 1844989
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1844990
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryPillsBlingBarPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1844991
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1844992
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1844980
    sget-object v0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->a:LX/1Cz;

    move-object v0, v0

    .line 1844981
    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1844977
    check-cast p2, LX/C33;

    .line 1844978
    iget-object v0, p0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryPillsBlingBarPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    iget-object v1, p0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryPillsBlingBarPartDefinition;->b:LX/C31;

    iget-object v2, p2, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/C33;->b:LX/C34;

    invoke-virtual {v1, v2, v3}, LX/C31;->c(Lcom/facebook/feed/rows/core/props/FeedProps;LX/C34;)LX/1X6;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1844979
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x49c220e5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1844970
    check-cast p1, LX/C33;

    check-cast p4, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    .line 1844971
    iget-object v1, p1, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1844972
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v2

    .line 1844973
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2, p4}, LX/39G;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/Integer;Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;)V

    .line 1844974
    new-instance v1, LX/C2z;

    invoke-direct {v1, p0, p1}, LX/C2z;-><init>(Lcom/facebook/feedplugins/condensedstory/CondensedStoryPillsBlingBarPartDefinition;LX/C33;)V

    invoke-virtual {p4, v1}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1844975
    const/16 v1, 0x1f

    const v2, -0x1e8d51d4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1844976
    const/4 v0, 0x1

    return v0
.end method
