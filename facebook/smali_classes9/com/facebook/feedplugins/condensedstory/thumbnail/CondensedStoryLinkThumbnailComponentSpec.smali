.class public Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryLinkThumbnailComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final c:Lcom/facebook/common/callercontext/CallerContext;

.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/1nu;

.field public final b:LX/C3e;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1846157
    const-class v0, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryLinkThumbnailComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryLinkThumbnailComponentSpec;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;LX/C3e;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1846153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1846154
    iput-object p1, p0, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryLinkThumbnailComponentSpec;->a:LX/1nu;

    .line 1846155
    iput-object p2, p0, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryLinkThumbnailComponentSpec;->b:LX/C3e;

    .line 1846156
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryLinkThumbnailComponentSpec;
    .locals 5

    .prologue
    .line 1846139
    const-class v1, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryLinkThumbnailComponentSpec;

    monitor-enter v1

    .line 1846140
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryLinkThumbnailComponentSpec;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1846141
    sput-object v2, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryLinkThumbnailComponentSpec;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1846142
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1846143
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1846144
    new-instance p0, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryLinkThumbnailComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v3

    check-cast v3, LX/1nu;

    invoke-static {v0}, LX/C3e;->a(LX/0QB;)LX/C3e;

    move-result-object v4

    check-cast v4, LX/C3e;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryLinkThumbnailComponentSpec;-><init>(LX/1nu;LX/C3e;)V

    .line 1846145
    move-object v0, p0

    .line 1846146
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1846147
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryLinkThumbnailComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1846148
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1846149
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1846150
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1846151
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1846152
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->Y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->Y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
