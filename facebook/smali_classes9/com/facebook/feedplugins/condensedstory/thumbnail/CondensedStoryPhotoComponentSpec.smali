.class public Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryPhotoComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:I

.field public static final e:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field public final b:LX/1VL;

.field public final c:LX/1nu;

.field public final d:LX/1vg;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1846374
    const v0, 0x7f0b1d6e

    sput v0, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryPhotoComponentSpec;->a:I

    .line 1846375
    const-class v0, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryPhotoComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryPhotoComponentSpec;->e:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1VL;LX/1nu;LX/1vg;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1846376
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1846377
    iput-object p1, p0, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryPhotoComponentSpec;->b:LX/1VL;

    .line 1846378
    iput-object p2, p0, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryPhotoComponentSpec;->c:LX/1nu;

    .line 1846379
    iput-object p3, p0, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryPhotoComponentSpec;->d:LX/1vg;

    .line 1846380
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryPhotoComponentSpec;
    .locals 6

    .prologue
    .line 1846363
    const-class v1, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryPhotoComponentSpec;

    monitor-enter v1

    .line 1846364
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryPhotoComponentSpec;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1846365
    sput-object v2, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryPhotoComponentSpec;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1846366
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1846367
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1846368
    new-instance p0, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryPhotoComponentSpec;

    invoke-static {v0}, LX/1VL;->b(LX/0QB;)LX/1VL;

    move-result-object v3

    check-cast v3, LX/1VL;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v4

    check-cast v4, LX/1nu;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v5

    check-cast v5, LX/1vg;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryPhotoComponentSpec;-><init>(LX/1VL;LX/1nu;LX/1vg;)V

    .line 1846369
    move-object v0, p0

    .line 1846370
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1846371
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryPhotoComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1846372
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1846373
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;LX/1VL;)Z
    .locals 4

    .prologue
    .line 1846381
    const/4 v1, 0x0

    .line 1846382
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    move v0, v1

    .line 1846383
    :goto_0
    move v0, v0

    .line 1846384
    if-nez v0, :cond_1

    .line 1846385
    invoke-static {p0}, LX/1WF;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1846386
    if-eqz v0, :cond_6

    .line 1846387
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 1846388
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {p1, v0}, LX/1VL;->g(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 1846389
    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 1846390
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1846391
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->Y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v2, v3}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v0, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ")",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1846357
    if-nez p0, :cond_0

    .line 1846358
    const/4 v0, 0x0

    .line 1846359
    :goto_0
    return-object v0

    .line 1846360
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1846361
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryPhotoComponentSpec;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    goto :goto_0

    .line 1846362
    :cond_2
    invoke-static {p0}, LX/1WF;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .locals 2

    .prologue
    .line 1846316
    if-nez p0, :cond_0

    .line 1846317
    const/4 v0, 0x0

    .line 1846318
    :goto_0
    return-object v0

    .line 1846319
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1846320
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryPhotoComponentSpec;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    goto :goto_0

    .line 1846321
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;IZ)LX/1Dg;
    .locals 7
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation

        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;IZ)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1846322
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 1846323
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1846324
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1846325
    invoke-static {v0}, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryPhotoComponentSpec;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v6

    .line 1846326
    if-eqz v6, :cond_1

    iget-object v4, p0, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryPhotoComponentSpec;->b:LX/1VL;

    .line 1846327
    iget-object v1, v6, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1846328
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v4, v1}, LX/1VL;->g(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v4, v5

    .line 1846329
    :goto_0
    if-eqz v4, :cond_4

    .line 1846330
    iget-object v1, p0, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryPhotoComponentSpec;->b:LX/1VL;

    .line 1846331
    iget-object v0, v6, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1846332
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1, v0}, LX/1VL;->h(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/0Rc;

    move-result-object v0

    .line 1846333
    invoke-virtual {v0}, LX/0Rc;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, LX/0Rc;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 1846334
    :goto_1
    if-nez v0, :cond_3

    move-object v0, v2

    :goto_2
    move v1, v3

    .line 1846335
    :goto_3
    if-nez v0, :cond_6

    const-string v0, ""

    :goto_4
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1846336
    iget-object v2, p0, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryPhotoComponentSpec;->c:LX/1nu;

    invoke-virtual {v2, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v0

    sget-object v2, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryPhotoComponentSpec;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v0

    .line 1846337
    if-eqz v1, :cond_8

    .line 1846338
    const v2, 0x7f020cce

    invoke-virtual {v0, v2}, LX/1nw;->l(I)LX/1nw;

    .line 1846339
    :cond_0
    :goto_5
    if-eqz p4, :cond_7

    .line 1846340
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    invoke-interface {v0, p3}, LX/1Di;->i(I)LX/1Di;

    move-result-object v0

    invoke-interface {v0, p3}, LX/1Di;->q(I)LX/1Di;

    move-result-object v0

    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    const v1, -0x333334

    invoke-interface {v0, v1}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object v0

    const/16 v1, 0x8

    invoke-interface {v0, v1, v5}, LX/1Di;->j(II)LX/1Di;

    move-result-object v0

    const v1, 0x7f0b1d7f

    invoke-interface {v0, v5, v1}, LX/1Di;->c(II)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    .line 1846341
    :goto_6
    move-object v0, v0

    .line 1846342
    return-object v0

    :cond_1
    move v4, v3

    .line 1846343
    goto :goto_0

    :cond_2
    move-object v0, v2

    .line 1846344
    goto :goto_1

    .line 1846345
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->Y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_2

    .line 1846346
    :cond_4
    invoke-static {v0}, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryPhotoComponentSpec;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 1846347
    invoke-static {v0}, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryPhotoComponentSpec;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 1846348
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->Y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 1846349
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v1, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_3

    :cond_5
    move-object v0, v2

    move v1, v3

    .line 1846350
    goto :goto_3

    .line 1846351
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 1846352
    :cond_7
    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const v1, 0x7f0b1d7f

    invoke-interface {v0, v5, v1}, LX/1Di;->c(II)LX/1Di;

    move-result-object v0

    invoke-interface {v0, p3}, LX/1Di;->i(I)LX/1Di;

    move-result-object v0

    invoke-interface {v0, p3}, LX/1Di;->q(I)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    goto :goto_6

    .line 1846353
    :cond_8
    if-eqz v4, :cond_0

    .line 1846354
    iget-object v2, p0, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryPhotoComponentSpec;->d:LX/1vg;

    invoke-virtual {v2, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v2

    const v3, 0x7f020320

    invoke-virtual {v2, v3}, LX/2xv;->h(I)LX/2xv;

    move-result-object v2

    const v3, 0x7f0a0097

    invoke-virtual {v2, v3}, LX/2xv;->j(I)LX/2xv;

    move-result-object v2

    invoke-virtual {v2}, LX/1n6;->b()LX/1dc;

    move-result-object v2

    .line 1846355
    iget-object v3, v0, LX/1nw;->a:Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;

    iput-object v2, v3, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->r:LX/1dc;

    .line 1846356
    goto/16 :goto_5
.end method
