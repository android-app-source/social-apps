.class public Lcom/facebook/feedplugins/condensedstory/CondensedStoryPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/C33;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/condensedstory/CondensedStorySelector;

.field private final b:Lcom/facebook/feedplugins/condensedstory/CondensedStoryPillsBlingBarPartDefinition;

.field private final c:LX/0Uh;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/condensedstory/CondensedStorySelector;Lcom/facebook/feedplugins/condensedstory/CondensedStoryPillsBlingBarPartDefinition;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1844962
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1844963
    iput-object p1, p0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryPartDefinition;->a:Lcom/facebook/feedplugins/condensedstory/CondensedStorySelector;

    .line 1844964
    iput-object p2, p0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryPartDefinition;->b:Lcom/facebook/feedplugins/condensedstory/CondensedStoryPillsBlingBarPartDefinition;

    .line 1844965
    iput-object p3, p0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryPartDefinition;->c:LX/0Uh;

    .line 1844966
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/condensedstory/CondensedStoryPartDefinition;
    .locals 13

    .prologue
    .line 1844940
    const-class v1, Lcom/facebook/feedplugins/condensedstory/CondensedStoryPartDefinition;

    monitor-enter v1

    .line 1844941
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1844942
    sput-object v2, Lcom/facebook/feedplugins/condensedstory/CondensedStoryPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1844943
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1844944
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1844945
    new-instance v6, Lcom/facebook/feedplugins/condensedstory/CondensedStoryPartDefinition;

    .line 1844946
    new-instance v7, Lcom/facebook/feedplugins/condensedstory/CondensedStorySelector;

    const/16 v8, 0x888

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x886

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x885

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x1eb4

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x884

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-direct/range {v7 .. v12}, Lcom/facebook/feedplugins/condensedstory/CondensedStorySelector;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 1844947
    move-object v3, v7

    .line 1844948
    check-cast v3, Lcom/facebook/feedplugins/condensedstory/CondensedStorySelector;

    invoke-static {v0}, Lcom/facebook/feedplugins/condensedstory/CondensedStoryPillsBlingBarPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/condensedstory/CondensedStoryPillsBlingBarPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/condensedstory/CondensedStoryPillsBlingBarPartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-direct {v6, v3, v4, v5}, Lcom/facebook/feedplugins/condensedstory/CondensedStoryPartDefinition;-><init>(Lcom/facebook/feedplugins/condensedstory/CondensedStorySelector;Lcom/facebook/feedplugins/condensedstory/CondensedStoryPillsBlingBarPartDefinition;LX/0Uh;)V

    .line 1844949
    move-object v0, v6

    .line 1844950
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1844951
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1844952
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1844953
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1844954
    check-cast p2, LX/C33;

    .line 1844955
    iget-object v0, p0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryPartDefinition;->a:Lcom/facebook/feedplugins/condensedstory/CondensedStorySelector;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1844956
    iget-object v0, p0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryPartDefinition;->b:Lcom/facebook/feedplugins/condensedstory/CondensedStoryPillsBlingBarPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1844957
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 1844958
    check-cast p1, LX/C33;

    .line 1844959
    iget-object v0, p1, LX/C33;->b:LX/C34;

    sget-object v1, LX/C34;->GROUP_RELATED_STORIES:LX/C34;

    if-ne v0, v1, :cond_0

    .line 1844960
    iget-object v0, p0, Lcom/facebook/feedplugins/condensedstory/CondensedStoryPartDefinition;->c:LX/0Uh;

    const/16 v1, 0x4f1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 1844961
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
