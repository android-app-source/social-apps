.class public final Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1797784
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1797785
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1797782
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1797783
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 8

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 1797751
    if-nez p1, :cond_0

    .line 1797752
    :goto_0
    return v0

    .line 1797753
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1797754
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1797755
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1797756
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1797757
    invoke-virtual {p0, p1, v5}, LX/15i;->p(II)I

    move-result v2

    .line 1797758
    const v3, 0x20defee2

    const/4 v7, 0x0

    .line 1797759
    if-nez v2, :cond_1

    move v4, v7

    .line 1797760
    :goto_1
    move v2, v4

    .line 1797761
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1797762
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1797763
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 1797764
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1797765
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1797766
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1797767
    invoke-virtual {p0, p1, v5, v0}, LX/15i;->a(III)I

    move-result v2

    .line 1797768
    invoke-virtual {p0, p1, v6, v0}, LX/15i;->a(III)I

    move-result v3

    .line 1797769
    const/4 v4, 0x3

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1797770
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1797771
    invoke-virtual {p3, v5, v2, v0}, LX/186;->a(III)V

    .line 1797772
    invoke-virtual {p3, v6, v3, v0}, LX/186;->a(III)V

    .line 1797773
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1797774
    :cond_1
    invoke-virtual {p0, v2}, LX/15i;->c(I)I

    move-result p1

    .line 1797775
    if-nez p1, :cond_2

    const/4 v4, 0x0

    .line 1797776
    :goto_2
    if-ge v7, p1, :cond_3

    .line 1797777
    invoke-virtual {p0, v2, v7}, LX/15i;->q(II)I

    move-result p2

    .line 1797778
    invoke-static {p0, p2, v3, p3}, Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p2

    aput p2, v4, v7

    .line 1797779
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 1797780
    :cond_2
    new-array v4, p1, [I

    goto :goto_2

    .line 1797781
    :cond_3
    const/4 v7, 0x1

    invoke-virtual {p3, v4, v7}, LX/186;->a([IZ)I

    move-result v4

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x20defee2 -> :sswitch_1
        0x3b0207c8 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1797742
    if-nez p0, :cond_0

    move v0, v1

    .line 1797743
    :goto_0
    return v0

    .line 1797744
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 1797745
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1797746
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1797747
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 1797748
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1797749
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1797750
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 1797735
    const/4 v7, 0x0

    .line 1797736
    const/4 v1, 0x0

    .line 1797737
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 1797738
    invoke-static {v2, v3, v0}, Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1797739
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 1797740
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1797741
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1797734
    new-instance v0, Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 1797723
    sparse-switch p2, :sswitch_data_0

    .line 1797724
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1797725
    :sswitch_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1797726
    const v1, 0x20defee2

    .line 1797727
    if-eqz v0, :cond_0

    .line 1797728
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 1797729
    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    .line 1797730
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 1797731
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1797732
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1797733
    :cond_0
    :sswitch_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x20defee2 -> :sswitch_1
        0x3b0207c8 -> :sswitch_0
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1797717
    if-eqz p1, :cond_0

    .line 1797718
    invoke-static {p0, p1, p2}, Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 1797719
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$DraculaImplementation;

    .line 1797720
    if-eq v0, v1, :cond_0

    .line 1797721
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1797722
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1797786
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1797684
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1797685
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1797686
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1797687
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1797688
    :cond_0
    iput-object p1, p0, Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1797689
    iput p2, p0, Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$DraculaImplementation;->b:I

    .line 1797690
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1797691
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1797692
    new-instance v0, Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1797693
    iget v0, p0, LX/1vt;->c:I

    .line 1797694
    move v0, v0

    .line 1797695
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1797696
    iget v0, p0, LX/1vt;->c:I

    .line 1797697
    move v0, v0

    .line 1797698
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1797699
    iget v0, p0, LX/1vt;->b:I

    .line 1797700
    move v0, v0

    .line 1797701
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1797702
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1797703
    move-object v0, v0

    .line 1797704
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1797705
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1797706
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1797707
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1797708
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1797709
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1797710
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1797711
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1797712
    invoke-static {v3, v9, v2}, Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1797713
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1797714
    iget v0, p0, LX/1vt;->c:I

    .line 1797715
    move v0, v0

    .line 1797716
    return v0
.end method
