.class public final Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$FetchVideoCaptionsGraphQLModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x17e568ec
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$FetchVideoCaptionsGraphQLModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$FetchVideoCaptionsGraphQLModel$Serializer;
.end annotation


# instance fields
.field private e:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1797851
    const-class v0, Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$FetchVideoCaptionsGraphQLModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1797852
    const-class v0, Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$FetchVideoCaptionsGraphQLModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1797853
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1797854
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1797845
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1797846
    invoke-virtual {p0}, Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$FetchVideoCaptionsGraphQLModel;->a()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v0

    .line 1797847
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1797848
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1797849
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1797850
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1797830
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1797831
    invoke-virtual {p0}, Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$FetchVideoCaptionsGraphQLModel;->a()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1797832
    invoke-virtual {p0}, Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$FetchVideoCaptionsGraphQLModel;->a()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 1797833
    if-eqz v1, :cond_0

    .line 1797834
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$FetchVideoCaptionsGraphQLModel;

    .line 1797835
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$FetchVideoCaptionsGraphQLModel;->e:LX/3Sb;

    .line 1797836
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1797837
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getVideoCaptions"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1797843
    iget-object v0, p0, Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$FetchVideoCaptionsGraphQLModel;->e:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x0

    const v4, 0x3b0207c8

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$FetchVideoCaptionsGraphQLModel;->e:LX/3Sb;

    .line 1797844
    iget-object v0, p0, Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$FetchVideoCaptionsGraphQLModel;->e:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1797840
    new-instance v0, Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$FetchVideoCaptionsGraphQLModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$FetchVideoCaptionsGraphQLModel;-><init>()V

    .line 1797841
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1797842
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1797839
    const v0, -0x486f084b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1797838
    const v0, 0x4ed245b

    return v0
.end method
