.class public final Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$MediaQuestionModel$MediaQuestionOptionOrderModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7af353be
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$MediaQuestionModel$MediaQuestionOptionOrderModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$MediaQuestionModel$MediaQuestionOptionOrderModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1797478
    const-class v0, Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$MediaQuestionModel$MediaQuestionOptionOrderModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1797479
    const-class v0, Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$MediaQuestionModel$MediaQuestionOptionOrderModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1797480
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1797481
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 1797490
    iput p1, p0, Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$MediaQuestionModel$MediaQuestionOptionOrderModel$EdgesModel$NodeModel;->f:I

    .line 1797491
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1797492
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1797493
    if-eqz v0, :cond_0

    .line 1797494
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 1797495
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1797482
    iput-boolean p1, p0, Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$MediaQuestionModel$MediaQuestionOptionOrderModel$EdgesModel$NodeModel;->e:Z

    .line 1797483
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1797484
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1797485
    if-eqz v0, :cond_0

    .line 1797486
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1797487
    :cond_0
    return-void
.end method

.method private a()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1797488
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1797489
    iget-boolean v0, p0, Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$MediaQuestionModel$MediaQuestionOptionOrderModel$EdgesModel$NodeModel;->e:Z

    return v0
.end method

.method private j()I
    .locals 2

    .prologue
    .line 1797470
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1797471
    iget v0, p0, Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$MediaQuestionModel$MediaQuestionOptionOrderModel$EdgesModel$NodeModel;->f:I

    return v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1797472
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1797473
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1797474
    iget-boolean v0, p0, Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$MediaQuestionModel$MediaQuestionOptionOrderModel$EdgesModel$NodeModel;->e:Z

    invoke-virtual {p1, v2, v0}, LX/186;->a(IZ)V

    .line 1797475
    const/4 v0, 0x1

    iget v1, p0, Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$MediaQuestionModel$MediaQuestionOptionOrderModel$EdgesModel$NodeModel;->f:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1797476
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1797477
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1797457
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1797458
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1797459
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1797442
    new-instance v0, LX/BZV;

    invoke-direct {v0, p1}, LX/BZV;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1797443
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1797444
    invoke-virtual {p1, p2, v1}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$MediaQuestionModel$MediaQuestionOptionOrderModel$EdgesModel$NodeModel;->e:Z

    .line 1797445
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$MediaQuestionModel$MediaQuestionOptionOrderModel$EdgesModel$NodeModel;->f:I

    .line 1797446
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1797447
    const-string v0, "viewer_has_chosen"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1797448
    invoke-direct {p0}, Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$MediaQuestionModel$MediaQuestionOptionOrderModel$EdgesModel$NodeModel;->a()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1797449
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1797450
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 1797451
    :goto_0
    return-void

    .line 1797452
    :cond_0
    const-string v0, "vote_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1797453
    invoke-direct {p0}, Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$MediaQuestionModel$MediaQuestionOptionOrderModel$EdgesModel$NodeModel;->j()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1797454
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1797455
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1797456
    :cond_1
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1797465
    const-string v0, "viewer_has_chosen"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1797466
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$MediaQuestionModel$MediaQuestionOptionOrderModel$EdgesModel$NodeModel;->a(Z)V

    .line 1797467
    :cond_0
    :goto_0
    return-void

    .line 1797468
    :cond_1
    const-string v0, "vote_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1797469
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$MediaQuestionModel$MediaQuestionOptionOrderModel$EdgesModel$NodeModel;->a(I)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1797460
    new-instance v0, Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$MediaQuestionModel$MediaQuestionOptionOrderModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$MediaQuestionModel$MediaQuestionOptionOrderModel$EdgesModel$NodeModel;-><init>()V

    .line 1797461
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1797462
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1797463
    const v0, -0x37da77f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1797464
    const v0, -0x49e6ef21

    return v0
.end method
