.class public final Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xb7c912e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$MediaQuestionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1797627
    const-class v0, Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1797618
    const-class v0, Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1797628
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1797629
    return-void
.end method

.method private a()Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$MediaQuestionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1797619
    iget-object v0, p0, Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel;->e:Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$MediaQuestionModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$MediaQuestionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$MediaQuestionModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel;->e:Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$MediaQuestionModel;

    .line 1797620
    iget-object v0, p0, Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel;->e:Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$MediaQuestionModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1797621
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1797622
    invoke-direct {p0}, Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel;->a()Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$MediaQuestionModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1797623
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1797624
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1797625
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1797626
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1797605
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1797606
    invoke-direct {p0}, Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel;->a()Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$MediaQuestionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1797607
    invoke-direct {p0}, Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel;->a()Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$MediaQuestionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$MediaQuestionModel;

    .line 1797608
    invoke-direct {p0}, Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel;->a()Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$MediaQuestionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1797609
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel;

    .line 1797610
    iput-object v0, v1, Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel;->e:Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel$MediaQuestionModel;

    .line 1797611
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1797612
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1797613
    new-instance v0, Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/gametime/GametimeFanFavoriteUpdateVotesModels$FBGametimeFanFavoriteUpdateVotesModel;-><init>()V

    .line 1797614
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1797615
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1797616
    const v0, 0x37e14d60

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1797617
    const v0, 0x3138d3cc

    return v0
.end method
