.class public Lcom/facebook/objectrec/manager/ObjectRecManager;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final a:LX/BEB;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1764178
    const-class v0, LX/BEA;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/objectrec/manager/ObjectRecManager;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/BEB;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BEB;",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1764179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1764180
    iput-object p1, p0, Lcom/facebook/objectrec/manager/ObjectRecManager;->a:LX/BEB;

    .line 1764181
    iput-object p2, p0, Lcom/facebook/objectrec/manager/ObjectRecManager;->b:LX/0Or;

    .line 1764182
    return-void
.end method


# virtual methods
.method public final a(LX/4cq;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1764183
    :try_start_0
    iget-object v0, p0, Lcom/facebook/objectrec/manager/ObjectRecManager;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11H;

    iget-object v1, p0, Lcom/facebook/objectrec/manager/ObjectRecManager;->a:LX/BEB;

    new-instance v2, LX/BEC;

    invoke-direct {v2}, LX/BEC;-><init>()V

    .line 1764184
    iput-object p1, v2, LX/BEC;->a:LX/4cq;

    .line 1764185
    move-object v2, v2

    .line 1764186
    sget-object v3, Lcom/facebook/objectrec/manager/ObjectRecManager;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1764187
    return-object v0

    .line 1764188
    :catch_0
    new-instance v0, LX/2Oo;

    const/4 v1, 0x1

    const-string v2, "ObjectRecManager threw an exception"

    invoke-static {v1, v2}, Lcom/facebook/http/protocol/ApiErrorResult;->a(ILjava/lang/String;)LX/2AV;

    move-result-object v1

    invoke-virtual {v1}, LX/2AV;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v1

    invoke-direct {v0, v1}, LX/2Oo;-><init>(Lcom/facebook/http/protocol/ApiErrorResult;)V

    throw v0
.end method
