.class public Lcom/facebook/push/adm/ADMRegistrarService;
.super LX/1ZN;
.source ""


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Lcom/amazon/device/messaging/ADM;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1925097
    const-class v0, Lcom/facebook/push/adm/ADMRegistrarService;

    sput-object v0, Lcom/facebook/push/adm/ADMRegistrarService;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1925095
    const-string v0, "ADMRegistrarService"

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 1925096
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1925090
    :try_start_0
    iget-object v0, p0, Lcom/facebook/push/adm/ADMRegistrarService;->c:Lcom/amazon/device/messaging/ADM;

    invoke-virtual {v0}, Lcom/amazon/device/messaging/ADM;->getRegistrationId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/push/adm/ADMRegistrarService;->a:LX/0Uh;

    const/16 v1, 0x254

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1925091
    :cond_0
    iget-object v0, p0, Lcom/facebook/push/adm/ADMRegistrarService;->c:Lcom/amazon/device/messaging/ADM;

    invoke-virtual {v0}, Lcom/amazon/device/messaging/ADM;->startRegister()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1925092
    :cond_1
    :goto_0
    return-void

    .line 1925093
    :catch_0
    move-exception v0

    .line 1925094
    sget-object v1, Lcom/facebook/push/adm/ADMRegistrarService;->b:Ljava/lang/Class;

    const-string v2, "ADM Exception"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/push/adm/ADMRegistrarService;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    iput-object v0, p0, Lcom/facebook/push/adm/ADMRegistrarService;->a:LX/0Uh;

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 1925085
    :try_start_0
    iget-object v0, p0, Lcom/facebook/push/adm/ADMRegistrarService;->c:Lcom/amazon/device/messaging/ADM;

    invoke-virtual {v0}, Lcom/amazon/device/messaging/ADM;->getRegistrationId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1925086
    iget-object v0, p0, Lcom/facebook/push/adm/ADMRegistrarService;->c:Lcom/amazon/device/messaging/ADM;

    invoke-virtual {v0}, Lcom/amazon/device/messaging/ADM;->startUnregister()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1925087
    :cond_0
    :goto_0
    return-void

    .line 1925088
    :catch_0
    move-exception v0

    .line 1925089
    sget-object v1, Lcom/facebook/push/adm/ADMRegistrarService;->b:Ljava/lang/Class;

    const-string v2, "ADM Exception"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x595076fe

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1925075
    iget-object v1, p0, Lcom/facebook/push/adm/ADMRegistrarService;->c:Lcom/amazon/device/messaging/ADM;

    if-nez v1, :cond_0

    .line 1925076
    const/16 v1, 0x25

    const v2, 0x77f63ca0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1925077
    :goto_0
    return-void

    .line 1925078
    :cond_0
    iget-object v1, p0, Lcom/facebook/push/adm/ADMRegistrarService;->c:Lcom/amazon/device/messaging/ADM;

    invoke-virtual {v1}, Lcom/amazon/device/messaging/ADM;->isSupported()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1925079
    const-string v1, "REQUEST"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1925080
    const-string v2, "REGISTER"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1925081
    invoke-direct {p0}, Lcom/facebook/push/adm/ADMRegistrarService;->a()V

    .line 1925082
    :cond_1
    :goto_1
    const v1, 0x52fe3ff2

    invoke-static {v1, v0}, LX/02F;->d(II)V

    goto :goto_0

    .line 1925083
    :cond_2
    const-string v2, "UNREGISTER"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1925084
    invoke-direct {p0}, Lcom/facebook/push/adm/ADMRegistrarService;->b()V

    goto :goto_1
.end method

.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 1925074
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x24

    const v2, -0x6538b5b5

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1925068
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 1925069
    invoke-static {p0, p0}, Lcom/facebook/push/adm/ADMRegistrarService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1925070
    :try_start_0
    new-instance v0, Lcom/amazon/device/messaging/ADM;

    invoke-virtual {p0}, Lcom/facebook/push/adm/ADMRegistrarService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/amazon/device/messaging/ADM;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/push/adm/ADMRegistrarService;->c:Lcom/amazon/device/messaging/ADM;
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1925071
    :goto_0
    const v0, 0x2e1d5cd

    invoke-static {v0, v1}, LX/02F;->d(II)V

    return-void

    .line 1925072
    :catch_0
    move-exception v0

    .line 1925073
    sget-object v2, Lcom/facebook/push/adm/ADMRegistrarService;->b:Ljava/lang/Class;

    const-string v3, "Device doesn\'t support ADM"

    invoke-static {v2, v3, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x796aba55

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1925067
    const/16 v1, 0x25

    const v2, -0x5c200344

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
