.class public Lcom/facebook/push/adm/ADMBroadcastReceiver;
.super Lcom/amazon/device/messaging/ADMMessageHandlerBase;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1925024
    const-class v0, Lcom/facebook/push/adm/ADMBroadcastReceiver;

    sput-object v0, Lcom/facebook/push/adm/ADMBroadcastReceiver;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1925025
    const-class v0, Lcom/facebook/push/adm/ADMBroadcastReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/amazon/device/messaging/ADMMessageHandlerBase;-><init>(Ljava/lang/String;)V

    .line 1925026
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1925027
    invoke-direct {p0, p1}, Lcom/amazon/device/messaging/ADMMessageHandlerBase;-><init>(Ljava/lang/String;)V

    .line 1925028
    return-void
.end method


# virtual methods
.method public final onMessage(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 1925029
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1925030
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/facebook/push/adm/ADMService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1925031
    const-string v2, "message_received"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1925032
    const-string v2, "bundle"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1925033
    invoke-virtual {p0, v1}, Lcom/facebook/push/adm/ADMBroadcastReceiver;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1925034
    return-void
.end method

.method public final onRegistered(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1925035
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "RegistrationId: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1925036
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/push/adm/ADMService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1925037
    const-string v1, "registration"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1925038
    const-string v1, "registration_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1925039
    invoke-virtual {p0, v0}, Lcom/facebook/push/adm/ADMBroadcastReceiver;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1925040
    return-void
.end method

.method public final onRegistrationError(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1925041
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "OnRegistrationError Id: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1925042
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/push/adm/ADMService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1925043
    const-string v1, "registration_error"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1925044
    const-string v1, "registration_error_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1925045
    invoke-virtual {p0, v0}, Lcom/facebook/push/adm/ADMBroadcastReceiver;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1925046
    return-void
.end method

.method public final onUnregistered(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1925047
    return-void
.end method
