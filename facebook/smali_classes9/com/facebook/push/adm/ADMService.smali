.class public Lcom/facebook/push/adm/ADMService;
.super LX/1ZN;
.source ""


# static fields
.field private static final g:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/2Gg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/2bC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/Cej;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/2bD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1925148
    const-class v0, Lcom/facebook/push/adm/ADMService;

    sput-object v0, Lcom/facebook/push/adm/ADMService;->g:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1925146
    const-string v0, "ADMService"

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 1925147
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 1925140
    iget-object v0, p0, Lcom/facebook/push/adm/ADMService;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 1925141
    iget-object v1, p0, Lcom/facebook/push/adm/ADMService;->e:LX/Cej;

    .line 1925142
    iget-object v2, v1, LX/2H3;->g:LX/0Tn;

    move-object v1, v2

    .line 1925143
    iget-object v2, p0, Lcom/facebook/push/adm/ADMService;->d:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    .line 1925144
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1925145
    return-void
.end method

.method private static a(Lcom/facebook/push/adm/ADMService;LX/2Gg;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2bC;LX/0SG;LX/Cej;LX/2bD;)V
    .locals 0

    .prologue
    .line 1925139
    iput-object p1, p0, Lcom/facebook/push/adm/ADMService;->a:LX/2Gg;

    iput-object p2, p0, Lcom/facebook/push/adm/ADMService;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p3, p0, Lcom/facebook/push/adm/ADMService;->c:LX/2bC;

    iput-object p4, p0, Lcom/facebook/push/adm/ADMService;->d:LX/0SG;

    iput-object p5, p0, Lcom/facebook/push/adm/ADMService;->e:LX/Cej;

    iput-object p6, p0, Lcom/facebook/push/adm/ADMService;->f:LX/2bD;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/push/adm/ADMService;

    invoke-static {v6}, LX/2Gg;->a(LX/0QB;)LX/2Gg;

    move-result-object v1

    check-cast v1, LX/2Gg;

    invoke-static {v6}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v6}, LX/2bC;->a(LX/0QB;)LX/2bC;

    move-result-object v3

    check-cast v3, LX/2bC;

    invoke-static {v6}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v6}, LX/Cej;->a(LX/0QB;)LX/Cej;

    move-result-object v5

    check-cast v5, LX/Cej;

    invoke-static {v6}, LX/2bD;->a(LX/0QB;)LX/2bD;

    move-result-object v6

    check-cast v6, LX/2bD;

    invoke-static/range {v0 .. v6}, Lcom/facebook/push/adm/ADMService;->a(Lcom/facebook/push/adm/ADMService;LX/2Gg;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2bC;LX/0SG;LX/Cej;LX/2bD;)V

    return-void
.end method

.method private b(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 1925136
    const-string v0, "registration_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1925137
    iget-object v1, p0, Lcom/facebook/push/adm/ADMService;->a:LX/2Gg;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, LX/2Gg;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1925138
    return-void
.end method

.method private c(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 1925098
    const-string v0, "registration_error_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1925099
    iget-object v1, p0, Lcom/facebook/push/adm/ADMService;->a:LX/2Gg;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, LX/2Gg;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1925100
    return-void
.end method

.method private d(Landroid/content/Intent;)V
    .locals 8

    .prologue
    .line 1925117
    const-string v2, "ADM"

    .line 1925118
    const/4 v1, 0x0

    .line 1925119
    const-string v0, "bundle"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    .line 1925120
    if-eqz v3, :cond_1

    .line 1925121
    invoke-direct {p0}, Lcom/facebook/push/adm/ADMService;->a()V

    .line 1925122
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 1925123
    :try_start_0
    invoke-virtual {v3}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1925124
    const-string v6, "params"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1925125
    new-instance v6, Lorg/json/JSONObject;

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1925126
    :goto_1
    if-eqz v0, :cond_3

    const-string v6, "PushNotifId"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1925127
    const-string v0, "PushNotifId"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    move-object v1, v0

    .line 1925128
    goto :goto_0

    .line 1925129
    :cond_0
    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v0, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1925130
    :catch_0
    move-exception v0

    .line 1925131
    sget-object v3, Lcom/facebook/push/adm/ADMService;->g:Ljava/lang/Class;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1925132
    iget-object v3, p0, Lcom/facebook/push/adm/ADMService;->c:LX/2bC;

    invoke-virtual {v3, v2, v1, v0}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1925133
    :goto_3
    iget-object v0, p0, Lcom/facebook/push/adm/ADMService;->f:LX/2bD;

    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/3B4;->ADM:LX/3B4;

    invoke-virtual {v0, p0, v1, v2}, LX/2bD;->a(Landroid/content/Context;Ljava/lang/String;LX/3B4;)V

    .line 1925134
    :cond_1
    return-void

    .line 1925135
    :cond_2
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "ADM JSON message: "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    :cond_3
    move-object v0, v1

    goto :goto_2
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x310a6cf3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1925106
    invoke-static {p0}, LX/1mU;->a(Landroid/content/Context;)V

    .line 1925107
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1925108
    :cond_0
    const/16 v1, 0x25

    const v2, -0x5f034cb7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1925109
    :goto_0
    return-void

    .line 1925110
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "registration"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1925111
    invoke-direct {p0, p1}, Lcom/facebook/push/adm/ADMService;->b(Landroid/content/Intent;)V

    .line 1925112
    :cond_2
    :goto_1
    const v1, 0x7ee578ed

    invoke-static {v1, v0}, LX/02F;->d(II)V

    goto :goto_0

    .line 1925113
    :cond_3
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "registration_error"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1925114
    invoke-direct {p0, p1}, Lcom/facebook/push/adm/ADMService;->c(Landroid/content/Intent;)V

    goto :goto_1

    .line 1925115
    :cond_4
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "message_received"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1925116
    invoke-direct {p0, p1}, Lcom/facebook/push/adm/ADMService;->d(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 1925105
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x2373228e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1925102
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 1925103
    invoke-static {p0, p0}, Lcom/facebook/push/adm/ADMService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1925104
    const/16 v1, 0x25

    const v2, 0x38f8558c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x38036e3a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1925101
    const/16 v1, 0x25

    const v2, -0x534eff68

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
