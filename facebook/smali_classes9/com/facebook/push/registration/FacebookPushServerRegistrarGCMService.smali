.class public Lcom/facebook/push/registration/FacebookPushServerRegistrarGCMService;
.super Lcom/facebook/common/jobscheduler/compat/GcmTaskServiceCompat;
.source ""


# instance fields
.field public a:LX/CfC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1925599
    invoke-direct {p0}, Lcom/facebook/common/jobscheduler/compat/GcmTaskServiceCompat;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/push/registration/FacebookPushServerRegistrarGCMService;

    invoke-static {v0}, LX/CfC;->a(LX/0QB;)LX/CfC;

    move-result-object v0

    check-cast v0, LX/CfC;

    iput-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrarGCMService;->a:LX/CfC;

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()LX/45y;
    .locals 1

    .prologue
    .line 1925600
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrarGCMService;->a:LX/CfC;

    if-nez v0, :cond_0

    .line 1925601
    invoke-static {p0, p0}, Lcom/facebook/push/registration/FacebookPushServerRegistrarGCMService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1925602
    :cond_0
    iget-object v0, p0, Lcom/facebook/push/registration/FacebookPushServerRegistrarGCMService;->a:LX/CfC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 1925603
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
