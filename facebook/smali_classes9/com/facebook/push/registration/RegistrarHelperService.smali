.class public Lcom/facebook/push/registration/RegistrarHelperService;
.super LX/1ZN;
.source ""


# static fields
.field private static final e:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/0Ot;
    .annotation runtime Lcom/facebook/push/registration/ADMService;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Gh;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation runtime Lcom/facebook/push/registration/NNAService;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Gh;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation runtime Lcom/facebook/push/registration/C2DMService;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Gh;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation runtime Lcom/facebook/push/registration/FbnsLiteService;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Gh;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1925658
    const-class v0, Lcom/facebook/push/registration/RegistrarHelperService;

    sput-object v0, Lcom/facebook/push/registration/RegistrarHelperService;->e:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1925659
    sget-object v0, Lcom/facebook/push/registration/RegistrarHelperService;->e:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 1925660
    return-void
.end method

.method private static a(Lcom/facebook/push/registration/RegistrarHelperService;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/push/registration/RegistrarHelperService;",
            "LX/0Ot",
            "<",
            "LX/2Gh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Gh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Gh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Gh;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1925661
    iput-object p1, p0, Lcom/facebook/push/registration/RegistrarHelperService;->a:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/push/registration/RegistrarHelperService;->b:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/push/registration/RegistrarHelperService;->c:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/push/registration/RegistrarHelperService;->d:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 5

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/push/registration/RegistrarHelperService;

    const/16 v1, 0xfe8

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x3025

    invoke-static {v0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0xfec

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0xffc

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    invoke-static {p0, v1, v2, v3, v0}, Lcom/facebook/push/registration/RegistrarHelperService;->a(Lcom/facebook/push/registration/RegistrarHelperService;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x24

    const v2, 0x124f9750

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1925662
    const-string v0, "serviceType"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1925663
    :try_start_0
    invoke-static {v0}, LX/2Ge;->valueOf(Ljava/lang/String;)LX/2Ge;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 1925664
    sget-object v2, LX/CfE;->a:[I

    invoke-virtual {v0}, LX/2Ge;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1925665
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Illegal ServiceType %s"

    new-array v4, v5, [Ljava/lang/Object;

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    const v0, 0x186051b4

    invoke-static {v0, v1}, LX/02F;->d(II)V

    throw v2

    .line 1925666
    :catch_0
    move-exception v0

    .line 1925667
    sget-object v2, Lcom/facebook/push/registration/RegistrarHelperService;->e:Ljava/lang/Class;

    const-string v3, "serviceTypeString is null"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1925668
    const v0, -0x648562d1

    invoke-static {v0, v1}, LX/02F;->d(II)V

    .line 1925669
    :goto_0
    return-void

    .line 1925670
    :catch_1
    move-exception v2

    .line 1925671
    sget-object v3, Lcom/facebook/push/registration/RegistrarHelperService;->e:Ljava/lang/Class;

    const-string v4, "Failed to convert serviceType=%s"

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v0, v5, v6

    invoke-static {v3, v2, v4, v5}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1925672
    const v0, 0x7b037ced

    invoke-static {v0, v1}, LX/02F;->d(II)V

    goto :goto_0

    .line 1925673
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/push/registration/RegistrarHelperService;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Gh;

    .line 1925674
    :goto_1
    invoke-interface {v0}, LX/2Gh;->b()V

    .line 1925675
    const v0, 0x143735a4

    invoke-static {v0, v1}, LX/02F;->d(II)V

    goto :goto_0

    .line 1925676
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/push/registration/RegistrarHelperService;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Gh;

    goto :goto_1

    .line 1925677
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/push/registration/RegistrarHelperService;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Gh;

    goto :goto_1

    .line 1925678
    :pswitch_3
    iget-object v0, p0, Lcom/facebook/push/registration/RegistrarHelperService;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Gh;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x59c6b11a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1925679
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 1925680
    invoke-static {p0}, LX/1mU;->a(Landroid/content/Context;)V

    .line 1925681
    invoke-static {p0, p0}, Lcom/facebook/push/registration/RegistrarHelperService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1925682
    const/16 v1, 0x25

    const v2, 0x3f66d44a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
