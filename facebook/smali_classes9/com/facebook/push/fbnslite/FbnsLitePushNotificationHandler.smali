.class public Lcom/facebook/push/fbnslite/FbnsLitePushNotificationHandler;
.super LX/0IB;
.source ""


# static fields
.field private static final h:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/2Gs;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/2bC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/2Gq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/push/registration/FacebookPushServerRegistrar;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/2Gn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/07y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/2bD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1925270
    const-class v0, Lcom/facebook/push/fbnslite/FbnsLitePushNotificationHandler;

    sput-object v0, Lcom/facebook/push/fbnslite/FbnsLitePushNotificationHandler;->h:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1925253
    const-class v0, Lcom/facebook/push/fbnslite/FbnsLitePushNotificationHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/0IB;-><init>(Ljava/lang/String;)V

    .line 1925254
    return-void
.end method

.method private a()Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 1925269
    sget-object v0, LX/01p;->f:LX/01q;

    invoke-static {p0, v0}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/push/fbnslite/FbnsLitePushNotificationHandler;LX/2Gs;LX/2bC;LX/2Gq;Lcom/facebook/push/registration/FacebookPushServerRegistrar;LX/2Gn;LX/07y;LX/2bD;)V
    .locals 0

    .prologue
    .line 1925268
    iput-object p1, p0, Lcom/facebook/push/fbnslite/FbnsLitePushNotificationHandler;->a:LX/2Gs;

    iput-object p2, p0, Lcom/facebook/push/fbnslite/FbnsLitePushNotificationHandler;->b:LX/2bC;

    iput-object p3, p0, Lcom/facebook/push/fbnslite/FbnsLitePushNotificationHandler;->c:LX/2Gq;

    iput-object p4, p0, Lcom/facebook/push/fbnslite/FbnsLitePushNotificationHandler;->d:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    iput-object p5, p0, Lcom/facebook/push/fbnslite/FbnsLitePushNotificationHandler;->e:LX/2Gn;

    iput-object p6, p0, Lcom/facebook/push/fbnslite/FbnsLitePushNotificationHandler;->f:LX/07y;

    iput-object p7, p0, Lcom/facebook/push/fbnslite/FbnsLitePushNotificationHandler;->g:LX/2bD;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v0, p0

    check-cast v0, Lcom/facebook/push/fbnslite/FbnsLitePushNotificationHandler;

    invoke-static {v7}, LX/2Gs;->a(LX/0QB;)LX/2Gs;

    move-result-object v1

    check-cast v1, LX/2Gs;

    invoke-static {v7}, LX/2bC;->a(LX/0QB;)LX/2bC;

    move-result-object v2

    check-cast v2, LX/2bC;

    invoke-static {v7}, LX/2Gq;->a(LX/0QB;)LX/2Gq;

    move-result-object v3

    check-cast v3, LX/2Gq;

    invoke-static {v7}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/0QB;)Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    move-result-object v4

    check-cast v4, Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    invoke-static {v7}, LX/2Gn;->a(LX/0QB;)LX/2Gn;

    move-result-object v5

    check-cast v5, LX/2Gn;

    invoke-static {v7}, LX/07y;->a(LX/0QB;)LX/07y;

    move-result-object v6

    check-cast v6, LX/07y;

    invoke-static {v7}, LX/2bD;->a(LX/0QB;)LX/2bD;

    move-result-object v7

    check-cast v7, LX/2bD;

    invoke-static/range {v0 .. v7}, Lcom/facebook/push/fbnslite/FbnsLitePushNotificationHandler;->a(Lcom/facebook/push/fbnslite/FbnsLitePushNotificationHandler;LX/2Gs;LX/2bC;LX/2Gq;Lcom/facebook/push/registration/FacebookPushServerRegistrar;LX/2Gn;LX/07y;LX/2bD;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 6

    .prologue
    .line 1925271
    const-string v0, "data"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1925272
    const-string v0, "extra_notification_sender"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1925273
    const-string v0, "extra_notification_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1925274
    iget-object v0, p0, Lcom/facebook/push/fbnslite/FbnsLitePushNotificationHandler;->g:LX/2bD;

    sget-object v3, LX/3B4;->FBNS_LITE:LX/3B4;

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, LX/2bD;->a(Landroid/content/Context;Ljava/lang/String;LX/3B4;Ljava/lang/String;Ljava/lang/String;)V

    .line 1925275
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1925262
    iget-object v0, p0, Lcom/facebook/push/fbnslite/FbnsLitePushNotificationHandler;->c:LX/2Gq;

    sget-object v1, LX/2Ge;->FBNS_LITE:LX/2Ge;

    invoke-virtual {v0, v1}, LX/2Gq;->a(LX/2Ge;)LX/2H0;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/2H0;->a(Ljava/lang/String;)V

    .line 1925263
    iget-object v0, p0, Lcom/facebook/push/fbnslite/FbnsLitePushNotificationHandler;->d:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    sget-object v1, LX/2Ge;->FBNS_LITE:LX/2Ge;

    iget-object v2, p0, Lcom/facebook/push/fbnslite/FbnsLitePushNotificationHandler;->e:LX/2Gn;

    iget-object v2, v2, LX/2Gn;->a:LX/2H7;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;LX/2H7;)V

    .line 1925264
    iget-object v0, p0, Lcom/facebook/push/fbnslite/FbnsLitePushNotificationHandler;->e:LX/2Gn;

    invoke-virtual {v0}, LX/2Gn;->e()V

    .line 1925265
    invoke-direct {p0}, Lcom/facebook/push/fbnslite/FbnsLitePushNotificationHandler;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "register_and_stop"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, LX/01r;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 1925266
    iget-object v0, p0, Lcom/facebook/push/fbnslite/FbnsLitePushNotificationHandler;->f:LX/07y;

    invoke-virtual {v0}, LX/07y;->a()V

    .line 1925267
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 7
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1925260
    iget-object v0, p0, Lcom/facebook/push/fbnslite/FbnsLitePushNotificationHandler;->b:LX/2bC;

    sget-object v1, LX/3B4;->FBNS_LITE:LX/3B4;

    invoke-virtual {v1}, LX/3B4;->name()Ljava/lang/String;

    move-result-object v1

    const-string v5, ""

    const-string v6, ""

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v6}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 1925261
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1925255
    iget-object v0, p0, Lcom/facebook/push/fbnslite/FbnsLitePushNotificationHandler;->a:LX/2Gs;

    sget-object v1, LX/2Ge;->FBNS_LITE:LX/2Ge;

    invoke-virtual {v1}, LX/2Ge;->name()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onRegistrationError"

    invoke-virtual {v0, v1, v2, p1}, LX/2Gs;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1925256
    iget-object v0, p0, Lcom/facebook/push/fbnslite/FbnsLitePushNotificationHandler;->e:LX/2Gn;

    .line 1925257
    iget-object v1, v0, LX/2Gn;->c:LX/2H4;

    .line 1925258
    invoke-static {v1}, LX/2H4;->f(LX/2H4;)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/2H4;->a(Landroid/app/PendingIntent;)V

    .line 1925259
    return-void
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x7e4218f0    # 6.449993E37f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1925249
    invoke-super {p0}, LX/0IB;->onCreate()V

    .line 1925250
    invoke-static {p0}, LX/1mU;->a(Landroid/content/Context;)V

    .line 1925251
    invoke-static {p0, p0}, Lcom/facebook/push/fbnslite/FbnsLitePushNotificationHandler;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1925252
    const/16 v1, 0x25

    const v2, 0x56b24b31

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
