.class public Lcom/facebook/push/crossapp/PackageRemovedReporterService;
.super LX/1ZN;
.source ""


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/Ces;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1925220
    const-class v0, Lcom/facebook/push/crossapp/PackageRemovedReporterService;

    sput-object v0, Lcom/facebook/push/crossapp/PackageRemovedReporterService;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1925242
    const-string v0, "DisableReceiverComponent"

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 1925243
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1925237
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.facebook.push.crossapp.REPORT_DELETION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/facebook/push/crossapp/PackageRemovedReporterService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "package_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "source"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1925238
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v0

    .line 1925239
    if-nez v0, :cond_0

    .line 1925240
    sget-object v0, Lcom/facebook/push/crossapp/PackageRemovedReporterService;->b:Ljava/lang/Class;

    const-string v1, "Service not found"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1925241
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/push/crossapp/PackageRemovedReporterService;

    invoke-static {v0}, LX/Ces;->a(LX/0QB;)LX/Ces;

    move-result-object v0

    check-cast v0, LX/Ces;

    iput-object v0, p0, Lcom/facebook/push/crossapp/PackageRemovedReporterService;->a:LX/Ces;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x24

    const v2, 0x583546f1

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1925225
    const-string v1, "com.facebook.push.crossapp.REPORT_DELETION"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1925226
    iget-object v1, p0, Lcom/facebook/push/crossapp/PackageRemovedReporterService;->a:LX/Ces;

    const-string v2, "package_name"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "source"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1925227
    iget-object v4, v1, LX/Ces;->f:LX/2Rl;

    .line 1925228
    iget-object v5, v4, LX/2Rl;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object p0

    sget-object v5, LX/2Rl;->b:LX/0Tn;

    invoke-virtual {v5, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v5

    check-cast v5, LX/0Tn;

    const/4 p1, 0x1

    invoke-interface {p0, v5, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v5

    invoke-interface {v5}, LX/0hN;->commit()V

    .line 1925229
    new-instance v4, Lcom/facebook/push/fbpushtoken/ReportAppDeletionParams;

    iget-object v5, v1, LX/Ces;->d:LX/0dC;

    invoke-virtual {v5}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v2, v5}, Lcom/facebook/push/fbpushtoken/ReportAppDeletionParams;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1925230
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 1925231
    const-string p0, "reportAppDeletionParams"

    invoke-virtual {v5, p0, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1925232
    iget-object v4, v1, LX/Ces;->c:Ljava/util/concurrent/ExecutorService;

    new-instance p0, Lcom/facebook/push/crossapp/PackageRemovedReporter$1;

    invoke-direct {p0, v1, v5, v2, v3}, Lcom/facebook/push/crossapp/PackageRemovedReporter$1;-><init>(LX/Ces;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    const v5, 0x13774282

    invoke-static {v4, p0, v5}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/Callable;I)Ljava/util/concurrent/Future;

    move-result-object v4

    move-object v1, v4

    .line 1925233
    const v2, -0x37d50328

    :try_start_0
    invoke-static {v1, v2}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1925234
    :cond_0
    :goto_0
    const v1, 0x4e3335b5    # 7.5166035E8f

    invoke-static {v1, v0}, LX/02F;->d(II)V

    return-void

    .line 1925235
    :catch_0
    move-exception v1

    .line 1925236
    sget-object v2, Lcom/facebook/push/crossapp/PackageRemovedReporterService;->b:Ljava/lang/Class;

    const-string v3, ""

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x486e5f6f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1925221
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 1925222
    invoke-static {p0}, LX/1mU;->a(Landroid/content/Context;)V

    .line 1925223
    invoke-static {p0, p0}, Lcom/facebook/push/crossapp/PackageRemovedReporterService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1925224
    const/16 v1, 0x25

    const v2, 0x43e00b3e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
