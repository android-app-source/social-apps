.class public final Lcom/facebook/push/crossapp/PackageRemovedReporter$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/os/Bundle;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/Ces;


# direct methods
.method public constructor <init>(LX/Ces;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1925190
    iput-object p1, p0, Lcom/facebook/push/crossapp/PackageRemovedReporter$1;->d:LX/Ces;

    iput-object p2, p0, Lcom/facebook/push/crossapp/PackageRemovedReporter$1;->a:Landroid/os/Bundle;

    iput-object p3, p0, Lcom/facebook/push/crossapp/PackageRemovedReporter$1;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/facebook/push/crossapp/PackageRemovedReporter$1;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1925191
    iget-object v0, p0, Lcom/facebook/push/crossapp/PackageRemovedReporter$1;->d:LX/Ces;

    iget-object v0, v0, LX/Ces;->b:LX/0aG;

    const-string v1, "report_app_deletion"

    iget-object v2, p0, Lcom/facebook/push/crossapp/PackageRemovedReporter$1;->a:Landroid/os/Bundle;

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v4, LX/Ces;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, 0x469bbd81

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 1925192
    const v1, 0x5608053

    :try_start_0
    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    .line 1925193
    iget-object v0, p0, Lcom/facebook/push/crossapp/PackageRemovedReporter$1;->d:LX/Ces;

    iget-object v0, v0, LX/Ces;->f:LX/2Rl;

    iget-object v1, p0, Lcom/facebook/push/crossapp/PackageRemovedReporter$1;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2Rl;->b(Ljava/lang/String;)V

    .line 1925194
    iget-object v0, p0, Lcom/facebook/push/crossapp/PackageRemovedReporter$1;->d:LX/Ces;

    iget-object v0, v0, LX/Ces;->e:LX/2Gs;

    iget-object v1, p0, Lcom/facebook/push/crossapp/PackageRemovedReporter$1;->b:Ljava/lang/String;

    sget-object v2, LX/Cem;->SUCCESS:LX/Cem;

    invoke-virtual {v2}, LX/Cem;->name()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/push/crossapp/PackageRemovedReporter$1;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LX/2Gs;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1925195
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 1925196
    :catch_0
    move-exception v0

    .line 1925197
    sget-object v1, LX/Ces;->a:Ljava/lang/Class;

    const-string v2, "Report package:%s failed"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/push/crossapp/PackageRemovedReporter$1;->b:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1925198
    iget-object v0, p0, Lcom/facebook/push/crossapp/PackageRemovedReporter$1;->d:LX/Ces;

    iget-object v0, v0, LX/Ces;->e:LX/2Gs;

    iget-object v1, p0, Lcom/facebook/push/crossapp/PackageRemovedReporter$1;->b:Ljava/lang/String;

    sget-object v2, LX/Cem;->FAILED:LX/Cem;

    invoke-virtual {v2}, LX/Cem;->name()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/push/crossapp/PackageRemovedReporter$1;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LX/2Gs;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
