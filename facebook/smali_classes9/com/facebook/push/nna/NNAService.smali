.class public Lcom/facebook/push/nna/NNAService;
.super LX/1ZN;
.source ""


# static fields
.field private static final g:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/Cf6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Cf1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/Cf0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/2bD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1925558
    const-class v0, Lcom/facebook/push/nna/NNAService;

    sput-object v0, Lcom/facebook/push/nna/NNAService;->g:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1925556
    const-string v0, "NNAReceiver"

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 1925557
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 1925550
    iget-object v0, p0, Lcom/facebook/push/nna/NNAService;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 1925551
    iget-object v1, p0, Lcom/facebook/push/nna/NNAService;->e:LX/Cf0;

    .line 1925552
    iget-object v2, v1, LX/2H3;->g:LX/0Tn;

    move-object v1, v2

    .line 1925553
    iget-object v2, p0, Lcom/facebook/push/nna/NNAService;->d:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    .line 1925554
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1925555
    return-void
.end method

.method private static a(Lcom/facebook/push/nna/NNAService;LX/Cf6;LX/Cf1;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/Cf0;LX/2bD;)V
    .locals 0

    .prologue
    .line 1925549
    iput-object p1, p0, Lcom/facebook/push/nna/NNAService;->a:LX/Cf6;

    iput-object p2, p0, Lcom/facebook/push/nna/NNAService;->b:LX/Cf1;

    iput-object p3, p0, Lcom/facebook/push/nna/NNAService;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p4, p0, Lcom/facebook/push/nna/NNAService;->d:LX/0SG;

    iput-object p5, p0, Lcom/facebook/push/nna/NNAService;->e:LX/Cf0;

    iput-object p6, p0, Lcom/facebook/push/nna/NNAService;->f:LX/2bD;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/push/nna/NNAService;

    invoke-static {v6}, LX/Cf6;->a(LX/0QB;)LX/Cf6;

    move-result-object v1

    check-cast v1, LX/Cf6;

    invoke-static {v6}, LX/Cf1;->a(LX/0QB;)LX/Cf1;

    move-result-object v2

    check-cast v2, LX/Cf1;

    invoke-static {v6}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v6}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v6}, LX/Cf0;->a(LX/0QB;)LX/Cf0;

    move-result-object v5

    check-cast v5, LX/Cf0;

    invoke-static {v6}, LX/2bD;->a(LX/0QB;)LX/2bD;

    move-result-object v6

    check-cast v6, LX/2bD;

    invoke-static/range {v0 .. v6}, Lcom/facebook/push/nna/NNAService;->a(Lcom/facebook/push/nna/NNAService;LX/Cf6;LX/Cf1;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/Cf0;LX/2bD;)V

    return-void
.end method

.method private b(Landroid/content/Intent;)V
    .locals 7

    .prologue
    .line 1925523
    const-string v0, "error"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1925524
    const-string v0, "registration_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1925525
    const-string v0, "unregistered"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1925526
    const/4 v3, 0x3

    invoke-static {v3}, LX/01m;->b(I)Z

    .line 1925527
    iget-object v3, p0, Lcom/facebook/push/nna/NNAService;->a:LX/Cf6;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const/4 p1, 0x0

    .line 1925528
    if-eqz v0, :cond_1

    .line 1925529
    iget-object v4, v3, LX/Cf6;->i:LX/2H0;

    invoke-virtual {v4}, LX/2H0;->h()V

    .line 1925530
    iget-object v4, v3, LX/Cf6;->k:LX/2H4;

    sget-object v5, LX/Cem;->SUCCESS:LX/Cem;

    invoke-virtual {v5}, LX/Cem;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, p1}, LX/2H4;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1925531
    :goto_1
    return-void

    .line 1925532
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1925533
    :cond_1
    iget-object v4, v3, LX/Cf6;->k:LX/2H4;

    invoke-virtual {v4}, LX/2H4;->c()V

    .line 1925534
    if-eqz v1, :cond_3

    .line 1925535
    iget-object v4, v3, LX/Cf6;->i:LX/2H0;

    invoke-virtual {v4}, LX/2H0;->h()V

    .line 1925536
    sget-object v4, LX/Cf6;->b:Ljava/lang/Class;

    const-string v5, "Registration error %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 p0, 0x0

    aput-object v1, v6, p0

    invoke-static {v4, v5, v6}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1925537
    const-string v4, "SERVICE_NOT_AVAILABLE"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1925538
    iget-object v4, v3, LX/Cf6;->d:Landroid/content/Context;

    const-string v5, "connectivity"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/ConnectivityManager;

    .line 1925539
    invoke-virtual {v4}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v4

    .line 1925540
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1925541
    sget-object v4, LX/Cf5;->RETRY:LX/Cf5;

    invoke-static {v3, v4}, LX/Cf6;->a(LX/Cf6;LX/Cf5;)Landroid/content/Intent;

    move-result-object v4

    .line 1925542
    const-string v5, "app"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/app/PendingIntent;

    .line 1925543
    iget-object v5, v3, LX/Cf6;->k:LX/2H4;

    invoke-virtual {v5, v4}, LX/2H4;->a(Landroid/app/PendingIntent;)V

    .line 1925544
    :cond_2
    iget-object v4, v3, LX/Cf6;->k:LX/2H4;

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, p1}, LX/2H4;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1925545
    :cond_3
    iget-object v4, v3, LX/Cf6;->i:LX/2H0;

    invoke-virtual {v4, v2}, LX/2H0;->a(Ljava/lang/String;)V

    .line 1925546
    iget-object v4, v3, LX/Cf6;->k:LX/2H4;

    sget-object v5, LX/2gP;->SUCCESS:LX/2gP;

    invoke-virtual {v5}, LX/2gP;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, p1}, LX/2H4;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1925547
    iget-object v4, v3, LX/Cf6;->k:LX/2H4;

    invoke-virtual {v4}, LX/2H4;->d()V

    .line 1925548
    iget-object v4, v3, LX/Cf6;->h:Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    sget-object v5, LX/2Ge;->NNA:LX/2Ge;

    iget-object v6, v3, LX/Cf6;->a:LX/2H7;

    invoke-virtual {v4, v5, v6}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;LX/2H7;)V

    goto :goto_1
.end method

.method private c(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 1925514
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1925515
    if-eqz v0, :cond_0

    .line 1925516
    invoke-direct {p0}, Lcom/facebook/push/nna/NNAService;->a()V

    .line 1925517
    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1925518
    if-eqz v0, :cond_1

    .line 1925519
    invoke-direct {p0}, Lcom/facebook/push/nna/NNAService;->a()V

    .line 1925520
    iget-object v1, p0, Lcom/facebook/push/nna/NNAService;->f:LX/2bD;

    sget-object v2, LX/3B4;->NNA:LX/3B4;

    invoke-virtual {v1, p0, v0, v2}, LX/2bD;->a(Landroid/content/Context;Ljava/lang/String;LX/3B4;)V

    .line 1925521
    :cond_0
    :goto_0
    return-void

    .line 1925522
    :cond_1
    sget-object v0, Lcom/facebook/push/nna/NNAService;->g:Ljava/lang/Class;

    const-string v1, "NNA payload missing or null"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x1b7a2dd8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1925500
    invoke-static {p0}, LX/1mU;->a(Landroid/content/Context;)V

    .line 1925501
    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-nez v0, :cond_1

    .line 1925502
    :cond_0
    iget-object v0, p0, Lcom/facebook/push/nna/NNAService;->b:LX/Cf1;

    iget-object v0, v0, LX/1qk;->a:LX/1ql;

    invoke-virtual {v0}, LX/1ql;->d()V

    const/16 v0, 0x25

    const v2, 0x4038ae18

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1925503
    :goto_0
    return-void

    .line 1925504
    :cond_1
    :try_start_1
    const-string v0, "com.nokia.pushnotifications.intent.REGISTRATION"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1925505
    invoke-direct {p0, p1}, Lcom/facebook/push/nna/NNAService;->b(Landroid/content/Intent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1925506
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/facebook/push/nna/NNAService;->b:LX/Cf1;

    iget-object v0, v0, LX/1qk;->a:LX/1ql;

    invoke-virtual {v0}, LX/1ql;->d()V

    .line 1925507
    const v0, 0x3be2401f

    invoke-static {v0, v1}, LX/02F;->d(II)V

    goto :goto_0

    .line 1925508
    :cond_3
    :try_start_2
    const-string v0, "com.nokia.pushnotifications.intent.RECEIVE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1925509
    invoke-direct {p0, p1}, Lcom/facebook/push/nna/NNAService;->c(Landroid/content/Intent;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 1925510
    :catchall_0
    move-exception v0

    iget-object v2, p0, Lcom/facebook/push/nna/NNAService;->b:LX/Cf1;

    iget-object v2, v2, LX/1qk;->a:LX/1ql;

    invoke-virtual {v2}, LX/1ql;->d()V

    const v2, -0x5e94df47

    invoke-static {v2, v1}, LX/02F;->d(II)V

    throw v0
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x7c6a79a6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1925511
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 1925512
    invoke-static {p0, p0}, Lcom/facebook/push/nna/NNAService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1925513
    const/16 v1, 0x25

    const v2, -0x968da12

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
