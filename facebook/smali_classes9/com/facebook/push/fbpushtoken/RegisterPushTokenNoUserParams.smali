.class public Lcom/facebook/push/fbpushtoken/RegisterPushTokenNoUserParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/push/fbpushtoken/RegisterPushTokenNoUserParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1925333
    new-instance v0, LX/Cew;

    invoke-direct {v0}, LX/Cew;-><init>()V

    sput-object v0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenNoUserParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1925334
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1925335
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenNoUserParams;->a:Ljava/lang/String;

    .line 1925336
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenNoUserParams;->b:Ljava/lang/String;

    .line 1925337
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenNoUserParams;->c:Ljava/lang/String;

    .line 1925338
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenNoUserParams;->d:Ljava/lang/String;

    .line 1925339
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1925327
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1925328
    iput-object p1, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenNoUserParams;->a:Ljava/lang/String;

    .line 1925329
    iput-object p2, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenNoUserParams;->b:Ljava/lang/String;

    .line 1925330
    iput-object p3, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenNoUserParams;->c:Ljava/lang/String;

    .line 1925331
    iput-object p4, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenNoUserParams;->d:Ljava/lang/String;

    .line 1925332
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1925326
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1925321
    iget-object v0, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenNoUserParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1925322
    iget-object v0, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenNoUserParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1925323
    iget-object v0, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenNoUserParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1925324
    iget-object v0, p0, Lcom/facebook/push/fbpushtoken/RegisterPushTokenNoUserParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1925325
    return-void
.end method
