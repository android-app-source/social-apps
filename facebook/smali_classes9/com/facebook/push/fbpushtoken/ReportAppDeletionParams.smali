.class public Lcom/facebook/push/fbpushtoken/ReportAppDeletionParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/push/fbpushtoken/ReportAppDeletionParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1925343
    new-instance v0, LX/Cex;

    invoke-direct {v0}, LX/Cex;-><init>()V

    sput-object v0, Lcom/facebook/push/fbpushtoken/ReportAppDeletionParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1925344
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1925345
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/push/fbpushtoken/ReportAppDeletionParams;->a:Ljava/lang/String;

    .line 1925346
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/push/fbpushtoken/ReportAppDeletionParams;->b:Ljava/lang/String;

    .line 1925347
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1925348
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1925349
    iput-object p1, p0, Lcom/facebook/push/fbpushtoken/ReportAppDeletionParams;->a:Ljava/lang/String;

    .line 1925350
    iput-object p2, p0, Lcom/facebook/push/fbpushtoken/ReportAppDeletionParams;->b:Ljava/lang/String;

    .line 1925351
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1925352
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1925353
    iget-object v0, p0, Lcom/facebook/push/fbpushtoken/ReportAppDeletionParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1925354
    iget-object v0, p0, Lcom/facebook/push/fbpushtoken/ReportAppDeletionParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1925355
    return-void
.end method
