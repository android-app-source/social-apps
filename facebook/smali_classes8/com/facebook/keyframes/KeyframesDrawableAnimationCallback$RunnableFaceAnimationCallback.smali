.class public final Lcom/facebook/keyframes/KeyframesDrawableAnimationCallback$RunnableFaceAnimationCallback;
.super LX/9Uh;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Landroid/os/Handler;


# direct methods
.method public constructor <init>(LX/9Uf;II)V
    .locals 2

    .prologue
    .line 1498491
    invoke-direct {p0, p1, p2, p3}, LX/9Uh;-><init>(LX/9Uf;II)V

    .line 1498492
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/facebook/keyframes/KeyframesDrawableAnimationCallback$RunnableFaceAnimationCallback;->a:Landroid/os/Handler;

    .line 1498493
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1498494
    iget-object v0, p0, Lcom/facebook/keyframes/KeyframesDrawableAnimationCallback$RunnableFaceAnimationCallback;->a:Landroid/os/Handler;

    const-wide/16 v2, 0x19

    const v1, 0x709027a0

    invoke-static {v0, p0, v2, v3, v1}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1498495
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1498496
    iget-object v0, p0, Lcom/facebook/keyframes/KeyframesDrawableAnimationCallback$RunnableFaceAnimationCallback;->a:Landroid/os/Handler;

    invoke-static {v0, p0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1498497
    return-void
.end method

.method public final run()V
    .locals 2

    .prologue
    .line 1498498
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LX/9Uh;->a(J)V

    .line 1498499
    return-void
.end method
