.class public final Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceInjectMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x489484f6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceInjectMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceInjectMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceInjectMutationModel$ViewerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1643712
    const-class v0, Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceInjectMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1643711
    const-class v0, Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceInjectMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1643709
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1643710
    return-void
.end method

.method private a()Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceInjectMutationModel$ViewerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1643707
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceInjectMutationModel;->f:Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceInjectMutationModel$ViewerModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceInjectMutationModel$ViewerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceInjectMutationModel$ViewerModel;

    iput-object v0, p0, Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceInjectMutationModel;->f:Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceInjectMutationModel$ViewerModel;

    .line 1643708
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceInjectMutationModel;->f:Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceInjectMutationModel$ViewerModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1643713
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1643714
    invoke-direct {p0}, Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceInjectMutationModel;->a()Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceInjectMutationModel$ViewerModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1643715
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1643716
    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceInjectMutationModel;->e:Z

    invoke-virtual {p1, v1, v2}, LX/186;->a(IZ)V

    .line 1643717
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1643718
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1643719
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1643699
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1643700
    invoke-direct {p0}, Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceInjectMutationModel;->a()Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceInjectMutationModel$ViewerModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1643701
    invoke-direct {p0}, Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceInjectMutationModel;->a()Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceInjectMutationModel$ViewerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceInjectMutationModel$ViewerModel;

    .line 1643702
    invoke-direct {p0}, Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceInjectMutationModel;->a()Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceInjectMutationModel$ViewerModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1643703
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceInjectMutationModel;

    .line 1643704
    iput-object v0, v1, Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceInjectMutationModel;->f:Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceInjectMutationModel$ViewerModel;

    .line 1643705
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1643706
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1643696
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1643697
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceInjectMutationModel;->e:Z

    .line 1643698
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1643693
    new-instance v0, Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceInjectMutationModel;

    invoke-direct {v0}, Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceInjectMutationModel;-><init>()V

    .line 1643694
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1643695
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1643692
    const v0, -0x3d400c7d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1643691
    const v0, 0x560ea642

    return v0
.end method
