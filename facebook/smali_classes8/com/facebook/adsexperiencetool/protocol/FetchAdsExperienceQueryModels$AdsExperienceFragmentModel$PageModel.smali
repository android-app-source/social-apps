.class public final Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x114411ef
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1644252
    const-class v0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1644251
    const-class v0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1644249
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1644250
    return-void
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1644247
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;->e:Ljava/lang/String;

    .line 1644248
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1644237
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1644238
    invoke-direct {p0}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1644239
    invoke-virtual {p0}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1644240
    invoke-virtual {p0}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;->k()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, 0x2aff05c6

    invoke-static {v3, v2, v4}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1644241
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1644242
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1644243
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1644244
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1644245
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1644246
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1644227
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1644228
    invoke-virtual {p0}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1644229
    invoke-virtual {p0}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x2aff05c6

    invoke-static {v2, v0, v3}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1644230
    invoke-virtual {p0}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1644231
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;

    .line 1644232
    iput v3, v0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;->g:I

    .line 1644233
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1644234
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 1644235
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 1644236
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1644226
    new-instance v0, LX/ADH;

    invoke-direct {v0, p1}, LX/ADH;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1644225
    invoke-direct {p0}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1644253
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1644254
    const/4 v0, 0x2

    const v1, 0x2aff05c6

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;->g:I

    .line 1644255
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1644223
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1644224
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1644222
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1644219
    new-instance v0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;

    invoke-direct {v0}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;-><init>()V

    .line 1644220
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1644221
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1644218
    const v0, 0x531357dc

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1644217
    const v0, 0x25d6af

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1644215
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;->f:Ljava/lang/String;

    .line 1644216
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfilePicture"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1644213
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1644214
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
