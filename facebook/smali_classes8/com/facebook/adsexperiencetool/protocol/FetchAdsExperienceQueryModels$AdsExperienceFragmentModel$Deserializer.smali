.class public final Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1644150
    const-class v0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;

    new-instance v1, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1644151
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1644152
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 1644153
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1644154
    const/4 v2, 0x0

    .line 1644155
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_9

    .line 1644156
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1644157
    :goto_0
    move v1, v2

    .line 1644158
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1644159
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1644160
    new-instance v1, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;

    invoke-direct {v1}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;-><init>()V

    .line 1644161
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1644162
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1644163
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1644164
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1644165
    :cond_0
    return-object v1

    .line 1644166
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1644167
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, p0, :cond_8

    .line 1644168
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1644169
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1644170
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v8, :cond_2

    .line 1644171
    const-string p0, "ad_preview_id"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1644172
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 1644173
    :cond_3
    const-string p0, "ad_shared_by"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1644174
    invoke-static {p1, v0}, LX/ADJ;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1644175
    :cond_4
    const-string p0, "ad_sharing_status"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1644176
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/graphql/enums/GraphQLAdsExperienceStatusEnum;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAdsExperienceStatusEnum;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    goto :goto_1

    .line 1644177
    :cond_5
    const-string p0, "adgroup"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1644178
    invoke-static {p1, v0}, LX/ADK;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1644179
    :cond_6
    const-string p0, "id"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 1644180
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1644181
    :cond_7
    const-string p0, "page"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1644182
    invoke-static {p1, v0}, LX/ADL;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_1

    .line 1644183
    :cond_8
    const/4 v8, 0x6

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 1644184
    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1644185
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1644186
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1644187
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1644188
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1644189
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1644190
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_9
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    goto/16 :goto_1
.end method
