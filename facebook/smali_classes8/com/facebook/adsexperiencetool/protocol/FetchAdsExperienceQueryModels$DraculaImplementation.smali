.class public final Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1644394
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1644395
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1644392
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1644393
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 1644378
    if-nez p1, :cond_0

    .line 1644379
    :goto_0
    return v0

    .line 1644380
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1644381
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1644382
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1644383
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1644384
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1644385
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1644386
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1644387
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1644388
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1644389
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1644390
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1644391
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x156cfe73 -> :sswitch_0
        0x2aff05c6 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1644377
    new-instance v0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1644374
    sparse-switch p0, :sswitch_data_0

    .line 1644375
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1644376
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x156cfe73 -> :sswitch_0
        0x2aff05c6 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1644373
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1644371
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$DraculaImplementation;->b(I)V

    .line 1644372
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1644396
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1644397
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1644398
    :cond_0
    iput-object p1, p0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$DraculaImplementation;->a:LX/15i;

    .line 1644399
    iput p2, p0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$DraculaImplementation;->b:I

    .line 1644400
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1644345
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1644370
    new-instance v0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1644367
    iget v0, p0, LX/1vt;->c:I

    .line 1644368
    move v0, v0

    .line 1644369
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1644364
    iget v0, p0, LX/1vt;->c:I

    .line 1644365
    move v0, v0

    .line 1644366
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1644361
    iget v0, p0, LX/1vt;->b:I

    .line 1644362
    move v0, v0

    .line 1644363
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1644358
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1644359
    move-object v0, v0

    .line 1644360
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1644349
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1644350
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1644351
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1644352
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1644353
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1644354
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1644355
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1644356
    invoke-static {v3, v9, v2}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1644357
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1644346
    iget v0, p0, LX/1vt;->c:I

    .line 1644347
    move v0, v0

    .line 1644348
    return v0
.end method
