.class public final Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceRemoveMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5c48d5fb
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceRemoveMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceRemoveMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceRemoveMutationModel$ViewerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1643878
    const-class v0, Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceRemoveMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1643877
    const-class v0, Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceRemoveMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1643875
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1643876
    return-void
.end method

.method private a()Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceRemoveMutationModel$ViewerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1643879
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceRemoveMutationModel;->e:Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceRemoveMutationModel$ViewerModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceRemoveMutationModel$ViewerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceRemoveMutationModel$ViewerModel;

    iput-object v0, p0, Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceRemoveMutationModel;->e:Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceRemoveMutationModel$ViewerModel;

    .line 1643880
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceRemoveMutationModel;->e:Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceRemoveMutationModel$ViewerModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1643869
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1643870
    invoke-direct {p0}, Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceRemoveMutationModel;->a()Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceRemoveMutationModel$ViewerModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1643871
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1643872
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1643873
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1643874
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1643861
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1643862
    invoke-direct {p0}, Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceRemoveMutationModel;->a()Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceRemoveMutationModel$ViewerModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1643863
    invoke-direct {p0}, Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceRemoveMutationModel;->a()Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceRemoveMutationModel$ViewerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceRemoveMutationModel$ViewerModel;

    .line 1643864
    invoke-direct {p0}, Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceRemoveMutationModel;->a()Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceRemoveMutationModel$ViewerModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1643865
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceRemoveMutationModel;

    .line 1643866
    iput-object v0, v1, Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceRemoveMutationModel;->e:Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceRemoveMutationModel$ViewerModel;

    .line 1643867
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1643868
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1643858
    new-instance v0, Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceRemoveMutationModel;

    invoke-direct {v0}, Lcom/facebook/adsexperiencetool/protocol/AdsExperienceMutationsModels$AdsExperienceRemoveMutationModel;-><init>()V

    .line 1643859
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1643860
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1643857
    const v0, 0x4a0be85f    # 2292247.8f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1643856
    const v0, 0x7e8032ef

    return v0
.end method
