.class public final Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1644287
    const-class v0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;

    new-instance v1, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1644288
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1644256
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1644258
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1644259
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x2

    .line 1644260
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1644261
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1644262
    if-eqz v2, :cond_0

    .line 1644263
    const-string v3, "ad_preview_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1644264
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1644265
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1644266
    if-eqz v2, :cond_1

    .line 1644267
    const-string v3, "ad_shared_by"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1644268
    invoke-static {v1, v2, p1, p2}, LX/ADJ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1644269
    :cond_1
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1644270
    if-eqz v2, :cond_2

    .line 1644271
    const-string v2, "ad_sharing_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1644272
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1644273
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1644274
    if-eqz v2, :cond_3

    .line 1644275
    const-string v3, "adgroup"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1644276
    invoke-static {v1, v2, p1}, LX/ADK;->a(LX/15i;ILX/0nX;)V

    .line 1644277
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1644278
    if-eqz v2, :cond_4

    .line 1644279
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1644280
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1644281
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1644282
    if-eqz v2, :cond_5

    .line 1644283
    const-string v3, "page"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1644284
    invoke-static {v1, v2, p1, p2}, LX/ADL;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1644285
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1644286
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1644257
    check-cast p1, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$Serializer;->a(Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
