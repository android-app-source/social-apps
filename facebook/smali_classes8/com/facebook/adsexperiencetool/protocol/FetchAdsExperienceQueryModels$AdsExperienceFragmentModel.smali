.class public final Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7500860d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$AdSharedByModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLAdsExperienceStatusEnum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$AdgroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1644343
    const-class v0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1644342
    const-class v0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1644340
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1644341
    return-void
.end method

.method private n()Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$AdSharedByModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAdSharedBy"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1644338
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->f:Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$AdSharedByModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$AdSharedByModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$AdSharedByModel;

    iput-object v0, p0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->f:Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$AdSharedByModel;

    .line 1644339
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->f:Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$AdSharedByModel;

    return-object v0
.end method

.method private o()Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$AdgroupModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1644336
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->h:Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$AdgroupModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$AdgroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$AdgroupModel;

    iput-object v0, p0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->h:Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$AdgroupModel;

    .line 1644337
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->h:Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$AdgroupModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 1644320
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1644321
    invoke-virtual {p0}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1644322
    invoke-direct {p0}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->n()Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$AdSharedByModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1644323
    invoke-virtual {p0}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLAdsExperienceStatusEnum;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1644324
    invoke-direct {p0}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->o()Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$AdgroupModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1644325
    invoke-virtual {p0}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1644326
    invoke-virtual {p0}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->m()Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1644327
    const/4 v6, 0x6

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1644328
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 1644329
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1644330
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1644331
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1644332
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1644333
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1644334
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1644335
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1644302
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1644303
    invoke-direct {p0}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->n()Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$AdSharedByModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1644304
    invoke-direct {p0}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->n()Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$AdSharedByModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$AdSharedByModel;

    .line 1644305
    invoke-direct {p0}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->n()Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$AdSharedByModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1644306
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;

    .line 1644307
    iput-object v0, v1, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->f:Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$AdSharedByModel;

    .line 1644308
    :cond_0
    invoke-direct {p0}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->o()Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$AdgroupModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1644309
    invoke-direct {p0}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->o()Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$AdgroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$AdgroupModel;

    .line 1644310
    invoke-direct {p0}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->o()Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$AdgroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1644311
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;

    .line 1644312
    iput-object v0, v1, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->h:Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$AdgroupModel;

    .line 1644313
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->m()Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1644314
    invoke-virtual {p0}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->m()Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;

    .line 1644315
    invoke-virtual {p0}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->m()Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1644316
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;

    .line 1644317
    iput-object v0, v1, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->j:Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;

    .line 1644318
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1644319
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1644344
    invoke-virtual {p0}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1644299
    new-instance v0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;

    invoke-direct {v0}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;-><init>()V

    .line 1644300
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1644301
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1644298
    const v0, -0x578e3741

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1644297
    const v0, -0x1ae23de6

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1644295
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->e:Ljava/lang/String;

    .line 1644296
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/enums/GraphQLAdsExperienceStatusEnum;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1644293
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->g:Lcom/facebook/graphql/enums/GraphQLAdsExperienceStatusEnum;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLAdsExperienceStatusEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLAdsExperienceStatusEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAdsExperienceStatusEnum;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAdsExperienceStatusEnum;

    iput-object v0, p0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->g:Lcom/facebook/graphql/enums/GraphQLAdsExperienceStatusEnum;

    .line 1644294
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->g:Lcom/facebook/graphql/enums/GraphQLAdsExperienceStatusEnum;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1644291
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->i:Ljava/lang/String;

    .line 1644292
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1644289
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->j:Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;

    iput-object v0, p0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->j:Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;

    .line 1644290
    iget-object v0, p0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->j:Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;

    return-object v0
.end method
