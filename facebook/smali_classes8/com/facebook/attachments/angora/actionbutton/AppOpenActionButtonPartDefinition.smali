.class public Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/35p;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/AEE;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/1nA;

.field public final c:LX/2v5;

.field public final d:LX/2v6;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1nA;LX/2v5;LX/2v6;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1646184
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1646185
    iput-object p1, p0, Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;->a:Landroid/content/Context;

    .line 1646186
    iput-object p2, p0, Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;->b:LX/1nA;

    .line 1646187
    iput-object p3, p0, Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;->c:LX/2v5;

    .line 1646188
    iput-object p4, p0, Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;->d:LX/2v6;

    .line 1646189
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;
    .locals 7

    .prologue
    .line 1646231
    const-class v1, Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;

    monitor-enter v1

    .line 1646232
    :try_start_0
    sget-object v0, Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1646233
    sput-object v2, Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1646234
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1646235
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1646236
    new-instance p0, Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1nA;->a(LX/0QB;)LX/1nA;

    move-result-object v4

    check-cast v4, LX/1nA;

    invoke-static {v0}, LX/2v5;->b(LX/0QB;)LX/2v5;

    move-result-object v5

    check-cast v5, LX/2v5;

    invoke-static {v0}, LX/2v6;->a(LX/0QB;)LX/2v6;

    move-result-object v6

    check-cast v6, LX/2v6;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;-><init>(Landroid/content/Context;LX/1nA;LX/2v5;LX/2v6;)V

    .line 1646237
    move-object v0, p0

    .line 1646238
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1646239
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1646240
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1646241
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;)Lcom/facebook/drawee/drawable/AutoRotateDrawable;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1646220
    sget-object v0, LX/6RR;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1646221
    const/4 v0, 0x0

    :goto_0
    move v0, v0

    .line 1646222
    if-nez v0, :cond_0

    .line 1646223
    const/4 v0, 0x0

    .line 1646224
    :goto_1
    return-object v0

    :cond_0
    const/4 p1, 0x0

    .line 1646225
    iget-object v0, p0, Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;->a:Landroid/content/Context;

    const/high16 v1, 0x41a00000    # 20.0f

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    .line 1646226
    new-instance v1, Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    iget-object v2, p0, Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020994

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const/16 v3, 0x320

    invoke-direct {v1, v2, v3}, Lcom/facebook/drawee/drawable/AutoRotateDrawable;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 1646227
    invoke-virtual {v1, p1, p1, v0, v0}, Lcom/facebook/drawee/drawable/AutoRotateDrawable;->setBounds(IIII)V

    .line 1646228
    move-object v0, v1

    .line 1646229
    goto :goto_1

    .line 1646230
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1646242
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pq;

    const/4 v7, 0x1

    .line 1646243
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v0

    .line 1646244
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1646245
    const v0, -0x1e53800c

    invoke-static {v5, v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v3

    .line 1646246
    invoke-static {p2}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    move-object v0, p3

    .line 1646247
    check-cast v0, LX/1Pr;

    new-instance v2, LX/AEC;

    invoke-direct {v2, v5}, LX/AEC;-><init>(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    invoke-interface {v0, v2, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/AEE;

    .line 1646248
    iget-object v0, v2, LX/AEE;->a:LX/6Qd;

    move-object v0, v0

    .line 1646249
    if-nez v0, :cond_0

    .line 1646250
    new-instance v0, LX/AED;

    move-object v1, p0

    move-object v4, p3

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, LX/AED;-><init>(Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;LX/AEE;Lcom/facebook/graphql/model/GraphQLStoryActionLink;LX/1Pq;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1646251
    iput-object v0, v2, LX/AEE;->a:LX/6Qd;

    .line 1646252
    :cond_0
    iget-object v0, p0, Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;->d:LX/2v6;

    invoke-virtual {v0}, LX/2v6;->a()V

    .line 1646253
    iget-object v0, v2, LX/AEE;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1646254
    if-nez v0, :cond_1

    .line 1646255
    iget-object v0, p0, Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;->c:LX/2v5;

    invoke-static {v0, v5, v3}, LX/AEB;->a(LX/2v5;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Ljava/lang/String;

    move-result-object v0

    .line 1646256
    iput-object v0, v2, LX/AEE;->f:Ljava/lang/String;

    .line 1646257
    :cond_1
    iget-boolean v0, v2, LX/AEE;->c:Z

    move v0, v0

    .line 1646258
    if-nez v0, :cond_2

    invoke-static {v3}, LX/AEB;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1646259
    iput-boolean v7, v2, LX/AEE;->c:Z

    .line 1646260
    :cond_2
    iget-object v0, v2, LX/AEE;->d:Landroid/view/View$OnClickListener;

    move-object v0, v0

    .line 1646261
    if-nez v0, :cond_3

    .line 1646262
    iget-object v0, p0, Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;->b:LX/1nA;

    invoke-virtual {v0, p2, v3}, LX/1nA;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Landroid/view/View$OnClickListener;

    move-result-object v0

    .line 1646263
    iput-object v0, v2, LX/AEE;->d:Landroid/view/View$OnClickListener;

    .line 1646264
    :cond_3
    iget-object v0, v2, LX/AEE;->e:Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    move-object v0, v0

    .line 1646265
    if-nez v0, :cond_5

    .line 1646266
    const/4 v0, 0x0

    .line 1646267
    if-nez v5, :cond_8

    .line 1646268
    :cond_4
    :goto_0
    move-object v0, v0

    .line 1646269
    iput-object v0, v2, LX/AEE;->e:Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    .line 1646270
    :cond_5
    iget-object v0, v2, LX/AEE;->b:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    move-object v0, v0

    .line 1646271
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->PENDING:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    if-eq v0, v1, :cond_6

    .line 1646272
    iget-object v0, v2, LX/AEE;->b:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    move-object v0, v0

    .line 1646273
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->INSTALLING:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    if-ne v0, v1, :cond_7

    .line 1646274
    :cond_6
    const/4 v0, 0x0

    .line 1646275
    iput-boolean v0, v2, LX/AEE;->g:Z

    .line 1646276
    :goto_1
    check-cast p3, LX/1Pr;

    new-instance v0, LX/AEC;

    invoke-direct {v0, v5}, LX/AEC;-><init>(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    invoke-interface {p3, v0, v2}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 1646277
    return-object v2

    .line 1646278
    :cond_7
    iput-boolean v7, v2, LX/AEE;->g:Z

    .line 1646279
    goto :goto_1

    .line 1646280
    :cond_8
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->k()Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    move-result-object v1

    .line 1646281
    if-eqz v1, :cond_4

    .line 1646282
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->o()Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;->a$redex0(Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;)Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    move-result-object v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x4ecc5659

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1646197
    check-cast p2, LX/AEE;

    const/4 p3, 0x0

    const/4 p1, 0x0

    .line 1646198
    iget-object v1, p0, Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;->d:LX/2v6;

    .line 1646199
    iget-object v2, p2, LX/AEE;->a:LX/6Qd;

    move-object v2, v2

    .line 1646200
    invoke-virtual {v1, v2}, LX/2v6;->a(LX/6Qd;)V

    .line 1646201
    check-cast p4, LX/35p;

    invoke-interface {p4}, LX/35p;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v1

    .line 1646202
    iget-boolean v2, p2, LX/AEE;->c:Z

    move v2, v2

    .line 1646203
    if-nez v2, :cond_0

    .line 1646204
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setVisibility(I)V

    .line 1646205
    :goto_0
    const/16 v1, 0x1f

    const v2, -0x186fa425

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1646206
    :cond_0
    invoke-virtual {v1, p3}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setVisibility(I)V

    .line 1646207
    iput-boolean p3, v1, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->g:Z

    .line 1646208
    iget-object v2, v1, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    move-object v1, v2

    .line 1646209
    iget-object v2, p2, LX/AEE;->f:Ljava/lang/String;

    move-object v2, v2

    .line 1646210
    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1646211
    const v2, 0x7f02079d

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setBackgroundResource(I)V

    .line 1646212
    iget-object v2, p2, LX/AEE;->d:Landroid/view/View$OnClickListener;

    move-object v2, v2

    .line 1646213
    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1646214
    iget-object v2, p2, LX/AEE;->e:Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    move-object v2, v2

    .line 1646215
    if-eqz v2, :cond_1

    .line 1646216
    iget-object v2, p2, LX/AEE;->e:Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    move-object v2, v2

    .line 1646217
    invoke-virtual {v1, p1, p1, v2, p1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1646218
    :cond_1
    iget-boolean v2, p2, LX/AEE;->g:Z

    move v2, v2

    .line 1646219
    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setEnabled(Z)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1646190
    check-cast p2, LX/AEE;

    .line 1646191
    iget-object v0, p0, Lcom/facebook/attachments/angora/actionbutton/AppOpenActionButtonPartDefinition;->d:LX/2v6;

    .line 1646192
    iget-object v1, p2, LX/AEE;->a:LX/6Qd;

    move-object v1, v1

    .line 1646193
    invoke-virtual {v0, v1}, LX/2v6;->b(LX/6Qd;)V

    move-object v0, p4

    .line 1646194
    check-cast v0, LX/35p;

    invoke-interface {v0}, LX/35p;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1646195
    :goto_0
    return-void

    .line 1646196
    :cond_0
    check-cast p4, LX/35p;

    invoke-interface {p4}, LX/35p;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a()V

    goto :goto_0
.end method
