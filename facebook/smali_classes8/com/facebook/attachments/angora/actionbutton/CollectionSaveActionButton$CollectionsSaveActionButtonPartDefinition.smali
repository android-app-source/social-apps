.class public final Lcom/facebook/attachments/angora/actionbutton/CollectionSaveActionButton$CollectionsSaveActionButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/35p;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AEM;


# direct methods
.method public constructor <init>(LX/AEM;)V
    .locals 0

    .prologue
    .line 1646423
    iput-object p1, p0, Lcom/facebook/attachments/angora/actionbutton/CollectionSaveActionButton$CollectionsSaveActionButtonPartDefinition;->a:LX/AEM;

    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1646424
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1646425
    iget-object v0, p0, Lcom/facebook/attachments/angora/actionbutton/CollectionSaveActionButton$CollectionsSaveActionButtonPartDefinition;->a:LX/AEM;

    iget-object v6, v0, LX/AEM;->g:Lcom/facebook/attachments/angora/actionbutton/FlatSaveButtonPartDefinition;

    new-instance v0, LX/2fn;

    .line 1646426
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1646427
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v1}, LX/AEM;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    const-string v2, "native_story"

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/attachments/angora/actionbutton/CollectionSaveActionButton$CollectionsSaveActionButtonPartDefinition;->a:LX/AEM;

    iget-object v4, v4, LX/AEM;->c:Landroid/view/View$OnClickListener;

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, LX/2fn;-><init>(Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View$OnClickListener;Z)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1646428
    const/4 v0, 0x0

    return-object v0
.end method
