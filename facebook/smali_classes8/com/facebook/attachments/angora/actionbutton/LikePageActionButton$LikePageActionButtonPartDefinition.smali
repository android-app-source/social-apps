.class public final Lcom/facebook/attachments/angora/actionbutton/LikePageActionButton$LikePageActionButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/35p;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/AEc;",
        "TE;TV;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AEd;


# direct methods
.method public constructor <init>(LX/AEd;)V
    .locals 0

    .prologue
    .line 1646817
    iput-object p1, p0, Lcom/facebook/attachments/angora/actionbutton/LikePageActionButton$LikePageActionButtonPartDefinition;->a:LX/AEd;

    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1646818
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1646819
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1646820
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1646821
    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1646822
    new-instance v0, LX/AEc;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/AEc;-><init>(Landroid/view/View$OnClickListener;Z)V

    .line 1646823
    :goto_0
    return-object v0

    .line 1646824
    :cond_0
    iget-object v1, p0, Lcom/facebook/attachments/angora/actionbutton/LikePageActionButton$LikePageActionButtonPartDefinition;->a:LX/AEd;

    invoke-static {v1, p2}, LX/AEd;->a$redex0(LX/AEd;Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/view/View$OnClickListener;

    move-result-object v1

    .line 1646825
    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->w()Z

    move-result v2

    .line 1646826
    new-instance v0, LX/AEc;

    invoke-direct {v0, v1, v2}, LX/AEc;-><init>(Landroid/view/View$OnClickListener;Z)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x40c10da9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1646827
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/AEc;

    const/4 p3, 0x0

    .line 1646828
    move-object v1, p4

    check-cast v1, LX/35p;

    invoke-interface {v1}, LX/35p;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v2

    .line 1646829
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1646830
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1646831
    const/16 v1, 0x8

    invoke-virtual {v2, v1}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setVisibility(I)V

    .line 1646832
    iget-object v1, p0, Lcom/facebook/attachments/angora/actionbutton/LikePageActionButton$LikePageActionButtonPartDefinition;->a:LX/AEd;

    iget-object v1, v1, LX/AEd;->g:LX/03V;

    sget-object v2, LX/AEd;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string p3, "Attachment does not contain an action link with a page! Story ID is "

    invoke-direct {v4, p3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p3

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1646833
    :goto_0
    const/16 v1, 0x1f

    const v2, -0x6d6e9cf

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1646834
    :cond_0
    invoke-virtual {v2, p3}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setVisibility(I)V

    .line 1646835
    const/4 v1, 0x1

    .line 1646836
    iput-boolean v1, v2, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->g:Z

    .line 1646837
    const v1, 0x7f020a8d

    invoke-virtual {v2, v1}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setButtonBackgroundResource(I)V

    .line 1646838
    iget-object v1, p0, Lcom/facebook/attachments/angora/actionbutton/LikePageActionButton$LikePageActionButtonPartDefinition;->a:LX/AEd;

    iget v1, v1, LX/AEd;->h:I

    iget-object v4, p0, Lcom/facebook/attachments/angora/actionbutton/LikePageActionButton$LikePageActionButtonPartDefinition;->a:LX/AEd;

    iget v4, v4, LX/AEd;->h:I

    invoke-virtual {v2, v1, p3, v4, p3}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setPadding(IIII)V

    .line 1646839
    check-cast p4, LX/35p;

    invoke-interface {p4}, LX/35p;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v1

    .line 1646840
    iget-object v2, v1, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    move-object v2, v2

    .line 1646841
    iget-boolean v1, p2, LX/AEc;->b:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/attachments/angora/actionbutton/LikePageActionButton$LikePageActionButtonPartDefinition;->a:LX/AEd;

    iget-object v1, v1, LX/AEd;->f:Ljava/lang/String;

    :goto_1
    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1646842
    invoke-virtual {v2, p3}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setCompoundDrawablePadding(I)V

    .line 1646843
    iget-object v1, p2, LX/AEc;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1646844
    iget-object v1, p0, Lcom/facebook/attachments/angora/actionbutton/LikePageActionButton$LikePageActionButtonPartDefinition;->a:LX/AEd;

    iget v1, v1, LX/AEd;->c:I

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageResource(I)V

    .line 1646845
    iget-boolean v1, p2, LX/AEc;->b:Z

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setSelected(Z)V

    .line 1646846
    iget-object v1, p0, Lcom/facebook/attachments/angora/actionbutton/LikePageActionButton$LikePageActionButtonPartDefinition;->a:LX/AEd;

    iget-object v1, v1, LX/AEd;->d:Landroid/content/res/ColorStateList;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setGlyphColor(Landroid/content/res/ColorStateList;)V

    goto :goto_0

    .line 1646847
    :cond_1
    iget-object v1, p0, Lcom/facebook/attachments/angora/actionbutton/LikePageActionButton$LikePageActionButtonPartDefinition;->a:LX/AEd;

    iget-object v1, v1, LX/AEd;->e:Ljava/lang/String;

    goto :goto_1
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1646848
    move-object v0, p4

    check-cast v0, LX/35p;

    invoke-interface {v0}, LX/35p;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1646849
    :goto_0
    return-void

    .line 1646850
    :cond_0
    check-cast p4, LX/35p;

    invoke-interface {p4}, LX/35p;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a()V

    goto :goto_0
.end method
