.class public final Lcom/facebook/attachments/angora/actionbutton/ClaimCouponActionButton$ClaimCouponActionButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/35p;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/AEH;",
        "TE;TV;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AEI;


# direct methods
.method public constructor <init>(LX/AEI;)V
    .locals 0

    .prologue
    .line 1646345
    iput-object p1, p0, Lcom/facebook/attachments/angora/actionbutton/ClaimCouponActionButton$ClaimCouponActionButtonPartDefinition;->a:LX/AEI;

    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1646336
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x0

    .line 1646337
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1646338
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1646339
    invoke-static {v0}, LX/AEI;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLCoupon;

    move-result-object v3

    move-object v1, v3

    .line 1646340
    if-nez v1, :cond_0

    .line 1646341
    new-instance v0, LX/AEH;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v2, v2}, LX/AEH;-><init>(ZLjava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 1646342
    :goto_0
    return-object v0

    .line 1646343
    :cond_0
    iget-object v0, p0, Lcom/facebook/attachments/angora/actionbutton/ClaimCouponActionButton$ClaimCouponActionButtonPartDefinition;->a:LX/AEI;

    invoke-static {p2}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v2

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    invoke-static {v3}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v3

    invoke-static {v0, v1, v2, v3}, LX/AEI;->a$redex0(LX/AEI;Lcom/facebook/graphql/model/GraphQLCoupon;LX/162;Z)Landroid/view/View$OnClickListener;

    move-result-object v2

    .line 1646344
    new-instance v0, LX/AEH;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/facebook/attachments/angora/actionbutton/ClaimCouponActionButton$ClaimCouponActionButtonPartDefinition;->a:LX/AEI;

    iget-object v4, v4, LX/AEI;->a:Landroid/content/res/Resources;

    invoke-static {v1}, LX/AEI;->c(Lcom/facebook/graphql/model/GraphQLCoupon;)I

    move-result v1

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1, v2}, LX/AEH;-><init>(ZLjava/lang/String;Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x7d782d5f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1646322
    check-cast p2, LX/AEH;

    .line 1646323
    move-object v1, p4

    check-cast v1, LX/35p;

    invoke-interface {v1}, LX/35p;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v1

    .line 1646324
    iget-boolean v2, p2, LX/AEH;->a:Z

    if-nez v2, :cond_0

    .line 1646325
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setVisibility(I)V

    .line 1646326
    :goto_0
    const/16 v1, 0x1f

    const v2, -0x335e0273    # -8.492964E7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1646327
    :cond_0
    check-cast p4, LX/35p;

    invoke-interface {p4}, LX/35p;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v2

    .line 1646328
    iget-object p0, v2, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    move-object v2, p0

    .line 1646329
    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setVisibility(I)V

    .line 1646330
    const/4 p0, 0x1

    .line 1646331
    iput-boolean p0, v1, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->g:Z

    .line 1646332
    iget-object v1, p2, LX/AEH;->b:Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1646333
    const v1, 0x7f020850

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageResource(I)V

    .line 1646334
    iget-object v1, p2, LX/AEH;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1646335
    const v1, 0x7f02079d

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1646319
    move-object v0, p4

    check-cast v0, LX/35p;

    invoke-interface {v0}, LX/35p;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1646320
    :goto_0
    return-void

    .line 1646321
    :cond_0
    check-cast p4, LX/35p;

    invoke-interface {p4}, LX/35p;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a()V

    goto :goto_0
.end method
