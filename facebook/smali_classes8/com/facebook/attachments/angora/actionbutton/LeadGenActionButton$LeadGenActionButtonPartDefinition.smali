.class public final Lcom/facebook/attachments/angora/actionbutton/LeadGenActionButton$LeadGenActionButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/35p;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/AEX;",
        "TE;TV;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AEY;


# direct methods
.method public constructor <init>(LX/AEY;)V
    .locals 0

    .prologue
    .line 1646709
    iput-object p1, p0, Lcom/facebook/attachments/angora/actionbutton/LeadGenActionButton$LeadGenActionButtonPartDefinition;->a:LX/AEY;

    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1646710
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pq;

    const/4 v2, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 1646711
    invoke-static {p2}, LX/AEY;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v3

    .line 1646712
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1646713
    new-instance v0, LX/AEX;

    move v3, v1

    move v4, v1

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/AEX;-><init>(ZLjava/lang/String;IILandroid/view/View$OnClickListener;)V

    .line 1646714
    :goto_0
    return-object v0

    .line 1646715
    :cond_0
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->Z()Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;

    move-result-object v0

    .line 1646716
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    move v1, v6

    .line 1646717
    :cond_1
    new-instance v0, LX/AEX;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/attachments/angora/actionbutton/LeadGenActionButton$LeadGenActionButtonPartDefinition;->a:LX/AEY;

    invoke-static {v3, v1}, LX/AEY;->a$redex0(LX/AEY;Z)I

    move-result v3

    .line 1646718
    invoke-static {v1}, LX/AEY;->b(Z)I

    move-result v5

    move v4, v5

    .line 1646719
    iget-object v1, p0, Lcom/facebook/attachments/angora/actionbutton/LeadGenActionButton$LeadGenActionButtonPartDefinition;->a:LX/AEY;

    iget-object v1, v1, LX/AEY;->b:LX/2yI;

    iget-object v5, p0, Lcom/facebook/attachments/angora/actionbutton/LeadGenActionButton$LeadGenActionButtonPartDefinition;->a:LX/AEY;

    iget-object v5, v5, LX/AEY;->c:Landroid/content/Context;

    invoke-virtual {v1, p2, v5, p3}, LX/2yI;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;LX/1Pq;)LX/B6B;

    move-result-object v5

    move v1, v6

    invoke-direct/range {v0 .. v5}, LX/AEX;-><init>(ZLjava/lang/String;IILandroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x276d50ab

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1646720
    check-cast p2, LX/AEX;

    const/4 p0, 0x0

    .line 1646721
    check-cast p4, LX/35p;

    invoke-interface {p4}, LX/35p;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v1

    .line 1646722
    iget-boolean v2, p2, LX/AEX;->a:Z

    if-nez v2, :cond_0

    .line 1646723
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setVisibility(I)V

    .line 1646724
    :goto_0
    const/16 v1, 0x1f

    const v2, -0x15b022be

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1646725
    :cond_0
    invoke-virtual {v1, p0}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setVisibility(I)V

    .line 1646726
    iput-boolean p0, v1, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->g:Z

    .line 1646727
    iget-object v2, v1, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    move-object v1, v2

    .line 1646728
    iget v2, p2, LX/AEX;->d:I

    if-nez v2, :cond_1

    .line 1646729
    invoke-virtual {v1, p0}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setCompoundDrawablePadding(I)V

    .line 1646730
    :goto_1
    iget v2, p2, LX/AEX;->c:I

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setTextColor(I)V

    .line 1646731
    iget-object v2, p2, LX/AEX;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1646732
    const v2, 0x7f02079d

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setBackgroundResource(I)V

    .line 1646733
    iget-object v2, p2, LX/AEX;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 1646734
    :cond_1
    iget v2, p2, LX/AEX;->d:I

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageResource(I)V

    goto :goto_1
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1646735
    move-object v0, p4

    check-cast v0, LX/35p;

    invoke-interface {v0}, LX/35p;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1646736
    :goto_0
    return-void

    .line 1646737
    :cond_0
    check-cast p4, LX/35p;

    invoke-interface {p4}, LX/35p;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a()V

    goto :goto_0
.end method
