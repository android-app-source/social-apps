.class public final Lcom/facebook/attachments/angora/actionbutton/MessagePageActionButton$MessagePageActionButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/35p;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/AEf;",
        "TE;TV;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AEg;


# direct methods
.method public constructor <init>(LX/AEg;)V
    .locals 0

    .prologue
    .line 1646905
    iput-object p1, p0, Lcom/facebook/attachments/angora/actionbutton/MessagePageActionButton$MessagePageActionButtonPartDefinition;->a:LX/AEg;

    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1646906
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1646907
    new-instance v0, LX/AEf;

    iget-object v1, p0, Lcom/facebook/attachments/angora/actionbutton/MessagePageActionButton$MessagePageActionButtonPartDefinition;->a:LX/AEg;

    iget-object v1, v1, LX/AEg;->d:LX/AEi;

    invoke-virtual {v1, p2}, LX/AEi;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/AEh;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/attachments/angora/actionbutton/MessagePageActionButton$MessagePageActionButtonPartDefinition;->a:LX/AEg;

    invoke-static {v2}, LX/AEg;->b$redex0(LX/AEg;)I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/AEf;-><init>(Landroid/view/View$OnClickListener;I)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x778ce6cd

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1646908
    check-cast p2, LX/AEf;

    const/4 p1, 0x0

    .line 1646909
    check-cast p4, LX/35p;

    invoke-interface {p4}, LX/35p;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v1

    .line 1646910
    invoke-virtual {v1, p1}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setVisibility(I)V

    .line 1646911
    const/4 v2, 0x1

    .line 1646912
    iput-boolean v2, v1, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->g:Z

    .line 1646913
    const v2, 0x7f020a8d

    invoke-virtual {v1, v2}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setButtonBackgroundResource(I)V

    .line 1646914
    iget-object v2, v1, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    move-object v1, v2

    .line 1646915
    iget-object v2, p0, Lcom/facebook/attachments/angora/actionbutton/MessagePageActionButton$MessagePageActionButtonPartDefinition;->a:LX/AEg;

    iget-object v2, v2, LX/AEg;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1646916
    invoke-virtual {v1, p1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setCompoundDrawablePadding(I)V

    .line 1646917
    iget-object v2, p2, LX/AEf;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1646918
    iget v2, p2, LX/AEf;->b:I

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageResource(I)V

    .line 1646919
    const/16 v1, 0x1f

    const v2, 0x6480bfd0    # 1.9000038E22f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1646920
    move-object v0, p4

    check-cast v0, LX/35p;

    invoke-interface {v0}, LX/35p;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1646921
    :goto_0
    return-void

    .line 1646922
    :cond_0
    check-cast p4, LX/35p;

    invoke-interface {p4}, LX/35p;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a()V

    goto :goto_0
.end method
