.class public final Lcom/facebook/attachments/angora/actionbutton/GroupJoinActionButton$GroupJoinActionButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/35p;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/AEV;",
        "TE;TV;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AEW;


# direct methods
.method public constructor <init>(LX/AEW;)V
    .locals 0

    .prologue
    .line 1646599
    iput-object p1, p0, Lcom/facebook/attachments/angora/actionbutton/GroupJoinActionButton$GroupJoinActionButtonPartDefinition;->a:LX/AEW;

    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 1646616
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pq;

    .line 1646617
    invoke-static {p2}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    move-object v0, p3

    .line 1646618
    check-cast v0, LX/1Pr;

    new-instance v2, LX/AET;

    invoke-direct {v2, v1}, LX/AET;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-interface {v0, v2, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/AEU;

    .line 1646619
    iget-boolean v0, v5, LX/AEU;->a:Z

    move v0, v0

    .line 1646620
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/facebook/attachments/angora/actionbutton/GroupJoinActionButton$GroupJoinActionButtonPartDefinition;->a:LX/AEW;

    iget-object v1, v1, LX/AEW;->c:Landroid/content/res/Resources;

    const v2, 0x7f081b9a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1646621
    :goto_0
    if-eqz v0, :cond_1

    const v2, 0x7f020a52

    .line 1646622
    :goto_1
    if-eqz v0, :cond_2

    const v3, 0x7f0a00d2

    .line 1646623
    :goto_2
    new-instance v0, LX/AEV;

    new-instance v4, LX/AES;

    invoke-direct {v4, p0, p2, p3, v5}, LX/AES;-><init>(Lcom/facebook/attachments/angora/actionbutton/GroupJoinActionButton$GroupJoinActionButtonPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pq;LX/AEU;)V

    iget-object v6, p0, Lcom/facebook/attachments/angora/actionbutton/GroupJoinActionButton$GroupJoinActionButtonPartDefinition;->a:LX/AEW;

    iget-object v6, v6, LX/AEW;->h:LX/0ad;

    sget-short v7, LX/6Bw;->a:S

    const/4 v8, 0x0

    invoke-interface {v6, v7, v8}, LX/0ad;->a(SZ)Z

    move-result v6

    invoke-direct/range {v0 .. v6}, LX/AEV;-><init>(Ljava/lang/String;IILandroid/view/View$OnClickListener;LX/AEU;Z)V

    return-object v0

    .line 1646624
    :cond_0
    iget-object v1, p0, Lcom/facebook/attachments/angora/actionbutton/GroupJoinActionButton$GroupJoinActionButtonPartDefinition;->a:LX/AEW;

    iget-object v1, v1, LX/AEW;->c:Landroid/content/res/Resources;

    const v2, 0x7f081b99

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1646625
    :cond_1
    const v2, 0x7f020ad5

    goto :goto_1

    .line 1646626
    :cond_2
    const v3, 0x7f0a033a

    goto :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x5038bd5a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1646603
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/AEV;

    const/4 p3, 0x0

    .line 1646604
    check-cast p4, LX/35p;

    invoke-interface {p4}, LX/35p;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v1

    .line 1646605
    iget-boolean v2, p2, LX/AEV;->f:Z

    if-eqz v2, :cond_0

    if-nez p1, :cond_1

    .line 1646606
    :cond_0
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setVisibility(I)V

    .line 1646607
    :goto_0
    const/16 v1, 0x1f

    const v2, -0x7fdcac7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1646608
    :cond_1
    invoke-virtual {v1, p3}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setVisibility(I)V

    .line 1646609
    const v2, 0x7f020a8d

    invoke-virtual {v1, v2}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setButtonBackgroundResource(I)V

    .line 1646610
    iget-object v2, v1, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    move-object v1, v2

    .line 1646611
    iget-object v2, p2, LX/AEV;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1646612
    invoke-virtual {v1, p3}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setCompoundDrawablePadding(I)V

    .line 1646613
    iget v2, p2, LX/AEV;->b:I

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageResource(I)V

    .line 1646614
    iget-object v2, p0, Lcom/facebook/attachments/angora/actionbutton/GroupJoinActionButton$GroupJoinActionButtonPartDefinition;->a:LX/AEW;

    iget-object v2, v2, LX/AEW;->c:Landroid/content/res/Resources;

    iget p3, p2, LX/AEV;->c:I

    invoke-virtual {v2, p3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setGlyphColor(I)V

    .line 1646615
    iget-object v2, p2, LX/AEV;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1646600
    move-object v0, p4

    check-cast v0, LX/35p;

    invoke-interface {v0}, LX/35p;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1646601
    :goto_0
    return-void

    .line 1646602
    :cond_0
    check-cast p4, LX/35p;

    invoke-interface {p4}, LX/35p;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a()V

    goto :goto_0
.end method
