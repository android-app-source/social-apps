.class public final Lcom/facebook/attachments/angora/actionbutton/AddFriendActionButton$AddFriendActionButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/35p;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/AE9;",
        "TE;TV;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AEA;


# direct methods
.method public constructor <init>(LX/AEA;)V
    .locals 0

    .prologue
    .line 1646031
    iput-object p1, p0, Lcom/facebook/attachments/angora/actionbutton/AddFriendActionButton$AddFriendActionButtonPartDefinition;->a:LX/AEA;

    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1646032
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1646033
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1646034
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1646035
    invoke-static {v0}, LX/1VO;->w(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    .line 1646036
    new-instance v2, LX/AE9;

    new-instance v3, LX/AE8;

    invoke-direct {v3, p0, p2}, LX/AE8;-><init>(Lcom/facebook/attachments/angora/actionbutton/AddFriendActionButton$AddFriendActionButtonPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    if-eqz v1, :cond_0

    const v0, 0x7f0208a0

    :goto_0
    invoke-direct {v2, v3, v0, v1}, LX/AE9;-><init>(Landroid/view/View$OnClickListener;IZ)V

    return-object v2

    :cond_0
    const v0, 0x7f020886

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x3aebcd56

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1646037
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/AE9;

    const/4 p3, 0x0

    .line 1646038
    check-cast p4, LX/35p;

    invoke-interface {p4}, LX/35p;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v1

    .line 1646039
    if-nez p1, :cond_0

    .line 1646040
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setVisibility(I)V

    .line 1646041
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x69dcb4ca

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1646042
    :cond_0
    invoke-virtual {v1, p3}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setVisibility(I)V

    .line 1646043
    const/4 v2, 0x1

    .line 1646044
    iput-boolean v2, v1, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->g:Z

    .line 1646045
    iget-object v2, p0, Lcom/facebook/attachments/angora/actionbutton/AddFriendActionButton$AddFriendActionButtonPartDefinition;->a:LX/AEA;

    iget v2, v2, LX/AEA;->i:I

    iget-object v4, p0, Lcom/facebook/attachments/angora/actionbutton/AddFriendActionButton$AddFriendActionButtonPartDefinition;->a:LX/AEA;

    iget v4, v4, LX/AEA;->i:I

    invoke-virtual {v1, v2, p3, v4, p3}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setPadding(IIII)V

    .line 1646046
    const v2, 0x7f020a8d

    invoke-virtual {v1, v2}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setButtonBackgroundResource(I)V

    .line 1646047
    iget-object v2, v1, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    move-object v2, v2

    .line 1646048
    iget-boolean v1, p2, LX/AE9;->c:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/attachments/angora/actionbutton/AddFriendActionButton$AddFriendActionButtonPartDefinition;->a:LX/AEA;

    iget-object v1, v1, LX/AEA;->h:Ljava/lang/String;

    :goto_1
    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1646049
    invoke-virtual {v2, p3}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setCompoundDrawablePadding(I)V

    .line 1646050
    iget-object v1, p2, LX/AE9;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1646051
    iget v1, p2, LX/AE9;->b:I

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageResource(I)V

    .line 1646052
    iget-boolean v1, p2, LX/AE9;->c:Z

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setSelected(Z)V

    .line 1646053
    iget-object v1, p0, Lcom/facebook/attachments/angora/actionbutton/AddFriendActionButton$AddFriendActionButtonPartDefinition;->a:LX/AEA;

    iget-object v1, v1, LX/AEA;->f:Landroid/content/res/ColorStateList;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setGlyphColor(Landroid/content/res/ColorStateList;)V

    goto :goto_0

    .line 1646054
    :cond_1
    iget-object v1, p0, Lcom/facebook/attachments/angora/actionbutton/AddFriendActionButton$AddFriendActionButtonPartDefinition;->a:LX/AEA;

    iget-object v1, v1, LX/AEA;->g:Ljava/lang/String;

    goto :goto_1
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1646055
    move-object v0, p4

    check-cast v0, LX/35p;

    invoke-interface {v0}, LX/35p;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1646056
    :goto_0
    return-void

    .line 1646057
    :cond_0
    check-cast p4, LX/35p;

    invoke-interface {p4}, LX/35p;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a()V

    goto :goto_0
.end method
