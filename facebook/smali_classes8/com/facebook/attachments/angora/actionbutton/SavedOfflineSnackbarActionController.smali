.class public Lcom/facebook/attachments/angora/actionbutton/SavedOfflineSnackbarActionController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/AEn;


# instance fields
.field private final a:LX/16H;

.field private final b:LX/1dv;

.field private final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;LX/16H;LX/1dv;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "LX/16H;",
            "LX/1dv;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1647039
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1647040
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    if-eqz p3, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "storyId, objectId and url can\'t all be null"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1647041
    iput-object p2, p0, Lcom/facebook/attachments/angora/actionbutton/SavedOfflineSnackbarActionController;->d:Ljava/lang/String;

    .line 1647042
    iput-object p4, p0, Lcom/facebook/attachments/angora/actionbutton/SavedOfflineSnackbarActionController;->f:LX/0Px;

    .line 1647043
    iput-object p5, p0, Lcom/facebook/attachments/angora/actionbutton/SavedOfflineSnackbarActionController;->g:Ljava/lang/String;

    .line 1647044
    iput-object p6, p0, Lcom/facebook/attachments/angora/actionbutton/SavedOfflineSnackbarActionController;->a:LX/16H;

    .line 1647045
    iput-object p1, p0, Lcom/facebook/attachments/angora/actionbutton/SavedOfflineSnackbarActionController;->c:Ljava/lang/String;

    .line 1647046
    iput-object p3, p0, Lcom/facebook/attachments/angora/actionbutton/SavedOfflineSnackbarActionController;->e:Ljava/lang/String;

    .line 1647047
    iput-object p7, p0, Lcom/facebook/attachments/angora/actionbutton/SavedOfflineSnackbarActionController;->b:LX/1dv;

    .line 1647048
    return-void

    .line 1647049
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1647050
    const v0, 0x7f080d29

    return v0
.end method

.method public final a(Landroid/content/Context;)V
    .locals 14

    .prologue
    .line 1647051
    iget-object v0, p0, Lcom/facebook/attachments/angora/actionbutton/SavedOfflineSnackbarActionController;->b:LX/1dv;

    new-instance v1, LX/5un;

    sget-object v2, LX/5uo;->SAVE:LX/5uo;

    const-string v3, "native_story"

    const-string v4, "offline_toast"

    iget-object v5, p0, Lcom/facebook/attachments/angora/actionbutton/SavedOfflineSnackbarActionController;->f:LX/0Px;

    invoke-direct {v1, v2, v3, v4, v5}, LX/5un;-><init>(LX/5uo;Ljava/lang/String;Ljava/lang/String;LX/0Px;)V

    iget-object v2, p0, Lcom/facebook/attachments/angora/actionbutton/SavedOfflineSnackbarActionController;->c:Ljava/lang/String;

    invoke-static {v2}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    .line 1647052
    iput-object v2, v1, LX/5un;->a:LX/0am;

    .line 1647053
    move-object v1, v1

    .line 1647054
    iget-object v2, p0, Lcom/facebook/attachments/angora/actionbutton/SavedOfflineSnackbarActionController;->d:Ljava/lang/String;

    invoke-static {v2}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    .line 1647055
    iput-object v2, v1, LX/5un;->d:LX/0am;

    .line 1647056
    move-object v1, v1

    .line 1647057
    iget-object v2, p0, Lcom/facebook/attachments/angora/actionbutton/SavedOfflineSnackbarActionController;->e:Ljava/lang/String;

    invoke-static {v2}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    .line 1647058
    iput-object v2, v1, LX/5un;->c:LX/0am;

    .line 1647059
    move-object v1, v1

    .line 1647060
    iget-object v2, p0, Lcom/facebook/attachments/angora/actionbutton/SavedOfflineSnackbarActionController;->g:Ljava/lang/String;

    invoke-static {v2}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    .line 1647061
    iput-object v2, v1, LX/5un;->e:LX/0am;

    .line 1647062
    move-object v1, v1

    .line 1647063
    invoke-virtual {v1}, LX/5un;->a()Lcom/facebook/saved/server/UpdateSavedStateParams;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    .line 1647064
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 1647065
    const-string v8, "updateStorySavedStateParamsKey"

    invoke-virtual {v10, v8, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1647066
    iget-object v8, v0, LX/1dv;->a:LX/0aG;

    const-string v9, "update_story_saved_state"

    sget-object v11, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const v13, -0x7a630fc9

    move-object v12, v2

    invoke-static/range {v8 .. v13}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v8

    .line 1647067
    iget-object v9, v0, LX/1dv;->b:LX/1dw;

    invoke-virtual {v9, v8}, LX/1dw;->a(LX/1MF;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1647068
    iget-object v0, p0, Lcom/facebook/attachments/angora/actionbutton/SavedOfflineSnackbarActionController;->a:LX/16H;

    const-string v1, "native_newsfeed"

    iget-object v2, p0, Lcom/facebook/attachments/angora/actionbutton/SavedOfflineSnackbarActionController;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/attachments/angora/actionbutton/SavedOfflineSnackbarActionController;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/attachments/angora/actionbutton/SavedOfflineSnackbarActionController;->e:Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "native_story"

    const-string v7, "offline_toast"

    const/4 v12, 0x0

    .line 1647069
    iget-object v8, v0, LX/16H;->b:LX/0gh;

    new-instance v9, LX/0P2;

    invoke-direct {v9}, LX/0P2;-><init>()V

    const-string v10, "action_name"

    const-string v11, "saved_collection_saved_button_clicked"

    invoke-virtual {v9, v10, v11}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v9

    const-string v10, "object_id"

    invoke-static {v2}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v9

    const-string v10, "story_id"

    invoke-static {v3}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v9

    const-string v10, "url"

    invoke-static {v4}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v9

    const-string v10, "collection_id"

    invoke-static {v5}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v9

    const-string v10, "surface"

    invoke-virtual {v9, v10, v6}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v9

    const-string v10, "mechanism"

    invoke-virtual {v9, v10, v7}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v9

    const-string v10, "event_id"

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v9

    invoke-virtual {v9}, LX/0P2;->b()LX/0P1;

    move-result-object v9

    invoke-virtual {v8, v1, v12, v12, v9}, LX/0gh;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 1647070
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1647071
    const v0, 0x7f020781

    return v0
.end method

.method public final c()V
    .locals 10

    .prologue
    .line 1647072
    iget-object v0, p0, Lcom/facebook/attachments/angora/actionbutton/SavedOfflineSnackbarActionController;->a:LX/16H;

    const-string v1, "native_newsfeed"

    iget-object v2, p0, Lcom/facebook/attachments/angora/actionbutton/SavedOfflineSnackbarActionController;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/attachments/angora/actionbutton/SavedOfflineSnackbarActionController;->c:Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "native_story"

    const-string v6, "offline_toast"

    const/4 v7, 0x1

    .line 1647073
    new-instance v8, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v9, "saved_collection_save_button_imp"

    invoke-direct {v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1647074
    iput-object v1, v8, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1647075
    move-object v8, v8

    .line 1647076
    const-string v9, "object_id"

    invoke-static {v2}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v8, v9, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v9, "story_id"

    invoke-static {v3}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v8, v9, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v9, "collection_id"

    invoke-static {v4}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v8, v9, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v9, "surface"

    invoke-virtual {v8, v9, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v9, "mechanism"

    invoke-virtual {v8, v9, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v9, "is_save_button"

    invoke-static {v7}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v8, v9, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v9, "event_id"

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v8, v9, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    .line 1647077
    iget-object v9, v0, LX/16H;->a:LX/0Zb;

    invoke-interface {v9, v8}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1647078
    return-void
.end method
