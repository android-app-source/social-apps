.class public Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;
.super Lcom/facebook/drawee/fbpipeline/FbDraweeView;
.source ""


# instance fields
.field public c:LX/0Or;
    .annotation runtime Lcom/facebook/dialtone/common/IsDialtonePhotoFeatureEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Z

.field public e:Landroid/graphics/Paint;

.field public f:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

.field public g:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1645820
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1645821
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1645818
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1645819
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    .line 1645804
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1645805
    const-class v0, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;

    invoke-static {v0, p0}, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1645806
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1645807
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const v2, 0x7f0a045d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 1645808
    new-instance v2, Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    const v3, 0x7f021af6

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const/16 v4, 0x3e8

    invoke-direct {v2, v3, v4}, Lcom/facebook/drawee/drawable/AutoRotateDrawable;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 1645809
    new-instance v3, LX/1Uo;

    invoke-direct {v3, v0}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 1645810
    iput-object v1, v3, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 1645811
    move-object v0, v3

    .line 1645812
    iput-object v2, v0, LX/1Uo;->l:Landroid/graphics/drawable/Drawable;

    .line 1645813
    move-object v0, v0

    .line 1645814
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 1645815
    invoke-virtual {p0, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 1645816
    const v0, 0x3ff745d1

    invoke-virtual {p0, v0}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 1645817
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;

    const/16 v1, 0x1488

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;->c:LX/0Or;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1645803
    return-void
.end method

.method public getCTAButton()Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;
    .locals 1

    .prologue
    .line 1645802
    iget-object v0, p0, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;->f:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    return-object v0
.end method

.method public getCTAButtonTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 1645801
    iget-object v0, p0, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;->g:Landroid/widget/TextView;

    return-object v0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1645797
    invoke-super {p0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->onDraw(Landroid/graphics/Canvas;)V

    .line 1645798
    iget-boolean v0, p0, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;->d:Z

    if-eqz v0, :cond_0

    .line 1645799
    iget-object v0, p0, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawPaint(Landroid/graphics/Paint;)V

    .line 1645800
    :cond_0
    return-void
.end method

.method public setAspectRatio(F)V
    .locals 1

    .prologue
    .line 1645793
    iget-object v0, p0, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1645794
    const v0, 0x3ff745d1

    invoke-super {p0, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setAspectRatio(F)V

    .line 1645795
    :goto_0
    return-void

    .line 1645796
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setAspectRatio(F)V

    goto :goto_0
.end method

.method public setCTAButton(Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;)V
    .locals 0

    .prologue
    .line 1645781
    iput-object p1, p0, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;->f:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    .line 1645782
    return-void
.end method

.method public setCTAButtonTextView(Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 1645791
    iput-object p1, p0, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;->g:Landroid/widget/TextView;

    .line 1645792
    return-void
.end method

.method public setIsOverlayVisible(Z)V
    .locals 2

    .prologue
    .line 1645785
    iput-boolean p1, p0, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;->d:Z

    .line 1645786
    iget-object v0, p0, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;->f:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    if-eqz v0, :cond_0

    .line 1645787
    if-eqz p1, :cond_1

    .line 1645788
    iget-object v0, p0, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;->f:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setVisibility(I)V

    .line 1645789
    :cond_0
    :goto_0
    return-void

    .line 1645790
    :cond_1
    iget-object v0, p0, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;->f:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public setOverlayPaint(Landroid/graphics/Paint;)V
    .locals 0

    .prologue
    .line 1645783
    iput-object p1, p0, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;->e:Landroid/graphics/Paint;

    .line 1645784
    return-void
.end method
