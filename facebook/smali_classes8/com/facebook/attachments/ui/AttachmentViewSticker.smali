.class public Lcom/facebook/attachments/ui/AttachmentViewSticker;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final j:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final k:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/stickers/client/StickerToPackMetadataLoader;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/11i;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/1CX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/8sB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:Ljava/lang/String;

.field private m:I

.field private n:Z

.field private o:LX/4BY;

.field public p:LX/8mh;

.field private q:Lcom/facebook/stickers/ui/StickerDraweeView;

.field private r:LX/1Ai;

.field private s:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1423954
    const-class v0, Lcom/facebook/attachments/ui/AttachmentViewSticker;

    sput-object v0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->j:Ljava/lang/Class;

    .line 1423955
    const-class v0, Lcom/facebook/attachments/ui/AttachmentViewSticker;

    const-string v1, "comment_attachment_fallback"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->k:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1423956
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1423957
    invoke-direct {p0}, Lcom/facebook/attachments/ui/AttachmentViewSticker;->c()V

    .line 1423958
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1423959
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1423960
    invoke-direct {p0}, Lcom/facebook/attachments/ui/AttachmentViewSticker;->c()V

    .line 1423961
    return-void
.end method

.method private static a(Lcom/facebook/attachments/ui/AttachmentViewSticker;LX/0Sh;LX/03V;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/stickers/client/StickerToPackMetadataLoader;LX/0kL;LX/0Zb;LX/11i;LX/1CX;LX/8sB;)V
    .locals 0

    .prologue
    .line 1423962
    iput-object p1, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->a:LX/0Sh;

    iput-object p2, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->b:LX/03V;

    iput-object p3, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->c:Lcom/facebook/content/SecureContextHelper;

    iput-object p4, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->d:Lcom/facebook/stickers/client/StickerToPackMetadataLoader;

    iput-object p5, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->e:LX/0kL;

    iput-object p6, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->f:LX/0Zb;

    iput-object p7, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->g:LX/11i;

    iput-object p8, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->h:LX/1CX;

    iput-object p9, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->i:LX/8sB;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/attachments/ui/AttachmentViewSticker;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 10

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v9

    move-object v0, p0

    check-cast v0, Lcom/facebook/attachments/ui/AttachmentViewSticker;

    invoke-static {v9}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-static {v9}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {v9}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v9}, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;->b(LX/0QB;)Lcom/facebook/stickers/client/StickerToPackMetadataLoader;

    move-result-object v4

    check-cast v4, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;

    invoke-static {v9}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v5

    check-cast v5, LX/0kL;

    invoke-static {v9}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    invoke-static {v9}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v7

    check-cast v7, LX/11i;

    invoke-static {v9}, LX/1CX;->a(LX/0QB;)LX/1CX;

    move-result-object v8

    check-cast v8, LX/1CX;

    invoke-static {v9}, LX/8sB;->b(LX/0QB;)LX/8sB;

    move-result-object v9

    check-cast v9, LX/8sB;

    invoke-static/range {v0 .. v9}, Lcom/facebook/attachments/ui/AttachmentViewSticker;->a(Lcom/facebook/attachments/ui/AttachmentViewSticker;LX/0Sh;LX/03V;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/stickers/client/StickerToPackMetadataLoader;LX/0kL;LX/0Zb;LX/11i;LX/1CX;LX/8sB;)V

    return-void
.end method

.method public static synthetic a(Lcom/facebook/attachments/ui/AttachmentViewSticker;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1423963
    invoke-direct {p0, p1}, Lcom/facebook/attachments/ui/AttachmentViewSticker;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 1423964
    iget-boolean v0, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->n:Z

    if-nez v0, :cond_0

    .line 1423965
    const/4 v0, 0x0

    .line 1423966
    :goto_0
    return v0

    .line 1423967
    :cond_0
    new-instance v0, LX/8jS;

    invoke-direct {v0, p1}, LX/8jS;-><init>(Ljava/lang/String;)V

    .line 1423968
    iget-object v1, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->d:Lcom/facebook/stickers/client/StickerToPackMetadataLoader;

    invoke-virtual {v1}, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;->a()V

    .line 1423969
    iget-object v1, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->d:Lcom/facebook/stickers/client/StickerToPackMetadataLoader;

    new-instance v2, LX/8xK;

    invoke-direct {v2, p0}, LX/8xK;-><init>(Lcom/facebook/attachments/ui/AttachmentViewSticker;)V

    .line 1423970
    iput-object v2, v1, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;->e:LX/3Mb;

    .line 1423971
    iget-object v1, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->d:Lcom/facebook/stickers/client/StickerToPackMetadataLoader;

    invoke-virtual {v1, v0}, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;->a(LX/8jS;)V

    .line 1423972
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 1423941
    invoke-virtual {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLComment;

    if-eqz v0, :cond_1

    .line 1423942
    invoke-virtual {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1423943
    iget-object v2, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->i:LX/8sB;

    invoke-virtual {v2, v0}, LX/8sB;->a(Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 1423944
    :goto_0
    return v0

    .line 1423945
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1423946
    goto :goto_0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 1423947
    const-class v0, Lcom/facebook/attachments/ui/AttachmentViewSticker;

    invoke-static {v0, p0}, Lcom/facebook/attachments/ui/AttachmentViewSticker;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1423948
    const v0, 0x7f03012b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1423949
    const v0, 0x7f0d05dc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/ui/StickerDraweeView;

    iput-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->q:Lcom/facebook/stickers/ui/StickerDraweeView;

    .line 1423950
    new-instance v0, LX/8xH;

    invoke-direct {v0, p0}, LX/8xH;-><init>(Lcom/facebook/attachments/ui/AttachmentViewSticker;)V

    iput-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->r:LX/1Ai;

    .line 1423951
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->q:Lcom/facebook/stickers/ui/StickerDraweeView;

    new-instance v1, LX/8xI;

    invoke-direct {v1, p0}, LX/8xI;-><init>(Lcom/facebook/attachments/ui/AttachmentViewSticker;)V

    invoke-virtual {v0, v1}, Lcom/facebook/stickers/ui/StickerDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1423952
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->q:Lcom/facebook/stickers/ui/StickerDraweeView;

    new-instance v1, LX/8xJ;

    invoke-direct {v1, p0}, LX/8xJ;-><init>(Lcom/facebook/attachments/ui/AttachmentViewSticker;)V

    invoke-virtual {v0, v1}, Lcom/facebook/stickers/ui/StickerDraweeView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1423953
    return-void
.end method

.method public static d$redex0(Lcom/facebook/attachments/ui/AttachmentViewSticker;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1423932
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->o:LX/4BY;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->o:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1423933
    :goto_0
    return-void

    .line 1423934
    :cond_0
    new-instance v0, LX/4BY;

    invoke-virtual {p0}, Lcom/facebook/attachments/ui/AttachmentViewSticker;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4BY;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->o:LX/4BY;

    .line 1423935
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->o:LX/4BY;

    invoke-virtual {v0, v2}, LX/4BY;->a(Z)V

    .line 1423936
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->o:LX/4BY;

    invoke-virtual {v0, v2}, LX/4BY;->setCanceledOnTouchOutside(Z)V

    .line 1423937
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->o:LX/4BY;

    invoke-virtual {p0}, Lcom/facebook/attachments/ui/AttachmentViewSticker;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080024

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2EJ;->a(Ljava/lang/CharSequence;)V

    .line 1423938
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->o:LX/4BY;

    new-instance v1, LX/8xL;

    invoke-direct {v1, p0}, LX/8xL;-><init>(Lcom/facebook/attachments/ui/AttachmentViewSticker;)V

    invoke-virtual {v0, v1}, LX/4BY;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 1423939
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->o:LX/4BY;

    invoke-static {v0}, LX/4md;->a(Landroid/app/Dialog;)V

    .line 1423940
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->o:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->show()V

    goto :goto_0
.end method

.method public static e$redex0(Lcom/facebook/attachments/ui/AttachmentViewSticker;)V
    .locals 1

    .prologue
    .line 1423930
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->d:Lcom/facebook/stickers/client/StickerToPackMetadataLoader;

    invoke-virtual {v0}, Lcom/facebook/stickers/client/StickerToPackMetadataLoader;->a()V

    .line 1423931
    return-void
.end method

.method public static f$redex0(Lcom/facebook/attachments/ui/AttachmentViewSticker;)V
    .locals 1

    .prologue
    .line 1423926
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->o:LX/4BY;

    if-eqz v0, :cond_0

    .line 1423927
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->o:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->cancel()V

    .line 1423928
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->o:LX/4BY;

    .line 1423929
    :cond_0
    return-void
.end method

.method public static g(Lcom/facebook/attachments/ui/AttachmentViewSticker;)V
    .locals 2

    .prologue
    .line 1423919
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->p:LX/8mh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->p:LX/8mh;

    .line 1423920
    iget-object v1, v0, LX/8mh;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v1

    move v0, v1

    .line 1423921
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->p:LX/8mh;

    invoke-virtual {v0}, LX/8mh;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->s:Z

    if-nez v0, :cond_0

    .line 1423922
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->p:LX/8mh;

    .line 1423923
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/8mh;->c:Z

    .line 1423924
    iget-object v1, v0, LX/8mh;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    .line 1423925
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1423901
    new-instance v0, LX/8mi;

    invoke-direct {v0}, LX/8mi;-><init>()V

    iget-object v1, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->l:Ljava/lang/String;

    .line 1423902
    iput-object v1, v0, LX/8mi;->f:Ljava/lang/String;

    .line 1423903
    move-object v0, v0

    .line 1423904
    iget v1, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->m:I

    .line 1423905
    iput v1, v0, LX/8mi;->b:I

    .line 1423906
    move-object v0, v0

    .line 1423907
    sget-object v1, Lcom/facebook/attachments/ui/AttachmentViewSticker;->k:Lcom/facebook/common/callercontext/CallerContext;

    .line 1423908
    iput-object v1, v0, LX/8mi;->g:Lcom/facebook/common/callercontext/CallerContext;

    .line 1423909
    move-object v0, v0

    .line 1423910
    iget-object v1, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->r:LX/1Ai;

    .line 1423911
    iput-object v1, v0, LX/8mi;->h:LX/1Ai;

    .line 1423912
    move-object v0, v0

    .line 1423913
    iput-boolean v2, v0, LX/8mi;->d:Z

    .line 1423914
    move-object v0, v0

    .line 1423915
    invoke-virtual {v0, v2}, LX/8mi;->a(Z)LX/8mi;

    move-result-object v0

    invoke-virtual {v0}, LX/8mi;->a()LX/8mj;

    move-result-object v0

    .line 1423916
    iget-object v1, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->q:Lcom/facebook/stickers/ui/StickerDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/stickers/ui/StickerDraweeView;->setSticker(LX/8mj;)V

    .line 1423917
    invoke-static {p0}, Lcom/facebook/attachments/ui/AttachmentViewSticker;->g(Lcom/facebook/attachments/ui/AttachmentViewSticker;)V

    .line 1423918
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1423895
    invoke-direct {p0, p1}, Lcom/facebook/attachments/ui/AttachmentViewSticker;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->n:Z

    .line 1423896
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1423897
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->l:Ljava/lang/String;

    .line 1423898
    invoke-virtual {p0}, Lcom/facebook/attachments/ui/AttachmentViewSticker;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0164

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->m:I

    .line 1423899
    invoke-virtual {p0}, Lcom/facebook/attachments/ui/AttachmentViewSticker;->a()V

    .line 1423900
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x597c9dd2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1423892
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onAttachedToWindow()V

    .line 1423893
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->s:Z

    .line 1423894
    const/16 v1, 0x2d

    const v2, -0x63dfed4d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x283690d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1423889
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onDetachedFromWindow()V

    .line 1423890
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->s:Z

    .line 1423891
    const/16 v1, 0x2d

    const v2, 0x79214186

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 0

    .prologue
    .line 1423886
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->setBackgroundColor(I)V

    .line 1423887
    iput p1, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->m:I

    .line 1423888
    return-void
.end method

.method public setSticker(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1423884
    iput-object p1, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->l:Ljava/lang/String;

    .line 1423885
    return-void
.end method
