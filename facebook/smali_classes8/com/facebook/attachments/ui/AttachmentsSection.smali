.class public Lcom/facebook/attachments/ui/AttachmentsSection;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/8rR;


# instance fields
.field public a:LX/8xP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/attachments/ui/AttachmentViewSticker;

.field private c:Lcom/facebook/attachments/ui/AttachmentViewPhoto;

.field private d:Lcom/facebook/attachments/ui/AttachmentViewStaticVideo;

.field private e:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1424047
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/attachments/ui/AttachmentsSection;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1424048
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1424045
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/attachments/ui/AttachmentsSection;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1424046
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1423997
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1423998
    const-class v0, Lcom/facebook/attachments/ui/AttachmentsSection;

    invoke-static {v0, p0}, Lcom/facebook/attachments/ui/AttachmentsSection;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1423999
    const v0, 0x7f03012f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1424000
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 1424036
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentsSection;->c:Lcom/facebook/attachments/ui/AttachmentViewPhoto;

    if-eqz v0, :cond_0

    .line 1424037
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentsSection;->c:Lcom/facebook/attachments/ui/AttachmentViewPhoto;

    invoke-virtual {v0, v1}, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->setVisibility(I)V

    .line 1424038
    :cond_0
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentsSection;->b:Lcom/facebook/attachments/ui/AttachmentViewSticker;

    if-eqz v0, :cond_1

    .line 1424039
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentsSection;->b:Lcom/facebook/attachments/ui/AttachmentViewSticker;

    invoke-virtual {v0, v1}, Lcom/facebook/attachments/ui/AttachmentViewSticker;->setVisibility(I)V

    .line 1424040
    :cond_1
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentsSection;->d:Lcom/facebook/attachments/ui/AttachmentViewStaticVideo;

    if-eqz v0, :cond_2

    .line 1424041
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentsSection;->d:Lcom/facebook/attachments/ui/AttachmentViewStaticVideo;

    invoke-virtual {v0, v1}, Lcom/facebook/attachments/ui/AttachmentViewStaticVideo;->setVisibility(I)V

    .line 1424042
    :cond_2
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentsSection;->f:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    if-eqz v0, :cond_3

    .line 1424043
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentsSection;->f:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setVisibility(I)V

    .line 1424044
    :cond_3
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/attachments/ui/AttachmentsSection;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/attachments/ui/AttachmentsSection;

    invoke-static {v0}, LX/8xP;->b(LX/0QB;)LX/8xP;

    move-result-object v0

    check-cast v0, LX/8xP;

    iput-object v0, p0, Lcom/facebook/attachments/ui/AttachmentsSection;->a:LX/8xP;

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1424031
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentsSection;->b:Lcom/facebook/attachments/ui/AttachmentViewSticker;

    if-nez v0, :cond_0

    .line 1424032
    const v0, 0x7f0d05df

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/attachments/ui/AttachmentViewSticker;

    iput-object v0, p0, Lcom/facebook/attachments/ui/AttachmentsSection;->b:Lcom/facebook/attachments/ui/AttachmentViewSticker;

    .line 1424033
    :cond_0
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentsSection;->b:Lcom/facebook/attachments/ui/AttachmentViewSticker;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/attachments/ui/AttachmentViewSticker;->setVisibility(I)V

    .line 1424034
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentsSection;->b:Lcom/facebook/attachments/ui/AttachmentViewSticker;

    iget-object v1, p0, Lcom/facebook/attachments/ui/AttachmentsSection;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0, v1}, Lcom/facebook/attachments/ui/AttachmentViewSticker;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1424035
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 1424026
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentsSection;->c:Lcom/facebook/attachments/ui/AttachmentViewPhoto;

    if-nez v0, :cond_0

    .line 1424027
    const v0, 0x7f0d05e0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/attachments/ui/AttachmentViewPhoto;

    iput-object v0, p0, Lcom/facebook/attachments/ui/AttachmentsSection;->c:Lcom/facebook/attachments/ui/AttachmentViewPhoto;

    .line 1424028
    :cond_0
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentsSection;->c:Lcom/facebook/attachments/ui/AttachmentViewPhoto;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->setVisibility(I)V

    .line 1424029
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentsSection;->c:Lcom/facebook/attachments/ui/AttachmentViewPhoto;

    iget-object v1, p0, Lcom/facebook/attachments/ui/AttachmentsSection;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0, v1}, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1424030
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 1424021
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentsSection;->d:Lcom/facebook/attachments/ui/AttachmentViewStaticVideo;

    if-nez v0, :cond_0

    .line 1424022
    const v0, 0x7f0d05e1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/attachments/ui/AttachmentViewStaticVideo;

    iput-object v0, p0, Lcom/facebook/attachments/ui/AttachmentsSection;->d:Lcom/facebook/attachments/ui/AttachmentViewStaticVideo;

    .line 1424023
    :cond_0
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentsSection;->d:Lcom/facebook/attachments/ui/AttachmentViewStaticVideo;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/attachments/ui/AttachmentViewStaticVideo;->setVisibility(I)V

    .line 1424024
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentsSection;->d:Lcom/facebook/attachments/ui/AttachmentViewStaticVideo;

    iget-object v1, p0, Lcom/facebook/attachments/ui/AttachmentsSection;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0, v1}, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1424025
    return-void
.end method

.method private g()V
    .locals 3

    .prologue
    .line 1424014
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentsSection;->f:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    if-nez v0, :cond_0

    .line 1424015
    const v0, 0x7f0d05e2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iput-object v0, p0, Lcom/facebook/attachments/ui/AttachmentsSection;->f:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 1424016
    :cond_0
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentsSection;->f:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setVisibility(I)V

    .line 1424017
    iget-object v1, p0, Lcom/facebook/attachments/ui/AttachmentsSection;->a:LX/8xP;

    iget-object v2, p0, Lcom/facebook/attachments/ui/AttachmentsSection;->f:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentsSection;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1424018
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 1424019
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1, v2, v0}, LX/8xP;->a(Lcom/facebook/fbui/widget/layout/ImageBlockLayout;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 1424020
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/6Bu;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;",
            "LX/6Bu;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1424001
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1424002
    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1424003
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/36l;->b(Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1424004
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/attachments/ui/AttachmentsSection;->setVisibility(I)V

    .line 1424005
    :goto_0
    return-void

    .line 1424006
    :cond_1
    invoke-direct {p0}, Lcom/facebook/attachments/ui/AttachmentsSection;->a()V

    .line 1424007
    invoke-static {v0}, LX/36l;->c(Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/attachments/ui/AttachmentsSection;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1424008
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentsSection;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {p2, v0}, LX/6Bu;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    move-result-object v0

    .line 1424009
    sget-object v1, LX/8xN;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1424010
    :pswitch_0
    invoke-direct {p0}, Lcom/facebook/attachments/ui/AttachmentsSection;->b()V

    goto :goto_0

    .line 1424011
    :pswitch_1
    invoke-direct {p0}, Lcom/facebook/attachments/ui/AttachmentsSection;->e()V

    goto :goto_0

    .line 1424012
    :pswitch_2
    invoke-direct {p0}, Lcom/facebook/attachments/ui/AttachmentsSection;->f()V

    goto :goto_0

    .line 1424013
    :pswitch_3
    invoke-direct {p0}, Lcom/facebook/attachments/ui/AttachmentsSection;->g()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
