.class public Lcom/facebook/attachments/ui/AttachmentViewPhoto;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final e:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/26J;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1ev;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/3iM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0sX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private h:Landroid/view/View;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1423803
    const-class v0, Lcom/facebook/attachments/ui/AttachmentViewPhoto;

    const-string v1, "story_feedback_flyout"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->e:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1423804
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/attachments/ui/AttachmentViewPhoto;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1423805
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1423806
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/attachments/ui/AttachmentViewPhoto;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1423807
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1423809
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1423810
    invoke-direct {p0}, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->b()V

    .line 1423811
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1423808
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->d:LX/0sX;

    invoke-virtual {v0}, LX/0sX;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->k()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/attachments/ui/AttachmentViewPhoto;LX/26J;LX/1ev;LX/3iM;LX/0sX;)V
    .locals 0

    .prologue
    .line 1423812
    iput-object p1, p0, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->a:LX/26J;

    iput-object p2, p0, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->b:LX/1ev;

    iput-object p3, p0, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->c:LX/3iM;

    iput-object p4, p0, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->d:LX/0sX;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/attachments/ui/AttachmentViewPhoto;

    invoke-static {v3}, LX/26J;->a(LX/0QB;)LX/26J;

    move-result-object v0

    check-cast v0, LX/26J;

    invoke-static {v3}, LX/1ev;->a(LX/0QB;)LX/1ev;

    move-result-object v1

    check-cast v1, LX/1ev;

    invoke-static {v3}, LX/3iM;->a(LX/0QB;)LX/3iM;

    move-result-object v2

    check-cast v2, LX/3iM;

    invoke-static {v3}, LX/0sX;->b(LX/0QB;)LX/0sX;

    move-result-object v3

    check-cast v3, LX/0sX;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->a(Lcom/facebook/attachments/ui/AttachmentViewPhoto;LX/26J;LX/1ev;LX/3iM;LX/0sX;)V

    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 1423799
    const-class v0, Lcom/facebook/attachments/ui/AttachmentViewPhoto;

    invoke-static {v0, p0}, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1423800
    const v0, 0x7f03012d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1423801
    const v0, 0x7f0d05dd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1423802
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 1423792
    invoke-direct {p0}, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->g()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1423793
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->h:Landroid/view/View;

    if-nez v0, :cond_0

    .line 1423794
    const v0, 0x7f0d05de

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->h:Landroid/view/View;

    .line 1423795
    :cond_0
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->h:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1423796
    :cond_1
    :goto_0
    return-void

    .line 1423797
    :cond_2
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->h:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 1423798
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->h:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 1423786
    const/4 v0, 0x0

    .line 1423787
    invoke-direct {p0}, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1423788
    new-instance v0, LX/8xG;

    invoke-direct {v0, p0}, LX/8xG;-><init>(Lcom/facebook/attachments/ui/AttachmentViewPhoto;)V

    .line 1423789
    :cond_0
    invoke-virtual {p0, v0}, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1423790
    iget-object v1, p0, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1423791
    return-void
.end method

.method private g()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1423781
    iget-object v1, p0, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->f:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, LX/1WF;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v1

    .line 1423782
    if-nez v1, :cond_1

    .line 1423783
    :cond_0
    :goto_0
    return v0

    .line 1423784
    :cond_1
    iget-object v2, p0, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->c:LX/3iM;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->H()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/3iM;->c(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v1

    .line 1423785
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->RETRYING_IN_BACKGROUND:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v1, v2, :cond_0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->POSTING:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 1423777
    iget-object v1, p0, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->a:LX/26J;

    invoke-virtual {p0}, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->f:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1423778
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 1423779
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1, v2, v0}, LX/26J;->b(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 1423780
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1423763
    iput-object p1, p0, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->f:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1423764
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->f:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1423765
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1423766
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1423767
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1423768
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->setVisibility(I)V

    .line 1423769
    :goto_0
    return-void

    .line 1423770
    :cond_1
    invoke-virtual {p0, v0}, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->setupImageView(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 1423771
    invoke-direct {p0}, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->e()V

    .line 1423772
    invoke-direct {p0}, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->f()V

    .line 1423773
    invoke-direct {p0, v0}, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v0

    .line 1423774
    iget-object v1, p0, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v0, :cond_2

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1423775
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->setVisibility(I)V

    goto :goto_0

    .line 1423776
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f080072

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public getAttachment()Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .locals 1

    .prologue
    .line 1423760
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->f:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1423761
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 1423762
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    return-object v0
.end method

.method public getImageView()Lcom/facebook/drawee/fbpipeline/FbDraweeView;
    .locals 1

    .prologue
    .line 1423759
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    return-object v0
.end method

.method public setupImageView(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 4

    .prologue
    .line 1423751
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 1423752
    iget-object v1, p0, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->b:LX/1ev;

    invoke-virtual {v1, v0}, LX/1ev;->b(Lcom/facebook/graphql/model/GraphQLMedia;)LX/1f6;

    move-result-object v0

    .line 1423753
    iget-object v1, p0, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1423754
    iget v2, v0, LX/1f6;->e:I

    move v2, v2

    .line 1423755
    iget v3, v0, LX/1f6;->f:I

    move v3, v3

    .line 1423756
    invoke-static {v2, v3}, LX/26J;->a(II)F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 1423757
    iget-object v1, p0, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, LX/1f6;->a()Landroid/net/Uri;

    move-result-object v0

    sget-object v2, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1423758
    return-void
.end method
