.class public final Lcom/facebook/attachments/ui/AttachmentViewSticker$4$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/8jT;

.field public final synthetic b:LX/8xK;


# direct methods
.method public constructor <init>(LX/8xK;LX/8jT;)V
    .locals 0

    .prologue
    .line 1423852
    iput-object p1, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker$4$2;->b:LX/8xK;

    iput-object p2, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker$4$2;->a:LX/8jT;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 1423853
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker$4$2;->b:LX/8xK;

    iget-object v0, v0, LX/8xK;->a:Lcom/facebook/attachments/ui/AttachmentViewSticker;

    .line 1423854
    invoke-static {v0}, Lcom/facebook/attachments/ui/AttachmentViewSticker;->f$redex0(Lcom/facebook/attachments/ui/AttachmentViewSticker;)V

    .line 1423855
    sget-object v0, LX/8xM;->a:[I

    iget-object v1, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker$4$2;->a:LX/8jT;

    iget-object v1, v1, LX/8jT;->b:LX/8m2;

    invoke-virtual {v1}, LX/8m2;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1423856
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker$4$2;->b:LX/8xK;

    iget-object v0, v0, LX/8xK;->a:Lcom/facebook/attachments/ui/AttachmentViewSticker;

    iget-object v0, v0, Lcom/facebook/attachments/ui/AttachmentViewSticker;->h:LX/1CX;

    iget-object v1, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker$4$2;->b:LX/8xK;

    iget-object v1, v1, LX/8xK;->a:Lcom/facebook/attachments/ui/AttachmentViewSticker;

    invoke-virtual {v1}, Lcom/facebook/attachments/ui/AttachmentViewSticker;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, LX/4mm;->a(Landroid/content/res/Resources;)LX/4mn;

    move-result-object v1

    const v2, 0x7f080244

    invoke-virtual {v1, v2}, LX/4mn;->b(I)LX/4mn;

    move-result-object v1

    invoke-virtual {v1}, LX/4mn;->l()LX/4mm;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1CX;->a(LX/4mm;)LX/2EJ;

    .line 1423857
    :goto_0
    :pswitch_0
    return-void

    .line 1423858
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker$4$2;->b:LX/8xK;

    iget-object v1, v1, LX/8xK;->a:Lcom/facebook/attachments/ui/AttachmentViewSticker;

    invoke-virtual {v1}, Lcom/facebook/attachments/ui/AttachmentViewSticker;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/facebook/stickers/store/StickerStoreActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1423859
    const-string v1, "stickerPack"

    iget-object v2, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker$4$2;->a:LX/8jT;

    iget-object v2, v2, LX/8jT;->a:Lcom/facebook/stickers/model/StickerPack;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1423860
    const-string v1, "stickerContext"

    sget-object v2, LX/4m4;->COMMENTS:LX/4m4;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1423861
    iget-object v1, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker$4$2;->b:LX/8xK;

    iget-object v1, v1, LX/8xK;->a:Lcom/facebook/attachments/ui/AttachmentViewSticker;

    iget-object v1, v1, Lcom/facebook/attachments/ui/AttachmentViewSticker;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, Lcom/facebook/attachments/ui/AttachmentViewSticker$4$2;->b:LX/8xK;

    iget-object v2, v2, LX/8xK;->a:Lcom/facebook/attachments/ui/AttachmentViewSticker;

    invoke-virtual {v2}, Lcom/facebook/attachments/ui/AttachmentViewSticker;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
