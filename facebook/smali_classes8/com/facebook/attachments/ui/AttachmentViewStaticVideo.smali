.class public Lcom/facebook/attachments/ui/AttachmentViewStaticVideo;
.super Lcom/facebook/attachments/ui/AttachmentViewPhoto;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1423813
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/attachments/ui/AttachmentViewStaticVideo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1423814
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1423815
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/attachments/ui/AttachmentViewStaticVideo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1423816
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1423817
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/attachments/ui/AttachmentViewPhoto;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1423818
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1423819
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->a:LX/26J;

    invoke-virtual {p0}, Lcom/facebook/attachments/ui/AttachmentViewStaticVideo;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->getAttachment()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    .line 1423820
    iget-object v3, v0, LX/26J;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1nB;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aS()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v3, p0}, LX/1nB;->c(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p0

    .line 1423821
    iget-object v3, v0, LX/26J;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v3, p0, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1423822
    return-void
.end method

.method public setupImageView(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 3

    .prologue
    .line 1423823
    invoke-super {p0, p1}, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->setupImageView(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 1423824
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewPhoto;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v0, v0

    .line 1423825
    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {p0}, Lcom/facebook/attachments/ui/AttachmentViewStaticVideo;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020aff

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1af;->a(Landroid/graphics/drawable/Drawable;)V

    .line 1423826
    return-void
.end method
