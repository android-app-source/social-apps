.class public Lcom/facebook/attachments/ui/AttachmentViewVideo;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/3iM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/facebook/video/player/RichVideoPlayer;

.field public e:Landroid/view/View;

.field public f:F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1423973
    const-class v0, Lcom/facebook/attachments/ui/AttachmentViewVideo;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/attachments/ui/AttachmentViewVideo;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1423974
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1423975
    const-class v0, Lcom/facebook/attachments/ui/AttachmentViewVideo;

    invoke-static {v0, p0}, Lcom/facebook/attachments/ui/AttachmentViewVideo;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1423976
    const v0, 0x7f03012e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1423977
    const v0, 0x7f0d053b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    iput-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewVideo;->d:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1423978
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewVideo;->d:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance p1, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-virtual {p0}, Lcom/facebook/attachments/ui/AttachmentViewVideo;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    .line 1423979
    invoke-static {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1423980
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewVideo;->d:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance p1, Lcom/facebook/video/player/plugins/CoverImagePlugin;

    invoke-virtual {p0}, Lcom/facebook/attachments/ui/AttachmentViewVideo;->getContext()Landroid/content/Context;

    move-result-object p2

    sget-object p3, Lcom/facebook/attachments/ui/AttachmentViewVideo;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {p1, p2, p3}, Lcom/facebook/video/player/plugins/CoverImagePlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1423981
    invoke-static {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1423982
    iget-object v0, p0, Lcom/facebook/attachments/ui/AttachmentViewVideo;->d:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance p1, LX/7N5;

    invoke-virtual {p0}, Lcom/facebook/attachments/ui/AttachmentViewVideo;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {p1, p2}, LX/7N5;-><init>(Landroid/content/Context;)V

    .line 1423983
    invoke-static {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1423984
    return-void
.end method

.method public static a(II)F
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1423985
    if-eqz p0, :cond_1

    if-eqz p1, :cond_1

    .line 1423986
    int-to-float v0, p0

    const/high16 v2, 0x3f800000    # 1.0f

    mul-float/2addr v0, v2

    int-to-float v2, p1

    div-float/2addr v0, v2

    .line 1423987
    :goto_0
    cmpl-float v1, v0, v1

    if-nez v1, :cond_0

    .line 1423988
    const v0, 0x3faaaaab

    .line 1423989
    :cond_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/attachments/ui/AttachmentViewVideo;

    invoke-static {p0}, LX/3iM;->a(LX/0QB;)LX/3iM;

    move-result-object p0

    check-cast p0, LX/3iM;

    iput-object p0, p1, Lcom/facebook/attachments/ui/AttachmentViewVideo;->a:LX/3iM;

    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 1423990
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 1423991
    invoke-virtual {p0}, Lcom/facebook/attachments/ui/AttachmentViewVideo;->getPaddingLeft()I

    move-result v1

    sub-int v1, v0, v1

    invoke-virtual {p0}, Lcom/facebook/attachments/ui/AttachmentViewVideo;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 1423992
    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/attachments/ui/AttachmentViewVideo;->f:F

    div-float/2addr v1, v2

    float-to-int v1, v1

    .line 1423993
    invoke-virtual {p0, v0, v1}, Lcom/facebook/attachments/ui/AttachmentViewVideo;->setMeasuredDimension(II)V

    .line 1423994
    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/attachments/ui/AttachmentViewVideo;->measureChildren(II)V

    .line 1423995
    return-void
.end method
