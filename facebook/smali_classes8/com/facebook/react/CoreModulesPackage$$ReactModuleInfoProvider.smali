.class public final Lcom/facebook/react/CoreModulesPackage$$ReactModuleInfoProvider;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5qR;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1535893
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class;",
            "LX/5qQ;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 1535894
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1535895
    const-class v1, LX/5qc;

    new-instance v2, LX/5qQ;

    const-string v3, "AndroidConstants"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1535896
    const-class v1, LX/5qS;

    new-instance v2, LX/5qQ;

    const-string v3, "AnimationsDebugModule"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1535897
    const-class v1, LX/9mz;

    new-instance v2, LX/5qQ;

    const-string v3, "DeviceEventManager"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1535898
    const-class v1, LX/9n0;

    new-instance v2, LX/5qQ;

    const-string v3, "RKExceptionsManager"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1535899
    const-class v1, LX/9n1;

    new-instance v2, LX/5qQ;

    const-string v3, "HeadlessJsTaskSupport"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1535900
    const-class v1, LX/5qY;

    new-instance v2, LX/5qQ;

    const-string v3, "RCTSourceCode"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1535901
    const-class v1, LX/9n7;

    new-instance v2, LX/5qQ;

    const-string v3, "RCTTiming"

    const/4 v4, 0x1

    invoke-direct {v2, v3, v5, v4, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1535902
    const-class v1, LX/5rQ;

    new-instance v2, LX/5qQ;

    const-string v3, "RKUIManager"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1535903
    const-class v1, LX/5s5;

    new-instance v2, LX/5qQ;

    const-string v3, "DebugComponentOwnershipModule"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1535904
    const-class v1, LX/5qN;

    new-instance v2, LX/5qQ;

    const-string v3, "JSCHeapCapture"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1535905
    const-class v1, LX/5qO;

    new-instance v2, LX/5qQ;

    const-string v3, "JSCSamplingProfiler"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1535906
    return-object v0
.end method
