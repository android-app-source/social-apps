.class public Lcom/facebook/react/CoreModulesPackage;
.super LX/9mi;
.source ""


# instance fields
.field public final a:LX/33y;

.field public final b:LX/98l;

.field private final c:LX/344;


# direct methods
.method public constructor <init>(LX/33y;LX/98l;LX/344;)V
    .locals 0

    .prologue
    .line 1535970
    invoke-direct {p0}, LX/9mi;-><init>()V

    .line 1535971
    iput-object p1, p0, Lcom/facebook/react/CoreModulesPackage;->a:LX/33y;

    .line 1535972
    iput-object p2, p0, Lcom/facebook/react/CoreModulesPackage;->b:LX/98l;

    .line 1535973
    iput-object p3, p0, Lcom/facebook/react/CoreModulesPackage;->c:LX/344;

    .line 1535974
    return-void
.end method

.method public static d(Lcom/facebook/react/CoreModulesPackage;LX/5pY;)LX/5rQ;
    .locals 6

    .prologue
    const-wide/16 v4, 0x2000

    .line 1535975
    const-string v0, "CREATE_UI_MANAGER_MODULE_START"

    invoke-static {v0}, Lcom/facebook/react/bridge/ReactMarker;->logMarker(Ljava/lang/String;)V

    .line 1535976
    const-string v0, "createUIManagerModule"

    invoke-static {v4, v5, v0}, LX/018;->a(JLjava/lang/String;)V

    .line 1535977
    :try_start_0
    iget-object v0, p0, Lcom/facebook/react/CoreModulesPackage;->a:LX/33y;

    invoke-virtual {v0, p1}, LX/33y;->a(LX/5pY;)Ljava/util/List;

    move-result-object v0

    .line 1535978
    new-instance v1, LX/5rQ;

    iget-object v2, p0, Lcom/facebook/react/CoreModulesPackage;->c:LX/344;

    invoke-direct {v1, p1, v0, v2}, LX/5rQ;-><init>(LX/5pY;Ljava/util/List;LX/344;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1535979
    invoke-static {v4, v5}, LX/018;->a(J)V

    .line 1535980
    const-string v0, "CREATE_UI_MANAGER_MODULE_END"

    invoke-static {v0}, Lcom/facebook/react/bridge/ReactMarker;->logMarker(Ljava/lang/String;)V

    return-object v1

    .line 1535981
    :catchall_0
    move-exception v0

    invoke-static {v4, v5}, LX/018;->a(J)V

    .line 1535982
    const-string v1, "CREATE_UI_MANAGER_MODULE_END"

    invoke-static {v1}, Lcom/facebook/react/bridge/ReactMarker;->logMarker(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/react/bridge/JavaScriptModule;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1535983
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-class v3, Lcom/facebook/react/modules/core/JSTimersExecution;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/facebook/react/uimanager/events/RCTEventEmitter;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lcom/facebook/react/uimanager/AppRegistry;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-class v3, Lcom/facebook/react/bridge/Systrace;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-class v3, Lcom/facebook/react/devsupport/HMRClient;

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1535984
    sget-boolean v1, LX/0AN;->a:Z

    if-eqz v1, :cond_0

    .line 1535985
    const-class v1, Lcom/facebook/react/uimanager/debug/DebugComponentOwnershipModule$RCTDebugComponentOwnership;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1535986
    const-class v1, Lcom/facebook/react/devsupport/JSCHeapCapture$HeapCapture;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1535987
    const-class v1, Lcom/facebook/react/devsupport/JSCSamplingProfiler$SamplingProfiler;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1535988
    :cond_0
    return-object v0
.end method

.method public final a(LX/5pY;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5pY;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/5pS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1535989
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1535990
    new-instance v1, LX/5pS;

    const-class v2, LX/5qc;

    new-instance v3, LX/9mZ;

    invoke-direct {v3, p0}, LX/9mZ;-><init>(Lcom/facebook/react/CoreModulesPackage;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1535991
    new-instance v1, LX/5pS;

    const-class v2, LX/5qS;

    new-instance v3, LX/9ma;

    invoke-direct {v3, p0, p1}, LX/9ma;-><init>(Lcom/facebook/react/CoreModulesPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1535992
    new-instance v1, LX/5pS;

    const-class v2, LX/9mz;

    new-instance v3, LX/9mb;

    invoke-direct {v3, p0, p1}, LX/9mb;-><init>(Lcom/facebook/react/CoreModulesPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1535993
    new-instance v1, LX/5pS;

    const-class v2, LX/9n0;

    new-instance v3, LX/9mc;

    invoke-direct {v3, p0}, LX/9mc;-><init>(Lcom/facebook/react/CoreModulesPackage;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1535994
    new-instance v1, LX/5pS;

    const-class v2, LX/9n1;

    new-instance v3, LX/9md;

    invoke-direct {v3, p0, p1}, LX/9md;-><init>(Lcom/facebook/react/CoreModulesPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1535995
    new-instance v1, LX/5pS;

    const-class v2, LX/5qY;

    new-instance v3, LX/9me;

    invoke-direct {v3, p0}, LX/9me;-><init>(Lcom/facebook/react/CoreModulesPackage;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1535996
    new-instance v1, LX/5pS;

    const-class v2, LX/9n7;

    new-instance v3, LX/9mf;

    invoke-direct {v3, p0, p1}, LX/9mf;-><init>(Lcom/facebook/react/CoreModulesPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1535997
    new-instance v1, LX/5pS;

    const-class v2, LX/5rQ;

    new-instance v3, LX/9mg;

    invoke-direct {v3, p0, p1}, LX/9mg;-><init>(Lcom/facebook/react/CoreModulesPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1535998
    sget-boolean v1, LX/0AN;->a:Z

    if-eqz v1, :cond_0

    .line 1535999
    new-instance v1, LX/5pS;

    const-class v2, LX/5s5;

    new-instance v3, LX/9mh;

    invoke-direct {v3, p0, p1}, LX/9mh;-><init>(Lcom/facebook/react/CoreModulesPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1536000
    new-instance v1, LX/5pS;

    const-class v2, LX/5qN;

    new-instance v3, LX/9mX;

    invoke-direct {v3, p0, p1}, LX/9mX;-><init>(Lcom/facebook/react/CoreModulesPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1536001
    new-instance v1, LX/5pS;

    const-class v2, LX/5qO;

    new-instance v3, LX/9mY;

    invoke-direct {v3, p0, p1}, LX/9mY;-><init>(Lcom/facebook/react/CoreModulesPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1536002
    :cond_0
    return-object v0
.end method

.method public final b()LX/5qR;
    .locals 1

    .prologue
    .line 1536003
    invoke-static {p0}, LX/9mi;->a(LX/9mi;)LX/5qR;

    move-result-object v0

    return-object v0
.end method
