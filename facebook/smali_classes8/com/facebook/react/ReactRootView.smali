.class public Lcom/facebook/react/ReactRootView;
.super LX/5rH;
.source ""

# interfaces
.implements LX/5rD;


# instance fields
.field public a:LX/33y;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Landroid/os/Bundle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:LX/9mn;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:I

.field private f:Z

.field public g:Z

.field private final h:LX/5qp;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1536175
    invoke-direct {p0, p1}, LX/5rH;-><init>(Landroid/content/Context;)V

    .line 1536176
    iput-boolean v0, p0, Lcom/facebook/react/ReactRootView;->f:Z

    .line 1536177
    iput-boolean v0, p0, Lcom/facebook/react/ReactRootView;->g:Z

    .line 1536178
    new-instance v0, LX/5qp;

    invoke-direct {v0, p0}, LX/5qp;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lcom/facebook/react/ReactRootView;->h:LX/5qp;

    .line 1536179
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1536170
    invoke-direct {p0, p1, p2}, LX/5rH;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1536171
    iput-boolean v0, p0, Lcom/facebook/react/ReactRootView;->f:Z

    .line 1536172
    iput-boolean v0, p0, Lcom/facebook/react/ReactRootView;->g:Z

    .line 1536173
    new-instance v0, LX/5qp;

    invoke-direct {v0, p0}, LX/5qp;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lcom/facebook/react/ReactRootView;->h:LX/5qp;

    .line 1536174
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1536165
    invoke-direct {p0, p1, p2, p3}, LX/5rH;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1536166
    iput-boolean v0, p0, Lcom/facebook/react/ReactRootView;->f:Z

    .line 1536167
    iput-boolean v0, p0, Lcom/facebook/react/ReactRootView;->g:Z

    .line 1536168
    new-instance v0, LX/5qp;

    invoke-direct {v0, p0}, LX/5qp;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lcom/facebook/react/ReactRootView;->h:LX/5qp;

    .line 1536169
    return-void
.end method

.method private b(Landroid/view/MotionEvent;)V
    .locals 2

    .prologue
    .line 1536158
    iget-object v0, p0, Lcom/facebook/react/ReactRootView;->a:LX/33y;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/react/ReactRootView;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/react/ReactRootView;->a:LX/33y;

    invoke-virtual {v0}, LX/33y;->k()LX/5pX;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1536159
    :cond_0
    const-string v0, "React"

    const-string v1, "Unable to dispatch touch to JS as the catalyst instance has not been attached"

    invoke-static {v0, v1}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1536160
    :goto_0
    return-void

    .line 1536161
    :cond_1
    iget-object v0, p0, Lcom/facebook/react/ReactRootView;->a:LX/33y;

    invoke-virtual {v0}, LX/33y;->k()LX/5pX;

    move-result-object v0

    .line 1536162
    const-class v1, LX/5rQ;

    invoke-virtual {v0, v1}, LX/5pX;->b(Ljava/lang/Class;)LX/5p4;

    move-result-object v0

    check-cast v0, LX/5rQ;

    .line 1536163
    iget-object v1, v0, LX/5rQ;->a:LX/5s9;

    move-object v0, v1

    .line 1536164
    iget-object v1, p0, Lcom/facebook/react/ReactRootView;->h:LX/5qp;

    invoke-virtual {v1, p1, v0}, LX/5qp;->b(Landroid/view/MotionEvent;LX/5s9;)V

    goto :goto_0
.end method

.method public static b(Lcom/facebook/react/ReactRootView;)V
    .locals 2

    .prologue
    .line 1536153
    iget-boolean v0, p0, Lcom/facebook/react/ReactRootView;->g:Z

    if-eqz v0, :cond_0

    .line 1536154
    :goto_0
    return-void

    .line 1536155
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/react/ReactRootView;->g:Z

    .line 1536156
    iget-object v0, p0, Lcom/facebook/react/ReactRootView;->a:LX/33y;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/33y;

    invoke-virtual {v0, p0}, LX/33y;->a(Lcom/facebook/react/ReactRootView;)V

    .line 1536157
    invoke-virtual {p0}, Lcom/facebook/react/ReactRootView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-direct {p0}, Lcom/facebook/react/ReactRootView;->getCustomGlobalLayoutListener()LX/9mn;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0
.end method

.method private getCustomGlobalLayoutListener()LX/9mn;
    .locals 1

    .prologue
    .line 1536150
    iget-object v0, p0, Lcom/facebook/react/ReactRootView;->d:LX/9mn;

    if-nez v0, :cond_0

    .line 1536151
    new-instance v0, LX/9mn;

    invoke-direct {v0, p0}, LX/9mn;-><init>(Lcom/facebook/react/ReactRootView;)V

    iput-object v0, p0, Lcom/facebook/react/ReactRootView;->d:LX/9mn;

    .line 1536152
    :cond_0
    iget-object v0, p0, Lcom/facebook/react/ReactRootView;->d:LX/9mn;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1536146
    iget-object v0, p0, Lcom/facebook/react/ReactRootView;->a:LX/33y;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/react/ReactRootView;->g:Z

    if-eqz v0, :cond_0

    .line 1536147
    iget-object v0, p0, Lcom/facebook/react/ReactRootView;->a:LX/33y;

    invoke-virtual {v0, p0}, LX/33y;->b(Lcom/facebook/react/ReactRootView;)V

    .line 1536148
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/react/ReactRootView;->g:Z

    .line 1536149
    :cond_0
    return-void
.end method

.method public final a(LX/33y;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1536144
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/facebook/react/ReactRootView;->a(LX/33y;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1536145
    return-void
.end method

.method public final a(LX/33y;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1536133
    invoke-static {}, LX/5pe;->b()V

    .line 1536134
    iget-object v0, p0, Lcom/facebook/react/ReactRootView;->a:LX/33y;

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    const-string v1, "This root view has already been attached to a catalyst instance manager"

    invoke-static {v0, v1}, LX/0nE;->a(ZLjava/lang/String;)V

    .line 1536135
    iput-object p1, p0, Lcom/facebook/react/ReactRootView;->a:LX/33y;

    .line 1536136
    iput-object p2, p0, Lcom/facebook/react/ReactRootView;->b:Ljava/lang/String;

    .line 1536137
    iput-object p3, p0, Lcom/facebook/react/ReactRootView;->c:Landroid/os/Bundle;

    .line 1536138
    iget-object v0, p0, Lcom/facebook/react/ReactRootView;->a:LX/33y;

    invoke-virtual {v0}, LX/33y;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1536139
    iget-object v0, p0, Lcom/facebook/react/ReactRootView;->a:LX/33y;

    invoke-virtual {v0}, LX/33y;->c()V

    .line 1536140
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/react/ReactRootView;->f:Z

    if-eqz v0, :cond_1

    .line 1536141
    invoke-static {p0}, Lcom/facebook/react/ReactRootView;->b(Lcom/facebook/react/ReactRootView;)V

    .line 1536142
    :cond_1
    return-void

    .line 1536143
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/MotionEvent;)V
    .locals 2

    .prologue
    .line 1536126
    iget-object v0, p0, Lcom/facebook/react/ReactRootView;->a:LX/33y;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/react/ReactRootView;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/react/ReactRootView;->a:LX/33y;

    invoke-virtual {v0}, LX/33y;->k()LX/5pX;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1536127
    :cond_0
    const-string v0, "React"

    const-string v1, "Unable to dispatch touch to JS as the catalyst instance has not been attached"

    invoke-static {v0, v1}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1536128
    :goto_0
    return-void

    .line 1536129
    :cond_1
    iget-object v0, p0, Lcom/facebook/react/ReactRootView;->a:LX/33y;

    invoke-virtual {v0}, LX/33y;->k()LX/5pX;

    move-result-object v0

    .line 1536130
    const-class v1, LX/5rQ;

    invoke-virtual {v0, v1}, LX/5pX;->b(Ljava/lang/Class;)LX/5p4;

    move-result-object v0

    check-cast v0, LX/5rQ;

    .line 1536131
    iget-object v1, v0, LX/5rQ;->a:LX/5s9;

    move-object v0, v1

    .line 1536132
    iget-object v1, p0, Lcom/facebook/react/ReactRootView;->h:LX/5qp;

    invoke-virtual {v1, p1, v0}, LX/5qp;->a(Landroid/view/MotionEvent;LX/5s9;)V

    goto :goto_0
.end method

.method public final finalize()V
    .locals 2

    .prologue
    .line 1536095
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 1536096
    iget-boolean v0, p0, Lcom/facebook/react/ReactRootView;->g:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "The application this ReactRootView was rendering was not unmounted before the ReactRootView was garbage collected. This usually means that your application is leaking large amounts of memory. To solve this, make sure to call ReactRootView#unmountReactApplication in the onDestroy() of your hosting Activity or in the onDestroyView() of your hosting Fragment."

    invoke-static {v0, v1}, LX/0nE;->a(ZLjava/lang/String;)V

    .line 1536097
    return-void

    .line 1536098
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getJSModuleName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1536125
    iget-object v0, p0, Lcom/facebook/react/ReactRootView;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getLaunchOptions()Landroid/os/Bundle;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1536124
    iget-object v0, p0, Lcom/facebook/react/ReactRootView;->c:Landroid/os/Bundle;

    return-object v0
.end method

.method public getRootViewTag()I
    .locals 1

    .prologue
    .line 1536123
    iget v0, p0, Lcom/facebook/react/ReactRootView;->e:I

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x4c660057

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1536119
    invoke-super {p0}, LX/5rH;->onAttachedToWindow()V

    .line 1536120
    iget-boolean v1, p0, Lcom/facebook/react/ReactRootView;->g:Z

    if-eqz v1, :cond_0

    .line 1536121
    invoke-virtual {p0}, Lcom/facebook/react/ReactRootView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    invoke-direct {p0}, Lcom/facebook/react/ReactRootView;->getCustomGlobalLayoutListener()LX/9mn;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1536122
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x727ca48f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x361f327c    # -1841584.5f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1536115
    invoke-super {p0}, LX/5rH;->onDetachedFromWindow()V

    .line 1536116
    iget-boolean v1, p0, Lcom/facebook/react/ReactRootView;->g:Z

    if-eqz v1, :cond_0

    .line 1536117
    invoke-virtual {p0}, Lcom/facebook/react/ReactRootView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    invoke-direct {p0}, Lcom/facebook/react/ReactRootView;->getCustomGlobalLayoutListener()LX/9mn;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1536118
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x71c374fb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1536113
    invoke-direct {p0, p1}, Lcom/facebook/react/ReactRootView;->b(Landroid/view/MotionEvent;)V

    .line 1536114
    invoke-super {p0, p1}, LX/5rH;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final onLayout(ZIIII)V
    .locals 0

    .prologue
    .line 1536112
    return-void
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 1536107
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/react/ReactRootView;->setMeasuredDimension(II)V

    .line 1536108
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/react/ReactRootView;->f:Z

    .line 1536109
    iget-object v0, p0, Lcom/facebook/react/ReactRootView;->a:LX/33y;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/react/ReactRootView;->g:Z

    if-nez v0, :cond_0

    .line 1536110
    new-instance v0, Lcom/facebook/react/ReactRootView$1;

    invoke-direct {v0, p0}, Lcom/facebook/react/ReactRootView$1;-><init>(Lcom/facebook/react/ReactRootView;)V

    invoke-static {v0}, LX/5pe;->a(Ljava/lang/Runnable;)V

    .line 1536111
    :cond_0
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x2

    const v0, 0xb32ff6a

    invoke-static {v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1536104
    invoke-direct {p0, p1}, Lcom/facebook/react/ReactRootView;->b(Landroid/view/MotionEvent;)V

    .line 1536105
    invoke-super {p0, p1}, LX/5rH;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1536106
    const v1, -0x3f194b15

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v3
.end method

.method public final requestDisallowInterceptTouchEvent(Z)V
    .locals 1

    .prologue
    .line 1536101
    invoke-virtual {p0}, Lcom/facebook/react/ReactRootView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1536102
    invoke-virtual {p0}, Lcom/facebook/react/ReactRootView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 1536103
    :cond_0
    return-void
.end method

.method public setRootViewTag(I)V
    .locals 0

    .prologue
    .line 1536099
    iput p1, p0, Lcom/facebook/react/ReactRootView;->e:I

    .line 1536100
    return-void
.end method
