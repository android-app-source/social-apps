.class public Lcom/facebook/react/views/view/ReactViewManager;
.super Lcom/facebook/react/uimanager/ViewGroupManager;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "RCTView"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/react/uimanager/ViewGroupManager",
        "<",
        "LX/9na;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1537983
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/facebook/react/views/view/ReactViewManager;->b:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x8
        0x0
        0x2
        0x1
        0x3
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1537984
    invoke-direct {p0}, Lcom/facebook/react/uimanager/ViewGroupManager;-><init>()V

    return-void
.end method

.method private static a(LX/9na;)I
    .locals 1

    .prologue
    .line 1537985
    invoke-virtual {p0}, LX/9na;->getRemoveClippedSubviews()Z

    move-result v0

    .line 1537986
    if-eqz v0, :cond_0

    .line 1537987
    iget v0, p0, LX/9na;->e:I

    move v0, v0

    .line 1537988
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, LX/9na;->getChildCount()I

    move-result v0

    goto :goto_0
.end method

.method private static a(LX/9na;I)Landroid/view/View;
    .locals 1

    .prologue
    .line 1537989
    invoke-virtual {p0}, LX/9na;->getRemoveClippedSubviews()Z

    move-result v0

    .line 1537990
    if-eqz v0, :cond_0

    .line 1537991
    invoke-virtual {p0, p1}, LX/9na;->a(I)Landroid/view/View;

    move-result-object v0

    .line 1537992
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, LX/9na;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/9na;ILX/5pC;)V
    .locals 4
    .param p2    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1537993
    packed-switch p1, :pswitch_data_0

    .line 1537994
    :cond_0
    :goto_0
    return-void

    .line 1537995
    :pswitch_0
    if-eqz p2, :cond_1

    invoke-interface {p2}, LX/5pC;->size()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    .line 1537996
    :cond_1
    new-instance v0, LX/5pA;

    const-string v1, "Illegal number of arguments for \'updateHotspot\' command"

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1537997
    :cond_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 1537998
    invoke-interface {p2, v2}, LX/5pC;->getDouble(I)D

    move-result-wide v0

    invoke-static {v0, v1}, LX/5r2;->a(D)F

    move-result v0

    .line 1537999
    invoke-interface {p2, v3}, LX/5pC;->getDouble(I)D

    move-result-wide v2

    invoke-static {v2, v3}, LX/5r2;->a(D)F

    move-result v1

    .line 1538000
    invoke-virtual {p0, v0, v1}, LX/9na;->drawableHotspotChanged(FF)V

    goto :goto_0

    .line 1538001
    :pswitch_1
    if-eqz p2, :cond_3

    invoke-interface {p2}, LX/5pC;->size()I

    move-result v0

    if-eq v0, v3, :cond_4

    .line 1538002
    :cond_3
    new-instance v0, LX/5pA;

    const-string v1, "Illegal number of arguments for \'setPressed\' command"

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1538003
    :cond_4
    invoke-interface {p2, v2}, LX/5pC;->getBoolean(I)Z

    move-result v0

    invoke-virtual {p0, v0}, LX/9na;->setPressed(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(LX/9na;Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 1538004
    invoke-virtual {p0}, LX/9na;->getRemoveClippedSubviews()Z

    move-result v0

    .line 1538005
    if-eqz v0, :cond_0

    .line 1538006
    invoke-static {p0, p1, p2}, LX/9na;->b(LX/9na;Landroid/view/View;I)V

    .line 1538007
    :goto_0
    invoke-static {p0}, Lcom/facebook/react/uimanager/ViewGroupManager;->a(Landroid/view/ViewGroup;)V

    .line 1538008
    return-void

    .line 1538009
    :cond_0
    invoke-virtual {p0, p1, p2}, LX/9na;->addView(Landroid/view/View;I)V

    goto :goto_0
.end method

.method private static b(LX/5rJ;)LX/9na;
    .locals 1

    .prologue
    .line 1538010
    new-instance v0, LX/9na;

    invoke-direct {v0, p0}, LX/9na;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private static b(LX/9na;)V
    .locals 1

    .prologue
    .line 1538011
    invoke-virtual {p0}, LX/9na;->getRemoveClippedSubviews()Z

    move-result v0

    .line 1538012
    if-eqz v0, :cond_0

    .line 1538013
    invoke-virtual {p0}, LX/9na;->b()V

    .line 1538014
    :goto_0
    return-void

    .line 1538015
    :cond_0
    invoke-virtual {p0}, LX/9na;->removeAllViews()V

    goto :goto_0
.end method

.method private static b(LX/9na;I)V
    .locals 2

    .prologue
    .line 1538016
    invoke-virtual {p0}, LX/9na;->getRemoveClippedSubviews()Z

    move-result v0

    .line 1538017
    if-eqz v0, :cond_1

    .line 1538018
    invoke-static {p0, p1}, Lcom/facebook/react/views/view/ReactViewManager;->a(LX/9na;I)Landroid/view/View;

    move-result-object v0

    .line 1538019
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1538020
    invoke-virtual {p0, v0}, LX/9na;->removeView(Landroid/view/View;)V

    .line 1538021
    :cond_0
    invoke-virtual {p0, v0}, LX/9na;->a(Landroid/view/View;)V

    .line 1538022
    :goto_0
    return-void

    .line 1538023
    :cond_1
    invoke-virtual {p0, p1}, LX/9na;->removeViewAt(I)V

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(LX/5rJ;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1538024
    invoke-static {p1}, Lcom/facebook/react/views/view/ReactViewManager;->b(LX/5rJ;)LX/9na;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 1

    .prologue
    .line 1538025
    check-cast p1, LX/9na;

    invoke-static {p1, p2}, Lcom/facebook/react/views/view/ReactViewManager;->a(LX/9na;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Landroid/view/View;ILX/5pC;)V
    .locals 0
    .param p3    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1537937
    check-cast p1, LX/9na;

    invoke-static {p1, p2, p3}, Lcom/facebook/react/views/view/ReactViewManager;->a(LX/9na;ILX/5pC;)V

    return-void
.end method

.method public final bridge synthetic a(Landroid/view/ViewGroup;Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 1538026
    check-cast p1, LX/9na;

    invoke-static {p1, p2, p3}, Lcom/facebook/react/views/view/ReactViewManager;->a(LX/9na;Landroid/view/View;I)V

    return-void
.end method

.method public final synthetic b(Landroid/view/ViewGroup;)I
    .locals 1

    .prologue
    .line 1538027
    check-cast p1, LX/9na;

    invoke-static {p1}, Lcom/facebook/react/views/view/ReactViewManager;->a(LX/9na;)I

    move-result v0

    return v0
.end method

.method public final bridge synthetic b(Landroid/view/ViewGroup;I)V
    .locals 0

    .prologue
    .line 1537982
    check-cast p1, LX/9na;

    invoke-static {p1, p2}, Lcom/facebook/react/views/view/ReactViewManager;->b(LX/9na;I)V

    return-void
.end method

.method public final synthetic c(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 1538028
    check-cast p1, LX/9na;

    invoke-static {p1}, Lcom/facebook/react/views/view/ReactViewManager;->b(LX/9na;)V

    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1537946
    const-string v0, "RCTView"

    return-object v0
.end method

.method public setAccessible(LX/9na;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "accessible"
    .end annotation

    .prologue
    .line 1537938
    invoke-virtual {p1, p2}, LX/9na;->setFocusable(Z)V

    .line 1537939
    return-void
.end method

.method public setBorderColor(LX/9na;ILjava/lang/Integer;)V
    .locals 3
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactPropGroup;
        a = {
            "borderColor",
            "borderLeftColor",
            "borderRightColor",
            "borderTopColor",
            "borderBottomColor"
        }
        customType = "Color"
    .end annotation

    .prologue
    const/high16 v0, 0x7fc00000    # NaNf

    .line 1537940
    if-nez p3, :cond_0

    move v1, v0

    .line 1537941
    :goto_0
    if-nez p3, :cond_1

    .line 1537942
    :goto_1
    sget-object v2, Lcom/facebook/react/views/view/ReactViewManager;->b:[I

    aget v2, v2, p2

    invoke-virtual {p1, v2, v1, v0}, LX/9na;->a(IFF)V

    .line 1537943
    return-void

    .line 1537944
    :cond_0
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const v2, 0xffffff

    and-int/2addr v1, v2

    int-to-float v1, v1

    goto :goto_0

    .line 1537945
    :cond_1
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    ushr-int/lit8 v0, v0, 0x18

    int-to-float v0, v0

    goto :goto_1
.end method

.method public setBorderRadius(LX/9na;IF)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactPropGroup;
        a = {
            "borderRadius",
            "borderTopLeftRadius",
            "borderTopRightRadius",
            "borderBottomRightRadius",
            "borderBottomLeftRadius"
        }
        b = NaNf
    .end annotation

    .prologue
    .line 1537947
    invoke-static {p3}, LX/1mo;->a(F)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1537948
    invoke-static {p3}, LX/5r2;->a(F)F

    move-result p3

    .line 1537949
    :cond_0
    if-nez p2, :cond_1

    .line 1537950
    invoke-virtual {p1, p3}, LX/9na;->setBorderRadius(F)V

    .line 1537951
    :goto_0
    return-void

    .line 1537952
    :cond_1
    add-int/lit8 v0, p2, -0x1

    invoke-virtual {p1, p3, v0}, LX/9na;->a(FI)V

    goto :goto_0
.end method

.method public setBorderStyle(LX/9na;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "borderStyle"
    .end annotation

    .prologue
    .line 1537953
    invoke-virtual {p1, p2}, LX/9na;->setBorderStyle(Ljava/lang/String;)V

    .line 1537954
    return-void
.end method

.method public setBorderWidth(LX/9na;IF)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactPropGroup;
        a = {
            "borderWidth",
            "borderLeftWidth",
            "borderRightWidth",
            "borderTopWidth",
            "borderBottomWidth"
        }
        b = NaNf
    .end annotation

    .prologue
    .line 1537955
    invoke-static {p3}, LX/1mo;->a(F)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1537956
    invoke-static {p3}, LX/5r2;->a(F)F

    move-result p3

    .line 1537957
    :cond_0
    sget-object v0, Lcom/facebook/react/views/view/ReactViewManager;->b:[I

    aget v0, v0, p2

    invoke-virtual {p1, v0, p3}, LX/9na;->a(IF)V

    .line 1537958
    return-void
.end method

.method public setCollapsable(LX/9na;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "collapsable"
    .end annotation

    .prologue
    .line 1537959
    return-void
.end method

.method public setHitSlop(LX/9na;LX/5pG;)V
    .locals 8
    .param p2    # LX/5pG;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "hitSlop"
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1537960
    if-nez p2, :cond_0

    .line 1537961
    const/4 v0, 0x0

    .line 1537962
    iput-object v0, p1, LX/9na;->g:Landroid/graphics/Rect;

    .line 1537963
    :goto_0
    return-void

    .line 1537964
    :cond_0
    new-instance v4, Landroid/graphics/Rect;

    const-string v0, "left"

    invoke-interface {p2, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "left"

    invoke-interface {p2, v0}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v2, v3}, LX/5r2;->a(D)F

    move-result v0

    float-to-int v0, v0

    :goto_1
    const-string v2, "top"

    invoke-interface {p2, v2}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "top"

    invoke-interface {p2, v2}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v2, v3}, LX/5r2;->a(D)F

    move-result v2

    float-to-int v2, v2

    :goto_2
    const-string v3, "right"

    invoke-interface {p2, v3}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "right"

    invoke-interface {p2, v3}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v6

    invoke-static {v6, v7}, LX/5r2;->a(D)F

    move-result v3

    float-to-int v3, v3

    :goto_3
    const-string v5, "bottom"

    invoke-interface {p2, v5}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v1, "bottom"

    invoke-interface {p2, v1}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v6

    invoke-static {v6, v7}, LX/5r2;->a(D)F

    move-result v1

    float-to-int v1, v1

    :cond_1
    invoke-direct {v4, v0, v2, v3, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1537965
    iput-object v4, p1, LX/9na;->g:Landroid/graphics/Rect;

    .line 1537966
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v2, v1

    goto :goto_2

    :cond_4
    move v3, v1

    goto :goto_3
.end method

.method public setNativeBackground(LX/9na;LX/5pG;)V
    .locals 1
    .param p2    # LX/5pG;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "nativeBackgroundAndroid"
    .end annotation

    .prologue
    .line 1537967
    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, LX/9na;->setTranslucentBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1537968
    return-void

    .line 1537969
    :cond_0
    invoke-virtual {p1}, LX/9na;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p2}, LX/9nV;->a(Landroid/content/Context;LX/5pG;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public setNativeForeground(LX/9na;LX/5pG;)V
    .locals 1
    .param p2    # LX/5pG;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "nativeForegroundAndroid"
    .end annotation

    .prologue
    .line 1537970
    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, LX/9na;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 1537971
    return-void

    .line 1537972
    :cond_0
    invoke-virtual {p1}, LX/9na;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p2}, LX/9nV;->a(Landroid/content/Context;LX/5pG;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public setNeedsOffscreenAlphaCompositing(LX/9na;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "needsOffscreenAlphaCompositing"
    .end annotation

    .prologue
    .line 1537973
    iput-boolean p2, p1, LX/9na;->l:Z

    .line 1537974
    return-void
.end method

.method public setPointerEvents(LX/9na;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "pointerEvents"
    .end annotation

    .prologue
    .line 1537975
    if-eqz p2, :cond_0

    .line 1537976
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p2, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "-"

    const-string v2, "_"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/5r3;->valueOf(Ljava/lang/String;)LX/5r3;

    move-result-object v0

    .line 1537977
    iput-object v0, p1, LX/9na;->h:LX/5r3;

    .line 1537978
    :cond_0
    return-void
.end method

.method public setRemoveClippedSubviews(LX/9na;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "removeClippedSubviews"
    .end annotation

    .prologue
    .line 1537979
    invoke-virtual {p1, p2}, LX/9na;->setRemoveClippedSubviews(Z)V

    .line 1537980
    return-void
.end method

.method public final v()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1537981
    const-string v0, "hotspotUpdate"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "setPressed"

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
