.class public final Lcom/facebook/react/modules/core/Timing$IdleCallbackRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/9n7;

.field private volatile b:Z

.field private final c:J


# direct methods
.method public constructor <init>(LX/9n7;J)V
    .locals 2

    .prologue
    .line 1536630
    iput-object p1, p0, Lcom/facebook/react/modules/core/Timing$IdleCallbackRunnable;->a:LX/9n7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1536631
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/react/modules/core/Timing$IdleCallbackRunnable;->b:Z

    .line 1536632
    iput-wide p2, p0, Lcom/facebook/react/modules/core/Timing$IdleCallbackRunnable;->c:J

    .line 1536633
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1536634
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/react/modules/core/Timing$IdleCallbackRunnable;->b:Z

    .line 1536635
    return-void
.end method

.method public final run()V
    .locals 6

    .prologue
    .line 1536636
    iget-boolean v0, p0, Lcom/facebook/react/modules/core/Timing$IdleCallbackRunnable;->b:Z

    if-eqz v0, :cond_1

    .line 1536637
    :cond_0
    :goto_0
    return-void

    .line 1536638
    :cond_1
    iget-wide v0, p0, Lcom/facebook/react/modules/core/Timing$IdleCallbackRunnable;->c:J

    const-wide/32 v2, 0xf4240

    div-long/2addr v0, v2

    .line 1536639
    invoke-static {}, LX/5pv;->c()J

    move-result-wide v2

    .line 1536640
    sub-long v0, v2, v0

    .line 1536641
    invoke-static {}, LX/5pv;->a()J

    move-result-wide v2

    .line 1536642
    sub-long/2addr v2, v0

    .line 1536643
    const v4, 0x41855555

    long-to-float v0, v0

    sub-float v0, v4, v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_0

    .line 1536644
    iget-object v0, p0, Lcom/facebook/react/modules/core/Timing$IdleCallbackRunnable;->a:LX/9n7;

    iget-object v0, v0, LX/9n7;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1536645
    iget-object v0, p0, Lcom/facebook/react/modules/core/Timing$IdleCallbackRunnable;->a:LX/9n7;

    iget-object v1, v0, LX/9n7;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 1536646
    :try_start_0
    iget-object v0, p0, Lcom/facebook/react/modules/core/Timing$IdleCallbackRunnable;->a:LX/9n7;

    iget-object v0, v0, LX/9n7;->o:Ljava/util/List;

    iget-object v4, p0, Lcom/facebook/react/modules/core/Timing$IdleCallbackRunnable;->a:LX/9n7;

    iget-object v4, v4, LX/9n7;->n:Ljava/util/Set;

    invoke-interface {v0, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1536647
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1536648
    iget-object v0, p0, Lcom/facebook/react/modules/core/Timing$IdleCallbackRunnable;->a:LX/9n7;

    iget-object v0, v0, LX/9n7;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/bridge/ExecutorToken;

    .line 1536649
    iget-object v4, p0, Lcom/facebook/react/modules/core/Timing$IdleCallbackRunnable;->a:LX/9n7;

    invoke-static {v4}, LX/9n7;->m(LX/9n7;)LX/5pY;

    move-result-object v4

    const-class v5, Lcom/facebook/react/modules/core/JSTimersExecution;

    invoke-virtual {v4, v0, v5}, LX/5pX;->a(Lcom/facebook/react/bridge/ExecutorToken;Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/modules/core/JSTimersExecution;

    long-to-double v4, v2

    invoke-interface {v0, v4, v5}, Lcom/facebook/react/modules/core/JSTimersExecution;->callIdleCallbacks(D)V

    goto :goto_1

    .line 1536650
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1536651
    :cond_2
    iget-object v0, p0, Lcom/facebook/react/modules/core/Timing$IdleCallbackRunnable;->a:LX/9n7;

    const/4 v1, 0x0

    .line 1536652
    iput-object v1, v0, LX/9n7;->j:Lcom/facebook/react/modules/core/Timing$IdleCallbackRunnable;

    .line 1536653
    goto :goto_0
.end method
