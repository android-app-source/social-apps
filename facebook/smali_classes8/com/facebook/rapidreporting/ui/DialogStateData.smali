.class public Lcom/facebook/rapidreporting/ui/DialogStateData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/rapidreporting/ui/DialogStateData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/rapidreporting/ui/DialogConfig;

.field public b:LX/9mN;

.field public c:Lcom/facebook/graphql/executor/GraphQLResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingTagsQueryModel;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/facebook/graphql/executor/GraphQLResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:Z

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/rapidreporting/ui/GuidedActionItem;",
            ">;"
        }
    .end annotation
.end field

.field public m:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1535114
    new-instance v0, LX/9mB;

    invoke-direct {v0}, LX/9mB;-><init>()V

    sput-object v0, Lcom/facebook/rapidreporting/ui/DialogStateData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 1535170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1535171
    sget-object v0, LX/9mN;->FEEDBACK:LX/9mN;

    iput-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->b:LX/9mN;

    .line 1535172
    iput-object v2, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->e:Ljava/lang/String;

    .line 1535173
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->f:Ljava/util/List;

    .line 1535174
    iput-boolean v1, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->g:Z

    .line 1535175
    iput-object v2, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->h:Ljava/lang/String;

    .line 1535176
    iput-boolean v1, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->i:Z

    .line 1535177
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->j:Z

    .line 1535178
    iput-boolean v1, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->k:Z

    .line 1535179
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->l:Ljava/util/List;

    .line 1535180
    iput-boolean v1, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->m:Z

    .line 1535181
    const-class v0, Lcom/facebook/rapidreporting/ui/DialogConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/rapidreporting/ui/DialogConfig;

    iput-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->a:Lcom/facebook/rapidreporting/ui/DialogConfig;

    .line 1535182
    invoke-static {}, LX/9mN;->values()[LX/9mN;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->b:LX/9mN;

    .line 1535183
    const-class v0, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    iput-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->c:Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1535184
    const-class v0, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    iput-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->d:Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1535185
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->e:Ljava/lang/String;

    .line 1535186
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->f:Ljava/util/List;

    .line 1535187
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->f:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 1535188
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->g:Z

    .line 1535189
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->h:Ljava/lang/String;

    .line 1535190
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->i:Z

    .line 1535191
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->j:Z

    .line 1535192
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->k:Z

    .line 1535193
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->l:Ljava/util/List;

    .line 1535194
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->l:Ljava/util/List;

    const-class v1, Lcom/facebook/rapidreporting/ui/GuidedActionItem;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 1535195
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->m:Z

    .line 1535196
    return-void
.end method

.method public constructor <init>(Lcom/facebook/rapidreporting/ui/DialogConfig;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1535157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1535158
    sget-object v0, LX/9mN;->FEEDBACK:LX/9mN;

    iput-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->b:LX/9mN;

    .line 1535159
    iput-object v2, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->e:Ljava/lang/String;

    .line 1535160
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->f:Ljava/util/List;

    .line 1535161
    iput-boolean v1, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->g:Z

    .line 1535162
    iput-object v2, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->h:Ljava/lang/String;

    .line 1535163
    iput-boolean v1, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->i:Z

    .line 1535164
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->j:Z

    .line 1535165
    iput-boolean v1, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->k:Z

    .line 1535166
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->l:Ljava/util/List;

    .line 1535167
    iput-boolean v1, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->m:Z

    .line 1535168
    iput-object p1, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->a:Lcom/facebook/rapidreporting/ui/DialogConfig;

    .line 1535169
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1535154
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1535155
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1535156
    :cond_0
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1535151
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->a:Lcom/facebook/rapidreporting/ui/DialogConfig;

    .line 1535152
    iget-object p0, v0, Lcom/facebook/rapidreporting/ui/DialogConfig;->a:Ljava/lang/String;

    move-object v0, p0

    .line 1535153
    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1535149
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1535150
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1535146
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->a:Lcom/facebook/rapidreporting/ui/DialogConfig;

    .line 1535147
    iget-object p0, v0, Lcom/facebook/rapidreporting/ui/DialogConfig;->b:Ljava/lang/String;

    move-object v0, p0

    .line 1535148
    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1535145
    const/4 v0, 0x0

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1535140
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->c:Lcom/facebook/graphql/executor/GraphQLResult;

    if-nez v0, :cond_0

    .line 1535141
    const/4 v0, 0x0

    .line 1535142
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->c:Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1535143
    iget-object p0, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, p0

    .line 1535144
    check-cast v0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingTagsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingTagsQueryModel;->a()Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel;->l()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final o()Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1535135
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->c:Lcom/facebook/graphql/executor/GraphQLResult;

    if-nez v0, :cond_0

    .line 1535136
    const/4 v0, 0x0

    .line 1535137
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->c:Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1535138
    iget-object v1, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v1

    .line 1535139
    check-cast v0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingTagsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingTagsQueryModel;->a()Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel;->p()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final q()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$ReportTagsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1535130
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->c:Lcom/facebook/graphql/executor/GraphQLResult;

    if-nez v0, :cond_0

    .line 1535131
    const/4 v0, 0x0

    .line 1535132
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->c:Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1535133
    iget-object p0, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, p0

    .line 1535134
    check-cast v0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingTagsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingTagsQueryModel;->a()Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel;->o()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1535116
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->a:Lcom/facebook/rapidreporting/ui/DialogConfig;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1535117
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->b:LX/9mN;

    invoke-virtual {v0}, LX/9mN;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1535118
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->c:Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1535119
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->d:Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1535120
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1535121
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->f:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1535122
    iget-boolean v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->g:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 1535123
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1535124
    iget-boolean v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->i:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 1535125
    iget-boolean v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->j:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 1535126
    iget-boolean v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->k:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 1535127
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->l:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1535128
    iget-boolean v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->m:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 1535129
    return-void
.end method

.method public final y()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/rapidreporting/ui/GuidedActionItem;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1535115
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogStateData;->l:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
