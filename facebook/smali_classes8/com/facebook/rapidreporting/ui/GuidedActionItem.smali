.class public Lcom/facebook/rapidreporting/ui/GuidedActionItem;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/rapidreporting/ui/GuidedActionItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field public g:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field private h:Z

.field public i:LX/9To;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1535200
    new-instance v0, LX/9mC;

    invoke-direct {v0}, LX/9mC;-><init>()V

    sput-object v0, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1535201
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1535202
    sget-object v0, LX/9To;->INITIAL:LX/9To;

    iput-object v0, p0, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->i:LX/9To;

    .line 1535203
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->a:Ljava/lang/String;

    .line 1535204
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->b:Ljava/lang/String;

    .line 1535205
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->c:Ljava/lang/String;

    .line 1535206
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->d:Ljava/lang/String;

    .line 1535207
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->e:Ljava/lang/String;

    .line 1535208
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->f:Ljava/lang/String;

    .line 1535209
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->values()[Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->g:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 1535210
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->h:Z

    .line 1535211
    invoke-static {}, LX/9To;->values()[LX/9To;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->i:LX/9To;

    .line 1535212
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;Z)V
    .locals 1

    .prologue
    .line 1535213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1535214
    sget-object v0, LX/9To;->INITIAL:LX/9To;

    iput-object v0, p0, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->i:LX/9To;

    .line 1535215
    iput-object p1, p0, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->a:Ljava/lang/String;

    .line 1535216
    iput-object p2, p0, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->b:Ljava/lang/String;

    .line 1535217
    iput-object p3, p0, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->c:Ljava/lang/String;

    .line 1535218
    iput-object p4, p0, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->d:Ljava/lang/String;

    .line 1535219
    iput-object p5, p0, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->e:Ljava/lang/String;

    .line 1535220
    iput-object p6, p0, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->f:Ljava/lang/String;

    .line 1535221
    iput-object p7, p0, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->g:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 1535222
    iput-boolean p8, p0, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->h:Z

    .line 1535223
    iget-boolean v0, p0, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->h:Z

    if-eqz v0, :cond_0

    .line 1535224
    sget-object v0, LX/9To;->COMPLETED:LX/9To;

    iput-object v0, p0, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->i:LX/9To;

    .line 1535225
    :cond_0
    return-void
.end method

.method public static a(Ljava/util/List;)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$FollowupActionsModel;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/rapidreporting/ui/GuidedActionItem;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v13, 0x0

    .line 1535226
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1535227
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$FollowupActionsModel;

    .line 1535228
    sget-object v0, LX/9Tp;->a:LX/0Rf;

    invoke-virtual {v8}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$FollowupActionsModel;->n()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1535229
    invoke-virtual {v8}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$FollowupActionsModel;->p()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    .line 1535230
    invoke-virtual {v8}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$FollowupActionsModel;->o()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v5, v0, LX/1vs;->b:I

    .line 1535231
    invoke-virtual {v8}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$FollowupActionsModel;->l()LX/1vs;

    move-result-object v0

    iget-object v6, v0, LX/1vs;->a:LX/15i;

    iget v7, v0, LX/1vs;->b:I

    .line 1535232
    invoke-virtual {v8}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$FollowupActionsModel;->k()LX/1vs;

    move-result-object v0

    iget-object v11, v0, LX/1vs;->a:LX/15i;

    iget v12, v0, LX/1vs;->b:I

    .line 1535233
    new-instance v0, Lcom/facebook/rapidreporting/ui/GuidedActionItem;

    invoke-virtual {v8}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$FollowupActionsModel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v13}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v5, v13}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v7, v13}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v11, v12, v13}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$FollowupActionsModel;->q()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$FollowupActionsModel;->n()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v7

    invoke-virtual {v8}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$FollowupActionsModel;->j()Z

    move-result v8

    invoke-direct/range {v0 .. v8}, Lcom/facebook/rapidreporting/ui/GuidedActionItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;Z)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1535234
    :cond_1
    return-object v9
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1535235
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1535236
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1535237
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1535238
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1535239
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1535240
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1535241
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1535242
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->g:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1535243
    iget-boolean v0, p0, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->h:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 1535244
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/GuidedActionItem;->i:LX/9To;

    invoke-virtual {v0}, LX/9To;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1535245
    return-void
.end method
