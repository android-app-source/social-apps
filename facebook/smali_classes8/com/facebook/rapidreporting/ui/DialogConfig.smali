.class public Lcom/facebook/rapidreporting/ui/DialogConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/rapidreporting/ui/DialogConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:LX/9lZ;

.field public final d:LX/Js1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1535094
    new-instance v0, LX/9m9;

    invoke-direct {v0}, LX/9m9;-><init>()V

    sput-object v0, Lcom/facebook/rapidreporting/ui/DialogConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/9mA;)V
    .locals 1

    .prologue
    .line 1535095
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1535096
    iget-object v0, p1, LX/9mA;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogConfig;->a:Ljava/lang/String;

    .line 1535097
    iget-object v0, p1, LX/9mA;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogConfig;->b:Ljava/lang/String;

    .line 1535098
    iget-object v0, p1, LX/9mA;->c:LX/9lZ;

    iput-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogConfig;->c:LX/9lZ;

    .line 1535099
    iget-object v0, p1, LX/9mA;->d:LX/Js1;

    iput-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogConfig;->d:LX/Js1;

    .line 1535100
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1535101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1535102
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogConfig;->a:Ljava/lang/String;

    .line 1535103
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogConfig;->b:Ljava/lang/String;

    .line 1535104
    iput-object v1, p0, Lcom/facebook/rapidreporting/ui/DialogConfig;->c:LX/9lZ;

    .line 1535105
    iput-object v1, p0, Lcom/facebook/rapidreporting/ui/DialogConfig;->d:LX/Js1;

    .line 1535106
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1535107
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1535108
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogConfig;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1535109
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/DialogConfig;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1535110
    return-void
.end method
