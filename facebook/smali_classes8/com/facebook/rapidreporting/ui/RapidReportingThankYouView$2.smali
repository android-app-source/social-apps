.class public final Lcom/facebook/rapidreporting/ui/RapidReportingThankYouView$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:LX/9mW;


# direct methods
.method public constructor <init>(LX/9mW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1535832
    iput-object p1, p0, Lcom/facebook/rapidreporting/ui/RapidReportingThankYouView$2;->b:LX/9mW;

    iput-object p2, p0, Lcom/facebook/rapidreporting/ui/RapidReportingThankYouView$2;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 1535822
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/RapidReportingThankYouView$2;->b:LX/9mW;

    iget-object v0, v0, LX/9mP;->a:Landroid/widget/ScrollView;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/rapidreporting/ui/RapidReportingThankYouView$2;->b:LX/9mW;

    iget-object v3, p0, Lcom/facebook/rapidreporting/ui/RapidReportingThankYouView$2;->a:Landroid/view/View;

    .line 1535823
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 1535824
    iget-object v5, v2, LX/9mP;->a:Landroid/widget/ScrollView;

    invoke-virtual {v5, v4}, Landroid/widget/ScrollView;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 1535825
    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v4

    .line 1535826
    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v5

    int-to-float v5, v5

    .line 1535827
    cmpl-float p0, v4, v5

    if-lez p0, :cond_0

    .line 1535828
    const/4 v4, 0x0

    .line 1535829
    :goto_0
    move v2, v4

    .line 1535830
    invoke-virtual {v0, v1, v2}, Landroid/widget/ScrollView;->smoothScrollBy(II)V

    .line 1535831
    return-void

    :cond_0
    sub-float v4, v5, v4

    float-to-int v4, v4

    goto :goto_0
.end method
