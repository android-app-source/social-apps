.class public Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:LX/9lb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/9lc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Lcom/facebook/rapidreporting/ui/DialogStateData;

.field public r:LX/9mU;

.field public final s:Landroid/content/DialogInterface$OnClickListener;

.field public final t:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1535565
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 1535566
    new-instance v0, LX/9mK;

    invoke-direct {v0, p0}, LX/9mK;-><init>(Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;)V

    iput-object v0, p0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->s:Landroid/content/DialogInterface$OnClickListener;

    .line 1535567
    new-instance v0, LX/9mL;

    invoke-direct {v0, p0}, LX/9mL;-><init>(Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;)V

    iput-object v0, p0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->t:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/View;)LX/0ju;
    .locals 6
    .param p1    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 1535559
    new-instance v0, LX/31Y;

    const v1, 0x7f0e09a8

    invoke-direct {v0, p0, v1}, LX/31Y;-><init>(Landroid/content/Context;I)V

    .line 1535560
    invoke-virtual {v0, v2}, LX/0ju;->c(Z)LX/0ju;

    .line 1535561
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0ju;->a(Landroid/view/View;)LX/0ju;

    .line 1535562
    invoke-virtual {v0, v2}, LX/0ju;->a(Z)LX/0ju;

    move-object v1, p1

    move v3, v2

    move v4, v2

    move v5, v2

    .line 1535563
    invoke-virtual/range {v0 .. v5}, LX/0ju;->a(Landroid/view/View;IIII)LX/0ju;

    .line 1535564
    return-object v0
.end method

.method public static a()Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;
    .locals 1

    .prologue
    .line 1535558
    new-instance v0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

    invoke-direct {v0}, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;-><init>()V

    return-object v0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

    invoke-static {p0}, LX/9lb;->b(LX/0QB;)LX/9lb;

    move-result-object v1

    check-cast v1, LX/9lb;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/9lc;->b(LX/0QB;)LX/9lc;

    move-result-object v3

    check-cast v3, LX/9lc;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object p0

    check-cast p0, LX/0wM;

    iput-object v1, p1, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->m:LX/9lb;

    iput-object v2, p1, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->n:Lcom/facebook/content/SecureContextHelper;

    iput-object v3, p1, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->o:LX/9lc;

    iput-object p0, p1, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->p:LX/0wM;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 12

    .prologue
    .line 1535521
    sget-object v0, LX/9mM;->a:[I

    iget-object v1, p0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->q:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1535522
    iget-object p1, v1, Lcom/facebook/rapidreporting/ui/DialogStateData;->b:LX/9mN;

    move-object v1, p1

    .line 1535523
    invoke-virtual {v1}, LX/9mN;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1535524
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 1535525
    :pswitch_0
    new-instance v2, LX/9mU;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->q:Lcom/facebook/rapidreporting/ui/DialogStateData;

    iget-object v7, p0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->p:LX/0wM;

    move-object v5, p0

    move-object v6, p0

    invoke-direct/range {v2 .. v7}, LX/9mU;-><init>(Landroid/content/Context;Lcom/facebook/rapidreporting/ui/DialogStateData;Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;LX/0wM;)V

    .line 1535526
    iput-object v2, p0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->r:LX/9mU;

    .line 1535527
    iget-object v3, p0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->q:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1535528
    iget-object v4, v3, Lcom/facebook/rapidreporting/ui/DialogStateData;->e:Ljava/lang/String;

    move-object v3, v4

    .line 1535529
    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->q:Lcom/facebook/rapidreporting/ui/DialogStateData;

    invoke-virtual {v3}, Lcom/facebook/rapidreporting/ui/DialogStateData;->l()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 1535530
    :cond_0
    iget-object v3, p0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->r:LX/9mU;

    invoke-virtual {v3}, LX/9mU;->a()V

    .line 1535531
    :goto_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->a(Landroid/content/Context;Landroid/view/View;)LX/0ju;

    move-result-object v2

    .line 1535532
    const v3, 0x7f0824b0

    iget-object v4, p0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->s:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v2, v3, v4}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1535533
    const v3, 0x7f080017

    iget-object v4, p0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->t:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v2, v3, v4}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1535534
    invoke-virtual {v2}, LX/0ju;->a()LX/2EJ;

    move-result-object v2

    move-object v0, v2

    .line 1535535
    :goto_1
    return-object v0

    .line 1535536
    :pswitch_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->a(Landroid/content/Context;Landroid/view/View;)LX/0ju;

    move-result-object v0

    .line 1535537
    const v1, 0x7f0824b1

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    .line 1535538
    const v1, 0x7f0824b2

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    .line 1535539
    const v1, 0x7f0824b3

    iget-object v2, p0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->s:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1535540
    const v1, 0x7f080027

    iget-object v2, p0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->t:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1535541
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    move-object v0, v0

    .line 1535542
    goto :goto_1

    .line 1535543
    :pswitch_2
    new-instance v2, LX/9mW;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->q:Lcom/facebook/rapidreporting/ui/DialogStateData;

    iget-object v7, p0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->p:LX/0wM;

    move-object v5, p0

    move-object v6, p0

    invoke-direct/range {v2 .. v7}, LX/9mW;-><init>(Landroid/content/Context;Lcom/facebook/rapidreporting/ui/DialogStateData;Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;LX/0wM;)V

    .line 1535544
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->a(Landroid/content/Context;Landroid/view/View;)LX/0ju;

    move-result-object v2

    .line 1535545
    const v3, 0x7f08001e

    iget-object v4, p0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->s:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v2, v3, v4}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1535546
    invoke-virtual {v2}, LX/0ju;->a()LX/2EJ;

    move-result-object v2

    move-object v0, v2

    .line 1535547
    goto :goto_1

    .line 1535548
    :cond_1
    iget-object v3, p0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->m:LX/9lb;

    iget-object v4, p0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->q:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1535549
    invoke-static {}, LX/9lx;->a()LX/9lw;

    move-result-object v8

    .line 1535550
    const-string v9, "object_id"

    invoke-virtual {v4}, Lcom/facebook/rapidreporting/ui/DialogStateData;->b()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1535551
    invoke-virtual {v4}, Lcom/facebook/rapidreporting/ui/DialogStateData;->c()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_2

    .line 1535552
    const-string v9, "location"

    invoke-virtual {v4}, Lcom/facebook/rapidreporting/ui/DialogStateData;->c()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1535553
    :cond_2
    invoke-static {v8}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v8

    sget-object v9, LX/0zS;->a:LX/0zS;

    invoke-virtual {v8, v9}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v8

    const-wide/16 v10, 0x78

    invoke-virtual {v8, v10, v11}, LX/0zO;->a(J)LX/0zO;

    move-result-object v8

    .line 1535554
    iget-object v9, v3, LX/9lb;->e:LX/0SI;

    invoke-interface {v9}, LX/0SI;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v9

    .line 1535555
    iput-object v9, v8, LX/0zO;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1535556
    iget-object v9, v3, LX/9lb;->b:LX/1Ck;

    sget-object v10, LX/9la;->FETCH_METADATA:LX/9la;

    iget-object v11, v3, LX/9lb;->f:LX/0tX;

    invoke-virtual {v11, v8}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v8

    new-instance v11, LX/9lR;

    invoke-direct {v11, v3, v4, p0, v2}, LX/9lR;-><init>(LX/9lb;Lcom/facebook/rapidreporting/ui/DialogStateData;Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;LX/9mU;)V

    invoke-static {v11}, LX/0Vd;->of(LX/0TF;)LX/0Vd;

    move-result-object v11

    invoke-virtual {v9, v10, v8, v11}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1535557
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 1535568
    if-eqz p2, :cond_0

    .line 1535569
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->q:Lcom/facebook/rapidreporting/ui/DialogStateData;

    invoke-virtual {v0, p1}, Lcom/facebook/rapidreporting/ui/DialogStateData;->a(Ljava/lang/String;)V

    .line 1535570
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->o:LX/9lc;

    iget-object v1, p0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->q:Lcom/facebook/rapidreporting/ui/DialogStateData;

    invoke-virtual {v1}, Lcom/facebook/rapidreporting/ui/DialogStateData;->b()Ljava/lang/String;

    move-result-object v1

    .line 1535571
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p2, "fb4a_rapid_reporting_selected_tag"

    invoke-direct {v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1535572
    const-string p2, "tag"

    invoke-virtual {v2, p2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1535573
    invoke-static {v0, v2, v1}, LX/9lc;->a(LX/9lc;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 1535574
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->b()V

    .line 1535575
    return-void

    .line 1535576
    :cond_0
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->q:Lcom/facebook/rapidreporting/ui/DialogStateData;

    invoke-virtual {v0, p1}, Lcom/facebook/rapidreporting/ui/DialogStateData;->b(Ljava/lang/String;)V

    .line 1535577
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->o:LX/9lc;

    iget-object v1, p0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->q:Lcom/facebook/rapidreporting/ui/DialogStateData;

    invoke-virtual {v1}, Lcom/facebook/rapidreporting/ui/DialogStateData;->b()Ljava/lang/String;

    move-result-object v1

    .line 1535578
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p2, "fb4a_rapid_reporting_deselected_tag"

    invoke-direct {v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1535579
    const-string p2, "tag"

    invoke-virtual {v2, p2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1535580
    invoke-static {v0, v2, v1}, LX/9lc;->a(LX/9lc;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 1535581
    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1535499
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->q:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1535500
    iget-object v1, v0, Lcom/facebook/rapidreporting/ui/DialogStateData;->b:LX/9mN;

    move-object v0, v1

    .line 1535501
    sget-object v1, LX/9mN;->FEEDBACK:LX/9mN;

    if-ne v0, v1, :cond_2

    .line 1535502
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->q:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1535503
    iget-boolean v1, v0, Lcom/facebook/rapidreporting/ui/DialogStateData;->k:Z

    move v0, v1

    .line 1535504
    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->q:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1535505
    iget-object v1, v0, Lcom/facebook/rapidreporting/ui/DialogStateData;->f:Ljava/util/List;

    move-object v0, v1

    .line 1535506
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->q:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1535507
    iget-object v1, v0, Lcom/facebook/rapidreporting/ui/DialogStateData;->c:Lcom/facebook/graphql/executor/GraphQLResult;

    if-nez v1, :cond_4

    .line 1535508
    const/4 v1, 0x0

    .line 1535509
    :goto_0
    move v0, v1

    .line 1535510
    if-eqz v0, :cond_3

    :cond_0
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->q:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1535511
    iget-object v1, v0, Lcom/facebook/rapidreporting/ui/DialogStateData;->h:Ljava/lang/String;

    move-object v0, v1

    .line 1535512
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->q:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1535513
    iget-boolean v1, v0, Lcom/facebook/rapidreporting/ui/DialogStateData;->j:Z

    move v0, v1

    .line 1535514
    if-eqz v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    move v1, v0

    .line 1535515
    :goto_1
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v0

    .line 1535516
    check-cast v0, LX/2EJ;

    const/4 v2, -0x1

    invoke-virtual {v0, v2}, LX/2EJ;->a(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1535517
    :cond_2
    return-void

    .line 1535518
    :cond_3
    const/4 v0, 0x0

    move v1, v0

    goto :goto_1

    :cond_4
    iget-object v1, v0, Lcom/facebook/rapidreporting/ui/DialogStateData;->c:Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1535519
    iget-object v0, v1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v0

    .line 1535520
    check-cast v1, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingTagsQueryModel;

    invoke-virtual {v1}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingTagsQueryModel;->a()Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel;->j()Z

    move-result v1

    goto :goto_0
.end method

.method public final c(Z)V
    .locals 2

    .prologue
    .line 1535491
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->q:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1535492
    iget-object v1, v0, Lcom/facebook/rapidreporting/ui/DialogStateData;->b:LX/9mN;

    move-object v0, v1

    .line 1535493
    sget-object v1, LX/9mN;->FEEDBACK:LX/9mN;

    if-ne v0, v1, :cond_0

    .line 1535494
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->q:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1535495
    iput-boolean p1, v0, Lcom/facebook/rapidreporting/ui/DialogStateData;->k:Z

    .line 1535496
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->r:LX/9mU;

    invoke-virtual {v0, p1}, LX/9mU;->a(Z)V

    .line 1535497
    invoke-virtual {p0}, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->b()V

    .line 1535498
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6c89f9bf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1535486
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1535487
    const-class v0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;

    invoke-static {v0, p0}, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1535488
    if-eqz p1, :cond_0

    .line 1535489
    const-string v0, "dialogStateData"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/rapidreporting/ui/DialogStateData;

    iput-object v0, p0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->q:Lcom/facebook/rapidreporting/ui/DialogStateData;

    .line 1535490
    :cond_0
    const/16 v0, 0x2b

    const v2, -0x1b8ab220

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1535483
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1535484
    const-string v0, "dialogStateData"

    iget-object v1, p0, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->q:Lcom/facebook/rapidreporting/ui/DialogStateData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1535485
    return-void
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x6cd855f5

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1535471
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onStart()V

    .line 1535472
    iget-object v1, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v1, v1

    .line 1535473
    if-eqz v1, :cond_0

    .line 1535474
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 1535475
    new-instance v2, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v2}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 1535476
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 1535477
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/WindowManager$LayoutParams;->copyFrom(Landroid/view/WindowManager$LayoutParams;)I

    .line 1535478
    const/4 v3, -0x1

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 1535479
    invoke-virtual {v1, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 1535480
    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 1535481
    invoke-virtual {p0}, Lcom/facebook/rapidreporting/ui/RapidReportingDialogFragment;->b()V

    .line 1535482
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x2e36f0ea

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
