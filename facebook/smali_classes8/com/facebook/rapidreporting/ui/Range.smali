.class public Lcom/facebook/rapidreporting/ui/Range;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/rapidreporting/ui/Range;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1535353
    new-instance v0, LX/9mJ;

    invoke-direct {v0}, LX/9mJ;-><init>()V

    sput-object v0, Lcom/facebook/rapidreporting/ui/Range;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1535388
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1535389
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rapidreporting/ui/Range;->a:Ljava/lang/String;

    .line 1535390
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/rapidreporting/ui/Range;->b:I

    .line 1535391
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/rapidreporting/ui/Range;->c:I

    .line 1535392
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 1535383
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1535384
    iput-object p1, p0, Lcom/facebook/rapidreporting/ui/Range;->a:Ljava/lang/String;

    .line 1535385
    iput p2, p0, Lcom/facebook/rapidreporting/ui/Range;->b:I

    .line 1535386
    iput p3, p0, Lcom/facebook/rapidreporting/ui/Range;->c:I

    .line 1535387
    return-void
.end method

.method public static a(Ljava/util/List;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/rapidreporting/ui/Range;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1535359
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1535360
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 1535361
    const/4 p0, 0x1

    .line 1535362
    const/4 v3, 0x0

    .line 1535363
    instance-of v4, v2, Lcom/facebook/rapidreporting/ui/Range;

    if-eqz v4, :cond_1

    .line 1535364
    check-cast v2, Lcom/facebook/rapidreporting/ui/Range;

    .line 1535365
    :goto_1
    move-object v2, v2

    .line 1535366
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1535367
    :cond_0
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0

    .line 1535368
    :cond_1
    instance-of v4, v2, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$SubtitleModel$RangesModel;

    if-eqz v4, :cond_2

    move-object v3, v2

    .line 1535369
    check-cast v3, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$SubtitleModel$RangesModel;

    invoke-virtual {v3}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$SubtitleModel$RangesModel;->k()I

    move-result v4

    move-object v3, v2

    .line 1535370
    check-cast v3, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$SubtitleModel$RangesModel;

    invoke-virtual {v3}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$SubtitleModel$RangesModel;->j()I

    move-result v3

    .line 1535371
    check-cast v2, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$SubtitleModel$RangesModel;

    invoke-virtual {v2}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$SubtitleModel$RangesModel;->a()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    invoke-virtual {v6, v5, p0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    .line 1535372
    new-instance v2, Lcom/facebook/rapidreporting/ui/Range;

    invoke-direct {v2, v5, v4, v3}, Lcom/facebook/rapidreporting/ui/Range;-><init>(Ljava/lang/String;II)V

    goto :goto_1

    .line 1535373
    :cond_2
    instance-of v4, v2, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$PersistentUnitsModel$MessageModel$RangesModel;

    if-eqz v4, :cond_3

    move-object v3, v2

    .line 1535374
    check-cast v3, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$PersistentUnitsModel$MessageModel$RangesModel;

    invoke-virtual {v3}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$PersistentUnitsModel$MessageModel$RangesModel;->k()I

    move-result v4

    move-object v3, v2

    .line 1535375
    check-cast v3, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$PersistentUnitsModel$MessageModel$RangesModel;

    invoke-virtual {v3}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$PersistentUnitsModel$MessageModel$RangesModel;->j()I

    move-result v3

    .line 1535376
    check-cast v2, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$PersistentUnitsModel$MessageModel$RangesModel;

    invoke-virtual {v2}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$PersistentUnitsModel$MessageModel$RangesModel;->a()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    invoke-virtual {v6, v5, p0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    .line 1535377
    new-instance v2, Lcom/facebook/rapidreporting/ui/Range;

    invoke-direct {v2, v5, v4, v3}, Lcom/facebook/rapidreporting/ui/Range;-><init>(Ljava/lang/String;II)V

    goto :goto_1

    .line 1535378
    :cond_3
    instance-of v4, v2, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$SubtitleModel$RangesModel;

    if-eqz v4, :cond_4

    move-object v3, v2

    .line 1535379
    check-cast v3, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$SubtitleModel$RangesModel;

    invoke-virtual {v3}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$SubtitleModel$RangesModel;->k()I

    move-result v4

    move-object v3, v2

    .line 1535380
    check-cast v3, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$SubtitleModel$RangesModel;

    invoke-virtual {v3}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$SubtitleModel$RangesModel;->j()I

    move-result v3

    .line 1535381
    check-cast v2, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$SubtitleModel$RangesModel;

    invoke-virtual {v2}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$SubtitleModel$RangesModel;->a()Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$SubtitleModel$RangesModel$EntityModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$SubtitleModel$RangesModel$EntityModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 1535382
    new-instance v2, Lcom/facebook/rapidreporting/ui/Range;

    invoke-direct {v2, v5, v4, v3}, Lcom/facebook/rapidreporting/ui/Range;-><init>(Ljava/lang/String;II)V

    goto :goto_1

    :cond_4
    move-object v2, v3

    goto :goto_1
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1535358
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1535354
    iget-object v0, p0, Lcom/facebook/rapidreporting/ui/Range;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1535355
    iget v0, p0, Lcom/facebook/rapidreporting/ui/Range;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1535356
    iget v0, p0, Lcom/facebook/rapidreporting/ui/Range;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1535357
    return-void
.end method
