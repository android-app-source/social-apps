.class public final Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1533641
    const-class v0, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel;

    new-instance v1, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1533642
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1533643
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1533644
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1533645
    const/4 v2, 0x0

    .line 1533646
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1533647
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1533648
    :goto_0
    move v1, v2

    .line 1533649
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1533650
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1533651
    new-instance v1, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel;

    invoke-direct {v1}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel;-><init>()V

    .line 1533652
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1533653
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1533654
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1533655
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1533656
    :cond_0
    return-object v1

    .line 1533657
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1533658
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1533659
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1533660
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1533661
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 1533662
    const-string v4, "confirmation_prompt"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1533663
    const/4 v3, 0x0

    .line 1533664
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_d

    .line 1533665
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1533666
    :goto_2
    move v1, v3

    .line 1533667
    goto :goto_1

    .line 1533668
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1533669
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1533670
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 1533671
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1533672
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_c

    .line 1533673
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1533674
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1533675
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, p0, :cond_6

    if-eqz v8, :cond_6

    .line 1533676
    const-string v9, "followup_actions"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 1533677
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1533678
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_ARRAY:LX/15z;

    if-ne v8, v9, :cond_7

    .line 1533679
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_ARRAY:LX/15z;

    if-eq v8, v9, :cond_7

    .line 1533680
    invoke-static {p1, v0}, LX/9lq;->b(LX/15w;LX/186;)I

    move-result v8

    .line 1533681
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1533682
    :cond_7
    invoke-static {v7, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v7

    move v7, v7

    .line 1533683
    goto :goto_3

    .line 1533684
    :cond_8
    const-string v9, "followup_actions_title"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 1533685
    const/4 v8, 0x0

    .line 1533686
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v6, v9, :cond_11

    .line 1533687
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1533688
    :goto_5
    move v6, v8

    .line 1533689
    goto :goto_3

    .line 1533690
    :cond_9
    const-string v9, "srt_job_id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 1533691
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_3

    .line 1533692
    :cond_a
    const-string v9, "subtitle"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_b

    .line 1533693
    invoke-static {p1, v0}, LX/9lt;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_3

    .line 1533694
    :cond_b
    const-string v9, "title"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1533695
    invoke-static {p1, v0}, LX/9lu;->a(LX/15w;LX/186;)I

    move-result v1

    goto/16 :goto_3

    .line 1533696
    :cond_c
    const/4 v8, 0x5

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 1533697
    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1533698
    const/4 v3, 0x1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1533699
    const/4 v3, 0x2

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 1533700
    const/4 v3, 0x3

    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 1533701
    const/4 v3, 0x4

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1533702
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto/16 :goto_2

    :cond_d
    move v1, v3

    move v4, v3

    move v5, v3

    move v6, v3

    move v7, v3

    goto/16 :goto_3

    .line 1533703
    :cond_e
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1533704
    :cond_f
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, p0, :cond_10

    .line 1533705
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1533706
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1533707
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_f

    if-eqz v9, :cond_f

    .line 1533708
    const-string p0, "text"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_e

    .line 1533709
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_6

    .line 1533710
    :cond_10
    const/4 v9, 0x1

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 1533711
    invoke-virtual {v0, v8, v6}, LX/186;->b(II)V

    .line 1533712
    invoke-virtual {v0}, LX/186;->d()I

    move-result v8

    goto/16 :goto_5

    :cond_11
    move v6, v8

    goto :goto_6
.end method
