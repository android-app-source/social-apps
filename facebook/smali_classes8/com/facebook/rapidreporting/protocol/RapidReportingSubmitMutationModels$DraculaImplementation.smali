.class public final Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1533222
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1533223
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1533220
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1533221
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 1533285
    if-nez p1, :cond_0

    move v0, v1

    .line 1533286
    :goto_0
    return v0

    .line 1533287
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1533288
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1533289
    :sswitch_0
    const-class v0, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$FollowupActionsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1533290
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 1533291
    invoke-virtual {p0, p1, v7}, LX/15i;->p(II)I

    move-result v0

    .line 1533292
    const v3, 0x6789741d

    invoke-static {p0, v0, v3, p3}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    .line 1533293
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1533294
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1533295
    const-class v0, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$SubtitleModel;

    invoke-virtual {p0, p1, v9, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$SubtitleModel;

    .line 1533296
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1533297
    const-class v0, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$TitleModel;

    invoke-virtual {p0, p1, v10, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$TitleModel;

    .line 1533298
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1533299
    const/4 v6, 0x5

    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1533300
    invoke-virtual {p3, v1, v2}, LX/186;->b(II)V

    .line 1533301
    invoke-virtual {p3, v7, v3}, LX/186;->b(II)V

    .line 1533302
    invoke-virtual {p3, v8, v4}, LX/186;->b(II)V

    .line 1533303
    invoke-virtual {p3, v9, v5}, LX/186;->b(II)V

    .line 1533304
    invoke-virtual {p3, v10, v0}, LX/186;->b(II)V

    .line 1533305
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1533306
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1533307
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1533308
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 1533309
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1533310
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1533311
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1533312
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1533313
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 1533314
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1533315
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1533316
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1533317
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1533318
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 1533319
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1533320
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1533321
    :sswitch_4
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1533322
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1533323
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 1533324
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1533325
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1533326
    :sswitch_5
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1533327
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1533328
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 1533329
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1533330
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1533331
    :sswitch_6
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1533332
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1533333
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 1533334
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1533335
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x27b40780 -> :sswitch_3
        0x8a11010 -> :sswitch_2
        0x16afdf5f -> :sswitch_1
        0x2b052d77 -> :sswitch_5
        0x2f0a3852 -> :sswitch_4
        0x593c9e06 -> :sswitch_0
        0x6789741d -> :sswitch_6
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1533284
    new-instance v0, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1533280
    invoke-static {p0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v0

    .line 1533281
    if-eqz v0, :cond_0

    .line 1533282
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1533283
    :cond_0
    return-void
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1533275
    if-eqz p0, :cond_0

    .line 1533276
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1533277
    if-eq v0, p0, :cond_0

    .line 1533278
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1533279
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1533264
    sparse-switch p2, :sswitch_data_0

    .line 1533265
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1533266
    :sswitch_0
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$FollowupActionsModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1533267
    invoke-static {v0, p3}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    .line 1533268
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1533269
    const v1, 0x6789741d

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1533270
    const/4 v0, 0x3

    const-class v1, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$SubtitleModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$SubtitleModel;

    .line 1533271
    invoke-static {v0, p3}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 1533272
    const/4 v0, 0x4

    const-class v1, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$TitleModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$ConfirmationPromptModel$TitleModel;

    .line 1533273
    invoke-static {v0, p3}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 1533274
    :sswitch_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x27b40780 -> :sswitch_1
        0x8a11010 -> :sswitch_1
        0x16afdf5f -> :sswitch_1
        0x2b052d77 -> :sswitch_1
        0x2f0a3852 -> :sswitch_1
        0x593c9e06 -> :sswitch_0
        0x6789741d -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1533258
    if-eqz p1, :cond_0

    .line 1533259
    invoke-static {p0, p1, p2}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$DraculaImplementation;

    move-result-object v1

    .line 1533260
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$DraculaImplementation;

    .line 1533261
    if-eq v0, v1, :cond_0

    .line 1533262
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1533263
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1533257
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1533255
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1533256
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1533250
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1533251
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1533252
    :cond_0
    iput-object p1, p0, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$DraculaImplementation;->a:LX/15i;

    .line 1533253
    iput p2, p0, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$DraculaImplementation;->b:I

    .line 1533254
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1533249
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1533248
    new-instance v0, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1533245
    iget v0, p0, LX/1vt;->c:I

    .line 1533246
    move v0, v0

    .line 1533247
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1533242
    iget v0, p0, LX/1vt;->c:I

    .line 1533243
    move v0, v0

    .line 1533244
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1533239
    iget v0, p0, LX/1vt;->b:I

    .line 1533240
    move v0, v0

    .line 1533241
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1533236
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1533237
    move-object v0, v0

    .line 1533238
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1533227
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1533228
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1533229
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1533230
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1533231
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1533232
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1533233
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1533234
    invoke-static {v3, v9, v2}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1533235
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1533224
    iget v0, p0, LX/1vt;->c:I

    .line 1533225
    move v0, v0

    .line 1533226
    return v0
.end method
