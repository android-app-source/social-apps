.class public final Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1534144
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1534145
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1534146
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1534147
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v0, 0x0

    const/4 v5, 0x1

    .line 1534148
    if-nez p1, :cond_0

    .line 1534149
    :goto_0
    return v0

    .line 1534150
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1534151
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1534152
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1534153
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1534154
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1534155
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1534156
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1534157
    :sswitch_1
    invoke-virtual {p0, p1, v5}, LX/15i;->b(II)Z

    move-result v0

    .line 1534158
    invoke-virtual {p0, p1, v6}, LX/15i;->p(II)I

    move-result v1

    .line 1534159
    const v2, -0x2570ce68

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1534160
    invoke-virtual {p0, p1, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1534161
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1534162
    invoke-virtual {p0, p1, v8}, LX/15i;->p(II)I

    move-result v3

    .line 1534163
    const v4, 0x55b67672

    invoke-static {p0, v3, v4, p3}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    .line 1534164
    const/4 v4, 0x5

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1534165
    invoke-virtual {p3, v5, v0}, LX/186;->a(IZ)V

    .line 1534166
    invoke-virtual {p3, v6, v1}, LX/186;->b(II)V

    .line 1534167
    invoke-virtual {p3, v7, v2}, LX/186;->b(II)V

    .line 1534168
    invoke-virtual {p3, v8, v3}, LX/186;->b(II)V

    .line 1534169
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1534170
    :sswitch_2
    const-class v1, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$PersistentUnitsModel$MessageModel$RangesModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v1}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v1

    .line 1534171
    invoke-static {p3, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1534172
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1534173
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1534174
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1534175
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1534176
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 1534177
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1534178
    :sswitch_3
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1534179
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1534180
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1534181
    invoke-virtual {p3, v5, v0}, LX/186;->b(II)V

    .line 1534182
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1534183
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1534184
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1534185
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1534186
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1534187
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1534188
    :sswitch_5
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1534189
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1534190
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1534191
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1534192
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1534193
    :sswitch_6
    const-class v1, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$SubtitleModel$RangesModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v1}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v1

    .line 1534194
    invoke-static {p3, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1534195
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1534196
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1534197
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1534198
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1534199
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 1534200
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1534201
    :sswitch_7
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1534202
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1534203
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1534204
    invoke-virtual {p3, v5, v0}, LX/186;->b(II)V

    .line 1534205
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1534206
    :sswitch_8
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1534207
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1534208
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1534209
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1534210
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x2e261c4d -> :sswitch_5
        -0x2570ce68 -> :sswitch_2
        -0x642aa25 -> :sswitch_0
        0xa9a1e68 -> :sswitch_6
        0xf9a0d7d -> :sswitch_7
        0x1141b9d6 -> :sswitch_8
        0x13e5ab88 -> :sswitch_1
        0x466b9d31 -> :sswitch_3
        0x55b67672 -> :sswitch_4
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1534211
    if-nez p0, :cond_0

    move v0, v1

    .line 1534212
    :goto_0
    return v0

    .line 1534213
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 1534214
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1534215
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1534216
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 1534217
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1534218
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1534219
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 1534241
    const/4 v7, 0x0

    .line 1534242
    const/4 v1, 0x0

    .line 1534243
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 1534244
    invoke-static {v2, v3, v0}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1534245
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 1534246
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1534247
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1534130
    new-instance v0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1534220
    invoke-static {p0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v0

    .line 1534221
    if-eqz v0, :cond_0

    .line 1534222
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1534223
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1534224
    sparse-switch p2, :sswitch_data_0

    .line 1534225
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1534226
    :sswitch_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1534227
    const v1, -0x2570ce68

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1534228
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1534229
    const v1, 0x55b67672

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1534230
    :goto_0
    :sswitch_1
    return-void

    .line 1534231
    :sswitch_2
    const-class v0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$PersistentUnitsModel$MessageModel$RangesModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1534232
    invoke-static {v0, p3}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    goto :goto_0

    .line 1534233
    :sswitch_3
    const-class v0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$SubtitleModel$RangesModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1534234
    invoke-static {v0, p3}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x2e261c4d -> :sswitch_1
        -0x2570ce68 -> :sswitch_2
        -0x642aa25 -> :sswitch_1
        0xa9a1e68 -> :sswitch_3
        0xf9a0d7d -> :sswitch_1
        0x1141b9d6 -> :sswitch_1
        0x13e5ab88 -> :sswitch_0
        0x466b9d31 -> :sswitch_1
        0x55b67672 -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1534235
    if-eqz p1, :cond_0

    .line 1534236
    invoke-static {p0, p1, p2}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$DraculaImplementation;

    move-result-object v1

    .line 1534237
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$DraculaImplementation;

    .line 1534238
    if-eq v0, v1, :cond_0

    .line 1534239
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1534240
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1534141
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1534142
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1534143
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1534136
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1534137
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1534138
    :cond_0
    iput-object p1, p0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$DraculaImplementation;->a:LX/15i;

    .line 1534139
    iput p2, p0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$DraculaImplementation;->b:I

    .line 1534140
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1534135
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1534134
    new-instance v0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1534131
    iget v0, p0, LX/1vt;->c:I

    .line 1534132
    move v0, v0

    .line 1534133
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1534127
    iget v0, p0, LX/1vt;->c:I

    .line 1534128
    move v0, v0

    .line 1534129
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1534124
    iget v0, p0, LX/1vt;->b:I

    .line 1534125
    move v0, v0

    .line 1534126
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1534121
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1534122
    move-object v0, v0

    .line 1534123
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1534112
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1534113
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1534114
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1534115
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1534116
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1534117
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1534118
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1534119
    invoke-static {v3, v9, v2}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1534120
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1534109
    iget v0, p0, LX/1vt;->c:I

    .line 1534110
    move v0, v0

    .line 1534111
    return v0
.end method
