.class public final Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x64fabfdc
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel$RapidReportingPromptModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1533185
    const-class v0, Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1533184
    const-class v0, Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1533182
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1533183
    return-void
.end method

.method private a()Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel$RapidReportingPromptModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1533161
    iget-object v0, p0, Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel;->e:Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel$RapidReportingPromptModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel$RapidReportingPromptModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel$RapidReportingPromptModel;

    iput-object v0, p0, Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel;->e:Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel$RapidReportingPromptModel;

    .line 1533162
    iget-object v0, p0, Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel;->e:Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel$RapidReportingPromptModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1533176
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1533177
    invoke-direct {p0}, Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel;->a()Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel$RapidReportingPromptModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1533178
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1533179
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1533180
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1533181
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1533168
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1533169
    invoke-direct {p0}, Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel;->a()Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel$RapidReportingPromptModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1533170
    invoke-direct {p0}, Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel;->a()Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel$RapidReportingPromptModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel$RapidReportingPromptModel;

    .line 1533171
    invoke-direct {p0}, Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel;->a()Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel$RapidReportingPromptModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1533172
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel;

    .line 1533173
    iput-object v0, v1, Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel;->e:Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel$RapidReportingPromptModel;

    .line 1533174
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1533175
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1533165
    new-instance v0, Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel;

    invoke-direct {v0}, Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel;-><init>()V

    .line 1533166
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1533167
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1533164
    const v0, -0x3a330d92

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1533163
    const v0, 0x5859a47f

    return v0
.end method
