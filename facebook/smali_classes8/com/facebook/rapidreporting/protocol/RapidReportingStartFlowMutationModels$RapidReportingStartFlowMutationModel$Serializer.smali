.class public final Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1533148
    const-class v0, Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel;

    new-instance v1, Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1533149
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1533150
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1533151
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1533152
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1533153
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1533154
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1533155
    if-eqz v2, :cond_0

    .line 1533156
    const-string p0, "rapid_reporting_prompt"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1533157
    invoke-static {v1, v2, p1}, LX/9lg;->a(LX/15i;ILX/0nX;)V

    .line 1533158
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1533159
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1533160
    check-cast p1, Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel$Serializer;->a(Lcom/facebook/rapidreporting/protocol/RapidReportingStartFlowMutationModels$RapidReportingStartFlowMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
