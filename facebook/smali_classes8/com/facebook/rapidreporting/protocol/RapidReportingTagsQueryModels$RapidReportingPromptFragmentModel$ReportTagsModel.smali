.class public final Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$ReportTagsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x635b3c6a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$ReportTagsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$ReportTagsModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1534339
    const-class v0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$ReportTagsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1534374
    const-class v0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$ReportTagsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1534372
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1534373
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1534363
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1534364
    invoke-virtual {p0}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$ReportTagsModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1534365
    invoke-virtual {p0}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$ReportTagsModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, -0x2e261c4d

    invoke-static {v2, v1, v3}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1534366
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1534367
    iget v2, p0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$ReportTagsModel;->e:I

    invoke-virtual {p1, v4, v2, v4}, LX/186;->a(III)V

    .line 1534368
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1534369
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1534370
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1534371
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1534353
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1534354
    invoke-virtual {p0}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$ReportTagsModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1534355
    invoke-virtual {p0}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$ReportTagsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x2e261c4d

    invoke-static {v2, v0, v3}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1534356
    invoke-virtual {p0}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$ReportTagsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1534357
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$ReportTagsModel;

    .line 1534358
    iput v3, v0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$ReportTagsModel;->g:I

    .line 1534359
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1534360
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 1534361
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 1534362
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1534351
    iget-object v0, p0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$ReportTagsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$ReportTagsModel;->f:Ljava/lang/String;

    .line 1534352
    iget-object v0, p0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$ReportTagsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1534347
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1534348
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$ReportTagsModel;->e:I

    .line 1534349
    const/4 v0, 0x2

    const v1, -0x2e261c4d

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$ReportTagsModel;->g:I

    .line 1534350
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1534344
    new-instance v0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$ReportTagsModel;

    invoke-direct {v0}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$ReportTagsModel;-><init>()V

    .line 1534345
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1534346
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1534343
    const v0, 0x77d8b089

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1534342
    const v0, -0x7145edc0

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTitle"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1534340
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1534341
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$ReportTagsModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
