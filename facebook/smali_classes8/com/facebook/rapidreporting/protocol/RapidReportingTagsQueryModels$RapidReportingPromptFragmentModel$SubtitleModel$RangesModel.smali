.class public final Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$SubtitleModel$RangesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x87a030d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$SubtitleModel$RangesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$SubtitleModel$RangesModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I

.field private g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1534439
    const-class v0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$SubtitleModel$RangesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1534438
    const-class v0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$SubtitleModel$RangesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1534436
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1534437
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1534428
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1534429
    invoke-virtual {p0}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$SubtitleModel$RangesModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, 0xf9a0d7d

    invoke-static {v1, v0, v2}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1534430
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1534431
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1534432
    const/4 v0, 0x1

    iget v1, p0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$SubtitleModel$RangesModel;->f:I

    invoke-virtual {p1, v0, v1, v3}, LX/186;->a(III)V

    .line 1534433
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$SubtitleModel$RangesModel;->g:I

    invoke-virtual {p1, v0, v1, v3}, LX/186;->a(III)V

    .line 1534434
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1534435
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1534418
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1534419
    invoke-virtual {p0}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$SubtitleModel$RangesModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1534420
    invoke-virtual {p0}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$SubtitleModel$RangesModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0xf9a0d7d

    invoke-static {v2, v0, v3}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1534421
    invoke-virtual {p0}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$SubtitleModel$RangesModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1534422
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$SubtitleModel$RangesModel;

    .line 1534423
    iput v3, v0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$SubtitleModel$RangesModel;->e:I

    .line 1534424
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1534425
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 1534426
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 1534427
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getEntity"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1534416
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1534417
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$SubtitleModel$RangesModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1534402
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1534403
    const v0, 0xf9a0d7d

    invoke-static {p1, p2, v2, v0}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$SubtitleModel$RangesModel;->e:I

    .line 1534404
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$SubtitleModel$RangesModel;->f:I

    .line 1534405
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$SubtitleModel$RangesModel;->g:I

    .line 1534406
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1534413
    new-instance v0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$SubtitleModel$RangesModel;

    invoke-direct {v0}, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$SubtitleModel$RangesModel;-><init>()V

    .line 1534414
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1534415
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1534412
    const v0, -0xd3c6a1b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1534411
    const v0, -0x3d10ccb9

    return v0
.end method

.method public final j()I
    .locals 2

    .prologue
    .line 1534409
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1534410
    iget v0, p0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$SubtitleModel$RangesModel;->f:I

    return v0
.end method

.method public final k()I
    .locals 2

    .prologue
    .line 1534407
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1534408
    iget v0, p0, Lcom/facebook/rapidreporting/protocol/RapidReportingTagsQueryModels$RapidReportingPromptFragmentModel$SubtitleModel$RangesModel;->g:I

    return v0
.end method
