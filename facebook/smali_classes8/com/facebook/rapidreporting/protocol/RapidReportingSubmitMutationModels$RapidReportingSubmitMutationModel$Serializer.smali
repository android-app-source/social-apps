.class public final Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1533713
    const-class v0, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel;

    new-instance v1, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1533714
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1533715
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1533716
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1533717
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1533718
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1533719
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1533720
    if-eqz v2, :cond_7

    .line 1533721
    const-string v3, "confirmation_prompt"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1533722
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1533723
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1533724
    if-eqz v3, :cond_1

    .line 1533725
    const-string p0, "followup_actions"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1533726
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1533727
    const/4 p0, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result v0

    if-ge p0, v0, :cond_0

    .line 1533728
    invoke-virtual {v1, v3, p0}, LX/15i;->q(II)I

    move-result v0

    invoke-static {v1, v0, p1, p2}, LX/9lq;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1533729
    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    .line 1533730
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1533731
    :cond_1
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1533732
    if-eqz v3, :cond_3

    .line 1533733
    const-string p0, "followup_actions_title"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1533734
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1533735
    const/4 p0, 0x0

    invoke-virtual {v1, v3, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1533736
    if-eqz p0, :cond_2

    .line 1533737
    const-string v0, "text"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1533738
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1533739
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1533740
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1533741
    if-eqz v3, :cond_4

    .line 1533742
    const-string p0, "srt_job_id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1533743
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1533744
    :cond_4
    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1533745
    if-eqz v3, :cond_5

    .line 1533746
    const-string p0, "subtitle"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1533747
    invoke-static {v1, v3, p1, p2}, LX/9lt;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1533748
    :cond_5
    const/4 v3, 0x4

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1533749
    if-eqz v3, :cond_6

    .line 1533750
    const-string p0, "title"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1533751
    invoke-static {v1, v3, p1}, LX/9lu;->a(LX/15i;ILX/0nX;)V

    .line 1533752
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1533753
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1533754
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1533755
    check-cast p1, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel$Serializer;->a(Lcom/facebook/rapidreporting/protocol/RapidReportingSubmitMutationModels$RapidReportingSubmitMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
