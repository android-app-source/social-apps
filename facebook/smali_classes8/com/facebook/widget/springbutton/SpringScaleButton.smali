.class public Lcom/facebook/widget/springbutton/SpringScaleButton;
.super Lcom/facebook/fbui/glyph/GlyphView;
.source ""

# interfaces
.implements LX/20T;


# instance fields
.field private b:LX/215;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1414224
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/springbutton/SpringScaleButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1414225
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1414226
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/glyph/GlyphView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1414227
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1414228
    invoke-virtual {p0, v1}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1414229
    iget-object v0, p0, Lcom/facebook/widget/springbutton/SpringScaleButton;->b:LX/215;

    invoke-virtual {v0}, LX/215;->a()V

    .line 1414230
    iput-object v1, p0, Lcom/facebook/widget/springbutton/SpringScaleButton;->b:LX/215;

    .line 1414231
    return-void
.end method

.method public final a(F)V
    .locals 0

    .prologue
    .line 1414232
    invoke-virtual {p0, p1}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setScaleX(F)V

    .line 1414233
    invoke-virtual {p0, p1}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setScaleY(F)V

    .line 1414234
    return-void
.end method

.method public final a(LX/215;)V
    .locals 1

    .prologue
    .line 1414235
    iput-object p1, p0, Lcom/facebook/widget/springbutton/SpringScaleButton;->b:LX/215;

    .line 1414236
    iget-object v0, p0, Lcom/facebook/widget/springbutton/SpringScaleButton;->b:LX/215;

    invoke-virtual {v0, p0}, LX/215;->a(LX/20T;)V

    .line 1414237
    invoke-virtual {p0, p1}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1414238
    return-void
.end method
