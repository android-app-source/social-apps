.class public Lcom/facebook/widget/error/GenericErrorView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private final a:Landroid/widget/ImageView;

.field private final b:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1413699
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/error/GenericErrorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1413700
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 1413701
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1413702
    const v0, 0x7f030798

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1413703
    const v0, 0x7f0d1446

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v1

    .line 1413704
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1413705
    const v0, 0x7f0d1447

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/widget/error/GenericErrorView;->a:Landroid/widget/ImageView;

    .line 1413706
    const v0, 0x7f0d105d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/widget/error/GenericErrorView;->b:Landroid/widget/TextView;

    .line 1413707
    return-void
.end method

.method private a(II)V
    .locals 2

    .prologue
    .line 1413708
    iget-object v0, p0, Lcom/facebook/widget/error/GenericErrorView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 1413709
    if-ltz p2, :cond_0

    .line 1413710
    iget-object v0, p0, Lcom/facebook/widget/error/GenericErrorView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1413711
    iget-object v0, p0, Lcom/facebook/widget/error/GenericErrorView;->a:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1413712
    :goto_0
    return-void

    .line 1413713
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/error/GenericErrorView;->a:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1413714
    const v0, 0x7f08003b

    const v1, 0x7f021120

    invoke-direct {p0, v0, v1}, Lcom/facebook/widget/error/GenericErrorView;->a(II)V

    .line 1413715
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1413716
    const v0, 0x7f08003c

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1}, Lcom/facebook/widget/error/GenericErrorView;->a(II)V

    .line 1413717
    return-void
.end method

.method public setCustomErrorMessage(I)V
    .locals 1

    .prologue
    .line 1413718
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/error/GenericErrorView;->a(II)V

    .line 1413719
    return-void
.end method
