.class public Lcom/facebook/widget/error/GenericErrorViewStub;
.super LX/4nv;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1413725
    invoke-direct {p0, p1}, LX/4nv;-><init>(Landroid/content/Context;)V

    .line 1413726
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1413723
    invoke-direct {p0, p1, p2}, LX/4nv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1413724
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1413720
    invoke-direct {p0, p1, p2, p3}, LX/4nv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1413721
    return-void
.end method


# virtual methods
.method public getInflatedLayout()Landroid/view/View;
    .locals 2

    .prologue
    .line 1413722
    new-instance v0, Lcom/facebook/widget/error/GenericErrorView;

    invoke-virtual {p0}, Lcom/facebook/widget/error/GenericErrorViewStub;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/error/GenericErrorView;-><init>(Landroid/content/Context;)V

    return-object v0
.end method
