.class public Lcom/facebook/widget/gridview/BetterGridView;
.super Landroid/widget/GridView;
.source ""

# interfaces
.implements LX/1Zi;


# instance fields
.field public a:LX/2hv;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1627619
    invoke-direct {p0, p1}, Landroid/widget/GridView;-><init>(Landroid/content/Context;)V

    .line 1627620
    invoke-direct {p0}, Lcom/facebook/widget/gridview/BetterGridView;->a()V

    .line 1627621
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1627616
    invoke-direct {p0, p1, p2}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1627617
    invoke-direct {p0}, Lcom/facebook/widget/gridview/BetterGridView;->a()V

    .line 1627618
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1627613
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1627614
    invoke-direct {p0}, Lcom/facebook/widget/gridview/BetterGridView;->a()V

    .line 1627615
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1627610
    const-class v0, Lcom/facebook/widget/gridview/BetterGridView;

    invoke-static {v0, p0}, Lcom/facebook/widget/gridview/BetterGridView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1627611
    iget-object v0, p0, Lcom/facebook/widget/gridview/BetterGridView;->a:LX/2hv;

    invoke-super {p0, v0}, Landroid/widget/GridView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1627612
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/widget/gridview/BetterGridView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/widget/gridview/BetterGridView;

    invoke-static {v0}, LX/2hv;->a(LX/0QB;)LX/2hv;

    move-result-object v0

    check-cast v0, LX/2hv;

    iput-object v0, p0, Lcom/facebook/widget/gridview/BetterGridView;->a:LX/2hv;

    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1627600
    invoke-virtual {p0}, Lcom/facebook/widget/gridview/BetterGridView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1627601
    invoke-virtual {p0}, Lcom/facebook/widget/gridview/BetterGridView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 1627602
    invoke-virtual {p0, v1}, Lcom/facebook/widget/gridview/BetterGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1627603
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 1627604
    if-gez v1, :cond_2

    .line 1627605
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v0, v1

    .line 1627606
    goto :goto_0

    .line 1627607
    :cond_2
    if-eq p1, v1, :cond_0

    .line 1627608
    sub-int/2addr v1, p1

    div-int/lit8 v1, v1, 0x2

    .line 1627609
    add-int/2addr v0, v1

    goto :goto_1
.end method

.method public final a(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 1

    .prologue
    .line 1627598
    iget-object v0, p0, Lcom/facebook/widget/gridview/BetterGridView;->a:LX/2hv;

    invoke-virtual {v0, p1}, LX/2hv;->b(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1627599
    return-void
.end method

.method public setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 1

    .prologue
    .line 1627595
    iget-object v0, p0, Lcom/facebook/widget/gridview/BetterGridView;->a:LX/2hv;

    .line 1627596
    iput-object p1, v0, LX/2hv;->b:Landroid/widget/AbsListView$OnScrollListener;

    .line 1627597
    return-void
.end method
