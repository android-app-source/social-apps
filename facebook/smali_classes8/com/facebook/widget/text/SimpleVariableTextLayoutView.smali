.class public Lcom/facebook/widget/text/SimpleVariableTextLayoutView;
.super LX/2Wj;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2Wj",
        "<",
        "Ljava/lang/CharSequence;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/A8a;

.field private b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1627877
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1627878
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1627853
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1627854
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1627868
    invoke-direct {p0, p1, p2, p3}, LX/2Wj;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1627869
    new-instance v0, LX/A8a;

    invoke-direct {v0}, LX/A8a;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;->a:LX/A8a;

    .line 1627870
    sget-object v0, LX/03r;->SimpleVariableTextLayoutView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 1627871
    const/16 v0, 0x1

    invoke-static {p1, v1, v0}, LX/1z0;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v0

    .line 1627872
    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    invoke-virtual {p0, v0}, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;->setText(Ljava/lang/CharSequence;)V

    .line 1627873
    const/16 v0, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;->b:Z

    .line 1627874
    iget-object v0, p0, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;->a:LX/A8a;

    iget-boolean v2, p0, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;->b:Z

    invoke-virtual {v0, v2}, LX/A8a;->a(Z)V

    .line 1627875
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 1627876
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;)F
    .locals 4

    .prologue
    .line 1627863
    invoke-virtual {p0}, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;->getWidth()I

    move-result v1

    .line 1627864
    iget v0, p0, LX/2Wj;->f:I

    int-to-float v0, v0

    move v2, v0

    .line 1627865
    iget v0, p0, LX/2Wj;->e:I

    int-to-float v0, v0

    move v3, v0

    .line 1627866
    invoke-static {p1, v1, v2, v3}, LX/A8a;->a(Ljava/lang/CharSequence;IFF)F

    move-result v0

    .line 1627867
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v1, v0, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    return v0
.end method

.method public final synthetic a(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1627860
    check-cast p1, Ljava/lang/CharSequence;

    .line 1627861
    move-object v0, p1

    .line 1627862
    return-object v0
.end method

.method public getVariableTextLayoutComputer()LX/3L3;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/3L3",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1627859
    iget-object v0, p0, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;->a:LX/A8a;

    return-object v0
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 1627855
    if-nez p1, :cond_0

    .line 1627856
    const-string p1, ""

    .line 1627857
    :cond_0
    invoke-virtual {p0, p1}, LX/2Wj;->setData(Ljava/lang/Object;)V

    .line 1627858
    return-void
.end method
