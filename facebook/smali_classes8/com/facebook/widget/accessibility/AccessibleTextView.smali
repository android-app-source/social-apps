.class public Lcom/facebook/widget/accessibility/AccessibleTextView;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""

# interfaces
.implements LX/0wO;


# instance fields
.field private final a:LX/4oe;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4oe",
            "<",
            "Lcom/facebook/widget/accessibility/AccessibleTextView;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/view/accessibility/AccessibilityManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1413647
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/accessibility/AccessibleTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1413648
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1413688
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/widget/accessibility/AccessibleTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1413689
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1413683
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1413684
    new-instance v0, LX/4oe;

    invoke-direct {v0, p0}, LX/4oe;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/widget/accessibility/AccessibleTextView;->a:LX/4oe;

    .line 1413685
    iget-object v0, p0, Lcom/facebook/widget/accessibility/AccessibleTextView;->a:LX/4oe;

    invoke-static {p0, v0}, LX/0vv;->a(Landroid/view/View;LX/0vn;)V

    .line 1413686
    invoke-virtual {p0}, Lcom/facebook/widget/accessibility/AccessibleTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Lcom/facebook/widget/accessibility/AccessibleTextView;->b:Landroid/view/accessibility/AccessibilityManager;

    .line 1413687
    return-void
.end method

.method private a()V
    .locals 8
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1413667
    new-instance v1, LX/6WS;

    invoke-virtual {p0}, Lcom/facebook/widget/accessibility/AccessibleTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 1413668
    invoke-virtual {v1}, LX/5OM;->c()LX/5OG;

    move-result-object v2

    .line 1413669
    iget-object v0, p0, Lcom/facebook/widget/accessibility/AccessibleTextView;->a:LX/4oe;

    invoke-virtual {v0}, LX/4oe;->f()[Landroid/text/style/ClickableSpan;

    move-result-object v3

    .line 1413670
    const/4 v0, 0x0

    :goto_0
    array-length v4, v3

    if-ge v0, v4, :cond_1

    .line 1413671
    iget-object v4, p0, Lcom/facebook/widget/accessibility/AccessibleTextView;->a:LX/4oe;

    const/4 v5, 0x0

    .line 1413672
    add-int/lit8 v6, v0, 0x1

    invoke-virtual {v4}, LX/2da;->c()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v7

    if-lt v6, v7, :cond_2

    .line 1413673
    :cond_0
    :goto_1
    move-object v4, v5

    .line 1413674
    invoke-virtual {v2, v4}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v4

    .line 1413675
    new-instance v5, LX/8td;

    invoke-direct {v5, p0, v3, v0}, LX/8td;-><init>(Lcom/facebook/widget/accessibility/AccessibleTextView;[Landroid/text/style/ClickableSpan;I)V

    invoke-virtual {v4, v5}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1413676
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1413677
    :cond_1
    const v0, 0x7f080017

    invoke-virtual {v2, v0}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v0

    .line 1413678
    new-instance v2, LX/8te;

    invoke-direct {v2, p0, v1}, LX/8te;-><init>(Lcom/facebook/widget/accessibility/AccessibleTextView;LX/5OM;)V

    invoke-virtual {v0, v2}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1413679
    invoke-virtual {v1, p0}, LX/0ht;->f(Landroid/view/View;)V

    .line 1413680
    return-void

    .line 1413681
    :cond_2
    add-int/lit8 v6, v0, 0x1

    invoke-virtual {v4, v6}, LX/2da;->b(I)LX/4od;

    move-result-object v6

    .line 1413682
    if-eqz v6, :cond_0

    iget-object v5, v6, LX/4od;->a:Ljava/lang/String;

    goto :goto_1
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1413690
    iget-object v0, p0, Lcom/facebook/widget/accessibility/AccessibleTextView;->a:LX/4oe;

    .line 1413691
    invoke-virtual {v0}, LX/4oe;->f()[Landroid/text/style/ClickableSpan;

    move-result-object v2

    array-length v2, v2

    if-nez v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    move v0, v2

    .line 1413692
    if-eqz v0, :cond_0

    .line 1413693
    :goto_1
    return-void

    .line 1413694
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/accessibility/AccessibleTextView;->a:LX/4oe;

    const/4 v2, 0x1

    .line 1413695
    invoke-virtual {v0}, LX/4oe;->f()[Landroid/text/style/ClickableSpan;

    move-result-object v3

    array-length v3, v3

    if-ne v3, v2, :cond_3

    :goto_2
    move v0, v2

    .line 1413696
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/widget/accessibility/AccessibleTextView;->a:LX/4oe;

    invoke-virtual {v0, v1}, LX/4oe;->c(I)Landroid/text/style/ClickableSpan;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1413697
    iget-object v0, p0, Lcom/facebook/widget/accessibility/AccessibleTextView;->a:LX/4oe;

    invoke-virtual {v0, v1}, LX/4oe;->c(I)Landroid/text/style/ClickableSpan;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/text/style/ClickableSpan;->onClick(Landroid/view/View;)V

    goto :goto_1

    .line 1413698
    :cond_1
    invoke-direct {p0}, Lcom/facebook/widget/accessibility/AccessibleTextView;->a()V

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_2
.end method


# virtual methods
.method public getTextSize()F
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1413661
    invoke-virtual {p0}, Lcom/facebook/widget/accessibility/AccessibleTextView;->getLayout()Landroid/text/Layout;

    move-result-object v1

    .line 1413662
    if-eqz v1, :cond_0

    .line 1413663
    invoke-virtual {v1}, Landroid/text/Layout;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    invoke-virtual {v1}, Landroid/text/TextPaint;->getTextSize()F

    move-result v1

    .line 1413664
    cmpl-float v0, v1, v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    move v0, v1

    .line 1413665
    :cond_0
    return v0

    .line 1413666
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x392f6c96

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1413653
    invoke-virtual {p0}, Lcom/facebook/widget/accessibility/AccessibleTextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    if-nez v2, :cond_0

    .line 1413654
    const/4 v0, 0x0

    const v2, 0x18af564c

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1413655
    :goto_0
    return v0

    .line 1413656
    :cond_0
    iget-object v2, p0, Lcom/facebook/widget/accessibility/AccessibleTextView;->b:Landroid/view/accessibility/AccessibilityManager;

    invoke-static {v2}, LX/1d8;->b(Landroid/view/accessibility/AccessibilityManager;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1413657
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-eq v2, v0, :cond_1

    .line 1413658
    invoke-direct {p0}, Lcom/facebook/widget/accessibility/AccessibleTextView;->b()V

    .line 1413659
    :cond_1
    const v2, -0x406a7963

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0

    .line 1413660
    :cond_2
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, 0x377cf7d5

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public final performAccessibilityAction(ILandroid/os/Bundle;)Z
    .locals 1

    .prologue
    .line 1413649
    const/16 v0, 0x10

    if-ne p1, v0, :cond_0

    .line 1413650
    invoke-direct {p0}, Lcom/facebook/widget/accessibility/AccessibleTextView;->b()V

    .line 1413651
    const/4 v0, 0x1

    .line 1413652
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    move-result v0

    goto :goto_0
.end method
