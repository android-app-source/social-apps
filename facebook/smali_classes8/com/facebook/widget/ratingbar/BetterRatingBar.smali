.class public Lcom/facebook/widget/ratingbar/BetterRatingBar;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private final a:Landroid/graphics/drawable/Drawable;

.field private final b:Landroid/graphics/drawable/Drawable;

.field private final c:I

.field private final d:Z

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/8pS;",
            ">;"
        }
    .end annotation
.end field

.field public f:I

.field private g:I

.field private h:Z

.field private i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1414176
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/ratingbar/BetterRatingBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1414177
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1414153
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1414154
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->e:Ljava/util/List;

    .line 1414155
    iput v2, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->f:I

    .line 1414156
    iput v2, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->g:I

    .line 1414157
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->h:Z

    .line 1414158
    sget-object v0, LX/03r;->BetterRatingBar:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 1414159
    const/16 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1414160
    const/16 v1, 0x1

    invoke-virtual {v3, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1414161
    if-eqz v0, :cond_0

    :goto_0
    iput-object v0, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->a:Landroid/graphics/drawable/Drawable;

    .line 1414162
    if-eqz v1, :cond_1

    move-object v0, v1

    :goto_1
    iput-object v0, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->b:Landroid/graphics/drawable/Drawable;

    .line 1414163
    const/16 v0, 0x2

    const/4 v1, 0x5

    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->c:I

    .line 1414164
    const/16 v0, 0x3

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    move v0, v2

    .line 1414165
    :goto_2
    iget v4, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->c:I

    if-ge v0, v4, :cond_2

    .line 1414166
    new-instance v4, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/facebook/widget/ratingbar/BetterRatingBar;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1414167
    iget-object v5, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1414168
    invoke-virtual {v4, v1, v2, v1, v2}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 1414169
    invoke-virtual {p0, v4}, Lcom/facebook/widget/ratingbar/BetterRatingBar;->addView(Landroid/view/View;)V

    .line 1414170
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1414171
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f021889

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 1414172
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02188c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_1

    .line 1414173
    :cond_2
    const/16 v0, 0x4

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->d:Z

    .line 1414174
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 1414175
    return-void
.end method

.method private a(F)I
    .locals 3

    .prologue
    .line 1414148
    iget v0, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->c:I

    int-to-float v0, v0

    mul-float/2addr v0, p1

    invoke-virtual {p0}, Lcom/facebook/widget/ratingbar/BetterRatingBar;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    float-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    .line 1414149
    const/4 v1, 0x1

    iget v2, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->c:I

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1414150
    invoke-virtual {p0}, Lcom/facebook/widget/ratingbar/BetterRatingBar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0hL;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1414151
    iget v1, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->c:I

    sub-int v0, v1, v0

    add-int/lit8 v0, v0, 0x1

    .line 1414152
    :cond_0
    return v0
.end method

.method private a(Landroid/view/MotionEvent;I)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 1414135
    packed-switch p2, :pswitch_data_0

    .line 1414136
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 1414137
    :pswitch_0
    iput-boolean v2, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->i:Z

    move v0, v2

    .line 1414138
    goto :goto_0

    .line 1414139
    :pswitch_1
    iput-boolean v0, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->i:Z

    .line 1414140
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 1414141
    :pswitch_2
    iget-boolean v1, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->i:Z

    if-eqz v1, :cond_0

    .line 1414142
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-direct {p0, v1}, Lcom/facebook/widget/ratingbar/BetterRatingBar;->a(F)I

    move-result v1

    .line 1414143
    iget v3, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->f:I

    if-ne v1, v3, :cond_1

    iget v3, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->f:I

    if-eqz v3, :cond_1

    .line 1414144
    :goto_1
    iput v0, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->g:I

    .line 1414145
    invoke-direct {p0}, Lcom/facebook/widget/ratingbar/BetterRatingBar;->b()V

    .line 1414146
    invoke-direct {p0, v0}, Lcom/facebook/widget/ratingbar/BetterRatingBar;->c(I)V

    move v0, v2

    .line 1414147
    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private b()V
    .locals 4

    .prologue
    .line 1414123
    const/4 v0, 0x0

    move v1, v0

    .line 1414124
    :goto_0
    iget v0, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->g:I

    if-ge v1, v0, :cond_1

    .line 1414125
    invoke-virtual {p0, v1}, Lcom/facebook/widget/ratingbar/BetterRatingBar;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 1414126
    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->a:Landroid/graphics/drawable/Drawable;

    if-eq v2, v3, :cond_0

    .line 1414127
    iget-object v2, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1414128
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1414129
    :cond_1
    :goto_1
    iget v0, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->c:I

    if-ge v1, v0, :cond_3

    .line 1414130
    invoke-virtual {p0, v1}, Lcom/facebook/widget/ratingbar/BetterRatingBar;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 1414131
    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->b:Landroid/graphics/drawable/Drawable;

    if-eq v2, v3, :cond_2

    .line 1414132
    iget-object v2, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1414133
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1414134
    :cond_3
    return-void
.end method

.method private b(Landroid/view/MotionEvent;I)Z
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 1414118
    if-nez p2, :cond_0

    .line 1414119
    invoke-virtual {p0, v0}, Lcom/facebook/widget/ratingbar/BetterRatingBar;->requestDisallowInterceptTouchEvent(Z)V

    .line 1414120
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 1414121
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 1414122
    :pswitch_0
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/ratingbar/BetterRatingBar;->c(Landroid/view/MotionEvent;I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private c(I)V
    .locals 2

    .prologue
    .line 1414113
    iput p1, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->f:I

    .line 1414114
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->g:I

    .line 1414115
    iget-object v0, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8pS;

    .line 1414116
    invoke-interface {v0, p1}, LX/8pS;->a(I)V

    goto :goto_0

    .line 1414117
    :cond_0
    return-void
.end method

.method private c(Landroid/view/MotionEvent;I)V
    .locals 2

    .prologue
    .line 1414108
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/widget/ratingbar/BetterRatingBar;->a(F)I

    move-result v0

    .line 1414109
    invoke-direct {p0, v0}, Lcom/facebook/widget/ratingbar/BetterRatingBar;->d(I)V

    .line 1414110
    const/4 v1, 0x1

    if-eq p2, v1, :cond_0

    const/4 v1, 0x3

    if-ne p2, v1, :cond_1

    .line 1414111
    :cond_0
    invoke-direct {p0, v0}, Lcom/facebook/widget/ratingbar/BetterRatingBar;->c(I)V

    .line 1414112
    :cond_1
    return-void
.end method

.method private d(I)V
    .locals 4

    .prologue
    .line 1414101
    iget v0, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->g:I

    if-ne p1, v0, :cond_1

    .line 1414102
    :cond_0
    return-void

    .line 1414103
    :cond_1
    iget v1, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->g:I

    .line 1414104
    iput p1, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->g:I

    .line 1414105
    invoke-direct {p0}, Lcom/facebook/widget/ratingbar/BetterRatingBar;->b()V

    .line 1414106
    iget-object v0, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8pS;

    .line 1414107
    iget v3, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->g:I

    invoke-interface {v0, v1, v3}, LX/8pS;->a(II)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 1414099
    iget-object v0, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1414100
    return-void
.end method

.method public final a(LX/8pS;)V
    .locals 1

    .prologue
    .line 1414073
    iget-object v0, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1414074
    return-void
.end method

.method public getNumStars()I
    .locals 1

    .prologue
    .line 1414098
    iget v0, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->c:I

    return v0
.end method

.method public getRating()I
    .locals 1

    .prologue
    .line 1414097
    iget v0, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->f:I

    return v0
.end method

.method public final isEnabled()Z
    .locals 1

    .prologue
    .line 1414096
    iget-boolean v0, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->h:Z

    return v0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x2418ddf5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1414089
    iget-boolean v0, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->h:Z

    if-nez v0, :cond_0

    .line 1414090
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, 0x8d3ca66

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1414091
    :goto_0
    return v0

    .line 1414092
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 1414093
    iget-boolean v2, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->d:Z

    if-eqz v2, :cond_1

    .line 1414094
    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/ratingbar/BetterRatingBar;->a(Landroid/view/MotionEvent;I)Z

    move-result v0

    const v2, 0x3e944262

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0

    .line 1414095
    :cond_1
    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/ratingbar/BetterRatingBar;->b(Landroid/view/MotionEvent;I)Z

    move-result v0

    const v2, 0x6f9ed44f

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public setAccessibilityTextForEachStar(I)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 1414083
    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lcom/facebook/widget/ratingbar/BetterRatingBar;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 1414084
    invoke-virtual {p0, v0}, Lcom/facebook/widget/ratingbar/BetterRatingBar;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1414085
    invoke-virtual {v2, v7}, Landroid/view/View;->setFocusable(Z)V

    .line 1414086
    invoke-virtual {p0}, Lcom/facebook/widget/ratingbar/BetterRatingBar;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    add-int/lit8 v4, v0, 0x1

    new-array v5, v7, [Ljava/lang/Object;

    add-int/lit8 v6, v0, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, p1, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1414087
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1414088
    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 0

    .prologue
    .line 1414081
    iput-boolean p1, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->h:Z

    .line 1414082
    return-void
.end method

.method public setRating(I)V
    .locals 1

    .prologue
    .line 1414075
    if-ltz p1, :cond_0

    iget v0, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->c:I

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1414076
    iput p1, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->f:I

    .line 1414077
    iput p1, p0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->g:I

    .line 1414078
    invoke-direct {p0}, Lcom/facebook/widget/ratingbar/BetterRatingBar;->b()V

    .line 1414079
    return-void

    .line 1414080
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
