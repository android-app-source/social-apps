.class public Lcom/facebook/widget/ratingbar/FractionalRatingBar;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private final a:Landroid/graphics/drawable/Drawable;

.field private final b:Landroid/graphics/drawable/Drawable;

.field private final c:Landroid/graphics/drawable/Drawable;

.field private d:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1414222
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/ratingbar/FractionalRatingBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1414223
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    .line 1414187
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1414188
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/widget/ratingbar/FractionalRatingBar;->d:F

    .line 1414189
    sget-object v0, LX/03r;->FractionalRatingBar:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1414190
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1414191
    const/16 v2, 0x0

    const v3, 0x7f021895

    invoke-static {v2, v0, v1, v3}, Lcom/facebook/widget/ratingbar/FractionalRatingBar;->a(ILandroid/content/res/TypedArray;Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/widget/ratingbar/FractionalRatingBar;->a:Landroid/graphics/drawable/Drawable;

    .line 1414192
    const/16 v2, 0x1

    const v3, 0x7f021897

    invoke-static {v2, v0, v1, v3}, Lcom/facebook/widget/ratingbar/FractionalRatingBar;->a(ILandroid/content/res/TypedArray;Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/widget/ratingbar/FractionalRatingBar;->b:Landroid/graphics/drawable/Drawable;

    .line 1414193
    const/16 v2, 0x2

    const v3, 0x7f021898

    invoke-static {v2, v0, v1, v3}, Lcom/facebook/widget/ratingbar/FractionalRatingBar;->a(ILandroid/content/res/TypedArray;Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/ratingbar/FractionalRatingBar;->c:Landroid/graphics/drawable/Drawable;

    .line 1414194
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1414195
    invoke-direct {p0}, Lcom/facebook/widget/ratingbar/FractionalRatingBar;->a()V

    .line 1414196
    return-void
.end method

.method private static a(ILandroid/content/res/TypedArray;Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 1414220
    invoke-virtual {p1, p0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1414221
    if-nez v0, :cond_0

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1414214
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    .line 1414215
    new-instance v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/facebook/widget/ratingbar/FractionalRatingBar;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1414216
    iget-object v2, p0, Lcom/facebook/widget/ratingbar/FractionalRatingBar;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1414217
    invoke-virtual {p0, v1}, Lcom/facebook/widget/ratingbar/FractionalRatingBar;->addView(Landroid/view/View;)V

    .line 1414218
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1414219
    :cond_0
    return-void
.end method


# virtual methods
.method public setRating(F)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const-wide/high16 v6, 0x3fd0000000000000L    # 0.25

    .line 1414197
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_1

    const/high16 v0, 0x40a00000    # 5.0f

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1414198
    iget v0, p0, Lcom/facebook/widget/ratingbar/FractionalRatingBar;->d:F

    cmpl-float v0, p1, v0

    if-nez v0, :cond_2

    .line 1414199
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 1414200
    goto :goto_0

    .line 1414201
    :cond_2
    iput p1, p0, Lcom/facebook/widget/ratingbar/FractionalRatingBar;->d:F

    .line 1414202
    iget v0, p0, Lcom/facebook/widget/ratingbar/FractionalRatingBar;->d:F

    float-to-double v2, v0

    add-double/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    .line 1414203
    iget v0, p0, Lcom/facebook/widget/ratingbar/FractionalRatingBar;->d:F

    float-to-double v2, v0

    sub-double/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    move v2, v1

    .line 1414204
    :goto_1
    const/4 v0, 0x5

    if-ge v2, v0, :cond_0

    .line 1414205
    int-to-double v0, v2

    cmpg-double v0, v0, v4

    if-gez v0, :cond_4

    .line 1414206
    iget-object v0, p0, Lcom/facebook/widget/ratingbar/FractionalRatingBar;->a:Landroid/graphics/drawable/Drawable;

    move-object v1, v0

    .line 1414207
    :goto_2
    invoke-virtual {p0, v2}, Lcom/facebook/widget/ratingbar/FractionalRatingBar;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 1414208
    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    if-eq v3, v1, :cond_3

    .line 1414209
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1414210
    :cond_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 1414211
    :cond_4
    int-to-double v0, v2

    cmpl-double v0, v0, v6

    if-ltz v0, :cond_5

    .line 1414212
    iget-object v0, p0, Lcom/facebook/widget/ratingbar/FractionalRatingBar;->c:Landroid/graphics/drawable/Drawable;

    move-object v1, v0

    goto :goto_2

    .line 1414213
    :cond_5
    iget-object v0, p0, Lcom/facebook/widget/ratingbar/FractionalRatingBar;->b:Landroid/graphics/drawable/Drawable;

    move-object v1, v0

    goto :goto_2
.end method
