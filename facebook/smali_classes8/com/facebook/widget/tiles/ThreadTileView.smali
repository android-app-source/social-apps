.class public Lcom/facebook/widget/tiles/ThreadTileView;
.super Landroid/view/View;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/tiles/ThreadTileDrawableController;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8uX;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1FZ;",
            ">;"
        }
    .end annotation
.end field

.field private e:I

.field private f:Z

.field private g:LX/8Vc;

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1415377
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1415378
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1415379
    iput-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileView;->b:LX/0Ot;

    .line 1415380
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1415381
    iput-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileView;->d:LX/0Ot;

    .line 1415382
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/facebook/widget/tiles/ThreadTileView;->a(Landroid/util/AttributeSet;I)V

    .line 1415383
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1415313
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1415314
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1415315
    iput-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileView;->b:LX/0Ot;

    .line 1415316
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1415317
    iput-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileView;->d:LX/0Ot;

    .line 1415318
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/facebook/widget/tiles/ThreadTileView;->a(Landroid/util/AttributeSet;I)V

    .line 1415319
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1415320
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1415321
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1415322
    iput-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileView;->b:LX/0Ot;

    .line 1415323
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1415324
    iput-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileView;->d:LX/0Ot;

    .line 1415325
    invoke-direct {p0, p2, p3}, Lcom/facebook/widget/tiles/ThreadTileView;->a(Landroid/util/AttributeSet;I)V

    .line 1415326
    return-void
.end method

.method private a()V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 1415327
    invoke-virtual {p0}, Lcom/facebook/widget/tiles/ThreadTileView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/widget/tiles/ThreadTileView;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/facebook/widget/tiles/ThreadTileView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v0, v2

    .line 1415328
    invoke-virtual {p0}, Lcom/facebook/widget/tiles/ThreadTileView;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/widget/tiles/ThreadTileView;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/facebook/widget/tiles/ThreadTileView;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    .line 1415329
    iget-object v3, p0, Lcom/facebook/widget/tiles/ThreadTileView;->a:Lcom/facebook/widget/tiles/ThreadTileDrawableController;

    .line 1415330
    iget v4, v3, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->r:I

    move v4, v4

    .line 1415331
    iget v3, p0, Lcom/facebook/widget/tiles/ThreadTileView;->e:I

    and-int/lit8 v3, v3, 0x7

    sparse-switch v3, :sswitch_data_0

    .line 1415332
    sub-int/2addr v0, v4

    move v5, v0

    move v3, v1

    .line 1415333
    :goto_0
    iget v0, p0, Lcom/facebook/widget/tiles/ThreadTileView;->e:I

    and-int/lit8 v0, v0, 0x70

    sparse-switch v0, :sswitch_data_1

    .line 1415334
    sub-int v0, v2, v4

    move v6, v0

    move v4, v1

    .line 1415335
    :goto_1
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileView;->a:Lcom/facebook/widget/tiles/ThreadTileDrawableController;

    invoke-virtual {p0}, Lcom/facebook/widget/tiles/ThreadTileView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/widget/tiles/ThreadTileView;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/widget/tiles/ThreadTileView;->getPaddingLeft()I

    move-result v7

    add-int/2addr v3, v7

    invoke-virtual {p0}, Lcom/facebook/widget/tiles/ThreadTileView;->getPaddingTop()I

    move-result v7

    add-int/2addr v4, v7

    invoke-virtual {p0}, Lcom/facebook/widget/tiles/ThreadTileView;->getPaddingRight()I

    move-result v7

    add-int/2addr v5, v7

    invoke-virtual {p0}, Lcom/facebook/widget/tiles/ThreadTileView;->getPaddingBottom()I

    move-result v7

    add-int/2addr v6, v7

    const/4 p0, 0x0

    .line 1415336
    iget-object v7, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->m:Landroid/graphics/drawable/Drawable;

    sub-int v8, v1, v5

    sub-int v9, v2, v6

    invoke-virtual {v7, v3, v4, v8, v9}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1415337
    iget-object v7, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->q:Landroid/graphics/drawable/Drawable;

    if-eqz v7, :cond_0

    .line 1415338
    iget-object v7, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v7, p0, p0, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1415339
    :cond_0
    return-void

    .line 1415340
    :sswitch_0
    sub-int/2addr v0, v4

    move v5, v1

    move v3, v0

    .line 1415341
    goto :goto_0

    .line 1415342
    :sswitch_1
    sub-int/2addr v0, v4

    div-int/lit8 v0, v0, 0x2

    move v5, v0

    move v3, v0

    .line 1415343
    goto :goto_0

    .line 1415344
    :sswitch_2
    sub-int v0, v2, v4

    move v6, v1

    move v4, v0

    .line 1415345
    goto :goto_1

    .line 1415346
    :sswitch_3
    sub-int v0, v2, v4

    div-int/lit8 v1, v0, 0x2

    move v6, v1

    move v4, v1

    .line 1415347
    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x5 -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x10 -> :sswitch_3
        0x50 -> :sswitch_2
    .end sparse-switch
.end method

.method private a(Landroid/util/AttributeSet;I)V
    .locals 9

    .prologue
    .line 1415348
    const-class v0, Lcom/facebook/widget/tiles/ThreadTileView;

    invoke-static {v0, p0}, Lcom/facebook/widget/tiles/ThreadTileView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1415349
    invoke-virtual {p0}, Lcom/facebook/widget/tiles/ThreadTileView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->ThreadTileView:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1415350
    const/16 v1, 0x0

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/tiles/ThreadTileView;->e:I

    .line 1415351
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1415352
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileView;->a:Lcom/facebook/widget/tiles/ThreadTileDrawableController;

    invoke-virtual {p0}, Lcom/facebook/widget/tiles/ThreadTileView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1415353
    sget-object v2, LX/03r;->ThreadTileDrawable:[I

    invoke-virtual {v1, p1, v2, p2, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 1415354
    const/16 v2, 0x1

    invoke-virtual {v3, v2, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    .line 1415355
    const/16 v2, 0x0

    invoke-virtual {v3, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->q:Landroid/graphics/drawable/Drawable;

    .line 1415356
    const/16 v2, 0x2

    invoke-virtual {v3, v2, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    .line 1415357
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 1415358
    if-lez v4, :cond_2

    .line 1415359
    iput v4, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->r:I

    .line 1415360
    :goto_0
    new-instance v3, LX/8uU;

    invoke-direct {v3, v1, p1, p2}, LX/8uU;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v3, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->n:LX/8uU;

    .line 1415361
    new-instance v3, LX/8ui;

    invoke-direct {v3}, LX/8ui;-><init>()V

    iput-object v3, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->o:LX/8ui;

    .line 1415362
    iget-object v3, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->o:LX/8ui;

    const v4, 0x7f0a003d

    invoke-virtual {v3, v1, v4}, LX/8ui;->a(Landroid/content/Context;I)V

    .line 1415363
    if-gtz v2, :cond_0

    .line 1415364
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0057

    invoke-static {v2, v3}, LX/0tP;->a(Landroid/content/res/Resources;I)I

    move-result v2

    .line 1415365
    :cond_0
    iget-object v3, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->o:LX/8ui;

    int-to-float v2, v2

    invoke-virtual {v3, v2}, LX/8ui;->a(F)V

    .line 1415366
    new-instance v2, LX/8ug;

    invoke-direct {v2, v1, p1, p2}, LX/8ug;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v2, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->p:LX/8ug;

    .line 1415367
    new-instance v3, Landroid/graphics/drawable/LayerDrawable;

    iget-object v2, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->q:Landroid/graphics/drawable/Drawable;

    if-nez v2, :cond_3

    new-array v2, v8, [Landroid/graphics/drawable/Drawable;

    iget-object v4, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->n:LX/8uU;

    aput-object v4, v2, v5

    iget-object v4, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->o:LX/8ui;

    aput-object v4, v2, v6

    iget-object v4, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->p:LX/8ug;

    aput-object v4, v2, v7

    :goto_1
    invoke-direct {v3, v2}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    iput-object v3, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->m:Landroid/graphics/drawable/Drawable;

    .line 1415368
    iget-boolean v2, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->x:Z

    if-nez v2, :cond_1

    .line 1415369
    iget-object v2, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->A:LX/0T2;

    invoke-static {v2, v1}, LX/43j;->a(LX/0T2;Landroid/content/Context;)V

    .line 1415370
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileView;->a:Lcom/facebook/widget/tiles/ThreadTileDrawableController;

    .line 1415371
    iget-object v1, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->m:Landroid/graphics/drawable/Drawable;

    move-object v0, v1

    .line 1415372
    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1415373
    return-void

    .line 1415374
    :cond_2
    const/high16 v3, 0x42480000    # 50.0f

    invoke-static {v1, v3}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v3

    iput v3, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->r:I

    goto :goto_0

    .line 1415375
    :cond_3
    const/4 v2, 0x4

    new-array v2, v2, [Landroid/graphics/drawable/Drawable;

    iget-object v4, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->n:LX/8uU;

    aput-object v4, v2, v5

    iget-object v4, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->o:LX/8ui;

    aput-object v4, v2, v6

    iget-object v4, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->q:Landroid/graphics/drawable/Drawable;

    aput-object v4, v2, v7

    iget-object v4, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->p:LX/8ug;

    aput-object v4, v2, v8

    goto :goto_1
.end method

.method private static a(Lcom/facebook/widget/tiles/ThreadTileView;Lcom/facebook/widget/tiles/ThreadTileDrawableController;LX/0Ot;LX/0Uh;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/widget/tiles/ThreadTileView;",
            "Lcom/facebook/widget/tiles/ThreadTileDrawableController;",
            "LX/0Ot",
            "<",
            "LX/8uX;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Ot",
            "<",
            "LX/1FZ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1415376
    iput-object p1, p0, Lcom/facebook/widget/tiles/ThreadTileView;->a:Lcom/facebook/widget/tiles/ThreadTileDrawableController;

    iput-object p2, p0, Lcom/facebook/widget/tiles/ThreadTileView;->b:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/widget/tiles/ThreadTileView;->c:LX/0Uh;

    iput-object p4, p0, Lcom/facebook/widget/tiles/ThreadTileView;->d:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/widget/tiles/ThreadTileView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 5

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/widget/tiles/ThreadTileView;

    invoke-static {v2}, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->b(LX/0QB;)Lcom/facebook/widget/tiles/ThreadTileDrawableController;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;

    const/16 v1, 0x38a8

    invoke-static {v2, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {v2}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    const/16 v4, 0xb91

    invoke-static {v2, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {p0, v0, v3, v1, v2}, Lcom/facebook/widget/tiles/ThreadTileView;->a(Lcom/facebook/widget/tiles/ThreadTileView;Lcom/facebook/widget/tiles/ThreadTileDrawableController;LX/0Ot;LX/0Uh;LX/0Ot;)V

    return-void
.end method


# virtual methods
.method public final drawableStateChanged()V
    .locals 2

    .prologue
    .line 1415384
    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    .line 1415385
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileView;->a:Lcom/facebook/widget/tiles/ThreadTileDrawableController;

    .line 1415386
    iget-object v1, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->m:Landroid/graphics/drawable/Drawable;

    move-object v0, v1

    .line 1415387
    invoke-virtual {p0}, Lcom/facebook/widget/tiles/ThreadTileView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 1415388
    return-void
.end method

.method public final jumpDrawablesToCurrentState()V
    .locals 1

    .prologue
    .line 1415301
    invoke-super {p0}, Landroid/view/View;->jumpDrawablesToCurrentState()V

    .line 1415302
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileView;->a:Lcom/facebook/widget/tiles/ThreadTileDrawableController;

    .line 1415303
    iget-object p0, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->m:Landroid/graphics/drawable/Drawable;

    move-object v0, p0

    .line 1415304
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    .line 1415305
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x1fe776e7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1415306
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 1415307
    iget-object v1, p0, Lcom/facebook/widget/tiles/ThreadTileView;->a:Lcom/facebook/widget/tiles/ThreadTileDrawableController;

    .line 1415308
    iget-boolean v2, v1, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->u:Z

    if-nez v2, :cond_0

    .line 1415309
    :goto_0
    const/16 v1, 0x2d

    const v2, 0x157632f9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1415310
    :cond_0
    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->u:Z

    .line 1415311
    iget-object v2, v1, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->g:LX/0Yb;

    invoke-virtual {v2}, LX/0Yb;->b()V

    .line 1415312
    invoke-static {v1}, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->h(Lcom/facebook/widget/tiles/ThreadTileDrawableController;)V

    goto :goto_0
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x30c1c608

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1415227
    iget-object v1, p0, Lcom/facebook/widget/tiles/ThreadTileView;->a:Lcom/facebook/widget/tiles/ThreadTileDrawableController;

    .line 1415228
    iget-boolean v2, v1, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->u:Z

    if-eqz v2, :cond_0

    .line 1415229
    :goto_0
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 1415230
    const/16 v1, 0x2d

    const v2, -0x4f987063

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1415231
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->u:Z

    .line 1415232
    invoke-static {v1}, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->f(Lcom/facebook/widget/tiles/ThreadTileDrawableController;)V

    .line 1415233
    iget-object v2, v1, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->g:LX/0Yb;

    invoke-virtual {v2}, LX/0Yb;->c()V

    goto :goto_0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 1415269
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1415270
    iget-boolean v0, p0, Lcom/facebook/widget/tiles/ThreadTileView;->f:Z

    if-eqz v0, :cond_0

    .line 1415271
    invoke-direct {p0}, Lcom/facebook/widget/tiles/ThreadTileView;->a()V

    .line 1415272
    iput-boolean v2, p0, Lcom/facebook/widget/tiles/ThreadTileView;->f:Z

    .line 1415273
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/widget/tiles/ThreadTileView;->h:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileView;->c:LX/0Uh;

    const/16 v1, 0x1e8

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1415274
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileView;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8uX;

    .line 1415275
    iget-boolean v1, v0, LX/8uX;->b:Z

    if-eqz v1, :cond_4

    .line 1415276
    :goto_0
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileView;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8uX;

    iget-object v1, p0, Lcom/facebook/widget/tiles/ThreadTileView;->g:LX/8Vc;

    .line 1415277
    iget-object v2, v0, LX/8uX;->c:LX/8uW;

    invoke-virtual {v2, v1}, LX/8uW;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1FJ;

    move-object v0, v2

    .line 1415278
    if-eqz v0, :cond_1

    .line 1415279
    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v4, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1415280
    :goto_1
    return-void

    .line 1415281
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileView;->a:Lcom/facebook/widget/tiles/ThreadTileDrawableController;

    .line 1415282
    iget-boolean v1, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->y:Z

    move v0, v1

    .line 1415283
    if-eqz v0, :cond_2

    .line 1415284
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileView;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FZ;

    invoke-virtual {p0}, Lcom/facebook/widget/tiles/ThreadTileView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/widget/tiles/ThreadTileView;->getHeight()I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v0, v1, v2, v3}, LX/1FZ;->a(IILandroid/graphics/Bitmap$Config;)LX/1FJ;

    move-result-object v2

    .line 1415285
    invoke-virtual {v2}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 1415286
    iget-object v1, p0, Lcom/facebook/widget/tiles/ThreadTileView;->a:Lcom/facebook/widget/tiles/ThreadTileDrawableController;

    .line 1415287
    iget-object v3, v1, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->m:Landroid/graphics/drawable/Drawable;

    move-object v1, v3

    .line 1415288
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v3}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1415289
    iget-object v1, p0, Lcom/facebook/widget/tiles/ThreadTileView;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8uX;

    iget-object v3, p0, Lcom/facebook/widget/tiles/ThreadTileView;->g:LX/8Vc;

    .line 1415290
    iget-object p0, v1, LX/8uX;->c:LX/8uW;

    invoke-virtual {p0, v3, v2}, LX/8uW;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1415291
    invoke-virtual {p1, v0, v4, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_1

    .line 1415292
    :cond_2
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileView;->a:Lcom/facebook/widget/tiles/ThreadTileDrawableController;

    .line 1415293
    iget-object v1, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->m:Landroid/graphics/drawable/Drawable;

    move-object v0, v1

    .line 1415294
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_1

    .line 1415295
    :cond_3
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileView;->a:Lcom/facebook/widget/tiles/ThreadTileDrawableController;

    .line 1415296
    iget-object v1, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->m:Landroid/graphics/drawable/Drawable;

    move-object v0, v1

    .line 1415297
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_1

    .line 1415298
    :cond_4
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/8uX;->b:Z

    .line 1415299
    iget-object v1, v0, LX/8uX;->a:LX/0Xl;

    invoke-interface {v1}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    new-instance v3, LX/8uV;

    invoke-direct {v3, v0}, LX/8uV;-><init>(LX/8uX;)V

    invoke-interface {v1, v2, v3}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    .line 1415300
    invoke-interface {v1}, LX/0YX;->a()LX/0Yb;

    move-result-object v1

    invoke-virtual {v1}, LX/0Yb;->b()V

    goto/16 :goto_0
.end method

.method public final onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 1415234
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 1415235
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/tiles/ThreadTileView;->f:Z

    .line 1415236
    return-void
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 1415237
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileView;->a:Lcom/facebook/widget/tiles/ThreadTileDrawableController;

    .line 1415238
    iget v1, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->r:I

    move v0, v1

    .line 1415239
    invoke-static {v0, p1}, Landroid/view/View;->resolveSize(II)I

    move-result v1

    invoke-static {v0, p2}, Landroid/view/View;->resolveSize(II)I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/facebook/widget/tiles/ThreadTileView;->setMeasuredDimension(II)V

    .line 1415240
    return-void
.end method

.method public setAsFilledRoundRect(F)V
    .locals 1

    .prologue
    .line 1415241
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileView;->a:Lcom/facebook/widget/tiles/ThreadTileDrawableController;

    .line 1415242
    iget-object p0, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->n:LX/8uU;

    .line 1415243
    iget v0, p0, LX/8uU;->l:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1415244
    sget-object v0, LX/8uT;->FILLED_ROUND_RECT:LX/8uT;

    iput-object v0, p0, LX/8uU;->c:LX/8uT;

    .line 1415245
    iput p1, p0, LX/8uU;->g:F

    .line 1415246
    return-void

    .line 1415247
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setIsTileImageCacheAllowed(Z)V
    .locals 0

    .prologue
    .line 1415248
    iput-boolean p1, p0, Lcom/facebook/widget/tiles/ThreadTileView;->h:Z

    .line 1415249
    return-void
.end method

.method public setShouldDrawBackground(Z)V
    .locals 1

    .prologue
    .line 1415250
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileView;->a:Lcom/facebook/widget/tiles/ThreadTileDrawableController;

    .line 1415251
    iget-object p0, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->n:LX/8uU;

    .line 1415252
    iput-boolean p1, p0, LX/8uU;->m:Z

    .line 1415253
    return-void
.end method

.method public setThreadTileViewData(LX/8Vc;)V
    .locals 1

    .prologue
    .line 1415254
    iput-object p1, p0, Lcom/facebook/widget/tiles/ThreadTileView;->g:LX/8Vc;

    .line 1415255
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileView;->a:Lcom/facebook/widget/tiles/ThreadTileDrawableController;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->a(LX/8Vc;)V

    .line 1415256
    return-void
.end method

.method public setTileSizePx(I)V
    .locals 2

    .prologue
    .line 1415257
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileView;->a:Lcom/facebook/widget/tiles/ThreadTileDrawableController;

    .line 1415258
    if-lez p1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1415259
    iget v1, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->r:I

    if-eq v1, p1, :cond_0

    .line 1415260
    iput p1, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->r:I

    .line 1415261
    iget-object v1, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->n:LX/8uU;

    iget p0, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->r:I

    .line 1415262
    iput p0, v1, LX/8uU;->j:I

    .line 1415263
    iget-object v1, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->t:LX/8Vc;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->a(LX/8Vc;)V

    .line 1415264
    :cond_0
    return-void

    .line 1415265
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 2

    .prologue
    .line 1415266
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileView;->a:Lcom/facebook/widget/tiles/ThreadTileDrawableController;

    .line 1415267
    iget-object v1, v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->m:Landroid/graphics/drawable/Drawable;

    move-object v0, v1

    .line 1415268
    if-eq p1, v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
