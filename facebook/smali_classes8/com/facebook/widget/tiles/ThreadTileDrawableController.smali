.class public Lcom/facebook/widget/tiles/ThreadTileDrawableController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final A:LX/0T2;

.field private final b:Landroid/content/res/Resources;

.field public final c:LX/1HI;

.field public final d:Ljava/util/concurrent/Executor;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/03V;

.field public final g:LX/0Yb;

.field private final h:LX/0Zb;

.field public final i:LX/3el;

.field public final j:LX/0Uh;

.field public final k:LX/3Rd;

.field public final l:[LX/8uc;

.field public m:Landroid/graphics/drawable/Drawable;

.field public n:LX/8uU;

.field public o:LX/8ui;

.field public p:LX/8ug;

.field public q:Landroid/graphics/drawable/Drawable;

.field public r:I

.field public s:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field public t:LX/8Vc;

.field public u:Z

.field private v:Z

.field public w:Z

.field public x:Z

.field public y:Z

.field public z:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1415226
    const-class v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;

    const-string v1, "thread_tile_view"

    const-string v2, "thread_tile"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method private constructor <init>(LX/0Zb;Landroid/content/res/Resources;LX/1HI;Ljava/util/concurrent/Executor;LX/0Or;LX/03V;LX/0Xl;LX/0Or;LX/3el;LX/0Uh;LX/3Rd;)V
    .locals 4
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThreadImmediate;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/widget/tiles/annotations/IsProfilePictureDiskCacheEnabled;
        .end annotation
    .end param
    .param p7    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/widget/tiles/annotations/IsSkipUserTileClearingEnabled;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "Landroid/content/res/Resources;",
            "LX/1HI;",
            "Ljava/util/concurrent/Executor;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Xl;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/3el;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/3Rd;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1415204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1415205
    new-instance v0, LX/8uY;

    invoke-direct {v0, p0}, LX/8uY;-><init>(Lcom/facebook/widget/tiles/ThreadTileDrawableController;)V

    iput-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->A:LX/0T2;

    .line 1415206
    iput-object p2, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->b:Landroid/content/res/Resources;

    .line 1415207
    iput-object p3, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->c:LX/1HI;

    .line 1415208
    iput-object p4, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->d:Ljava/util/concurrent/Executor;

    .line 1415209
    iput-object p5, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->e:LX/0Or;

    .line 1415210
    iput-object p6, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->f:LX/03V;

    .line 1415211
    iput-boolean v2, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->w:Z

    .line 1415212
    iput-object p1, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->h:LX/0Zb;

    .line 1415213
    iput-object p9, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->i:LX/3el;

    .line 1415214
    iput-object p10, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->j:LX/0Uh;

    .line 1415215
    iput-object p11, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->k:LX/3Rd;

    .line 1415216
    invoke-interface {p8}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->x:Z

    .line 1415217
    iput-boolean v2, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->u:Z

    .line 1415218
    iput-boolean v1, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->v:Z

    .line 1415219
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, v3}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->z:Ljava/util/Set;

    .line 1415220
    new-array v0, v3, [LX/8uc;

    iput-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->l:[LX/8uc;

    move v0, v1

    .line 1415221
    :goto_0
    iget-object v2, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->l:[LX/8uc;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 1415222
    iget-object v2, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->l:[LX/8uc;

    new-instance v3, LX/8uc;

    invoke-direct {v3}, LX/8uc;-><init>()V

    aput-object v3, v2, v0

    .line 1415223
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1415224
    :cond_0
    invoke-interface {p7}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.users.ACTION_USERS_UPDATED"

    new-instance v2, LX/8uZ;

    invoke-direct {v2, p0}, LX/8uZ;-><init>(Lcom/facebook/widget/tiles/ThreadTileDrawableController;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->g:LX/0Yb;

    .line 1415225
    return-void
.end method

.method public static a(Lcom/facebook/widget/tiles/ThreadTileDrawableController;ILX/1bf;Z)V
    .locals 11

    .prologue
    .line 1415166
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->l:[LX/8uc;

    aget-object v0, v0, p1

    .line 1415167
    iget-object v1, v0, LX/8uc;->b:LX/1bf;

    invoke-static {v1, p2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->s:I

    iget-object v2, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->k:LX/3Rd;

    iget-object v3, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->t:LX/8Vc;

    invoke-virtual {v2, v3}, LX/3Rd;->a(LX/8Vc;)I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 1415168
    :cond_0
    :goto_0
    return-void

    .line 1415169
    :cond_1
    invoke-direct {p0}, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->g()V

    .line 1415170
    invoke-static {p0, p1}, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->b(Lcom/facebook/widget/tiles/ThreadTileDrawableController;I)V

    .line 1415171
    iput-object p2, v0, LX/8uc;->b:LX/1bf;

    .line 1415172
    if-eqz p2, :cond_0

    .line 1415173
    invoke-direct {p0}, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->l()Z

    .line 1415174
    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 1415175
    invoke-static {p2}, LX/1bX;->a(LX/1bf;)LX/1bX;

    move-result-object v7

    .line 1415176
    iget-object v4, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->e:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->t:LX/8Vc;

    invoke-interface {v4}, LX/8Vc;->c()Z

    move-result v4

    if-nez v4, :cond_2

    .line 1415177
    sget-object v4, LX/1bb;->SMALL:LX/1bb;

    .line 1415178
    iput-object v4, v7, LX/1bX;->f:LX/1bb;

    .line 1415179
    :cond_2
    iget-object v4, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->j:LX/0Uh;

    const/16 v5, 0x16b

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1415180
    iget v4, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->r:I

    if-lez v4, :cond_3

    .line 1415181
    new-instance v4, LX/1o9;

    iget v5, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->r:I

    iget v6, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->r:I

    invoke-direct {v4, v5, v6}, LX/1o9;-><init>(II)V

    .line 1415182
    iput-object v4, v7, LX/1bX;->c:LX/1o9;

    .line 1415183
    :goto_1
    iget-object v4, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->j:LX/0Uh;

    const/16 v5, 0x555

    invoke-virtual {v4, v5, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1415184
    invoke-virtual {v7}, LX/1bX;->n()LX/1bf;

    move-result-object v4

    .line 1415185
    iget-object v5, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->c:LX/1HI;

    .line 1415186
    iget-object v6, v5, LX/1HI;->e:LX/1Fh;

    move-object v5, v6

    .line 1415187
    iget-object v6, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->c:LX/1HI;

    .line 1415188
    iget-object v8, v6, LX/1HI;->i:LX/1Ao;

    move-object v6, v8

    .line 1415189
    invoke-virtual {v6, v4, v9}, LX/1Ao;->a(LX/1bf;Ljava/lang/Object;)LX/1bh;

    move-result-object v4

    invoke-interface {v5, v4}, LX/1Fh;->a(Ljava/lang/Object;)LX/1FJ;

    move-result-object v4

    .line 1415190
    invoke-static {v4}, LX/1FJ;->a(LX/1FJ;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1415191
    iput-object v9, v0, LX/8uc;->c:LX/1ca;

    .line 1415192
    iget-object v5, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->o:LX/8ui;

    invoke-virtual {v5}, LX/8ui;->a()V

    .line 1415193
    iget-object v5, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->l:[LX/8uc;

    aget-object v5, v5, p1

    invoke-static {p0, p1, v5, v4}, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->a(Lcom/facebook/widget/tiles/ThreadTileDrawableController;ILX/8uc;LX/1FJ;)V

    .line 1415194
    :goto_2
    goto :goto_0

    .line 1415195
    :cond_3
    new-instance v4, LX/1o9;

    iget-object v5, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->n:LX/8uU;

    invoke-virtual {v5}, LX/8uU;->getBounds()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    iget-object v6, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->n:LX/8uU;

    invoke-virtual {v6}, LX/8uU;->getBounds()Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    invoke-direct {v4, v5, v6}, LX/1o9;-><init>(II)V

    .line 1415196
    iput-object v4, v7, LX/1bX;->c:LX/1o9;

    .line 1415197
    goto :goto_1

    .line 1415198
    :cond_4
    iget-object v4, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->i:LX/3el;

    invoke-virtual {v4, p2}, LX/3el;->a(LX/1bf;)Ljava/util/concurrent/ExecutorService;

    move-result-object v10

    new-instance v4, Lcom/facebook/widget/tiles/ThreadTileDrawableController$3;

    move-object v5, p0

    move-object v6, v0

    move v8, p1

    move v9, p3

    invoke-direct/range {v4 .. v9}, Lcom/facebook/widget/tiles/ThreadTileDrawableController$3;-><init>(Lcom/facebook/widget/tiles/ThreadTileDrawableController;LX/8uc;LX/1bX;IZ)V

    const v5, 0x6f010a62

    invoke-static {v10, v4, v5}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    move-result-object v4

    iput-object v4, v0, LX/8uc;->a:Ljava/util/concurrent/Future;

    goto :goto_2

    .line 1415199
    :cond_5
    iget-object v4, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->c:LX/1HI;

    invoke-virtual {v7}, LX/1bX;->n()LX/1bf;

    move-result-object v5

    .line 1415200
    sget-object v6, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->a:Lcom/facebook/common/callercontext/CallerContext;

    move-object v6, v6

    .line 1415201
    invoke-virtual {v4, v5, v6}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v4

    iput-object v4, v0, LX/8uc;->c:LX/1ca;

    .line 1415202
    new-instance v5, LX/8ub;

    invoke-direct {v5, p0, p1, p3}, LX/8ub;-><init>(Lcom/facebook/widget/tiles/ThreadTileDrawableController;IZ)V

    .line 1415203
    iget-object v6, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->d:Ljava/util/concurrent/Executor;

    invoke-interface {v4, v5, v6}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    goto :goto_2
.end method

.method public static a(Lcom/facebook/widget/tiles/ThreadTileDrawableController;ILX/8uc;LX/1FJ;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/8uc;",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1415144
    iput-object p3, p2, LX/8uc;->d:LX/1FJ;

    .line 1415145
    invoke-virtual {p3}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lm;

    .line 1415146
    iget-object v1, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->n:LX/8uU;

    invoke-virtual {v0}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1415147
    if-ltz p1, :cond_1

    iget v3, v1, LX/8uU;->l:I

    if-ge p1, v3, :cond_1

    if-eqz v2, :cond_1

    const/4 v3, 0x1

    :goto_0
    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 1415148
    iget-object v3, v1, LX/8uU;->k:[Landroid/graphics/drawable/ShapeDrawable;

    aget-object v5, v3, p1

    .line 1415149
    invoke-virtual {v5}, Landroid/graphics/drawable/ShapeDrawable;->getShape()Landroid/graphics/drawable/shapes/Shape;

    move-result-object v4

    .line 1415150
    instance-of v3, v4, LX/7U6;

    if-eqz v3, :cond_2

    move-object v3, v4

    check-cast v3, LX/7U6;

    .line 1415151
    :goto_1
    new-instance p3, LX/7U7;

    invoke-direct {p3, v3, v2}, LX/7U7;-><init>(LX/7U6;Landroid/graphics/Bitmap;)V

    invoke-virtual {v5, p3}, Landroid/graphics/drawable/ShapeDrawable;->setShaderFactory(Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;)V

    .line 1415152
    invoke-virtual {v5, v4}, Landroid/graphics/drawable/ShapeDrawable;->setShape(Landroid/graphics/drawable/shapes/Shape;)V

    .line 1415153
    iget-object v1, p2, LX/8uc;->b:LX/1bf;

    .line 1415154
    const/4 v2, 0x0

    iput v2, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->s:I

    .line 1415155
    iget-object v2, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->t:LX/8Vc;

    invoke-interface {v2}, LX/8Vc;->b()LX/8ue;

    move-result-object v2

    invoke-static {v2, v1}, LX/3Rd;->a(LX/8ue;LX/1bf;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1415156
    iget-object v2, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->k:LX/3Rd;

    iget-object v3, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->t:LX/8Vc;

    invoke-virtual {v2, v3}, LX/3Rd;->a(LX/8Vc;)I

    move-result v2

    iput v2, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->s:I

    .line 1415157
    :cond_0
    iget v2, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->s:I

    if-eqz v2, :cond_3

    .line 1415158
    iget-object v2, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->n:LX/8uU;

    iget v3, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->s:I

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v3, v4}, LX/8uU;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 1415159
    :goto_2
    iget-object v1, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->h:LX/0Zb;

    .line 1415160
    sget-object v2, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->a:Lcom/facebook/common/callercontext/CallerContext;

    move-object v2, v2

    .line 1415161
    iget-object v3, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->n:LX/8uU;

    invoke-virtual {v0}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v1, v2, v3, v0}, Lcom/facebook/widget/tiles/InstrumentTile;->a(LX/0Zb;Lcom/facebook/common/callercontext/CallerContext;Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;)V

    .line 1415162
    return-void

    .line 1415163
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 1415164
    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    .line 1415165
    :cond_3
    iget-object v2, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->n:LX/8uU;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/8uU;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_2
.end method

.method public static a$redex0(Lcom/facebook/widget/tiles/ThreadTileDrawableController;ILX/1ca;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1415120
    invoke-interface {p2}, LX/1ca;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1415121
    :cond_0
    :goto_0
    return-void

    .line 1415122
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->l:[LX/8uc;

    aget-object v1, v0, p1

    .line 1415123
    iget-object v0, v1, LX/8uc;->c:LX/1ca;

    if-eqz v0, :cond_2

    iget-object v0, v1, LX/8uc;->c:LX/1ca;

    if-ne v0, p2, :cond_0

    .line 1415124
    :cond_2
    const/4 v0, 0x0

    iput-object v0, v1, LX/8uc;->c:LX/1ca;

    .line 1415125
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->n:LX/8uU;

    .line 1415126
    iget v2, v0, LX/8uU;->l:I

    move v0, v2

    .line 1415127
    if-lt p1, v0, :cond_3

    .line 1415128
    iget-object v1, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->f:LX/03V;

    const-string v2, "T5504543"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Setting ThreadTile at an invalid index ("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", tileCount = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1415129
    :cond_3
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->o:LX/8ui;

    invoke-virtual {v0}, LX/8ui;->a()V

    .line 1415130
    invoke-interface {p2}, LX/1ca;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 1415131
    if-eqz v0, :cond_5

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, LX/1lm;

    if-eqz v2, :cond_5

    .line 1415132
    invoke-static {p0, p1, v1, v0}, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->a(Lcom/facebook/widget/tiles/ThreadTileDrawableController;ILX/8uc;LX/1FJ;)V

    .line 1415133
    :goto_1
    const/4 v0, 0x0

    .line 1415134
    iget-boolean v1, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->y:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 1415135
    iget-object v2, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->l:[LX/8uc;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    .line 1415136
    iget-object p2, v4, LX/8uc;->b:LX/1bf;

    if-eqz p2, :cond_6

    iget-object v4, v4, LX/8uc;->c:LX/1ca;

    if-eqz v4, :cond_6

    .line 1415137
    :cond_4
    :goto_3
    goto :goto_0

    .line 1415138
    :cond_5
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_1

    .line 1415139
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1415140
    :cond_7
    iget-object v1, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->z:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1415141
    iget-object v1, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->t:LX/8Vc;

    if-eqz v1, :cond_8

    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->t:LX/8Vc;

    invoke-interface {v0}, LX/8Vc;->a()I

    move-result v0

    .line 1415142
    :cond_8
    iget-object v1, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->z:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-ne v1, v0, :cond_4

    .line 1415143
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->y:Z

    goto :goto_3
.end method

.method public static a$redex0(Lcom/facebook/widget/tiles/ThreadTileDrawableController;IZ)V
    .locals 3

    .prologue
    .line 1415108
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->n:LX/8uU;

    .line 1415109
    iget-object v1, v0, LX/8uU;->k:[Landroid/graphics/drawable/ShapeDrawable;

    aget-object v1, v1, p1

    .line 1415110
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/graphics/drawable/ShapeDrawable;->getShaderFactory()Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;

    move-result-object v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 1415111
    if-eqz v0, :cond_1

    .line 1415112
    :cond_0
    :goto_1
    return-void

    .line 1415113
    :cond_1
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->t:LX/8Vc;

    invoke-interface {v0}, LX/8Vc;->b()LX/8ue;

    move-result-object v0

    sget-object v1, LX/8ue;->SMS:LX/8ue;

    if-ne v0, v1, :cond_0

    .line 1415114
    invoke-direct {p0}, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->g()V

    .line 1415115
    invoke-direct {p0}, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1415116
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->l:[LX/8uc;

    aget-object v0, v0, p1

    const/4 v1, 0x0

    iput-object v1, v0, LX/8uc;->b:LX/1bf;

    goto :goto_1

    .line 1415117
    :cond_2
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->t:LX/8Vc;

    iget v1, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->r:I

    iget v2, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->r:I

    invoke-interface {v0, p1, v1, v2}, LX/8Vc;->b(III)LX/1bf;

    move-result-object v0

    .line 1415118
    if-eqz v0, :cond_0

    .line 1415119
    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->a(Lcom/facebook/widget/tiles/ThreadTileDrawableController;ILX/1bf;Z)V

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)Lcom/facebook/widget/tiles/ThreadTileDrawableController;
    .locals 12

    .prologue
    .line 1415106
    new-instance v0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    invoke-static {p0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v3

    check-cast v3, LX/1HI;

    invoke-static {p0}, LX/0W1;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/Executor;

    const/16 v5, 0x15a0

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v7

    check-cast v7, LX/0Xl;

    const/16 v8, 0x15a1

    invoke-static {p0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {p0}, LX/3el;->a(LX/0QB;)LX/3el;

    move-result-object v9

    check-cast v9, LX/3el;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    invoke-static {p0}, LX/3Rd;->b(LX/0QB;)LX/3Rd;

    move-result-object v11

    check-cast v11, LX/3Rd;

    invoke-direct/range {v0 .. v11}, Lcom/facebook/widget/tiles/ThreadTileDrawableController;-><init>(LX/0Zb;Landroid/content/res/Resources;LX/1HI;Ljava/util/concurrent/Executor;LX/0Or;LX/03V;LX/0Xl;LX/0Or;LX/3el;LX/0Uh;LX/3Rd;)V

    .line 1415107
    return-object v0
.end method

.method public static b(Lcom/facebook/widget/tiles/ThreadTileDrawableController;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1415030
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->l:[LX/8uc;

    aget-object v0, v0, p1

    .line 1415031
    iget-object v1, v0, LX/8uc;->d:LX/1FJ;

    if-eqz v1, :cond_0

    .line 1415032
    iget-object v1, v0, LX/8uc;->d:LX/1FJ;

    invoke-virtual {v1}, LX/1FJ;->close()V

    .line 1415033
    iput-object v3, v0, LX/8uc;->d:LX/1FJ;

    .line 1415034
    :cond_0
    iget-object v1, v0, LX/8uc;->c:LX/1ca;

    if-eqz v1, :cond_1

    .line 1415035
    iget-object v1, v0, LX/8uc;->c:LX/1ca;

    invoke-interface {v1}, LX/1ca;->g()Z

    .line 1415036
    iput-object v3, v0, LX/8uc;->c:LX/1ca;

    .line 1415037
    :cond_1
    iget-object v1, v0, LX/8uc;->a:Ljava/util/concurrent/Future;

    if-eqz v1, :cond_2

    .line 1415038
    iget-object v1, v0, LX/8uc;->a:Ljava/util/concurrent/Future;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 1415039
    iput-object v3, v0, LX/8uc;->a:Ljava/util/concurrent/Future;

    .line 1415040
    :cond_2
    iput-object v3, v0, LX/8uc;->b:LX/1bf;

    .line 1415041
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->n:LX/8uU;

    const/4 p0, 0x0

    .line 1415042
    if-ltz p1, :cond_4

    iget-object v1, v0, LX/8uU;->k:[Landroid/graphics/drawable/ShapeDrawable;

    array-length v1, v1

    if-ge p1, v1, :cond_4

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1415043
    iget-object v1, v0, LX/8uU;->k:[Landroid/graphics/drawable/ShapeDrawable;

    aget-object v1, v1, p1

    .line 1415044
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/graphics/drawable/ShapeDrawable;->getShaderFactory()Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;

    move-result-object v2

    if-nez v2, :cond_5

    .line 1415045
    :cond_3
    :goto_1
    return-void

    .line 1415046
    :cond_4
    const/4 v1, 0x0

    goto :goto_0

    .line 1415047
    :cond_5
    invoke-virtual {v1}, Landroid/graphics/drawable/ShapeDrawable;->getShape()Landroid/graphics/drawable/shapes/Shape;

    move-result-object v2

    .line 1415048
    invoke-virtual {v1, p0}, Landroid/graphics/drawable/ShapeDrawable;->setShaderFactory(Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;)V

    .line 1415049
    invoke-virtual {v1}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 1415050
    invoke-virtual {v1, v2}, Landroid/graphics/drawable/ShapeDrawable;->setShape(Landroid/graphics/drawable/shapes/Shape;)V

    goto :goto_1
.end method

.method public static f(Lcom/facebook/widget/tiles/ThreadTileDrawableController;)V
    .locals 2

    .prologue
    .line 1415100
    iget-boolean v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->u:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->w:Z

    if-eqz v0, :cond_1

    .line 1415101
    :cond_0
    return-void

    .line 1415102
    :cond_1
    invoke-static {p0}, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->j(Lcom/facebook/widget/tiles/ThreadTileDrawableController;)V

    .line 1415103
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->l:[LX/8uc;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 1415104
    invoke-static {p0, v0}, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->b(Lcom/facebook/widget/tiles/ThreadTileDrawableController;I)V

    .line 1415105
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private g()V
    .locals 3

    .prologue
    .line 1415092
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->o:LX/8ui;

    .line 1415093
    iget-object v1, v0, LX/8ui;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1415094
    if-nez v0, :cond_1

    .line 1415095
    :cond_0
    :goto_0
    return-void

    .line 1415096
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->o:LX/8ui;

    iget-object v1, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->t:LX/8Vc;

    invoke-interface {v1}, LX/8Vc;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8ui;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->o:LX/8ui;

    .line 1415097
    iget v1, v0, LX/8ui;->f:I

    move v0, v1

    .line 1415098
    iget-object v1, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->k:LX/3Rd;

    iget-object v2, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->t:LX/8Vc;

    invoke-virtual {v1, v2}, LX/3Rd;->a(LX/8Vc;)I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 1415099
    :cond_2
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->o:LX/8ui;

    invoke-virtual {v0}, LX/8ui;->a()V

    goto :goto_0
.end method

.method public static h(Lcom/facebook/widget/tiles/ThreadTileDrawableController;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1415071
    iget-boolean v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->u:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->w:Z

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->v:Z

    if-nez v0, :cond_1

    .line 1415072
    :goto_0
    return-void

    .line 1415073
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->t:LX/8Vc;

    instance-of v0, v0, LX/8ud;

    if-eqz v0, :cond_4

    .line 1415074
    invoke-static {p0}, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->j(Lcom/facebook/widget/tiles/ThreadTileDrawableController;)V

    .line 1415075
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->l:[LX/8uc;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 1415076
    invoke-static {p0, v0}, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->b(Lcom/facebook/widget/tiles/ThreadTileDrawableController;I)V

    .line 1415077
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1415078
    :cond_2
    invoke-direct {p0}, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->l()Z

    .line 1415079
    :cond_3
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->t:LX/8Vc;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->t:LX/8Vc;

    invoke-interface {v0}, LX/8Vc;->b()LX/8ue;

    move-result-object v0

    .line 1415080
    :goto_2
    iget-object v1, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->p:LX/8ug;

    invoke-virtual {v1, v0}, LX/8ug;->a(LX/8ue;)V

    goto :goto_0

    .line 1415081
    :cond_4
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->t:LX/8Vc;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->t:LX/8Vc;

    invoke-interface {v0}, LX/8Vc;->a()I

    move-result v0

    .line 1415082
    :goto_3
    iget-object v2, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->n:LX/8uU;

    invoke-virtual {v2, v0}, LX/8uU;->a(I)V

    .line 1415083
    :goto_4
    iget-object v2, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->l:[LX/8uc;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 1415084
    if-lt v1, v0, :cond_6

    .line 1415085
    invoke-static {p0, v1}, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->b(Lcom/facebook/widget/tiles/ThreadTileDrawableController;I)V

    .line 1415086
    :goto_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_5
    move v0, v1

    .line 1415087
    goto :goto_3

    .line 1415088
    :cond_6
    iget-object v2, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->t:LX/8Vc;

    iget v3, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->r:I

    iget v4, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->r:I

    invoke-interface {v2, v1, v3, v4}, LX/8Vc;->a(III)LX/1bf;

    move-result-object v2

    .line 1415089
    const/4 v3, 0x1

    invoke-static {p0, v1, v2, v3}, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->a(Lcom/facebook/widget/tiles/ThreadTileDrawableController;ILX/1bf;Z)V

    .line 1415090
    goto :goto_5

    .line 1415091
    :cond_7
    sget-object v0, LX/8ue;->NONE:LX/8ue;

    goto :goto_2
.end method

.method public static j(Lcom/facebook/widget/tiles/ThreadTileDrawableController;)V
    .locals 1

    .prologue
    .line 1415068
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->z:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1415069
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->y:Z

    .line 1415070
    return-void
.end method

.method private l()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1415055
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->t:LX/8Vc;

    invoke-interface {v0}, LX/8Vc;->b()LX/8ue;

    move-result-object v0

    sget-object v2, LX/8ue;->SMS:LX/8ue;

    if-ne v0, v2, :cond_0

    .line 1415056
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->o:LX/8ui;

    iget-object v2, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->t:LX/8Vc;

    invoke-interface {v2}, LX/8Vc;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/8ui;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1415057
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->o:LX/8ui;

    iget-object v2, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->b:Landroid/content/res/Resources;

    const v3, 0x7f0a0048

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, LX/8ui;->b(I)V

    .line 1415058
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->k:LX/3Rd;

    iget-object v2, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->t:LX/8Vc;

    invoke-virtual {v0, v2}, LX/3Rd;->a(LX/8Vc;)I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->s:I

    .line 1415059
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->o:LX/8ui;

    iget v2, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->s:I

    invoke-virtual {v0, v2}, LX/8ui;->c(I)V

    move v0, v1

    .line 1415060
    :goto_0
    return v0

    .line 1415061
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->t:LX/8Vc;

    instance-of v0, v0, LX/8ud;

    if-eqz v0, :cond_1

    .line 1415062
    iget-object v0, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->t:LX/8Vc;

    check-cast v0, LX/8ud;

    .line 1415063
    iget-object v2, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->o:LX/8ui;

    iget-object v3, v0, LX/8ud;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/8ui;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1415064
    iget-object v2, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->o:LX/8ui;

    iget v3, v0, LX/8ud;->b:I

    invoke-virtual {v2, v3}, LX/8ui;->b(I)V

    .line 1415065
    iget-object v2, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->o:LX/8ui;

    iget v0, v0, LX/8ud;->c:I

    invoke-virtual {v2, v0}, LX/8ui;->c(I)V

    move v0, v1

    .line 1415066
    goto :goto_0

    .line 1415067
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/8Vc;)V
    .locals 0

    .prologue
    .line 1415051
    iput-object p1, p0, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->t:LX/8Vc;

    .line 1415052
    invoke-static {p0}, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->j(Lcom/facebook/widget/tiles/ThreadTileDrawableController;)V

    .line 1415053
    invoke-static {p0}, Lcom/facebook/widget/tiles/ThreadTileDrawableController;->h(Lcom/facebook/widget/tiles/ThreadTileDrawableController;)V

    .line 1415054
    return-void
.end method
