.class public Lcom/facebook/widget/popover/SimplePopoverFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:Z

.field public o:LX/8qU;

.field public p:LX/8ts;

.field public q:Ljava/lang/Runnable;

.field public r:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1407324
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 1407325
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/popover/SimplePopoverFragment;->n:Z

    .line 1407326
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/widget/popover/SimplePopoverFragment;

    invoke-static {p0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object p0

    check-cast p0, Landroid/os/Handler;

    iput-object p0, p1, Lcom/facebook/widget/popover/SimplePopoverFragment;->m:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public S_()Z
    .locals 2

    .prologue
    .line 1407327
    iget-boolean v0, p0, Lcom/facebook/widget/popover/SimplePopoverFragment;->n:Z

    if-eqz v0, :cond_0

    .line 1407328
    iget-object v0, p0, Lcom/facebook/widget/popover/SimplePopoverFragment;->p:LX/8ts;

    .line 1407329
    sget-object v1, LX/31M;->DOWN:LX/31M;

    invoke-static {v0, v1}, LX/8ts;->a(LX/8ts;LX/31M;)V

    .line 1407330
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 1407331
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v0

    .line 1407332
    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 1407333
    :goto_1
    if-eqz v0, :cond_1

    .line 1407334
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/facebook/widget/popover/SimplePopoverFragment;->r:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 1407335
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->c()V

    goto :goto_0

    .line 1407336
    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v0

    .line 1407337
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1407349
    new-instance v0, LX/8to;

    invoke-direct {v0, p0}, LX/8to;-><init>(Lcom/facebook/widget/popover/SimplePopoverFragment;)V

    .line 1407350
    invoke-virtual {p0}, Lcom/facebook/widget/popover/SimplePopoverFragment;->k()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1407351
    invoke-virtual {p0, v0}, Lcom/facebook/widget/popover/SimplePopoverFragment;->a(Landroid/app/Dialog;)V

    .line 1407352
    :cond_0
    return-object v0
.end method

.method public a(Landroid/app/Dialog;)V
    .locals 0

    .prologue
    .line 1407338
    invoke-static {p1}, LX/8ti;->a(Landroid/app/Dialog;)V

    .line 1407339
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1407340
    iget-object v0, p0, Lcom/facebook/widget/popover/SimplePopoverFragment;->p:LX/8ts;

    if-eqz v0, :cond_1

    .line 1407341
    iget-object v0, p0, Lcom/facebook/widget/popover/SimplePopoverFragment;->p:LX/8ts;

    .line 1407342
    iget-object v1, v0, LX/8ts;->f:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    const-string p0, "In order to set the footer, the footer needs to be in the layout."

    invoke-static {v1, p0}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1407343
    iget-object v1, v0, LX/8ts;->f:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1407344
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1407345
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1407346
    :cond_0
    iget-object v1, v0, LX/8ts;->f:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1407347
    :cond_1
    return-void

    .line 1407348
    :cond_2
    iget-object v1, v0, LX/8ts;->f:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    goto :goto_0
.end method

.method public final eK_()I
    .locals 1

    .prologue
    .line 1407318
    iget-boolean v0, p0, Lcom/facebook/widget/popover/SimplePopoverFragment;->n:Z

    if-eqz v0, :cond_1

    .line 1407319
    invoke-virtual {p0}, Lcom/facebook/widget/popover/SimplePopoverFragment;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0e05f9

    .line 1407320
    :goto_0
    return v0

    .line 1407321
    :cond_0
    const v0, 0x7f0e05fb

    goto :goto_0

    .line 1407322
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/widget/popover/SimplePopoverFragment;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0e05fc

    goto :goto_0

    :cond_2
    const v0, 0x7f0e05fd

    goto :goto_0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 1407323
    const/4 v0, 0x1

    return v0
.end method

.method public l()LX/8qU;
    .locals 1

    .prologue
    .line 1407317
    new-instance v0, LX/8qV;

    invoke-direct {v0, p0}, LX/8qV;-><init>(Lcom/facebook/widget/popover/SimplePopoverFragment;)V

    return-object v0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 1407312
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1407313
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v0

    .line 1407314
    invoke-virtual {p0}, Lcom/facebook/widget/popover/SimplePopoverFragment;->k()Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1407315
    invoke-virtual {p0, v0}, Lcom/facebook/widget/popover/SimplePopoverFragment;->a(Landroid/app/Dialog;)V

    .line 1407316
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x2

    const/16 v0, 0x2a

    const v1, 0x4eeab75

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1407305
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1407306
    const-class v1, Lcom/facebook/widget/popover/SimplePopoverFragment;

    invoke-static {v1, p0}, Lcom/facebook/widget/popover/SimplePopoverFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1407307
    invoke-virtual {p0}, Lcom/facebook/widget/popover/SimplePopoverFragment;->l()LX/8qU;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/popover/SimplePopoverFragment;->o:LX/8qU;

    .line 1407308
    iget-boolean v1, p0, Lcom/facebook/widget/popover/SimplePopoverFragment;->n:Z

    if-nez v1, :cond_0

    .line 1407309
    new-instance v1, Lcom/facebook/widget/popover/SimplePopoverFragment$1;

    invoke-direct {v1, p0}, Lcom/facebook/widget/popover/SimplePopoverFragment$1;-><init>(Lcom/facebook/widget/popover/SimplePopoverFragment;)V

    iput-object v1, p0, Lcom/facebook/widget/popover/SimplePopoverFragment;->q:Ljava/lang/Runnable;

    .line 1407310
    iget-object v1, p0, Lcom/facebook/widget/popover/SimplePopoverFragment;->m:Landroid/os/Handler;

    iget-object v2, p0, Lcom/facebook/widget/popover/SimplePopoverFragment;->q:Ljava/lang/Runnable;

    const-wide/16 v4, 0x226

    const v3, -0x39cbb17f

    invoke-static {v1, v2, v4, v5, v3}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1407311
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x758e8f70

    invoke-static {v6, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x1f7ef992

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1407275
    new-instance v1, LX/8ts;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 1407276
    const v3, 0x7f030feb

    move v3, v3

    .line 1407277
    invoke-direct {v1, v2, v3}, LX/8ts;-><init>(Landroid/content/Context;I)V

    iget-object v2, p0, Lcom/facebook/widget/popover/SimplePopoverFragment;->o:LX/8qU;

    .line 1407278
    iput-object v2, v1, LX/8ts;->d:LX/8qU;

    .line 1407279
    move-object v1, v1

    .line 1407280
    iput-object v1, p0, Lcom/facebook/widget/popover/SimplePopoverFragment;->p:LX/8ts;

    .line 1407281
    iget-object v1, p0, Lcom/facebook/widget/popover/SimplePopoverFragment;->p:LX/8ts;

    const/16 v2, 0x2b

    const v3, -0x64a19d33

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4c2d2fa

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1407301
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDestroy()V

    .line 1407302
    iget-object v1, p0, Lcom/facebook/widget/popover/SimplePopoverFragment;->q:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    .line 1407303
    iget-object v1, p0, Lcom/facebook/widget/popover/SimplePopoverFragment;->m:Landroid/os/Handler;

    iget-object v2, p0, Lcom/facebook/widget/popover/SimplePopoverFragment;->q:Ljava/lang/Runnable;

    invoke-static {v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1407304
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x950af2b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x47c79f17

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1407297
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onResume()V

    .line 1407298
    iget-object v1, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v1, v1

    .line 1407299
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    const/4 v2, 0x0

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 1407300
    const/16 v1, 0x2b

    const v2, -0x250f5358

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/16 v0, 0x2a

    const v1, 0x14f222d

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1407282
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onStart()V

    .line 1407283
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v0

    .line 1407284
    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 1407285
    :goto_0
    if-eqz v0, :cond_0

    .line 1407286
    invoke-virtual {v0, v2}, Landroid/view/Window;->addFlags(I)V

    .line 1407287
    const v2, 0x3f333333    # 0.7f

    invoke-virtual {v0, v2}, Landroid/view/Window;->setDimAmount(F)V

    .line 1407288
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    iput v0, p0, Lcom/facebook/widget/popover/SimplePopoverFragment;->r:I

    .line 1407289
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/widget/popover/SimplePopoverFragment;->n:Z

    if-eqz v0, :cond_1

    .line 1407290
    iget-object v0, p0, Lcom/facebook/widget/popover/SimplePopoverFragment;->p:LX/8ts;

    .line 1407291
    invoke-virtual {v0}, LX/8ts;->getContext()Landroid/content/Context;

    move-result-object v2

    const p0, 0x7f0400f2

    invoke-static {v2, p0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    .line 1407292
    new-instance p0, LX/8tp;

    invoke-direct {p0, v0}, LX/8tp;-><init>(LX/8ts;)V

    invoke-virtual {v2, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1407293
    iget-object p0, v0, LX/8ts;->g:Landroid/view/ViewGroup;

    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1407294
    :cond_1
    const v0, -0x24ed84dd

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 1407295
    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v0

    .line 1407296
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    goto :goto_0
.end method
