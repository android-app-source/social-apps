.class public Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;
.super Lcom/facebook/resources/ui/FbAutoCompleteTextView;
.source ""

# interfaces
.implements LX/3NY;


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private A:Z

.field private B:Z

.field private C:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8v7;",
            ">;"
        }
    .end annotation
.end field

.field private D:LX/0wM;

.field private E:Landroid/graphics/drawable/Drawable;

.field private F:[Landroid/graphics/drawable/Drawable;

.field private G:LX/8uk;

.field private H:LX/0wL;

.field private I:LX/8uv;

.field private J:Landroid/content/res/ColorStateList;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private K:LX/0Uh;

.field private L:LX/8uk;

.field public M:LX/8uw;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:I

.field private final d:Landroid/graphics/Rect;

.field private final e:Landroid/graphics/Rect;

.field public f:LX/8ut;

.field private g:LX/8uy;

.field private h:LX/8us;

.field private i:LX/8ur;

.field private j:I

.field private k:LX/8uu;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:LX/8un;

.field private m:Landroid/view/inputmethod/InputMethodManager;

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/8QK;",
            ">;"
        }
    .end annotation
.end field

.field private o:Z

.field public p:I

.field private q:I

.field private r:I

.field private s:I

.field private t:Ljava/lang/Integer;

.field private u:F

.field private v:I

.field private w:Landroid/graphics/drawable/Drawable;

.field private x:Landroid/graphics/drawable/Drawable;

.field private y:Ljava/lang/String;

.field private z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1416340
    const-class v0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    sput-object v0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1416332
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;-><init>(Landroid/content/Context;)V

    .line 1416333
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->d:Landroid/graphics/Rect;

    .line 1416334
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->e:Landroid/graphics/Rect;

    .line 1416335
    sget-object v0, LX/8ut;->NORMAL:LX/8ut;

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->f:LX/8ut;

    .line 1416336
    sget-object v0, LX/8uy;->STYLIZED:LX/8uy;

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->g:LX/8uy;

    .line 1416337
    sget-object v0, LX/8ur;->NONE:LX/8ur;

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->i:LX/8ur;

    .line 1416338
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1416339
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1416324
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1416325
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->d:Landroid/graphics/Rect;

    .line 1416326
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->e:Landroid/graphics/Rect;

    .line 1416327
    sget-object v0, LX/8ut;->NORMAL:LX/8ut;

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->f:LX/8ut;

    .line 1416328
    sget-object v0, LX/8uy;->STYLIZED:LX/8uy;

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->g:LX/8uy;

    .line 1416329
    sget-object v0, LX/8ur;->NONE:LX/8ur;

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->i:LX/8ur;

    .line 1416330
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1416331
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1416316
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1416317
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->d:Landroid/graphics/Rect;

    .line 1416318
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->e:Landroid/graphics/Rect;

    .line 1416319
    sget-object v0, LX/8ut;->NORMAL:LX/8ut;

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->f:LX/8ut;

    .line 1416320
    sget-object v0, LX/8uy;->STYLIZED:LX/8uy;

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->g:LX/8uy;

    .line 1416321
    sget-object v0, LX/8ur;->NONE:LX/8ur;

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->i:LX/8ur;

    .line 1416322
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1416323
    return-void
.end method

.method private a(LX/8QK;I)V
    .locals 1

    .prologue
    .line 1416310
    iput p2, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->c:I

    .line 1416311
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getMeasuredWidth()I

    move-result v0

    if-nez v0, :cond_0

    .line 1416312
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->n:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1416313
    :goto_0
    return-void

    .line 1416314
    :cond_0
    invoke-direct {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->i()V

    .line 1416315
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->b(LX/8QK;Z)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1416274
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    .line 1416275
    new-instance v0, LX/8un;

    invoke-direct {v0}, LX/8un;-><init>()V

    .line 1416276
    move-object v0, v0

    .line 1416277
    move-object v0, v0

    .line 1416278
    check-cast v0, LX/8un;

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->l:LX/8un;

    .line 1416279
    invoke-static {v1}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->m:Landroid/view/inputmethod/InputMethodManager;

    .line 1416280
    const/16 v0, 0x38af

    invoke-static {v1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->C:LX/0Ot;

    .line 1416281
    invoke-static {v1}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->D:LX/0wM;

    .line 1416282
    invoke-static {v1}, LX/0wL;->a(LX/0QB;)LX/0wL;

    move-result-object v0

    check-cast v0, LX/0wL;

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->H:LX/0wL;

    .line 1416283
    invoke-static {v1}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->K:LX/0Uh;

    .line 1416284
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->n:Ljava/util/List;

    .line 1416285
    invoke-direct {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->l()V

    .line 1416286
    invoke-static {p0}, LX/191;->c(Landroid/widget/TextView;)V

    .line 1416287
    const v0, 0x2000006

    invoke-virtual {p0, v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setImeOptions(I)V

    .line 1416288
    iput v4, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->c:I

    .line 1416289
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->TokenizedAutoCompleteTextView:[I

    invoke-virtual {v0, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1416290
    const/16 v1, 0x0

    sget-object v2, LX/8uy;->STYLIZED:LX/8uy;

    invoke-virtual {v2}, LX/8uy;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 1416291
    invoke-static {}, LX/8uy;->values()[LX/8uy;

    move-result-object v2

    aget-object v1, v2, v1

    iput-object v1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->g:LX/8uy;

    .line 1416292
    const/16 v1, 0x1

    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getCurrentTextColor()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->p:I

    .line 1416293
    const/16 v1, 0x2

    iget v2, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->p:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->q:I

    .line 1416294
    const/16 v1, 0x3

    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0048

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->r:I

    .line 1416295
    const/16 v1, 0x4

    iget v2, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->r:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->s:I

    .line 1416296
    const/16 v1, 0x5

    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getTextSize()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->u:F

    .line 1416297
    const/16 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->w:Landroid/graphics/drawable/Drawable;

    .line 1416298
    const/16 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->x:Landroid/graphics/drawable/Drawable;

    .line 1416299
    const/16 v1, 0x9

    const v2, 0x7f0a0040

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->j:I

    .line 1416300
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->E:Landroid/graphics/drawable/Drawable;

    .line 1416301
    const/16 v1, 0xa

    sget-object v2, LX/8us;->NEVER:LX/8us;

    invoke-virtual {v2}, LX/8us;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 1416302
    invoke-static {}, LX/8us;->values()[LX/8us;

    move-result-object v2

    aget-object v1, v2, v1

    invoke-virtual {p0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setClearButtonMode(LX/8us;)V

    .line 1416303
    const/16 v1, 0xb

    const v2, 0x7f0a0231

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1416304
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->J:Landroid/content/res/ColorStateList;

    .line 1416305
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->t:Ljava/lang/Integer;

    .line 1416306
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1416307
    sget-object v0, LX/8ux;->BLUE:LX/8ux;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setSelectedTokenHighlightColor(LX/8ux;)V

    .line 1416308
    new-instance v0, LX/8v1;

    sget-object v1, Landroid/text/method/TextKeyListener$Capitalize;->NONE:Landroid/text/method/TextKeyListener$Capitalize;

    invoke-direct {v0, p0, v1, v4}, LX/8v1;-><init>(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;Landroid/text/method/TextKeyListener$Capitalize;Z)V

    invoke-virtual {p0, v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 1416309
    return-void
.end method

.method private a(Ljava/lang/CharSequence;)V
    .locals 5

    .prologue
    .line 1416265
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    .line 1416266
    invoke-static {v0}, LX/8un;->a(Ljava/lang/CharSequence;)LX/8um;

    move-result-object v1

    .line 1416267
    iget-object v2, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->f:LX/8ut;

    sget-object v3, LX/8ut;->NO_DROPDOWN:LX/8ut;

    if-ne v2, v3, :cond_0

    .line 1416268
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->clearComposingText()V

    .line 1416269
    :cond_0
    if-eqz v1, :cond_1

    .line 1416270
    iget v2, v1, LX/8um;->a:I

    iget v3, v1, LX/8um;->b:I

    const-string v4, ""

    invoke-static {v0, v2, v3, v4}, Landroid/text/method/QwertyKeyListener;->markAsReplaced(Landroid/text/Spannable;IILjava/lang/String;)V

    .line 1416271
    iget v2, v1, LX/8um;->a:I

    iget v1, v1, LX/8um;->b:I

    invoke-interface {v0, v2, v1, p1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 1416272
    :cond_1
    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-static {v0, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 1416273
    return-void
.end method

.method private a(Z)V
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/4 v11, -0x1

    .line 1416245
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getEditableText()Landroid/text/Editable;

    move-result-object v4

    .line 1416246
    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v0

    const-class v1, LX/8uk;

    invoke-interface {v4, v2, v0, v1}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8uk;

    .line 1416247
    invoke-static {v4}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v5

    .line 1416248
    invoke-static {v4}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v6

    .line 1416249
    if-eqz p1, :cond_1

    array-length v1, v0

    add-int/lit8 v1, v1, -0x2

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_4

    .line 1416250
    aget-object v3, v0, v1

    invoke-interface {v4, v3}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v7

    .line 1416251
    aget-object v3, v0, v1

    invoke-interface {v4, v3}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v8

    .line 1416252
    aget-object v3, v0, v1

    .line 1416253
    iget-object v9, v3, LX/8uk;->f:LX/8QK;

    move-object v9, v9

    .line 1416254
    iget-object v3, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->g:LX/8uy;

    sget-object v10, LX/8uy;->PLAIN_TEXT:LX/8uy;

    if-ne v3, v10, :cond_2

    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v3

    if-ge v8, v3, :cond_2

    const/4 v3, 0x1

    :goto_1
    invoke-direct {p0, v9, v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->b(LX/8QK;Z)Ljava/lang/CharSequence;

    move-result-object v3

    .line 1416255
    if-eq v7, v11, :cond_0

    if-ne v8, v11, :cond_3

    .line 1416256
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Token not found in editable"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v1, v2

    .line 1416257
    goto :goto_0

    :cond_2
    move v3, v2

    .line 1416258
    goto :goto_1

    .line 1416259
    :cond_3
    aget-object v9, v0, v1

    invoke-interface {v4, v9}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    .line 1416260
    invoke-interface {v4, v7, v8, v3}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 1416261
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1416262
    :cond_4
    if-eq v5, v11, :cond_5

    if-eq v6, v11, :cond_5

    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v0

    if-gt v6, v0, :cond_5

    .line 1416263
    invoke-static {v4, v5, v6}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    .line 1416264
    :cond_5
    return-void
.end method

.method private a(LX/8uk;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1416234
    iget-object v2, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->K:LX/0Uh;

    const/16 v3, 0x10c

    invoke-virtual {v2, v3, v1}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_0

    instance-of v2, p1, LX/8v6;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->L:LX/8uk;

    if-ne p1, v2, :cond_1

    :cond_0
    move v0, v1

    .line 1416235
    :goto_0
    return v0

    .line 1416236
    :cond_1
    iget-object v2, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->L:LX/8uk;

    if-eqz v2, :cond_2

    .line 1416237
    invoke-static {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->o(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)V

    .line 1416238
    :cond_2
    iget-object v2, p1, LX/8uk;->f:LX/8QK;

    move-object v2, v2

    .line 1416239
    invoke-virtual {v2}, LX/8QK;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1416240
    iput-object p1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->L:LX/8uk;

    .line 1416241
    invoke-virtual {p0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setCursorVisible(Z)V

    .line 1416242
    iput-boolean v0, v2, LX/8QK;->c:Z

    .line 1416243
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->c()V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1416244
    goto :goto_0
.end method

.method private a(LX/8uk;FF)Z
    .locals 4

    .prologue
    .line 1416059
    const/4 v0, 0x0

    .line 1416060
    iget-object v1, p1, LX/8uk;->f:LX/8QK;

    move-object v1, v1

    .line 1416061
    iget-boolean v2, v1, LX/8QK;->a:Z

    move v1, v2

    .line 1416062
    if-eqz v1, :cond_0

    .line 1416063
    iget-object v1, p1, LX/8uk;->f:LX/8QK;

    move-object v1, v1

    .line 1416064
    invoke-direct {p0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->c(LX/8QK;)Landroid/graphics/Point;

    move-result-object v1

    .line 1416065
    iget-object v2, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->e:Landroid/graphics/Rect;

    invoke-virtual {p1, v2}, LX/8uk;->b(Landroid/graphics/Rect;)V

    .line 1416066
    iget v2, v1, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    sub-float v2, p2, v2

    float-to-int v2, v2

    .line 1416067
    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    sub-float v1, p3, v1

    float-to-int v1, v1

    .line 1416068
    iget-object v3, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->e:Landroid/graphics/Rect;

    invoke-virtual {v3, v2, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1416069
    const/4 v0, 0x1

    .line 1416070
    :cond_0
    return v0
.end method

.method private a(LX/8uk;Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    .line 1416223
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->d:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, LX/8uk;->a(Landroid/graphics/Rect;)V

    .line 1416224
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->d:Landroid/graphics/Rect;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getScrollX()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getScrollY()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    return v0
.end method

.method private a(Landroid/view/MotionEvent;)Z
    .locals 11

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1416183
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1416184
    :cond_0
    :goto_0
    return v1

    .line 1416185
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 1416186
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->b(Landroid/view/MotionEvent;)LX/8uk;

    move-result-object v2

    .line 1416187
    if-eqz v2, :cond_6

    .line 1416188
    iget-object v1, v2, LX/8uk;->f:LX/8QK;

    move-object v1, v1

    .line 1416189
    iput-object v2, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->G:LX/8uk;

    .line 1416190
    invoke-virtual {v1}, LX/8QK;->a()Z

    move-result v2

    .line 1416191
    iput-boolean v2, v1, LX/8QK;->d:Z

    .line 1416192
    :goto_1
    move v1, v0

    .line 1416193
    goto :goto_0

    .line 1416194
    :pswitch_1
    iget-object v2, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->G:LX/8uk;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->G:LX/8uk;

    invoke-direct {p0, v2, p1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8uk;Landroid/view/MotionEvent;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1416195
    iget-object v2, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->G:LX/8uk;

    .line 1416196
    iget-object v3, v2, LX/8uk;->f:LX/8QK;

    move-object v2, v3

    .line 1416197
    iput-boolean v1, v2, LX/8QK;->d:Z

    .line 1416198
    :cond_2
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->G:LX/8uk;

    move v1, v0

    .line 1416199
    goto :goto_0

    :pswitch_2
    move v2, v0

    .line 1416200
    :goto_2
    invoke-static {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->o(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)V

    .line 1416201
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getEditableText()Landroid/text/Editable;

    move-result-object v5

    .line 1416202
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getPickedTokenSpans()[LX/8uk;

    move-result-object v6

    array-length v7, v6

    move v4, v1

    :goto_3
    if-ge v4, v7, :cond_2

    aget-object v3, v6, v4

    .line 1416203
    iget-object v8, v3, LX/8uk;->f:LX/8QK;

    move-object v8, v8

    .line 1416204
    iput-boolean v1, v8, LX/8QK;->d:Z

    .line 1416205
    if-nez v2, :cond_5

    iget-object v9, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->G:LX/8uk;

    if-eqz v9, :cond_5

    iget-object v9, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->G:LX/8uk;

    .line 1416206
    iget-object v10, v9, LX/8uk;->f:LX/8QK;

    move-object v9, v10

    .line 1416207
    invoke-virtual {v8, v9}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1416208
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v10

    invoke-direct {p0, v3, v9, v10}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8uk;FF)Z

    move-result v9

    .line 1416209
    invoke-virtual {v8, v9}, LX/8QK;->a(Z)V

    .line 1416210
    invoke-interface {v5, v3}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v10

    .line 1416211
    const/4 v3, -0x1

    if-eq v10, v3, :cond_3

    .line 1416212
    if-nez v9, :cond_3

    .line 1416213
    iget-boolean v3, v8, LX/8QK;->c:Z

    move v3, v3

    .line 1416214
    if-nez v3, :cond_4

    invoke-virtual {v8}, LX/8QK;->a()Z

    move-result v3

    if-eqz v3, :cond_4

    move v3, v0

    .line 1416215
    :goto_4
    iput-boolean v3, v8, LX/8QK;->c:Z

    .line 1416216
    iget-boolean v3, v8, LX/8QK;->c:Z

    move v3, v3

    .line 1416217
    if-eqz v3, :cond_3

    .line 1416218
    invoke-virtual {p0, v10}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setSelection(I)V

    .line 1416219
    :cond_3
    :goto_5
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_3

    :cond_4
    move v3, v1

    .line 1416220
    goto :goto_4

    .line 1416221
    :cond_5
    iput-boolean v1, v8, LX/8QK;->c:Z

    .line 1416222
    goto :goto_5

    :cond_6
    move v0, v1

    goto :goto_1

    :pswitch_3
    move v2, v1

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private b(Landroid/view/MotionEvent;)LX/8uk;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1416177
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getPickedTokenSpans()[LX/8uk;

    move-result-object v2

    .line 1416178
    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 1416179
    invoke-direct {p0, v0, p1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8uk;Landroid/view/MotionEvent;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1416180
    :goto_1
    return-object v0

    .line 1416181
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1416182
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private b(LX/8QK;)Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    .line 1416164
    iget-object v1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->w:Landroid/graphics/drawable/Drawable;

    .line 1416165
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->g:LX/8uy;

    sget-object v2, LX/8uy;->STYLIZED:LX/8uy;

    if-ne v0, v2, :cond_0

    move-object v0, p1

    check-cast v0, LX/8QL;

    invoke-virtual {v0}, LX/8QL;->m()I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1416166
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    check-cast p1, LX/8QL;

    invoke-virtual {p1}, LX/8QL;->m()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1416167
    :goto_0
    return-object v0

    .line 1416168
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->g:LX/8uy;

    sget-object v2, LX/8uy;->PLAIN_TEXT:LX/8uy;

    if-ne v0, v2, :cond_1

    .line 1416169
    iget-boolean v0, p1, LX/8QK;->c:Z

    move v0, v0

    .line 1416170
    if-eqz v0, :cond_1

    .line 1416171
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->v:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 1416172
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->g:LX/8uy;

    sget-object v2, LX/8uy;->PLAIN_TEXT:LX/8uy;

    if-ne v0, v2, :cond_2

    .line 1416173
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02197d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 1416174
    :cond_2
    iget-boolean v0, p1, LX/8QK;->c:Z

    move v0, v0

    .line 1416175
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->x:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_3

    .line 1416176
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->x:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method private b(LX/8QK;Z)Ljava/lang/CharSequence;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 1416101
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1416102
    new-instance v3, Landroid/text/TextPaint;

    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    .line 1416103
    invoke-direct {p0, p1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->b(LX/8QK;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 1416104
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00e7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 1416105
    invoke-virtual {p1}, LX/8QK;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->g:LX/8uy;

    sget-object v2, LX/8uy;->CHIPS:LX/8uy;

    if-eq v0, v2, :cond_0

    move v0, v1

    move-object v2, v3

    .line 1416106
    :goto_0
    invoke-virtual {v2, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 1416107
    :goto_1
    iget v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->u:F

    invoke-virtual {v3, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 1416108
    invoke-direct {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getDrawablesWidth()I

    move-result v2

    .line 1416109
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->g:LX/8uy;

    sget-object v6, LX/8uy;->CHIPS:LX/8uy;

    if-ne v0, v6, :cond_4

    .line 1416110
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8v7;

    .line 1416111
    new-instance v5, LX/8v5;

    iget-object v1, v0, LX/8v7;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/tiles/UserTileDrawableController;

    iget-object v6, v0, LX/8v7;->b:LX/1FZ;

    invoke-direct {v5, v1, v6}, LX/8v5;-><init>(Lcom/facebook/user/tiles/UserTileDrawableController;LX/1FZ;)V

    move-object v0, v5

    .line 1416112
    check-cast p1, LX/8v8;

    .line 1416113
    iput-object p1, v0, LX/8v5;->c:LX/8v8;

    .line 1416114
    move-object v0, v0

    .line 1416115
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getMeasuredWidth()I

    move-result v1

    iget v5, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->c:I

    sub-int/2addr v1, v5

    sub-int/2addr v1, v2

    .line 1416116
    iput v1, v0, LX/8v5;->f:I

    .line 1416117
    move-object v0, v0

    .line 1416118
    iput-object v3, v0, LX/8v5;->d:Landroid/text/TextPaint;

    .line 1416119
    move-object v0, v0

    .line 1416120
    iget-object v1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->J:Landroid/content/res/ColorStateList;

    .line 1416121
    iput-object v1, v0, LX/8v5;->h:Landroid/content/res/ColorStateList;

    .line 1416122
    move-object v0, v0

    .line 1416123
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1416124
    iput-object v1, v0, LX/8v5;->e:Landroid/content/res/Resources;

    .line 1416125
    move-object v0, v0

    .line 1416126
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8v5;->a(Landroid/content/Context;)LX/8v6;

    move-result-object v0

    .line 1416127
    :goto_2
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1416128
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x21

    invoke-virtual {v1, v0, v8, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1416129
    return-object v1

    .line 1416130
    :cond_0
    iget-boolean v0, p1, LX/8QK;->c:Z

    move v0, v0

    .line 1416131
    if-eqz v0, :cond_2

    .line 1416132
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->g:LX/8uy;

    sget-object v2, LX/8uy;->CHIPS:LX/8uy;

    if-ne v0, v2, :cond_1

    iget v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->s:I

    :goto_3
    invoke-virtual {v3, v0}, Landroid/text/TextPaint;->setColor(I)V

    goto :goto_1

    :cond_1
    iget v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->r:I

    goto :goto_3

    .line 1416133
    :cond_2
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->g:LX/8uy;

    sget-object v2, LX/8uy;->CHIPS:LX/8uy;

    if-ne v0, v2, :cond_3

    iget v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->q:I

    move-object v2, v3

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->p:I

    move-object v2, v3

    goto/16 :goto_0

    .line 1416134
    :cond_4
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->g:LX/8uy;

    sget-object v6, LX/8uy;->PLAIN_TEXT:LX/8uy;

    if-ne v0, v6, :cond_6

    .line 1416135
    iget-boolean v0, p1, LX/8QK;->c:Z

    move v0, v0

    .line 1416136
    if-nez v0, :cond_6

    .line 1416137
    new-instance v6, LX/8uj;

    invoke-direct {v6}, LX/8uj;-><init>()V

    move-object v0, p1

    check-cast v0, LX/8QL;

    .line 1416138
    iput-object v0, v6, LX/8uj;->a:LX/8QL;

    .line 1416139
    move-object v0, v6

    .line 1416140
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getMeasuredWidth()I

    move-result v6

    iget v7, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->c:I

    sub-int/2addr v6, v7

    sub-int v2, v6, v2

    invoke-virtual {v0, v2}, LX/8uj;->a(I)LX/8uj;

    move-result-object v0

    .line 1416141
    iput-object v3, v0, LX/8uj;->b:Landroid/text/TextPaint;

    .line 1416142
    move-object v0, v0

    .line 1416143
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1416144
    iput-object v2, v0, LX/8uj;->c:Landroid/content/res/Resources;

    .line 1416145
    move-object v0, v0

    .line 1416146
    iput-object v5, v0, LX/8uj;->e:Landroid/graphics/drawable/Drawable;

    .line 1416147
    move-object v0, v0

    .line 1416148
    invoke-virtual {v0, v8}, LX/8uj;->b(I)LX/8uj;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/8uj;->a(Z)LX/8uj;

    move-result-object v0

    invoke-virtual {p1}, LX/8QK;->a()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->t:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :cond_5
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 1416149
    iput-object v1, v0, LX/8uj;->h:Ljava/lang/Integer;

    .line 1416150
    move-object v0, v0

    .line 1416151
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8uj;->a(Landroid/content/Context;)LX/8ul;

    move-result-object v0

    goto/16 :goto_2

    .line 1416152
    :cond_6
    new-instance v0, LX/8uj;

    invoke-direct {v0}, LX/8uj;-><init>()V

    check-cast p1, LX/8QL;

    .line 1416153
    iput-object p1, v0, LX/8uj;->a:LX/8QL;

    .line 1416154
    move-object v0, v0

    .line 1416155
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getMeasuredWidth()I

    move-result v1

    iget v6, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->c:I

    sub-int/2addr v1, v6

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, LX/8uj;->a(I)LX/8uj;

    move-result-object v0

    .line 1416156
    iput-object v3, v0, LX/8uj;->b:Landroid/text/TextPaint;

    .line 1416157
    move-object v0, v0

    .line 1416158
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1416159
    iput-object v1, v0, LX/8uj;->c:Landroid/content/res/Resources;

    .line 1416160
    move-object v0, v0

    .line 1416161
    iput-object v5, v0, LX/8uj;->e:Landroid/graphics/drawable/Drawable;

    .line 1416162
    move-object v0, v0

    .line 1416163
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8uj;->a(Landroid/content/Context;)LX/8ul;

    move-result-object v0

    goto/16 :goto_2
.end method

.method private b(I)V
    .locals 0

    .prologue
    .line 1416098
    if-lez p1, :cond_0

    .line 1416099
    invoke-virtual {p0, p1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setMinHeight(I)V

    .line 1416100
    :cond_0
    return-void
.end method

.method private c(LX/8QK;)Landroid/graphics/Point;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1416088
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getPickedTokenSpans()[LX/8uk;

    move-result-object v3

    .line 1416089
    array-length v4, v3

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v1, v3, v2

    .line 1416090
    iget-object v5, v1, LX/8uk;->f:LX/8QK;

    move-object v5, v5

    .line 1416091
    invoke-virtual {v5, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1416092
    :goto_1
    if-nez v1, :cond_1

    .line 1416093
    :goto_2
    return-object v0

    .line 1416094
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1416095
    :cond_1
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 1416096
    invoke-virtual {v1, v2}, LX/8uk;->a(Landroid/graphics/Rect;)V

    .line 1416097
    new-instance v0, Landroid/graphics/Point;

    iget v1, v2, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getScrollX()I

    move-result v3

    sub-int/2addr v1, v3

    iget v2, v2, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getScrollY()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    goto :goto_2

    :cond_2
    move-object v1, v0

    goto :goto_1
.end method

.method private c(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1416083
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->i:LX/8ur;

    sget-object v3, LX/8ur;->NONE:LX/8ur;

    if-eq v2, v3, :cond_0

    invoke-direct {p0, p1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->d(Landroid/view/MotionEvent;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    .line 1416084
    :goto_0
    return v0

    .line 1416085
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_2

    .line 1416086
    invoke-static {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->q(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1416087
    goto :goto_0
.end method

.method private d(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    .line 1416081
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v1, 0x2

    aget-object v0, v0, v1

    .line 1416082
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-static {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getAccessoryButtonHeight(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, v1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1416071
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getEditableText()Landroid/text/Editable;

    move-result-object v3

    .line 1416072
    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 1416073
    :cond_0
    return v1

    .line 1416074
    :cond_1
    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v2

    .line 1416075
    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v0

    const-class v4, LX/8uk;

    invoke-interface {v3, v1, v0, v4}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8uk;

    .line 1416076
    array-length v4, v0

    move v7, v1

    move v1, v2

    move v2, v7

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v5, v0, v2

    .line 1416077
    iget-object v6, v5, LX/8uk;->f:LX/8QK;

    move-object v6, v6

    .line 1416078
    invoke-virtual {v6}, LX/8QK;->a()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-interface {v3, v5}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v6

    if-ge v6, v1, :cond_2

    .line 1416079
    invoke-interface {v3, v5}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v1

    .line 1416080
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static g(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1416474
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getSelectionStart()I

    move-result v0

    .line 1416475
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getEditableText()Landroid/text/Editable;

    move-result-object v3

    .line 1416476
    if-nez v0, :cond_0

    move v0, v1

    .line 1416477
    :goto_0
    return v0

    .line 1416478
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getSelectionEnd()I

    move-result v4

    if-ne v0, v4, :cond_1

    .line 1416479
    invoke-direct {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->f()I

    move-result v3

    .line 1416480
    if-le v0, v3, :cond_3

    .line 1416481
    invoke-virtual {p0, v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setSelection(I)V

    move v0, v2

    .line 1416482
    goto :goto_0

    .line 1416483
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getSelectionEnd()I

    move-result v4

    const-class v5, LX/8uk;

    invoke-interface {v3, v0, v4, v5}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8uk;

    .line 1416484
    array-length v4, v0

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_3

    aget-object v5, v0, v3

    .line 1416485
    iget-object v6, v5, LX/8uk;->f:LX/8QK;

    move-object v5, v6

    .line 1416486
    invoke-virtual {v5}, LX/8QK;->a()Z

    move-result v5

    if-nez v5, :cond_2

    .line 1416487
    invoke-direct {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->f()I

    move-result v0

    .line 1416488
    invoke-virtual {p0, v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setSelection(I)V

    move v0, v2

    .line 1416489
    goto :goto_0

    .line 1416490
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    move v0, v1

    .line 1416491
    goto :goto_0
.end method

.method public static getAccessoryButtonHeight(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)I
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 1416341
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getMinHeight()I

    move-result v0

    if-lez v0, :cond_0

    .line 1416342
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getMinHeight()I

    move-result v0

    .line 1416343
    :goto_0
    return v0

    .line 1416344
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v1, 0x2

    aget-object v0, v0, v1

    .line 1416345
    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    goto :goto_0
.end method

.method private getClearButtonDrawable()Landroid/graphics/drawable/Drawable;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1416469
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->E:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 1416470
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f021979

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->E:Landroid/graphics/drawable/Drawable;

    .line 1416471
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->D:LX/0wM;

    iget-object v1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->E:Landroid/graphics/drawable/Drawable;

    iget v2, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->j:I

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->E:Landroid/graphics/drawable/Drawable;

    .line 1416472
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->E:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->E:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->E:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1416473
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->E:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method private getDrawablesWidth()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x0

    .line 1416463
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1416464
    aget-object v2, v1, v0

    if-eqz v2, :cond_0

    .line 1416465
    aget-object v2, v1, v0

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Rect;->right:I

    aget-object v0, v1, v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->left:I

    sub-int v0, v2, v0

    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getCompoundDrawablePadding()I

    move-result v2

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 1416466
    :cond_0
    aget-object v2, v1, v3

    if-eqz v2, :cond_1

    .line 1416467
    aget-object v2, v1, v3

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Rect;->right:I

    aget-object v1, v1, v3

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int v1, v2, v1

    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getCompoundDrawablePadding()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 1416468
    :cond_1
    return v0
.end method

.method private getInputTypeSwitchButtonDrawables()[Landroid/graphics/drawable/Drawable;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1416453
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->F:[Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 1416454
    invoke-static {}, LX/8uu;->values()[LX/8uu;

    move-result-object v2

    .line 1416455
    array-length v0, v2

    new-array v0, v0, [Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->F:[Landroid/graphics/drawable/Drawable;

    move v0, v1

    .line 1416456
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 1416457
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    aget-object v4, v2, v0

    iget v4, v4, LX/8uu;->drawableResourceId:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 1416458
    iget-object v4, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->D:LX/0wM;

    iget v5, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->j:I

    invoke-virtual {v4, v3, v5}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 1416459
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    invoke-virtual {v3, v1, v1, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1416460
    iget-object v4, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->F:[Landroid/graphics/drawable/Drawable;

    aput-object v3, v4, v0

    .line 1416461
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1416462
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->F:[Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method private h()LX/8uk;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1416445
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getSelectionStart()I

    move-result v2

    .line 1416446
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getEditableText()Landroid/text/Editable;

    move-result-object v3

    .line 1416447
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getPickedTokenSpans()[LX/8uk;

    move-result-object v4

    .line 1416448
    array-length v5, v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_1

    aget-object v0, v4, v1

    .line 1416449
    invoke-interface {v3, v0}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v6

    if-ne v2, v6, :cond_0

    .line 1416450
    :goto_1
    return-object v0

    .line 1416451
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1416452
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private i()V
    .locals 2

    .prologue
    .line 1416439
    iget-boolean v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->B:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->k:LX/8uu;

    if-eqz v0, :cond_1

    .line 1416440
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->k:LX/8uu;

    .line 1416441
    :goto_0
    invoke-direct {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->k()V

    .line 1416442
    :cond_0
    return-void

    .line 1416443
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->B:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->k:LX/8uu;

    sget-object v1, LX/8uu;->DIALPAD:LX/8uu;

    if-eq v0, v1, :cond_0

    .line 1416444
    sget-object v0, LX/8uu;->DIALPAD:LX/8uu;

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->k:LX/8uu;

    goto :goto_0
.end method

.method private j()V
    .locals 3

    .prologue
    .line 1416433
    iget-boolean v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->B:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->k:LX/8uu;

    if-nez v0, :cond_1

    .line 1416434
    :cond_0
    :goto_0
    return-void

    .line 1416435
    :cond_1
    invoke-static {}, LX/8uu;->values()[LX/8uu;

    move-result-object v0

    .line 1416436
    iget-object v1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->k:LX/8uu;

    invoke-virtual {v1}, LX/8uu;->ordinal()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    array-length v2, v0

    rem-int/2addr v1, v2

    .line 1416437
    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->k:LX/8uu;

    .line 1416438
    invoke-direct {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->k()V

    goto :goto_0
.end method

.method private k()V
    .locals 3

    .prologue
    .line 1416420
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->k:LX/8uu;

    if-nez v0, :cond_1

    .line 1416421
    sget-object v0, LX/8uu;->TEXT_NO_SUGGESTIONS:LX/8uu;

    iget v0, v0, LX/8uu;->inputTypeFlags:I

    .line 1416422
    :goto_0
    invoke-direct {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->m()I

    move-result v1

    .line 1416423
    invoke-virtual {p0, v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setInputType(I)V

    .line 1416424
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->requestFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1416425
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->m:Landroid/view/inputmethod/InputMethodManager;

    const/4 v2, 0x1

    invoke-virtual {v0, p0, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 1416426
    :cond_0
    invoke-direct {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->l()V

    .line 1416427
    invoke-direct {p0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->b(I)V

    .line 1416428
    invoke-direct {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->p()V

    .line 1416429
    return-void

    .line 1416430
    :cond_1
    invoke-static {}, LX/8uu;->values()[LX/8uu;

    move-result-object v0

    .line 1416431
    iget-object v1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->k:LX/8uu;

    invoke-virtual {v1}, LX/8uu;->ordinal()I

    move-result v1

    array-length v2, v0

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    array-length v2, v0

    rem-int/2addr v1, v2

    .line 1416432
    aget-object v0, v0, v1

    iget v0, v0, LX/8uu;->inputTypeFlags:I

    goto :goto_0
.end method

.method private l()V
    .locals 1

    .prologue
    .line 1416416
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setSingleLine(Z)V

    .line 1416417
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setMaxLines(I)V

    .line 1416418
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setHorizontallyScrolling(Z)V

    .line 1416419
    return-void
.end method

.method private m()I
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 1416492
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getMinHeight()I

    move-result v0

    if-lez v0, :cond_0

    .line 1416493
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getMinHeight()I

    move-result v0

    .line 1416494
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static n(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1416402
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->g:LX/8uy;

    sget-object v3, LX/8uy;->CHIPS:LX/8uy;

    if-ne v0, v3, :cond_3

    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->K:LX/0Uh;

    const/16 v3, 0x10c

    invoke-virtual {v0, v3, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->L:LX/8uk;

    if-nez v0, :cond_3

    .line 1416403
    invoke-direct {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->h()LX/8uk;

    move-result-object v0

    .line 1416404
    if-eqz v0, :cond_3

    .line 1416405
    invoke-direct {p0, v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8uk;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 1416406
    :goto_0
    if-eqz v0, :cond_2

    .line 1416407
    invoke-direct {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->h()LX/8uk;

    move-result-object v3

    .line 1416408
    if-eqz v3, :cond_2

    iget-object v4, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->M:LX/8uw;

    if-eqz v4, :cond_2

    .line 1416409
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getPickedTokenSpans()[LX/8uk;

    .line 1416410
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->M:LX/8uw;

    .line 1416411
    iget-object v4, v3, LX/8uk;->f:LX/8QK;

    move-object v3, v4

    .line 1416412
    invoke-interface {v0, p0, v3}, LX/8uw;->a(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;LX/8QK;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1416413
    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 1416414
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1416415
    goto :goto_1

    :cond_2
    move v1, v0

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public static o(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1416394
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->L:LX/8uk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->K:LX/0Uh;

    const/16 v1, 0x10c

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1416395
    :cond_0
    :goto_0
    return-void

    .line 1416396
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->L:LX/8uk;

    .line 1416397
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->L:LX/8uk;

    .line 1416398
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setCursorVisible(Z)V

    .line 1416399
    iget-object v1, v0, LX/8uk;->f:LX/8QK;

    move-object v0, v1

    .line 1416400
    iput-boolean v2, v0, LX/8QK;->c:Z

    .line 1416401
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->c()V

    goto :goto_0
.end method

.method private p()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x0

    .line 1416377
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 1416378
    sget-object v0, LX/8ur;->NONE:LX/8ur;

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->i:LX/8ur;

    .line 1416379
    sget-object v0, LX/8uq;->b:[I

    iget-object v2, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->h:LX/8us;

    invoke-virtual {v2}, LX/8us;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    :cond_0
    move-object v0, v1

    .line 1416380
    :goto_0
    iget-object v2, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->i:LX/8ur;

    sget-object v4, LX/8ur;->NONE:LX/8ur;

    if-ne v2, v4, :cond_1

    iget-boolean v2, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->B:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->k:LX/8uu;

    if-eqz v2, :cond_1

    .line 1416381
    invoke-direct {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getInputTypeSwitchButtonDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->k:LX/8uu;

    invoke-virtual {v2}, LX/8uu;->ordinal()I

    move-result v2

    aget-object v0, v0, v2

    .line 1416382
    sget-object v2, LX/8ur;->INPUT_TYPE_SWITCH:LX/8ur;

    iput-object v2, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->i:LX/8ur;

    .line 1416383
    :cond_1
    const/4 v2, 0x2

    aget-object v2, v3, v2

    if-ne v0, v2, :cond_2

    .line 1416384
    :goto_1
    return-void

    .line 1416385
    :pswitch_0
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1416386
    :pswitch_1
    invoke-direct {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getClearButtonDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1416387
    sget-object v2, LX/8ur;->CLEAR:LX/8ur;

    iput-object v2, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->i:LX/8ur;

    goto :goto_0

    .line 1416388
    :cond_2
    if-eqz v0, :cond_3

    .line 1416389
    new-instance v2, LX/8up;

    invoke-direct {v2, p0, v0}, LX/8up;-><init>(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;Landroid/graphics/drawable/Drawable;)V

    .line 1416390
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-virtual {v2, v7, v7, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1416391
    new-instance v1, LX/8uz;

    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getWidth()I

    move-result v4

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    invoke-direct {v0, v4, v7, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->i:LX/8ur;

    invoke-virtual {v5}, LX/8ur;->getAccessibilityText()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, p0, p0, v0, v4}, LX/8uz;-><init>(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;Landroid/graphics/Rect;Ljava/lang/String;)V

    move-object v0, v2

    .line 1416392
    :cond_3
    aget-object v2, v3, v7

    const/4 v4, 0x1

    aget-object v4, v3, v4

    const/4 v5, 0x3

    aget-object v3, v3, v5

    invoke-virtual {p0, v2, v4, v0, v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1416393
    invoke-static {p0, v1}, LX/0vv;->a(Landroid/view/View;LX/0vn;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static q(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)V
    .locals 2

    .prologue
    .line 1416370
    sget-object v0, LX/8uq;->c:[I

    iget-object v1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->i:LX/8ur;

    invoke-virtual {v1}, LX/8ur;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1416371
    :cond_0
    :goto_0
    return-void

    .line 1416372
    :pswitch_0
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getPickedTokenSpans()[LX/8uk;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_2

    const/4 v0, 0x1

    .line 1416373
    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->M:LX/8uw;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->M:LX/8uw;

    invoke-interface {v0}, LX/8uw;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1416374
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1416375
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 1416376
    :pswitch_1
    invoke-direct {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->j()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1416367
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getUserEnteredPlainText()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1416368
    const-string v0, ""

    invoke-direct {p0, v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(Ljava/lang/CharSequence;)V

    .line 1416369
    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 1416365
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->onFilterComplete(I)V

    .line 1416366
    return-void
.end method

.method public final a(LX/3Mr;)V
    .locals 0

    .prologue
    .line 1416364
    return-void
.end method

.method public final a(LX/8QK;)V
    .locals 1

    .prologue
    .line 1416362
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;I)V

    .line 1416363
    return-void
.end method

.method public final a(LX/8QK;Z)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1416348
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getEditableText()Landroid/text/Editable;

    move-result-object v2

    .line 1416349
    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v0

    const-class v3, LX/8uk;

    invoke-interface {v2, v1, v0, v3}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8uk;

    .line 1416350
    array-length v3, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v0, v1

    .line 1416351
    iget-object v5, v4, LX/8uk;->f:LX/8QK;

    move-object v5, v5

    .line 1416352
    invoke-virtual {v5, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1416353
    invoke-interface {v2, v4}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v5

    .line 1416354
    invoke-interface {v2, v4}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v6

    .line 1416355
    invoke-interface {v2, v4}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    .line 1416356
    invoke-virtual {v4}, LX/8uk;->a()V

    .line 1416357
    const-string v4, ""

    invoke-interface {v2, v5, v6, v4}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 1416358
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1416359
    :cond_1
    if-eqz p2, :cond_2

    .line 1416360
    const-string v0, ""

    invoke-direct {p0, v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(Ljava/lang/CharSequence;)V

    .line 1416361
    :cond_2
    return-void
.end method

.method public final b()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1416225
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1416226
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getEditableText()Landroid/text/Editable;

    move-result-object v2

    .line 1416227
    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v0

    const-class v3, LX/8uk;

    invoke-interface {v2, v1, v0, v3}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8uk;

    .line 1416228
    array-length v3, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    .line 1416229
    invoke-interface {v2, v4}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    .line 1416230
    invoke-virtual {v4}, LX/8uk;->a()V

    .line 1416231
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1416232
    :cond_0
    invoke-interface {v2}, Landroid/text/Editable;->clear()V

    .line 1416233
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1416346
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(Z)V

    .line 1416347
    return-void
.end method

.method public final d()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1415940
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->g:LX/8uy;

    sget-object v1, LX/8uy;->PLAIN_TEXT:LX/8uy;

    if-eq v0, v1, :cond_0

    .line 1415941
    :goto_0
    return-void

    .line 1415942
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    .line 1415943
    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v1

    const-class v2, LX/8uk;

    invoke-interface {v0, v3, v1, v2}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8uk;

    .line 1415944
    array-length v0, v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 1415945
    invoke-direct {p0, v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(Z)V

    goto :goto_0

    .line 1415946
    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(Z)V

    goto :goto_0
.end method

.method public final e()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1415974
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getPickedTokenSpans()[LX/8uk;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 1415975
    iget-object v5, v4, LX/8uk;->f:LX/8QK;

    move-object v4, v5

    .line 1415976
    iput-boolean v1, v4, LX/8QK;->c:Z

    .line 1415977
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1415978
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->c()V

    .line 1415979
    return-void
.end method

.method public final enoughToFilter()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1415980
    iget-object v1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->f:LX/8ut;

    sget-object v2, LX/8ut;->NO_DROPDOWN:LX/8ut;

    if-ne v1, v2, :cond_1

    .line 1415981
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getUserEnteredPlainText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getThreshold()I

    move-result v2

    if-lt v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getBottomFadingEdgeStrength()F
    .locals 3

    .prologue
    .line 1415971
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getVerticalFadingEdgeLength()I

    move-result v0

    int-to-float v0, v0

    .line 1415972
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->computeVerticalScrollRange()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->computeVerticalScrollExtent()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->computeVerticalScrollOffset()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    .line 1415973
    div-float v0, v1, v0

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v0, v1, v2}, LX/0yq;->b(FFF)F

    move-result v0

    return v0
.end method

.method public getIsImmSuggestionClicked()Z
    .locals 1

    .prologue
    .line 1415970
    iget-boolean v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->o:Z

    return v0
.end method

.method public getPickedTokenSpans()[LX/8uk;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "LX/8uk;",
            ">()[TT;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1415966
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    .line 1415967
    iget-object v1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->g:LX/8uy;

    sget-object v2, LX/8uy;->CHIPS:LX/8uy;

    if-ne v1, v2, :cond_0

    .line 1415968
    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v1

    const-class v2, LX/8v6;

    invoke-interface {v0, v3, v1, v2}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8uk;

    check-cast v0, [LX/8uk;

    .line 1415969
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v1

    const-class v2, LX/8ul;

    invoke-interface {v0, v3, v1, v2}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8uk;

    check-cast v0, [LX/8uk;

    goto :goto_0
.end method

.method public getPickedTokens()LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/8QK;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1415957
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1415958
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getPickedTokenSpans()[LX/8uk;

    move-result-object v2

    .line 1415959
    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 1415960
    iget-object v5, v4, LX/8uk;->f:LX/8QK;

    move-object v4, v5

    .line 1415961
    invoke-virtual {v1, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1415962
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1415963
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QK;

    .line 1415964
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1415965
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public getTextMode()LX/8uy;
    .locals 1

    .prologue
    .line 1415956
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->g:LX/8uy;

    return-object v0
.end method

.method public getTopFadingEdgeStrength()F
    .locals 3

    .prologue
    .line 1415953
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getVerticalFadingEdgeLength()I

    move-result v0

    int-to-float v0, v0

    .line 1415954
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->computeVerticalScrollOffset()I

    move-result v1

    int-to-float v1, v1

    .line 1415955
    div-float v0, v1, v0

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v0, v1, v2}, LX/0yq;->b(FFF)F

    move-result v0

    return v0
.end method

.method public getUserEnteredPlainText()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 1415951
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 1415952
    invoke-static {v0}, LX/8un;->b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final onCommitCompletion(Landroid/view/inputmethod/CompletionInfo;)V
    .locals 1

    .prologue
    .line 1415947
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->o:Z

    .line 1415948
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->onCommitCompletion(Landroid/view/inputmethod/CompletionInfo;)V

    .line 1415949
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->o:Z

    .line 1415950
    return-void
.end method

.method public final onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 3

    .prologue
    .line 1415890
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v1

    .line 1415891
    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/8v0;

    const/4 v2, 0x1

    invoke-direct {v0, p0, v1, v2}, LX/8v0;-><init>(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;Landroid/view/inputmethod/InputConnection;Z)V

    goto :goto_0
.end method

.method public final onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2

    .prologue
    .line 1415907
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->y:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1415908
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->y:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1415909
    :goto_0
    return-void

    .line 1415910
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_0
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1415911
    const/16 v1, 0x42

    if-ne p1, v1, :cond_1

    .line 1415912
    :cond_0
    :goto_0
    return v0

    .line 1415913
    :cond_1
    const/16 v1, 0x43

    if-ne p1, v1, :cond_3

    .line 1415914
    invoke-static {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->g(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->n(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1415915
    :cond_3
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1415916
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    iget-boolean v1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->z:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x4

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->m:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1415917
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 1415918
    const/16 v0, 0x42

    if-ne p1, v0, :cond_0

    .line 1415919
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getUserEnteredPlainText()Ljava/lang/CharSequence;

    .line 1415920
    const/4 v0, 0x1

    .line 1415921
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 1415922
    check-cast p1, Landroid/os/Bundle;

    .line 1415923
    const-string v0, "text_view_state_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    .line 1415924
    const-string v1, "text_mode_key"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/8uy;->valueOf(Ljava/lang/String;)LX/8uy;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->g:LX/8uy;

    .line 1415925
    invoke-super {p0, v0}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1415926
    return-void
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 1415927
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1415928
    const-string v1, "text_view_state_key"

    invoke-super {p0}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1415929
    const-string v1, "text_mode_key"

    iget-object v2, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->g:LX/8uy;

    invoke-virtual {v2}, LX/8uy;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1415930
    return-object v0
.end method

.method public final onScrollChanged(IIII)V
    .locals 2

    .prologue
    .line 1415931
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->onScrollChanged(IIII)V

    .line 1415932
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->G:LX/8uk;

    if-eqz v0, :cond_0

    .line 1415933
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->G:LX/8uk;

    .line 1415934
    iget-object v1, v0, LX/8uk;->f:LX/8QK;

    move-object v0, v1

    .line 1415935
    const/4 v1, 0x0

    .line 1415936
    iput-boolean v1, v0, LX/8QK;->d:Z

    .line 1415937
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->G:LX/8uk;

    .line 1415938
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->c()V

    .line 1415939
    :cond_0
    return-void
.end method

.method public final onSizeChanged(IIII)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2c

    const v2, 0x349bd321

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1415892
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->onSizeChanged(IIII)V

    .line 1415893
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getUserEnteredPlainText()Ljava/lang/CharSequence;

    move-result-object v3

    .line 1415894
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->c()V

    .line 1415895
    const/4 v0, 0x0

    .line 1415896
    iget-object v1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->n:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1415897
    const/4 v1, 0x1

    .line 1415898
    new-instance v0, Ljava/util/LinkedList;

    iget-object v4, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->n:Ljava/util/List;

    invoke-direct {v0, v4}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 1415899
    iget-object v4, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->n:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 1415900
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QK;

    .line 1415901
    invoke-virtual {p0, v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;)V

    .line 1415902
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->d()V

    goto :goto_0

    :cond_0
    move v0, v1

    .line 1415903
    :cond_1
    invoke-direct {p0, v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(Ljava/lang/CharSequence;)V

    .line 1415904
    if-eqz v0, :cond_2

    .line 1415905
    new-instance v0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView$1;

    invoke-direct {v0, p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView$1;-><init>(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->post(Ljava/lang/Runnable;)Z

    .line 1415906
    :cond_2
    const v0, -0x4abeff45

    invoke-static {v0, v2}, LX/02F;->g(II)V

    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1416009
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 1416010
    iget-object v2, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->h:LX/8us;

    sget-object v3, LX/8us;->WHILE_EDITING:LX/8us;

    if-ne v2, v3, :cond_0

    if-lez p3, :cond_2

    move v2, v0

    :goto_0
    if-lez p4, :cond_3

    :goto_1
    xor-int/2addr v0, v2

    if-eqz v0, :cond_0

    .line 1416011
    invoke-direct {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->p()V

    .line 1416012
    :cond_0
    if-eq p3, p4, :cond_1

    .line 1416013
    invoke-static {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->o(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)V

    .line 1416014
    :cond_1
    return-void

    :cond_2
    move v2, v1

    .line 1416015
    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x2

    const v1, 0x4fe6247b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1416051
    invoke-direct {p0, p1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->c(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1416052
    const v2, 0x19b1f49f

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1416053
    :goto_0
    return v0

    .line 1416054
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 1416055
    invoke-direct {p0, p1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(Landroid/view/MotionEvent;)Z

    move-result v2

    .line 1416056
    if-eqz v2, :cond_1

    .line 1416057
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->c()V

    .line 1416058
    :cond_1
    const v2, 0x6036a909

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public final performFiltering(Ljava/lang/CharSequence;I)V
    .locals 2

    .prologue
    .line 1416046
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 1416047
    invoke-static {v0}, LX/8un;->b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 1416048
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, LX/3LJ;

    .line 1416049
    invoke-interface {v0}, LX/3LJ;->a()LX/333;

    move-result-object v0

    invoke-interface {v0, v1, p0}, LX/333;->a(Ljava/lang/CharSequence;LX/3NY;)V

    .line 1416050
    return-void
.end method

.method public final replaceText(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 1416044
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->clearComposingText()V

    .line 1416045
    return-void
.end method

.method public setChipBackgroundColor(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 1416041
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->J:Landroid/content/res/ColorStateList;

    .line 1416042
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->c()V

    .line 1416043
    return-void
.end method

.method public setChipBackgroundColor(Landroid/content/res/ColorStateList;)V
    .locals 0

    .prologue
    .line 1416038
    iput-object p1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->J:Landroid/content/res/ColorStateList;

    .line 1416039
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->c()V

    .line 1416040
    return-void
.end method

.method public setClearButtonDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 1416035
    iput-object p1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->E:Landroid/graphics/drawable/Drawable;

    .line 1416036
    invoke-direct {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->p()V

    .line 1416037
    return-void
.end method

.method public setClearButtonMode(LX/8us;)V
    .locals 1

    .prologue
    .line 1416030
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1416031
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->h:LX/8us;

    if-ne v0, p1, :cond_0

    .line 1416032
    :goto_0
    return-void

    .line 1416033
    :cond_0
    iput-object p1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->h:LX/8us;

    .line 1416034
    invoke-direct {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->p()V

    goto :goto_0
.end method

.method public setDropdownMode(LX/8ut;)V
    .locals 0

    .prologue
    .line 1416028
    iput-object p1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->f:LX/8ut;

    .line 1416029
    return-void
.end method

.method public setEnabled(Z)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1416016
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->setEnabled(Z)V

    .line 1416017
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->E:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1416018
    iget-object v2, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->E:Landroid/graphics/drawable/Drawable;

    if-eqz p1, :cond_1

    const/16 v0, 0xff

    :goto_0
    invoke-virtual {v2, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1416019
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getPickedTokenSpans()[LX/8uk;

    move-result-object v3

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v0, v3, v2

    .line 1416020
    iget-object v5, v0, LX/8uk;->f:LX/8QK;

    move-object v5, v5

    .line 1416021
    if-nez p1, :cond_2

    const/4 v0, 0x1

    .line 1416022
    :goto_2
    iput-boolean v0, v5, LX/8QK;->e:Z

    .line 1416023
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1416024
    :cond_1
    const/16 v0, 0x80

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1416025
    goto :goto_2

    .line 1416026
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->c()V

    .line 1416027
    return-void
.end method

.method public setHideSoftKeyboardOnBackButton(Z)V
    .locals 0

    .prologue
    .line 1415982
    iput-boolean p1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->z:Z

    .line 1415983
    return-void
.end method

.method public setInputDoneOnEnterKeyUse(Z)V
    .locals 0

    .prologue
    .line 1416007
    iput-boolean p1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->A:Z

    .line 1416008
    return-void
.end method

.method public setOnInputDoneListener(LX/8uv;)V
    .locals 0

    .prologue
    .line 1416005
    iput-object p1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->I:LX/8uv;

    .line 1416006
    return-void
.end method

.method public setOnTokensChangedListener(LX/8uw;)V
    .locals 0

    .prologue
    .line 1416003
    iput-object p1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->M:LX/8uw;

    .line 1416004
    return-void
.end method

.method public setSelectedTokenBackgroundDrawable(I)V
    .locals 1

    .prologue
    .line 1416001
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->x:Landroid/graphics/drawable/Drawable;

    .line 1416002
    return-void
.end method

.method public setSelectedTokenHighlightColor(LX/8ux;)V
    .locals 2

    .prologue
    .line 1415997
    sget-object v0, LX/8uq;->a:[I

    invoke-virtual {p1}, LX/8ux;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1415998
    :goto_0
    return-void

    .line 1415999
    :pswitch_0
    const v0, 0x7f02197b

    iput v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->v:I

    goto :goto_0

    .line 1416000
    :pswitch_1
    const v0, 0x7f02197c

    iput v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->v:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setSelectedTokenTextColor(I)V
    .locals 0

    .prologue
    .line 1415995
    iput p1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->r:I

    .line 1415996
    return-void
.end method

.method public setShowInputTypeSwitchButton(Z)V
    .locals 1

    .prologue
    .line 1415991
    iget-boolean v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->B:Z

    if-ne v0, p1, :cond_0

    .line 1415992
    :goto_0
    return-void

    .line 1415993
    :cond_0
    iput-boolean p1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->B:Z

    .line 1415994
    invoke-direct {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->i()V

    goto :goto_0
.end method

.method public setTextMode(LX/8uy;)V
    .locals 0

    .prologue
    .line 1415988
    iput-object p1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->g:LX/8uy;

    .line 1415989
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->c()V

    .line 1415990
    return-void
.end method

.method public setTokenIconColor(I)V
    .locals 1

    .prologue
    .line 1415986
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->t:Ljava/lang/Integer;

    .line 1415987
    return-void
.end method

.method public setTokenTextColor(I)V
    .locals 0

    .prologue
    .line 1415984
    iput p1, p0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->p:I

    .line 1415985
    return-void
.end method
