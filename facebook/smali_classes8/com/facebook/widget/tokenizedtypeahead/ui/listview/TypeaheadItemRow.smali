.class public Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:Landroid/widget/TextView;

.field public final c:Lcom/facebook/fbui/glyph/GlyphView;

.field public final d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final e:Landroid/widget/ToggleButton;

.field public final f:Landroid/widget/ImageView;

.field public final g:Landroid/widget/ImageView;

.field public final h:Landroid/view/View;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1416987
    const-class v0, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1416988
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 1416989
    const v0, 0x7f03152c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1416990
    const v0, 0x7f0d0beb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->b:Landroid/widget/TextView;

    .line 1416991
    const v0, 0x7f0d0281

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->c:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1416992
    const v0, 0x7f0d0e17

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1416993
    const v0, 0x7f0d0e16

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->e:Landroid/widget/ToggleButton;

    .line 1416994
    const v0, 0x7f0d1b2b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->f:Landroid/widget/ImageView;

    .line 1416995
    const v0, 0x7f0d0602

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->g:Landroid/widget/ImageView;

    .line 1416996
    const v0, 0x7f0d1812

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->h:Landroid/view/View;

    .line 1416997
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1416998
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 1416999
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const v2, 0x7f01028e

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v1, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 1417000
    const v0, 0x7f020241

    invoke-virtual {p0, v0}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->setCheckboxBackground(I)V

    .line 1417001
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->e:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1417002
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b01b7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b01b9

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b01b8

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b01b9

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1417003
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b01b7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-static {v0, v2}, LX/1ck;->a(Landroid/view/ViewGroup$MarginLayoutParams;I)V

    .line 1417004
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b01b8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-static {v0, v2}, LX/1ck;->b(Landroid/view/ViewGroup$MarginLayoutParams;I)V

    .line 1417005
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b01bb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1417006
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b01bb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 1417007
    iget-object v2, p0, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->e:Landroid/widget/ToggleButton;

    invoke-virtual {v2, v0}, Landroid/widget/ToggleButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1417008
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b01b4

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1417009
    iget-object v2, p0, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, v0, v0, v0, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setPadding(IIII)V

    .line 1417010
    iget-object v2, p0, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->c:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v2, v0, v0, v0, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setPadding(IIII)V

    .line 1417011
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b01b5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v7, v7, v2, v7}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1417012
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0050

    invoke-static {v2, v3}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 1417013
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->b:Landroid/widget/TextView;

    iget v1, v1, Landroid/util/TypedValue;->data:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1417014
    const v0, 0x7f0d07fd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1417015
    const v0, 0x7f0d0c4d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1417016
    return-object p0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1417017
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->g:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1417018
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b01b6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {v0, v1}, LX/1ck;->b(Landroid/view/ViewGroup$MarginLayoutParams;I)V

    .line 1417019
    return-void
.end method

.method public setCheckboxBackground(I)V
    .locals 1

    .prologue
    .line 1417020
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->e:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p1}, Landroid/widget/ToggleButton;->setBackgroundResource(I)V

    .line 1417021
    return-void
.end method
