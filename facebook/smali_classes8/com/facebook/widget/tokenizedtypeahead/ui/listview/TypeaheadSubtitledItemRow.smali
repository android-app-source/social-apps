.class public Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:Landroid/widget/TextView;

.field public final c:Landroid/widget/TextView;

.field public final d:Lcom/facebook/fbui/glyph/GlyphView;

.field public final e:Landroid/widget/ToggleButton;

.field public final f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1417024
    const-class v0, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1417025
    const v0, 0x7f03152e

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;-><init>(Landroid/content/Context;I)V

    .line 1417026
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param

    .prologue
    .line 1417027
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 1417028
    invoke-virtual {p0, p2}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1417029
    const v0, 0x7f0d0e18

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;->b:Landroid/widget/TextView;

    .line 1417030
    const v0, 0x7f0d2fc4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;->d:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1417031
    const v0, 0x7f0d0e16

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;->e:Landroid/widget/ToggleButton;

    .line 1417032
    const v0, 0x7f0d0e19

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;->c:Landroid/widget/TextView;

    .line 1417033
    const v0, 0x7f0d0e17

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1417034
    return-void
.end method
