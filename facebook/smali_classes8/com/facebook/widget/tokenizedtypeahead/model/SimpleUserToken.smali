.class public Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;
.super LX/8QL;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/8QL",
        "<",
        "Lcom/facebook/user/model/UserKey;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final e:Lcom/facebook/user/model/Name;

.field private final f:Ljava/lang/String;

.field public final g:Lcom/facebook/user/model/UserKey;

.field private h:Z

.field public i:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1416805
    new-instance v0, LX/8vD;

    invoke-direct {v0}, LX/8vD;-><init>()V

    sput-object v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    .line 1416806
    const-class v0, Lcom/facebook/user/model/Name;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/Name;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    const-class v0, Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/user/model/UserKey;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    const-string v4, "true"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    const-string v5, "true"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;-><init>(Lcom/facebook/user/model/Name;Ljava/lang/String;Lcom/facebook/user/model/UserKey;ZZ)V

    .line 1416807
    return-void
.end method

.method public constructor <init>(Lcom/facebook/user/model/Name;Ljava/lang/String;Lcom/facebook/user/model/UserKey;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1416808
    sget-object v0, LX/8vA;->USER:LX/8vA;

    invoke-direct {p0, v0}, LX/8QL;-><init>(LX/8vA;)V

    .line 1416809
    iput-boolean v1, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->i:Z

    .line 1416810
    iput-object p1, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->e:Lcom/facebook/user/model/Name;

    .line 1416811
    iput-object p2, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->f:Ljava/lang/String;

    .line 1416812
    iput-object p3, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    .line 1416813
    iput-boolean v1, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->h:Z

    .line 1416814
    return-void
.end method

.method public constructor <init>(Lcom/facebook/user/model/Name;Ljava/lang/String;Lcom/facebook/user/model/UserKey;ZZ)V
    .locals 1

    .prologue
    .line 1416815
    sget-object v0, LX/8vA;->USER:LX/8vA;

    invoke-direct {p0, v0}, LX/8QL;-><init>(LX/8vA;)V

    .line 1416816
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->i:Z

    .line 1416817
    iput-object p1, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->e:Lcom/facebook/user/model/Name;

    .line 1416818
    iput-object p2, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->f:Ljava/lang/String;

    .line 1416819
    iput-object p3, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    .line 1416820
    iput-boolean p4, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->i:Z

    .line 1416821
    iput-boolean p5, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->h:Z

    .line 1416822
    return-void
.end method

.method public constructor <init>(Lcom/facebook/user/model/User;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1416823
    sget-object v0, LX/8vA;->USER:LX/8vA;

    invoke-direct {p0, v0}, LX/8QL;-><init>(LX/8vA;)V

    .line 1416824
    iput-boolean v1, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->i:Z

    .line 1416825
    iget-object v0, p1, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 1416826
    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->e:Lcom/facebook/user/model/Name;

    .line 1416827
    invoke-virtual {p1}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->f:Ljava/lang/String;

    .line 1416828
    iget-object v0, p1, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v0, v0

    .line 1416829
    iput-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    .line 1416830
    iput-boolean v1, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->h:Z

    .line 1416831
    return-void
.end method

.method public constructor <init>(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;)V
    .locals 3

    .prologue
    .line 1416833
    iget-object v0, p1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->e:Lcom/facebook/user/model/Name;

    iget-object v1, p1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->f:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;-><init>(Lcom/facebook/user/model/Name;Ljava/lang/String;Lcom/facebook/user/model/UserKey;)V

    .line 1416834
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 1416832
    iget-boolean v0, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->i:Z

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1416837
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->e:Lcom/facebook/user/model/Name;

    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1416835
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    move-object v0, v0

    .line 1416836
    return-object v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 1416804
    const/4 v0, -0x1

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 1416784
    const/4 v0, 0x0

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 1416803
    const/4 v0, -0x1

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1416785
    if-ne p1, p0, :cond_0

    .line 1416786
    const/4 v0, 0x1

    .line 1416787
    :goto_0
    return v0

    .line 1416788
    :cond_0
    instance-of v0, p1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    if-nez v0, :cond_1

    .line 1416789
    const/4 v0, 0x0

    goto :goto_0

    .line 1416790
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    check-cast p1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    iget-object v1, p1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0, v1}, Lcom/facebook/user/model/UserKey;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1416791
    const/4 v0, -0x1

    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 1416792
    const/4 v0, -0x1

    return v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1416793
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1416794
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 1416795
    iget-boolean v0, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->h:Z

    return v0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1416796
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1416797
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->e:Lcom/facebook/user/model/Name;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 1416798
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1416799
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 1416800
    iget-boolean v0, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->i:Z

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1416801
    iget-boolean v0, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->h:Z

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1416802
    return-void
.end method
