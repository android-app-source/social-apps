.class public Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;
.super LX/8QL;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/8QL",
        "<",
        "Ljava/lang/String;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public e:Ljava/lang/String;

.field private final f:I

.field private final g:I

.field private final h:I

.field private i:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1416780
    new-instance v0, LX/8vC;

    invoke-direct {v0}, LX/8vC;-><init>()V

    sput-object v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    .line 1416778
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    .line 1416779
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IIILjava/lang/String;)V
    .locals 1

    .prologue
    .line 1416771
    sget-object v0, LX/8vA;->FRIENDLIST:LX/8vA;

    invoke-direct {p0, v0}, LX/8QL;-><init>(LX/8vA;)V

    .line 1416772
    iput-object p1, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;->e:Ljava/lang/String;

    .line 1416773
    iput p2, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;->f:I

    .line 1416774
    iput p3, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;->g:I

    .line 1416775
    iput p4, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;->h:I

    .line 1416776
    iput-object p5, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;->i:Ljava/lang/String;

    .line 1416777
    return-void
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1416770
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1416768
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1416769
    return-object v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 1416767
    iget v0, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;->f:I

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1416766
    const/4 v0, 0x0

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 1416765
    iget v0, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;->g:I

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1416757
    if-ne p1, p0, :cond_0

    .line 1416758
    const/4 v0, 0x1

    .line 1416759
    :goto_0
    return v0

    .line 1416760
    :cond_0
    instance-of v0, p1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;

    if-nez v0, :cond_1

    .line 1416761
    const/4 v0, 0x0

    goto :goto_0

    .line 1416762
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;->e:Ljava/lang/String;

    check-cast p1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;

    .line 1416763
    iget-object v1, p1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;->e:Ljava/lang/String;

    move-object v1, v1

    .line 1416764
    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1416756
    iget v0, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;->h:I

    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 1416747
    iget v0, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;->g:I

    return v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1416755
    const/4 v0, 0x0

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1416754
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1416748
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1416749
    iget v0, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1416750
    iget v0, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1416751
    iget v0, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1416752
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1416753
    return-void
.end method
