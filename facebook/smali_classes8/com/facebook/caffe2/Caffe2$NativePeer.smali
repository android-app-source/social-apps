.class public final Lcom/facebook/caffe2/Caffe2$NativePeer;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1665990
    const-string v0, "caffe2"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>([B[B)V
    .locals 1

    .prologue
    .line 1665987
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1665988
    invoke-static {p1, p2}, Lcom/facebook/caffe2/Caffe2$NativePeer;->initHybrid([B[B)Lcom/facebook/jni/HybridData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/caffe2/Caffe2$NativePeer;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 1665989
    return-void
.end method

.method private static native initHybrid([B[B)Lcom/facebook/jni/HybridData;
.end method


# virtual methods
.method public native copyOutput(Ljava/nio/FloatBuffer;)V
.end method

.method public native copyOutputBitmap(Landroid/graphics/Bitmap;)V
.end method

.method public native getOutputBytes()Ljava/nio/ByteBuffer;
.end method

.method public native putYuvImage(Ljava/nio/ByteBuffer;IILjava/nio/ByteBuffer;Ljava/nio/ByteBuffer;IIII)V
.end method

.method public native run([ILjava/nio/FloatBuffer;)[I
.end method

.method public native runBitmap(Landroid/graphics/Bitmap;)[I
.end method

.method public native runYuvImage(II)[I
.end method
