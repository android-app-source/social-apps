.class public final Lcom/facebook/checkin/socialsearch/nux/SocialSearchFeedStoryNuxManager$FeedStoryNuxRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/8xm;

.field public b:Landroid/view/View;

.field public c:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field

.field public d:Z


# direct methods
.method public constructor <init>(LX/8xm;Landroid/view/View;I)V
    .locals 0
    .param p3    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 1424694
    iput-object p1, p0, Lcom/facebook/checkin/socialsearch/nux/SocialSearchFeedStoryNuxManager$FeedStoryNuxRunnable;->a:LX/8xm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1424695
    iput-object p2, p0, Lcom/facebook/checkin/socialsearch/nux/SocialSearchFeedStoryNuxManager$FeedStoryNuxRunnable;->b:Landroid/view/View;

    .line 1424696
    iput p3, p0, Lcom/facebook/checkin/socialsearch/nux/SocialSearchFeedStoryNuxManager$FeedStoryNuxRunnable;->c:I

    .line 1424697
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 1424698
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1424699
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/nux/SocialSearchFeedStoryNuxManager$FeedStoryNuxRunnable;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1424700
    :cond_0
    :goto_0
    return-void

    .line 1424701
    :cond_1
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/nux/SocialSearchFeedStoryNuxManager$FeedStoryNuxRunnable;->b:Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/nux/SocialSearchFeedStoryNuxManager$FeedStoryNuxRunnable;->a:LX/8xm;

    iget-object v1, v1, LX/8xm;->e:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1424702
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/nux/SocialSearchFeedStoryNuxManager$FeedStoryNuxRunnable;->a:LX/8xm;

    iget-object v0, v0, LX/8xm;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/nux/SocialSearchFeedStoryNuxManager$FeedStoryNuxRunnable;->a:LX/8xm;

    iget-wide v2, v2, LX/8xm;->g:J

    sub-long/2addr v0, v2

    long-to-int v1, v0

    .line 1424703
    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    .line 1424704
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/nux/SocialSearchFeedStoryNuxManager$FeedStoryNuxRunnable;->a:LX/8xm;

    iget-object v0, v0, LX/8xm;->a:Landroid/content/Context;

    const-string v3, "window"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 1424705
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 1424706
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/nux/SocialSearchFeedStoryNuxManager$FeedStoryNuxRunnable;->a:LX/8xm;

    .line 1424707
    iget-object v3, v0, LX/8xm;->e:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget v4, v0, LX/8xm;->h:I

    sub-int v4, v3, v4

    .line 1424708
    iget-object v3, v0, LX/8xm;->e:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iget v5, v0, LX/8xm;->i:I

    sub-int/2addr v3, v5

    .line 1424709
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v5

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v6

    if-le v5, v6, :cond_6

    .line 1424710
    :goto_1
    mul-int/lit16 v3, v3, 0x3e8

    div-int/2addr v3, v1

    .line 1424711
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    iput v3, v0, LX/8xm;->j:I

    .line 1424712
    const/4 v0, 0x2

    if-lt v1, v0, :cond_3

    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/nux/SocialSearchFeedStoryNuxManager$FeedStoryNuxRunnable;->a:LX/8xm;

    iget-object v0, v0, LX/8xm;->e:Landroid/graphics/Rect;

    iget v1, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 1424713
    int-to-double v4, v1

    const-wide v6, 0x3fa999999999999aL    # 0.05

    mul-double/2addr v4, v6

    .line 1424714
    iget v6, v0, Landroid/graphics/Rect;->top:I

    int-to-double v6, v6

    cmpg-double v6, v6, v4

    if-ltz v6, :cond_2

    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    double-to-int v4, v4

    sub-int v4, v1, v4

    if-le v6, v4, :cond_7

    :cond_2
    const/4 v4, 0x1

    :goto_2
    move v0, v4

    .line 1424715
    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/nux/SocialSearchFeedStoryNuxManager$FeedStoryNuxRunnable;->a:LX/8xm;

    .line 1424716
    iget v1, v0, LX/8xm;->j:I

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    iget-object v2, v0, LX/8xm;->c:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->a()I

    move-result v2

    div-int/2addr v1, v2

    .line 1424717
    if-lez v1, :cond_8

    const/4 v1, 0x1

    :goto_3
    move v0, v1

    .line 1424718
    if-eqz v0, :cond_5

    .line 1424719
    :cond_3
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/nux/SocialSearchFeedStoryNuxManager$FeedStoryNuxRunnable;->a:LX/8xm;

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/nux/SocialSearchFeedStoryNuxManager$FeedStoryNuxRunnable;->a:LX/8xm;

    iget-object v1, v1, LX/8xm;->b:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, LX/8xm;->a$redex0(LX/8xm;J)V

    .line 1424720
    iget-boolean v0, p0, Lcom/facebook/checkin/socialsearch/nux/SocialSearchFeedStoryNuxManager$FeedStoryNuxRunnable;->d:Z

    if-nez v0, :cond_4

    .line 1424721
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/nux/SocialSearchFeedStoryNuxManager$FeedStoryNuxRunnable;->a:LX/8xm;

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/nux/SocialSearchFeedStoryNuxManager$FeedStoryNuxRunnable;->b:Landroid/view/View;

    iget v2, p0, Lcom/facebook/checkin/socialsearch/nux/SocialSearchFeedStoryNuxManager$FeedStoryNuxRunnable;->c:I

    invoke-virtual {v0, v1, v2}, LX/8xm;->a(Landroid/view/View;I)V

    .line 1424722
    :cond_4
    goto/16 :goto_0

    .line 1424723
    :cond_5
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/nux/SocialSearchFeedStoryNuxManager$FeedStoryNuxRunnable;->a:LX/8xm;

    iget-object v0, v0, LX/8xm;->d:LX/0iA;

    sget-object v1, LX/8xo;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v2, LX/8xl;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    .line 1424724
    if-eqz v0, :cond_0

    .line 1424725
    check-cast v0, LX/8xl;

    .line 1424726
    iget-boolean v1, v0, LX/8xl;->c:Z

    move v1, v1

    .line 1424727
    if-nez v1, :cond_0

    .line 1424728
    iget v1, p0, Lcom/facebook/checkin/socialsearch/nux/SocialSearchFeedStoryNuxManager$FeedStoryNuxRunnable;->c:I

    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/nux/SocialSearchFeedStoryNuxManager$FeedStoryNuxRunnable;->b:Landroid/view/View;

    invoke-static {v1, v2}, LX/8xo;->a(ILandroid/view/View;)V

    .line 1424729
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/8xl;->c:Z

    .line 1424730
    iget-object v1, v0, LX/8xl;->b:LX/0iA;

    invoke-virtual {v1}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v1

    const-string v2, "4481"

    invoke-virtual {v1, v2}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 1424731
    goto/16 :goto_0

    :cond_6
    move v3, v4

    goto/16 :goto_1

    :cond_7
    const/4 v4, 0x0

    goto :goto_2

    :cond_8
    const/4 v1, 0x0

    goto :goto_3
.end method
