.class public Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private A:Lcom/facebook/graphql/model/GraphQLFeedback;

.field private p:LX/6wo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:LX/8xy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private r:LX/8y2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private s:Ljava/lang/String;

.field private t:Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Landroid/view/View;

.field private w:Lcom/facebook/fig/textinput/FigEditText;

.field public x:Lcom/facebook/fig/textinput/FigEditText;

.field private y:Lcom/facebook/fig/textinput/FigEditText;

.field private z:Lcom/facebook/fig/textinput/FigEditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1424653
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;LX/6wo;LX/8xy;LX/8y2;)V
    .locals 0

    .prologue
    .line 1424652
    iput-object p1, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->p:LX/6wo;

    iput-object p2, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->q:LX/8xy;

    iput-object p3, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->r:LX/8y2;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;

    invoke-static {v2}, LX/6wo;->b(LX/0QB;)LX/6wo;

    move-result-object v0

    check-cast v0, LX/6wo;

    new-instance p1, LX/8xy;

    invoke-static {v2}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {v2}, LX/20j;->a(LX/0QB;)LX/20j;

    move-result-object v3

    check-cast v3, LX/20j;

    invoke-static {v2}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object v4

    check-cast v4, LX/1K9;

    invoke-static {v2}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    invoke-direct {p1, v1, v3, v4, v5}, LX/8xy;-><init>(LX/0tX;LX/20j;LX/1K9;LX/1Ck;)V

    move-object v1, p1

    check-cast v1, LX/8xy;

    new-instance p1, LX/8y2;

    invoke-static {v2}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v2}, LX/20j;->a(LX/0QB;)LX/20j;

    move-result-object v4

    check-cast v4, LX/20j;

    invoke-static {v2}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object v5

    check-cast v5, LX/1K9;

    invoke-static {v2}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-direct {p1, v3, v4, v5, v6}, LX/8y2;-><init>(LX/0tX;LX/20j;LX/1K9;LX/1Ck;)V

    move-object v2, p1

    check-cast v2, LX/8y2;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->a(Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;LX/6wo;LX/8xy;LX/8y2;)V

    return-void
.end method

.method public static a(Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;)Z
    .locals 1

    .prologue
    .line 1424650
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->n()V

    .line 1424651
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()Z
    .locals 2

    .prologue
    .line 1424646
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->w:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v0}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1424647
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->w:Lcom/facebook/fig/textinput/FigEditText;

    const v1, 0x7f0823e8

    invoke-virtual {p0, v1}, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/textinput/FigEditText;->setErrorMessage(Ljava/lang/CharSequence;)V

    .line 1424648
    const/4 v0, 0x0

    .line 1424649
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private l()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1424639
    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->x:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v2}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->y:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v2}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v2, v1

    .line 1424640
    :goto_0
    if-nez v2, :cond_2

    .line 1424641
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->v:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1424642
    :goto_1
    return v0

    :cond_1
    move v2, v0

    .line 1424643
    goto :goto_0

    .line 1424644
    :cond_2
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->v:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    move v0, v1

    .line 1424645
    goto :goto_1
.end method

.method private m()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1424633
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->y:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v1}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->x:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v1}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1424634
    :cond_0
    :goto_0
    return v0

    .line 1424635
    :cond_1
    new-instance v1, LX/8xf;

    invoke-direct {v1, p0}, LX/8xf;-><init>(Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;)V

    .line 1424636
    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->p:LX/6wo;

    invoke-virtual {v2, v1}, LX/6wo;->a(LX/6z8;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1424637
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->x:Lcom/facebook/fig/textinput/FigEditText;

    const v1, 0x7f0823e7

    invoke-virtual {p0, v1}, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/textinput/FigEditText;->setErrorMessage(Ljava/lang/CharSequence;)V

    .line 1424638
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private n()V
    .locals 2

    .prologue
    .line 1424628
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->w:Lcom/facebook/fig/textinput/FigEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/fig/textinput/FigEditText;->setErrorMessage(Ljava/lang/CharSequence;)V

    .line 1424629
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->y:Lcom/facebook/fig/textinput/FigEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/fig/textinput/FigEditText;->setErrorMessage(Ljava/lang/CharSequence;)V

    .line 1424630
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->x:Lcom/facebook/fig/textinput/FigEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/fig/textinput/FigEditText;->setErrorMessage(Ljava/lang/CharSequence;)V

    .line 1424631
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->v:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1424632
    return-void
.end method

.method public static o(Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;)V
    .locals 12

    .prologue
    .line 1424566
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->u:Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    if-nez v0, :cond_0

    .line 1424567
    :goto_0
    return-void

    .line 1424568
    :cond_0
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->r:LX/8y2;

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->A:Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->s:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->u:Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->l()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->x()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->w()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->v()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->u()Ljava/lang/String;

    move-result-object v7

    new-instance v8, LX/8xg;

    invoke-direct {v8, p0}, LX/8xg;-><init>(Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;)V

    .line 1424569
    new-instance v9, LX/4II;

    invoke-direct {v9}, LX/4II;-><init>()V

    .line 1424570
    const-string v10, "lightweight_rec_id"

    invoke-virtual {v9, v10, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1424571
    move-object v9, v9

    .line 1424572
    const-string v10, "name"

    invoke-virtual {v9, v10, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1424573
    move-object v9, v9

    .line 1424574
    const-string v10, "address"

    invoke-virtual {v9, v10, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1424575
    const-string v10, "phone_number"

    invoke-virtual {v9, v10, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1424576
    const-string v10, "website_uri"

    invoke-virtual {v9, v10, v7}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1424577
    new-instance v10, LX/5HJ;

    invoke-direct {v10}, LX/5HJ;-><init>()V

    move-object v10, v10

    .line 1424578
    const-string v11, "input"

    invoke-virtual {v10, v11, v9}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1424579
    move-object v9, v10

    .line 1424580
    invoke-static {v9}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v9

    .line 1424581
    iget-object v10, v0, LX/8y2;->d:LX/1Ck;

    const-string v11, "place_list_lightweight_rec_edit"

    iget-object p0, v0, LX/8y2;->a:LX/0tX;

    invoke-virtual {p0, v9}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v9

    new-instance p0, LX/8y1;

    invoke-direct {p0, v0, v8, v1, v2}, LX/8y1;-><init>(LX/8y2;LX/0TF;Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;)V

    invoke-virtual {v10, v11, v9, p0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1424582
    goto :goto_0
.end method

.method public static p(Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;)V
    .locals 9

    .prologue
    .line 1424625
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->t:Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;

    if-nez v0, :cond_0

    .line 1424626
    :goto_0
    return-void

    .line 1424627
    :cond_0
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->q:LX/8xy;

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->A:Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->s:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->t:Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->j()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->x()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->w()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->v()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->u()Ljava/lang/String;

    move-result-object v7

    new-instance v8, LX/8xh;

    invoke-direct {v8, p0}, LX/8xh;-><init>(Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;)V

    invoke-virtual/range {v0 .. v8}, LX/8xy;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0TF;)V

    goto :goto_0
.end method

.method private q()V
    .locals 2

    .prologue
    .line 1424619
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->u:Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    if-nez v0, :cond_0

    .line 1424620
    :goto_0
    return-void

    .line 1424621
    :cond_0
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->w:Lcom/facebook/fig/textinput/FigEditText;

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->u:Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/textinput/FigEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1424622
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->x:Lcom/facebook/fig/textinput/FigEditText;

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->u:Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/textinput/FigEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1424623
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->y:Lcom/facebook/fig/textinput/FigEditText;

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->u:Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/textinput/FigEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1424624
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->z:Lcom/facebook/fig/textinput/FigEditText;

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->u:Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/textinput/FigEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private r()V
    .locals 2

    .prologue
    .line 1424614
    const v0, 0x7f0d2d1c

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1424615
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->t()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1424616
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->u:Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    if-eqz v1, :cond_0

    const v1, 0x7f0823dc

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1424617
    return-void

    .line 1424618
    :cond_0
    const v1, 0x7f0823db

    goto :goto_0
.end method

.method private s()V
    .locals 2

    .prologue
    .line 1424609
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 1424610
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->u:Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    if-eqz v1, :cond_0

    const v1, 0x7f0823de

    :goto_0
    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(I)V

    .line 1424611
    new-instance v1, LX/8xi;

    invoke-direct {v1, p0}, LX/8xi;-><init>(Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 1424612
    return-void

    .line 1424613
    :cond_0
    const v1, 0x7f0823dd

    goto :goto_0
.end method

.method private t()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1424608
    new-instance v0, LX/8xj;

    invoke-direct {v0, p0}, LX/8xj;-><init>(Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;)V

    return-object v0
.end method

.method private u()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1424605
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->z:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v0}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 1424606
    invoke-static {v0}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0

    .line 1424607
    invoke-virtual {v0}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private v()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1424604
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->x:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v0}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private w()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1424603
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->y:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v0}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private x()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1424602
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->w:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v0}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1424583
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1424584
    const v0, 0x7f031384

    invoke-virtual {p0, v0}, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->setContentView(I)V

    .line 1424585
    invoke-static {p0, p0}, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1424586
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_PENDING_PLACE_SLOT"

    invoke-static {v0, v1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->t:Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;

    .line 1424587
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_EXISTING_PLACE"

    invoke-static {v0, v1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->u:Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    .line 1424588
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_FEEDBACK"

    invoke-static {v0, v1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->A:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1424589
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_COMMENT_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->s:Ljava/lang/String;

    .line 1424590
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->s()V

    .line 1424591
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->r()V

    .line 1424592
    const v0, 0x7f0d2d1b

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->v:Landroid/view/View;

    .line 1424593
    const v0, 0x7f0d2d17

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/textinput/FigEditText;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->w:Lcom/facebook/fig/textinput/FigEditText;

    .line 1424594
    const v0, 0x7f0d2d18

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/textinput/FigEditText;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->x:Lcom/facebook/fig/textinput/FigEditText;

    .line 1424595
    const v0, 0x7f0d2d19

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/textinput/FigEditText;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->y:Lcom/facebook/fig/textinput/FigEditText;

    .line 1424596
    const v0, 0x7f0d2d1a

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/textinput/FigEditText;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->z:Lcom/facebook/fig/textinput/FigEditText;

    .line 1424597
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->u:Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    if-eqz v0, :cond_0

    .line 1424598
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->q()V

    .line 1424599
    :goto_0
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->x:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v0}, Lcom/facebook/fig/textinput/FigEditText;->requestFocus()Z

    .line 1424600
    return-void

    .line 1424601
    :cond_0
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->w:Lcom/facebook/fig/textinput/FigEditText;

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/lightweight/LightweightRecommendationActivity;->t:Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/textinput/FigEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
