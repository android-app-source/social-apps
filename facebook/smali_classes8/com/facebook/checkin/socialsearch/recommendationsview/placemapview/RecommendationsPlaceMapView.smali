.class public Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/6Zz;


# instance fields
.field public a:LX/8yJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/8xo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/AOo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Ljava/lang/String;

.field public e:LX/1c9;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1c9",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPlaceListItem;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/model/GraphQLPlaceListItem;

.field private g:LX/6al;

.field private h:Lcom/facebook/maps/FbMapViewDelegate;

.field private i:I

.field private j:I

.field private k:I

.field public l:LX/0Ri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ri",
            "<",
            "LX/6ax;",
            "Lcom/facebook/graphql/model/GraphQLPlaceListItem;",
            ">;"
        }
    .end annotation
.end field

.field private m:I

.field private n:Lcom/facebook/widget/CustomViewPager;

.field private o:Landroid/view/View;

.field private p:Lcom/facebook/uicontrib/fab/FabView;

.field public q:Lcom/facebook/graphql/model/GraphQLPage;

.field private r:LX/AOn;

.field private s:LX/AOv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1669635
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1669636
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->j()V

    .line 1669637
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1669638
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1669639
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->j()V

    .line 1669640
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1669641
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1669642
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->j()V

    .line 1669643
    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLGeoRectangle;)LX/697;
    .locals 8

    .prologue
    .line 1669644
    new-instance v0, LX/697;

    new-instance v1, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->k()D

    move-result-wide v2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->l()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    new-instance v2, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->j()D

    move-result-wide v4

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->a()D

    move-result-wide v6

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    invoke-direct {v0, v1, v2}, LX/697;-><init>(Lcom/facebook/android/maps/model/LatLng;Lcom/facebook/android/maps/model/LatLng;)V

    return-object v0
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLPlaceListItem;)LX/699;
    .locals 12

    .prologue
    const/high16 v1, 0x3f000000    # 0.5f

    .line 1669645
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v3

    .line 1669646
    invoke-static {p0, p1}, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->d(Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;Lcom/facebook/graphql/model/GraphQLPlaceListItem;)Z

    move-result v4

    .line 1669647
    if-eqz v4, :cond_0

    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->r:LX/AOn;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPage;->aY()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/AOn;->b(Ljava/lang/String;)LX/68w;

    move-result-object v0

    move-object v2, v0

    .line 1669648
    :goto_0
    if-eqz v4, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    .line 1669649
    :goto_1
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPage;->N()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v4

    invoke-static {v4}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLLocation;)Landroid/location/Location;

    move-result-object v4

    .line 1669650
    new-instance v5, LX/699;

    invoke-direct {v5}, LX/699;-><init>()V

    new-instance v6, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v4}, Landroid/location/Location;->getLatitude()D

    move-result-wide v8

    invoke-virtual {v4}, Landroid/location/Location;->getLongitude()D

    move-result-wide v10

    invoke-direct {v6, v8, v9, v10, v11}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    .line 1669651
    iput-object v6, v5, LX/699;->b:Lcom/facebook/android/maps/model/LatLng;

    .line 1669652
    move-object v4, v5

    .line 1669653
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v3

    .line 1669654
    iput-object v3, v4, LX/699;->h:Ljava/lang/String;

    .line 1669655
    move-object v3, v4

    .line 1669656
    iput-object v2, v3, LX/699;->c:LX/68w;

    .line 1669657
    move-object v2, v3

    .line 1669658
    invoke-virtual {v2, v1, v0}, LX/699;->a(FF)LX/699;

    move-result-object v0

    return-object v0

    .line 1669659
    :cond_0
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->r:LX/AOn;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPage;->aY()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/AOn;->a(Ljava/lang/String;)LX/68w;

    move-result-object v0

    move-object v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1669660
    goto :goto_1
.end method

.method private a(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLPlaceListItem;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1669661
    if-nez p1, :cond_1

    move-object v0, v1

    .line 1669662
    :cond_0
    :goto_0
    return-object v0

    .line 1669663
    :cond_1
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->e:LX/1c9;

    invoke-virtual {v0}, LX/1c9;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_2

    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->e:LX/1c9;

    invoke-virtual {v0, v2}, LX/1c9;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceListItem;

    .line 1669664
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1669665
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 1669666
    goto :goto_0
.end method

.method private static a(LX/6al;II)V
    .locals 1

    .prologue
    .line 1669667
    add-int v0, p1, p2

    invoke-virtual {p0, p1, p1, p1, v0}, LX/6al;->a(IIII)V

    .line 1669668
    return-void
.end method

.method private a(LX/6ax;)V
    .locals 4

    .prologue
    .line 1669689
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->g:LX/6al;

    invoke-virtual {v0}, LX/6al;->b()LX/6ay;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1669690
    :cond_0
    :goto_0
    return-void

    .line 1669691
    :cond_1
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->g:LX/6al;

    invoke-virtual {v0}, LX/6al;->b()LX/6ay;

    move-result-object v0

    invoke-virtual {v0}, LX/6ay;->a()LX/69F;

    move-result-object v0

    .line 1669692
    invoke-virtual {p1}, LX/6ax;->a()Lcom/facebook/android/maps/model/LatLng;

    move-result-object v1

    .line 1669693
    iget-object v0, v0, LX/69F;->e:LX/697;

    invoke-virtual {v0, v1}, LX/697;->a(Lcom/facebook/android/maps/model/LatLng;)Z

    move-result v0

    .line 1669694
    if-nez v0, :cond_0

    .line 1669695
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->g:LX/6al;

    invoke-static {v1}, LX/6aN;->a(Lcom/facebook/android/maps/model/LatLng;)LX/6aM;

    move-result-object v1

    iget v2, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->m:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/6al;->a(LX/6aM;ILX/6aj;)V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;LX/AOv;LX/8yJ;LX/8xo;LX/AOo;)V
    .locals 0

    .prologue
    .line 1669669
    iput-object p1, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->s:LX/AOv;

    iput-object p2, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->a:LX/8yJ;

    iput-object p3, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->b:LX/8xo;

    iput-object p4, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->c:LX/AOo;

    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLPlaceListItem;Lcom/facebook/graphql/model/GraphQLPlaceListItem;)V
    .locals 2

    .prologue
    .line 1669670
    if-eqz p2, :cond_0

    .line 1669671
    invoke-direct {p0, p2}, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->c(Lcom/facebook/graphql/model/GraphQLPlaceListItem;)V

    .line 1669672
    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->b(Lcom/facebook/graphql/model/GraphQLPlaceListItem;)LX/6ax;

    move-result-object v0

    .line 1669673
    invoke-direct {p0, v0}, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->a(LX/6ax;)V

    .line 1669674
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->n:Lcom/facebook/widget/CustomViewPager;

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->e:LX/1c9;

    invoke-virtual {v1, p1}, LX/1c9;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 1669675
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 5

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;

    new-instance v2, LX/AOv;

    invoke-static {v3}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-static {v3}, LX/9Er;->b(LX/0QB;)LX/9Er;

    move-result-object v1

    check-cast v1, LX/9Er;

    invoke-direct {v2, v0, v1}, LX/AOv;-><init>(LX/0Sh;LX/9Er;)V

    move-object v0, v2

    check-cast v0, LX/AOv;

    invoke-static {v3}, LX/8yJ;->b(LX/0QB;)LX/8yJ;

    move-result-object v1

    check-cast v1, LX/8yJ;

    invoke-static {v3}, LX/8xo;->b(LX/0QB;)LX/8xo;

    move-result-object v2

    check-cast v2, LX/8xo;

    const-class v4, LX/AOo;

    invoke-interface {v3, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/AOo;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->a(Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;LX/AOv;LX/8yJ;LX/8xo;LX/AOo;)V

    return-void
.end method

.method private b(Lcom/facebook/graphql/model/GraphQLPlaceListItem;)LX/6ax;
    .locals 3

    .prologue
    .line 1669676
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->l:LX/0Ri;

    invoke-interface {v0}, LX/0Ri;->a_()LX/0Ri;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0Ri;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6ax;

    .line 1669677
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->r:LX/AOn;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->aY()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/AOn;->b(Ljava/lang/String;)LX/68w;

    move-result-object v1

    .line 1669678
    invoke-virtual {v0, v1}, LX/6ax;->a(LX/68w;)V

    .line 1669679
    const/high16 v1, 0x3f000000    # 0.5f

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2}, LX/6ax;->a(FF)V

    .line 1669680
    return-object v0
.end method

.method private c(Lcom/facebook/graphql/model/GraphQLPlaceListItem;)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f000000    # 0.5f

    .line 1669681
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->l:LX/0Ri;

    invoke-interface {v0}, LX/0Ri;->a_()LX/0Ri;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0Ri;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6ax;

    .line 1669682
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->r:LX/AOn;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->aY()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/AOn;->a(Ljava/lang/String;)LX/68w;

    move-result-object v1

    .line 1669683
    invoke-virtual {v0, v1}, LX/6ax;->a(LX/68w;)V

    .line 1669684
    invoke-virtual {v0, v3, v3}, LX/6ax;->a(FF)V

    .line 1669685
    return-void
.end method

.method public static d(Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;Lcom/facebook/graphql/model/GraphQLPlaceListItem;)Z
    .locals 2

    .prologue
    .line 1669686
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->f:Lcom/facebook/graphql/model/GraphQLPlaceListItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->f:Lcom/facebook/graphql/model/GraphQLPlaceListItem;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1669687
    :cond_0
    const/4 v0, 0x0

    .line 1669688
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->f:Lcom/facebook/graphql/model/GraphQLPlaceListItem;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static e(Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;Lcom/facebook/graphql/model/GraphQLPlaceListItem;)V
    .locals 2

    .prologue
    .line 1669625
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->f:Lcom/facebook/graphql/model/GraphQLPlaceListItem;

    .line 1669626
    iput-object p1, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->f:Lcom/facebook/graphql/model/GraphQLPlaceListItem;

    .line 1669627
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->f:Lcom/facebook/graphql/model/GraphQLPlaceListItem;

    invoke-direct {p0, v1, v0}, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->a(Lcom/facebook/graphql/model/GraphQLPlaceListItem;Lcom/facebook/graphql/model/GraphQLPlaceListItem;)V

    .line 1669628
    return-void
.end method

.method private h()V
    .locals 6

    .prologue
    .line 1669629
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->q:Lcom/facebook/graphql/model/GraphQLPage;

    if-nez v0, :cond_0

    .line 1669630
    :goto_0
    return-void

    .line 1669631
    :cond_0
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->q:Lcom/facebook/graphql/model/GraphQLPage;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->O()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v0

    .line 1669632
    if-eqz v0, :cond_1

    .line 1669633
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->g:LX/6al;

    invoke-static {v0}, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->a(Lcom/facebook/graphql/model/GraphQLGeoRectangle;)LX/697;

    move-result-object v0

    iget v2, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->k:I

    invoke-static {v0, v2}, LX/6aN;->a(LX/697;I)LX/6aM;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/6al;->a(LX/6aM;)V

    goto :goto_0

    .line 1669634
    :cond_1
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->g:LX/6al;

    new-instance v1, Lcom/facebook/android/maps/model/LatLng;

    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->q:Lcom/facebook/graphql/model/GraphQLPage;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->N()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLLocation;->a()D

    move-result-wide v2

    iget-object v4, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->q:Lcom/facebook/graphql/model/GraphQLPage;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->N()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLLocation;->b()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    const/high16 v2, 0x41500000    # 13.0f

    invoke-static {v1, v2}, LX/6aN;->a(Lcom/facebook/android/maps/model/LatLng;F)LX/6aM;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6al;->a(LX/6aM;)V

    goto :goto_0
.end method

.method private i()V
    .locals 7

    .prologue
    .line 1669504
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->g:LX/6al;

    if-eqz v0, :cond_0

    .line 1669505
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->g:LX/6al;

    invoke-virtual {v0}, LX/6al;->a()V

    .line 1669506
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->g:LX/6al;

    iget v1, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->j:I

    iget v2, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->i:I

    invoke-static {v0, v1, v2}, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->a(LX/6al;II)V

    .line 1669507
    :cond_0
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->g:LX/6al;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->e:LX/1c9;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->e:LX/1c9;

    invoke-virtual {v0}, LX/1c9;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1669508
    :cond_1
    :goto_0
    return-void

    .line 1669509
    :cond_2
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->e:LX/1c9;

    invoke-virtual {v0}, LX/1c9;->size()I

    move-result v0

    invoke-static {v0}, LX/1Ei;->a(I)LX/1Ei;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->l:LX/0Ri;

    .line 1669510
    new-instance v2, LX/696;

    invoke-direct {v2}, LX/696;-><init>()V

    .line 1669511
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->e:LX/1c9;

    invoke-virtual {v0}, LX/1c9;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_4

    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->e:LX/1c9;

    invoke-virtual {v0, v1}, LX/1c9;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceListItem;

    .line 1669512
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    .line 1669513
    if-eqz v4, :cond_3

    .line 1669514
    invoke-direct {p0, v0}, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->a(Lcom/facebook/graphql/model/GraphQLPlaceListItem;)LX/699;

    move-result-object v4

    .line 1669515
    iget-object v5, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->g:LX/6al;

    invoke-virtual {v5, v4}, LX/6al;->a(LX/699;)LX/6ax;

    move-result-object v5

    .line 1669516
    iget-object v6, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->l:LX/0Ri;

    invoke-interface {v6, v5, v0}, LX/0Ri;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1669517
    iget-object v0, v4, LX/699;->b:Lcom/facebook/android/maps/model/LatLng;

    move-object v0, v0

    .line 1669518
    invoke-virtual {v2, v0}, LX/696;->a(Lcom/facebook/android/maps/model/LatLng;)LX/696;

    .line 1669519
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1669520
    :cond_4
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->g:LX/6al;

    invoke-virtual {v2}, LX/696;->a()LX/697;

    move-result-object v1

    iget v2, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->k:I

    invoke-static {v1, v2}, LX/6aN;->a(LX/697;I)LX/6aM;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6al;->a(LX/6aM;)V

    goto :goto_0
.end method

.method private j()V
    .locals 4

    .prologue
    .line 1669521
    const-class v0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;

    invoke-static {v0, p0}, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1669522
    const v0, 0x7f031387

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1669523
    const v0, 0x7f0d1d47

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/maps/FbMapViewDelegate;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->h:Lcom/facebook/maps/FbMapViewDelegate;

    .line 1669524
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1669525
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1669526
    const v0, 0x7f0b19bf

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->j:I

    .line 1669527
    const v0, 0x7f0b19be

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iget v3, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->j:I

    add-int/2addr v0, v3

    iput v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->k:I

    .line 1669528
    const v0, 0x10e0001

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->m:I

    .line 1669529
    const v0, 0x7f0d1d48

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomViewPager;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->n:Lcom/facebook/widget/CustomViewPager;

    .line 1669530
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->n:Lcom/facebook/widget/CustomViewPager;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 1669531
    const v0, 0x7f0d2d1e

    invoke-virtual {p0, v0}, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/fab/FabView;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->p:Lcom/facebook/uicontrib/fab/FabView;

    .line 1669532
    const v0, 0x7f0b0060

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 1669533
    const/high16 v2, -0x40400000    # -1.5f

    int-to-float v0, v0

    mul-float/2addr v0, v2

    .line 1669534
    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->n:Lcom/facebook/widget/CustomViewPager;

    float-to-int v0, v0

    invoke-virtual {v2, v0}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    .line 1669535
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->n:Lcom/facebook/widget/CustomViewPager;

    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->s:LX/AOv;

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 1669536
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->n:Lcom/facebook/widget/CustomViewPager;

    new-instance v2, LX/AOr;

    invoke-direct {v2, p0}, LX/AOr;-><init>(Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;)V

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 1669537
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->c:LX/AOo;

    .line 1669538
    new-instance v3, LX/AOn;

    invoke-static {v0}, LX/1Et;->a(LX/0QB;)LX/1FZ;

    move-result-object v2

    check-cast v2, LX/1FZ;

    invoke-direct {v3, v1, v2}, LX/AOn;-><init>(Landroid/content/Context;LX/1FZ;)V

    .line 1669539
    move-object v0, v3

    .line 1669540
    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->r:LX/AOn;

    .line 1669541
    const v0, 0x7f0d2d23

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->o:Landroid/view/View;

    .line 1669542
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1669543
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->h:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v0}, LX/6Zn;->d()V

    .line 1669544
    return-void
.end method

.method public final a(LX/6al;)V
    .locals 2

    .prologue
    .line 1669545
    iput-object p1, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->g:LX/6al;

    .line 1669546
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->g:LX/6al;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/6al;->a(Z)V

    .line 1669547
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->e:LX/1c9;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->e:LX/1c9;

    invoke-virtual {v0}, LX/1c9;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1669548
    :cond_0
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->h()V

    .line 1669549
    :goto_0
    return-void

    .line 1669550
    :cond_1
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->i()V

    .line 1669551
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->g:LX/6al;

    new-instance v1, LX/AOq;

    invoke-direct {v1, p0}, LX/AOq;-><init>(Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;)V

    invoke-virtual {v0, v1}, LX/6al;->a(LX/6ak;)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1669552
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->h:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v0, p1}, LX/6Zn;->a(Landroid/os/Bundle;)V

    .line 1669553
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->h:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v0, p0}, LX/6Zn;->a(LX/6Zz;)V

    .line 1669554
    return-void
.end method

.method public final a(Ljava/lang/String;LX/1c9;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/1c9",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPlaceListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1669555
    iput-object p1, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->d:Ljava/lang/String;

    .line 1669556
    iput-object p2, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->e:LX/1c9;

    .line 1669557
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->e:LX/1c9;

    invoke-virtual {v0}, LX/1c9;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1669558
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->e:LX/1c9;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1c9;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceListItem;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->f:Lcom/facebook/graphql/model/GraphQLPlaceListItem;

    .line 1669559
    :goto_0
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->s:LX/AOv;

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->e:LX/1c9;

    .line 1669560
    iput-object v1, v0, LX/AOv;->a:Ljava/lang/String;

    .line 1669561
    iput-object v2, v0, LX/AOv;->b:Ljava/util/List;

    .line 1669562
    invoke-virtual {v0}, LX/0gG;->kV_()V

    .line 1669563
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->n:Lcom/facebook/widget/CustomViewPager;

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->s:LX/AOv;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 1669564
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->r:LX/AOn;

    .line 1669565
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlaceListItem;

    .line 1669566
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    .line 1669567
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPage;->aY()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 1669568
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPage;->aY()Ljava/lang/String;

    move-result-object v1

    .line 1669569
    iget-object p1, v0, LX/AOn;->b:Ljava/util/Map;

    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 1669570
    invoke-static {v0, v1}, LX/AOn;->c(LX/AOn;Ljava/lang/String;)V

    goto :goto_1

    .line 1669571
    :cond_1
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->g:LX/6al;

    if-nez v0, :cond_3

    .line 1669572
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->h:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v0, p0}, LX/6Zn;->a(LX/6Zz;)V

    .line 1669573
    :goto_2
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->requestLayout()V

    .line 1669574
    return-void

    .line 1669575
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->f:Lcom/facebook/graphql/model/GraphQLPlaceListItem;

    goto :goto_0

    .line 1669576
    :cond_3
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->i()V

    goto :goto_2
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1669577
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->h:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v0}, LX/6Zn;->b()V

    .line 1669578
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1669579
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->h:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v0}, LX/6Zn;->c()V

    .line 1669580
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 1669502
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->h:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v0}, LX/6Zn;->a()V

    .line 1669503
    return-void
.end method

.method public final g()V
    .locals 4

    .prologue
    .line 1669586
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->b:LX/8xo;

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->o:Landroid/view/View;

    .line 1669587
    iget-object v2, v0, LX/8xo;->d:LX/0iA;

    sget-object v3, LX/8xo;->b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class p0, LX/8xn;

    invoke-virtual {v2, v3, p0}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v2

    .line 1669588
    if-eqz v2, :cond_0

    .line 1669589
    check-cast v2, LX/8xn;

    .line 1669590
    sget-boolean v3, LX/8xn;->c:Z

    move v3, v3

    .line 1669591
    if-nez v3, :cond_0

    .line 1669592
    const v3, 0x7f0823d1

    invoke-static {v3, v1}, LX/8xo;->a(ILandroid/view/View;)V

    .line 1669593
    const/4 v3, 0x1

    sput-boolean v3, LX/8xn;->c:Z

    .line 1669594
    iget-object v3, v2, LX/8xn;->b:LX/0iA;

    invoke-virtual {v3}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v3

    const-string p0, "4495"

    invoke-virtual {v3, p0}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 1669595
    :cond_0
    return-void
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    .line 1669596
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 1669597
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->n:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v0}, Lcom/facebook/widget/CustomViewPager;->getMeasuredHeight()I

    move-result v0

    .line 1669598
    iget v1, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->i:I

    if-eq v1, v0, :cond_0

    .line 1669599
    iput v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->i:I

    .line 1669600
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->g:LX/6al;

    if-eqz v0, :cond_0

    .line 1669601
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->g:LX/6al;

    iget v1, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->j:I

    iget v2, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->i:I

    invoke-static {v0, v1, v2}, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->a(LX/6al;II)V

    .line 1669602
    :cond_0
    return-void
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 1669603
    instance-of v0, p1, Landroid/os/Bundle;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 1669604
    check-cast v0, Landroid/os/Bundle;

    .line 1669605
    check-cast p1, Landroid/os/Bundle;

    const-string v1, "SUPER_STATE"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Lcom/facebook/widget/CustomFrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1669606
    const-string v1, "EXTRA_SELECTED_PAGE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1669607
    invoke-direct {p0, v0}, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->a(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLPlaceListItem;

    move-result-object v0

    .line 1669608
    if-eqz v0, :cond_0

    .line 1669609
    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->f:Lcom/facebook/graphql/model/GraphQLPlaceListItem;

    .line 1669610
    :cond_0
    return-void
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    .prologue
    .line 1669611
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 1669612
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1669613
    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->h:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v2, v1}, LX/6Zn;->b(Landroid/os/Bundle;)V

    .line 1669614
    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->f:Lcom/facebook/graphql/model/GraphQLPlaceListItem;

    if-eqz v2, :cond_0

    .line 1669615
    const-string v2, "EXTRA_SELECTED_PAGE"

    iget-object v3, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->f:Lcom/facebook/graphql/model/GraphQLPlaceListItem;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1669616
    :cond_0
    const-string v2, "SUPER_STATE"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1669617
    return-object v1
.end method

.method public setAddPlaceButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1669618
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->p:Lcom/facebook/uicontrib/fab/FabView;

    invoke-virtual {v0, p1}, Lcom/facebook/uicontrib/fab/FabView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1669619
    return-void
.end method

.method public setCanViewerEdit(Z)V
    .locals 2

    .prologue
    .line 1669581
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->s:LX/AOv;

    .line 1669582
    iput-boolean p1, v0, LX/AOv;->c:Z

    .line 1669583
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->p:Lcom/facebook/uicontrib/fab/FabView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/uicontrib/fab/FabView;->setVisibility(I)V

    .line 1669584
    return-void

    .line 1669585
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setDeleteRequestedListener(LX/AOi;)V
    .locals 1

    .prologue
    .line 1669620
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->s:LX/AOv;

    .line 1669621
    iput-object p1, v0, LX/AOv;->e:LX/AOi;

    .line 1669622
    return-void
.end method

.method public setMapCenter(Lcom/facebook/graphql/model/GraphQLPage;)V
    .locals 0

    .prologue
    .line 1669623
    iput-object p1, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->q:Lcom/facebook/graphql/model/GraphQLPage;

    .line 1669624
    return-void
.end method
