.class public Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/AOi;


# instance fields
.field public A:Z

.field private B:Landroid/widget/ViewFlipper;

.field private C:LX/8yD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final D:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;",
            ">;>;"
        }
    .end annotation
.end field

.field private final E:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLAddPlaceRecommendationFromPlaceListMutationCallModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public p:LX/8yJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/9jH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/2cm;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/8yB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/1c9;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1c9",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPlaceListItem;",
            ">;"
        }
    .end annotation
.end field

.field public v:Ljava/lang/String;

.field private w:Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;

.field private x:Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;

.field private y:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

.field public z:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1669310
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1669311
    new-instance v0, LX/AOc;

    invoke-direct {v0, p0}, LX/AOc;-><init>(Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;)V

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->D:LX/0TF;

    .line 1669312
    new-instance v0, LX/AOd;

    invoke-direct {v0, p0}, LX/AOd;-><init>(Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;)V

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->E:LX/0TF;

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1669305
    const v0, 0x7f0d2d24

    invoke-virtual {p0, v0}, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->y:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 1669306
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->y:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0824f1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(Ljava/lang/String;)V

    .line 1669307
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->y:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    new-instance v1, LX/AOf;

    invoke-direct {v1, p0}, LX/AOf;-><init>(Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 1669308
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->y:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    new-instance v1, LX/AOg;

    invoke-direct {v1, p0}, LX/AOg;-><init>(Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setActionButtonOnClickListener(LX/107;)V

    .line 1669309
    return-void
.end method

.method private a(IZ)V
    .locals 2

    .prologue
    .line 1669295
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->B:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v0

    .line 1669296
    if-ne v0, p1, :cond_0

    .line 1669297
    :goto_0
    return-void

    .line 1669298
    :cond_0
    if-eqz p2, :cond_1

    .line 1669299
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 1669300
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->B:Landroid/widget/ViewFlipper;

    const v1, 0x7f0400b5

    invoke-virtual {v0, p0, v1}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/content/Context;I)V

    .line 1669301
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->B:Landroid/widget/ViewFlipper;

    const v1, 0x7f0400b2

    invoke-virtual {v0, p0, v1}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/content/Context;I)V

    .line 1669302
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->B:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, p1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    goto :goto_0

    .line 1669303
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->B:Landroid/widget/ViewFlipper;

    const v1, 0x7f0400b4

    invoke-virtual {v0, p0, v1}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/content/Context;I)V

    .line 1669304
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->B:Landroid/widget/ViewFlipper;

    const v1, 0x7f0400b3

    invoke-virtual {v0, p0, v1}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/content/Context;I)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;LX/8yJ;LX/9jH;LX/2cm;LX/8yB;LX/0kL;LX/8yD;)V
    .locals 0

    .prologue
    .line 1669294
    iput-object p1, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->p:LX/8yJ;

    iput-object p2, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->q:LX/9jH;

    iput-object p3, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->r:LX/2cm;

    iput-object p4, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->s:LX/8yB;

    iput-object p5, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->t:LX/0kL;

    iput-object p6, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->C:LX/8yD;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;

    invoke-static {v6}, LX/8yJ;->b(LX/0QB;)LX/8yJ;

    move-result-object v1

    check-cast v1, LX/8yJ;

    invoke-static {v6}, LX/9jH;->b(LX/0QB;)LX/9jH;

    move-result-object v2

    check-cast v2, LX/9jH;

    invoke-static {v6}, LX/2cm;->b(LX/0QB;)LX/2cm;

    move-result-object v3

    check-cast v3, LX/2cm;

    new-instance p1, LX/8yB;

    invoke-static {v6}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v6}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v5

    check-cast v5, LX/0sa;

    invoke-static {v6}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object p0

    check-cast p0, LX/1Ck;

    invoke-direct {p1, v4, v5, p0}, LX/8yB;-><init>(LX/0tX;LX/0sa;LX/1Ck;)V

    move-object v4, p1

    check-cast v4, LX/8yB;

    invoke-static {v6}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v5

    check-cast v5, LX/0kL;

    new-instance p1, LX/8yD;

    invoke-static {v6}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v7

    check-cast v7, LX/0tX;

    invoke-static {v6}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object p0

    check-cast p0, LX/1Ck;

    invoke-direct {p1, v7, p0}, LX/8yD;-><init>(LX/0tX;LX/1Ck;)V

    move-object v6, p1

    check-cast v6, LX/8yD;

    invoke-static/range {v0 .. v6}, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->a(Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;LX/8yJ;LX/9jH;LX/2cm;LX/8yB;LX/0kL;LX/8yD;)V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLPlaceListItem;Lcom/facebook/graphql/model/GraphQLPage;)Z
    .locals 2

    .prologue
    .line 1669293
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPlaceListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1669289
    invoke-static {p1}, LX/1c9;->a(Ljava/util/List;)LX/1c9;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->u:LX/1c9;

    .line 1669290
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->w:Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->v:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->u:LX/1c9;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1669291
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->x:Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->v:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->u:LX/1c9;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->a(Ljava/lang/String;LX/1c9;)V

    .line 1669292
    return-void
.end method

.method private b(Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1669288
    new-instance v0, LX/AOh;

    invoke-direct {v0, p0, p1}, LX/AOh;-><init>(Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;Ljava/lang/String;)V

    return-object v0
.end method

.method public static b(Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;Lcom/facebook/graphql/model/GraphQLPlaceListItem;)V
    .locals 8

    .prologue
    .line 1669270
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    .line 1669271
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->C:LX/8yD;

    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->v:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->D:LX/0TF;

    .line 1669272
    new-instance v5, LX/4IO;

    invoke-direct {v5}, LX/4IO;-><init>()V

    .line 1669273
    const-string v6, "story_id"

    invoke-virtual {v5, v6, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1669274
    move-object v5, v5

    .line 1669275
    const-string v6, "place_id"

    invoke-virtual {v5, v6, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1669276
    move-object v5, v5

    .line 1669277
    new-instance v6, LX/5He;

    invoke-direct {v6}, LX/5He;-><init>()V

    move-object v6, v6

    .line 1669278
    const-string v7, "input"

    invoke-virtual {v6, v7, v5}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1669279
    iget-object v5, v0, LX/8yD;->b:LX/1Ck;

    const-string v7, "seeker_delete_place"

    iget-object p1, v0, LX/8yD;->a:LX/0tX;

    invoke-static {v6}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    new-instance p1, LX/8yC;

    invoke-direct {p1, v0, v4}, LX/8yC;-><init>(LX/8yD;LX/0TF;)V

    invoke-virtual {v5, v7, v6, p1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1669280
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->u:LX/1c9;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1669281
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 1669282
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1669283
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceListItem;

    .line 1669284
    invoke-static {v0, v1}, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->a(Lcom/facebook/graphql/model/GraphQLPlaceListItem;Lcom/facebook/graphql/model/GraphQLPage;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1669285
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 1669286
    :cond_1
    invoke-static {p0, v2}, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->a$redex0(Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;Ljava/util/List;)V

    .line 1669287
    return-void
.end method

.method public static b$redex0(Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1669263
    invoke-direct {p0, v3, p1}, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->a(IZ)V

    .line 1669264
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->y:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    const v2, 0x7f0824f3

    invoke-virtual {p0, v2}, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1669265
    iput-object v2, v1, LX/108;->g:Ljava/lang/String;

    .line 1669266
    move-object v1, v1

    .line 1669267
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setPrimaryButton(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 1669268
    iput-boolean v3, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->z:Z

    .line 1669269
    return-void
.end method

.method public static c$redex0(Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1669256
    invoke-direct {p0, v3, p1}, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->a(IZ)V

    .line 1669257
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->y:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    const v2, 0x7f0824f4

    invoke-virtual {p0, v2}, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1669258
    iput-object v2, v1, LX/108;->g:Ljava/lang/String;

    .line 1669259
    move-object v1, v1

    .line 1669260
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setPrimaryButton(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 1669261
    iput-boolean v3, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->z:Z

    .line 1669262
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLPlaceListItem;)V
    .locals 3

    .prologue
    .line 1669254
    new-instance v0, LX/31Y;

    invoke-direct {v0, p0}, LX/31Y;-><init>(Landroid/content/Context;)V

    const/high16 v1, 0x1040000

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f0824f6

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f0824f7

    new-instance v2, LX/AOe;

    invoke-direct {v2, p0, p1}, LX/AOe;-><init>(Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;Lcom/facebook/graphql/model/GraphQLPlaceListItem;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 1669255
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 1669172
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1669173
    invoke-static {p0, p0}, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1669174
    const v0, 0x7f031388

    invoke-virtual {p0, v0}, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->setContentView(I)V

    .line 1669175
    if-eqz p1, :cond_0

    .line 1669176
    const-string v0, "social_search_place_list_extra"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->A:Z

    .line 1669177
    :cond_0
    const v0, 0x7f0d2d26

    invoke-virtual {p0, v0}, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->w:Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;

    .line 1669178
    const v0, 0x7f0d2d27

    invoke-virtual {p0, v0}, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->x:Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;

    .line 1669179
    const v0, 0x7f0d2d25

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->B:Landroid/widget/ViewFlipper;

    .line 1669180
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 1669181
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "social_search_place_list_extra"

    invoke-static {v0, v1}, LX/4By;->b(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 1669182
    if-nez v0, :cond_1

    .line 1669183
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1669184
    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "social_search_place_location_extra"

    invoke-static {v0, v3}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 1669185
    iget-object v3, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->x:Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;

    .line 1669186
    iput-object v0, v3, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->q:Lcom/facebook/graphql/model/GraphQLPage;

    .line 1669187
    move-object v0, v1

    .line 1669188
    :cond_1
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->w:Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;

    iget-object v3, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->v:Ljava/lang/String;

    invoke-virtual {v1, v3, v0}, Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1669189
    const-string v1, "social_search_story_id_extra"

    invoke-virtual {v2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->v:Ljava/lang/String;

    .line 1669190
    if-nez p1, :cond_2

    .line 1669191
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->p:LX/8yJ;

    iget-object v3, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->v:Ljava/lang/String;

    .line 1669192
    iget-object v5, v1, LX/8yJ;->b:LX/0Zb;

    const-string v6, "social_search_full_map_open"

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v5

    .line 1669193
    invoke-virtual {v5}, LX/0oG;->a()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1669194
    const-string v6, "social_search"

    invoke-virtual {v5, v6}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 1669195
    iget-object v6, v1, LX/8yJ;->c:LX/0kv;

    iget-object v7, v1, LX/8yJ;->a:Landroid/content/Context;

    invoke-virtual {v6, v7}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    .line 1669196
    invoke-virtual {v5, v3}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    .line 1669197
    const-string v6, "story_graphql_id"

    invoke-virtual {v5, v6, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1669198
    const-string v6, "entrypoint"

    const-string v7, "full_map_permalink"

    invoke-virtual {v5, v6, v7}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1669199
    invoke-virtual {v5}, LX/0oG;->d()V

    .line 1669200
    :cond_2
    const-string v1, "social_search_can_viewer_edit_extra"

    invoke-virtual {v2, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 1669201
    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->x:Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;

    invoke-virtual {v2, v1}, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->setCanViewerEdit(Z)V

    .line 1669202
    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->w:Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;

    invoke-virtual {v2, v1}, Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;->setCanViewerEdit(Z)V

    .line 1669203
    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->v:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->b(Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v2

    .line 1669204
    iget-object v3, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->x:Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;

    invoke-virtual {v3, v2}, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->setAddPlaceButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1669205
    iget-object v3, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->w:Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;

    invoke-virtual {v3, v2}, Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;->setAddPlaceButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1669206
    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->x:Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;

    invoke-virtual {v2, p0}, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->setDeleteRequestedListener(LX/AOi;)V

    .line 1669207
    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->w:Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;

    invoke-virtual {v2, p0}, Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;->setDeleteRequestedListener(LX/AOi;)V

    .line 1669208
    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->x:Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;

    invoke-virtual {v2, p1}, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->a(Landroid/os/Bundle;)V

    .line 1669209
    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1669210
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->x:Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;

    invoke-virtual {v1}, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->g()V

    .line 1669211
    :cond_3
    invoke-static {p0, v0}, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->a$redex0(Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;Ljava/util/List;)V

    .line 1669212
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->a()V

    .line 1669213
    invoke-static {p0, v4}, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->b$redex0(Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;Z)V

    .line 1669214
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 1669237
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1669238
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 1669239
    :goto_0
    return-void

    .line 1669240
    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 1669241
    :pswitch_0
    const-string v0, "extra_place"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1669242
    const-string v1, "story_id_for_social_search"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1669243
    iget-object v2, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->s:LX/8yB;

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->E:LX/0TF;

    .line 1669244
    new-instance p0, LX/4IF;

    invoke-direct {p0}, LX/4IF;-><init>()V

    .line 1669245
    const-string p1, "story_id"

    invoke-virtual {p0, p1, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1669246
    move-object p0, p0

    .line 1669247
    const-string p1, "place_id"

    invoke-virtual {p0, p1, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1669248
    move-object p0, p0

    .line 1669249
    new-instance p1, LX/5Hd;

    invoke-direct {p1}, LX/5Hd;-><init>()V

    move-object p1, p1

    .line 1669250
    const-string p2, "input"

    invoke-virtual {p1, p2, p0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1669251
    const-string p0, "profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p0, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1669252
    iget-object p0, v2, LX/8yB;->c:LX/1Ck;

    const-string p2, "seeker_add_place"

    iget-object p3, v2, LX/8yB;->a:LX/0tX;

    invoke-static {p1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object p1

    invoke-virtual {p3, p1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p1

    new-instance p3, LX/8yA;

    invoke-direct {p3, v2, v3}, LX/8yA;-><init>(LX/8yB;LX/0TF;)V

    invoke-virtual {p0, p2, p1, p3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1669253
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x138a
        :pswitch_0
    .end packed-switch
.end method

.method public final onBackPressed()V
    .locals 3

    .prologue
    .line 1669230
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1669231
    const-string v1, "social_search_place_list_extra"

    iget-boolean v2, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->A:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1669232
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1669233
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1669234
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->setResult(ILandroid/content/Intent;)V

    .line 1669235
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 1669236
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x321477cb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1669227
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 1669228
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->x:Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;

    invoke-virtual {v1}, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->f()V

    .line 1669229
    const/16 v1, 0x23

    const v2, -0x75c07bdd

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onLowMemory()V
    .locals 1

    .prologue
    .line 1669224
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onLowMemory()V

    .line 1669225
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->x:Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->b()V

    .line 1669226
    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x54e6af3b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1669221
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 1669222
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->x:Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;

    invoke-virtual {v1}, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->e()V

    .line 1669223
    const/16 v1, 0x23

    const v2, 0x7a3f8775

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x5ef1d9cd

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1669218
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 1669219
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->x:Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;

    invoke-virtual {v1}, Lcom/facebook/checkin/socialsearch/recommendationsview/placemapview/RecommendationsPlaceMapView;->a()V

    .line 1669220
    const/16 v1, 0x23

    const v2, -0x4d57ed82

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1669215
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1669216
    const-string v0, "social_search_place_list_extra"

    iget-boolean v1, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/SocialSearchRecommendationsViewActivity;->A:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1669217
    return-void
.end method
