.class public Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/AOm;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1e2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Landroid/support/v7/widget/RecyclerView;

.field private d:LX/1OR;

.field private e:Lcom/facebook/uicontrib/fab/FabView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1669330
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1669331
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;->a()V

    .line 1669332
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1669333
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1669334
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;->a()V

    .line 1669335
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1669344
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1669345
    invoke-direct {p0}, Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;->a()V

    .line 1669346
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1669336
    const-class v0, Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;

    invoke-static {v0, p0}, Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1669337
    const v0, 0x7f031385

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1669338
    const v0, 0x7f0d2d1d

    invoke-virtual {p0, v0}, Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;->c:Landroid/support/v7/widget/RecyclerView;

    .line 1669339
    new-instance v0, LX/1P1;

    invoke-virtual {p0}, Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1P1;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;->d:LX/1OR;

    .line 1669340
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;->d:LX/1OR;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1669341
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;->a:LX/AOm;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1669342
    const v0, 0x7f0d2d1e

    invoke-virtual {p0, v0}, Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/fab/FabView;

    iput-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;->e:Lcom/facebook/uicontrib/fab/FabView;

    .line 1669343
    return-void
.end method

.method private static a(Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;LX/AOm;LX/1e2;)V
    .locals 0

    .prologue
    .line 1669329
    iput-object p1, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;->a:LX/AOm;

    iput-object p2, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;->b:LX/1e2;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;

    new-instance p1, LX/AOm;

    invoke-static {v1}, LX/9Er;->b(LX/0QB;)LX/9Er;

    move-result-object v0

    check-cast v0, LX/9Er;

    invoke-static {v1}, LX/1e2;->b(LX/0QB;)LX/1e2;

    move-result-object v2

    check-cast v2, LX/1e2;

    invoke-direct {p1, v0, v2}, LX/AOm;-><init>(LX/9Er;LX/1e2;)V

    move-object v0, p1

    check-cast v0, LX/AOm;

    invoke-static {v1}, LX/1e2;->b(LX/0QB;)LX/1e2;

    move-result-object v1

    check-cast v1, LX/1e2;

    invoke-static {p0, v0, v1}, Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;->a(Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;LX/AOm;LX/1e2;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPlaceListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1669324
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;->a:LX/AOm;

    .line 1669325
    iput-object p1, v0, LX/AOm;->a:Ljava/lang/String;

    .line 1669326
    iput-object p2, v0, LX/AOm;->b:Ljava/util/List;

    .line 1669327
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 1669328
    return-void
.end method

.method public setAddPlaceButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1669322
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;->e:Lcom/facebook/uicontrib/fab/FabView;

    invoke-virtual {v0, p1}, Lcom/facebook/uicontrib/fab/FabView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1669323
    return-void
.end method

.method public setCanViewerEdit(Z)V
    .locals 2

    .prologue
    .line 1669316
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;->a:LX/AOm;

    .line 1669317
    iput-boolean p1, v0, LX/AOm;->c:Z

    .line 1669318
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    .line 1669319
    :goto_0
    iget-object v1, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;->e:Lcom/facebook/uicontrib/fab/FabView;

    invoke-virtual {v1, v0}, Lcom/facebook/uicontrib/fab/FabView;->setVisibility(I)V

    .line 1669320
    return-void

    .line 1669321
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setDeleteRequestedListener(LX/AOi;)V
    .locals 1

    .prologue
    .line 1669313
    iget-object v0, p0, Lcom/facebook/checkin/socialsearch/recommendationsview/placelistview/RecommendationsPlaceListView;->a:LX/AOm;

    .line 1669314
    iput-object p1, v0, LX/AOm;->d:LX/AOi;

    .line 1669315
    return-void
.end method
