.class public Lcom/facebook/livephotos/LivePhotoView;
.super Landroid/widget/FrameLayout;
.source ""

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/9Ut;
.implements LX/9Ux;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# static fields
.field public static final e:Ljava/lang/String;

.field public static final f:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/9V3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/9Uz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/9VM;

.field public h:Landroid/view/SurfaceView;

.field public i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private j:LX/9Uv;

.field public k:Landroid/os/Handler;

.field public l:LX/9Uy;

.field private m:LX/9Ux;

.field private n:Z

.field private o:Z

.field private p:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljava/lang/String;

.field private s:Landroid/net/Uri;

.field public t:Landroid/widget/ImageView;

.field private u:Z

.field public v:Z

.field private w:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1498704
    const-class v0, Lcom/facebook/livephotos/LivePhotoView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/livephotos/LivePhotoView;->e:Ljava/lang/String;

    .line 1498705
    const-class v0, Lcom/facebook/livephotos/LivePhotoView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/livephotos/LivePhotoView;->f:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 1498706
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 1498707
    sget-object v0, LX/9Uv;->IDLE:LX/9Uv;

    iput-object v0, p0, Lcom/facebook/livephotos/LivePhotoView;->j:LX/9Uv;

    .line 1498708
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030a35

    const/4 v3, 0x1

    invoke-virtual {v1, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 1498709
    const-class v1, Lcom/facebook/livephotos/LivePhotoView;

    invoke-static {v1, p0}, Lcom/facebook/livephotos/LivePhotoView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1498710
    const v1, 0x7f0d1063

    invoke-virtual {p0, v1}, Lcom/facebook/livephotos/LivePhotoView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v1, p0, Lcom/facebook/livephotos/LivePhotoView;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1498711
    new-instance v1, LX/1Uo;

    invoke-virtual {p0}, Lcom/facebook/livephotos/LivePhotoView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    const/4 v2, 0x0

    .line 1498712
    iput v2, v1, LX/1Uo;->d:I

    .line 1498713
    move-object v1, v1

    .line 1498714
    sget-object v2, LX/1Up;->g:LX/1Up;

    invoke-virtual {v1, v2}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v1

    invoke-virtual {v1}, LX/1Uo;->u()LX/1af;

    move-result-object v2

    .line 1498715
    const v1, 0x7f0d19d2

    invoke-virtual {p0, v1}, Lcom/facebook/livephotos/LivePhotoView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/facebook/livephotos/LivePhotoView;->t:Landroid/widget/ImageView;

    .line 1498716
    iget-object v1, p0, Lcom/facebook/livephotos/LivePhotoView;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 1498717
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lcom/facebook/livephotos/LivePhotoView;->k:Landroid/os/Handler;

    .line 1498718
    new-instance v1, LX/9VM;

    invoke-virtual {p0}, Lcom/facebook/livephotos/LivePhotoView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/livephotos/LivePhotoView;->k:Landroid/os/Handler;

    invoke-direct {v1, v2, v3, p0}, LX/9VM;-><init>(Landroid/content/Context;Landroid/os/Handler;LX/9Ux;)V

    iput-object v1, p0, Lcom/facebook/livephotos/LivePhotoView;->g:LX/9VM;

    .line 1498719
    new-instance v1, LX/9Uy;

    invoke-direct {v1, p1, p0}, LX/9Uy;-><init>(Landroid/content/Context;LX/9Ut;)V

    iput-object v1, p0, Lcom/facebook/livephotos/LivePhotoView;->l:LX/9Uy;

    .line 1498720
    goto :goto_0

    .line 1498721
    :goto_0
    return-void
.end method

.method public static a(Lcom/facebook/livephotos/LivePhotoView;Z)V
    .locals 1

    .prologue
    .line 1498722
    invoke-virtual {p0}, Lcom/facebook/livephotos/LivePhotoView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 1498723
    if-eqz v0, :cond_0

    .line 1498724
    invoke-interface {v0, p1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 1498725
    :cond_0
    return-void
.end method

.method public static a(Lcom/facebook/livephotos/LivePhotoView;ZZ)V
    .locals 6

    .prologue
    const v0, 0x3e99999a    # 0.3f

    const/4 v3, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 1498726
    if-eqz p1, :cond_3

    move v2, v1

    .line 1498727
    :goto_0
    iget-object v4, p0, Lcom/facebook/livephotos/LivePhotoView;->t:Landroid/widget/ImageView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1498728
    iget-object v4, p0, Lcom/facebook/livephotos/LivePhotoView;->t:Landroid/widget/ImageView;

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 1498729
    iget-object v4, p0, Lcom/facebook/livephotos/LivePhotoView;->t:Landroid/widget/ImageView;

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 1498730
    iget-object v4, p0, Lcom/facebook/livephotos/LivePhotoView;->t:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    move v2, v1

    :goto_1
    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 1498731
    iget-object v2, p0, Lcom/facebook/livephotos/LivePhotoView;->t:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    if-eqz p1, :cond_1

    :goto_2
    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    if-eqz p2, :cond_2

    const-wide/16 v0, 0x64

    :goto_3
    invoke-virtual {v2, v0, v1}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 1498732
    return-void

    :cond_0
    move v2, v3

    .line 1498733
    goto :goto_1

    :cond_1
    move v3, v1

    .line 1498734
    goto :goto_2

    :cond_2
    const-wide/16 v0, 0x1f4

    goto :goto_3

    :cond_3
    move v2, v0

    move v0, v1

    goto :goto_0
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/livephotos/LivePhotoView;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-static {p0}, Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;->a(LX/0QB;)Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;

    move-result-object v2

    check-cast v2, Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;

    invoke-static {p0}, LX/9V3;->a(LX/0QB;)LX/9V3;

    move-result-object v3

    check-cast v3, LX/9V3;

    invoke-static {p0}, LX/9Uz;->a(LX/0QB;)LX/9Uz;

    move-result-object p0

    check-cast p0, LX/9Uz;

    iput-object v1, p1, Lcom/facebook/livephotos/LivePhotoView;->a:LX/1Ad;

    iput-object v2, p1, Lcom/facebook/livephotos/LivePhotoView;->b:Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;

    iput-object v3, p1, Lcom/facebook/livephotos/LivePhotoView;->c:LX/9V3;

    iput-object p0, p1, Lcom/facebook/livephotos/LivePhotoView;->d:LX/9Uz;

    return-void
.end method

.method private g()V
    .locals 1

    .prologue
    .line 1498687
    iget-object v0, p0, Lcom/facebook/livephotos/LivePhotoView;->h:Landroid/view/SurfaceView;

    if-eqz v0, :cond_0

    .line 1498688
    iget-object v0, p0, Lcom/facebook/livephotos/LivePhotoView;->h:Landroid/view/SurfaceView;

    invoke-virtual {p0, v0}, Lcom/facebook/livephotos/LivePhotoView;->removeView(Landroid/view/View;)V

    .line 1498689
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/livephotos/LivePhotoView;->h:Landroid/view/SurfaceView;

    .line 1498690
    :cond_0
    return-void
.end method

.method private h()V
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1498735
    iget-object v0, p0, Lcom/facebook/livephotos/LivePhotoView;->j:LX/9Uv;

    sget-object v3, LX/9Uv;->IDLE:LX/9Uv;

    if-ne v0, v3, :cond_2

    move v0, v1

    .line 1498736
    :goto_0
    iget-object v3, p0, Lcom/facebook/livephotos/LivePhotoView;->s:Landroid/net/Uri;

    if-eqz v3, :cond_0

    move v2, v1

    .line 1498737
    :cond_0
    if-eqz v0, :cond_1

    if-eqz v2, :cond_1

    iget-boolean v0, p0, Lcom/facebook/livephotos/LivePhotoView;->o:Z

    if-eqz v0, :cond_1

    .line 1498738
    invoke-static {p0, v1}, Lcom/facebook/livephotos/LivePhotoView;->a(Lcom/facebook/livephotos/LivePhotoView;Z)V

    .line 1498739
    new-instance v0, Landroid/view/SurfaceView;

    invoke-virtual {p0}, Lcom/facebook/livephotos/LivePhotoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/livephotos/LivePhotoView;->h:Landroid/view/SurfaceView;

    .line 1498740
    iget-object v0, p0, Lcom/facebook/livephotos/LivePhotoView;->h:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/livephotos/LivePhotoView;->g:LX/9VM;

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 1498741
    iget-object v0, p0, Lcom/facebook/livephotos/LivePhotoView;->h:Landroid/view/SurfaceView;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/livephotos/LivePhotoView;->addView(Landroid/view/View;I)V

    .line 1498742
    iget-object v0, p0, Lcom/facebook/livephotos/LivePhotoView;->g:LX/9VM;

    iget-object v1, p0, Lcom/facebook/livephotos/LivePhotoView;->s:Landroid/net/Uri;

    .line 1498743
    iget-object v2, v0, LX/9VM;->c:LX/9Uv;

    sget-object v3, LX/9Uv;->IDLE:LX/9Uv;

    if-ne v2, v3, :cond_1

    .line 1498744
    sget-object v2, LX/9Uv;->PREPARING:LX/9Uv;

    invoke-static {v0, v2}, LX/9VM;->a(LX/9VM;LX/9Uv;)V

    .line 1498745
    const/4 v6, 0x4

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1498746
    invoke-static {v6, v7, v7}, LX/0Kw;->a(III)LX/0Kx;

    move-result-object v2

    iput-object v2, v0, LX/9VM;->g:LX/0Kx;

    .line 1498747
    iget-object v2, v0, LX/9VM;->g:LX/0Kx;

    invoke-interface {v2, v0}, LX/0Kx;->a(LX/0KW;)V

    .line 1498748
    new-instance v2, LX/9VW;

    iget-object v3, v0, LX/9VM;->g:LX/0Kx;

    invoke-direct {v2, v3}, LX/9VW;-><init>(LX/0Kx;)V

    iput-object v2, v0, LX/9VM;->h:LX/9VW;

    .line 1498749
    iget-object v2, v0, LX/9VM;->d:Landroid/content/Context;

    const/4 v5, 0x0

    .line 1498750
    invoke-static {v2, v1}, LX/9VU;->a(Landroid/content/Context;Landroid/net/Uri;)LX/0L9;

    move-result-object v3

    .line 1498751
    new-instance v4, LX/9VQ;

    invoke-direct {v4, v2, v3, v5, v5}, LX/9VQ;-><init>(Landroid/content/Context;LX/0L9;Landroid/os/Handler;LX/0Ju;)V

    .line 1498752
    new-instance v5, LX/9VU;

    new-instance v10, LX/9VP;

    invoke-direct {v10, v3}, LX/9VP;-><init>(LX/0L9;)V

    invoke-direct {v5, v4, v10}, LX/9VU;-><init>(LX/0GT;LX/0GT;)V

    move-object v2, v5

    .line 1498753
    iget-object v3, v0, LX/9VM;->d:Landroid/content/Context;

    iget-object v4, v0, LX/9VM;->e:Landroid/os/Handler;

    iget-object v5, v0, LX/9VM;->h:LX/9VW;

    .line 1498754
    new-instance v10, LX/9VO;

    invoke-static {v3, v1}, LX/9VU;->a(Landroid/content/Context;Landroid/net/Uri;)LX/0L9;

    move-result-object v11

    invoke-direct {v10, v3, v11, v4, v5}, LX/9VO;-><init>(Landroid/content/Context;LX/0L9;Landroid/os/Handler;LX/0Ju;)V

    .line 1498755
    new-instance v11, LX/9VU;

    const/4 p0, 0x0

    invoke-direct {v11, v10, p0}, LX/9VU;-><init>(LX/0GT;LX/0GT;)V

    move-object v3, v11

    .line 1498756
    iget-object v4, v0, LX/9VM;->h:LX/9VW;

    new-array v5, v9, [LX/9VU;

    aput-object v2, v5, v7

    aput-object v3, v5, v8

    .line 1498757
    iput-object v5, v4, LX/9VW;->j:[LX/9VU;

    .line 1498758
    iget-object v4, v0, LX/9VM;->g:LX/0Kx;

    new-array v5, v6, [LX/0GT;

    iget-object v6, v2, LX/9VU;->e:LX/0GT;

    aput-object v6, v5, v7

    iget-object v3, v3, LX/9VU;->e:LX/0GT;

    aput-object v3, v5, v8

    iget-object v2, v2, LX/9VU;->f:LX/0GT;

    aput-object v2, v5, v9

    const/4 v2, 0x3

    iget-object v3, v0, LX/9VM;->h:LX/9VW;

    aput-object v3, v5, v2

    invoke-interface {v4, v5}, LX/0Kx;->a([LX/0GT;)V

    .line 1498759
    iget-object v2, v0, LX/9VM;->g:LX/0Kx;

    invoke-interface {v2, v8}, LX/0Kx;->a(Z)V

    .line 1498760
    invoke-static {v0}, LX/9VM;->c(LX/9VM;)V

    .line 1498761
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 1498762
    goto/16 :goto_0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 1498763
    iget-object v0, p0, Lcom/facebook/livephotos/LivePhotoView;->j:LX/9Uv;

    sget-object v1, LX/9Uv;->IDLE:LX/9Uv;

    if-eq v0, v1, :cond_0

    .line 1498764
    iget-object v0, p0, Lcom/facebook/livephotos/LivePhotoView;->g:LX/9VM;

    invoke-virtual {v0}, LX/9VM;->b()V

    .line 1498765
    invoke-direct {p0}, Lcom/facebook/livephotos/LivePhotoView;->g()V

    .line 1498766
    :cond_0
    return-void
.end method

.method private j()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1498767
    iget-boolean v0, p0, Lcom/facebook/livephotos/LivePhotoView;->n:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/livephotos/LivePhotoView;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/livephotos/LivePhotoView;->s:Landroid/net/Uri;

    if-nez v0, :cond_0

    .line 1498768
    iget-object v0, p0, Lcom/facebook/livephotos/LivePhotoView;->b:Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;

    iget-object v1, p0, Lcom/facebook/livephotos/LivePhotoView;->q:Ljava/lang/String;

    .line 1498769
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 1498770
    iget-object v2, v0, Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;->f:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1j2;

    .line 1498771
    if-eqz v2, :cond_1

    .line 1498772
    iget-object v4, v2, LX/1j2;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v4, v4

    .line 1498773
    invoke-interface {v4}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1498774
    iget-object v4, v2, LX/1j2;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v2, v4

    .line 1498775
    :goto_0
    move-object v0, v2

    .line 1498776
    iput-object v0, p0, Lcom/facebook/livephotos/LivePhotoView;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1498777
    iget-object v0, p0, Lcom/facebook/livephotos/LivePhotoView;->k:Landroid/os/Handler;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/livephotos/LivePhotoView;->q:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 1498778
    iget-object v1, p0, Lcom/facebook/livephotos/LivePhotoView;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, LX/9Uw;

    invoke-direct {v2, v0}, LX/9Uw;-><init>(Landroid/os/Message;)V

    invoke-static {v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1498779
    :cond_0
    return-void

    .line 1498780
    :cond_1
    new-instance v2, LX/9V6;

    invoke-direct {v2, v0, v1}, LX/9V6;-><init>(Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;Ljava/lang/String;)V

    .line 1498781
    iget-object v4, v0, Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;->c:LX/0TD;

    invoke-interface {v4, v2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 1498782
    new-instance v4, LX/9V7;

    invoke-direct {v4, v0, v1, v3}, LX/9V7;-><init>(Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;Ljava/lang/String;Z)V

    .line 1498783
    iget-object v5, v0, Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;->c:LX/0TD;

    invoke-static {v2, v4, v5}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 1498784
    new-instance v4, Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher$3;

    invoke-direct {v4, v0, v1}, Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher$3;-><init>(Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;Ljava/lang/String;)V

    .line 1498785
    sget-object v5, LX/131;->INSTANCE:LX/131;

    move-object v5, v5

    .line 1498786
    invoke-interface {v2, v4, v5}, Lcom/google/common/util/concurrent/ListenableFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1498787
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/livephotos/LivePhotoView;->o:Z

    .line 1498788
    invoke-direct {p0}, Lcom/facebook/livephotos/LivePhotoView;->h()V

    .line 1498789
    return-void
.end method

.method public final a(J)V
    .locals 12

    .prologue
    .line 1498691
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/livephotos/LivePhotoView;->o:Z

    .line 1498692
    iput-wide p1, p0, Lcom/facebook/livephotos/LivePhotoView;->w:J

    .line 1498693
    iget-object v0, p0, Lcom/facebook/livephotos/LivePhotoView;->j:LX/9Uv;

    sget-object v1, LX/9Uv;->PLAYING:LX/9Uv;

    if-ne v0, v1, :cond_1

    .line 1498694
    const/4 v3, 0x0

    invoke-static {p0, v3}, Lcom/facebook/livephotos/LivePhotoView;->a(Lcom/facebook/livephotos/LivePhotoView;Z)V

    .line 1498695
    iget-object v3, p0, Lcom/facebook/livephotos/LivePhotoView;->g:LX/9VM;

    .line 1498696
    iget-object v4, v3, LX/9VM;->g:LX/0Kx;

    if-eqz v4, :cond_0

    iget-object v4, v3, LX/9VM;->h:LX/9VW;

    if-eqz v4, :cond_0

    .line 1498697
    iget-object v4, v3, LX/9VM;->g:LX/0Kx;

    iget-object v5, v3, LX/9VM;->h:LX/9VW;

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-interface {v4, v5, v6, v7}, LX/0Kx;->a(LX/0GS;ILjava/lang/Object;)V

    .line 1498698
    :cond_0
    const-wide/16 v3, 0x1f4

    const-wide/16 v9, 0x2

    .line 1498699
    iget-object v5, p0, Lcom/facebook/livephotos/LivePhotoView;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setAlpha(F)V

    .line 1498700
    iget-object v5, p0, Lcom/facebook/livephotos/LivePhotoView;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {v5, v6}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    div-long v7, v3, v9

    invoke-virtual {v5, v7, v8}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    div-long v7, v3, v9

    invoke-virtual {v5, v7, v8}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    .line 1498701
    :goto_0
    return-void

    .line 1498702
    :cond_1
    invoke-direct {p0}, Lcom/facebook/livephotos/LivePhotoView;->i()V

    .line 1498703
    invoke-direct {p0}, Lcom/facebook/livephotos/LivePhotoView;->g()V

    goto :goto_0
.end method

.method public final a(LX/9Uv;)V
    .locals 5

    .prologue
    .line 1498790
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onPlaybackStateChanged - "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1498791
    iput-object p1, p0, Lcom/facebook/livephotos/LivePhotoView;->j:LX/9Uv;

    .line 1498792
    sget-object v0, LX/9Uu;->a:[I

    invoke-virtual {p1}, LX/9Uv;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1498793
    :cond_0
    :goto_0
    return-void

    .line 1498794
    :pswitch_0
    invoke-direct {p0}, Lcom/facebook/livephotos/LivePhotoView;->g()V

    .line 1498795
    iget-object v0, p0, Lcom/facebook/livephotos/LivePhotoView;->c:LX/9V3;

    iget-wide v2, p0, Lcom/facebook/livephotos/LivePhotoView;->w:J

    .line 1498796
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "live_photo_view"

    invoke-direct {v1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v4, "gesture_time_millis"

    invoke-virtual {v1, v4, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 1498797
    iget-object v4, v0, LX/9V3;->a:LX/0Zb;

    invoke-interface {v4, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1498798
    const/4 v1, 0x1

    .line 1498799
    iget-boolean v0, p0, Lcom/facebook/livephotos/LivePhotoView;->v:Z

    if-nez v0, :cond_1

    .line 1498800
    iput-boolean v1, p0, Lcom/facebook/livephotos/LivePhotoView;->v:Z

    .line 1498801
    const/4 v0, 0x0

    invoke-static {p0, v0, v1}, Lcom/facebook/livephotos/LivePhotoView;->a(Lcom/facebook/livephotos/LivePhotoView;ZZ)V

    .line 1498802
    :cond_1
    goto :goto_0

    .line 1498803
    :pswitch_1
    iget-boolean v0, p0, Lcom/facebook/livephotos/LivePhotoView;->o:Z

    if-eqz v0, :cond_0

    .line 1498804
    const-wide/16 v0, 0x96

    .line 1498805
    iget-object v2, p0, Lcom/facebook/livephotos/LivePhotoView;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setAlpha(F)V

    .line 1498806
    iget-object v2, p0, Lcom/facebook/livephotos/LivePhotoView;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 1498807
    const/4 v1, 0x1

    .line 1498808
    iget-boolean v0, p0, Lcom/facebook/livephotos/LivePhotoView;->v:Z

    if-eqz v0, :cond_2

    .line 1498809
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/livephotos/LivePhotoView;->v:Z

    .line 1498810
    invoke-static {p0, v1, v1}, Lcom/facebook/livephotos/LivePhotoView;->a(Lcom/facebook/livephotos/LivePhotoView;ZZ)V

    .line 1498811
    :cond_2
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final handleMessage(Landroid/os/Message;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1498600
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 1498601
    :cond_0
    :goto_0
    return v1

    .line 1498602
    :pswitch_0
    invoke-virtual {p0, v1}, Lcom/facebook/livephotos/LivePhotoView;->setIsLivePhoto(Z)V

    .line 1498603
    iget-object v0, p0, Lcom/facebook/livephotos/LivePhotoView;->q:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/livephotos/LivePhotoView;->q:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    iget v2, p1, Landroid/os/Message;->arg1:I

    if-ne v0, v2, :cond_1

    move v0, v1

    .line 1498604
    :goto_1
    iget-boolean v2, p0, Lcom/facebook/livephotos/LivePhotoView;->n:Z

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    .line 1498605
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/livephotos/LivePhotoView;->s:Landroid/net/Uri;

    .line 1498606
    invoke-direct {p0}, Lcom/facebook/livephotos/LivePhotoView;->h()V

    goto :goto_0

    .line 1498607
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x7a9604ac

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1498608
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 1498609
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/livephotos/LivePhotoView;->n:Z

    .line 1498610
    invoke-direct {p0}, Lcom/facebook/livephotos/LivePhotoView;->j()V

    .line 1498611
    const/16 v1, 0x2d

    const v2, 0xe25e84c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x1c053195    # -9.25312E21f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1498612
    invoke-direct {p0}, Lcom/facebook/livephotos/LivePhotoView;->i()V

    .line 1498613
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 1498614
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/livephotos/LivePhotoView;->n:Z

    .line 1498615
    const/16 v1, 0x2d

    const v2, 0x2762dcf9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 5

    .prologue
    .line 1498616
    invoke-virtual {p0}, Lcom/facebook/livephotos/LivePhotoView;->getPaddingLeft()I

    move-result v0

    .line 1498617
    invoke-virtual {p0}, Lcom/facebook/livephotos/LivePhotoView;->getPaddingTop()I

    move-result v1

    .line 1498618
    sub-int v2, p4, p2

    sub-int/2addr v2, v0

    invoke-virtual {p0}, Lcom/facebook/livephotos/LivePhotoView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    .line 1498619
    sub-int v3, p5, p3

    sub-int/2addr v3, v1

    invoke-virtual {p0}, Lcom/facebook/livephotos/LivePhotoView;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v3, v4

    .line 1498620
    add-int/2addr v2, v0

    .line 1498621
    add-int/2addr v3, v1

    .line 1498622
    iget-object v4, p0, Lcom/facebook/livephotos/LivePhotoView;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v4, v0, v1, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->layout(IIII)V

    .line 1498623
    iget-object v4, p0, Lcom/facebook/livephotos/LivePhotoView;->h:Landroid/view/SurfaceView;

    if-eqz v4, :cond_0

    .line 1498624
    iget-object v4, p0, Lcom/facebook/livephotos/LivePhotoView;->h:Landroid/view/SurfaceView;

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/view/SurfaceView;->layout(IIII)V

    .line 1498625
    :cond_0
    iget-object v0, p0, Lcom/facebook/livephotos/LivePhotoView;->t:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v0

    sub-int v0, v2, v0

    .line 1498626
    iget-object v1, p0, Lcom/facebook/livephotos/LivePhotoView;->t:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v1

    sub-int v1, v3, v1

    .line 1498627
    iget-object v4, p0, Lcom/facebook/livephotos/LivePhotoView;->t:Landroid/widget/ImageView;

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/widget/ImageView;->layout(IIII)V

    .line 1498628
    return-void
.end method

.method public final onMeasure(II)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1498629
    iget-object v1, p0, Lcom/facebook/livephotos/LivePhotoView;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v0, p0

    move v2, p1

    move v4, p2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/livephotos/LivePhotoView;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 1498630
    iget-object v1, p0, Lcom/facebook/livephotos/LivePhotoView;->t:Landroid/widget/ImageView;

    move-object v0, p0

    move v2, p1

    move v4, p2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/livephotos/LivePhotoView;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 1498631
    invoke-virtual {p0}, Lcom/facebook/livephotos/LivePhotoView;->getSuggestedMinimumWidth()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/livephotos/LivePhotoView;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getMeasuredWidth()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1498632
    invoke-virtual {p0}, Lcom/facebook/livephotos/LivePhotoView;->getSuggestedMinimumHeight()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/livephotos/LivePhotoView;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getMeasuredHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1498633
    iget-object v2, p0, Lcom/facebook/livephotos/LivePhotoView;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getMeasuredState()I

    move-result v2

    .line 1498634
    shl-int/lit8 v3, v2, 0x10

    .line 1498635
    invoke-static {v0, p1, v2}, Landroid/view/View;->resolveSizeAndState(III)I

    move-result v0

    .line 1498636
    invoke-static {v1, p2, v3}, Landroid/view/View;->resolveSizeAndState(III)I

    move-result v1

    .line 1498637
    invoke-virtual {p0, v0, v1}, Lcom/facebook/livephotos/LivePhotoView;->setMeasuredDimension(II)V

    .line 1498638
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, -0x30cb0d45

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1498639
    iget-object v2, p0, Lcom/facebook/livephotos/LivePhotoView;->l:LX/9Uy;

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1498640
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    .line 1498641
    packed-switch v5, :pswitch_data_0

    .line 1498642
    :cond_0
    :goto_0
    move v2, v3

    .line 1498643
    if-nez v2, :cond_1

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    :goto_1
    const v2, -0x54d0e64a

    invoke-static {v2, v1}, LX/02F;->a(II)V

    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 1498644
    :pswitch_0
    iget-boolean v5, v2, LX/9Uy;->f:Z

    if-nez v5, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v5

    if-nez v5, :cond_0

    .line 1498645
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    float-to-int v5, v5

    iput v5, v2, LX/9Uy;->a:I

    .line 1498646
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    iput v5, v2, LX/9Uy;->b:I

    .line 1498647
    iget-object v5, v2, LX/9Uy;->c:Landroid/os/Handler;

    sget-wide v7, LX/9V0;->a:J

    invoke-virtual {v5, v4, v7, v8}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 1498648
    :pswitch_1
    iget-boolean v5, v2, LX/9Uy;->f:Z

    if-eqz v5, :cond_3

    .line 1498649
    const-wide/16 v5, 0x1

    invoke-static {v5, v6}, Landroid/os/SystemClock;->sleep(J)V

    move v3, v4

    .line 1498650
    goto :goto_0

    .line 1498651
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    float-to-int v5, v5

    iget v6, v2, LX/9Uy;->a:I

    sub-int/2addr v5, v6

    .line 1498652
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v6, v6

    iget v7, v2, LX/9Uy;->b:I

    sub-int/2addr v6, v7

    .line 1498653
    mul-int/2addr v5, v5

    mul-int/2addr v6, v6

    add-int/2addr v5, v6

    .line 1498654
    iget v6, v2, LX/9Uy;->d:I

    if-le v5, v6, :cond_0

    .line 1498655
    iput-boolean v3, v2, LX/9Uy;->f:Z

    .line 1498656
    iget-object v5, v2, LX/9Uy;->c:Landroid/os/Handler;

    invoke-virtual {v5, v4}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 1498657
    :pswitch_2
    iget-object v5, v2, LX/9Uy;->c:Landroid/os/Handler;

    invoke-virtual {v5, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 1498658
    iget-boolean v5, v2, LX/9Uy;->f:Z

    if-eqz v5, :cond_0

    .line 1498659
    iput-boolean v3, v2, LX/9Uy;->f:Z

    .line 1498660
    iget-object v3, v2, LX/9Uy;->e:LX/9Ut;

    invoke-static {}, LX/9Uy;->a()J

    move-result-wide v5

    iget-wide v7, v2, LX/9Uy;->g:J

    sub-long/2addr v5, v7

    invoke-interface {v3, v5, v6}, LX/9Ut;->a(J)V

    move v3, v4

    .line 1498661
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setIsLivePhoto(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1498662
    iput-boolean p1, p0, Lcom/facebook/livephotos/LivePhotoView;->v:Z

    .line 1498663
    iget-boolean v0, p0, Lcom/facebook/livephotos/LivePhotoView;->u:Z

    if-eq v0, p1, :cond_1

    .line 1498664
    iput-boolean p1, p0, Lcom/facebook/livephotos/LivePhotoView;->u:Z

    .line 1498665
    if-nez p1, :cond_0

    .line 1498666
    invoke-super {p0, v1}, Landroid/widget/FrameLayout;->setLongClickable(Z)V

    .line 1498667
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/livephotos/LivePhotoView;->n:Z

    if-eqz v0, :cond_3

    .line 1498668
    if-nez p1, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {p0, v0, v1}, Lcom/facebook/livephotos/LivePhotoView;->a(Lcom/facebook/livephotos/LivePhotoView;ZZ)V

    .line 1498669
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v1

    .line 1498670
    goto :goto_0

    .line 1498671
    :cond_3
    iget-object v0, p0, Lcom/facebook/livephotos/LivePhotoView;->t:Landroid/widget/ImageView;

    if-eqz p1, :cond_4

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    :cond_4
    const/16 v1, 0x8

    goto :goto_2
.end method

.method public setListener(LX/9Ux;)V
    .locals 0

    .prologue
    .line 1498598
    iput-object p1, p0, Lcom/facebook/livephotos/LivePhotoView;->m:LX/9Ux;

    .line 1498599
    return-void
.end method

.method public setLongClickable(Z)V
    .locals 1

    .prologue
    .line 1498672
    iget-boolean v0, p0, Lcom/facebook/livephotos/LivePhotoView;->u:Z

    if-nez v0, :cond_0

    .line 1498673
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setLongClickable(Z)V

    .line 1498674
    :cond_0
    return-void
.end method

.method public setVideoUri(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1498675
    iget-object v2, p0, Lcom/facebook/livephotos/LivePhotoView;->d:LX/9Uz;

    iget-boolean v2, v2, LX/9Uz;->a:Z

    if-eqz v2, :cond_0

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-ge v2, v3, :cond_1

    .line 1498676
    :cond_0
    :goto_0
    return-void

    .line 1498677
    :cond_1
    if-nez p1, :cond_3

    iget-object v2, p0, Lcom/facebook/livephotos/LivePhotoView;->q:Ljava/lang/String;

    if-eqz v2, :cond_3

    move v2, v0

    .line 1498678
    :goto_1
    if-eqz p1, :cond_4

    iget-object v3, p0, Lcom/facebook/livephotos/LivePhotoView;->q:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 1498679
    :goto_2
    if-nez v2, :cond_2

    if-eqz v0, :cond_0

    .line 1498680
    :cond_2
    iput-object p1, p0, Lcom/facebook/livephotos/LivePhotoView;->q:Ljava/lang/String;

    .line 1498681
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/livephotos/LivePhotoView;->s:Landroid/net/Uri;

    .line 1498682
    if-eqz p1, :cond_5

    .line 1498683
    invoke-direct {p0}, Lcom/facebook/livephotos/LivePhotoView;->j()V

    goto :goto_0

    :cond_3
    move v2, v1

    .line 1498684
    goto :goto_1

    :cond_4
    move v0, v1

    .line 1498685
    goto :goto_2

    .line 1498686
    :cond_5
    invoke-virtual {p0, v1}, Lcom/facebook/livephotos/LivePhotoView;->setIsLivePhoto(Z)V

    goto :goto_0
.end method
