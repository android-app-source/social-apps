.class public Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile g:Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;


# instance fields
.field public c:LX/0TD;

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9V5;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/1Hk;

.field public final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/1j2",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1498973
    const-class v0, Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;->a:Ljava/lang/String;

    .line 1498974
    const-class v0, Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0TD;LX/1Hk;LX/0Ot;)V
    .locals 1
    .param p1    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ImageOffUiThreadExecutor;
        .end annotation
    .end param
    .param p2    # LX/1Hk;
        .annotation build Lcom/facebook/livephotos/downloader/LivePhotosFileCache;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TD;",
            "LX/1Hk;",
            "LX/0Ot",
            "<",
            "LX/9V5;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1498975
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1498976
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;->f:Ljava/util/Map;

    .line 1498977
    iput-object p1, p0, Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;->c:LX/0TD;

    .line 1498978
    iput-object p3, p0, Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;->d:LX/0Ot;

    .line 1498979
    iput-object p2, p0, Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;->e:LX/1Hk;

    .line 1498980
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;
    .locals 6

    .prologue
    .line 1498981
    sget-object v0, Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;->g:Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;

    if-nez v0, :cond_1

    .line 1498982
    const-class v1, Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;

    monitor-enter v1

    .line 1498983
    :try_start_0
    sget-object v0, Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;->g:Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1498984
    if-eqz v2, :cond_0

    .line 1498985
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1498986
    new-instance v5, Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;

    invoke-static {v0}, LX/1G6;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, LX/0TD;

    invoke-static {v0}, LX/9Us;->a(LX/0QB;)LX/1Hk;

    move-result-object v4

    check-cast v4, LX/1Hk;

    const/16 p0, 0x25e8

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;-><init>(LX/0TD;LX/1Hk;LX/0Ot;)V

    .line 1498987
    move-object v0, v5

    .line 1498988
    sput-object v0, Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;->g:Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1498989
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1498990
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1498991
    :cond_1
    sget-object v0, Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;->g:Lcom/facebook/livephotos/downloader/LivePhotosPrefetcher;

    return-object v0

    .line 1498992
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1498993
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/1gI;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1498994
    if-eqz p0, :cond_0

    instance-of v0, p0, LX/1gH;

    if-nez v0, :cond_1

    .line 1498995
    :cond_0
    const/4 v0, 0x0

    .line 1498996
    :goto_0
    return-object v0

    :cond_1
    check-cast p0, LX/1gH;

    .line 1498997
    iget-object v0, p0, LX/1gH;->a:Ljava/io/File;

    move-object v0, v0

    .line 1498998
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
