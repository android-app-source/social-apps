.class public Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile c:Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;


# instance fields
.field private final b:LX/1nu;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1426201
    const-class v0, Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1426198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1426199
    iput-object p1, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;->b:LX/1nu;

    .line 1426200
    return-void
.end method

.method private static a(LX/1De;II)I
    .locals 1

    .prologue
    .line 1426195
    if-nez p1, :cond_0

    .line 1426196
    invoke-virtual {p0}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int p1, v0

    .line 1426197
    :cond_0
    return p1
.end method

.method private a(LX/1De;LX/1dc;Landroid/net/Uri;)LX/1Di;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;",
            "Landroid/net/Uri;",
            ")",
            "LX/1Di;"
        }
    .end annotation

    .prologue
    .line 1426202
    if-nez p2, :cond_0

    if-nez p3, :cond_0

    .line 1426203
    const/4 v0, 0x0

    .line 1426204
    :goto_0
    return-object v0

    .line 1426205
    :cond_0
    if-eqz p3, :cond_1

    .line 1426206
    iget-object v0, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;->b:LX/1nu;

    invoke-virtual {v0, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v0

    sget-object v1, Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    goto :goto_0

    .line 1426207
    :cond_1
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;
    .locals 4

    .prologue
    .line 1426170
    sget-object v0, Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;->c:Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;

    if-nez v0, :cond_1

    .line 1426171
    const-class v1, Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;

    monitor-enter v1

    .line 1426172
    :try_start_0
    sget-object v0, Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;->c:Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1426173
    if-eqz v2, :cond_0

    .line 1426174
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1426175
    new-instance p0, Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v3

    check-cast v3, LX/1nu;

    invoke-direct {p0, v3}, Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;-><init>(LX/1nu;)V

    .line 1426176
    move-object v0, p0

    .line 1426177
    sput-object v0, Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;->c:Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1426178
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1426179
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1426180
    :cond_1
    sget-object v0, Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;->c:Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;

    return-object v0

    .line 1426181
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1426182
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/1De;II)I
    .locals 1

    .prologue
    .line 1426192
    if-nez p1, :cond_0

    .line 1426193
    invoke-virtual {p0}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    .line 1426194
    :cond_0
    return p1
.end method


# virtual methods
.method public final a(LX/1De;Ljava/lang/CharSequence;LX/1dQ;ILjava/lang/CharSequence;IILX/1dQ;Ljava/lang/CharSequence;IILX/1dQ;IZIILandroid/net/Uri;LX/1dc;IIILX/1dQ;Landroid/net/Uri;LX/1dc;IIILX/1dQ;)LX/1Dg;
    .locals 8
    .param p2    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p3    # LX/1dQ;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_TEXT:LX/32B;
        .end annotation
    .end param
    .param p5    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p6    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_TEXT:LX/32B;
        .end annotation
    .end param
    .param p7    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p8    # LX/1dQ;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p9    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p10    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_TEXT:LX/32B;
        .end annotation
    .end param
    .param p11    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p12    # LX/1dQ;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p13    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p14    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p15    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .param p16    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p17    # Landroid/net/Uri;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p18    # LX/1dc;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DRAWABLE:LX/32B;
        .end annotation
    .end param
    .param p19    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .param p20    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .param p21    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .param p22    # LX/1dQ;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p23    # Landroid/net/Uri;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p24    # LX/1dc;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DRAWABLE:LX/32B;
        .end annotation
    .end param
    .param p25    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .param p26    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Ljava/lang/CharSequence;",
            "LX/1dQ;",
            "I",
            "Ljava/lang/CharSequence;",
            "II",
            "LX/1dQ;",
            "Ljava/lang/CharSequence;",
            "II",
            "LX/1dQ;",
            "IZII",
            "Landroid/net/Uri;",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;III",
            "LX/1dQ;",
            "Landroid/net/Uri;",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;III",
            "LX/1dQ;",
            ")",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1426183
    if-eqz p23, :cond_0

    if-eqz p24, :cond_0

    .line 1426184
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Only one of accessoryUri and accessoryDrawable can be used at the same time."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1426185
    :cond_0
    if-eqz p17, :cond_1

    if-eqz p18, :cond_1

    .line 1426186
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Only one of imageUri and imageDrawable can be used at the same time."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1426187
    :cond_1
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static/range {p9 .. p9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1426188
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "You must have a second line in order to present the third line"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1426189
    :cond_2
    move-object/from16 v0, p24

    move-object/from16 v1, p23

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;->a(LX/1De;LX/1dc;Landroid/net/Uri;)LX/1Di;

    move-result-object v3

    .line 1426190
    move-object/from16 v0, p18

    move-object/from16 v1, p17

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;->a(LX/1De;LX/1dc;Landroid/net/Uri;)LX/1Di;

    move-result-object v2

    .line 1426191
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x2

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x2

    invoke-interface {v4, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    move/from16 v0, p13

    invoke-interface {v4, v0}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object v4

    if-nez v2, :cond_3

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v5, 0x0

    invoke-interface {v2, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v2, v5}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, p2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    const v6, 0x7f0b0050

    invoke-static {p1, p4, v6}, Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;->a(LX/1De;II)I

    move-result v6

    invoke-virtual {v5, v6}, LX/1ne;->p(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const/4 v6, 0x2

    const v7, 0x7f0b1536

    invoke-interface {v5, v6, v7}, LX/1Di;->g(II)LX/1Di;

    move-result-object v5

    invoke-interface {v5, p3}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v5

    invoke-interface {v2, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x0

    :goto_1
    invoke-interface {v5, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    invoke-static/range {p9 .. p9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x0

    :goto_2
    invoke-interface {v5, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    if-nez p14, :cond_6

    const/4 v2, 0x0

    :goto_3
    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    if-nez v3, :cond_8

    const/4 v2, 0x0

    :goto_4
    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    return-object v2

    :cond_3
    const v5, 0x7f0b1534

    move/from16 v0, p20

    invoke-static {p1, v0, v5}, Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;->a(LX/1De;II)I

    move-result v5

    invoke-interface {v2, v5}, LX/1Di;->g(I)LX/1Di;

    move-result-object v2

    const v5, 0x7f0b1534

    move/from16 v0, p19

    invoke-static {p1, v0, v5}, Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;->a(LX/1De;II)I

    move-result v5

    invoke-interface {v2, v5}, LX/1Di;->o(I)LX/1Di;

    move-result-object v2

    const/16 v5, 0x8

    const v6, 0x7f0b1536

    move/from16 v0, p21

    invoke-static {p1, v0, v6}, Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;->a(LX/1De;II)I

    move-result v6

    invoke-interface {v2, v5, v6}, LX/1Di;->a(II)LX/1Di;

    move-result-object v2

    move-object/from16 v0, p22

    invoke-interface {v2, v0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    goto/16 :goto_0

    :cond_4
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    const v6, 0x7f0b004e

    invoke-static {p1, p6, v6}, Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;->a(LX/1De;II)I

    move-result v6

    invoke-virtual {v2, v6}, LX/1ne;->p(I)LX/1ne;

    move-result-object v2

    const v6, 0x7f0a010e

    invoke-static {p1, p7, v6}, Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;->b(LX/1De;II)I

    move-result v6

    invoke-virtual {v2, v6}, LX/1ne;->m(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/4 v6, 0x2

    const v7, 0x7f0b1536

    invoke-interface {v2, v6, v7}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    move-object/from16 v0, p8

    invoke-interface {v2, v0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    goto/16 :goto_1

    :cond_5
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    move-object/from16 v0, p9

    invoke-virtual {v2, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    const v6, 0x7f0b004e

    move/from16 v0, p10

    invoke-static {p1, v0, v6}, Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;->a(LX/1De;II)I

    move-result v6

    invoke-virtual {v2, v6}, LX/1ne;->p(I)LX/1ne;

    move-result-object v2

    const v6, 0x7f0a010e

    move/from16 v0, p11

    invoke-static {p1, v0, v6}, Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;->b(LX/1De;II)I

    move-result v6

    invoke-virtual {v2, v6}, LX/1ne;->m(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/4 v6, 0x2

    const v7, 0x7f0b1536

    invoke-interface {v2, v6, v7}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    move-object/from16 v0, p12

    invoke-interface {v2, v0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    goto/16 :goto_2

    :cond_6
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v5, 0x0

    invoke-interface {v2, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v5

    const v6, 0x7f0a015a

    move/from16 v0, p16

    invoke-static {p1, v0, v6}, Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;->b(LX/1De;II)I

    move-result v6

    invoke-virtual {v5, v6}, LX/25Q;->h(I)LX/25Q;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    if-nez p15, :cond_7

    const/16 p15, 0x1

    :cond_7
    move/from16 v0, p15

    invoke-interface {v5, v0}, LX/1Di;->g(I)LX/1Di;

    move-result-object v5

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-interface {v5, v6}, LX/1Di;->b(F)LX/1Di;

    move-result-object v5

    invoke-interface {v2, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    const/4 v5, 0x4

    invoke-interface {v2, v5}, LX/1Dh;->B(I)LX/1Dh;

    move-result-object v2

    goto/16 :goto_3

    :cond_8
    const v2, 0x7f0b1535

    move/from16 v0, p26

    invoke-static {p1, v0, v2}, Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;->a(LX/1De;II)I

    move-result v2

    invoke-interface {v3, v2}, LX/1Di;->g(I)LX/1Di;

    move-result-object v2

    const v3, 0x7f0b1535

    move/from16 v0, p25

    invoke-static {p1, v0, v3}, Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;->a(LX/1De;II)I

    move-result v3

    invoke-interface {v2, v3}, LX/1Di;->o(I)LX/1Di;

    move-result-object v2

    const/16 v3, 0x8

    const v5, 0x7f0b1536

    move/from16 v0, p27

    invoke-static {p1, v0, v5}, Lcom/facebook/components/fb/widget/FbImageBlockComponentSpec;->a(LX/1De;II)I

    move-result v5

    invoke-interface {v2, v3, v5}, LX/1Di;->a(II)LX/1Di;

    move-result-object v2

    move-object/from16 v0, p28

    invoke-interface {v2, v0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    goto/16 :goto_4
.end method
