.class public final Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/8yS;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/common/callercontext/CallerContext;

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:Z

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field public i:I

.field public j:I

.field public k:I

.field public l:I

.field public m:I

.field public final synthetic n:LX/8yS;


# direct methods
.method public constructor <init>(LX/8yS;)V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    .line 1425498
    iput-object p1, p0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->n:LX/8yS;

    .line 1425499
    move-object v0, p1

    .line 1425500
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1425501
    iput v1, p0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->b:I

    .line 1425502
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->c:I

    .line 1425503
    iput v1, p0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->d:I

    .line 1425504
    iput v1, p0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->e:I

    .line 1425505
    iput v1, p0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->f:I

    .line 1425506
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->g:Z

    .line 1425507
    sget-object v0, LX/8yT;->a:Ljava/util/List;

    iput-object v0, p0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->h:Ljava/util/List;

    .line 1425508
    iput v1, p0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->i:I

    .line 1425509
    iput v1, p0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->j:I

    .line 1425510
    iput v1, p0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->k:I

    .line 1425511
    iput v1, p0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->l:I

    .line 1425512
    iput v1, p0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->m:I

    .line 1425513
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1425514
    const-string v0, "FbCentralizedSymmetricFacepileComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1425515
    if-ne p0, p1, :cond_1

    .line 1425516
    :cond_0
    :goto_0
    return v0

    .line 1425517
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1425518
    goto :goto_0

    .line 1425519
    :cond_3
    check-cast p1, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;

    .line 1425520
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1425521
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1425522
    if-eq v2, v3, :cond_0

    .line 1425523
    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p1, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1425524
    goto :goto_0

    .line 1425525
    :cond_5
    iget-object v2, p1, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    if-nez v2, :cond_4

    .line 1425526
    :cond_6
    iget v2, p0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->b:I

    iget v3, p1, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->b:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 1425527
    goto :goto_0

    .line 1425528
    :cond_7
    iget v2, p0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->c:I

    iget v3, p1, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->c:I

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 1425529
    goto :goto_0

    .line 1425530
    :cond_8
    iget v2, p0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->d:I

    iget v3, p1, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->d:I

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 1425531
    goto :goto_0

    .line 1425532
    :cond_9
    iget v2, p0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->e:I

    iget v3, p1, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->e:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 1425533
    goto :goto_0

    .line 1425534
    :cond_a
    iget v2, p0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->f:I

    iget v3, p1, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->f:I

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 1425535
    goto :goto_0

    .line 1425536
    :cond_b
    iget-boolean v2, p0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->g:Z

    iget-boolean v3, p1, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->g:Z

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 1425537
    goto :goto_0

    .line 1425538
    :cond_c
    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->h:Ljava/util/List;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->h:Ljava/util/List;

    iget-object v3, p1, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->h:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 1425539
    goto :goto_0

    .line 1425540
    :cond_e
    iget-object v2, p1, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->h:Ljava/util/List;

    if-nez v2, :cond_d

    .line 1425541
    :cond_f
    iget v2, p0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->i:I

    iget v3, p1, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->i:I

    if-eq v2, v3, :cond_10

    move v0, v1

    .line 1425542
    goto :goto_0

    .line 1425543
    :cond_10
    iget v2, p0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->j:I

    iget v3, p1, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->j:I

    if-eq v2, v3, :cond_11

    move v0, v1

    .line 1425544
    goto :goto_0

    .line 1425545
    :cond_11
    iget v2, p0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->k:I

    iget v3, p1, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->k:I

    if-eq v2, v3, :cond_12

    move v0, v1

    .line 1425546
    goto/16 :goto_0

    .line 1425547
    :cond_12
    iget v2, p0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->l:I

    iget v3, p1, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->l:I

    if-eq v2, v3, :cond_13

    move v0, v1

    .line 1425548
    goto/16 :goto_0

    .line 1425549
    :cond_13
    iget v2, p0, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->m:I

    iget v3, p1, Lcom/facebook/components/fb/widget/FbCentralizedSymmetricFacepileComponent$FbCentralizedSymmetricFacepileComponentImpl;->m:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1425550
    goto/16 :goto_0
.end method
