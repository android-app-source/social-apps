.class public final Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/8yb;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public A:I

.field public B:I

.field public C:LX/1dQ;

.field public a:Ljava/lang/CharSequence;

.field public b:LX/1dQ;

.field public c:I

.field public d:Ljava/lang/CharSequence;

.field public e:I

.field public f:I

.field public g:LX/1dQ;

.field public h:Ljava/lang/CharSequence;

.field public i:I

.field public j:I

.field public k:LX/1dQ;

.field public l:I

.field public m:Z

.field public n:I

.field public o:I

.field public p:Landroid/net/Uri;

.field public q:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public r:I

.field public s:I

.field public t:I

.field public u:LX/1dQ;

.field public v:Lcom/facebook/common/callercontext/CallerContext;

.field public w:LX/4Ab;

.field public x:Landroid/net/Uri;

.field public y:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public z:I


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1426080
    const-string v0, "FbImageBlockComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1426081
    if-ne p0, p1, :cond_1

    .line 1426082
    :cond_0
    :goto_0
    return v0

    .line 1426083
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1426084
    goto :goto_0

    .line 1426085
    :cond_3
    check-cast p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;

    .line 1426086
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1426087
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1426088
    if-eq v2, v3, :cond_0

    .line 1426089
    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->a:Ljava/lang/CharSequence;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->a:Ljava/lang/CharSequence;

    iget-object v3, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->a:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1426090
    goto :goto_0

    .line 1426091
    :cond_5
    iget-object v2, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->a:Ljava/lang/CharSequence;

    if-nez v2, :cond_4

    .line 1426092
    :cond_6
    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->b:LX/1dQ;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->b:LX/1dQ;

    iget-object v3, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->b:LX/1dQ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1426093
    goto :goto_0

    .line 1426094
    :cond_8
    iget-object v2, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->b:LX/1dQ;

    if-nez v2, :cond_7

    .line 1426095
    :cond_9
    iget v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->c:I

    iget v3, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->c:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 1426096
    goto :goto_0

    .line 1426097
    :cond_a
    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->d:Ljava/lang/CharSequence;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->d:Ljava/lang/CharSequence;

    iget-object v3, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->d:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 1426098
    goto :goto_0

    .line 1426099
    :cond_c
    iget-object v2, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->d:Ljava/lang/CharSequence;

    if-nez v2, :cond_b

    .line 1426100
    :cond_d
    iget v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->e:I

    iget v3, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->e:I

    if-eq v2, v3, :cond_e

    move v0, v1

    .line 1426101
    goto :goto_0

    .line 1426102
    :cond_e
    iget v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->f:I

    iget v3, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->f:I

    if-eq v2, v3, :cond_f

    move v0, v1

    .line 1426103
    goto :goto_0

    .line 1426104
    :cond_f
    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->g:LX/1dQ;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->g:LX/1dQ;

    iget-object v3, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->g:LX/1dQ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    .line 1426105
    goto :goto_0

    .line 1426106
    :cond_11
    iget-object v2, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->g:LX/1dQ;

    if-nez v2, :cond_10

    .line 1426107
    :cond_12
    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->h:Ljava/lang/CharSequence;

    if-eqz v2, :cond_14

    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->h:Ljava/lang/CharSequence;

    iget-object v3, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->h:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    :cond_13
    move v0, v1

    .line 1426108
    goto/16 :goto_0

    .line 1426109
    :cond_14
    iget-object v2, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->h:Ljava/lang/CharSequence;

    if-nez v2, :cond_13

    .line 1426110
    :cond_15
    iget v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->i:I

    iget v3, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->i:I

    if-eq v2, v3, :cond_16

    move v0, v1

    .line 1426111
    goto/16 :goto_0

    .line 1426112
    :cond_16
    iget v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->j:I

    iget v3, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->j:I

    if-eq v2, v3, :cond_17

    move v0, v1

    .line 1426113
    goto/16 :goto_0

    .line 1426114
    :cond_17
    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->k:LX/1dQ;

    if-eqz v2, :cond_19

    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->k:LX/1dQ;

    iget-object v3, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->k:LX/1dQ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1a

    :cond_18
    move v0, v1

    .line 1426115
    goto/16 :goto_0

    .line 1426116
    :cond_19
    iget-object v2, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->k:LX/1dQ;

    if-nez v2, :cond_18

    .line 1426117
    :cond_1a
    iget v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->l:I

    iget v3, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->l:I

    if-eq v2, v3, :cond_1b

    move v0, v1

    .line 1426118
    goto/16 :goto_0

    .line 1426119
    :cond_1b
    iget-boolean v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->m:Z

    iget-boolean v3, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->m:Z

    if-eq v2, v3, :cond_1c

    move v0, v1

    .line 1426120
    goto/16 :goto_0

    .line 1426121
    :cond_1c
    iget v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->n:I

    iget v3, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->n:I

    if-eq v2, v3, :cond_1d

    move v0, v1

    .line 1426122
    goto/16 :goto_0

    .line 1426123
    :cond_1d
    iget v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->o:I

    iget v3, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->o:I

    if-eq v2, v3, :cond_1e

    move v0, v1

    .line 1426124
    goto/16 :goto_0

    .line 1426125
    :cond_1e
    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->p:Landroid/net/Uri;

    if-eqz v2, :cond_20

    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->p:Landroid/net/Uri;

    iget-object v3, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->p:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_21

    :cond_1f
    move v0, v1

    .line 1426126
    goto/16 :goto_0

    .line 1426127
    :cond_20
    iget-object v2, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->p:Landroid/net/Uri;

    if-nez v2, :cond_1f

    .line 1426128
    :cond_21
    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->q:LX/1dc;

    if-eqz v2, :cond_23

    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->q:LX/1dc;

    iget-object v3, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->q:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_24

    :cond_22
    move v0, v1

    .line 1426129
    goto/16 :goto_0

    .line 1426130
    :cond_23
    iget-object v2, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->q:LX/1dc;

    if-nez v2, :cond_22

    .line 1426131
    :cond_24
    iget v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->r:I

    iget v3, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->r:I

    if-eq v2, v3, :cond_25

    move v0, v1

    .line 1426132
    goto/16 :goto_0

    .line 1426133
    :cond_25
    iget v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->s:I

    iget v3, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->s:I

    if-eq v2, v3, :cond_26

    move v0, v1

    .line 1426134
    goto/16 :goto_0

    .line 1426135
    :cond_26
    iget v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->t:I

    iget v3, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->t:I

    if-eq v2, v3, :cond_27

    move v0, v1

    .line 1426136
    goto/16 :goto_0

    .line 1426137
    :cond_27
    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->u:LX/1dQ;

    if-eqz v2, :cond_29

    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->u:LX/1dQ;

    iget-object v3, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->u:LX/1dQ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2a

    :cond_28
    move v0, v1

    .line 1426138
    goto/16 :goto_0

    .line 1426139
    :cond_29
    iget-object v2, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->u:LX/1dQ;

    if-nez v2, :cond_28

    .line 1426140
    :cond_2a
    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->v:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_2c

    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->v:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->v:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2d

    :cond_2b
    move v0, v1

    .line 1426141
    goto/16 :goto_0

    .line 1426142
    :cond_2c
    iget-object v2, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->v:Lcom/facebook/common/callercontext/CallerContext;

    if-nez v2, :cond_2b

    .line 1426143
    :cond_2d
    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->w:LX/4Ab;

    if-eqz v2, :cond_2f

    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->w:LX/4Ab;

    iget-object v3, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->w:LX/4Ab;

    invoke-virtual {v2, v3}, LX/4Ab;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_30

    :cond_2e
    move v0, v1

    .line 1426144
    goto/16 :goto_0

    .line 1426145
    :cond_2f
    iget-object v2, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->w:LX/4Ab;

    if-nez v2, :cond_2e

    .line 1426146
    :cond_30
    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->x:Landroid/net/Uri;

    if-eqz v2, :cond_32

    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->x:Landroid/net/Uri;

    iget-object v3, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->x:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_33

    :cond_31
    move v0, v1

    .line 1426147
    goto/16 :goto_0

    .line 1426148
    :cond_32
    iget-object v2, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->x:Landroid/net/Uri;

    if-nez v2, :cond_31

    .line 1426149
    :cond_33
    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->y:LX/1dc;

    if-eqz v2, :cond_35

    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->y:LX/1dc;

    iget-object v3, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->y:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_36

    :cond_34
    move v0, v1

    .line 1426150
    goto/16 :goto_0

    .line 1426151
    :cond_35
    iget-object v2, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->y:LX/1dc;

    if-nez v2, :cond_34

    .line 1426152
    :cond_36
    iget v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->z:I

    iget v3, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->z:I

    if-eq v2, v3, :cond_37

    move v0, v1

    .line 1426153
    goto/16 :goto_0

    .line 1426154
    :cond_37
    iget v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->A:I

    iget v3, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->A:I

    if-eq v2, v3, :cond_38

    move v0, v1

    .line 1426155
    goto/16 :goto_0

    .line 1426156
    :cond_38
    iget v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->B:I

    iget v3, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->B:I

    if-eq v2, v3, :cond_39

    move v0, v1

    .line 1426157
    goto/16 :goto_0

    .line 1426158
    :cond_39
    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->C:LX/1dQ;

    if-eqz v2, :cond_3a

    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->C:LX/1dQ;

    iget-object v3, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->C:LX/1dQ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1426159
    goto/16 :goto_0

    .line 1426160
    :cond_3a
    iget-object v2, p1, Lcom/facebook/components/fb/widget/FbImageBlockComponent$FbImageBlockComponentImpl;->C:LX/1dQ;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
