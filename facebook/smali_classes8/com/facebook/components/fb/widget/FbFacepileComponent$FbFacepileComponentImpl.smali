.class public final Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/8yV;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/common/callercontext/CallerContext;

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:Z

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field public h:I

.field public i:I

.field public j:I

.field public k:I

.field public final synthetic l:LX/8yV;


# direct methods
.method public constructor <init>(LX/8yV;)V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    .line 1425785
    iput-object p1, p0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->l:LX/8yV;

    .line 1425786
    move-object v0, p1

    .line 1425787
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1425788
    iput v1, p0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->b:I

    .line 1425789
    iput v1, p0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->c:I

    .line 1425790
    iput v1, p0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->d:I

    .line 1425791
    iput v1, p0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->e:I

    .line 1425792
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->f:Z

    .line 1425793
    sget-object v0, LX/8yW;->a:Ljava/util/List;

    iput-object v0, p0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->g:Ljava/util/List;

    .line 1425794
    iput v1, p0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->h:I

    .line 1425795
    iput v1, p0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->i:I

    .line 1425796
    iput v1, p0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->j:I

    .line 1425797
    iput v1, p0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->k:I

    .line 1425798
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1425799
    const-string v0, "FbFacepileComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1425800
    if-ne p0, p1, :cond_1

    .line 1425801
    :cond_0
    :goto_0
    return v0

    .line 1425802
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1425803
    goto :goto_0

    .line 1425804
    :cond_3
    check-cast p1, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;

    .line 1425805
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1425806
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1425807
    if-eq v2, v3, :cond_0

    .line 1425808
    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p1, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1425809
    goto :goto_0

    .line 1425810
    :cond_5
    iget-object v2, p1, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    if-nez v2, :cond_4

    .line 1425811
    :cond_6
    iget v2, p0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->b:I

    iget v3, p1, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->b:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 1425812
    goto :goto_0

    .line 1425813
    :cond_7
    iget v2, p0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->c:I

    iget v3, p1, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->c:I

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 1425814
    goto :goto_0

    .line 1425815
    :cond_8
    iget v2, p0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->d:I

    iget v3, p1, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->d:I

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 1425816
    goto :goto_0

    .line 1425817
    :cond_9
    iget v2, p0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->e:I

    iget v3, p1, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->e:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 1425818
    goto :goto_0

    .line 1425819
    :cond_a
    iget-boolean v2, p0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->f:Z

    iget-boolean v3, p1, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->f:Z

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 1425820
    goto :goto_0

    .line 1425821
    :cond_b
    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->g:Ljava/util/List;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->g:Ljava/util/List;

    iget-object v3, p1, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->g:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    :cond_c
    move v0, v1

    .line 1425822
    goto :goto_0

    .line 1425823
    :cond_d
    iget-object v2, p1, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->g:Ljava/util/List;

    if-nez v2, :cond_c

    .line 1425824
    :cond_e
    iget v2, p0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->h:I

    iget v3, p1, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->h:I

    if-eq v2, v3, :cond_f

    move v0, v1

    .line 1425825
    goto :goto_0

    .line 1425826
    :cond_f
    iget v2, p0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->i:I

    iget v3, p1, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->i:I

    if-eq v2, v3, :cond_10

    move v0, v1

    .line 1425827
    goto :goto_0

    .line 1425828
    :cond_10
    iget v2, p0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->j:I

    iget v3, p1, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->j:I

    if-eq v2, v3, :cond_11

    move v0, v1

    .line 1425829
    goto :goto_0

    .line 1425830
    :cond_11
    iget v2, p0, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->k:I

    iget v3, p1, Lcom/facebook/components/fb/widget/FbFacepileComponent$FbFacepileComponentImpl;->k:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1425831
    goto/16 :goto_0
.end method
