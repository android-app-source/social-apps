.class public Lcom/facebook/speech/SpeechRecognition;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field public final a:LX/0Sh;

.field private final b:Ljava/util/concurrent/ExecutorService;

.field public final c:Lcom/facebook/speech/SpeechRecognitionClient;

.field public d:Landroid/media/AudioRecord;

.field public volatile e:Z

.field public final f:Ljava/lang/Object;

.field public g:Z

.field public h:Z

.field public i:D

.field public j:I

.field public k:I

.field public volatile l:Z

.field public m:Z

.field public n:LX/9Ey;

.field public o:Lcom/facebook/speech/Result;

.field public p:Z

.field private q:LX/0if;

.field private final r:Ljava/util/Random;

.field private volatile s:I

.field public t:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Sh;Ljava/util/concurrent/ExecutorService;LX/0if;Lcom/facebook/speech/SpeechRecognitionClient;)V
    .locals 2
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1621247
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1621248
    iput-boolean v1, p0, Lcom/facebook/speech/SpeechRecognition;->e:Z

    .line 1621249
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/facebook/speech/SpeechRecognition;->f:Ljava/lang/Object;

    .line 1621250
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/speech/SpeechRecognition;->g:Z

    .line 1621251
    iput-boolean v1, p0, Lcom/facebook/speech/SpeechRecognition;->l:Z

    .line 1621252
    iput-boolean v1, p0, Lcom/facebook/speech/SpeechRecognition;->m:Z

    .line 1621253
    iput-boolean v1, p0, Lcom/facebook/speech/SpeechRecognition;->p:Z

    .line 1621254
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/facebook/speech/SpeechRecognition;->r:Ljava/util/Random;

    .line 1621255
    iput-object p1, p0, Lcom/facebook/speech/SpeechRecognition;->a:LX/0Sh;

    .line 1621256
    iput-object p2, p0, Lcom/facebook/speech/SpeechRecognition;->b:Ljava/util/concurrent/ExecutorService;

    .line 1621257
    iput-object p3, p0, Lcom/facebook/speech/SpeechRecognition;->q:LX/0if;

    .line 1621258
    iput-object p4, p0, Lcom/facebook/speech/SpeechRecognition;->c:Lcom/facebook/speech/SpeechRecognitionClient;

    .line 1621259
    return-void
.end method

.method public static a$redex0(Lcom/facebook/speech/SpeechRecognition;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 1621260
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/facebook/speech/SpeechRecognition;->a$redex0(Lcom/facebook/speech/SpeechRecognition;Ljava/lang/String;Ljava/lang/String;)V

    .line 1621261
    return-void
.end method

.method public static a$redex0(Lcom/facebook/speech/SpeechRecognition;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1621245
    iget-object v0, p0, Lcom/facebook/speech/SpeechRecognition;->q:LX/0if;

    sget-object v1, LX/0ig;->B:LX/0ih;

    iget v2, p0, Lcom/facebook/speech/SpeechRecognition;->s:I

    int-to-long v2, v2

    move-object v4, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, LX/0if;->a(LX/0ih;JLjava/lang/String;Ljava/lang/String;)V

    .line 1621246
    return-void
.end method

.method public static b([SI)D
    .locals 10

    .prologue
    const-wide/high16 v8, 0x4034000000000000L    # 20.0

    const-wide/16 v4, 0x0

    .line 1621238
    const/4 v0, 0x0

    move-wide v2, v4

    :goto_0
    if-ge v0, p1, :cond_0

    .line 1621239
    aget-short v1, p0, v0

    aget-short v6, p0, v0

    mul-int/2addr v1, v6

    int-to-double v6, v1

    add-double/2addr v2, v6

    .line 1621240
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1621241
    :cond_0
    int-to-double v0, p1

    div-double v0, v2, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    .line 1621242
    div-double/2addr v0, v8

    invoke-static {v0, v1}, Ljava/lang/Math;->log10(D)D

    move-result-wide v0

    mul-double/2addr v0, v8

    .line 1621243
    sub-double/2addr v0, v4

    const-wide/high16 v2, 0x4044000000000000L    # 40.0

    div-double/2addr v0, v2

    .line 1621244
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1621233
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/speech/SpeechRecognition;->i:D

    .line 1621234
    iput-boolean v2, p0, Lcom/facebook/speech/SpeechRecognition;->h:Z

    .line 1621235
    iput v2, p0, Lcom/facebook/speech/SpeechRecognition;->j:I

    .line 1621236
    iput v2, p0, Lcom/facebook/speech/SpeechRecognition;->k:I

    .line 1621237
    return-void
.end method

.method public static c(Lcom/facebook/speech/SpeechRecognition;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1621262
    iget-object v0, p0, Lcom/facebook/speech/SpeechRecognition;->q:LX/0if;

    sget-object v1, LX/0ig;->B:LX/0ih;

    iget v2, p0, Lcom/facebook/speech/SpeechRecognition;->s:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3, p1}, LX/0if;->b(LX/0ih;JLjava/lang/String;)V

    .line 1621263
    return-void
.end method

.method private static e(Lcom/facebook/speech/SpeechRecognition;)V
    .locals 7

    .prologue
    .line 1621226
    iget-object v0, p0, Lcom/facebook/speech/SpeechRecognition;->r:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/speech/SpeechRecognition;->s:I

    .line 1621227
    iget-object v0, p0, Lcom/facebook/speech/SpeechRecognition;->q:LX/0if;

    sget-object v1, LX/0ig;->B:LX/0ih;

    iget v2, p0, Lcom/facebook/speech/SpeechRecognition;->s:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, LX/0if;->a(LX/0ih;J)V

    .line 1621228
    iget-object v0, p0, Lcom/facebook/speech/SpeechRecognition;->q:LX/0if;

    sget-object v1, LX/0ig;->B:LX/0ih;

    iget v2, p0, Lcom/facebook/speech/SpeechRecognition;->s:I

    int-to-long v2, v2

    iget-object v4, p0, Lcom/facebook/speech/SpeechRecognition;->t:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0if;->a(LX/0ih;JLjava/lang/String;)V

    .line 1621229
    iget-object v0, p0, Lcom/facebook/speech/SpeechRecognition;->q:LX/0if;

    sget-object v1, LX/0ig;->B:LX/0ih;

    iget v2, p0, Lcom/facebook/speech/SpeechRecognition;->s:I

    int-to-long v2, v2

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "fbtrace:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/facebook/speech/SpeechRecognition;->c:Lcom/facebook/speech/SpeechRecognitionClient;

    invoke-virtual {v5}, Lcom/facebook/speech/SpeechRecognitionClient;->b()Ljava/lang/String;

    move-result-object v5

    .line 1621230
    const/4 v6, 0x0

    const/16 p0, 0xb

    invoke-virtual {v5, v6, p0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    move-object v5, v6

    .line 1621231
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0if;->a(LX/0ih;JLjava/lang/String;)V

    .line 1621232
    return-void
.end method

.method public static f(Lcom/facebook/speech/SpeechRecognition;)V
    .locals 4

    .prologue
    .line 1621224
    iget-object v0, p0, Lcom/facebook/speech/SpeechRecognition;->q:LX/0if;

    sget-object v1, LX/0ig;->B:LX/0ih;

    iget v2, p0, Lcom/facebook/speech/SpeechRecognition;->s:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, LX/0if;->c(LX/0ih;J)V

    .line 1621225
    return-void
.end method

.method public static g(Lcom/facebook/speech/SpeechRecognition;)V
    .locals 1

    .prologue
    .line 1621218
    const-string v0, "stop_recognition"

    invoke-static {p0, v0}, Lcom/facebook/speech/SpeechRecognition;->c(Lcom/facebook/speech/SpeechRecognition;Ljava/lang/String;)V

    .line 1621219
    iget-boolean v0, p0, Lcom/facebook/speech/SpeechRecognition;->l:Z

    if-eqz v0, :cond_0

    .line 1621220
    invoke-static {p0}, Lcom/facebook/speech/SpeechRecognition;->h(Lcom/facebook/speech/SpeechRecognition;)V

    .line 1621221
    :goto_0
    return-void

    .line 1621222
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/speech/SpeechRecognition;->b()V

    .line 1621223
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/speech/SpeechRecognition;->e:Z

    goto :goto_0
.end method

.method public static h(Lcom/facebook/speech/SpeechRecognition;)V
    .locals 1

    .prologue
    .line 1621214
    iget-boolean v0, p0, Lcom/facebook/speech/SpeechRecognition;->h:Z

    if-eqz v0, :cond_0

    .line 1621215
    iget-object v0, p0, Lcom/facebook/speech/SpeechRecognition;->c:Lcom/facebook/speech/SpeechRecognitionClient;

    invoke-virtual {v0}, Lcom/facebook/speech/SpeechRecognitionClient;->a()V

    .line 1621216
    :cond_0
    invoke-direct {p0}, Lcom/facebook/speech/SpeechRecognition;->c()V

    .line 1621217
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 1621186
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/facebook/speech/SpeechRecognition;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 1621187
    :goto_0
    monitor-exit p0

    return-void

    .line 1621188
    :cond_0
    :try_start_1
    invoke-static {p0}, Lcom/facebook/speech/SpeechRecognition;->e(Lcom/facebook/speech/SpeechRecognition;)V

    .line 1621189
    const/16 v0, 0x3e80

    const/16 v1, 0x10

    const/4 v2, 0x2

    invoke-static {v0, v1, v2}, Landroid/media/AudioRecord;->getMinBufferSize(III)I

    move-result v5

    .line 1621190
    new-instance v0, Landroid/media/AudioRecord;

    const/4 v1, 0x6

    const/16 v2, 0x3e80

    const/16 v3, 0x10

    const/4 v4, 0x2

    mul-int/lit8 v5, v5, 0x5

    invoke-direct/range {v0 .. v5}, Landroid/media/AudioRecord;-><init>(IIIII)V

    iput-object v0, p0, Lcom/facebook/speech/SpeechRecognition;->d:Landroid/media/AudioRecord;

    .line 1621191
    iget-object v0, p0, Lcom/facebook/speech/SpeechRecognition;->d:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->getState()I

    move-result v0

    if-eq v0, v6, :cond_1

    .line 1621192
    iget-object v0, p0, Lcom/facebook/speech/SpeechRecognition;->a:LX/0Sh;

    new-instance v1, Lcom/facebook/speech/SpeechRecognition$1;

    invoke-direct {v1, p0}, Lcom/facebook/speech/SpeechRecognition$1;-><init>(Lcom/facebook/speech/SpeechRecognition;)V

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1621193
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1621194
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/facebook/speech/SpeechRecognition;->d:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->startRecording()V

    .line 1621195
    iget-object v0, p0, Lcom/facebook/speech/SpeechRecognition;->d:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->getRecordingState()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    .line 1621196
    iget-object v0, p0, Lcom/facebook/speech/SpeechRecognition;->a:LX/0Sh;

    new-instance v1, Lcom/facebook/speech/SpeechRecognition$2;

    invoke-direct {v1, p0}, Lcom/facebook/speech/SpeechRecognition$2;-><init>(Lcom/facebook/speech/SpeechRecognition;)V

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1621197
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/speech/SpeechRecognition;->e:Z

    .line 1621198
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/speech/SpeechRecognition;->p:Z

    .line 1621199
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/speech/SpeechRecognition;->o:Lcom/facebook/speech/Result;

    .line 1621200
    invoke-direct {p0}, Lcom/facebook/speech/SpeechRecognition;->c()V

    .line 1621201
    iget-object v0, p0, Lcom/facebook/speech/SpeechRecognition;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/speech/SpeechRecognition$3;

    invoke-direct {v1, p0}, Lcom/facebook/speech/SpeechRecognition$3;-><init>(Lcom/facebook/speech/SpeechRecognition;)V

    const v2, 0x5b858e8

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized b()V
    .locals 3

    .prologue
    .line 1621202
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/facebook/speech/SpeechRecognition;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v0, :cond_0

    .line 1621203
    :goto_0
    monitor-exit p0

    return-void

    .line 1621204
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lcom/facebook/speech/SpeechRecognition;->e:Z

    .line 1621205
    iget-object v1, p0, Lcom/facebook/speech/SpeechRecognition;->f:Ljava/lang/Object;

    monitor-enter v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1621206
    :try_start_2
    invoke-direct {p0}, Lcom/facebook/speech/SpeechRecognition;->c()V

    .line 1621207
    iget-object v0, p0, Lcom/facebook/speech/SpeechRecognition;->d:Landroid/media/AudioRecord;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/speech/SpeechRecognition;->d:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->getState()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 1621208
    iget-object v0, p0, Lcom/facebook/speech/SpeechRecognition;->d:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->stop()V

    .line 1621209
    iget-object v0, p0, Lcom/facebook/speech/SpeechRecognition;->d:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V

    .line 1621210
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/speech/SpeechRecognition;->l:Z

    if-nez v0, :cond_2

    .line 1621211
    iget-object v0, p0, Lcom/facebook/speech/SpeechRecognition;->c:Lcom/facebook/speech/SpeechRecognitionClient;

    invoke-virtual {v0}, Lcom/facebook/speech/SpeechRecognitionClient;->a()V

    .line 1621212
    :cond_2
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1621213
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method
