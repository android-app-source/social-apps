.class public Lcom/facebook/speech/SpeechRecognitionClient;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field private volatile a:Z

.field private final b:LX/1sj;

.field private c:Lcom/facebook/fbtrace/FbTraceNode;

.field private mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1621292
    const-string v0, "shortwaveclient"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 1621293
    return-void
.end method

.method public constructor <init>(LX/1sj;Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1621286
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1621287
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/speech/SpeechRecognitionClient;->a:Z

    .line 1621288
    iput-object p1, p0, Lcom/facebook/speech/SpeechRecognitionClient;->b:LX/1sj;

    .line 1621289
    iget-object v0, p2, Lcom/facebook/auth/viewercontext/ViewerContext;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1621290
    invoke-direct {p0, v0}, Lcom/facebook/speech/SpeechRecognitionClient;->initHybrid(Ljava/lang/String;)Lcom/facebook/jni/HybridData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/speech/SpeechRecognitionClient;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 1621291
    return-void
.end method

.method private native asyncSendAudio([SI)V
.end method

.method public static b(LX/0QB;)Lcom/facebook/speech/SpeechRecognitionClient;
    .locals 3

    .prologue
    .line 1621284
    new-instance v2, Lcom/facebook/speech/SpeechRecognitionClient;

    invoke-static {p0}, LX/1sj;->a(LX/0QB;)LX/1sj;

    move-result-object v0

    check-cast v0, LX/1sj;

    invoke-static {p0}, LX/0eQ;->b(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    check-cast v1, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-direct {v2, v0, v1}, Lcom/facebook/speech/SpeechRecognitionClient;-><init>(LX/1sj;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 1621285
    return-object v2
.end method

.method private native initHybrid(Ljava/lang/String;)Lcom/facebook/jni/HybridData;
.end method

.method private native setNativeDomain(Ljava/lang/String;)V
.end method

.method private native setNativeListener(Lcom/facebook/speech/SpeechRecognitionClient$ClientRecognitionListener;)V
.end method

.method private native setNativeServerSettings(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private native startNativeRecognition()V
.end method

.method private native stopNativeRecognition()V
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1621264
    iget-boolean v0, p0, Lcom/facebook/speech/SpeechRecognitionClient;->a:Z

    if-eqz v0, :cond_0

    .line 1621265
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/speech/SpeechRecognitionClient;->a:Z

    .line 1621266
    invoke-direct {p0}, Lcom/facebook/speech/SpeechRecognitionClient;->stopNativeRecognition()V

    .line 1621267
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/speech/SpeechRecognitionClient$ClientRecognitionListener;)V
    .locals 0

    .prologue
    .line 1621282
    invoke-direct {p0, p1}, Lcom/facebook/speech/SpeechRecognitionClient;->setNativeListener(Lcom/facebook/speech/SpeechRecognitionClient$ClientRecognitionListener;)V

    .line 1621283
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1621278
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1621279
    const-string p1, ""

    .line 1621280
    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/speech/SpeechRecognitionClient;->setNativeDomain(Ljava/lang/String;)V

    .line 1621281
    return-void
.end method

.method public final a([SI)V
    .locals 1

    .prologue
    .line 1621271
    iget-boolean v0, p0, Lcom/facebook/speech/SpeechRecognitionClient;->a:Z

    if-nez v0, :cond_0

    .line 1621272
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/speech/SpeechRecognitionClient;->a:Z

    .line 1621273
    invoke-virtual {p0}, Lcom/facebook/speech/SpeechRecognitionClient;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/speech/SpeechRecognitionClient;->setNativeFbTraceMeta(Ljava/lang/String;)V

    .line 1621274
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/speech/SpeechRecognitionClient;->c:Lcom/facebook/fbtrace/FbTraceNode;

    .line 1621275
    invoke-direct {p0}, Lcom/facebook/speech/SpeechRecognitionClient;->startNativeRecognition()V

    .line 1621276
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/facebook/speech/SpeechRecognitionClient;->asyncSendAudio([SI)V

    .line 1621277
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1621268
    iget-object v0, p0, Lcom/facebook/speech/SpeechRecognitionClient;->c:Lcom/facebook/fbtrace/FbTraceNode;

    if-nez v0, :cond_0

    .line 1621269
    iget-object v0, p0, Lcom/facebook/speech/SpeechRecognitionClient;->b:LX/1sj;

    invoke-virtual {v0}, LX/1sj;->a()Lcom/facebook/fbtrace/FbTraceNode;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/speech/SpeechRecognitionClient;->c:Lcom/facebook/fbtrace/FbTraceNode;

    .line 1621270
    :cond_0
    iget-object v0, p0, Lcom/facebook/speech/SpeechRecognitionClient;->c:Lcom/facebook/fbtrace/FbTraceNode;

    invoke-virtual {v0}, Lcom/facebook/fbtrace/FbTraceNode;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public native setEndOfSpeechDetection(Z)V
.end method

.method public native setNativeFbTraceMeta(Ljava/lang/String;)V
.end method
