.class public Lcom/facebook/speech/Result;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/CharSequence;


# instance fields
.field private a:Ljava/lang/String;

.field private final mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1621065
    const-string v0, "shortwaveclient"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 1621066
    return-void
.end method

.method private constructor <init>(Lcom/facebook/jni/HybridData;)V
    .locals 0

    .prologue
    .line 1621067
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1621068
    iput-object p1, p0, Lcom/facebook/speech/Result;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 1621069
    return-void
.end method

.method private native nativeWrittenForm()Ljava/lang/String;
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1621070
    iget-object v0, p0, Lcom/facebook/speech/Result;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1621071
    invoke-direct {p0}, Lcom/facebook/speech/Result;->nativeWrittenForm()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/speech/Result;->a:Ljava/lang/String;

    .line 1621072
    :cond_0
    iget-object v0, p0, Lcom/facebook/speech/Result;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final charAt(I)C
    .locals 1

    .prologue
    .line 1621073
    invoke-virtual {p0}, Lcom/facebook/speech/Result;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    return v0
.end method

.method public final length()I
    .locals 1

    .prologue
    .line 1621074
    invoke-virtual {p0}, Lcom/facebook/speech/Result;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    return v0
.end method

.method public final subSequence(II)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1621075
    invoke-virtual {p0}, Lcom/facebook/speech/Result;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1621076
    invoke-virtual {p0}, Lcom/facebook/speech/Result;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
