.class public final Lcom/facebook/speech/SpeechRecognition$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/speech/SpeechRecognition;


# direct methods
.method public constructor <init>(Lcom/facebook/speech/SpeechRecognition;)V
    .locals 0

    .prologue
    .line 1621091
    iput-object p1, p0, Lcom/facebook/speech/SpeechRecognition$3;->a:Lcom/facebook/speech/SpeechRecognition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 14

    .prologue
    const/16 v0, 0x400

    const/4 v6, 0x0

    .line 1621092
    new-array v0, v0, [S

    .line 1621093
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/facebook/speech/SpeechRecognition$3;->a:Lcom/facebook/speech/SpeechRecognition;

    iget-object v1, v1, Lcom/facebook/speech/SpeechRecognition;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 1621094
    :try_start_0
    iget-object v2, p0, Lcom/facebook/speech/SpeechRecognition$3;->a:Lcom/facebook/speech/SpeechRecognition;

    iget-boolean v2, v2, Lcom/facebook/speech/SpeechRecognition;->e:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/facebook/speech/SpeechRecognition$3;->a:Lcom/facebook/speech/SpeechRecognition;

    iget-object v2, v2, Lcom/facebook/speech/SpeechRecognition;->d:Landroid/media/AudioRecord;

    invoke-virtual {v2}, Landroid/media/AudioRecord;->getRecordingState()I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_2

    .line 1621095
    :cond_1
    monitor-exit v1

    return-void

    .line 1621096
    :cond_2
    iget-object v2, p0, Lcom/facebook/speech/SpeechRecognition$3;->a:Lcom/facebook/speech/SpeechRecognition;

    iget-object v2, v2, Lcom/facebook/speech/SpeechRecognition;->d:Landroid/media/AudioRecord;

    const/4 v3, 0x0

    const/16 v4, 0x400

    invoke-virtual {v2, v0, v3, v4}, Landroid/media/AudioRecord;->read([SII)I

    move-result v2

    .line 1621097
    if-lez v2, :cond_4

    .line 1621098
    iget-object v3, p0, Lcom/facebook/speech/SpeechRecognition$3;->a:Lcom/facebook/speech/SpeechRecognition;

    iget-boolean v3, v3, Lcom/facebook/speech/SpeechRecognition;->l:Z

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/facebook/speech/SpeechRecognition$3;->a:Lcom/facebook/speech/SpeechRecognition;

    iget-boolean v3, v3, Lcom/facebook/speech/SpeechRecognition;->h:Z

    if-eqz v3, :cond_4

    .line 1621099
    :cond_3
    iget-object v3, p0, Lcom/facebook/speech/SpeechRecognition$3;->a:Lcom/facebook/speech/SpeechRecognition;

    iget-object v3, v3, Lcom/facebook/speech/SpeechRecognition;->c:Lcom/facebook/speech/SpeechRecognitionClient;

    invoke-virtual {v3, v0, v2}, Lcom/facebook/speech/SpeechRecognitionClient;->a([SI)V

    .line 1621100
    :cond_4
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1621101
    if-lez v2, :cond_0

    .line 1621102
    invoke-static {v0, v2}, Lcom/facebook/speech/SpeechRecognition;->b([SI)D

    move-result-wide v4

    .line 1621103
    iget-object v1, p0, Lcom/facebook/speech/SpeechRecognition$3;->a:Lcom/facebook/speech/SpeechRecognition;

    iget-object v1, v1, Lcom/facebook/speech/SpeechRecognition;->a:LX/0Sh;

    new-instance v3, Lcom/facebook/speech/SpeechRecognition$3$1;

    invoke-direct {v3, p0, v4, v5}, Lcom/facebook/speech/SpeechRecognition$3$1;-><init>(Lcom/facebook/speech/SpeechRecognition$3;D)V

    invoke-virtual {v1, v3}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    .line 1621104
    iget-object v1, p0, Lcom/facebook/speech/SpeechRecognition$3;->a:Lcom/facebook/speech/SpeechRecognition;

    iget-boolean v1, v1, Lcom/facebook/speech/SpeechRecognition;->m:Z

    if-eqz v1, :cond_5

    .line 1621105
    iget-object v1, p0, Lcom/facebook/speech/SpeechRecognition$3;->a:Lcom/facebook/speech/SpeechRecognition;

    invoke-static {v1}, Lcom/facebook/speech/SpeechRecognition;->h(Lcom/facebook/speech/SpeechRecognition;)V

    .line 1621106
    iget-object v1, p0, Lcom/facebook/speech/SpeechRecognition$3;->a:Lcom/facebook/speech/SpeechRecognition;

    .line 1621107
    iput-boolean v6, v1, Lcom/facebook/speech/SpeechRecognition;->m:Z

    .line 1621108
    goto :goto_0

    .line 1621109
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1621110
    :cond_5
    iget-object v1, p0, Lcom/facebook/speech/SpeechRecognition$3;->a:Lcom/facebook/speech/SpeechRecognition;

    iget-boolean v1, v1, Lcom/facebook/speech/SpeechRecognition;->g:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/facebook/speech/SpeechRecognition$3;->a:Lcom/facebook/speech/SpeechRecognition;

    iget-boolean v1, v1, Lcom/facebook/speech/SpeechRecognition;->l:Z

    if-eqz v1, :cond_0

    .line 1621111
    :cond_6
    iget-object v1, p0, Lcom/facebook/speech/SpeechRecognition$3;->a:Lcom/facebook/speech/SpeechRecognition;

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1621112
    iget v9, v1, Lcom/facebook/speech/SpeechRecognition;->k:I

    const/4 v10, 0x2

    if-ge v9, v10, :cond_8

    .line 1621113
    iget v8, v1, Lcom/facebook/speech/SpeechRecognition;->k:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v1, Lcom/facebook/speech/SpeechRecognition;->k:I

    .line 1621114
    :cond_7
    :goto_1
    move v1, v7

    .line 1621115
    if-eqz v1, :cond_0

    .line 1621116
    iget-object v1, p0, Lcom/facebook/speech/SpeechRecognition$3;->a:Lcom/facebook/speech/SpeechRecognition;

    iget-object v1, v1, Lcom/facebook/speech/SpeechRecognition;->c:Lcom/facebook/speech/SpeechRecognitionClient;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/speech/SpeechRecognitionClient;->a([SI)V

    .line 1621117
    iget-object v1, p0, Lcom/facebook/speech/SpeechRecognition$3;->a:Lcom/facebook/speech/SpeechRecognition;

    invoke-static {v1}, Lcom/facebook/speech/SpeechRecognition;->g(Lcom/facebook/speech/SpeechRecognition;)V

    goto/16 :goto_0

    .line 1621118
    :cond_8
    iget-wide v9, v1, Lcom/facebook/speech/SpeechRecognition;->i:D

    cmpl-double v9, v4, v9

    if-lez v9, :cond_9

    .line 1621119
    iput-wide v4, v1, Lcom/facebook/speech/SpeechRecognition;->i:D

    .line 1621120
    :cond_9
    iget-boolean v9, v1, Lcom/facebook/speech/SpeechRecognition;->h:Z

    if-nez v9, :cond_a

    iget-wide v9, v1, Lcom/facebook/speech/SpeechRecognition;->i:D

    const-wide v11, 0x3fc3333333333333L    # 0.15

    cmpl-double v9, v9, v11

    if-lez v9, :cond_a

    .line 1621121
    iput-boolean v8, v1, Lcom/facebook/speech/SpeechRecognition;->h:Z

    .line 1621122
    iget-boolean v8, v1, Lcom/facebook/speech/SpeechRecognition;->l:Z

    if-eqz v8, :cond_7

    .line 1621123
    iget-object v8, v1, Lcom/facebook/speech/SpeechRecognition;->c:Lcom/facebook/speech/SpeechRecognitionClient;

    invoke-virtual {v8, v0, v2}, Lcom/facebook/speech/SpeechRecognitionClient;->a([SI)V

    goto :goto_1

    .line 1621124
    :cond_a
    iget-boolean v9, v1, Lcom/facebook/speech/SpeechRecognition;->h:Z

    if-eqz v9, :cond_7

    .line 1621125
    const-wide v9, 0x3fd999999999999aL    # 0.4

    iget-wide v11, v1, Lcom/facebook/speech/SpeechRecognition;->i:D

    mul-double/2addr v9, v11

    cmpg-double v9, v4, v9

    if-gez v9, :cond_b

    .line 1621126
    iget v9, v1, Lcom/facebook/speech/SpeechRecognition;->j:I

    add-int/2addr v9, v2

    iput v9, v1, Lcom/facebook/speech/SpeechRecognition;->j:I

    .line 1621127
    iget v9, v1, Lcom/facebook/speech/SpeechRecognition;->j:I

    int-to-long v9, v9

    const-wide v11, 0x40cf400000000000L    # 16000.0

    invoke-static {v11, v12}, Ljava/lang/Math;->round(D)J

    move-result-wide v11

    cmp-long v9, v9, v11

    if-lez v9, :cond_7

    move v7, v8

    .line 1621128
    goto :goto_1

    .line 1621129
    :cond_b
    iput v7, v1, Lcom/facebook/speech/SpeechRecognition;->j:I

    goto :goto_1
.end method
