.class public final Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$FetchProfilesQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$FetchProfilesQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1408564
    const-class v0, Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$FetchProfilesQueryModel;

    new-instance v1, Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$FetchProfilesQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$FetchProfilesQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1408565
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1408527
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$FetchProfilesQueryModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1408529
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1408530
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x1

    const/4 v3, 0x0

    .line 1408531
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1408532
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 1408533
    if-eqz v2, :cond_0

    .line 1408534
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1408535
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1408536
    :cond_0
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1408537
    if-eqz v2, :cond_1

    .line 1408538
    const-string v2, "friendship_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1408539
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1408540
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1408541
    if-eqz v2, :cond_2

    .line 1408542
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1408543
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1408544
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1408545
    if-eqz v2, :cond_4

    .line 1408546
    const-string v3, "mutual_friends"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1408547
    const/4 v3, 0x0

    .line 1408548
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1408549
    invoke-virtual {v1, v2, v3, v3}, LX/15i;->a(III)I

    move-result v3

    .line 1408550
    if-eqz v3, :cond_3

    .line 1408551
    const-string p0, "count"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1408552
    invoke-virtual {p1, v3}, LX/0nX;->b(I)V

    .line 1408553
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1408554
    :cond_4
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1408555
    if-eqz v2, :cond_5

    .line 1408556
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1408557
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1408558
    :cond_5
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1408559
    if-eqz v2, :cond_6

    .line 1408560
    const-string v3, "profile_picture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1408561
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1408562
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1408563
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1408528
    check-cast p1, Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$FetchProfilesQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$FetchProfilesQueryModel$Serializer;->a(Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$FetchProfilesQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
