.class public final Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1408505
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1408506
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1408503
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1408504
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1408495
    if-nez p1, :cond_0

    .line 1408496
    :goto_0
    return v0

    .line 1408497
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 1408498
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1408499
    :pswitch_0
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 1408500
    const/4 v2, 0x1

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1408501
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 1408502
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x72b2fc5c
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1408494
    new-instance v0, Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1408491
    packed-switch p0, :pswitch_data_0

    .line 1408492
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1408493
    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch -0x72b2fc5c
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1408490
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1408488
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$DraculaImplementation;->b(I)V

    .line 1408489
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1408507
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1408508
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1408509
    :cond_0
    iput-object p1, p0, Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1408510
    iput p2, p0, Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$DraculaImplementation;->b:I

    .line 1408511
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1408487
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1408486
    new-instance v0, Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1408483
    iget v0, p0, LX/1vt;->c:I

    .line 1408484
    move v0, v0

    .line 1408485
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1408480
    iget v0, p0, LX/1vt;->c:I

    .line 1408481
    move v0, v0

    .line 1408482
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1408477
    iget v0, p0, LX/1vt;->b:I

    .line 1408478
    move v0, v0

    .line 1408479
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1408474
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1408475
    move-object v0, v0

    .line 1408476
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1408465
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1408466
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1408467
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1408468
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1408469
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1408470
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1408471
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1408472
    invoke-static {v3, v9, v2}, Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/ufiservices/graphql/FetchProfilesGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1408473
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1408462
    iget v0, p0, LX/1vt;->c:I

    .line 1408463
    move v0, v0

    .line 1408464
    return v0
.end method
