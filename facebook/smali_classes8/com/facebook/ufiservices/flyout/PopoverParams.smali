.class public Lcom/facebook/ufiservices/flyout/PopoverParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ufiservices/flyout/PopoverParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1407070
    new-instance v0, LX/8qN;

    invoke-direct {v0}, LX/8qN;-><init>()V

    sput-object v0, Lcom/facebook/ufiservices/flyout/PopoverParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/8qO;)V
    .locals 1

    .prologue
    .line 1407071
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1407072
    iget-boolean v0, p1, LX/8qO;->a:Z

    iput-boolean v0, p0, Lcom/facebook/ufiservices/flyout/PopoverParams;->a:Z

    .line 1407073
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1407074
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1407075
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ufiservices/flyout/PopoverParams;->a:Z

    .line 1407076
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1407077
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1407078
    iget-boolean v0, p0, Lcom/facebook/ufiservices/flyout/PopoverParams;->a:Z

    move v0, v0

    .line 1407079
    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1407080
    return-void
.end method
