.class public Lcom/facebook/ufiservices/flyout/UFIProfileListFragment;
.super Lcom/facebook/ufiservices/ui/ProfileListFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/8qC;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/ufiservices/ui/ProfileListFragment",
        "<",
        "LX/36L;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "LX/8qC;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:LX/8qM;

.field public c:LX/0hG;

.field private d:Lcom/facebook/ufiservices/flyout/ProfileListParams;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1407638
    const-class v0, Lcom/facebook/ufiservices/flyout/UFIProfileListFragment;

    const-string v1, "story_feedback_flyout"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/ufiservices/flyout/UFIProfileListFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1407656
    invoke-direct {p0}, Lcom/facebook/ufiservices/ui/ProfileListFragment;-><init>()V

    .line 1407657
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/ufiservices/flyout/UFIProfileListFragment;

    invoke-static {p0}, LX/8qM;->a(LX/0QB;)LX/8qM;

    move-result-object p0

    check-cast p0, LX/8qM;

    iput-object p0, p1, Lcom/facebook/ufiservices/flyout/UFIProfileListFragment;->b:LX/8qM;

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;)",
            "Ljava/util/List",
            "<",
            "LX/36L;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1407652
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1407653
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 1407654
    invoke-static {v0}, LX/8qb;->a(Lcom/facebook/graphql/model/GraphQLActor;)LX/36L;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1407655
    :cond_0
    return-object v1
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1407645
    invoke-super {p0, p1}, Lcom/facebook/ufiservices/ui/ProfileListFragment;->a(Landroid/os/Bundle;)V

    .line 1407646
    const-class v0, Lcom/facebook/ufiservices/flyout/UFIProfileListFragment;

    invoke-static {v0, p0}, Lcom/facebook/ufiservices/flyout/UFIProfileListFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1407647
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1407648
    const-string v1, "profileListParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/flyout/ProfileListParams;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/UFIProfileListFragment;->d:Lcom/facebook/ufiservices/flyout/ProfileListParams;

    .line 1407649
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mParentFragment:Landroid/support/v4/app/Fragment;

    move-object v0, v0

    .line 1407650
    check-cast v0, LX/0hG;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/UFIProfileListFragment;->c:LX/0hG;

    .line 1407651
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1407644
    return-void
.end method

.method public final a(FFLX/31M;)Z
    .locals 3

    .prologue
    .line 1407639
    iget-object v0, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->j:Lcom/facebook/widget/listview/BetterListView;

    move-object v0, v0

    .line 1407640
    sget-object v1, LX/8qf;->a:[I

    invoke-virtual {p3}, LX/31M;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1407641
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1407642
    :pswitch_0
    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->isAtBottom()Z

    move-result v0

    goto :goto_0

    .line 1407643
    :pswitch_1
    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->a()Z

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1407637
    const-string v0, "flyout_likers_animation_perf"

    return-object v0
.end method

.method public final gj_()V
    .locals 0

    .prologue
    .line 1407658
    return-void
.end method

.method public final gk_()V
    .locals 0

    .prologue
    .line 1407636
    return-void
.end method

.method public final hu_()Z
    .locals 1

    .prologue
    .line 1407635
    const/4 v0, 0x0

    return v0
.end method

.method public final k()V
    .locals 0

    .prologue
    .line 1407589
    return-void
.end method

.method public final l()Landroid/view/View;
    .locals 1

    .prologue
    .line 1407634
    const/4 v0, 0x0

    return-object v0
.end method

.method public final m()Landroid/widget/BaseAdapter;
    .locals 2

    .prologue
    .line 1407631
    new-instance v0, LX/8rX;

    .line 1407632
    iget-object v1, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->m:LX/55h;

    move-object v1, v1

    .line 1407633
    invoke-direct {v0, v1}, LX/8rX;-><init>(LX/55h;)V

    return-object v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 1407630
    const v0, 0x7f0306c4

    return v0
.end method

.method public final o()Lcom/facebook/common/callercontext/CallerContext;
    .locals 1

    .prologue
    .line 1407629
    sget-object v0, Lcom/facebook/ufiservices/flyout/UFIProfileListFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-object v0
.end method

.method public final onCreateAnimation(IZI)Landroid/view/animation/Animation;
    .locals 4

    .prologue
    .line 1407618
    const/4 v0, 0x0

    .line 1407619
    iget-object v1, p0, Lcom/facebook/ufiservices/flyout/UFIProfileListFragment;->b:LX/8qM;

    .line 1407620
    iget-boolean v2, v1, LX/8qM;->a:Z

    move v1, v2

    .line 1407621
    if-nez v1, :cond_1

    .line 1407622
    new-instance v0, LX/8qd;

    invoke-direct {v0, p0}, LX/8qd;-><init>(Lcom/facebook/ufiservices/flyout/UFIProfileListFragment;)V

    .line 1407623
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1407624
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 1407625
    :goto_1
    return-object v0

    .line 1407626
    :cond_1
    if-eqz p3, :cond_0

    .line 1407627
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 1407628
    :cond_2
    new-instance v1, LX/8qe;

    invoke-direct {v1, p0}, LX/8qe;-><init>(Lcom/facebook/ufiservices/flyout/UFIProfileListFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    goto :goto_1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1407590
    invoke-super {p0, p1, p2}, Lcom/facebook/ufiservices/ui/ProfileListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1407591
    const v0, 0x7f0d124f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;

    .line 1407592
    const v1, 0x7f0d1250

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    .line 1407593
    const v2, 0x7f080fe6

    invoke-virtual {v0, v2}, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;->setHeaderText(I)V

    .line 1407594
    iget-object v2, p0, Lcom/facebook/ufiservices/flyout/UFIProfileListFragment;->d:Lcom/facebook/ufiservices/flyout/ProfileListParams;

    .line 1407595
    iget-boolean p1, v2, Lcom/facebook/ufiservices/flyout/ProfileListParams;->g:Z

    move v2, p1

    .line 1407596
    if-nez v2, :cond_2

    .line 1407597
    new-instance v1, LX/8qc;

    invoke-direct {v1, p0}, LX/8qc;-><init>(Lcom/facebook/ufiservices/flyout/UFIProfileListFragment;)V

    .line 1407598
    invoke-virtual {v0, v1}, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1407599
    invoke-virtual {v0, v4}, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;->setLeftArrowFocusable(Z)V

    .line 1407600
    invoke-virtual {v0, v3}, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;->setLeftArrowVisibility(I)V

    .line 1407601
    invoke-virtual {v0, v4}, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;->setHeaderTextFocusable(Z)V

    .line 1407602
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/UFIProfileListFragment;->d:Lcom/facebook/ufiservices/flyout/ProfileListParams;

    .line 1407603
    iget-object v1, v0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->d:LX/89l;

    move-object v0, v1

    .line 1407604
    sget-object v1, LX/89l;->PROFILES:LX/89l;

    if-ne v0, v1, :cond_1

    .line 1407605
    const v0, 0x7f0d123f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1407606
    :cond_1
    return-void

    .line 1407607
    :cond_2
    iget-object v2, p0, Lcom/facebook/ufiservices/flyout/UFIProfileListFragment;->d:Lcom/facebook/ufiservices/flyout/ProfileListParams;

    .line 1407608
    iget-object v4, v2, Lcom/facebook/ufiservices/flyout/ProfileListParams;->k:Ljava/lang/String;

    move-object v2, v4

    .line 1407609
    if-eqz v2, :cond_3

    .line 1407610
    iget-object v2, p0, Lcom/facebook/ufiservices/flyout/UFIProfileListFragment;->d:Lcom/facebook/ufiservices/flyout/ProfileListParams;

    .line 1407611
    iget-object v4, v2, Lcom/facebook/ufiservices/flyout/ProfileListParams;->k:Ljava/lang/String;

    move-object v2, v4

    .line 1407612
    invoke-virtual {v0, v2}, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;->setHeaderText(Ljava/lang/String;)V

    .line 1407613
    :cond_3
    iget-object v2, p0, Lcom/facebook/ufiservices/flyout/UFIProfileListFragment;->d:Lcom/facebook/ufiservices/flyout/ProfileListParams;

    .line 1407614
    iget-boolean v4, v2, Lcom/facebook/ufiservices/flyout/ProfileListParams;->f:Z

    move v2, v4

    .line 1407615
    if-nez v2, :cond_0

    .line 1407616
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;->setVisibility(I)V

    .line 1407617
    const/4 v0, 0x4

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
