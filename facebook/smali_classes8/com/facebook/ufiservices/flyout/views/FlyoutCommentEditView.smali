.class public Lcom/facebook/ufiservices/flyout/views/FlyoutCommentEditView;
.super Lcom/facebook/ufiservices/ui/CommentEditView;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1408317
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/ufiservices/flyout/views/FlyoutCommentEditView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1408318
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1408335
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/ufiservices/flyout/views/FlyoutCommentEditView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1408336
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1408330
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/ufiservices/ui/CommentEditView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1408331
    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/views/FlyoutCommentEditView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b08c5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1408332
    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/views/FlyoutCommentEditView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b08bc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1408333
    invoke-virtual {p0, v1, v0, v1, v0}, Lcom/facebook/ufiservices/flyout/views/FlyoutCommentEditView;->setPadding(IIII)V

    .line 1408334
    return-void
.end method


# virtual methods
.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 1408319
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1408320
    iget-object v1, p0, Lcom/facebook/ufiservices/ui/CommentEditView;->g:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    move-object v1, v1

    .line 1408321
    invoke-virtual {v1, v0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getHitRect(Landroid/graphics/Rect;)V

    .line 1408322
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1408323
    iget v1, v0, Landroid/graphics/Rect;->left:I

    neg-int v1, v1

    int-to-float v1, v1

    iget v2, v0, Landroid/graphics/Rect;->top:I

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 1408324
    iget-object v1, p0, Lcom/facebook/ufiservices/ui/CommentEditView;->g:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    move-object v1, v1

    .line 1408325
    invoke-virtual {v1, p1}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 1408326
    iget v2, v0, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    invoke-virtual {p1, v2, v0}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 1408327
    if-eqz v1, :cond_0

    .line 1408328
    const/4 v0, 0x1

    .line 1408329
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/ufiservices/ui/CommentEditView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method
