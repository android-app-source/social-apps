.class public Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Landroid/view/View;

.field public c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public d:Landroid/widget/TextView;

.field public e:Landroid/widget/TextView;

.field public f:Landroid/widget/ImageView;

.field public g:LX/01T;

.field public h:LX/2do;

.field public i:LX/8rk;

.field public j:LX/0ad;

.field public k:Z

.field public l:LX/03V;

.field public m:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1408337
    const-class v0, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;

    const-string v1, "story_feedback_flyout"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 1408338
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1408339
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->k:Z

    .line 1408340
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->m:Z

    .line 1408341
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v3

    check-cast v3, LX/01T;

    invoke-static {v0}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v4

    check-cast v4, LX/2do;

    invoke-static {v0}, LX/8rk;->b(LX/0QB;)LX/8rk;

    move-result-object v5

    check-cast v5, LX/8rk;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object p1

    check-cast p1, LX/03V;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    iput-object v3, v2, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->g:LX/01T;

    iput-object v4, v2, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->h:LX/2do;

    iput-object v5, v2, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->i:LX/8rk;

    iput-object p1, v2, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->l:LX/03V;

    iput-object v0, v2, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->j:LX/0ad;

    .line 1408342
    const v0, 0x7f03067c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1408343
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->j:LX/0ad;

    sget-short v1, LX/2ez;->q:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->m:Z

    .line 1408344
    const v0, 0x7f0d11cd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->b:Landroid/view/View;

    .line 1408345
    const v0, 0x7f0d09c2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1408346
    const v0, 0x7f0d11d0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->d:Landroid/widget/TextView;

    .line 1408347
    const v0, 0x7f0d11d2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->e:Landroid/widget/TextView;

    .line 1408348
    const v0, 0x7f0d11d1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->f:Landroid/widget/ImageView;

    .line 1408349
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->i:LX/8rk;

    .line 1408350
    sget-object v1, LX/01T;->FB4A:LX/01T;

    iget-object v2, p0, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->g:LX/01T;

    invoke-virtual {v1, v2}, LX/01T;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, LX/8ry;

    invoke-direct {v1}, LX/8ry;-><init>()V

    :goto_0
    move-object v1, v1

    .line 1408351
    const v2, 0x7f0d11cf

    invoke-virtual {p0, v2}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 1408352
    sget-object v3, LX/01T;->FB4A:LX/01T;

    iget-object v4, p0, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->g:LX/01T;

    invoke-virtual {v3, v4}, LX/01T;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1408353
    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f03067b

    invoke-static {v3, v4, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1408354
    const v2, 0x7f0d11ce

    invoke-virtual {p0, v2}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/friends/ui/SmartButtonLite;

    .line 1408355
    :goto_1
    move-object v2, v2

    .line 1408356
    iget-object v3, p0, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v2, v3}, LX/8rk;->a(LX/8rg;Landroid/view/View;Landroid/widget/TextView;)V

    .line 1408357
    iget-boolean v0, p0, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->m:Z

    if-nez v0, :cond_0

    .line 1408358
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->i:LX/8rk;

    invoke-virtual {v0}, LX/8rk;->a()LX/8rj;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(LX/0b2;)Z

    .line 1408359
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b08b7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->getPaddingRight()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->setPadding(IIII)V

    .line 1408360
    return-void

    :cond_1
    new-instance v1, LX/8rw;

    invoke-direct {v1}, LX/8rw;-><init>()V

    goto :goto_0

    .line 1408361
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f030678

    invoke-static {v3, v4, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1408362
    const v2, 0x7f0d11ce

    invoke-virtual {p0, v2}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/friends/ui/FriendingButton;

    goto :goto_1
.end method

.method private getAllTextViews()[Landroid/widget/TextView;
    .locals 3

    .prologue
    .line 1408363
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/widget/TextView;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->d:Landroid/widget/TextView;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->e:Landroid/widget/TextView;

    aput-object v2, v0, v1

    return-object v0
.end method


# virtual methods
.method public final a(LX/36M;)V
    .locals 10

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 1408364
    if-nez p1, :cond_0

    .line 1408365
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->l:LX/03V;

    const-string v1, "FeedFlyoutLikesAdapter"

    const-string v2, "profile cannot be null"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1408366
    :goto_0
    return-void

    .line 1408367
    :cond_0
    iget-object v3, p0, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->b:Landroid/view/View;

    iget-boolean v0, p0, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->k:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1408368
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1408369
    invoke-interface {p1}, LX/36M;->n()LX/8qg;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, LX/36M;->n()LX/8qg;

    move-result-object v0

    invoke-interface {v0}, LX/8qg;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1408370
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-interface {p1}, LX/36M;->n()LX/8qg;

    move-result-object v1

    invoke-interface {v1}, LX/8qg;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v3, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1408371
    :goto_2
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->d:Landroid/widget/TextView;

    invoke-interface {p1}, LX/36M;->v_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1408372
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->i:LX/8rk;

    sget-object v1, LX/8s1;->NOT_SUPPORTED:LX/8s1;

    iget-boolean v3, p0, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->m:Z

    if-nez v3, :cond_1

    const/4 v2, 0x1

    :cond_1
    const/4 v8, 0x0

    .line 1408373
    move-object v4, v0

    move-object v5, p1

    move-object v6, v1

    move v7, v2

    move-object v9, v8

    invoke-virtual/range {v4 .. v9}, LX/8rk;->a(LX/36N;LX/8s1;ZLcom/facebook/ipc/feed/ViewPermalinkParams;LX/82m;)V

    .line 1408374
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1408375
    goto :goto_1

    .line 1408376
    :cond_3
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    sget-object v3, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_2
.end method

.method public getEventBus()LX/0b4;
    .locals 1

    .prologue
    .line 1408377
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->h:LX/2do;

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x144facc0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1408378
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->onAttachedToWindow()V

    .line 1408379
    iget-object v1, p0, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->i:LX/8rk;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/8rk;->a(Z)V

    .line 1408380
    const/16 v1, 0x2d

    const v2, 0x8e6ecb5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x759530be

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1408381
    iget-object v1, p0, Lcom/facebook/ufiservices/flyout/views/FlyoutLikerView;->i:LX/8rk;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/8rk;->a(Z)V

    .line 1408382
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->onDetachedFromWindow()V

    .line 1408383
    const/16 v1, 0x2d

    const v2, -0x1a4da578

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
