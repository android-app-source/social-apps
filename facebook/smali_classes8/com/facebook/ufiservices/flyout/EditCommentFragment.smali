.class public Lcom/facebook/ufiservices/flyout/EditCommentFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/8qC;


# instance fields
.field public a:LX/0Zb;

.field public b:LX/6Bu;

.field private c:LX/8nK;

.field public d:LX/17V;

.field private e:LX/1CX;

.field private f:LX/1CW;

.field public g:LX/1K9;

.field public h:LX/3H7;

.field public i:LX/20j;

.field private j:LX/1AM;

.field private k:LX/8qM;

.field public l:LX/1Ck;

.field private m:LX/0kL;

.field private n:LX/3iU;

.field private o:LX/3iV;

.field private p:Lcom/facebook/graphql/model/GraphQLComment;

.field private q:LX/8qE;

.field private r:Landroid/view/View;

.field public s:Lcom/facebook/ufiservices/flyout/views/FlyoutCommentEditView;

.field public t:Ljava/lang/String;

.field public u:Lcom/facebook/graphql/model/GraphQLFeedback;

.field private v:Z

.field public w:LX/0hG;

.field private x:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1406724
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1406725
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->x:Landroid/graphics/Rect;

    return-void
.end method

.method private static a(Lcom/facebook/ufiservices/flyout/EditCommentFragment;LX/0Zb;LX/6Bu;LX/8nK;LX/17V;LX/1CX;LX/1CW;LX/1K9;LX/3H7;LX/20j;LX/1AM;LX/8qM;LX/1Ck;LX/0kL;LX/3iU;LX/3iV;)V
    .locals 0
    .param p1    # LX/0Zb;
        .annotation runtime Lcom/facebook/ufiservices/ui/UfiSupportedAttachmentStyle;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1406726
    iput-object p1, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->a:LX/0Zb;

    .line 1406727
    iput-object p2, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->b:LX/6Bu;

    .line 1406728
    iput-object p3, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->c:LX/8nK;

    .line 1406729
    iput-object p4, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->d:LX/17V;

    .line 1406730
    iput-object p5, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->e:LX/1CX;

    .line 1406731
    iput-object p6, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->f:LX/1CW;

    .line 1406732
    iput-object p7, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->g:LX/1K9;

    .line 1406733
    iput-object p8, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->h:LX/3H7;

    .line 1406734
    iput-object p9, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->i:LX/20j;

    .line 1406735
    iput-object p10, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->j:LX/1AM;

    .line 1406736
    iput-object p11, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->k:LX/8qM;

    .line 1406737
    iput-object p12, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->l:LX/1Ck;

    .line 1406738
    iput-object p13, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->m:LX/0kL;

    .line 1406739
    iput-object p14, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->n:LX/3iU;

    .line 1406740
    iput-object p15, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->o:LX/3iV;

    .line 1406741
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 16

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v15

    move-object/from16 v0, p0

    check-cast v0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;

    invoke-static {v15}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-static {v15}, LX/8rE;->a(LX/0QB;)LX/6Bu;

    move-result-object v2

    check-cast v2, LX/6Bu;

    invoke-static {v15}, LX/8nK;->b(LX/0QB;)LX/8nK;

    move-result-object v3

    check-cast v3, LX/8nK;

    invoke-static {v15}, LX/17V;->b(LX/0QB;)LX/17V;

    move-result-object v4

    check-cast v4, LX/17V;

    invoke-static {v15}, LX/1CX;->a(LX/0QB;)LX/1CX;

    move-result-object v5

    check-cast v5, LX/1CX;

    invoke-static {v15}, LX/1CW;->b(LX/0QB;)LX/1CW;

    move-result-object v6

    check-cast v6, LX/1CW;

    invoke-static {v15}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object v7

    check-cast v7, LX/1K9;

    invoke-static {v15}, LX/3H7;->a(LX/0QB;)LX/3H7;

    move-result-object v8

    check-cast v8, LX/3H7;

    invoke-static {v15}, LX/20j;->a(LX/0QB;)LX/20j;

    move-result-object v9

    check-cast v9, LX/20j;

    invoke-static {v15}, LX/1AM;->a(LX/0QB;)LX/1AM;

    move-result-object v10

    check-cast v10, LX/1AM;

    invoke-static {v15}, LX/8qM;->a(LX/0QB;)LX/8qM;

    move-result-object v11

    check-cast v11, LX/8qM;

    invoke-static {v15}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v12

    check-cast v12, LX/1Ck;

    invoke-static {v15}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v13

    check-cast v13, LX/0kL;

    invoke-static {v15}, LX/3iU;->a(LX/0QB;)LX/3iU;

    move-result-object v14

    check-cast v14, LX/3iU;

    invoke-static {v15}, LX/3iV;->a(LX/0QB;)LX/3iV;

    move-result-object v15

    check-cast v15, LX/3iV;

    invoke-static/range {v0 .. v15}, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->a(Lcom/facebook/ufiservices/flyout/EditCommentFragment;LX/0Zb;LX/6Bu;LX/8nK;LX/17V;LX/1CX;LX/1CW;LX/1K9;LX/3H7;LX/20j;LX/1AM;LX/8qM;LX/1Ck;LX/0kL;LX/3iU;LX/3iV;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/ufiservices/flyout/EditCommentFragment;Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1406742
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->f:LX/1CW;

    invoke-virtual {v0, p1, v1, v1}, LX/1CW;->a(Lcom/facebook/fbservice/service/ServiceException;ZZ)Ljava/lang/String;

    move-result-object v0

    .line 1406743
    invoke-static {p1}, LX/1CW;->b(Lcom/facebook/fbservice/service/ServiceException;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1406744
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mHost:LX/0k3;

    move-object v1, v1

    .line 1406745
    if-eqz v1, :cond_0

    .line 1406746
    iget-object v1, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->e:LX/1CX;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2}, LX/4mm;->a(Landroid/content/res/Resources;)LX/4mn;

    move-result-object v2

    sget-object v3, LX/8iJ;->SENTRY_COMMENT_EDIT_BLOCK:LX/8iJ;

    invoke-virtual {v3}, LX/8iJ;->getTitleId()I

    move-result v3

    invoke-virtual {v2, v3}, LX/4mn;->a(I)LX/4mn;

    move-result-object v2

    .line 1406747
    iput-object v0, v2, LX/4mn;->c:Ljava/lang/String;

    .line 1406748
    move-object v0, v2

    .line 1406749
    const v2, 0x7f080054

    invoke-virtual {v0, v2}, LX/4mn;->c(I)LX/4mn;

    move-result-object v0

    sget-object v2, LX/8iK;->a:Landroid/net/Uri;

    .line 1406750
    iput-object v2, v0, LX/4mn;->e:Landroid/net/Uri;

    .line 1406751
    move-object v0, v0

    .line 1406752
    invoke-virtual {v0}, LX/4mn;->l()LX/4mm;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1CX;->a(LX/4mm;)LX/2EJ;

    .line 1406753
    :goto_0
    return-void

    .line 1406754
    :cond_0
    iget-object v1, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->m:LX/0kL;

    new-instance v2, LX/27k;

    invoke-direct {v2, v0}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/ufiservices/flyout/EditCommentFragment;Lcom/facebook/graphql/model/GraphQLComment;Landroid/text/Editable;)V
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 1406775
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->a:LX/0Zb;

    const-string v1, "comment_edit"

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->t:Ljava/lang/String;

    invoke-static {v1, v2, v3}, LX/17V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1406776
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2}, LX/8oj;->a(Landroid/text/Editable;)Ljava/util/List;

    move-result-object v1

    .line 1406777
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1406778
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 1406779
    invoke-static {v0, v1, v2, v3}, LX/16z;->a(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    .line 1406780
    new-instance v4, LX/4W5;

    invoke-direct {v4}, LX/4W5;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->y()Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->y()Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;->a()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 1406781
    :goto_0
    iput v2, v4, LX/4W5;->b:I

    .line 1406782
    move-object v2, v4

    .line 1406783
    invoke-virtual {v2}, LX/4W5;->a()Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    move-result-object v2

    .line 1406784
    invoke-static {p1}, LX/4Vu;->a(Lcom/facebook/graphql/model/GraphQLComment;)LX/4Vu;

    move-result-object v4

    .line 1406785
    iput-object v3, v4, LX/4Vu;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1406786
    move-object v3, v4

    .line 1406787
    iput-object v2, v3, LX/4Vu;->o:Lcom/facebook/graphql/model/GraphQLEditHistoryConnection;

    .line 1406788
    move-object v2, v3

    .line 1406789
    invoke-virtual {v2}, LX/4Vu;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v2

    move-object v5, v2

    .line 1406790
    const/4 v4, 0x0

    .line 1406791
    invoke-static {p1}, LX/36l;->b(Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1406792
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->b:LX/6Bu;

    invoke-static {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-static {p1}, LX/36l;->c(Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6Bu;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    move-result-object v0

    .line 1406793
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-ne v0, v1, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 1406794
    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->j()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1406795
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->j()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v4

    .line 1406796
    :cond_1
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->g:LX/1K9;

    new-instance v1, LX/8q4;

    iget-object v2, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->u:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v5, v2}, LX/8q4;-><init>(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/1K9;->a(LX/1KJ;)V

    .line 1406797
    new-instance v0, Lcom/facebook/api/ufiservices/common/EditCommentParams;

    iget-object v1, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->u:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {p2, v3}, LX/8oj;->a(Landroid/text/Editable;Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct/range {v0 .. v5}, Lcom/facebook/api/ufiservices/common/EditCommentParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1406798
    iget-object v1, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->o:LX/3iV;

    const/4 p2, 0x0

    .line 1406799
    iget-object v2, v1, LX/3iV;->n:LX/0Uh;

    const/16 v3, 0x402

    invoke-virtual {v2, v3, p2}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1406800
    iget-object v2, v1, LX/3iV;->a:LX/0tc;

    const/4 v3, 0x1

    new-array v3, v3, [LX/4VT;

    new-instance v4, LX/6P0;

    .line 1406801
    iget-object v6, v0, Lcom/facebook/api/ufiservices/common/EditCommentParams;->b:Ljava/lang/String;

    move-object v6, v6

    .line 1406802
    iget-object v7, v0, Lcom/facebook/api/ufiservices/common/EditCommentParams;->a:Ljava/lang/String;

    move-object v7, v7

    .line 1406803
    iget-object v8, v0, Lcom/facebook/api/ufiservices/common/EditCommentParams;->e:Lcom/facebook/graphql/model/GraphQLComment;

    move-object v8, v8

    .line 1406804
    invoke-direct {v4, v6, v7, v8}, LX/6P0;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLComment;)V

    aput-object v4, v3, p2

    invoke-virtual {v2, v3}, LX/0tc;->a([LX/4VT;)LX/37X;

    move-result-object v2

    .line 1406805
    :goto_2
    new-instance v3, LX/6PL;

    invoke-direct {v3, v1, v0}, LX/6PL;-><init>(LX/3iV;Lcom/facebook/api/ufiservices/common/EditCommentParams;)V

    invoke-static {v1, v2, v3}, LX/3iV;->a(LX/3iV;LX/37X;Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 1406806
    new-instance v1, LX/8qI;

    invoke-direct {v1, p0, p1, v5}, LX/8qI;-><init>(Lcom/facebook/ufiservices/flyout/EditCommentFragment;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1406807
    iget-object v2, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->l:LX/1Ck;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "edit_comment_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, LX/52F;->a(Ljava/lang/Object;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1406808
    return-void

    .line 1406809
    :cond_2
    const/4 v2, 0x1

    goto/16 :goto_0

    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 1406810
    :cond_4
    iget-object v2, v0, Lcom/facebook/api/ufiservices/common/EditCommentParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1406811
    iget-object v3, v0, Lcom/facebook/api/ufiservices/common/EditCommentParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1406812
    iget-object v4, v0, Lcom/facebook/api/ufiservices/common/EditCommentParams;->e:Lcom/facebook/graphql/model/GraphQLComment;

    move-object v4, v4

    .line 1406813
    new-instance v6, LX/6PH;

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v3, v7, v8

    const/4 v8, 0x1

    aput-object v2, v7, v8

    invoke-direct {v6, v1, v7, v2, v4}, LX/6PH;-><init>(LX/3iV;[Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1406814
    iget-object v7, v1, LX/3iV;->a:LX/0tc;

    invoke-virtual {v7, v6}, LX/0tc;->a(LX/3Bq;)LX/37X;

    move-result-object v6

    move-object v2, v6

    .line 1406815
    goto :goto_2
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1406755
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1406756
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1406757
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v0

    .line 1406758
    const-string v0, "moduleName"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->t:Ljava/lang/String;

    .line 1406759
    const-string v0, "feedback"

    invoke-static {v1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->u:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1406760
    const-string v0, "comment"

    invoke-static {v1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->p:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1406761
    const-string v0, "standalone"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->v:Z

    .line 1406762
    new-instance v0, LX/8qE;

    invoke-direct {v0, p0}, LX/8qE;-><init>(Lcom/facebook/ufiservices/flyout/EditCommentFragment;)V

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->q:LX/8qE;

    .line 1406763
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mParentFragment:Landroid/support/v4/app/Fragment;

    move-object v0, v0

    .line 1406764
    check-cast v0, LX/0hG;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->w:LX/0hG;

    .line 1406765
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1406766
    return-void
.end method

.method public final a(FFLX/31M;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1406767
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->s:Lcom/facebook/ufiservices/flyout/views/FlyoutCommentEditView;

    .line 1406768
    iget-object v2, v0, Lcom/facebook/ufiservices/ui/CommentEditView;->g:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    move-object v0, v2

    .line 1406769
    iget-object v2, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->x:Landroid/graphics/Rect;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1406770
    invoke-virtual {v0}, Landroid/widget/TextView;->getLineCount()I

    move-result v2

    invoke-virtual {v0}, Landroid/widget/TextView;->getLineHeight()I

    move-result v3

    mul-int/2addr v2, v3

    invoke-virtual {v0}, Landroid/widget/TextView;->getHeight()I

    move-result v0

    if-le v2, v0, :cond_0

    const/4 v0, 0x1

    .line 1406771
    :goto_0
    iget-object v2, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->x:Landroid/graphics/Rect;

    float-to-int v3, p1

    float-to-int v4, p2

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    .line 1406772
    :goto_1
    return v1

    :cond_0
    move v0, v1

    .line 1406773
    goto :goto_0

    .line 1406774
    :cond_1
    invoke-virtual {p3}, LX/31M;->isYAxis()Z

    move-result v1

    goto :goto_1
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1406723
    const-string v0, "flyout_comments_edit_animation_perf"

    return-object v0
.end method

.method public final gj_()V
    .locals 0

    .prologue
    .line 1406722
    return-void
.end method

.method public final gk_()V
    .locals 0

    .prologue
    .line 1406674
    return-void
.end method

.method public final hu_()Z
    .locals 1

    .prologue
    .line 1406675
    const/4 v0, 0x0

    return v0
.end method

.method public final k()V
    .locals 0

    .prologue
    .line 1406676
    return-void
.end method

.method public final l()Landroid/view/View;
    .locals 1

    .prologue
    .line 1406677
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onCreateAnimation(IZI)Landroid/view/animation/Animation;
    .locals 4

    .prologue
    .line 1406678
    const/4 v0, 0x0

    .line 1406679
    iget-object v1, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->k:LX/8qM;

    .line 1406680
    iget-boolean v2, v1, LX/8qM;->a:Z

    move v1, v2

    .line 1406681
    if-nez v1, :cond_1

    .line 1406682
    new-instance v0, LX/8qG;

    invoke-direct {v0, p0}, LX/8qG;-><init>(Lcom/facebook/ufiservices/flyout/EditCommentFragment;)V

    .line 1406683
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1406684
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 1406685
    :goto_1
    return-object v0

    .line 1406686
    :cond_1
    if-eqz p3, :cond_0

    .line 1406687
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 1406688
    :cond_2
    new-instance v1, LX/8qH;

    invoke-direct {v1, p0}, LX/8qH;-><init>(Lcom/facebook/ufiservices/flyout/EditCommentFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    goto :goto_1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x283984d6

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1406689
    const v1, 0x7f030675

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->r:Landroid/view/View;

    .line 1406690
    iget-object v1, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->r:Landroid/view/View;

    const/16 v2, 0x2b

    const v3, 0x323e69b6

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1406691
    const v0, 0x7f0d11ca

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/flyout/views/FlyoutCommentEditView;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->s:Lcom/facebook/ufiservices/flyout/views/FlyoutCommentEditView;

    .line 1406692
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->s:Lcom/facebook/ufiservices/flyout/views/FlyoutCommentEditView;

    .line 1406693
    iget-object v1, v0, Lcom/facebook/ufiservices/ui/CommentEditView;->g:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    move-object v0, v1

    .line 1406694
    iget-object v1, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->c:LX/8nK;

    invoke-virtual {v0, v1}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->a(LX/8nB;)V

    .line 1406695
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->s:Lcom/facebook/ufiservices/flyout/views/FlyoutCommentEditView;

    iget-object v1, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->p:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-static {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->q:LX/8qE;

    .line 1406696
    iput-object v1, v0, Lcom/facebook/ufiservices/ui/CommentEditView;->m:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1406697
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1406698
    check-cast v3, Lcom/facebook/graphql/model/GraphQLComment;

    iput-object v3, v0, Lcom/facebook/ufiservices/ui/CommentEditView;->l:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1406699
    iput-object v2, v0, Lcom/facebook/ufiservices/ui/CommentEditView;->n:LX/8qE;

    .line 1406700
    iget-object v3, v0, Lcom/facebook/ufiservices/ui/CommentEditView;->l:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 1406701
    iget-object v3, v0, Lcom/facebook/ufiservices/ui/CommentEditView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object p1, v0, Lcom/facebook/ufiservices/ui/CommentEditView;->l:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    sget-object p2, Lcom/facebook/ufiservices/ui/CommentEditView;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1406702
    :goto_0
    iget-object v3, v0, Lcom/facebook/ufiservices/ui/CommentEditView;->g:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    iget-object p1, v0, Lcom/facebook/ufiservices/ui/CommentEditView;->l:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p1

    iget-object p2, v0, Lcom/facebook/ufiservices/ui/CommentEditView;->b:LX/3iT;

    invoke-static {p1, p2}, LX/8of;->a(LX/175;LX/3iT;)LX/8of;

    move-result-object p1

    invoke-virtual {v3, p1}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1406703
    invoke-static {v0}, Lcom/facebook/ufiservices/ui/CommentEditView;->c(Lcom/facebook/ufiservices/ui/CommentEditView;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1406704
    iget-object v3, v0, Lcom/facebook/ufiservices/ui/CommentEditView;->k:LX/8rR;

    if-nez v3, :cond_0

    .line 1406705
    iget-object v3, v0, Lcom/facebook/ufiservices/ui/CommentEditView;->j:Lcom/facebook/ufiservices/ui/CommentAttachmentViewStub;

    invoke-virtual {v3}, LX/4nv;->a()Landroid/view/View;

    move-result-object v3

    check-cast v3, LX/8rR;

    iput-object v3, v0, Lcom/facebook/ufiservices/ui/CommentEditView;->k:LX/8rR;

    .line 1406706
    :cond_0
    iget-object v3, v0, Lcom/facebook/ufiservices/ui/CommentEditView;->k:LX/8rR;

    const/4 p1, 0x0

    invoke-interface {v3, p1}, LX/8rR;->setVisibility(I)V

    .line 1406707
    iget-object v3, v0, Lcom/facebook/ufiservices/ui/CommentEditView;->k:LX/8rR;

    iget-object p1, v0, Lcom/facebook/ufiservices/ui/CommentEditView;->m:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p2, v0, Lcom/facebook/ufiservices/ui/CommentEditView;->c:LX/6Bu;

    invoke-interface {v3, p1, p2}, LX/8rR;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/6Bu;)V

    .line 1406708
    :cond_1
    :goto_1
    iget-boolean v3, v0, Lcom/facebook/ufiservices/ui/CommentEditView;->o:Z

    if-eqz v3, :cond_5

    .line 1406709
    :goto_2
    const v0, 0x7f0d11c8

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;

    .line 1406710
    const v1, 0x7f080fd1

    invoke-virtual {v0, v1}, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;->setHeaderText(I)V

    .line 1406711
    iget-boolean v1, p0, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->v:Z

    if-nez v1, :cond_2

    .line 1406712
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;->setLeftArrowVisibility(I)V

    .line 1406713
    new-instance v1, LX/8qF;

    invoke-direct {v1, p0}, LX/8qF;-><init>(Lcom/facebook/ufiservices/flyout/EditCommentFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1406714
    :cond_2
    return-void

    .line 1406715
    :cond_3
    iget-object v3, v0, Lcom/facebook/ufiservices/ui/CommentEditView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 p1, 0x0

    sget-object p2, Lcom/facebook/ufiservices/ui/CommentEditView;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_0

    .line 1406716
    :cond_4
    iget-object v3, v0, Lcom/facebook/ufiservices/ui/CommentEditView;->k:LX/8rR;

    if-eqz v3, :cond_1

    .line 1406717
    iget-object v3, v0, Lcom/facebook/ufiservices/ui/CommentEditView;->k:LX/8rR;

    const/16 p1, 0x8

    invoke-interface {v3, p1}, LX/8rR;->setVisibility(I)V

    goto :goto_1

    .line 1406718
    :cond_5
    const/4 v3, 0x1

    iput-boolean v3, v0, Lcom/facebook/ufiservices/ui/CommentEditView;->o:Z

    .line 1406719
    iget-object v3, v0, Lcom/facebook/ufiservices/ui/CommentEditView;->g:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    new-instance p1, LX/8rS;

    invoke-direct {p1, v0}, LX/8rS;-><init>(Lcom/facebook/ufiservices/ui/CommentEditView;)V

    invoke-virtual {v3, p1}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1406720
    iget-object v3, v0, Lcom/facebook/ufiservices/ui/CommentEditView;->h:Landroid/widget/Button;

    new-instance p1, LX/8rT;

    invoke-direct {p1, v0}, LX/8rT;-><init>(Lcom/facebook/ufiservices/ui/CommentEditView;)V

    invoke-virtual {v3, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1406721
    iget-object v3, v0, Lcom/facebook/ufiservices/ui/CommentEditView;->i:Landroid/widget/Button;

    new-instance p1, LX/8rU;

    invoke-direct {p1, v0}, LX/8rU;-><init>(Lcom/facebook/ufiservices/ui/CommentEditView;)V

    invoke-virtual {v3, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2
.end method
