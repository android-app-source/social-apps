.class public Lcom/facebook/ufiservices/flyout/history/CommentEditHistoryFragment;
.super Lcom/facebook/ui/edithistory/EditHistoryFragment;
.source ""

# interfaces
.implements LX/8qC;


# instance fields
.field public c:LX/A7G;

.field public d:LX/8qM;

.field private e:Landroid/view/View;

.field public f:LX/0hG;

.field private g:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1625858
    invoke-direct {p0}, Lcom/facebook/ui/edithistory/EditHistoryFragment;-><init>()V

    .line 1625859
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, Lcom/facebook/ufiservices/flyout/history/CommentEditHistoryFragment;

    new-instance v0, LX/A7G;

    invoke-static {v2}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v1

    check-cast v1, LX/11S;

    invoke-static {v2}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object p0

    check-cast p0, LX/1Uf;

    invoke-direct {v0, v1, p0}, LX/A7G;-><init>(LX/11S;LX/1Uf;)V

    move-object v1, v0

    check-cast v1, LX/A7G;

    invoke-static {v2}, LX/8qM;->a(LX/0QB;)LX/8qM;

    move-result-object v2

    check-cast v2, LX/8qM;

    iput-object v1, p1, Lcom/facebook/ufiservices/flyout/history/CommentEditHistoryFragment;->c:LX/A7G;

    iput-object v2, p1, Lcom/facebook/ufiservices/flyout/history/CommentEditHistoryFragment;->d:LX/8qM;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1625867
    invoke-super {p0, p1}, Lcom/facebook/ui/edithistory/EditHistoryFragment;->a(Landroid/os/Bundle;)V

    .line 1625868
    const-class v0, Lcom/facebook/ufiservices/flyout/history/CommentEditHistoryFragment;

    invoke-static {v0, p0}, Lcom/facebook/ufiservices/flyout/history/CommentEditHistoryFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1625869
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1625870
    const-string v1, "standalone"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ufiservices/flyout/history/CommentEditHistoryFragment;->g:Z

    .line 1625871
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mParentFragment:Landroid/support/v4/app/Fragment;

    move-object v0, v0

    .line 1625872
    check-cast v0, LX/0hG;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/history/CommentEditHistoryFragment;->f:LX/0hG;

    .line 1625873
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1625866
    return-void
.end method

.method public final a(FFLX/31M;)Z
    .locals 3

    .prologue
    .line 1625861
    iget-object v0, p0, Lcom/facebook/ui/edithistory/EditHistoryFragment;->e:Lcom/facebook/widget/listview/BetterListView;

    move-object v0, v0

    .line 1625862
    sget-object v1, LX/A7K;->a:[I

    invoke-virtual {p3}, LX/31M;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1625863
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1625864
    :pswitch_0
    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->isAtBottom()Z

    move-result v0

    goto :goto_0

    .line 1625865
    :pswitch_1
    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->a()Z

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1625860
    const-string v0, "flyout_comments_edit_history_animation_perf"

    return-object v0
.end method

.method public final gj_()V
    .locals 0

    .prologue
    .line 1625833
    return-void
.end method

.method public final gk_()V
    .locals 0

    .prologue
    .line 1625857
    return-void
.end method

.method public final hu_()Z
    .locals 1

    .prologue
    .line 1625874
    const/4 v0, 0x0

    return v0
.end method

.method public final k()V
    .locals 0

    .prologue
    .line 1625856
    return-void
.end method

.method public final l()Landroid/view/View;
    .locals 1

    .prologue
    .line 1625855
    const/4 v0, 0x0

    return-object v0
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 1625854
    const v0, 0x7f030676

    return v0
.end method

.method public final n()LX/A7F;
    .locals 1

    .prologue
    .line 1625853
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/history/CommentEditHistoryFragment;->c:LX/A7G;

    return-object v0
.end method

.method public final onCreateAnimation(IZI)Landroid/view/animation/Animation;
    .locals 4

    .prologue
    .line 1625842
    const/4 v0, 0x0

    .line 1625843
    iget-object v1, p0, Lcom/facebook/ufiservices/flyout/history/CommentEditHistoryFragment;->d:LX/8qM;

    .line 1625844
    iget-boolean v2, v1, LX/8qM;->a:Z

    move v1, v2

    .line 1625845
    if-nez v1, :cond_1

    .line 1625846
    new-instance v0, LX/A7I;

    invoke-direct {v0, p0}, LX/A7I;-><init>(Lcom/facebook/ufiservices/flyout/history/CommentEditHistoryFragment;)V

    .line 1625847
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1625848
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 1625849
    :goto_1
    return-object v0

    .line 1625850
    :cond_1
    if-eqz p3, :cond_0

    .line 1625851
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 1625852
    :cond_2
    new-instance v1, LX/A7J;

    invoke-direct {v1, p0}, LX/A7J;-><init>(Lcom/facebook/ufiservices/flyout/history/CommentEditHistoryFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    goto :goto_1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x29d51aad

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1625840
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/ui/edithistory/EditHistoryFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/ufiservices/flyout/history/CommentEditHistoryFragment;->e:Landroid/view/View;

    .line 1625841
    iget-object v1, p0, Lcom/facebook/ufiservices/flyout/history/CommentEditHistoryFragment;->e:Landroid/view/View;

    const/16 v2, 0x2b

    const v3, 0x22be0325

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1625834
    const v0, 0x7f0d11cb

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;

    .line 1625835
    const v1, 0x7f080fe5

    invoke-virtual {v0, v1}, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;->setHeaderText(I)V

    .line 1625836
    iget-boolean v1, p0, Lcom/facebook/ufiservices/flyout/history/CommentEditHistoryFragment;->g:Z

    if-nez v1, :cond_0

    .line 1625837
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;->setLeftArrowVisibility(I)V

    .line 1625838
    new-instance v1, LX/A7H;

    invoke-direct {v1, p0}, LX/A7H;-><init>(Lcom/facebook/ufiservices/flyout/history/CommentEditHistoryFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1625839
    :cond_0
    return-void
.end method
