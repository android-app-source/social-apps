.class public Lcom/facebook/ufiservices/flyout/ProfileListParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ufiservices/flyout/ProfileListParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/89l;

.field public e:Lcom/facebook/ipc/feed/ViewPermalinkParams;

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:LX/8s1;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field private m:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1407091
    new-instance v0, LX/8qP;

    invoke-direct {v0}, LX/8qP;-><init>()V

    sput-object v0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/8qQ;)V
    .locals 1

    .prologue
    .line 1407092
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1407093
    sget-object v0, LX/89l;->UNKNOWN:LX/89l;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->d:LX/89l;

    .line 1407094
    sget-object v0, LX/8s1;->NOT_SUPPORTED:LX/8s1;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->j:LX/8s1;

    .line 1407095
    iget-object v0, p1, LX/8qQ;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->a:Ljava/lang/String;

    .line 1407096
    iget-object v0, p1, LX/8qQ;->b:Ljava/util/List;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->b:Ljava/util/List;

    .line 1407097
    iget-object v0, p1, LX/8qQ;->c:Ljava/util/List;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->c:Ljava/util/List;

    .line 1407098
    iget-object v0, p1, LX/8qQ;->d:LX/89l;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->d:LX/89l;

    .line 1407099
    iget-object v0, p1, LX/8qQ;->e:Lcom/facebook/ipc/feed/ViewPermalinkParams;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->e:Lcom/facebook/ipc/feed/ViewPermalinkParams;

    .line 1407100
    iget-boolean v0, p1, LX/8qQ;->f:Z

    iput-boolean v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->f:Z

    .line 1407101
    iget-boolean v0, p1, LX/8qQ;->g:Z

    iput-boolean v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->g:Z

    .line 1407102
    iget-boolean v0, p1, LX/8qQ;->h:Z

    iput-boolean v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->h:Z

    .line 1407103
    iget-boolean v0, p1, LX/8qQ;->i:Z

    iput-boolean v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->i:Z

    .line 1407104
    iget-object v0, p1, LX/8qQ;->j:LX/8s1;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->j:LX/8s1;

    .line 1407105
    iget-object v0, p1, LX/8qQ;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->k:Ljava/lang/String;

    .line 1407106
    iget-object v0, p1, LX/8qQ;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->l:Ljava/lang/String;

    .line 1407107
    iget-object v0, p1, LX/8qQ;->m:Ljava/util/HashMap;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->m:Ljava/util/HashMap;

    .line 1407108
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1407109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1407110
    sget-object v0, LX/89l;->UNKNOWN:LX/89l;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->d:LX/89l;

    .line 1407111
    sget-object v0, LX/8s1;->NOT_SUPPORTED:LX/8s1;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->j:LX/8s1;

    .line 1407112
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->a:Ljava/lang/String;

    .line 1407113
    invoke-static {p1}, LX/4By;->b(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->b:Ljava/util/List;

    .line 1407114
    const-class v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->c:Ljava/util/List;

    .line 1407115
    invoke-static {}, LX/89l;->values()[LX/89l;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->d:LX/89l;

    .line 1407116
    const-class v0, Lcom/facebook/ipc/feed/ViewPermalinkParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/feed/ViewPermalinkParams;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->e:Lcom/facebook/ipc/feed/ViewPermalinkParams;

    .line 1407117
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->f:Z

    .line 1407118
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->g:Z

    .line 1407119
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->i:Z

    .line 1407120
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->h:Z

    .line 1407121
    const-class v0, LX/8s1;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8s1;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->j:LX/8s1;

    .line 1407122
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->k:Ljava/lang/String;

    .line 1407123
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->l:Ljava/lang/String;

    .line 1407124
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->m:Ljava/util/HashMap;

    .line 1407125
    return-void
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1407126
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->b:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1407127
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->c:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1407128
    const/4 v0, 0x0

    return v0
.end method

.method public final m()Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 1407129
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1407130
    const-string v1, "profileListParams"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1407131
    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1407132
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1407133
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1407134
    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/ProfileListParams;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Ljava/util/List;)V

    .line 1407135
    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/ProfileListParams;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1407136
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->d:LX/89l;

    move-object v0, v0

    .line 1407137
    invoke-virtual {v0}, LX/89l;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1407138
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->e:Lcom/facebook/ipc/feed/ViewPermalinkParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1407139
    iget-boolean v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->f:Z

    move v0, v0

    .line 1407140
    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1407141
    iget-boolean v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->g:Z

    move v0, v0

    .line 1407142
    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1407143
    iget-boolean v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->h:Z

    move v0, v0

    .line 1407144
    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1407145
    iget-boolean v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->i:Z

    move v0, v0

    .line 1407146
    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1407147
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->j:LX/8s1;

    move-object v0, v0

    .line 1407148
    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1407149
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->k:Ljava/lang/String;

    move-object v0, v0

    .line 1407150
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1407151
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->l:Ljava/lang/String;

    move-object v0, v0

    .line 1407152
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1407153
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->m:Ljava/util/HashMap;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1407154
    return-void
.end method
