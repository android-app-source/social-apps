.class public Lcom/facebook/ufiservices/flyout/FeedbackParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ufiservices/flyout/FeedbackParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public b:Lcom/facebook/graphql/model/GraphQLComment;

.field public c:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:Ljava/lang/Long;

.field public l:Lcom/facebook/ipc/media/MediaItem;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:LX/21y;

.field public p:Z

.field public q:Z

.field public r:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1407024
    new-instance v0, LX/8qK;

    invoke-direct {v0}, LX/8qK;-><init>()V

    sput-object v0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/8qL;)V
    .locals 2

    .prologue
    .line 1407004
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1407005
    iget-object v0, p1, LX/8qL;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1407006
    iget-object v0, p1, LX/8qL;->b:Lcom/facebook/graphql/model/GraphQLComment;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->b:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1407007
    iget-object v0, p1, LX/8qL;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->d:Ljava/lang/String;

    .line 1407008
    iget-object v0, p1, LX/8qL;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->e:Ljava/lang/String;

    .line 1407009
    iget-object v0, p1, LX/8qL;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->f:Ljava/lang/String;

    .line 1407010
    iget-object v0, p1, LX/8qL;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1407011
    iget-boolean v0, p1, LX/8qL;->h:Z

    iput-boolean v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->h:Z

    .line 1407012
    iget-boolean v0, p1, LX/8qL;->i:Z

    iput-boolean v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->i:Z

    .line 1407013
    iget-boolean v0, p1, LX/8qL;->j:Z

    iput-boolean v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->j:Z

    .line 1407014
    iget-wide v0, p1, LX/8qL;->k:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->k:Ljava/lang/Long;

    .line 1407015
    iget-object v0, p1, LX/8qL;->l:Lcom/facebook/ipc/media/MediaItem;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->l:Lcom/facebook/ipc/media/MediaItem;

    .line 1407016
    iget-object v0, p1, LX/8qL;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->m:Ljava/lang/String;

    .line 1407017
    iget-object v0, p1, LX/8qL;->n:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->n:Ljava/lang/String;

    .line 1407018
    iget-object v0, p1, LX/8qL;->o:LX/21y;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->o:LX/21y;

    .line 1407019
    iget-boolean v0, p1, LX/8qL;->p:Z

    iput-boolean v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->p:Z

    .line 1407020
    iget-boolean v0, p1, LX/8qL;->q:Z

    iput-boolean v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->q:Z

    .line 1407021
    iget-boolean v0, p1, LX/8qL;->r:Z

    iput-boolean v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->r:Z

    .line 1407022
    iget-object v0, p1, LX/8qL;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1407023
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1406984
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1406985
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1406986
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->b:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1406987
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->d:Ljava/lang/String;

    .line 1406988
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->e:Ljava/lang/String;

    .line 1406989
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->f:Ljava/lang/String;

    .line 1406990
    const-class v0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1406991
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->h:Z

    .line 1406992
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->i:Z

    .line 1406993
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->j:Z

    .line 1406994
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->k:Ljava/lang/Long;

    .line 1406995
    const-class v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->l:Lcom/facebook/ipc/media/MediaItem;

    .line 1406996
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->m:Ljava/lang/String;

    .line 1406997
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->n:Ljava/lang/String;

    .line 1406998
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/21y;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->o:LX/21y;

    .line 1406999
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->p:Z

    .line 1407000
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->q:Z

    .line 1407001
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->r:Z

    .line 1407002
    const-class v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1407003
    return-void
.end method


# virtual methods
.method public final a(LX/21B;)V
    .locals 1

    .prologue
    .line 1406978
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v0, v0

    .line 1406979
    invoke-static {v0}, LX/21A;->a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)LX/21A;

    move-result-object v0

    .line 1406980
    iput-object p1, v0, LX/21A;->e:LX/21B;

    .line 1406981
    move-object v0, v0

    .line 1406982
    invoke-virtual {v0}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1406983
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1406975
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    .line 1406976
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v0

    .line 1406977
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1407025
    const/4 v0, 0x0

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1406972
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    .line 1406973
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    .line 1406974
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1406891
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    if-nez v0, :cond_0

    .line 1406892
    const-string v0, "unknown"

    .line 1406893
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1406894
    iget-object p0, v0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->c:Ljava/lang/String;

    move-object v0, p0

    .line 1406895
    goto :goto_0
.end method

.method public final u()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1406932
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1406933
    const-string v0, "Feedback Id: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1406934
    const-string v0, "Legacy Api Post Id: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1406935
    const-string v0, "Parent Feedback Id: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1406936
    iget-object v2, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->f:Ljava/lang/String;

    move-object v2, v2

    .line 1406937
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1406938
    const-string v0, "Show Likers: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1406939
    iget-boolean v2, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->j:Z

    move v2, v2

    .line 1406940
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1406941
    const-string v0, "Group Id: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1406942
    iget-object v2, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->k:Ljava/lang/Long;

    move-object v2, v2

    .line 1406943
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1406944
    const-string v0, "Show Keyboard On First Load: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1406945
    iget-boolean v2, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->i:Z

    move v2, v2

    .line 1406946
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1406947
    const-string v0, "Scroll To Bottom On First Load: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1406948
    iget-boolean v2, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->h:Z

    move v2, v2

    .line 1406949
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1406950
    const-string v0, "Focused Comment Id: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1406951
    iget-object v2, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->m:Ljava/lang/String;

    move-object v2, v2

    .line 1406952
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1406953
    const-string v0, "Focused Comment Parent Id: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1406954
    iget-object v2, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->n:Ljava/lang/String;

    move-object v2, v2

    .line 1406955
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1406956
    const-string v0, "Comment Order Type: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1406957
    iget-object v2, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->o:LX/21y;

    move-object v2, v2

    .line 1406958
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1406959
    const-string v0, "Include Comments Disabled Fields: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1406960
    iget-boolean v2, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->p:Z

    move v2, v2

    .line 1406961
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1406962
    const-string v0, "ShouldFetchPrivateReplyContext: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1406963
    iget-boolean v2, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->q:Z

    move v2, v2

    .line 1406964
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1406965
    const-string v0, "IsOptimisticPost: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->r:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1406966
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_0

    .line 1406967
    const-string v0, "DeepStory CacheId: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1406968
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 1406969
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1406970
    :cond_0
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-virtual {v0}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1406971
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final v()Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 1406929
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1406930
    const-string v1, "feedbackParams"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1406931
    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1406896
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    move-object v0, v0

    .line 1406897
    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1406898
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->b:Lcom/facebook/graphql/model/GraphQLComment;

    move-object v0, v0

    .line 1406899
    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1406900
    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1406901
    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/FeedbackParams;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1406902
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1406903
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1406904
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v0, v0

    .line 1406905
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1406906
    iget-boolean v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->h:Z

    move v0, v0

    .line 1406907
    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1406908
    iget-boolean v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->i:Z

    move v0, v0

    .line 1406909
    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1406910
    iget-boolean v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->j:Z

    move v0, v0

    .line 1406911
    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1406912
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->k:Ljava/lang/Long;

    move-object v0, v0

    .line 1406913
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1406914
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->l:Lcom/facebook/ipc/media/MediaItem;

    move-object v0, v0

    .line 1406915
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1406916
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->m:Ljava/lang/String;

    move-object v0, v0

    .line 1406917
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1406918
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->n:Ljava/lang/String;

    move-object v0, v0

    .line 1406919
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1406920
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->o:LX/21y;

    move-object v0, v0

    .line 1406921
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1406922
    iget-boolean v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->p:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1406923
    iget-boolean v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->q:Z

    move v0, v0

    .line 1406924
    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1406925
    iget-boolean v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->r:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1406926
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/FeedbackParams;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 1406927
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1406928
    return-void
.end method
