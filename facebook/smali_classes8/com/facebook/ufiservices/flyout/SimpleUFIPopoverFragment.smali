.class public Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;
.super Lcom/facebook/widget/popover/SimplePopoverFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/0hF;
.implements LX/0hG;


# static fields
.field public static final s:Ljava/lang/String;


# instance fields
.field public n:LX/1AM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/193;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/8qa;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/8qM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/1nJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/8qC;

.field public u:LX/195;

.field private v:LX/8qU;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1407416
    const-class v0, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->s:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1407415
    invoke-direct {p0}, Lcom/facebook/widget/popover/SimplePopoverFragment;-><init>()V

    return-void
.end method

.method private a(LX/8qC;Z)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1407405
    iput-object p1, p0, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->t:LX/8qC;

    .line 1407406
    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->m()LX/8qC;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1407407
    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->m()LX/8qC;

    move-result-object v0

    invoke-interface {v0}, LX/8qC;->k()V

    .line 1407408
    if-eqz p2, :cond_0

    .line 1407409
    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->m()LX/8qC;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->f()Landroid/view/View;

    move-result-object v2

    invoke-interface {v0, v2}, LX/8qC;->a(Landroid/view/View;)V

    .line 1407410
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 1407411
    invoke-direct {p0, v0}, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->b(Landroid/view/View;)V

    .line 1407412
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v2

    if-eqz p2, :cond_2

    const v0, 0x7f040056

    :goto_0
    const v3, 0x7f040092

    const v4, 0x7f040055

    if-eqz p2, :cond_1

    const v1, 0x7f040093

    :cond_1
    invoke-virtual {v2, v0, v3, v4, v1}, LX/0hH;->a(IIII)LX/0hH;

    move-result-object v0

    const v1, 0x7f0d0807

    check-cast p1, Landroid/support/v4/app/Fragment;

    const-string v2, "ufi:popover:content:fragment:tag"

    invoke-virtual {v0, v1, p1, v2}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1407413
    return-void

    :cond_2
    move v0, v1

    .line 1407414
    goto :goto_0
.end method

.method private b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1407402
    invoke-virtual {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1407403
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, LX/2Na;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 1407404
    :cond_0
    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1407396
    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->m()LX/8qC;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->m()LX/8qC;

    move-result-object v0

    invoke-interface {v0}, LX/8qC;->hu_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1407397
    :goto_0
    return v2

    .line 1407398
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->f()I

    move-result v0

    if-le v0, v2, :cond_1

    .line 1407399
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->d()V

    .line 1407400
    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->m()LX/8qC;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->f()Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, LX/8qC;->a(Landroid/view/View;)V

    goto :goto_0

    .line 1407401
    :cond_1
    invoke-super {p0}, Lcom/facebook/widget/popover/SimplePopoverFragment;->S_()Z

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1407395
    const-string v0, "story_feedback_flyout"

    return-object v0
.end method

.method public final a(LX/8qC;)V
    .locals 1

    .prologue
    .line 1407393
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->a(LX/8qC;Z)V

    .line 1407394
    return-void
.end method

.method public final a(Landroid/app/Dialog;)V
    .locals 1

    .prologue
    .line 1407391
    invoke-static {p1}, LX/8qa;->a(Landroid/app/Dialog;)V

    .line 1407392
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1407353
    if-eqz p1, :cond_0

    .line 1407354
    invoke-super {p0, p1}, Lcom/facebook/widget/popover/SimplePopoverFragment;->a(Landroid/view/View;)V

    .line 1407355
    :cond_0
    return-void
.end method

.method public final b()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1407390
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->t:LX/8qC;

    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->m()LX/8qC;

    move-result-object v1

    invoke-static {v0, v1}, LX/8qa;->a(LX/8qC;LX/8qC;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1407384
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 1407385
    invoke-direct {p0, v0}, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->b(Landroid/view/View;)V

    .line 1407386
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->n:LX/1AM;

    new-instance v1, LX/1ZJ;

    invoke-direct {v1}, LX/1ZJ;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1407387
    invoke-super {p0}, Lcom/facebook/widget/popover/SimplePopoverFragment;->d()V

    .line 1407388
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->r:LX/1nJ;

    invoke-virtual {v0}, LX/1nJ;->c()V

    .line 1407389
    return-void
.end method

.method public final eJ_()V
    .locals 1

    .prologue
    .line 1407382
    invoke-virtual {p0}, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->m()LX/8qC;

    move-result-object v0

    invoke-interface {v0}, LX/8qC;->l()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->a(Landroid/view/View;)V

    .line 1407383
    return-void
.end method

.method public final f()Landroid/view/View;
    .locals 2

    .prologue
    .line 1407378
    const v0, 0x7f0d1605

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1407379
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 1407380
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1407381
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 1407375
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->p:LX/8qa;

    if-nez v0, :cond_0

    .line 1407376
    const/4 v0, 0x1

    .line 1407377
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->p:LX/8qa;

    invoke-virtual {v0}, LX/8qa;->a()Z

    move-result v0

    goto :goto_0
.end method

.method public final l()LX/8qU;
    .locals 1

    .prologue
    .line 1407372
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->v:LX/8qU;

    if-nez v0, :cond_0

    .line 1407373
    new-instance v0, LX/8qW;

    invoke-direct {v0, p0}, LX/8qW;-><init>(Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;)V

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->v:LX/8qU;

    .line 1407374
    :cond_0
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->v:LX/8qU;

    return-object v0
.end method

.method public final m()LX/8qC;
    .locals 2

    .prologue
    .line 1407371
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d0807

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, LX/8qC;

    return-object v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0xb513dc9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1407367
    invoke-super {p0, p1}, Lcom/facebook/widget/popover/SimplePopoverFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1407368
    iget-object v1, p0, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->q:LX/8qM;

    const/4 v2, 0x0

    .line 1407369
    iput-boolean v2, v1, LX/8qM;->a:Z

    .line 1407370
    const/16 v1, 0x2b

    const v2, -0x13d812cf

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, -0x5f6e6d7f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1407361
    invoke-super {p0, p1}, Lcom/facebook/widget/popover/SimplePopoverFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1407362
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    move-object v5, p0

    check-cast v5, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;

    invoke-static {v1}, LX/1AM;->a(LX/0QB;)LX/1AM;

    move-result-object v6

    check-cast v6, LX/1AM;

    const-class v7, LX/193;

    invoke-interface {v1, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/193;

    invoke-static {v1}, LX/8qa;->b(LX/0QB;)LX/8qa;

    move-result-object v8

    check-cast v8, LX/8qa;

    invoke-static {v1}, LX/8qM;->a(LX/0QB;)LX/8qM;

    move-result-object p1

    check-cast p1, LX/8qM;

    invoke-static {v1}, LX/1nJ;->a(LX/0QB;)LX/1nJ;

    move-result-object v1

    check-cast v1, LX/1nJ;

    iput-object v6, v5, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->n:LX/1AM;

    iput-object v7, v5, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->o:LX/193;

    iput-object v8, v5, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->p:LX/8qa;

    iput-object p1, v5, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->q:LX/8qM;

    iput-object v1, v5, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->r:LX/1nJ;

    .line 1407363
    iget-object v1, p0, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->t:LX/8qC;

    if-eqz v1, :cond_0

    .line 1407364
    iget-object v1, p0, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->t:LX/8qC;

    invoke-direct {p0, v1, v2}, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->a(LX/8qC;Z)V

    .line 1407365
    iget-object v1, p0, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->o:LX/193;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->t:LX/8qC;

    invoke-interface {v3}, LX/8qC;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/193;->a(Ljava/lang/Boolean;Ljava/lang/String;)LX/195;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->u:LX/195;

    .line 1407366
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x6e469e2b

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4e188cfc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1407356
    invoke-super {p0}, Lcom/facebook/widget/popover/SimplePopoverFragment;->onResume()V

    .line 1407357
    iget-object v1, p0, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->q:LX/8qM;

    const/4 v2, 0x1

    .line 1407358
    iput-boolean v2, v1, LX/8qM;->a:Z

    .line 1407359
    iget-object v1, p0, Lcom/facebook/ufiservices/flyout/SimpleUFIPopoverFragment;->n:LX/1AM;

    new-instance v2, LX/1ZH;

    invoke-direct {v2}, LX/1ZH;-><init>()V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1407360
    const/16 v1, 0x2b

    const v2, -0x6d930401

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
