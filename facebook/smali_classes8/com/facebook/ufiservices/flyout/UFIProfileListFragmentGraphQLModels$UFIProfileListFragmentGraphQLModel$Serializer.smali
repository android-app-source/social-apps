.class public final Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1407818
    const-class v0, Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel;

    new-instance v1, Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1407819
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1407817
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1407775
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1407776
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v3, 0x1

    const/4 p0, 0x0

    .line 1407777
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1407778
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1407779
    if-eqz v2, :cond_0

    .line 1407780
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1407781
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1407782
    :cond_0
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 1407783
    if-eqz v2, :cond_1

    .line 1407784
    const-string v2, "friendship_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1407785
    invoke-virtual {v1, v0, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1407786
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1407787
    if-eqz v2, :cond_2

    .line 1407788
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1407789
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1407790
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1407791
    if-eqz v2, :cond_3

    .line 1407792
    const-string v3, "mutual_friends"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1407793
    invoke-static {v1, v2, p1}, LX/8rr;->a(LX/15i;ILX/0nX;)V

    .line 1407794
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1407795
    if-eqz v2, :cond_4

    .line 1407796
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1407797
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1407798
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1407799
    if-eqz v2, :cond_5

    .line 1407800
    const-string v3, "profile_picture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1407801
    invoke-static {v1, v2, p1}, LX/8qk;->a(LX/15i;ILX/0nX;)V

    .line 1407802
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2, p0}, LX/15i;->a(III)I

    move-result v2

    .line 1407803
    if-eqz v2, :cond_6

    .line 1407804
    const-string v3, "unread_count"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1407805
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1407806
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1407807
    if-eqz v2, :cond_7

    .line 1407808
    const-string v3, "unseen_stories"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1407809
    invoke-static {v1, v2, p1}, LX/8rs;->a(LX/15i;ILX/0nX;)V

    .line 1407810
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1407811
    if-eqz v2, :cond_8

    .line 1407812
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1407813
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1407814
    :cond_8
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1407815
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1407816
    check-cast p1, Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel$Serializer;->a(Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel;LX/0nX;LX/0my;)V

    return-void
.end method
