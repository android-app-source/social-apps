.class public Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/FbImageView;

.field private b:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1407043
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1407044
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1407038
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1407039
    const v0, 0x7f03067f

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1407040
    const v0, 0x7f0d11d7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbImageView;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;->a:Lcom/facebook/widget/FbImageView;

    .line 1407041
    const v0, 0x7f0d11d8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1407042
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1407036
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;->a:Lcom/facebook/widget/FbImageView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/FbImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1407037
    return-void
.end method

.method public setHeaderText(I)V
    .locals 1

    .prologue
    .line 1407026
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1407027
    return-void
.end method

.method public setHeaderText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1407034
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1407035
    return-void
.end method

.method public setHeaderTextFocusable(Z)V
    .locals 1

    .prologue
    .line 1407032
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setFocusable(Z)V

    .line 1407033
    return-void
.end method

.method public setLeftArrowFocusable(Z)V
    .locals 1

    .prologue
    .line 1407030
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;->a:Lcom/facebook/widget/FbImageView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/FbImageView;->setFocusable(Z)V

    .line 1407031
    return-void
.end method

.method public setLeftArrowVisibility(I)V
    .locals 1

    .prologue
    .line 1407028
    iget-object v0, p0, Lcom/facebook/ufiservices/flyout/FlyoutSwitchView;->a:Lcom/facebook/widget/FbImageView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/FbImageView;->setVisibility(I)V

    .line 1407029
    return-void
.end method
