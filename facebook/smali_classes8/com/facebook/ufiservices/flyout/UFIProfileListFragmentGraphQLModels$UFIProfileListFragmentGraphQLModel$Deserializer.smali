.class public final Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1407661
    const-class v0, Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel;

    new-instance v1, Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1407662
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1407663
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 1407664
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1407665
    const/4 v10, 0x0

    .line 1407666
    const/4 v9, 0x0

    .line 1407667
    const/4 v8, 0x0

    .line 1407668
    const/4 v7, 0x0

    .line 1407669
    const/4 v6, 0x0

    .line 1407670
    const/4 v5, 0x0

    .line 1407671
    const/4 v4, 0x0

    .line 1407672
    const/4 v3, 0x0

    .line 1407673
    const/4 v2, 0x0

    .line 1407674
    const/4 v1, 0x0

    .line 1407675
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, p0, :cond_2

    .line 1407676
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1407677
    const/4 v1, 0x0

    .line 1407678
    :goto_0
    move v1, v1

    .line 1407679
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1407680
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1407681
    new-instance v1, Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel;

    invoke-direct {v1}, Lcom/facebook/ufiservices/flyout/UFIProfileListFragmentGraphQLModels$UFIProfileListFragmentGraphQLModel;-><init>()V

    .line 1407682
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1407683
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1407684
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1407685
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1407686
    :cond_0
    return-object v1

    .line 1407687
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1407688
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_c

    .line 1407689
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1407690
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1407691
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v11, :cond_2

    .line 1407692
    const-string p0, "__type__"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_3

    const-string p0, "__typename"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1407693
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v10

    invoke-virtual {v0, v10}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v10

    goto :goto_1

    .line 1407694
    :cond_4
    const-string p0, "friendship_status"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1407695
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    goto :goto_1

    .line 1407696
    :cond_5
    const-string p0, "id"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1407697
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 1407698
    :cond_6
    const-string p0, "mutual_friends"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 1407699
    invoke-static {p1, v0}, LX/8rr;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1407700
    :cond_7
    const-string p0, "name"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 1407701
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 1407702
    :cond_8
    const-string p0, "profile_picture"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 1407703
    invoke-static {p1, v0}, LX/8qk;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 1407704
    :cond_9
    const-string p0, "unread_count"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_a

    .line 1407705
    const/4 v1, 0x1

    .line 1407706
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v4

    goto/16 :goto_1

    .line 1407707
    :cond_a
    const-string p0, "unseen_stories"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_b

    .line 1407708
    invoke-static {p1, v0}, LX/8rs;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 1407709
    :cond_b
    const-string p0, "url"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 1407710
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto/16 :goto_1

    .line 1407711
    :cond_c
    const/16 v11, 0x9

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 1407712
    const/4 v11, 0x0

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 1407713
    const/4 v10, 0x1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 1407714
    const/4 v9, 0x2

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 1407715
    const/4 v8, 0x3

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 1407716
    const/4 v7, 0x4

    invoke-virtual {v0, v7, v6}, LX/186;->b(II)V

    .line 1407717
    const/4 v6, 0x5

    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 1407718
    if-eqz v1, :cond_d

    .line 1407719
    const/4 v1, 0x6

    const/4 v5, 0x0

    invoke-virtual {v0, v1, v4, v5}, LX/186;->a(III)V

    .line 1407720
    :cond_d
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1407721
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1407722
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method
