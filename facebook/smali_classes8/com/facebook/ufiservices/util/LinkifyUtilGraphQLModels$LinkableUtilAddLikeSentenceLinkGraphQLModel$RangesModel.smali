.class public final Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/1eF;
.implements LX/8sJ;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1411e5c5
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel$EntityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I

.field private g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1410243
    const-class v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1410242
    const-class v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1410240
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1410241
    return-void
.end method

.method private k()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel$EntityModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1410238
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel;->e:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel$EntityModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel$EntityModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel$EntityModel;

    iput-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel;->e:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel$EntityModel;

    .line 1410239
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel;->e:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel$EntityModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1410230
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1410231
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel;->k()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel$EntityModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1410232
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1410233
    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1410234
    const/4 v0, 0x1

    iget v1, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel;->f:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1410235
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel;->g:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1410236
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1410237
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1410222
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1410223
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel;->k()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel$EntityModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1410224
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel;->k()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel$EntityModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel$EntityModel;

    .line 1410225
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel;->k()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel$EntityModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1410226
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel;

    .line 1410227
    iput-object v0, v1, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel;->e:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel$EntityModel;

    .line 1410228
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1410229
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()LX/8sI;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1410221
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel;->k()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel$EntityModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1410217
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1410218
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel;->f:I

    .line 1410219
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel;->g:I

    .line 1410220
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 1410244
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1410245
    iget v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel;->f:I

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1410214
    new-instance v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel;

    invoke-direct {v0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel;-><init>()V

    .line 1410215
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1410216
    return-object v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 1410212
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1410213
    iget v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel;->g:I

    return v0
.end method

.method public final synthetic d()LX/1y9;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1410211
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel;->k()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel$EntityModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1410210
    const v0, -0x5f3b9ef5

    return v0
.end method

.method public final synthetic e()LX/1yC;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1410205
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel;->k()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel$EntityModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic eL_()LX/1yH;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1410209
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel;->k()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel$EntityModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic eM_()LX/1yI;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1410208
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel;->k()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel$EntityModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1410207
    const v0, -0x3d10ccb9

    return v0
.end method

.method public final synthetic j()LX/1yK;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1410206
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel;->k()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$RangesModel$EntityModel;

    move-result-object v0

    return-object v0
.end method
