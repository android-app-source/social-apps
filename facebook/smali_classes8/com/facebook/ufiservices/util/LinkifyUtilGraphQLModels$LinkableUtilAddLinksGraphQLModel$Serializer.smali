.class public final Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1410346
    const-class v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;

    new-instance v1, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1410347
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1410345
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1410348
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1410349
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1410350
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1410351
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1410352
    if-eqz v2, :cond_0

    .line 1410353
    const-string p0, "aggregated_ranges"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1410354
    invoke-static {v1, v2, p1, p2}, LX/8sc;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1410355
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1410356
    if-eqz v2, :cond_1

    .line 1410357
    const-string p0, "ranges"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1410358
    invoke-static {v1, v2, p1, p2}, LX/8sZ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1410359
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1410360
    if-eqz v2, :cond_2

    .line 1410361
    const-string p0, "text"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1410362
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1410363
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1410364
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1410344
    check-cast p1, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel$Serializer;->a(Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;LX/0nX;LX/0my;)V

    return-void
.end method
