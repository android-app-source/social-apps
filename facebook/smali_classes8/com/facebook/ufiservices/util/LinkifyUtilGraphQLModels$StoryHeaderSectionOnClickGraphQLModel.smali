.class public final Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;
.implements LX/1yG;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4908e613
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1411007
    const-class v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1411008
    const-class v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1411009
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1411010
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1411011
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1411012
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1411013
    :cond_0
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1411005
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel;->f:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    iput-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel;->f:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    .line 1411006
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel;->f:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1411014
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel;->g:Ljava/lang/String;

    .line 1411015
    iget-object v0, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1410995
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1410996
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1410997
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel;->k()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1410998
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1410999
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1411000
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1411001
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1411002
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1411003
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1411004
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1410987
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1410988
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel;->k()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1410989
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel;->k()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    .line 1410990
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel;->k()Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1410991
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel;

    .line 1410992
    iput-object v0, v1, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel;->f:Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel$AppSectionModel;

    .line 1410993
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1410994
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1410986
    invoke-direct {p0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1410983
    new-instance v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel;

    invoke-direct {v0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$StoryHeaderSectionOnClickGraphQLModel;-><init>()V

    .line 1410984
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1410985
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1410982
    const v0, 0x1726cdfb

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1410981
    const v0, 0x7c02d003

    return v0
.end method
