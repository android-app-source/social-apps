.class public final Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1410047
    const-class v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel;

    new-instance v1, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1410048
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1409987
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 1409988
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1409989
    const/4 v2, 0x0

    .line 1409990
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_8

    .line 1409991
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1409992
    :goto_0
    move v1, v2

    .line 1409993
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1409994
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1409995
    new-instance v1, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel;

    invoke-direct {v1}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel;-><init>()V

    .line 1409996
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1409997
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1409998
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1409999
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1410000
    :cond_0
    return-object v1

    .line 1410001
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1410002
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_7

    .line 1410003
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1410004
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1410005
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_2

    if-eqz v5, :cond_2

    .line 1410006
    const-string v6, "aggregated_ranges"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1410007
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1410008
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v6, :cond_3

    .line 1410009
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v6, :cond_3

    .line 1410010
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1410011
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v8, :cond_f

    .line 1410012
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1410013
    :goto_3
    move v5, v6

    .line 1410014
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1410015
    :cond_3
    invoke-static {v4, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v4

    move v4, v4

    .line 1410016
    goto :goto_1

    .line 1410017
    :cond_4
    const-string v6, "ranges"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1410018
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1410019
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v6, :cond_5

    .line 1410020
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v6, :cond_5

    .line 1410021
    invoke-static {p1, v0}, LX/8sV;->b(LX/15w;LX/186;)I

    move-result v5

    .line 1410022
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1410023
    :cond_5
    invoke-static {v3, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 1410024
    goto :goto_1

    .line 1410025
    :cond_6
    const-string v6, "text"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1410026
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto/16 :goto_1

    .line 1410027
    :cond_7
    const/4 v5, 0x3

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1410028
    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1410029
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1410030
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1410031
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_8
    move v1, v2

    move v3, v2

    move v4, v2

    goto/16 :goto_1

    .line 1410032
    :cond_9
    :goto_5
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_c

    .line 1410033
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1410034
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1410035
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_9

    if-eqz v11, :cond_9

    .line 1410036
    const-string p0, "length"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_a

    .line 1410037
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v8

    move v10, v8

    move v8, v7

    goto :goto_5

    .line 1410038
    :cond_a
    const-string p0, "offset"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_b

    .line 1410039
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v5

    move v9, v5

    move v5, v7

    goto :goto_5

    .line 1410040
    :cond_b
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_5

    .line 1410041
    :cond_c
    const/4 v11, 0x2

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 1410042
    if-eqz v8, :cond_d

    .line 1410043
    invoke-virtual {v0, v6, v10, v6}, LX/186;->a(III)V

    .line 1410044
    :cond_d
    if-eqz v5, :cond_e

    .line 1410045
    invoke-virtual {v0, v7, v9, v6}, LX/186;->a(III)V

    .line 1410046
    :cond_e
    invoke-virtual {v0}, LX/186;->d()I

    move-result v6

    goto/16 :goto_3

    :cond_f
    move v5, v6

    move v8, v6

    move v9, v6

    move v10, v6

    goto :goto_5
.end method
