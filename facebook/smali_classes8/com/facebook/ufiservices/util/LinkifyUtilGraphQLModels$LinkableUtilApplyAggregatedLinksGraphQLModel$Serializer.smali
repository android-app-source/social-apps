.class public final Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1410585
    const-class v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel;

    new-instance v1, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1410586
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1410587
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1410588
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1410589
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1410590
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1410591
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1410592
    if-eqz v2, :cond_0

    .line 1410593
    const-string p0, "aggregated_ranges"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1410594
    invoke-static {v1, v2, p1, p2}, LX/8sc;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1410595
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1410596
    if-eqz v2, :cond_1

    .line 1410597
    const-string p0, "text"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1410598
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1410599
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1410600
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1410601
    check-cast p1, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel$Serializer;->a(Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilApplyAggregatedLinksGraphQLModel;LX/0nX;LX/0my;)V

    return-void
.end method
