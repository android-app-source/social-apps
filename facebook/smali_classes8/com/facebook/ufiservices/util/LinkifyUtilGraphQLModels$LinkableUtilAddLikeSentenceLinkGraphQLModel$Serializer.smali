.class public final Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1410246
    const-class v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel;

    new-instance v1, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1410247
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1410270
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1410249
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1410250
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1410251
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1410252
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1410253
    if-eqz v2, :cond_0

    .line 1410254
    const-string v3, "aggregated_ranges"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1410255
    invoke-static {v1, v2, p1, p2}, LX/8sT;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1410256
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1410257
    if-eqz v2, :cond_2

    .line 1410258
    const-string v3, "ranges"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1410259
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1410260
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_1

    .line 1410261
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1, p2}, LX/8sV;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1410262
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1410263
    :cond_1
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1410264
    :cond_2
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1410265
    if-eqz v2, :cond_3

    .line 1410266
    const-string v3, "text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1410267
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1410268
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1410269
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1410248
    check-cast p1, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel$Serializer;->a(Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLikeSentenceLinkGraphQLModel;LX/0nX;LX/0my;)V

    return-void
.end method
