.class public final Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1409953
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1409954
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1409980
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1409981
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 1409964
    if-nez p1, :cond_0

    .line 1409965
    :goto_0
    return v0

    .line 1409966
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1409967
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1409968
    :sswitch_0
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 1409969
    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(III)I

    move-result v2

    .line 1409970
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1409971
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 1409972
    invoke-virtual {p3, v3, v2, v0}, LX/186;->a(III)V

    .line 1409973
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1409974
    :sswitch_1
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 1409975
    invoke-virtual {p0, p1, v3, v0}, LX/15i;->a(III)I

    move-result v2

    .line 1409976
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1409977
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 1409978
    invoke-virtual {p3, v3, v2, v0}, LX/186;->a(III)V

    .line 1409979
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x4602419e -> :sswitch_1
        0x7f84660e -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1409955
    if-nez p0, :cond_0

    move v0, v1

    .line 1409956
    :goto_0
    return v0

    .line 1409957
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 1409958
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1409959
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1409960
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 1409961
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1409962
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1409963
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 1409913
    const/4 v7, 0x0

    .line 1409914
    const/4 v1, 0x0

    .line 1409915
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 1409916
    invoke-static {v2, v3, v0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1409917
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 1409918
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1409919
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1409952
    new-instance v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1409949
    sparse-switch p0, :sswitch_data_0

    .line 1409950
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1409951
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x4602419e -> :sswitch_0
        0x7f84660e -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1409948
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1409946
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$DraculaImplementation;->b(I)V

    .line 1409947
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1409982
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1409983
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1409984
    :cond_0
    iput-object p1, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1409985
    iput p2, p0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$DraculaImplementation;->b:I

    .line 1409986
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1409945
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1409944
    new-instance v0, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1409941
    iget v0, p0, LX/1vt;->c:I

    .line 1409942
    move v0, v0

    .line 1409943
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1409938
    iget v0, p0, LX/1vt;->c:I

    .line 1409939
    move v0, v0

    .line 1409940
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1409935
    iget v0, p0, LX/1vt;->b:I

    .line 1409936
    move v0, v0

    .line 1409937
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1409932
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1409933
    move-object v0, v0

    .line 1409934
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1409923
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1409924
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1409925
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1409926
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1409927
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1409928
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1409929
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1409930
    invoke-static {v3, v9, v2}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1409931
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1409920
    iget v0, p0, LX/1vt;->c:I

    .line 1409921
    move v0, v0

    .line 1409922
    return v0
.end method
