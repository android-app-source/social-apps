.class public abstract Lcom/facebook/ufiservices/ui/CommentEditView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final d:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:Landroid/view/inputmethod/InputMethodManager;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/3iT;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/6Bu;
    .annotation runtime Lcom/facebook/ufiservices/ui/UfiSupportedAttachmentStyle;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private final e:Landroid/content/Context;

.field public final f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final g:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

.field public final h:Landroid/widget/Button;

.field public final i:Landroid/widget/Button;

.field public final j:Lcom/facebook/ufiservices/ui/CommentAttachmentViewStub;

.field public k:LX/8rR;

.field public l:Lcom/facebook/graphql/model/GraphQLComment;

.field public m:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/8qE;

.field public o:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1408294
    const-class v0, Lcom/facebook/ufiservices/ui/CommentEditView;

    const-string v1, "story_feedback_flyout"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/ufiservices/ui/CommentEditView;->d:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1408295
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/ufiservices/ui/CommentEditView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1408296
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1408297
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/ufiservices/ui/CommentEditView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1408298
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1408299
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1408300
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/ufiservices/ui/CommentEditView;->o:Z

    .line 1408301
    const v0, 0x7f0302ca

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1408302
    iput-object p1, p0, Lcom/facebook/ufiservices/ui/CommentEditView;->e:Landroid/content/Context;

    .line 1408303
    const-class v0, Lcom/facebook/ufiservices/ui/CommentEditView;

    invoke-static {v0, p0}, Lcom/facebook/ufiservices/ui/CommentEditView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1408304
    const v0, 0x7f0d09c2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/ufiservices/ui/CommentEditView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1408305
    const v0, 0x7f0d09c3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    iput-object v0, p0, Lcom/facebook/ufiservices/ui/CommentEditView;->g:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    .line 1408306
    const v0, 0x7f0d09c5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/ufiservices/ui/CommentEditView;->h:Landroid/widget/Button;

    .line 1408307
    const v0, 0x7f0d09c4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/ufiservices/ui/CommentEditView;->i:Landroid/widget/Button;

    .line 1408308
    const v0, 0x7f0d09c6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/ui/CommentAttachmentViewStub;

    iput-object v0, p0, Lcom/facebook/ufiservices/ui/CommentEditView;->j:Lcom/facebook/ufiservices/ui/CommentAttachmentViewStub;

    .line 1408309
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/ufiservices/ui/CommentEditView;

    invoke-static {p0}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    invoke-static {p0}, LX/3iT;->b(LX/0QB;)LX/3iT;

    move-result-object v2

    check-cast v2, LX/3iT;

    invoke-static {p0}, LX/8rE;->a(LX/0QB;)LX/6Bu;

    move-result-object p0

    check-cast p0, LX/6Bu;

    iput-object v1, p1, Lcom/facebook/ufiservices/ui/CommentEditView;->a:Landroid/view/inputmethod/InputMethodManager;

    iput-object v2, p1, Lcom/facebook/ufiservices/ui/CommentEditView;->b:LX/3iT;

    iput-object p0, p1, Lcom/facebook/ufiservices/ui/CommentEditView;->c:LX/6Bu;

    return-void
.end method

.method public static c(Lcom/facebook/ufiservices/ui/CommentEditView;)Z
    .locals 3

    .prologue
    .line 1408310
    iget-object v0, p0, Lcom/facebook/ufiservices/ui/CommentEditView;->l:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-static {v0}, LX/36l;->b(Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/ufiservices/ui/CommentEditView;->c:LX/6Bu;

    iget-object v1, p0, Lcom/facebook/ufiservices/ui/CommentEditView;->m:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p0, Lcom/facebook/ufiservices/ui/CommentEditView;->l:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-static {v2}, LX/36l;->c(Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6Bu;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1408311
    iget-object v0, p0, Lcom/facebook/ufiservices/ui/CommentEditView;->g:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->clearFocus()V

    .line 1408312
    iget-object v0, p0, Lcom/facebook/ufiservices/ui/CommentEditView;->a:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p0}, Lcom/facebook/ufiservices/ui/CommentEditView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1408313
    iget-object v0, p0, Lcom/facebook/ufiservices/ui/CommentEditView;->n:LX/8qE;

    if-eqz v0, :cond_0

    .line 1408314
    iget-object v0, p0, Lcom/facebook/ufiservices/ui/CommentEditView;->n:LX/8qE;

    .line 1408315
    iget-object v1, v0, LX/8qE;->a:Lcom/facebook/ufiservices/flyout/EditCommentFragment;

    iget-object v1, v1, Lcom/facebook/ufiservices/flyout/EditCommentFragment;->w:LX/0hG;

    invoke-interface {v1}, LX/0hG;->S_()Z

    .line 1408316
    :cond_0
    return-void
.end method
