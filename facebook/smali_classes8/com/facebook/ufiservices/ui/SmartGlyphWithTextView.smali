.class public Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;
.super Lcom/facebook/fbui/widget/text/GlyphWithTextView;
.source ""


# instance fields
.field private a:Ljava/lang/CharSequence;

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1409691
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;-><init>(Landroid/content/Context;)V

    .line 1409692
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1409689
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1409690
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1409687
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1409688
    return-void
.end method

.method private a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1409682
    invoke-virtual {p0}, Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;->getLayout()Landroid/text/Layout;

    move-result-object v1

    .line 1409683
    if-nez v1, :cond_1

    .line 1409684
    :cond_0
    :goto_0
    return v0

    .line 1409685
    :cond_1
    invoke-virtual {v1}, Landroid/text/Layout;->getLineCount()I

    move-result v2

    .line 1409686
    if-lez v2, :cond_0

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Landroid/text/Layout;->getEllipsisCount(I)I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 1409667
    invoke-virtual {p0, p1}, Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1409668
    iput-object p2, p0, Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;->a:Ljava/lang/CharSequence;

    .line 1409669
    invoke-virtual {p0}, Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;->requestLayout()V

    .line 1409670
    return-void
.end method

.method public final canScrollHorizontally(I)Z
    .locals 1

    .prologue
    .line 1409681
    const/4 v0, 0x0

    return v0
.end method

.method public final onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 1409675
    invoke-super/range {p0 .. p5}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->onLayout(ZIIII)V

    .line 1409676
    iget-object v0, p0, Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;->a:Ljava/lang/CharSequence;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;->a:Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1409677
    :cond_0
    :goto_0
    return-void

    .line 1409678
    :cond_1
    invoke-direct {p0}, Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1409679
    iget-object v0, p0, Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;->a:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1409680
    iget v0, p0, Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;->b:I

    iget v1, p0, Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;->c:I

    invoke-virtual {p0, v0, v1}, Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;->measure(II)V

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x44868606

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1409671
    invoke-super {p0, p1, p2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->onMeasure(II)V

    .line 1409672
    iput p1, p0, Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;->b:I

    .line 1409673
    iput p2, p0, Lcom/facebook/ufiservices/ui/SmartGlyphWithTextView;->c:I

    .line 1409674
    const/16 v1, 0x2d

    const v2, -0x2e56c947

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
