.class public abstract Lcom/facebook/ufiservices/ui/ProfileListFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LX/2vI;",
        ">",
        "Lcom/facebook/base/fragment/FbFragment;"
    }
.end annotation


# instance fields
.field private final a:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<TT;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1CX;

.field public c:LX/1CW;

.field public d:LX/03V;

.field public e:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field public f:LX/1nG;

.field public g:LX/8pr;

.field public h:LX/0ad;

.field private i:Lcom/facebook/ufiservices/flyout/ProfileListParams;

.field public j:Lcom/facebook/widget/listview/BetterListView;

.field public k:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public l:Landroid/widget/BaseAdapter;

.field public m:LX/55h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/55h",
            "<TT;>;"
        }
    .end annotation
.end field

.field public n:LX/8re;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/ufiservices/ui/ProfileListFragment",
            "<TT;>.RetryTrigger;"
        }
    .end annotation
.end field

.field private o:Landroid/view/View;

.field private p:Landroid/widget/AbsListView$OnScrollListener;

.field private q:LX/8pe;

.field public r:Z

.field public s:Ljava/lang/String;

.field private t:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1407565
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1407566
    new-instance v0, LX/8rZ;

    invoke-direct {v0, p0}, LX/8rZ;-><init>(Lcom/facebook/ufiservices/ui/ProfileListFragment;)V

    iput-object v0, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->a:LX/0QK;

    .line 1407567
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->t:Z

    .line 1407568
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    move-object v1, p1

    check-cast v1, Lcom/facebook/ufiservices/ui/ProfileListFragment;

    invoke-static {p0}, LX/1CX;->a(LX/0QB;)LX/1CX;

    move-result-object v2

    check-cast v2, LX/1CX;

    invoke-static {p0}, LX/1CW;->b(LX/0QB;)LX/1CW;

    move-result-object v3

    check-cast v3, LX/1CW;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {p0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v5

    check-cast v5, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {p0}, LX/1nG;->a(LX/0QB;)LX/1nG;

    move-result-object v6

    check-cast v6, LX/1nG;

    new-instance v7, LX/8pr;

    new-instance p1, LX/8ps;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v0

    invoke-direct {p1, v0}, LX/8ps;-><init>(LX/0QB;)V

    move-object p1, p1

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v0

    invoke-static {p1, v0}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object p1

    move-object p1, p1

    invoke-direct {v7, p1}, LX/8pr;-><init>(LX/0Ot;)V

    move-object v7, v7

    check-cast v7, LX/8pr;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object p0

    check-cast p0, LX/0ad;

    iput-object v2, v1, Lcom/facebook/ufiservices/ui/ProfileListFragment;->b:LX/1CX;

    iput-object v3, v1, Lcom/facebook/ufiservices/ui/ProfileListFragment;->c:LX/1CW;

    iput-object v4, v1, Lcom/facebook/ufiservices/ui/ProfileListFragment;->d:LX/03V;

    iput-object v5, v1, Lcom/facebook/ufiservices/ui/ProfileListFragment;->e:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iput-object v6, v1, Lcom/facebook/ufiservices/ui/ProfileListFragment;->f:LX/1nG;

    iput-object v7, v1, Lcom/facebook/ufiservices/ui/ProfileListFragment;->g:LX/8pr;

    iput-object p0, v1, Lcom/facebook/ufiservices/ui/ProfileListFragment;->h:LX/0ad;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/ufiservices/ui/ProfileListFragment;Z)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    .line 1407553
    iget-object v0, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->q:LX/8pe;

    if-nez v0, :cond_1

    .line 1407554
    :cond_0
    :goto_0
    return-void

    .line 1407555
    :cond_1
    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->q:LX/8pe;

    invoke-interface {v0}, LX/8pe;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1407556
    :cond_2
    iget-boolean v0, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->t:Z

    if-eqz v0, :cond_3

    .line 1407557
    iget-object v0, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->q:LX/8pe;

    iget-object v1, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->i:Lcom/facebook/ufiservices/flyout/ProfileListParams;

    invoke-direct {p0}, Lcom/facebook/ufiservices/ui/ProfileListFragment;->b()LX/0Vd;

    move-result-object v2

    .line 1407558
    new-instance v3, LX/8rc;

    invoke-direct {v3, p0}, LX/8rc;-><init>(Lcom/facebook/ufiservices/ui/ProfileListFragment;)V

    move-object v3, v3

    .line 1407559
    iget-object v5, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->m:LX/55h;

    .line 1407560
    iget-object v6, v5, LX/55h;->e:Ljava/lang/String;

    move-object v5, v6

    .line 1407561
    invoke-virtual {p0}, Lcom/facebook/ufiservices/ui/ProfileListFragment;->o()Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v6

    invoke-interface/range {v0 .. v6}, LX/8pe;->a(Lcom/facebook/ufiservices/flyout/ProfileListParams;LX/0Vd;LX/0TF;ZLjava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_0

    .line 1407562
    :cond_3
    iget-object v1, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->q:LX/8pe;

    iget-object v2, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->i:Lcom/facebook/ufiservices/flyout/ProfileListParams;

    invoke-direct {p0}, Lcom/facebook/ufiservices/ui/ProfileListFragment;->b()LX/0Vd;

    move-result-object v3

    iget-object v0, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->m:LX/55h;

    .line 1407563
    iget-object v5, v0, LX/55h;->e:Ljava/lang/String;

    move-object v5, v5

    .line 1407564
    invoke-virtual {p0}, Lcom/facebook/ufiservices/ui/ProfileListFragment;->o()Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v6

    invoke-interface/range {v1 .. v6}, LX/8pe;->a(Lcom/facebook/ufiservices/flyout/ProfileListParams;LX/0Vd;ZLjava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_0
.end method

.method private b()LX/0Vd;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Vd",
            "<",
            "LX/44w",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPageInfo;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1407552
    new-instance v0, LX/8rb;

    invoke-direct {v0, p0}, LX/8rb;-><init>(Lcom/facebook/ufiservices/ui/ProfileListFragment;)V

    return-object v0
.end method


# virtual methods
.method public abstract a(Ljava/util/List;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1407569
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1407570
    const-class v0, Lcom/facebook/ufiservices/ui/ProfileListFragment;

    invoke-static {v0, p0}, Lcom/facebook/ufiservices/ui/ProfileListFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1407571
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1407572
    const-string v1, "profileListParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/flyout/ProfileListParams;

    iput-object v0, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->i:Lcom/facebook/ufiservices/flyout/ProfileListParams;

    .line 1407573
    new-instance v0, LX/55h;

    iget-object v1, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->a:LX/0QK;

    invoke-direct {v0, v1}, LX/55h;-><init>(LX/0QK;)V

    iput-object v0, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->m:LX/55h;

    .line 1407574
    iget-object v0, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->h:LX/0ad;

    sget-short v1, LX/2ez;->q:S

    invoke-interface {v0, v1, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->t:Z

    .line 1407575
    invoke-virtual {p0}, Lcom/facebook/ufiservices/ui/ProfileListFragment;->m()Landroid/widget/BaseAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->l:Landroid/widget/BaseAdapter;

    .line 1407576
    new-instance v0, LX/8re;

    invoke-direct {v0, p0}, LX/8re;-><init>(Lcom/facebook/ufiservices/ui/ProfileListFragment;)V

    iput-object v0, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->n:LX/8re;

    .line 1407577
    iget-object v0, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->g:LX/8pr;

    iget-object v1, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->i:Lcom/facebook/ufiservices/flyout/ProfileListParams;

    .line 1407578
    iget-object v2, v1, Lcom/facebook/ufiservices/flyout/ProfileListParams;->d:LX/89l;

    move-object v1, v2

    .line 1407579
    iget-object v2, v0, LX/8pr;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8pe;

    .line 1407580
    invoke-interface {v2}, LX/8pe;->a()LX/89l;

    move-result-object p1

    invoke-virtual {p1, v1}, LX/89l;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 1407581
    :goto_0
    move-object v0, v2

    .line 1407582
    iput-object v0, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->q:LX/8pe;

    .line 1407583
    iget-object v0, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->q:LX/8pe;

    if-nez v0, :cond_1

    .line 1407584
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "Unsupported profile list type %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->i:Lcom/facebook/ufiservices/flyout/ProfileListParams;

    .line 1407585
    iget-object p1, v3, Lcom/facebook/ufiservices/flyout/ProfileListParams;->d:LX/89l;

    move-object v3, p1

    .line 1407586
    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1407587
    :cond_1
    new-instance v0, LX/8ra;

    invoke-direct {v0, p0}, LX/8ra;-><init>(Lcom/facebook/ufiservices/ui/ProfileListFragment;)V

    iput-object v0, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->p:Landroid/widget/AbsListView$OnScrollListener;

    .line 1407588
    return-void

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public abstract m()Landroid/widget/BaseAdapter;
.end method

.method public abstract n()I
.end method

.method public abstract o()Lcom/facebook/common/callercontext/CallerContext;
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x218f9a3e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1407549
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1407550
    const/4 v1, 0x1

    invoke-static {p0, v1}, Lcom/facebook/ufiservices/ui/ProfileListFragment;->a$redex0(Lcom/facebook/ufiservices/ui/ProfileListFragment;Z)V

    .line 1407551
    const/16 v1, 0x2b

    const v2, -0x656099c4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x7c5dd833

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1407539
    invoke-virtual {p0}, Lcom/facebook/ufiservices/ui/ProfileListFragment;->n()I

    move-result v0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->o:Landroid/view/View;

    .line 1407540
    iget-object v0, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->i:Lcom/facebook/ufiservices/flyout/ProfileListParams;

    .line 1407541
    iget-object v2, v0, Lcom/facebook/ufiservices/flyout/ProfileListParams;->a:Ljava/lang/String;

    move-object v0, v2

    .line 1407542
    iput-object v0, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->s:Ljava/lang/String;

    .line 1407543
    iget-object v0, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->o:Landroid/view/View;

    const v2, 0x7f0d123f

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->j:Lcom/facebook/widget/listview/BetterListView;

    .line 1407544
    iget-object v0, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->j:Lcom/facebook/widget/listview/BetterListView;

    iget-object v2, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->l:Landroid/widget/BaseAdapter;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1407545
    iget-object v0, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->j:Lcom/facebook/widget/listview/BetterListView;

    iget-object v2, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->p:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1407546
    iget-object v0, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->j:Lcom/facebook/widget/listview/BetterListView;

    new-instance v2, LX/8rd;

    invoke-direct {v2, p0}, LX/8rd;-><init>(Lcom/facebook/ufiservices/ui/ProfileListFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1407547
    iget-object v0, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->o:Landroid/view/View;

    const v2, 0x7f0d0a95

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->k:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 1407548
    iget-object v0, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->o:Landroid/view/View;

    const/16 v2, 0x2b

    const v3, -0x56bc0ddc

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x21f23e82

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1407535
    iget-object v1, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->q:LX/8pe;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->t:Z

    if-eqz v1, :cond_0

    .line 1407536
    iget-object v1, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->q:LX/8pe;

    invoke-interface {v1}, LX/8pe;->c()V

    .line 1407537
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1407538
    const/16 v1, 0x2b

    const v2, -0x75a08600    # -1.0760005E-32f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
