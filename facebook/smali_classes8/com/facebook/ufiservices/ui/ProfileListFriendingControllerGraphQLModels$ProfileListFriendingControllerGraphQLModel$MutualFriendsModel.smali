.class public final Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x66c20030
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel$Serializer;
.end annotation


# instance fields
.field private e:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1409315
    const-class v0, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1409272
    const-class v0, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1409273
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1409274
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1409275
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1409276
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1409277
    return-void
.end method

.method public static a(Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;)Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;
    .locals 8

    .prologue
    .line 1409278
    if-nez p0, :cond_0

    .line 1409279
    const/4 p0, 0x0

    .line 1409280
    :goto_0
    return-object p0

    .line 1409281
    :cond_0
    instance-of v0, p0, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    if-eqz v0, :cond_1

    .line 1409282
    check-cast p0, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    goto :goto_0

    .line 1409283
    :cond_1
    new-instance v0, LX/8ro;

    invoke-direct {v0}, LX/8ro;-><init>()V

    .line 1409284
    invoke-virtual {p0}, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;->a()I

    move-result v1

    iput v1, v0, LX/8ro;->a:I

    .line 1409285
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 1409286
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1409287
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1409288
    iget v3, v0, LX/8ro;->a:I

    invoke-virtual {v2, v5, v3, v5}, LX/186;->a(III)V

    .line 1409289
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1409290
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1409291
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1409292
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1409293
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1409294
    new-instance v3, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    invoke-direct {v3, v2}, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;-><init>(LX/15i;)V

    .line 1409295
    move-object p0, v3

    .line 1409296
    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1409297
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1409298
    iget v0, p0, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1409299
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1409300
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1409301
    iget v0, p0, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;->e:I

    invoke-virtual {p1, v1, v0, v1}, LX/186;->a(III)V

    .line 1409302
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1409303
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1409304
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1409305
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1409306
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1409307
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1409308
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;->e:I

    .line 1409309
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1409310
    new-instance v0, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    invoke-direct {v0}, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;-><init>()V

    .line 1409311
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1409312
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1409313
    const v0, 0x7c736ce9

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1409314
    const v0, 0x5b54b87f

    return v0
.end method
