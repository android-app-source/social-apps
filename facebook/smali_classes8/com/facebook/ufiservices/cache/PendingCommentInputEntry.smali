.class public Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Z

.field public final e:Z

.field public final f:Lcom/facebook/ipc/media/MediaItem;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Lcom/facebook/ipc/media/StickerItem;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Z

.field public final i:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1406199
    new-instance v0, LX/8pc;

    invoke-direct {v0}, LX/8pc;-><init>()V

    sput-object v0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1406185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1406186
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->a:Ljava/lang/String;

    .line 1406187
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->b:Ljava/lang/String;

    .line 1406188
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->c:Ljava/lang/String;

    .line 1406189
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->d:Z

    .line 1406190
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->e:Z

    .line 1406191
    const-class v0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    iput-object v0, p0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->f:Lcom/facebook/ipc/media/MediaItem;

    .line 1406192
    const-class v0, Lcom/facebook/ipc/media/StickerItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/StickerItem;

    iput-object v0, p0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->g:Lcom/facebook/ipc/media/StickerItem;

    .line 1406193
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_2

    :goto_2
    iput-boolean v1, p0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->h:Z

    .line 1406194
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->i:I

    .line 1406195
    return-void

    :cond_0
    move v0, v2

    .line 1406196
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1406197
    goto :goto_1

    :cond_2
    move v1, v2

    .line 1406198
    goto :goto_2
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/facebook/ipc/media/MediaItem;Lcom/facebook/ipc/media/StickerItem;ZI)V
    .locals 0
    .param p6    # Lcom/facebook/ipc/media/MediaItem;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Lcom/facebook/ipc/media/StickerItem;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1406174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1406175
    iput-object p1, p0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->a:Ljava/lang/String;

    .line 1406176
    iput-object p2, p0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->b:Ljava/lang/String;

    .line 1406177
    iput-object p3, p0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->c:Ljava/lang/String;

    .line 1406178
    iput-boolean p4, p0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->d:Z

    .line 1406179
    iput-boolean p5, p0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->e:Z

    .line 1406180
    iput-object p6, p0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->f:Lcom/facebook/ipc/media/MediaItem;

    .line 1406181
    iput-object p7, p0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->g:Lcom/facebook/ipc/media/StickerItem;

    .line 1406182
    iput-boolean p8, p0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->h:Z

    .line 1406183
    iput p9, p0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->i:I

    .line 1406184
    return-void
.end method

.method public static a(Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;)Z
    .locals 1

    .prologue
    .line 1406159
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->g:Lcom/facebook/ipc/media/StickerItem;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->f:Lcom/facebook/ipc/media/MediaItem;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1406173
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1406160
    iget-object v0, p0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1406161
    iget-object v0, p0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1406162
    iget-object v0, p0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1406163
    iget-boolean v0, p0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->d:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1406164
    iget-boolean v0, p0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->e:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1406165
    iget-object v0, p0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->f:Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1406166
    iget-object v0, p0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->g:Lcom/facebook/ipc/media/StickerItem;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1406167
    iget-boolean v0, p0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->h:Z

    if-eqz v0, :cond_2

    :goto_2
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1406168
    iget v0, p0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->i:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1406169
    return-void

    :cond_0
    move v0, v2

    .line 1406170
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1406171
    goto :goto_1

    :cond_2
    move v1, v2

    .line 1406172
    goto :goto_2
.end method
