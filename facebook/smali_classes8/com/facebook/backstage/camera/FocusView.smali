.class public Lcom/facebook/backstage/camera/FocusView;
.super Landroid/view/View;
.source ""


# instance fields
.field public final a:Landroid/graphics/Paint;

.field private final b:Landroid/graphics/Paint;

.field public final c:F

.field private final d:Landroid/graphics/PointF;

.field private final e:Ljava/lang/Runnable;

.field public f:Z

.field public g:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1665124
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/backstage/camera/FocusView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1665125
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1665126
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/backstage/camera/FocusView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1665127
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1665128
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1665129
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/facebook/backstage/camera/FocusView;->d:Landroid/graphics/PointF;

    .line 1665130
    new-instance v0, Lcom/facebook/backstage/camera/FocusView$1;

    invoke-direct {v0, p0}, Lcom/facebook/backstage/camera/FocusView$1;-><init>(Lcom/facebook/backstage/camera/FocusView;)V

    iput-object v0, p0, Lcom/facebook/backstage/camera/FocusView;->e:Ljava/lang/Runnable;

    .line 1665131
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/camera/FocusView;->setWillNotDraw(Z)V

    .line 1665132
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/backstage/camera/FocusView;->a:Landroid/graphics/Paint;

    .line 1665133
    iget-object v0, p0, Lcom/facebook/backstage/camera/FocusView;->a:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1665134
    iget-object v0, p0, Lcom/facebook/backstage/camera/FocusView;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1665135
    iget-object v0, p0, Lcom/facebook/backstage/camera/FocusView;->a:Landroid/graphics/Paint;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {p0}, Lcom/facebook/backstage/camera/FocusView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v3, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1665136
    new-instance v0, Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/facebook/backstage/camera/FocusView;->a:Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v0, p0, Lcom/facebook/backstage/camera/FocusView;->b:Landroid/graphics/Paint;

    .line 1665137
    iget-object v0, p0, Lcom/facebook/backstage/camera/FocusView;->b:Landroid/graphics/Paint;

    const v1, -0x333334

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1665138
    iget-object v0, p0, Lcom/facebook/backstage/camera/FocusView;->b:Landroid/graphics/Paint;

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1665139
    const/high16 v0, 0x41700000    # 15.0f

    invoke-virtual {p0}, Lcom/facebook/backstage/camera/FocusView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v3, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    iput v0, p0, Lcom/facebook/backstage/camera/FocusView;->c:F

    .line 1665140
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1665141
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/backstage/camera/FocusView;->f:Z

    .line 1665142
    invoke-virtual {p0}, Lcom/facebook/backstage/camera/FocusView;->invalidate()V

    .line 1665143
    return-void
.end method

.method public final a(II)V
    .locals 4

    .prologue
    .line 1665144
    iget-object v0, p0, Lcom/facebook/backstage/camera/FocusView;->d:Landroid/graphics/PointF;

    int-to-float v1, p1

    int-to-float v2, p2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 1665145
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/backstage/camera/FocusView;->f:Z

    .line 1665146
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 1665147
    new-instance v1, LX/ALc;

    invoke-direct {v1, p0}, LX/ALc;-><init>(Lcom/facebook/backstage/camera/FocusView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1665148
    const-wide/16 v2, 0x1c2

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1665149
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 1665150
    invoke-virtual {p0}, Lcom/facebook/backstage/camera/FocusView;->invalidate()V

    .line 1665151
    iget-object v0, p0, Lcom/facebook/backstage/camera/FocusView;->e:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/camera/FocusView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1665152
    iget-object v0, p0, Lcom/facebook/backstage/camera/FocusView;->e:Ljava/lang/Runnable;

    const-wide/16 v2, 0xbb8

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/backstage/camera/FocusView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1665153
    return-void

    .line 1665154
    :array_0
    .array-data 4
        0x40400000    # 3.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1665155
    iget-object v0, p0, Lcom/facebook/backstage/camera/FocusView;->a:Landroid/graphics/Paint;

    const v1, -0xff0100

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1665156
    invoke-virtual {p0}, Lcom/facebook/backstage/camera/FocusView;->invalidate()V

    .line 1665157
    iget-object v0, p0, Lcom/facebook/backstage/camera/FocusView;->e:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/backstage/camera/FocusView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1665158
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 1665159
    iget-object v0, p0, Lcom/facebook/backstage/camera/FocusView;->a:Landroid/graphics/Paint;

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1665160
    invoke-virtual {p0}, Lcom/facebook/backstage/camera/FocusView;->invalidate()V

    .line 1665161
    iget-object v0, p0, Lcom/facebook/backstage/camera/FocusView;->e:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/backstage/camera/FocusView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1665162
    return-void
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 1665163
    iget-object v0, p0, Lcom/facebook/backstage/camera/FocusView;->a:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1665164
    invoke-virtual {p0}, Lcom/facebook/backstage/camera/FocusView;->invalidate()V

    .line 1665165
    iget-object v0, p0, Lcom/facebook/backstage/camera/FocusView;->e:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/camera/FocusView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1665166
    iget-object v0, p0, Lcom/facebook/backstage/camera/FocusView;->e:Ljava/lang/Runnable;

    const-wide/16 v2, 0xbb8

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/backstage/camera/FocusView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1665167
    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 1665168
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1665169
    iget-boolean v0, p0, Lcom/facebook/backstage/camera/FocusView;->f:Z

    if-eqz v0, :cond_0

    .line 1665170
    iget-object v0, p0, Lcom/facebook/backstage/camera/FocusView;->d:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lcom/facebook/backstage/camera/FocusView;->d:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget v2, p0, Lcom/facebook/backstage/camera/FocusView;->c:F

    const/high16 v3, 0x40400000    # 3.0f

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/facebook/backstage/camera/FocusView;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1665171
    iget-object v0, p0, Lcom/facebook/backstage/camera/FocusView;->d:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lcom/facebook/backstage/camera/FocusView;->d:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget v2, p0, Lcom/facebook/backstage/camera/FocusView;->g:F

    iget-object v3, p0, Lcom/facebook/backstage/camera/FocusView;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1665172
    :cond_0
    return-void
.end method
