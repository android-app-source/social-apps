.class public Lcom/facebook/backstage/camera/CameraView;
.super Landroid/widget/FrameLayout;
.source ""


# static fields
.field public static final f:Ljava/lang/String;


# instance fields
.field public a:Lcom/facebook/quicklog/QuickPerformanceLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1Er;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/ALC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1Eo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final g:LX/ALG;

.field private final h:LX/ALL;

.field private final i:LX/ALN;

.field public final j:Lcom/facebook/optic/CameraPreviewView;

.field public final k:Lcom/facebook/backstage/camera/FocusView;

.field public final l:Lcom/facebook/backstage/camera/ZoomView;

.field private final m:Landroid/widget/LinearLayout;

.field private final n:Landroid/widget/FrameLayout;

.field public final o:Landroid/widget/ImageView;

.field public final p:Landroid/widget/ImageView;

.field private final q:Lcom/facebook/backstage/ui/RotateButton;

.field private final r:Lcom/facebook/backstage/camera/CaptureButton;

.field public final s:Lcom/facebook/backstage/ui/ToggleTextButton;

.field public final t:Lcom/facebook/backstage/ui/VideoRecordingProgressBar;

.field public final u:Landroid/os/Handler;

.field public final v:Ljava/lang/Runnable;

.field public w:LX/ALV;

.field public x:LX/ALU;

.field public y:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1664975
    const-class v0, Lcom/facebook/backstage/camera/CameraView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/backstage/camera/CameraView;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1664976
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/backstage/camera/CameraView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1664977
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1664978
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/backstage/camera/CameraView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1664979
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1664926
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1664927
    new-instance v0, LX/ALH;

    invoke-direct {v0, p0}, LX/ALH;-><init>(Lcom/facebook/backstage/camera/CameraView;)V

    iput-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->g:LX/ALG;

    .line 1664928
    new-instance v0, LX/ALM;

    invoke-direct {v0, p0}, LX/ALM;-><init>(Lcom/facebook/backstage/camera/CameraView;)V

    iput-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->h:LX/ALL;

    .line 1664929
    new-instance v0, LX/ALO;

    invoke-direct {v0, p0}, LX/ALO;-><init>(Lcom/facebook/backstage/camera/CameraView;)V

    iput-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->i:LX/ALN;

    .line 1664930
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->u:Landroid/os/Handler;

    .line 1664931
    new-instance v0, Lcom/facebook/backstage/camera/CameraView$4;

    invoke-direct {v0, p0}, Lcom/facebook/backstage/camera/CameraView$4;-><init>(Lcom/facebook/backstage/camera/CameraView;)V

    iput-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->v:Ljava/lang/Runnable;

    .line 1664932
    const-class v0, Lcom/facebook/backstage/camera/CameraView;

    invoke-static {v0, p0}, Lcom/facebook/backstage/camera/CameraView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1664933
    const v0, 0x7f03016d

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1664934
    const v0, 0x7f0d0681

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/camera/CameraView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/optic/CameraPreviewView;

    iput-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->j:Lcom/facebook/optic/CameraPreviewView;

    .line 1664935
    const v0, 0x7f0d0682

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/camera/CameraView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/backstage/camera/FocusView;

    iput-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->k:Lcom/facebook/backstage/camera/FocusView;

    .line 1664936
    const v0, 0x7f0d0683

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/camera/CameraView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/backstage/camera/ZoomView;

    iput-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->l:Lcom/facebook/backstage/camera/ZoomView;

    .line 1664937
    const v0, 0x7f0d0687

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/camera/CameraView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->n:Landroid/widget/FrameLayout;

    .line 1664938
    const v0, 0x7f0d0684

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/camera/CameraView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->m:Landroid/widget/LinearLayout;

    .line 1664939
    const v0, 0x7f0d0688

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/camera/CameraView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->o:Landroid/widget/ImageView;

    .line 1664940
    const v0, 0x7f0d0689

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/camera/CameraView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->p:Landroid/widget/ImageView;

    .line 1664941
    const v0, 0x7f0d068a

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/camera/CameraView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/backstage/ui/RotateButton;

    iput-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->q:Lcom/facebook/backstage/ui/RotateButton;

    .line 1664942
    const v0, 0x7f0d0686

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/camera/CameraView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/backstage/camera/CaptureButton;

    iput-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->r:Lcom/facebook/backstage/camera/CaptureButton;

    .line 1664943
    const v0, 0x7f0d0685

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/camera/CameraView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/backstage/ui/ToggleTextButton;

    iput-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->s:Lcom/facebook/backstage/ui/ToggleTextButton;

    .line 1664944
    const v0, 0x7f0d068b

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/camera/CameraView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/backstage/ui/VideoRecordingProgressBar;

    iput-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->t:Lcom/facebook/backstage/ui/VideoRecordingProgressBar;

    .line 1664945
    new-instance v0, LX/ALV;

    invoke-direct {v0, p0, p1}, LX/ALV;-><init>(Lcom/facebook/backstage/camera/CameraView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->w:LX/ALV;

    .line 1664946
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->j:Lcom/facebook/optic/CameraPreviewView;

    new-instance v1, LX/ALB;

    invoke-direct {v1}, LX/ALB;-><init>()V

    .line 1664947
    iput-object v1, v0, Lcom/facebook/optic/CameraPreviewView;->u:LX/5fi;

    .line 1664948
    invoke-virtual {p0}, Lcom/facebook/backstage/camera/CameraView;->c()V

    .line 1664949
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 1664950
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->q:Lcom/facebook/backstage/ui/RotateButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/backstage/ui/RotateButton;->setVisibility(I)V

    .line 1664951
    :cond_0
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->j:Lcom/facebook/optic/CameraPreviewView;

    new-instance v1, LX/ALP;

    invoke-direct {v1, p0}, LX/ALP;-><init>(Lcom/facebook/backstage/camera/CameraView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/optic/CameraPreviewView;->setCameraInitialisedCallback(LX/5fb;)V

    .line 1664952
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->j:Lcom/facebook/optic/CameraPreviewView;

    new-instance v1, LX/ALQ;

    invoke-direct {v1, p0}, LX/ALQ;-><init>(Lcom/facebook/backstage/camera/CameraView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/optic/CameraPreviewView;->setOnPreviewStartedListener(LX/ALQ;)V

    .line 1664953
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->j:Lcom/facebook/optic/CameraPreviewView;

    new-instance v1, LX/ALR;

    invoke-direct {v1, p0}, LX/ALR;-><init>(Lcom/facebook/backstage/camera/CameraView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/optic/CameraPreviewView;->setOnPreviewStoppedListener(LX/ALR;)V

    .line 1664954
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->j:Lcom/facebook/optic/CameraPreviewView;

    new-instance v1, LX/ALS;

    invoke-direct {v1, p0}, LX/ALS;-><init>(Lcom/facebook/backstage/camera/CameraView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/optic/CameraPreviewView;->setFocusCallbackListener(LX/5fY;)V

    .line 1664955
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->j:Lcom/facebook/optic/CameraPreviewView;

    new-instance v1, LX/ALT;

    invoke-direct {v1, p0}, LX/ALT;-><init>(Lcom/facebook/backstage/camera/CameraView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/optic/CameraPreviewView;->setZoomChangeListener(LX/ALT;)V

    .line 1664956
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->j:Lcom/facebook/optic/CameraPreviewView;

    new-instance v1, LX/ALE;

    invoke-direct {v1, p0}, LX/ALE;-><init>(Lcom/facebook/backstage/camera/CameraView;)V

    .line 1664957
    iput-object v1, v0, Lcom/facebook/optic/CameraPreviewView;->o:LX/5ff;

    .line 1664958
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->j:Lcom/facebook/optic/CameraPreviewView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/optic/CameraPreviewView;->setMediaOrientationLocked(Z)V

    .line 1664959
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->q:Lcom/facebook/backstage/ui/RotateButton;

    iget-object v1, p0, Lcom/facebook/backstage/camera/CameraView;->g:LX/ALG;

    .line 1664960
    iput-object v1, v0, Lcom/facebook/backstage/ui/RotateButton;->b:LX/ALG;

    .line 1664961
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->r:Lcom/facebook/backstage/camera/CaptureButton;

    iget-object v1, p0, Lcom/facebook/backstage/camera/CameraView;->h:LX/ALL;

    .line 1664962
    iput-object v1, v0, Lcom/facebook/backstage/camera/CaptureButton;->j:LX/ALL;

    .line 1664963
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->s:Lcom/facebook/backstage/ui/ToggleTextButton;

    iget-object v1, p0, Lcom/facebook/backstage/camera/CameraView;->i:LX/ALN;

    .line 1664964
    iput-object v1, v0, Lcom/facebook/backstage/ui/ToggleTextButton;->a:LX/ALN;

    .line 1664965
    return-void
.end method

.method private static a(Lcom/facebook/backstage/camera/CameraView;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/1Er;LX/0hB;LX/ALC;LX/1Eo;)V
    .locals 0

    .prologue
    .line 1664980
    iput-object p1, p0, Lcom/facebook/backstage/camera/CameraView;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iput-object p2, p0, Lcom/facebook/backstage/camera/CameraView;->b:LX/1Er;

    iput-object p3, p0, Lcom/facebook/backstage/camera/CameraView;->c:LX/0hB;

    iput-object p4, p0, Lcom/facebook/backstage/camera/CameraView;->d:LX/ALC;

    iput-object p5, p0, Lcom/facebook/backstage/camera/CameraView;->e:LX/1Eo;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/backstage/camera/CameraView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/backstage/camera/CameraView;

    invoke-static {v5}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v1

    check-cast v1, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v5}, LX/1Er;->a(LX/0QB;)LX/1Er;

    move-result-object v2

    check-cast v2, LX/1Er;

    invoke-static {v5}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v3

    check-cast v3, LX/0hB;

    invoke-static {v5}, LX/ALC;->b(LX/0QB;)LX/ALC;

    move-result-object v4

    check-cast v4, LX/ALC;

    invoke-static {v5}, LX/1Eo;->a(LX/0QB;)LX/1Eo;

    move-result-object v5

    check-cast v5, LX/1Eo;

    invoke-static/range {v0 .. v5}, Lcom/facebook/backstage/camera/CameraView;->a(Lcom/facebook/backstage/camera/CameraView;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/1Er;LX/0hB;LX/ALC;LX/1Eo;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/backstage/camera/CameraView;ZZ)V
    .locals 5

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 1664981
    if-eqz p1, :cond_1

    .line 1664982
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->n:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1664983
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->s:Lcom/facebook/backstage/ui/ToggleTextButton;

    :cond_0
    move-object v1, v0

    move v0, v2

    .line 1664984
    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/backstage/ui/ToggleTextButton;->setVisibility(I)V

    .line 1664985
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->q:Lcom/facebook/backstage/ui/RotateButton;

    invoke-virtual {v0, p2}, Lcom/facebook/backstage/ui/RotateButton;->setEnabled(Z)V

    .line 1664986
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->s:Lcom/facebook/backstage/ui/ToggleTextButton;

    invoke-virtual {v0, p2}, Lcom/facebook/backstage/ui/ToggleTextButton;->setEnabled(Z)V

    .line 1664987
    return-void

    .line 1664988
    :cond_1
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->n:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1664989
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->s:Lcom/facebook/backstage/ui/ToggleTextButton;

    invoke-static {p0}, Lcom/facebook/backstage/camera/CameraView;->h(Lcom/facebook/backstage/camera/CameraView;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v4, v1

    move-object v1, v0

    move v0, v4

    goto :goto_0
.end method

.method private f()Z
    .locals 2

    .prologue
    .line 1664990
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->j:Lcom/facebook/optic/CameraPreviewView;

    invoke-virtual {v0}, Lcom/facebook/optic/CameraPreviewView;->getCameraFacing()LX/5fM;

    move-result-object v0

    sget-object v1, LX/5fM;->FRONT:LX/5fM;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic f(Lcom/facebook/backstage/camera/CameraView;)Z
    .locals 1

    .prologue
    .line 1664991
    invoke-direct {p0}, Lcom/facebook/backstage/camera/CameraView;->f()Z

    move-result v0

    return v0
.end method

.method public static g(Lcom/facebook/backstage/camera/CameraView;)Z
    .locals 2

    .prologue
    .line 1664992
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->j:Lcom/facebook/optic/CameraPreviewView;

    invoke-virtual {v0}, Lcom/facebook/optic/CameraPreviewView;->getFlashMode()Ljava/lang/String;

    move-result-object v0

    const-string v1, "torch"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static h(Lcom/facebook/backstage/camera/CameraView;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1664966
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->j:Lcom/facebook/optic/CameraPreviewView;

    invoke-virtual {v0}, Lcom/facebook/optic/CameraPreviewView;->getSupportedFlashModes()Ljava/util/List;

    move-result-object v3

    .line 1664967
    if-eqz v3, :cond_0

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 1664968
    :goto_0
    if-nez v0, :cond_1

    move v0, v1

    .line 1664969
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 1664970
    goto :goto_0

    .line 1664971
    :cond_1
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1664972
    const-string v4, "torch"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    .line 1664973
    goto :goto_1

    :cond_3
    move v0, v1

    .line 1664974
    goto :goto_1
.end method

.method public static i(Lcom/facebook/backstage/camera/CameraView;)Z
    .locals 2

    .prologue
    .line 1664844
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->j:Lcom/facebook/optic/CameraPreviewView;

    invoke-virtual {v0}, Lcom/facebook/optic/CameraPreviewView;->getCameraFacing()LX/5fM;

    move-result-object v0

    sget-object v1, LX/5fM;->FRONT:LX/5fM;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static j(Lcom/facebook/backstage/camera/CameraView;)V
    .locals 4

    .prologue
    const/16 v3, 0x3a98

    .line 1664845
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 1664846
    new-instance v2, Lcom/facebook/backstage/camera/CameraView$11;

    invoke-direct {v2, p0, v0, v1}, Lcom/facebook/backstage/camera/CameraView$11;-><init>(Lcom/facebook/backstage/camera/CameraView;J)V

    .line 1664847
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->t:Lcom/facebook/backstage/ui/VideoRecordingProgressBar;

    invoke-virtual {v0, v3}, Lcom/facebook/backstage/ui/VideoRecordingProgressBar;->setProgress(I)V

    .line 1664848
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->t:Lcom/facebook/backstage/ui/VideoRecordingProgressBar;

    invoke-virtual {v0, v3}, Lcom/facebook/backstage/ui/VideoRecordingProgressBar;->setMax(I)V

    .line 1664849
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->t:Lcom/facebook/backstage/ui/VideoRecordingProgressBar;

    invoke-virtual {v0, v2}, Lcom/facebook/backstage/ui/VideoRecordingProgressBar;->post(Ljava/lang/Runnable;)Z

    .line 1664850
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->t:Lcom/facebook/backstage/ui/VideoRecordingProgressBar;

    invoke-virtual {v0}, Lcom/facebook/backstage/ui/VideoRecordingProgressBar;->getTop()I

    move-result v0

    int-to-float v0, v0

    .line 1664851
    iget-object v1, p0, Lcom/facebook/backstage/camera/CameraView;->t:Lcom/facebook/backstage/ui/VideoRecordingProgressBar;

    invoke-virtual {v1}, Lcom/facebook/backstage/ui/VideoRecordingProgressBar;->getHeight()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    .line 1664852
    iget-object v2, p0, Lcom/facebook/backstage/camera/CameraView;->t:Lcom/facebook/backstage/ui/VideoRecordingProgressBar;

    invoke-static {v2}, LX/7hT;->a(Landroid/view/View;)LX/7hT;

    move-result-object v2

    invoke-virtual {v2}, LX/7hT;->e()LX/7hT;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, LX/7hT;->b(FF)LX/7hT;

    move-result-object v0

    const/4 v1, 0x0

    .line 1664853
    iput v1, v0, LX/7hT;->z:I

    .line 1664854
    move-object v0, v0

    .line 1664855
    invoke-virtual {v0}, LX/7hT;->d()LX/7hT;

    .line 1664856
    return-void
.end method

.method public static k(Lcom/facebook/backstage/camera/CameraView;)V
    .locals 3

    .prologue
    .line 1664857
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->t:Lcom/facebook/backstage/ui/VideoRecordingProgressBar;

    invoke-virtual {v0}, Lcom/facebook/backstage/ui/VideoRecordingProgressBar;->getHeight()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    .line 1664858
    iget-object v1, p0, Lcom/facebook/backstage/camera/CameraView;->t:Lcom/facebook/backstage/ui/VideoRecordingProgressBar;

    invoke-virtual {v1}, Lcom/facebook/backstage/ui/VideoRecordingProgressBar;->getTop()I

    move-result v1

    int-to-float v1, v1

    .line 1664859
    iget-object v2, p0, Lcom/facebook/backstage/camera/CameraView;->t:Lcom/facebook/backstage/ui/VideoRecordingProgressBar;

    invoke-static {v2}, LX/7hT;->a(Landroid/view/View;)LX/7hT;

    move-result-object v2

    invoke-virtual {v2}, LX/7hT;->e()LX/7hT;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, LX/7hT;->b(FF)LX/7hT;

    move-result-object v0

    const/4 v1, 0x4

    .line 1664860
    iput v1, v0, LX/7hT;->A:I

    .line 1664861
    move-object v0, v0

    .line 1664862
    invoke-virtual {v0}, LX/7hT;->d()LX/7hT;

    .line 1664863
    return-void
.end method

.method public static setFlash(Lcom/facebook/backstage/camera/CameraView;Z)V
    .locals 4

    .prologue
    .line 1664864
    invoke-static {p0}, Lcom/facebook/backstage/camera/CameraView;->g(Lcom/facebook/backstage/camera/CameraView;)Z

    move-result v0

    if-eq v0, p1, :cond_0

    .line 1664865
    iget-object v1, p0, Lcom/facebook/backstage/camera/CameraView;->j:Lcom/facebook/optic/CameraPreviewView;

    if-eqz p1, :cond_1

    const-string v0, "torch"

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/optic/CameraPreviewView;->setFlashMode(Ljava/lang/String;)V

    .line 1664866
    :cond_0
    if-eqz p1, :cond_2

    .line 1664867
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->s:Lcom/facebook/backstage/ui/ToggleTextButton;

    const v1, 0x7f02077d

    invoke-virtual {v0, v1}, Lcom/facebook/backstage/ui/ToggleTextButton;->setBackgroundResource(I)V

    .line 1664868
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/backstage/camera/CameraView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a004b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-static {v0}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    .line 1664869
    iget-object v1, p0, Lcom/facebook/backstage/camera/CameraView;->s:Lcom/facebook/backstage/ui/ToggleTextButton;

    invoke-virtual {v1}, Lcom/facebook/backstage/ui/ToggleTextButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    new-instance v2, Landroid/graphics/PorterDuffColorFilter;

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v0

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v2, v0, v3}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 1664870
    return-void

    .line 1664871
    :cond_1
    const-string v0, "off"

    goto :goto_0

    .line 1664872
    :cond_2
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->s:Lcom/facebook/backstage/ui/ToggleTextButton;

    const v1, 0x7f02077e

    invoke-virtual {v0, v1}, Lcom/facebook/backstage/ui/ToggleTextButton;->setBackgroundResource(I)V

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1664873
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->m:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1664874
    invoke-static {}, Lcom/facebook/optic/CameraPreviewView;->a()V

    .line 1664875
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1664876
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->m:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1664877
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->j:Lcom/facebook/optic/CameraPreviewView;

    invoke-virtual {v0}, Lcom/facebook/optic/CameraPreviewView;->b()V

    .line 1664878
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1664879
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->q:Lcom/facebook/backstage/ui/RotateButton;

    .line 1664880
    iput-object v1, v0, Lcom/facebook/backstage/ui/RotateButton;->b:LX/ALG;

    .line 1664881
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->r:Lcom/facebook/backstage/camera/CaptureButton;

    .line 1664882
    iput-object v1, v0, Lcom/facebook/backstage/camera/CaptureButton;->j:LX/ALL;

    .line 1664883
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->r:Lcom/facebook/backstage/camera/CaptureButton;

    invoke-virtual {v0}, Lcom/facebook/backstage/camera/CaptureButton;->a()V

    .line 1664884
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->s:Lcom/facebook/backstage/ui/ToggleTextButton;

    .line 1664885
    iput-object v1, v0, Lcom/facebook/backstage/ui/ToggleTextButton;->a:LX/ALN;

    .line 1664886
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->j:Lcom/facebook/optic/CameraPreviewView;

    const/4 v1, 0x0

    .line 1664887
    iput-boolean v1, v0, Lcom/facebook/optic/CameraPreviewView;->t:Z

    .line 1664888
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->w:LX/ALV;

    invoke-virtual {v0}, LX/ALV;->disable()V

    .line 1664889
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1664890
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->q:Lcom/facebook/backstage/ui/RotateButton;

    iget-object v1, p0, Lcom/facebook/backstage/camera/CameraView;->g:LX/ALG;

    .line 1664891
    iput-object v1, v0, Lcom/facebook/backstage/ui/RotateButton;->b:LX/ALG;

    .line 1664892
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->r:Lcom/facebook/backstage/camera/CaptureButton;

    iget-object v1, p0, Lcom/facebook/backstage/camera/CameraView;->h:LX/ALL;

    .line 1664893
    iput-object v1, v0, Lcom/facebook/backstage/camera/CaptureButton;->j:LX/ALL;

    .line 1664894
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->s:Lcom/facebook/backstage/ui/ToggleTextButton;

    iget-object v1, p0, Lcom/facebook/backstage/camera/CameraView;->i:LX/ALN;

    .line 1664895
    iput-object v1, v0, Lcom/facebook/backstage/ui/ToggleTextButton;->a:LX/ALN;

    .line 1664896
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->j:Lcom/facebook/optic/CameraPreviewView;

    const/4 v1, 0x1

    .line 1664897
    iput-boolean v1, v0, Lcom/facebook/optic/CameraPreviewView;->t:Z

    .line 1664898
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->w:LX/ALV;

    invoke-virtual {v0}, LX/ALV;->enable()V

    .line 1664899
    return-void
.end method

.method public final onVisibilityChanged(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 1664900
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onVisibilityChanged(Landroid/view/View;I)V

    .line 1664901
    iget-boolean v0, p0, Lcom/facebook/backstage/camera/CameraView;->y:Z

    if-eqz v0, :cond_0

    .line 1664902
    if-nez p2, :cond_1

    .line 1664903
    invoke-virtual {p0}, Lcom/facebook/backstage/camera/CameraView;->b()V

    .line 1664904
    :cond_0
    :goto_0
    return-void

    .line 1664905
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/backstage/camera/CameraView;->a()V

    goto :goto_0
.end method

.method public setCameraPreviewAspectRatio(F)V
    .locals 4

    .prologue
    .line 1664906
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->c:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v1

    .line 1664907
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->d:LX/ALC;

    invoke-virtual {v0, p1}, LX/ALC;->a(F)I

    move-result v0

    .line 1664908
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v1, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1664909
    const/16 v3, 0x30

    iput v3, v2, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1664910
    iget-object v3, p0, Lcom/facebook/backstage/camera/CameraView;->j:Lcom/facebook/optic/CameraPreviewView;

    invoke-virtual {v3, v2}, Lcom/facebook/optic/CameraPreviewView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1664911
    iget-object v3, p0, Lcom/facebook/backstage/camera/CameraView;->l:Lcom/facebook/backstage/camera/ZoomView;

    invoke-virtual {v3, v2}, Lcom/facebook/backstage/camera/ZoomView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1664912
    iget-object v3, p0, Lcom/facebook/backstage/camera/CameraView;->k:Lcom/facebook/backstage/camera/FocusView;

    invoke-virtual {v3, v2}, Lcom/facebook/backstage/camera/FocusView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1664913
    iget-object v2, p0, Lcom/facebook/backstage/camera/CameraView;->c:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->d()I

    move-result v2

    sub-int v0, v2, v0

    .line 1664914
    if-gtz v0, :cond_0

    .line 1664915
    const/4 v0, -0x2

    .line 1664916
    :cond_0
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v1, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1664917
    const/16 v0, 0x50

    iput v0, v2, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1664918
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1664919
    return-void
.end method

.method public setListener(LX/ALU;)V
    .locals 0

    .prologue
    .line 1664924
    iput-object p1, p0, Lcom/facebook/backstage/camera/CameraView;->x:LX/ALU;

    .line 1664925
    return-void
.end method

.method public setSelfie(Z)V
    .locals 2

    .prologue
    .line 1664920
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->j:Lcom/facebook/optic/CameraPreviewView;

    invoke-virtual {v0}, Lcom/facebook/optic/CameraPreviewView;->getCameraFacing()LX/5fM;

    move-result-object v0

    sget-object v1, LX/5fM;->FRONT:LX/5fM;

    if-eq v0, v1, :cond_0

    .line 1664921
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView;->j:Lcom/facebook/optic/CameraPreviewView;

    sget-object v1, LX/5fM;->FRONT:LX/5fM;

    .line 1664922
    iput-object v1, v0, Lcom/facebook/optic/CameraPreviewView;->p:LX/5fM;

    .line 1664923
    :cond_0
    return-void
.end method
