.class public final Lcom/facebook/backstage/camera/CameraView$11;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:J

.field public final synthetic b:Lcom/facebook/backstage/camera/CameraView;


# direct methods
.method public constructor <init>(Lcom/facebook/backstage/camera/CameraView;J)V
    .locals 0

    .prologue
    .line 1664714
    iput-object p1, p0, Lcom/facebook/backstage/camera/CameraView$11;->b:Lcom/facebook/backstage/camera/CameraView;

    iput-wide p2, p0, Lcom/facebook/backstage/camera/CameraView$11;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 1664715
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 1664716
    iget-object v2, p0, Lcom/facebook/backstage/camera/CameraView$11;->b:Lcom/facebook/backstage/camera/CameraView;

    iget-object v2, v2, Lcom/facebook/backstage/camera/CameraView;->t:Lcom/facebook/backstage/ui/VideoRecordingProgressBar;

    iget-wide v4, p0, Lcom/facebook/backstage/camera/CameraView$11;->a:J

    sub-long/2addr v0, v4

    long-to-int v0, v0

    rsub-int v0, v0, 0x3a98

    invoke-virtual {v2, v0}, Lcom/facebook/backstage/ui/VideoRecordingProgressBar;->setProgress(I)V

    .line 1664717
    invoke-static {}, Lcom/facebook/optic/CameraPreviewView;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1664718
    iget-object v0, p0, Lcom/facebook/backstage/camera/CameraView$11;->b:Lcom/facebook/backstage/camera/CameraView;

    iget-object v0, v0, Lcom/facebook/backstage/camera/CameraView;->t:Lcom/facebook/backstage/ui/VideoRecordingProgressBar;

    invoke-virtual {v0, p0}, Lcom/facebook/backstage/ui/VideoRecordingProgressBar;->post(Ljava/lang/Runnable;)Z

    .line 1664719
    :cond_0
    return-void
.end method
