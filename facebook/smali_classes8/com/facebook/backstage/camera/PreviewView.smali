.class public Lcom/facebook/backstage/camera/PreviewView;
.super Landroid/widget/FrameLayout;
.source ""


# static fields
.field public static final h:Ljava/lang/String;


# instance fields
.field public a:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1En;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/ALC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/1Eo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0fO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private i:Z

.field private final j:Landroid/view/ViewGroup;

.field private final k:Lcom/facebook/backstage/ui/MediaView;

.field public final l:Lcom/facebook/resources/ui/FbButton;

.field public final m:Lcom/facebook/backstage/ui/ScalingToggleableEditText;

.field public final n:Landroid/widget/FrameLayout;

.field public final o:Lcom/facebook/messaging/doodle/ColourIndicator;

.field private final p:Lcom/facebook/messaging/doodle/ColourPicker;

.field public final q:Lcom/facebook/fbui/glyph/GlyphView;

.field private final r:Landroid/view/View$OnClickListener;

.field private s:Landroid/view/View$OnClickListener;

.field public t:LX/7gj;

.field public u:LX/ALj;

.field public v:Lcom/google/common/util/concurrent/ListenableFuture;

.field public w:Lcom/google/common/util/concurrent/ListenableFuture;

.field private final x:Landroid/view/View$OnClickListener;

.field private final y:LX/8CF;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1665359
    const-class v0, Lcom/facebook/backstage/camera/PreviewView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/backstage/camera/PreviewView;->h:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1665357
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/backstage/camera/PreviewView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1665358
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1665355
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/backstage/camera/PreviewView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1665356
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 1665330
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1665331
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/backstage/camera/PreviewView;->i:Z

    .line 1665332
    new-instance v0, LX/ALe;

    invoke-direct {v0, p0}, LX/ALe;-><init>(Lcom/facebook/backstage/camera/PreviewView;)V

    iput-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->r:Landroid/view/View$OnClickListener;

    .line 1665333
    new-instance v0, LX/ALg;

    invoke-direct {v0, p0}, LX/ALg;-><init>(Lcom/facebook/backstage/camera/PreviewView;)V

    iput-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->s:Landroid/view/View$OnClickListener;

    .line 1665334
    new-instance v0, LX/ALh;

    invoke-direct {v0, p0}, LX/ALh;-><init>(Lcom/facebook/backstage/camera/PreviewView;)V

    iput-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->x:Landroid/view/View$OnClickListener;

    .line 1665335
    new-instance v0, LX/ALi;

    invoke-direct {v0, p0}, LX/ALi;-><init>(Lcom/facebook/backstage/camera/PreviewView;)V

    iput-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->y:LX/8CF;

    .line 1665336
    const-class v0, Lcom/facebook/backstage/camera/PreviewView;

    invoke-static {v0, p0}, Lcom/facebook/backstage/camera/PreviewView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1665337
    const v0, 0x7f031008

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1665338
    const v0, 0x7f0d16c3

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/camera/PreviewView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/backstage/ui/MediaView;

    iput-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->k:Lcom/facebook/backstage/ui/MediaView;

    .line 1665339
    const v0, 0x7f0d119d

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/camera/PreviewView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->l:Lcom/facebook/resources/ui/FbButton;

    .line 1665340
    const v0, 0x7f0d2687

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/camera/PreviewView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->q:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1665341
    const v0, 0x7f0d2688

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/camera/PreviewView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->j:Landroid/view/ViewGroup;

    .line 1665342
    const v0, 0x7f0d06be

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/camera/PreviewView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/backstage/ui/ScalingToggleableEditText;

    iput-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->m:Lcom/facebook/backstage/ui/ScalingToggleableEditText;

    .line 1665343
    const v0, 0x7f0d2686

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/camera/PreviewView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->n:Landroid/widget/FrameLayout;

    .line 1665344
    const v0, 0x7f0d2509

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/camera/PreviewView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/doodle/ColourIndicator;

    iput-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->o:Lcom/facebook/messaging/doodle/ColourIndicator;

    .line 1665345
    const v0, 0x7f0d250a

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/camera/PreviewView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/doodle/ColourPicker;

    iput-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->p:Lcom/facebook/messaging/doodle/ColourPicker;

    .line 1665346
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->p:Lcom/facebook/messaging/doodle/ColourPicker;

    iget-object v1, p0, Lcom/facebook/backstage/camera/PreviewView;->y:LX/8CF;

    .line 1665347
    iput-object v1, v0, Lcom/facebook/messaging/doodle/ColourPicker;->c:LX/8CF;

    .line 1665348
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->o:Lcom/facebook/messaging/doodle/ColourIndicator;

    invoke-virtual {v0, v2}, Lcom/facebook/messaging/doodle/ColourIndicator;->setVisibility(I)V

    .line 1665349
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->p:Lcom/facebook/messaging/doodle/ColourPicker;

    invoke-virtual {v0, v2}, Lcom/facebook/messaging/doodle/ColourPicker;->setVisibility(I)V

    .line 1665350
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->k:Lcom/facebook/backstage/ui/MediaView;

    iget-object v1, p0, Lcom/facebook/backstage/camera/PreviewView;->x:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/backstage/ui/MediaView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1665351
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->g:LX/0fO;

    invoke-virtual {v0}, LX/0fO;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1665352
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->l:Lcom/facebook/resources/ui/FbButton;

    const v1, 0x7f08284c

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    .line 1665353
    :goto_0
    return-void

    .line 1665354
    :cond_0
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->l:Lcom/facebook/resources/ui/FbButton;

    const v1, 0x7f08284d

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    goto :goto_0
.end method

.method private a(F)V
    .locals 4

    .prologue
    .line 1665313
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->t:LX/7gj;

    invoke-virtual {v0}, LX/7gj;->k()I

    move-result v0

    const/16 v1, 0x5a

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->t:LX/7gj;

    invoke-virtual {v0}, LX/7gj;->k()I

    move-result v0

    const/16 v1, 0x10e

    if-ne v0, v1, :cond_1

    .line 1665314
    :cond_0
    const/16 v0, 0x10

    .line 1665315
    const/high16 v1, 0x3f800000    # 1.0f

    div-float p1, v1, p1

    .line 1665316
    :goto_0
    iget-object v1, p0, Lcom/facebook/backstage/camera/PreviewView;->a:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->c()I

    move-result v1

    .line 1665317
    int-to-float v2, v1

    mul-float/2addr v2, p1

    float-to-int v2, v2

    .line 1665318
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1665319
    iput v1, v3, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 1665320
    iput v2, v3, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 1665321
    iput v0, v3, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1665322
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->n:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1665323
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->k:Lcom/facebook/backstage/ui/MediaView;

    invoke-virtual {v0, v3}, Lcom/facebook/backstage/ui/MediaView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1665324
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->m:Lcom/facebook/backstage/ui/ScalingToggleableEditText;

    invoke-virtual {v0, v3}, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1665325
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->n:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->invalidate()V

    .line 1665326
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->k:Lcom/facebook/backstage/ui/MediaView;

    invoke-virtual {v0}, Lcom/facebook/backstage/ui/MediaView;->invalidate()V

    .line 1665327
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->m:Lcom/facebook/backstage/ui/ScalingToggleableEditText;

    invoke-virtual {v0}, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->invalidate()V

    .line 1665328
    return-void

    .line 1665329
    :cond_1
    const/16 v0, 0x30

    goto :goto_0
.end method

.method private static a(Lcom/facebook/backstage/camera/PreviewView;LX/0hB;LX/0TD;LX/1En;LX/03V;LX/ALC;LX/1Eo;LX/0fO;)V
    .locals 0

    .prologue
    .line 1665312
    iput-object p1, p0, Lcom/facebook/backstage/camera/PreviewView;->a:LX/0hB;

    iput-object p2, p0, Lcom/facebook/backstage/camera/PreviewView;->b:LX/0TD;

    iput-object p3, p0, Lcom/facebook/backstage/camera/PreviewView;->c:LX/1En;

    iput-object p4, p0, Lcom/facebook/backstage/camera/PreviewView;->d:LX/03V;

    iput-object p5, p0, Lcom/facebook/backstage/camera/PreviewView;->e:LX/ALC;

    iput-object p6, p0, Lcom/facebook/backstage/camera/PreviewView;->f:LX/1Eo;

    iput-object p7, p0, Lcom/facebook/backstage/camera/PreviewView;->g:LX/0fO;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/backstage/camera/PreviewView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v0, p0

    check-cast v0, Lcom/facebook/backstage/camera/PreviewView;

    invoke-static {v7}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v1

    check-cast v1, LX/0hB;

    invoke-static {v7}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, LX/0TD;

    invoke-static {v7}, LX/1En;->a(LX/0QB;)LX/1En;

    move-result-object v3

    check-cast v3, LX/1En;

    invoke-static {v7}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v7}, LX/ALC;->b(LX/0QB;)LX/ALC;

    move-result-object v5

    check-cast v5, LX/ALC;

    invoke-static {v7}, LX/1Eo;->a(LX/0QB;)LX/1Eo;

    move-result-object v6

    check-cast v6, LX/1Eo;

    invoke-static {v7}, LX/0fO;->b(LX/0QB;)LX/0fO;

    move-result-object v7

    check-cast v7, LX/0fO;

    invoke-static/range {v0 .. v7}, Lcom/facebook/backstage/camera/PreviewView;->a(Lcom/facebook/backstage/camera/PreviewView;LX/0hB;LX/0TD;LX/1En;LX/03V;LX/ALC;LX/1Eo;LX/0fO;)V

    return-void
.end method

.method public static f(Lcom/facebook/backstage/camera/PreviewView;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1665296
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->t:LX/7gj;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->t:LX/7gj;

    invoke-virtual {v0}, LX/7gj;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1665297
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->n:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->destroyDrawingCache()V

    .line 1665298
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->n:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setDrawingCacheEnabled(Z)V

    .line 1665299
    iget-boolean v0, p0, Lcom/facebook/backstage/camera/PreviewView;->i:Z

    if-eqz v0, :cond_0

    .line 1665300
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->m:Lcom/facebook/backstage/ui/ScalingToggleableEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->setVisibility(I)V

    .line 1665301
    :cond_0
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->m:Lcom/facebook/backstage/ui/ScalingToggleableEditText;

    invoke-virtual {v0, v2}, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->setCursorVisible(Z)V

    .line 1665302
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->t:LX/7gj;

    .line 1665303
    iget-object v1, v0, LX/7gj;->c:Landroid/graphics/Bitmap;

    move-object v0, v1

    .line 1665304
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->t:LX/7gj;

    .line 1665305
    iget-object v1, v0, LX/7gj;->c:Landroid/graphics/Bitmap;

    move-object v0, v1

    .line 1665306
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1665307
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->t:LX/7gj;

    .line 1665308
    iget-object v1, v0, LX/7gj;->c:Landroid/graphics/Bitmap;

    move-object v0, v1

    .line 1665309
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1665310
    :cond_1
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->l:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 1665311
    return-void
.end method

.method public static g(Lcom/facebook/backstage/camera/PreviewView;)V
    .locals 2

    .prologue
    .line 1665294
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->c:LX/1En;

    iget-object v1, p0, Lcom/facebook/backstage/camera/PreviewView;->t:LX/7gj;

    invoke-virtual {v0, v1}, LX/1En;->a(LX/7gj;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->v:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1665295
    return-void
.end method

.method public static h(Lcom/facebook/backstage/camera/PreviewView;)V
    .locals 6

    .prologue
    .line 1665360
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->c:LX/1En;

    iget-object v1, p0, Lcom/facebook/backstage/camera/PreviewView;->t:LX/7gj;

    .line 1665361
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v2

    .line 1665362
    iget-object v3, v0, LX/1En;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v4, Lcom/facebook/audience/util/MediaProcessor$2;

    invoke-direct {v4, v0, v1, v2}, Lcom/facebook/audience/util/MediaProcessor$2;-><init>(LX/1En;LX/7gj;Lcom/google/common/util/concurrent/SettableFuture;)V

    const v5, -0x4e4e3c5c

    invoke-static {v3, v4, v5}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1665363
    move-object v0, v2

    .line 1665364
    iput-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->w:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1665365
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1665291
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->k:Lcom/facebook/backstage/ui/MediaView;

    invoke-virtual {v0}, Lcom/facebook/backstage/ui/MediaView;->c()V

    .line 1665292
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->l:Lcom/facebook/resources/ui/FbButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 1665293
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 1665286
    iput-boolean p1, p0, Lcom/facebook/backstage/camera/PreviewView;->i:Z

    .line 1665287
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    .line 1665288
    :goto_0
    iget-object v1, p0, Lcom/facebook/backstage/camera/PreviewView;->m:Lcom/facebook/backstage/ui/ScalingToggleableEditText;

    invoke-virtual {v1, v0}, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->setVisibility(I)V

    .line 1665289
    return-void

    .line 1665290
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1665284
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->k:Lcom/facebook/backstage/ui/MediaView;

    invoke-virtual {v0}, Lcom/facebook/backstage/ui/MediaView;->b()V

    .line 1665285
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1665282
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->q:Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1665283
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1665272
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->l:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1665273
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->q:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1665274
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->q:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1665275
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->m:Lcom/facebook/backstage/ui/ScalingToggleableEditText;

    invoke-virtual {v0}, LX/AM1;->b()V

    .line 1665276
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->k:Lcom/facebook/backstage/ui/MediaView;

    invoke-virtual {v0}, Lcom/facebook/backstage/ui/MediaView;->a()V

    .line 1665277
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->m:Lcom/facebook/backstage/ui/ScalingToggleableEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1665278
    iput-object v2, p0, Lcom/facebook/backstage/camera/PreviewView;->t:LX/7gj;

    .line 1665279
    iput-object v2, p0, Lcom/facebook/backstage/camera/PreviewView;->v:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1665280
    iput-object v2, p0, Lcom/facebook/backstage/camera/PreviewView;->w:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1665281
    return-void
.end method

.method public setIsHiding(Z)V
    .locals 2

    .prologue
    .line 1665267
    if-eqz p1, :cond_0

    .line 1665268
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->k:Lcom/facebook/backstage/ui/MediaView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/backstage/ui/MediaView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1665269
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->m:Lcom/facebook/backstage/ui/ScalingToggleableEditText;

    invoke-virtual {v0}, LX/AM1;->b()V

    .line 1665270
    :goto_0
    return-void

    .line 1665271
    :cond_0
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->k:Lcom/facebook/backstage/ui/MediaView;

    iget-object v1, p0, Lcom/facebook/backstage/camera/PreviewView;->x:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/backstage/ui/MediaView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public setListener(LX/ALj;)V
    .locals 0

    .prologue
    .line 1665265
    iput-object p1, p0, Lcom/facebook/backstage/camera/PreviewView;->u:LX/ALj;

    .line 1665266
    return-void
.end method

.method public setPreviewAspectRatio(F)V
    .locals 4

    .prologue
    .line 1665252
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->a:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v2

    .line 1665253
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->e:LX/ALC;

    invoke-virtual {v0, p1}, LX/ALC;->a(F)I

    move-result v0

    .line 1665254
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1665255
    const/16 v3, 0x30

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1665256
    iget-object v3, p0, Lcom/facebook/backstage/camera/PreviewView;->n:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1665257
    iget-object v1, p0, Lcom/facebook/backstage/camera/PreviewView;->a:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->d()I

    move-result v1

    sub-int v0, v1, v0

    .line 1665258
    if-gtz v0, :cond_0

    .line 1665259
    const/4 v0, -0x2

    move v1, v0

    .line 1665260
    :goto_0
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->j:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1665261
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 1665262
    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 1665263
    iget-object v1, p0, Lcom/facebook/backstage/camera/PreviewView;->j:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1665264
    return-void

    :cond_0
    move v1, v0

    goto :goto_0
.end method

.method public setShareButtonLabel(I)V
    .locals 1

    .prologue
    .line 1665250
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->l:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    .line 1665251
    return-void
.end method

.method public setShot(LX/7gj;)V
    .locals 4

    .prologue
    .line 1665230
    iput-object p1, p0, Lcom/facebook/backstage/camera/PreviewView;->t:LX/7gj;

    .line 1665231
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->t:LX/7gj;

    .line 1665232
    iget v1, v0, LX/7gj;->k:F

    move v0, v1

    .line 1665233
    invoke-direct {p0, v0}, Lcom/facebook/backstage/camera/PreviewView;->a(F)V

    .line 1665234
    invoke-virtual {p1}, LX/7gj;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1665235
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->k:Lcom/facebook/backstage/ui/MediaView;

    iget-object v1, p0, Lcom/facebook/backstage/camera/PreviewView;->t:LX/7gj;

    .line 1665236
    iget-object v2, v1, LX/7gj;->c:Landroid/graphics/Bitmap;

    move-object v1, v2

    .line 1665237
    iget-object v2, p0, Lcom/facebook/backstage/camera/PreviewView;->t:LX/7gj;

    invoke-virtual {v2}, LX/7gj;->k()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/backstage/camera/PreviewView;->t:LX/7gj;

    .line 1665238
    iget-boolean p1, v3, LX/7gj;->h:Z

    move v3, p1

    .line 1665239
    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/backstage/ui/MediaView;->a(Landroid/graphics/Bitmap;IZ)V

    .line 1665240
    :goto_0
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->l:Lcom/facebook/resources/ui/FbButton;

    iget-object v1, p0, Lcom/facebook/backstage/camera/PreviewView;->s:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1665241
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->q:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v1, p0, Lcom/facebook/backstage/camera/PreviewView;->r:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1665242
    return-void

    .line 1665243
    :cond_0
    iget-object v0, p0, Lcom/facebook/backstage/camera/PreviewView;->k:Lcom/facebook/backstage/ui/MediaView;

    iget-object v1, p0, Lcom/facebook/backstage/camera/PreviewView;->t:LX/7gj;

    .line 1665244
    iget-object v2, v1, LX/7gj;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1665245
    iget-object v2, p0, Lcom/facebook/backstage/camera/PreviewView;->t:LX/7gj;

    .line 1665246
    iget v3, v2, LX/7gj;->g:I

    move v2, v3

    .line 1665247
    iget-object v3, p0, Lcom/facebook/backstage/camera/PreviewView;->t:LX/7gj;

    .line 1665248
    iget-boolean p1, v3, LX/7gj;->h:Z

    move v3, p1

    .line 1665249
    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/backstage/ui/MediaView;->a(Ljava/lang/String;IZ)V

    goto :goto_0
.end method
