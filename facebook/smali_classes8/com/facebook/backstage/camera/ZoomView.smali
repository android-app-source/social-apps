.class public Lcom/facebook/backstage/camera/ZoomView;
.super Landroid/widget/FrameLayout;
.source ""


# instance fields
.field private final a:Landroid/widget/TextView;

.field private final b:LX/ALt;

.field private c:Landroid/view/ViewPropertyAnimator;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1665481
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/backstage/camera/ZoomView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1665482
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1665483
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/backstage/camera/ZoomView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1665484
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v1, -0x2

    const/16 v3, 0xff

    .line 1665485
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1665486
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/backstage/camera/ZoomView;->a:Landroid/widget/TextView;

    .line 1665487
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1665488
    const/16 v1, 0x11

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1665489
    iget-object v1, p0, Lcom/facebook/backstage/camera/ZoomView;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1665490
    iget-object v0, p0, Lcom/facebook/backstage/camera/ZoomView;->a:Landroid/widget/TextView;

    const/4 v1, 0x2

    const/high16 v2, 0x42000000    # 32.0f

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1665491
    iget-object v0, p0, Lcom/facebook/backstage/camera/ZoomView;->a:Landroid/widget/TextView;

    const/16 v1, 0x80

    invoke-static {v1, v3, v3, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1665492
    iget-object v0, p0, Lcom/facebook/backstage/camera/ZoomView;->a:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/camera/ZoomView;->addView(Landroid/view/View;)V

    .line 1665493
    new-instance v0, LX/ALt;

    invoke-direct {v0, p0, p1}, LX/ALt;-><init>(Lcom/facebook/backstage/camera/ZoomView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/backstage/camera/ZoomView;->b:LX/ALt;

    .line 1665494
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 1665495
    iget-object v1, p0, Lcom/facebook/backstage/camera/ZoomView;->b:LX/ALt;

    invoke-virtual {v1, v0}, LX/ALt;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1665496
    iget-object v0, p0, Lcom/facebook/backstage/camera/ZoomView;->b:LX/ALt;

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/camera/ZoomView;->addView(Landroid/view/View;)V

    .line 1665497
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1665498
    iget-object v0, p0, Lcom/facebook/backstage/camera/ZoomView;->c:Landroid/view/ViewPropertyAnimator;

    if-eqz v0, :cond_0

    .line 1665499
    iget-object v0, p0, Lcom/facebook/backstage/camera/ZoomView;->c:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 1665500
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/backstage/camera/ZoomView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/backstage/camera/ZoomView;->c:Landroid/view/ViewPropertyAnimator;

    .line 1665501
    iget-object v0, p0, Lcom/facebook/backstage/camera/ZoomView;->c:Landroid/view/ViewPropertyAnimator;

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 1665502
    iget-object v0, p0, Lcom/facebook/backstage/camera/ZoomView;->c:Landroid/view/ViewPropertyAnimator;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 1665503
    iget-object v0, p0, Lcom/facebook/backstage/camera/ZoomView;->c:Landroid/view/ViewPropertyAnimator;

    new-instance v1, LX/ALr;

    invoke-direct {v1, p0}, LX/ALr;-><init>(Lcom/facebook/backstage/camera/ZoomView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 1665504
    iget-object v0, p0, Lcom/facebook/backstage/camera/ZoomView;->c:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 1665505
    return-void
.end method

.method public final a(II)V
    .locals 7

    .prologue
    const/high16 v2, 0x42c80000    # 100.0f

    .line 1665506
    int-to-float v0, p1

    div-float/2addr v0, v2

    .line 1665507
    int-to-float v1, p2

    div-float/2addr v1, v2

    .line 1665508
    iget-object v2, p0, Lcom/facebook/backstage/camera/ZoomView;->a:Landroid/widget/TextView;

    const-string v3, "%.2f"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1665509
    iget-object v2, p0, Lcom/facebook/backstage/camera/ZoomView;->b:LX/ALt;

    invoke-virtual {v2, v0, v1}, LX/ALt;->a(FF)V

    .line 1665510
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1665511
    iget-object v0, p0, Lcom/facebook/backstage/camera/ZoomView;->c:Landroid/view/ViewPropertyAnimator;

    if-eqz v0, :cond_0

    .line 1665512
    iget-object v0, p0, Lcom/facebook/backstage/camera/ZoomView;->c:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 1665513
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/backstage/camera/ZoomView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/backstage/camera/ZoomView;->c:Landroid/view/ViewPropertyAnimator;

    .line 1665514
    iget-object v0, p0, Lcom/facebook/backstage/camera/ZoomView;->c:Landroid/view/ViewPropertyAnimator;

    const-wide/16 v2, 0x1c2

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 1665515
    iget-object v0, p0, Lcom/facebook/backstage/camera/ZoomView;->c:Landroid/view/ViewPropertyAnimator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 1665516
    iget-object v0, p0, Lcom/facebook/backstage/camera/ZoomView;->c:Landroid/view/ViewPropertyAnimator;

    new-instance v1, LX/ALs;

    invoke-direct {v1, p0}, LX/ALs;-><init>(Lcom/facebook/backstage/camera/ZoomView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 1665517
    iget-object v0, p0, Lcom/facebook/backstage/camera/ZoomView;->c:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 1665518
    return-void
.end method
