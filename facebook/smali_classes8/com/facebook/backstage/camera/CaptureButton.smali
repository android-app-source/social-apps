.class public Lcom/facebook/backstage/camera/CaptureButton;
.super LX/ALb;
.source ""


# instance fields
.field private final a:LX/8YL;

.field private final b:LX/8YL;

.field private final c:LX/8YK;

.field private final d:Landroid/graphics/drawable/Drawable;

.field private final e:Landroid/graphics/drawable/Drawable;

.field public final f:Landroid/os/Handler;

.field public final g:Ljava/lang/Runnable;

.field public h:LX/ALY;

.field public i:LX/ALZ;

.field public j:LX/ALL;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1665060
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/backstage/camera/CaptureButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1665061
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1665062
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/backstage/camera/CaptureButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1665063
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 1665064
    invoke-direct {p0, p1, p2, p3}, LX/ALb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1665065
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/backstage/camera/CaptureButton;->f:Landroid/os/Handler;

    .line 1665066
    new-instance v0, Lcom/facebook/backstage/camera/CaptureButton$1;

    invoke-direct {v0, p0}, Lcom/facebook/backstage/camera/CaptureButton$1;-><init>(Lcom/facebook/backstage/camera/CaptureButton;)V

    iput-object v0, p0, Lcom/facebook/backstage/camera/CaptureButton;->g:Ljava/lang/Runnable;

    .line 1665067
    sget-object v0, LX/ALY;->NONE:LX/ALY;

    iput-object v0, p0, Lcom/facebook/backstage/camera/CaptureButton;->h:LX/ALY;

    .line 1665068
    sget-object v0, LX/ALZ;->PHOTO:LX/ALZ;

    iput-object v0, p0, Lcom/facebook/backstage/camera/CaptureButton;->i:LX/ALZ;

    .line 1665069
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1665070
    const v1, 0x7f020e7c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/backstage/camera/CaptureButton;->d:Landroid/graphics/drawable/Drawable;

    .line 1665071
    const v1, 0x7f020e7b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/backstage/camera/CaptureButton;->e:Landroid/graphics/drawable/Drawable;

    .line 1665072
    invoke-direct {p0, v6}, Lcom/facebook/backstage/camera/CaptureButton;->setPressedColour(Z)V

    .line 1665073
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/ALb;->setLongPressEnabled(Z)V

    .line 1665074
    new-instance v0, LX/AMC;

    const-wide/high16 v2, 0x4034000000000000L    # 20.0

    const-wide/16 v4, 0x0

    invoke-direct {v0, v2, v3, v4, v5}, LX/AMC;-><init>(DD)V

    .line 1665075
    iget-wide v7, v0, LX/AMC;->a:D

    move-wide v2, v7

    .line 1665076
    iget-wide v7, v0, LX/AMC;->b:D

    move-wide v0, v7

    .line 1665077
    invoke-static {v2, v3, v0, v1}, LX/8YL;->a(DD)LX/8YL;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/backstage/camera/CaptureButton;->a:LX/8YL;

    .line 1665078
    new-instance v0, LX/AMC;

    const-wide/high16 v2, 0x4014000000000000L    # 5.0

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    invoke-direct {v0, v2, v3, v4, v5}, LX/AMC;-><init>(DD)V

    .line 1665079
    iget-wide v7, v0, LX/AMC;->a:D

    move-wide v2, v7

    .line 1665080
    iget-wide v7, v0, LX/AMC;->b:D

    move-wide v0, v7

    .line 1665081
    invoke-static {v2, v3, v0, v1}, LX/8YL;->a(DD)LX/8YL;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/backstage/camera/CaptureButton;->b:LX/8YL;

    .line 1665082
    invoke-static {}, LX/8YM;->b()LX/8YM;

    move-result-object v0

    invoke-virtual {v0}, LX/8YH;->a()LX/8YK;

    move-result-object v0

    new-instance v1, LX/ALa;

    invoke-direct {v1, p0}, LX/ALa;-><init>(Lcom/facebook/backstage/camera/CaptureButton;)V

    invoke-virtual {v0, v1}, LX/8YK;->a(LX/7hQ;)LX/8YK;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/backstage/camera/CaptureButton;->a:LX/8YL;

    invoke-virtual {v0, v1}, LX/8YK;->a(LX/8YL;)LX/8YK;

    move-result-object v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/8YK;->a(D)LX/8YK;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/backstage/camera/CaptureButton;->c:LX/8YK;

    .line 1665083
    new-instance v0, LX/ALW;

    invoke-direct {v0, p0}, LX/ALW;-><init>(Lcom/facebook/backstage/camera/CaptureButton;)V

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/camera/CaptureButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1665084
    return-void
.end method

.method public static b(Lcom/facebook/backstage/camera/CaptureButton;)V
    .locals 2

    .prologue
    .line 1665085
    invoke-direct {p0}, Lcom/facebook/backstage/camera/CaptureButton;->d()V

    .line 1665086
    sget-object v0, LX/ALX;->a:[I

    iget-object v1, p0, Lcom/facebook/backstage/camera/CaptureButton;->i:LX/ALZ;

    invoke-virtual {v1}, LX/ALZ;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1665087
    :cond_0
    :goto_0
    return-void

    .line 1665088
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/backstage/camera/CaptureButton;->j:LX/ALL;

    if-eqz v0, :cond_0

    .line 1665089
    iget-object v0, p0, Lcom/facebook/backstage/camera/CaptureButton;->j:LX/ALL;

    invoke-interface {v0}, LX/ALL;->a()V

    goto :goto_0

    .line 1665090
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/backstage/camera/CaptureButton;->j:LX/ALL;

    if-eqz v0, :cond_0

    .line 1665091
    iget-object v0, p0, Lcom/facebook/backstage/camera/CaptureButton;->j:LX/ALL;

    invoke-interface {v0}, LX/ALL;->c()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static c(Lcom/facebook/backstage/camera/CaptureButton;)V
    .locals 4

    .prologue
    .line 1665092
    iget-object v0, p0, Lcom/facebook/backstage/camera/CaptureButton;->h:LX/ALY;

    sget-object v1, LX/ALY;->PRESSING:LX/ALY;

    invoke-virtual {v0, v1}, LX/ALY;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1665093
    :goto_0
    return-void

    .line 1665094
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/backstage/camera/CaptureButton;->setPressedColour(Z)V

    .line 1665095
    sget-object v0, LX/ALY;->PRESSING:LX/ALY;

    iput-object v0, p0, Lcom/facebook/backstage/camera/CaptureButton;->h:LX/ALY;

    .line 1665096
    iget-object v0, p0, Lcom/facebook/backstage/camera/CaptureButton;->c:LX/8YK;

    iget-object v1, p0, Lcom/facebook/backstage/camera/CaptureButton;->a:LX/8YL;

    invoke-virtual {v0, v1}, LX/8YK;->a(LX/8YL;)LX/8YK;

    .line 1665097
    iget-object v0, p0, Lcom/facebook/backstage/camera/CaptureButton;->c:LX/8YK;

    const-wide v2, 0x3feb333333333333L    # 0.85

    invoke-virtual {v0, v2, v3}, LX/8YK;->b(D)LX/8YK;

    goto :goto_0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 1665098
    iget-object v0, p0, Lcom/facebook/backstage/camera/CaptureButton;->h:LX/ALY;

    sget-object v1, LX/ALY;->UNPRESSING:LX/ALY;

    invoke-virtual {v0, v1}, LX/ALY;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1665099
    :goto_0
    return-void

    .line 1665100
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/backstage/camera/CaptureButton;->setPressedColour(Z)V

    .line 1665101
    sget-object v0, LX/ALY;->UNPRESSING:LX/ALY;

    iput-object v0, p0, Lcom/facebook/backstage/camera/CaptureButton;->h:LX/ALY;

    .line 1665102
    iget-object v0, p0, Lcom/facebook/backstage/camera/CaptureButton;->c:LX/8YK;

    iget-object v1, p0, Lcom/facebook/backstage/camera/CaptureButton;->b:LX/8YL;

    invoke-virtual {v0, v1}, LX/8YK;->a(LX/8YL;)LX/8YK;

    .line 1665103
    iget-object v0, p0, Lcom/facebook/backstage/camera/CaptureButton;->c:LX/8YK;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/8YK;->b(D)LX/8YK;

    move-result-object v0

    const-wide/high16 v2, 0x4020000000000000L    # 8.0

    invoke-virtual {v0, v2, v3}, LX/8YK;->c(D)LX/8YK;

    goto :goto_0
.end method

.method private setPressedColour(Z)V
    .locals 1

    .prologue
    .line 1665104
    if-eqz p1, :cond_0

    .line 1665105
    iget-object v0, p0, Lcom/facebook/backstage/camera/CaptureButton;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/camera/CaptureButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1665106
    :goto_0
    return-void

    .line 1665107
    :cond_0
    iget-object v0, p0, Lcom/facebook/backstage/camera/CaptureButton;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/camera/CaptureButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1665108
    iget-object v0, p0, Lcom/facebook/backstage/camera/CaptureButton;->f:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/backstage/camera/CaptureButton;->g:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1665109
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, 0x6d81f043

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1665110
    invoke-virtual {p0}, Lcom/facebook/backstage/camera/CaptureButton;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-super {p0, p1}, LX/ALb;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :cond_0
    const v2, -0x18e3cdd3

    invoke-static {v2, v1}, LX/02F;->a(II)V

    return v0
.end method

.method public setCaptureListener(LX/ALL;)V
    .locals 0

    .prologue
    .line 1665111
    iput-object p1, p0, Lcom/facebook/backstage/camera/CaptureButton;->j:LX/ALL;

    .line 1665112
    return-void
.end method
