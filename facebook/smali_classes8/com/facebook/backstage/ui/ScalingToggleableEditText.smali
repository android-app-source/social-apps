.class public Lcom/facebook/backstage/ui/ScalingToggleableEditText;
.super LX/AM1;
.source ""


# instance fields
.field public c:Landroid/view/ScaleGestureDetector;

.field public d:F

.field public e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1665749
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/backstage/ui/ScalingToggleableEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1665750
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1665751
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/backstage/ui/ScalingToggleableEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1665752
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    .line 1665753
    invoke-direct {p0, p1, p2, p3}, LX/AM1;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1665754
    new-instance v0, Landroid/view/ScaleGestureDetector;

    invoke-virtual {p0}, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, LX/AM0;

    invoke-direct {v2, p0}, LX/AM0;-><init>(Lcom/facebook/backstage/ui/ScalingToggleableEditText;)V

    invoke-direct {v0, v1, v2}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->c:Landroid/view/ScaleGestureDetector;

    .line 1665755
    new-instance v0, LX/ALz;

    invoke-direct {v0, p0}, LX/ALz;-><init>(Lcom/facebook/backstage/ui/ScalingToggleableEditText;)V

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1665756
    return-void
.end method

.method private c()V
    .locals 5

    .prologue
    const/high16 v4, 0x40800000    # 4.0f

    .line 1665757
    invoke-virtual {p0}, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->getHeight()I

    move-result v0

    .line 1665758
    invoke-virtual {p0}, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->getLineCount()I

    move-result v1

    .line 1665759
    add-int/lit8 v1, v1, -0x1

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p0, v1, v2}, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->getLineBounds(ILandroid/graphics/Rect;)I

    move-result v1

    .line 1665760
    invoke-virtual {p0}, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->getTextSize()F

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->scaledDensity:F

    div-float/2addr v2, v3

    .line 1665761
    cmpg-float v3, v2, v4

    if-gez v3, :cond_0

    .line 1665762
    iput v4, p0, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->d:F

    .line 1665763
    iget v0, p0, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->d:F

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->setTextSize(F)V

    .line 1665764
    :goto_0
    return-void

    .line 1665765
    :cond_0
    if-le v1, v0, :cond_1

    cmpl-float v0, v2, v4

    if-lez v0, :cond_1

    .line 1665766
    const/high16 v0, 0x40000000    # 2.0f

    sub-float v0, v2, v0

    iput v0, p0, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->d:F

    .line 1665767
    iget v0, p0, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->d:F

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->setTextSize(F)V

    goto :goto_0

    .line 1665768
    :cond_1
    iput v2, p0, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->d:F

    goto :goto_0
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 1665769
    invoke-super {p0, p1}, LX/AM1;->onDraw(Landroid/graphics/Canvas;)V

    .line 1665770
    invoke-direct {p0}, Lcom/facebook/backstage/ui/ScalingToggleableEditText;->c()V

    .line 1665771
    return-void
.end method
