.class public Lcom/facebook/backstage/ui/MediaView;
.super Landroid/widget/FrameLayout;
.source ""


# instance fields
.field private final a:Lcom/facebook/backstage/ui/PhotoView;

.field private final b:Lcom/facebook/backstage/ui/VideoView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1665564
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/backstage/ui/MediaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1665565
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1665566
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/backstage/ui/MediaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1665567
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1665568
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1665569
    const v0, 0x7f030aae

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1665570
    const v0, 0x7f0d1b47

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/ui/MediaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/backstage/ui/PhotoView;

    iput-object v0, p0, Lcom/facebook/backstage/ui/MediaView;->a:Lcom/facebook/backstage/ui/PhotoView;

    .line 1665571
    const v0, 0x7f0d1b48

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/ui/MediaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/backstage/ui/VideoView;

    iput-object v0, p0, Lcom/facebook/backstage/ui/MediaView;->b:Lcom/facebook/backstage/ui/VideoView;

    .line 1665572
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 1665573
    iget-object v0, p0, Lcom/facebook/backstage/ui/MediaView;->b:Lcom/facebook/backstage/ui/VideoView;

    invoke-virtual {v0}, Lcom/facebook/backstage/ui/VideoView;->c()V

    .line 1665574
    iget-object v0, p0, Lcom/facebook/backstage/ui/MediaView;->a:Lcom/facebook/backstage/ui/PhotoView;

    invoke-virtual {v0, v1}, Lcom/facebook/backstage/ui/PhotoView;->setVisibility(I)V

    .line 1665575
    iget-object v0, p0, Lcom/facebook/backstage/ui/MediaView;->b:Lcom/facebook/backstage/ui/VideoView;

    invoke-virtual {v0, v1}, Lcom/facebook/backstage/ui/VideoView;->setVisibility(I)V

    .line 1665576
    iget-object v0, p0, Lcom/facebook/backstage/ui/MediaView;->a:Lcom/facebook/backstage/ui/PhotoView;

    invoke-virtual {v0}, Lcom/facebook/backstage/ui/PhotoView;->a()V

    .line 1665577
    iget-object v0, p0, Lcom/facebook/backstage/ui/MediaView;->b:Lcom/facebook/backstage/ui/VideoView;

    invoke-virtual {v0}, Lcom/facebook/backstage/ui/VideoView;->a()V

    .line 1665578
    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;IZ)V
    .locals 2

    .prologue
    .line 1665579
    iget-object v0, p0, Lcom/facebook/backstage/ui/MediaView;->a:Lcom/facebook/backstage/ui/PhotoView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/backstage/ui/PhotoView;->setVisibility(I)V

    .line 1665580
    iget-object v0, p0, Lcom/facebook/backstage/ui/MediaView;->b:Lcom/facebook/backstage/ui/VideoView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/backstage/ui/VideoView;->setVisibility(I)V

    .line 1665581
    iget-object v0, p0, Lcom/facebook/backstage/ui/MediaView;->a:Lcom/facebook/backstage/ui/PhotoView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/facebook/backstage/ui/PhotoView;->a(Landroid/graphics/Bitmap;IZ)V

    .line 1665582
    return-void
.end method

.method public final a(Ljava/lang/String;IZ)V
    .locals 2

    .prologue
    .line 1665583
    iget-object v0, p0, Lcom/facebook/backstage/ui/MediaView;->a:Lcom/facebook/backstage/ui/PhotoView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/backstage/ui/PhotoView;->setVisibility(I)V

    .line 1665584
    iget-object v0, p0, Lcom/facebook/backstage/ui/MediaView;->b:Lcom/facebook/backstage/ui/VideoView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/backstage/ui/VideoView;->setVisibility(I)V

    .line 1665585
    iget-object v0, p0, Lcom/facebook/backstage/ui/MediaView;->b:Lcom/facebook/backstage/ui/VideoView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/facebook/backstage/ui/VideoView;->a(Ljava/lang/String;IZ)V

    .line 1665586
    iget-object v0, p0, Lcom/facebook/backstage/ui/MediaView;->b:Lcom/facebook/backstage/ui/VideoView;

    invoke-virtual {v0}, Lcom/facebook/backstage/ui/VideoView;->b()V

    .line 1665587
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1665588
    iget-object v0, p0, Lcom/facebook/backstage/ui/MediaView;->b:Lcom/facebook/backstage/ui/VideoView;

    invoke-virtual {v0}, Lcom/facebook/backstage/ui/VideoView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1665589
    iget-object v0, p0, Lcom/facebook/backstage/ui/MediaView;->b:Lcom/facebook/backstage/ui/VideoView;

    invoke-virtual {v0}, Lcom/facebook/backstage/ui/VideoView;->c()V

    .line 1665590
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1665591
    iget-object v0, p0, Lcom/facebook/backstage/ui/MediaView;->b:Lcom/facebook/backstage/ui/VideoView;

    invoke-virtual {v0}, Lcom/facebook/backstage/ui/VideoView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1665592
    iget-object v0, p0, Lcom/facebook/backstage/ui/MediaView;->b:Lcom/facebook/backstage/ui/VideoView;

    invoke-virtual {v0}, Lcom/facebook/backstage/ui/VideoView;->b()V

    .line 1665593
    :cond_0
    return-void
.end method
