.class public Lcom/facebook/backstage/ui/RotateButton;
.super Landroid/widget/ImageView;
.source ""


# instance fields
.field public final a:LX/8YK;

.field public b:LX/ALG;

.field public c:D


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1665663
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/backstage/ui/RotateButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1665664
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1665665
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/backstage/ui/RotateButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1665666
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1665667
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1665668
    invoke-static {}, LX/8YM;->b()LX/8YM;

    move-result-object v0

    .line 1665669
    invoke-virtual {v0}, LX/8YH;->a()LX/8YK;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/backstage/ui/RotateButton;->a:LX/8YK;

    .line 1665670
    iget-object v0, p0, Lcom/facebook/backstage/ui/RotateButton;->a:LX/8YK;

    sget-object v1, LX/7hM;->a:LX/8YL;

    invoke-virtual {v0, v1}, LX/8YK;->a(LX/8YL;)LX/8YK;

    .line 1665671
    iget-object v0, p0, Lcom/facebook/backstage/ui/RotateButton;->a:LX/8YK;

    new-instance v1, LX/ALy;

    invoke-direct {v1, p0}, LX/ALy;-><init>(Lcom/facebook/backstage/ui/RotateButton;)V

    invoke-virtual {v0, v1}, LX/8YK;->a(LX/7hQ;)LX/8YK;

    .line 1665672
    new-instance v0, LX/ALx;

    invoke-direct {v0, p0}, LX/ALx;-><init>(Lcom/facebook/backstage/ui/RotateButton;)V

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/ui/RotateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1665673
    return-void
.end method

.method public static synthetic a(Lcom/facebook/backstage/ui/RotateButton;D)D
    .locals 3

    .prologue
    .line 1665674
    iget-wide v0, p0, Lcom/facebook/backstage/ui/RotateButton;->c:D

    sub-double/2addr v0, p1

    iput-wide v0, p0, Lcom/facebook/backstage/ui/RotateButton;->c:D

    return-wide v0
.end method


# virtual methods
.method public setListener(LX/ALG;)V
    .locals 0

    .prologue
    .line 1665675
    iput-object p1, p0, Lcom/facebook/backstage/ui/RotateButton;->b:LX/ALG;

    .line 1665676
    return-void
.end method
