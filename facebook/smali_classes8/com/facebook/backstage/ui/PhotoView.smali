.class public Lcom/facebook/backstage/ui/PhotoView;
.super Landroid/widget/ImageView;
.source ""


# static fields
.field private static final d:Ljava/lang/String;


# instance fields
.field public a:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/quicklog/QuickPerformanceLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:Landroid/graphics/Bitmap;

.field private f:I

.field private g:Z

.field private h:Lcom/google/common/util/concurrent/ListenableFuture;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1665597
    const-class v0, Lcom/facebook/backstage/ui/PhotoView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/backstage/ui/PhotoView;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1665598
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/backstage/ui/PhotoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1665599
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1665600
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/backstage/ui/PhotoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1665601
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1665602
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1665603
    const-class v0, Lcom/facebook/backstage/ui/PhotoView;

    invoke-static {v0, p0}, Lcom/facebook/backstage/ui/PhotoView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1665604
    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/ui/PhotoView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1665605
    return-void
.end method

.method private a(IIIZ)Landroid/graphics/Matrix;
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const v9, 0x3c23d70a    # 0.01f

    .line 1665606
    invoke-virtual {p0}, Lcom/facebook/backstage/ui/PhotoView;->getWidth()I

    move-result v5

    .line 1665607
    invoke-virtual {p0}, Lcom/facebook/backstage/ui/PhotoView;->getHeight()I

    move-result v6

    .line 1665608
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 1665609
    sub-int v0, v5, p1

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    sub-int v3, v6, p2

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {v7, v0, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1665610
    if-eqz p3, :cond_1

    .line 1665611
    int-to-float v0, p3

    div-int/lit8 v3, v5, 0x2

    int-to-float v3, v3

    div-int/lit8 v4, v6, 0x2

    int-to-float v4, v4

    invoke-virtual {v7, v0, v3, v4}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 1665612
    const/16 v0, 0x5a

    if-eq p3, v0, :cond_0

    const/16 v0, 0x10e

    if-ne p3, v0, :cond_1

    :cond_0
    move v10, p1

    move p1, p2

    move p2, v10

    .line 1665613
    :cond_1
    int-to-float v0, v5

    int-to-float v3, p1

    div-float/2addr v0, v3

    .line 1665614
    int-to-float v3, v6

    int-to-float v4, p2

    div-float/2addr v3, v4

    .line 1665615
    int-to-float v4, p2

    mul-float/2addr v4, v0

    int-to-float v8, v6

    cmpl-float v4, v4, v8

    if-ltz v4, :cond_3

    move v3, v0

    move v4, v0

    .line 1665616
    :goto_0
    int-to-float v0, p2

    mul-float/2addr v0, v3

    add-float/2addr v0, v9

    int-to-float v8, v6

    cmpl-float v0, v0, v8

    if-lez v0, :cond_4

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1665617
    int-to-float v0, p1

    mul-float/2addr v0, v4

    add-float/2addr v0, v9

    int-to-float v8, v5

    cmpl-float v0, v0, v8

    if-lez v0, :cond_5

    :goto_2
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1665618
    div-int/lit8 v0, v5, 0x2

    int-to-float v0, v0

    div-int/lit8 v1, v6, 0x2

    int-to-float v1, v1

    invoke-virtual {v7, v4, v3, v0, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 1665619
    if-eqz p4, :cond_2

    .line 1665620
    const/high16 v0, -0x40800000    # -1.0f

    const/high16 v1, 0x3f800000    # 1.0f

    div-int/lit8 v2, v5, 0x2

    int-to-float v2, v2

    div-int/lit8 v3, v6, 0x2

    int-to-float v3, v3

    invoke-virtual {v7, v0, v1, v2, v3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 1665621
    :cond_2
    return-object v7

    :cond_3
    move v4, v3

    .line 1665622
    goto :goto_0

    :cond_4
    move v0, v2

    .line 1665623
    goto :goto_1

    :cond_5
    move v1, v2

    .line 1665624
    goto :goto_2
.end method

.method private static a(Lcom/facebook/backstage/ui/PhotoView;LX/0TD;LX/0TD;Lcom/facebook/quicklog/QuickPerformanceLogger;)V
    .locals 0

    .prologue
    .line 1665625
    iput-object p1, p0, Lcom/facebook/backstage/ui/PhotoView;->a:LX/0TD;

    iput-object p2, p0, Lcom/facebook/backstage/ui/PhotoView;->b:LX/0TD;

    iput-object p3, p0, Lcom/facebook/backstage/ui/PhotoView;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/backstage/ui/PhotoView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/backstage/ui/PhotoView;

    invoke-static {v2}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v0

    check-cast v0, LX/0TD;

    invoke-static {v2}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, LX/0TD;

    invoke-static {v2}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v2

    check-cast v2, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/backstage/ui/PhotoView;->a(Lcom/facebook/backstage/ui/PhotoView;LX/0TD;LX/0TD;Lcom/facebook/quicklog/QuickPerformanceLogger;)V

    return-void
.end method

.method public static b(Lcom/facebook/backstage/ui/PhotoView;)V
    .locals 4

    .prologue
    .line 1665626
    iget-object v0, p0, Lcom/facebook/backstage/ui/PhotoView;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/backstage/ui/PhotoView;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    iget v2, p0, Lcom/facebook/backstage/ui/PhotoView;->f:I

    iget-boolean v3, p0, Lcom/facebook/backstage/ui/PhotoView;->g:Z

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/facebook/backstage/ui/PhotoView;->a(IIIZ)Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/ui/PhotoView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 1665627
    iget-object v0, p0, Lcom/facebook/backstage/ui/PhotoView;->e:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/ui/PhotoView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1665628
    iget-object v0, p0, Lcom/facebook/backstage/ui/PhotoView;->c:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x5e0001

    const/16 v2, 0x3c

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 1665629
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1665630
    iget-object v0, p0, Lcom/facebook/backstage/ui/PhotoView;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 1665631
    iget-object v0, p0, Lcom/facebook/backstage/ui/PhotoView;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1665632
    :cond_0
    iget-object v0, p0, Lcom/facebook/backstage/ui/PhotoView;->e:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 1665633
    iget-object v0, p0, Lcom/facebook/backstage/ui/PhotoView;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1665634
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/backstage/ui/PhotoView;->e:Landroid/graphics/Bitmap;

    .line 1665635
    :cond_1
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/ui/PhotoView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 1665636
    const v0, 0x106000d

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/ui/PhotoView;->setImageResource(I)V

    .line 1665637
    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;IZ)V
    .locals 1

    .prologue
    .line 1665638
    iput-object p1, p0, Lcom/facebook/backstage/ui/PhotoView;->e:Landroid/graphics/Bitmap;

    .line 1665639
    iput p2, p0, Lcom/facebook/backstage/ui/PhotoView;->f:I

    .line 1665640
    iput-boolean p3, p0, Lcom/facebook/backstage/ui/PhotoView;->g:Z

    .line 1665641
    invoke-virtual {p0}, Lcom/facebook/backstage/ui/PhotoView;->getWidth()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/backstage/ui/PhotoView;->getHeight()I

    move-result v0

    if-nez v0, :cond_1

    .line 1665642
    :cond_0
    new-instance v0, Lcom/facebook/backstage/ui/PhotoView$1;

    invoke-direct {v0, p0}, Lcom/facebook/backstage/ui/PhotoView$1;-><init>(Lcom/facebook/backstage/ui/PhotoView;)V

    .line 1665643
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result p1

    if-lez p1, :cond_2

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result p1

    if-lez p1, :cond_2

    .line 1665644
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1665645
    :goto_0
    return-void

    .line 1665646
    :cond_1
    invoke-static {p0}, Lcom/facebook/backstage/ui/PhotoView;->b(Lcom/facebook/backstage/ui/PhotoView;)V

    goto :goto_0

    .line 1665647
    :cond_2
    new-instance p1, LX/AMD;

    invoke-direct {p1, p0, v0}, LX/AMD;-><init>(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 1665648
    if-nez p0, :cond_3

    .line 1665649
    :goto_1
    goto :goto_0

    .line 1665650
    :cond_3
    invoke-virtual {p0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_1
.end method
