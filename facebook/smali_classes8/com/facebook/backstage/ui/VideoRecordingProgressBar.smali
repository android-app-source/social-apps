.class public Lcom/facebook/backstage/ui/VideoRecordingProgressBar;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field private final a:Landroid/widget/ProgressBar;

.field private final b:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1665854
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/backstage/ui/VideoRecordingProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1665855
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1665856
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/backstage/ui/VideoRecordingProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1665857
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1665858
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1665859
    const v0, 0x7f0315b5

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1665860
    const v0, 0x7f0d30f1

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/ui/VideoRecordingProgressBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/backstage/ui/VideoRecordingProgressBar;->a:Landroid/widget/ProgressBar;

    .line 1665861
    const v0, 0x7f0d30f2

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/ui/VideoRecordingProgressBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/backstage/ui/VideoRecordingProgressBar;->b:Landroid/widget/ProgressBar;

    .line 1665862
    return-void
.end method


# virtual methods
.method public setMax(I)V
    .locals 1

    .prologue
    .line 1665863
    iget-object v0, p0, Lcom/facebook/backstage/ui/VideoRecordingProgressBar;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 1665864
    iget-object v0, p0, Lcom/facebook/backstage/ui/VideoRecordingProgressBar;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 1665865
    return-void
.end method

.method public setProgress(I)V
    .locals 1

    .prologue
    .line 1665866
    iget-object v0, p0, Lcom/facebook/backstage/ui/VideoRecordingProgressBar;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 1665867
    iget-object v0, p0, Lcom/facebook/backstage/ui/VideoRecordingProgressBar;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 1665868
    return-void
.end method
