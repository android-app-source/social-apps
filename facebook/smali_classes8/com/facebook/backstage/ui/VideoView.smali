.class public Lcom/facebook/backstage/ui/VideoView;
.super Landroid/widget/FrameLayout;
.source ""

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final c:Landroid/view/TextureView;

.field private d:Ljava/lang/String;

.field public e:I

.field public f:Z

.field private g:Landroid/media/MediaPlayer;

.field public volatile h:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1665943
    const-class v0, Lcom/facebook/backstage/ui/VideoView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/backstage/ui/VideoView;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1665941
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/backstage/ui/VideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1665942
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1665939
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/backstage/ui/VideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1665940
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1665932
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1665933
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/facebook/backstage/ui/VideoView;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1665934
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/backstage/ui/VideoView;->h:Z

    .line 1665935
    const v0, 0x7f0315bd

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1665936
    const v0, 0x7f0d2df0

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/ui/VideoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/TextureView;

    iput-object v0, p0, Lcom/facebook/backstage/ui/VideoView;->c:Landroid/view/TextureView;

    .line 1665937
    iget-object v0, p0, Lcom/facebook/backstage/ui/VideoView;->c:Landroid/view/TextureView;

    invoke-virtual {v0, p0}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 1665938
    return-void
.end method

.method private a(IIIZ)Landroid/graphics/Matrix;
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    .line 1665913
    if-lez p1, :cond_3

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1665914
    if-lez p2, :cond_4

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1665915
    invoke-virtual {p0}, Lcom/facebook/backstage/ui/VideoView;->getWidth()I

    move-result v2

    .line 1665916
    invoke-virtual {p0}, Lcom/facebook/backstage/ui/VideoView;->getHeight()I

    move-result v4

    .line 1665917
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 1665918
    if-eqz p3, :cond_1

    .line 1665919
    int-to-float v0, p3

    div-int/lit8 v1, v2, 0x2

    int-to-float v1, v1

    div-int/lit8 v6, v4, 0x2

    int-to-float v6, v6

    invoke-virtual {v5, v0, v1, v6}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 1665920
    const/16 v0, 0x5a

    if-eq p3, v0, :cond_0

    const/16 v0, 0x10e

    if-ne p3, v0, :cond_1

    :cond_0
    move v8, p1

    move p1, p2

    move p2, v8

    .line 1665921
    :cond_1
    int-to-float v0, p1

    int-to-float v1, p2

    div-float/2addr v0, v1

    .line 1665922
    int-to-float v1, v2

    int-to-float v6, v4

    div-float/2addr v1, v6

    .line 1665923
    cmpg-float v6, v1, v0

    if-gez v6, :cond_5

    .line 1665924
    div-float/2addr v0, v1

    move v1, v0

    move v0, v3

    .line 1665925
    :goto_2
    div-int/lit8 v6, v2, 0x2

    int-to-float v6, v6

    div-int/lit8 v7, v4, 0x2

    int-to-float v7, v7

    invoke-virtual {v5, v1, v0, v6, v7}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 1665926
    if-eqz p4, :cond_2

    .line 1665927
    const/high16 v0, -0x40800000    # -1.0f

    div-int/lit8 v1, v2, 0x2

    int-to-float v1, v1

    div-int/lit8 v2, v4, 0x2

    int-to-float v2, v2

    invoke-virtual {v5, v0, v3, v1, v2}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 1665928
    :cond_2
    return-object v5

    :cond_3
    move v0, v2

    .line 1665929
    goto :goto_0

    :cond_4
    move v1, v2

    .line 1665930
    goto :goto_1

    .line 1665931
    :cond_5
    div-float v0, v1, v0

    move v1, v3

    goto :goto_2
.end method

.method public static synthetic a(Lcom/facebook/backstage/ui/VideoView;IIIZ)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 1665912
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/backstage/ui/VideoView;->a(IIIZ)Landroid/graphics/Matrix;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized d()V
    .locals 3

    .prologue
    .line 1665903
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/backstage/ui/VideoView;->g:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/facebook/backstage/ui/VideoView;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 1665904
    iget-object v0, p0, Lcom/facebook/backstage/ui/VideoView;->g:Landroid/media/MediaPlayer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 1665905
    iget-object v0, p0, Lcom/facebook/backstage/ui/VideoView;->g:Landroid/media/MediaPlayer;

    new-instance v1, LX/AMB;

    invoke-direct {v1, p0}, LX/AMB;-><init>(Lcom/facebook/backstage/ui/VideoView;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 1665906
    iget-object v0, p0, Lcom/facebook/backstage/ui/VideoView;->g:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1665907
    :goto_0
    monitor-exit p0

    return-void

    .line 1665908
    :catch_0
    move-exception v0

    .line 1665909
    :try_start_1
    iget-object v1, p0, Lcom/facebook/backstage/ui/VideoView;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1665910
    sget-object v1, Lcom/facebook/backstage/ui/VideoView;->a:Ljava/lang/String;

    const-string v2, "Failed to initialize the media player"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1665911
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1665944
    iget-object v0, p0, Lcom/facebook/backstage/ui/VideoView;->g:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 1665945
    iget-object v0, p0, Lcom/facebook/backstage/ui/VideoView;->g:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 1665946
    iget-object v0, p0, Lcom/facebook/backstage/ui/VideoView;->g:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 1665947
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/backstage/ui/VideoView;->g:Landroid/media/MediaPlayer;

    .line 1665948
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;IZ)V
    .locals 3

    .prologue
    .line 1665893
    iget-object v0, p0, Lcom/facebook/backstage/ui/VideoView;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/facebook/backstage/ui/VideoView;->e:I

    if-ne v0, p2, :cond_1

    iget-boolean v0, p0, Lcom/facebook/backstage/ui/VideoView;->f:Z

    if-ne v0, p3, :cond_1

    iget-object v0, p0, Lcom/facebook/backstage/ui/VideoView;->g:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 1665894
    :cond_0
    :goto_0
    return-void

    .line 1665895
    :cond_1
    iput-object p1, p0, Lcom/facebook/backstage/ui/VideoView;->d:Ljava/lang/String;

    .line 1665896
    iput p2, p0, Lcom/facebook/backstage/ui/VideoView;->e:I

    .line 1665897
    iput-boolean p3, p0, Lcom/facebook/backstage/ui/VideoView;->f:Z

    .line 1665898
    iget-object v0, p0, Lcom/facebook/backstage/ui/VideoView;->c:Landroid/view/TextureView;

    invoke-virtual {v0}, Landroid/view/TextureView;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1665899
    iget-object v0, p0, Lcom/facebook/backstage/ui/VideoView;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1665900
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/facebook/backstage/ui/VideoView;->g:Landroid/media/MediaPlayer;

    .line 1665901
    iget-object v0, p0, Lcom/facebook/backstage/ui/VideoView;->g:Landroid/media/MediaPlayer;

    new-instance v1, Landroid/view/Surface;

    iget-object v2, p0, Lcom/facebook/backstage/ui/VideoView;->c:Landroid/view/TextureView;

    invoke-virtual {v2}, Landroid/view/TextureView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setSurface(Landroid/view/Surface;)V

    .line 1665902
    invoke-direct {p0}, Lcom/facebook/backstage/ui/VideoView;->d()V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1665888
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/backstage/ui/VideoView;->h:Z

    .line 1665889
    iget-object v0, p0, Lcom/facebook/backstage/ui/VideoView;->g:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/backstage/ui/VideoView;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1665890
    iget-object v0, p0, Lcom/facebook/backstage/ui/VideoView;->g:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1665891
    iget-object v0, p0, Lcom/facebook/backstage/ui/VideoView;->g:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 1665892
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1665883
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/backstage/ui/VideoView;->h:Z

    .line 1665884
    iget-object v0, p0, Lcom/facebook/backstage/ui/VideoView;->g:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/backstage/ui/VideoView;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1665885
    iget-object v0, p0, Lcom/facebook/backstage/ui/VideoView;->g:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1665886
    iget-object v0, p0, Lcom/facebook/backstage/ui/VideoView;->g:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 1665887
    :cond_0
    return-void
.end method

.method public final onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 2

    .prologue
    .line 1665878
    iget-object v0, p0, Lcom/facebook/backstage/ui/VideoView;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/backstage/ui/VideoView;->g:Landroid/media/MediaPlayer;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/backstage/ui/VideoView;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1665879
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/facebook/backstage/ui/VideoView;->g:Landroid/media/MediaPlayer;

    .line 1665880
    iget-object v0, p0, Lcom/facebook/backstage/ui/VideoView;->g:Landroid/media/MediaPlayer;

    new-instance v1, Landroid/view/Surface;

    invoke-direct {v1, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setSurface(Landroid/view/Surface;)V

    .line 1665881
    invoke-direct {p0}, Lcom/facebook/backstage/ui/VideoView;->d()V

    .line 1665882
    :cond_0
    return-void
.end method

.method public final onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1

    .prologue
    .line 1665877
    const/4 v0, 0x1

    return v0
.end method

.method public final onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 0

    .prologue
    .line 1665876
    return-void
.end method

.method public final onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    .prologue
    .line 1665875
    return-void
.end method
