.class public Lcom/facebook/backstage/ui/ToggleTextButton;
.super LX/ALb;
.source ""


# instance fields
.field public a:LX/ALN;

.field public b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1665846
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/backstage/ui/ToggleTextButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1665847
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1665844
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/backstage/ui/ToggleTextButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1665845
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1665833
    invoke-direct {p0, p1, p2, p3}, LX/ALb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1665834
    new-instance v0, LX/AM8;

    invoke-direct {v0, p0}, LX/AM8;-><init>(Lcom/facebook/backstage/ui/ToggleTextButton;)V

    .line 1665835
    iput-object v0, p0, LX/ALb;->f:LX/AM8;

    .line 1665836
    return-void
.end method


# virtual methods
.method public setToggleState(Z)V
    .locals 2

    .prologue
    .line 1665839
    iput-boolean p1, p0, Lcom/facebook/backstage/ui/ToggleTextButton;->b:Z

    .line 1665840
    iget-boolean v0, p0, Lcom/facebook/backstage/ui/ToggleTextButton;->b:Z

    if-eqz v0, :cond_0

    .line 1665841
    invoke-virtual {p0}, Lcom/facebook/backstage/ui/ToggleTextButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a078d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/ui/ToggleTextButton;->setTextColor(I)V

    .line 1665842
    :goto_0
    return-void

    .line 1665843
    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/facebook/backstage/ui/ToggleTextButton;->setTextColor(I)V

    goto :goto_0
.end method

.method public setToggleTextButtonListener(LX/ALN;)V
    .locals 0

    .prologue
    .line 1665837
    iput-object p1, p0, Lcom/facebook/backstage/ui/ToggleTextButton;->a:LX/ALN;

    .line 1665838
    return-void
.end method
