.class public Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1529173
    new-instance v0, LX/9j6;

    invoke-direct {v0}, LX/9j6;-><init>()V

    sput-object v0, Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1529174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1529175
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;->a:Ljava/lang/String;

    .line 1529176
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;->b:Ljava/lang/String;

    .line 1529177
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;->c:J

    .line 1529178
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 1

    .prologue
    .line 1529179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1529180
    iput-object p1, p0, Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;->a:Ljava/lang/String;

    .line 1529181
    iput-object p2, p0, Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;->b:Ljava/lang/String;

    .line 1529182
    iput-wide p3, p0, Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;->c:J

    .line 1529183
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1529184
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1529185
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PlacePickerSessionData{placePickerSessionId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", composerSessionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", placePickerStartTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;->c:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1529186
    iget-object v0, p0, Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1529187
    iget-object v0, p0, Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1529188
    iget-wide v0, p0, Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1529189
    return-void
.end method
