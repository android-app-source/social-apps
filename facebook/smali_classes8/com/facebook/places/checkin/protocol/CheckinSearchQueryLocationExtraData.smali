.class public Lcom/facebook/places/checkin/protocol/CheckinSearchQueryLocationExtraData;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/places/checkin/protocol/CheckinSearchQueryLocationExtraDataDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/places/checkin/protocol/CheckinSearchQueryLocationExtraDataSerializer;
.end annotation


# instance fields
.field public final mBle:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "ble"
    .end annotation
.end field

.field public final mWifi:Lcom/facebook/places/checkin/protocol/CheckinSearchQueryLocationExtraData$CheckinSearchQueryLocationExtraDataWifi;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "wifi"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1530078
    const-class v0, Lcom/facebook/places/checkin/protocol/CheckinSearchQueryLocationExtraDataDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1530079
    const-class v0, Lcom/facebook/places/checkin/protocol/CheckinSearchQueryLocationExtraDataSerializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1530080
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1530081
    iput-object v0, p0, Lcom/facebook/places/checkin/protocol/CheckinSearchQueryLocationExtraData;->mWifi:Lcom/facebook/places/checkin/protocol/CheckinSearchQueryLocationExtraData$CheckinSearchQueryLocationExtraDataWifi;

    .line 1530082
    iput-object v0, p0, Lcom/facebook/places/checkin/protocol/CheckinSearchQueryLocationExtraData;->mBle:Ljava/lang/String;

    .line 1530083
    return-void
.end method

.method public constructor <init>(Lcom/facebook/places/checkin/protocol/CheckinSearchQueryLocationExtraData$CheckinSearchQueryLocationExtraDataWifi;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1530084
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1530085
    iput-object p1, p0, Lcom/facebook/places/checkin/protocol/CheckinSearchQueryLocationExtraData;->mWifi:Lcom/facebook/places/checkin/protocol/CheckinSearchQueryLocationExtraData$CheckinSearchQueryLocationExtraDataWifi;

    .line 1530086
    iput-object p2, p0, Lcom/facebook/places/checkin/protocol/CheckinSearchQueryLocationExtraData;->mBle:Ljava/lang/String;

    .line 1530087
    return-void
.end method
