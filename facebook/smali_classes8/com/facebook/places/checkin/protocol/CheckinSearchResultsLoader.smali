.class public Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Lcom/facebook/location/FbLocationOperationParams;

.field public static final c:Lcom/facebook/location/FbLocationOperationParams;


# instance fields
.field public final d:LX/9jn;

.field public final e:LX/9jZ;

.field public final f:LX/0tX;

.field public final g:LX/9kB;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/9kB",
            "<",
            "LX/9jc;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1sS;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljava/util/concurrent/Executor;

.field public final j:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "LX/9jN;",
            "LX/9jN;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const-wide/16 v4, 0x3e8

    const/high16 v1, 0x447a0000    # 1000.0f

    .line 1530275
    const-class v0, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;

    sput-object v0, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->a:Ljava/lang/Class;

    .line 1530276
    sget-object v0, LX/0yF;->HIGH_ACCURACY:LX/0yF;

    invoke-static {v0}, Lcom/facebook/location/FbLocationOperationParams;->a(LX/0yF;)LX/1S7;

    move-result-object v0

    .line 1530277
    iput-wide v4, v0, LX/1S7;->d:J

    .line 1530278
    move-object v0, v0

    .line 1530279
    iput v1, v0, LX/1S7;->c:F

    .line 1530280
    move-object v0, v0

    .line 1530281
    const-wide/32 v2, 0x493e0

    .line 1530282
    iput-wide v2, v0, LX/1S7;->b:J

    .line 1530283
    move-object v0, v0

    .line 1530284
    invoke-virtual {v0}, LX/1S7;->a()Lcom/facebook/location/FbLocationOperationParams;

    move-result-object v0

    sput-object v0, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->b:Lcom/facebook/location/FbLocationOperationParams;

    .line 1530285
    sget-object v0, LX/0yF;->HIGH_ACCURACY:LX/0yF;

    invoke-static {v0}, Lcom/facebook/location/FbLocationOperationParams;->a(LX/0yF;)LX/1S7;

    move-result-object v0

    const-wide/16 v2, 0x2710

    .line 1530286
    iput-wide v2, v0, LX/1S7;->d:J

    .line 1530287
    move-object v0, v0

    .line 1530288
    iput v1, v0, LX/1S7;->c:F

    .line 1530289
    move-object v0, v0

    .line 1530290
    iput-wide v4, v0, LX/1S7;->b:J

    .line 1530291
    move-object v0, v0

    .line 1530292
    invoke-virtual {v0}, LX/1S7;->a()Lcom/facebook/location/FbLocationOperationParams;

    move-result-object v0

    sput-object v0, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->c:Lcom/facebook/location/FbLocationOperationParams;

    return-void
.end method

.method public constructor <init>(LX/9jn;LX/9jZ;LX/0tX;LX/9kB;LX/0Or;Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p6    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThreadImmediate;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/9jn;",
            "LX/9jZ;",
            "LX/0tX;",
            "LX/9kB;",
            "LX/0Or",
            "<",
            "LX/1sS;",
            ">;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1530237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1530238
    new-instance v0, LX/9ja;

    invoke-direct {v0, p0}, LX/9ja;-><init>(Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;)V

    iput-object v0, p0, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->j:LX/0QK;

    .line 1530239
    iput-object p1, p0, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->d:LX/9jn;

    .line 1530240
    iput-object p2, p0, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->e:LX/9jZ;

    .line 1530241
    iput-object p3, p0, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->f:LX/0tX;

    .line 1530242
    iput-object p4, p0, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->g:LX/9kB;

    .line 1530243
    iput-object p5, p0, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->h:LX/0Or;

    .line 1530244
    iput-object p6, p0, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->i:Ljava/util/concurrent/Executor;

    .line 1530245
    return-void
.end method

.method public static a(Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;Lcom/facebook/location/FbLocationOperationParams;LX/0TF;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/location/FbLocationOperationParams;",
            "LX/0TF",
            "<",
            "Lcom/facebook/location/ImmutableLocation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1530246
    iget-object v0, p0, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1sS;

    .line 1530247
    sget-object v1, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->a:Ljava/lang/Class;

    const-string v2, "checkin"

    invoke-static {v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/1sS;->a(Lcom/facebook/location/FbLocationOperationParams;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1530248
    iget-object v1, p0, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->g:LX/9kB;

    sget-object v2, LX/9jc;->NEARBY_LOCATION:LX/9jc;

    invoke-static {p2}, LX/0Vd;->of(LX/0TF;)LX/0Vd;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1530249
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;
    .locals 7

    .prologue
    .line 1530250
    new-instance v0, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;

    invoke-static {p0}, LX/9jn;->a(LX/0QB;)LX/9jn;

    move-result-object v1

    check-cast v1, LX/9jn;

    invoke-static {p0}, LX/9jZ;->a(LX/0QB;)LX/9jZ;

    move-result-object v2

    check-cast v2, LX/9jZ;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {p0}, LX/9kB;->c(LX/0QB;)LX/9kB;

    move-result-object v4

    check-cast v4, LX/9kB;

    const/16 v5, 0xc81

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {p0}, LX/0W1;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/Executor;

    invoke-direct/range {v0 .. v6}, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;-><init>(LX/9jn;LX/9jZ;LX/0tX;LX/9kB;LX/0Or;Ljava/util/concurrent/Executor;)V

    .line 1530251
    return-object v0
.end method


# virtual methods
.method public final a(LX/0TF;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TF",
            "<",
            "Lcom/facebook/location/ImmutableLocation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1530252
    sget-object v0, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->b:Lcom/facebook/location/FbLocationOperationParams;

    invoke-static {p0, v0, p1}, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->a(Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;Lcom/facebook/location/FbLocationOperationParams;LX/0TF;)V

    .line 1530253
    return-void
.end method

.method public final a(LX/9jo;LX/0TF;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/9jo;",
            "LX/0TF",
            "<",
            "LX/9jN;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1530254
    invoke-static {p2}, LX/0Vd;->of(LX/0TF;)LX/0Vd;

    move-result-object v0

    .line 1530255
    iget-object v1, p0, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->d:LX/9jn;

    .line 1530256
    iget-object v2, v1, LX/9jn;->a:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->a()V

    .line 1530257
    iget-object v2, v1, LX/9jn;->b:LX/2SA;

    new-instance v3, LX/9jl;

    invoke-direct {v3, v1}, LX/9jl;-><init>(LX/9jn;)V

    invoke-static {v2, v3}, LX/0Ph;->a(Ljava/lang/Iterable;LX/0Rl;)Z

    .line 1530258
    iget-object v2, v1, LX/9jn;->b:LX/2SA;

    new-instance v3, LX/9jk;

    invoke-direct {v3, v1, p1}, LX/9jk;-><init>(LX/9jn;LX/9jo;)V

    .line 1530259
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v1, v3}, LX/0RZ;->f(Ljava/util/Iterator;LX/0Rl;)LX/0am;

    move-result-object v1

    move-object v2, v1

    .line 1530260
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/9jm;

    iget-object v2, v2, LX/9jm;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    :goto_0
    move-object v1, v2

    .line 1530261
    if-eqz v1, :cond_0

    .line 1530262
    iget-object v2, p0, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->j:LX/0QK;

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v3

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 1530263
    :cond_0
    move-object v1, v1

    .line 1530264
    if-eqz v1, :cond_1

    .line 1530265
    iget-object v2, p0, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->i:Ljava/util/concurrent/Executor;

    invoke-static {v1, v0, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1530266
    :goto_1
    return-void

    .line 1530267
    :cond_1
    iget-object v1, p0, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->e:LX/9jZ;

    invoke-virtual {v1, p1}, LX/9jZ;->a(LX/9jo;)LX/0zO;

    move-result-object v1

    .line 1530268
    iget-object v2, p0, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->f:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    invoke-static {v1}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, LX/9jb;

    invoke-direct {v2, p0, p1}, LX/9jb;-><init>(Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;LX/9jo;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v3

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v1, v1

    .line 1530269
    iget-object v2, p0, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->d:LX/9jn;

    .line 1530270
    iget-object v3, v2, LX/9jn;->a:LX/0Sh;

    invoke-virtual {v3}, LX/0Sh;->a()V

    .line 1530271
    iget-object v3, v2, LX/9jn;->b:LX/2SA;

    new-instance v4, LX/9jm;

    invoke-direct {v4, p1, v1}, LX/9jm;-><init>(LX/9jo;Lcom/google/common/util/concurrent/ListenableFuture;)V

    invoke-virtual {v3, v4}, LX/306;->add(Ljava/lang/Object;)Z

    .line 1530272
    iget-object v2, p0, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->g:LX/9kB;

    sget-object v3, LX/9jc;->NEARBY_PLACES:LX/9jc;

    invoke-virtual {v2, v3, v1, v0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1530273
    iget-object v0, p0, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->g:LX/9kB;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 1530274
    return-void
.end method
