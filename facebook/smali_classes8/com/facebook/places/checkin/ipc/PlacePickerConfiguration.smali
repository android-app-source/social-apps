.class public Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final A:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final B:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final a:Z

.field public final b:Z

.field public final c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Lcom/facebook/ipc/composer/model/ComposerLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Z

.field public final i:Z

.field public final j:Z

.field public final k:Z

.field public final l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final o:Lcom/facebook/composer/minutiae/model/MinutiaeObject;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final p:Lcom/facebook/ipc/composer/model/ComposerLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final q:LX/9jG;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final r:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;"
        }
    .end annotation
.end field

.field public final s:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final t:Z

.field public final u:Z

.field public final v:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final x:Z

.field public final y:Z

.field public final z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1529656
    new-instance v0, LX/9jE;

    invoke-direct {v0}, LX/9jE;-><init>()V

    sput-object v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/9jF;)V
    .locals 1

    .prologue
    .line 1529626
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1529627
    iget-boolean v0, p1, LX/9jF;->a:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->a:Z

    .line 1529628
    iget-boolean v0, p1, LX/9jF;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->b:Z

    .line 1529629
    iget-object v0, p1, LX/9jF;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iput-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1529630
    iget-object v0, p1, LX/9jF;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->d:Ljava/lang/String;

    .line 1529631
    iget-object v0, p1, LX/9jF;->e:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    iput-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->e:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1529632
    iget-object v0, p1, LX/9jF;->f:Lcom/facebook/ipc/composer/model/ComposerLocation;

    iput-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->f:Lcom/facebook/ipc/composer/model/ComposerLocation;

    .line 1529633
    iget-object v0, p1, LX/9jF;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->g:Ljava/lang/String;

    .line 1529634
    iget-boolean v0, p1, LX/9jF;->h:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->h:Z

    .line 1529635
    iget-boolean v0, p1, LX/9jF;->i:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->i:Z

    .line 1529636
    iget-boolean v0, p1, LX/9jF;->j:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->j:Z

    .line 1529637
    iget-boolean v0, p1, LX/9jF;->k:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->k:Z

    .line 1529638
    iget-object v0, p1, LX/9jF;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->l:Ljava/lang/String;

    .line 1529639
    iget-object v0, p1, LX/9jF;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->m:Ljava/lang/String;

    .line 1529640
    iget-object v0, p1, LX/9jF;->n:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->n:Ljava/lang/String;

    .line 1529641
    iget-object v0, p1, LX/9jF;->o:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iput-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->o:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1529642
    iget-object v0, p1, LX/9jF;->p:Lcom/facebook/ipc/composer/model/ComposerLocation;

    iput-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->p:Lcom/facebook/ipc/composer/model/ComposerLocation;

    .line 1529643
    iget-object v0, p1, LX/9jF;->q:LX/9jG;

    iput-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->q:LX/9jG;

    .line 1529644
    iget-object v0, p1, LX/9jF;->r:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->r:LX/0Px;

    .line 1529645
    iget-object v0, p1, LX/9jF;->s:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->s:LX/0Px;

    .line 1529646
    iget-boolean v0, p1, LX/9jF;->t:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->t:Z

    .line 1529647
    iget-boolean v0, p1, LX/9jF;->u:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->u:Z

    .line 1529648
    iget-object v0, p1, LX/9jF;->v:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->v:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1529649
    iget-object v0, p1, LX/9jF;->w:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->w:Ljava/lang/String;

    .line 1529650
    iget-boolean v0, p1, LX/9jF;->x:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->x:Z

    .line 1529651
    iget-boolean v0, p1, LX/9jF;->y:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->y:Z

    .line 1529652
    iget-boolean v0, p1, LX/9jF;->z:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->z:Z

    .line 1529653
    iget-object v0, p1, LX/9jF;->A:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    iput-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->A:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    .line 1529654
    iget-object v0, p1, LX/9jF;->B:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->B:Ljava/lang/String;

    .line 1529655
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x0

    const/4 v1, 0x1

    .line 1529545
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1529546
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->a:Z

    .line 1529547
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->b:Z

    .line 1529548
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_2

    .line 1529549
    iput-object v6, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1529550
    :goto_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_3

    .line 1529551
    iput-object v6, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->d:Ljava/lang/String;

    .line 1529552
    :goto_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_4

    .line 1529553
    iput-object v6, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->e:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1529554
    :goto_4
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_5

    .line 1529555
    iput-object v6, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->f:Lcom/facebook/ipc/composer/model/ComposerLocation;

    .line 1529556
    :goto_5
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_6

    .line 1529557
    iput-object v6, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->g:Ljava/lang/String;

    .line 1529558
    :goto_6
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_7

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->h:Z

    .line 1529559
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_8

    move v0, v1

    :goto_8
    iput-boolean v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->i:Z

    .line 1529560
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_9

    move v0, v1

    :goto_9
    iput-boolean v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->j:Z

    .line 1529561
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_a

    move v0, v1

    :goto_a
    iput-boolean v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->k:Z

    .line 1529562
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_b

    .line 1529563
    iput-object v6, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->l:Ljava/lang/String;

    .line 1529564
    :goto_b
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_c

    .line 1529565
    iput-object v6, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->m:Ljava/lang/String;

    .line 1529566
    :goto_c
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_d

    .line 1529567
    iput-object v6, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->n:Ljava/lang/String;

    .line 1529568
    :goto_d
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_e

    .line 1529569
    iput-object v6, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->o:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1529570
    :goto_e
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_f

    .line 1529571
    iput-object v6, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->p:Lcom/facebook/ipc/composer/model/ComposerLocation;

    .line 1529572
    :goto_f
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_10

    .line 1529573
    iput-object v6, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->q:LX/9jG;

    .line 1529574
    :goto_10
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v4, v0, [Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    move v3, v2

    .line 1529575
    :goto_11
    array-length v0, v4

    if-ge v3, v0, :cond_11

    .line 1529576
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    .line 1529577
    aput-object v0, v4, v3

    .line 1529578
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_11

    :cond_0
    move v0, v2

    .line 1529579
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 1529580
    goto/16 :goto_1

    .line 1529581
    :cond_2
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iput-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    goto/16 :goto_2

    .line 1529582
    :cond_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->d:Ljava/lang/String;

    goto/16 :goto_3

    .line 1529583
    :cond_4
    sget-object v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    iput-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->e:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    goto/16 :goto_4

    .line 1529584
    :cond_5
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerLocation;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerLocation;

    iput-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->f:Lcom/facebook/ipc/composer/model/ComposerLocation;

    goto/16 :goto_5

    .line 1529585
    :cond_6
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->g:Ljava/lang/String;

    goto/16 :goto_6

    :cond_7
    move v0, v2

    .line 1529586
    goto/16 :goto_7

    :cond_8
    move v0, v2

    .line 1529587
    goto/16 :goto_8

    :cond_9
    move v0, v2

    .line 1529588
    goto/16 :goto_9

    :cond_a
    move v0, v2

    .line 1529589
    goto/16 :goto_a

    .line 1529590
    :cond_b
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->l:Ljava/lang/String;

    goto/16 :goto_b

    .line 1529591
    :cond_c
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->m:Ljava/lang/String;

    goto/16 :goto_c

    .line 1529592
    :cond_d
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->n:Ljava/lang/String;

    goto/16 :goto_d

    .line 1529593
    :cond_e
    sget-object v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iput-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->o:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    goto/16 :goto_e

    .line 1529594
    :cond_f
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerLocation;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerLocation;

    iput-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->p:Lcom/facebook/ipc/composer/model/ComposerLocation;

    goto/16 :goto_f

    .line 1529595
    :cond_10
    invoke-static {}, LX/9jG;->values()[LX/9jG;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    aget-object v0, v0, v3

    iput-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->q:LX/9jG;

    goto/16 :goto_10

    .line 1529596
    :cond_11
    invoke-static {v4}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->r:LX/0Px;

    .line 1529597
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v3, v0, [Ljava/lang/Long;

    move v0, v2

    .line 1529598
    :goto_12
    array-length v4, v3

    if-ge v0, v4, :cond_12

    .line 1529599
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 1529600
    aput-object v4, v3, v0

    .line 1529601
    add-int/lit8 v0, v0, 0x1

    goto :goto_12

    .line 1529602
    :cond_12
    invoke-static {v3}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->s:LX/0Px;

    .line 1529603
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_13

    move v0, v1

    :goto_13
    iput-boolean v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->t:Z

    .line 1529604
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_14

    move v0, v1

    :goto_14
    iput-boolean v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->u:Z

    .line 1529605
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_15

    .line 1529606
    iput-object v6, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->v:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1529607
    :goto_15
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_16

    .line 1529608
    iput-object v6, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->w:Ljava/lang/String;

    .line 1529609
    :goto_16
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_17

    move v0, v1

    :goto_17
    iput-boolean v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->x:Z

    .line 1529610
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_18

    move v0, v1

    :goto_18
    iput-boolean v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->y:Z

    .line 1529611
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_19

    :goto_19
    iput-boolean v1, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->z:Z

    .line 1529612
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1a

    .line 1529613
    iput-object v6, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->A:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    .line 1529614
    :goto_1a
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1b

    .line 1529615
    iput-object v6, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->B:Ljava/lang/String;

    .line 1529616
    :goto_1b
    return-void

    :cond_13
    move v0, v2

    .line 1529617
    goto :goto_13

    :cond_14
    move v0, v2

    .line 1529618
    goto :goto_14

    .line 1529619
    :cond_15
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->v:Lcom/facebook/graphql/model/GraphQLFeedback;

    goto :goto_15

    .line 1529620
    :cond_16
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->w:Ljava/lang/String;

    goto :goto_16

    :cond_17
    move v0, v2

    .line 1529621
    goto :goto_17

    :cond_18
    move v0, v2

    .line 1529622
    goto :goto_18

    :cond_19
    move v1, v2

    .line 1529623
    goto :goto_19

    .line 1529624
    :cond_1a
    const-class v0, Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    iput-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->A:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    goto :goto_1a

    .line 1529625
    :cond_1b
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->B:Ljava/lang/String;

    goto :goto_1b
.end method

.method public static newBuilder()LX/9jF;
    .locals 2

    .prologue
    .line 1529657
    new-instance v0, LX/9jF;

    invoke-direct {v0}, LX/9jF;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1529544
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1529483
    if-ne p0, p1, :cond_1

    .line 1529484
    :cond_0
    :goto_0
    return v0

    .line 1529485
    :cond_1
    instance-of v2, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    if-nez v2, :cond_2

    move v0, v1

    .line 1529486
    goto :goto_0

    .line 1529487
    :cond_2
    check-cast p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 1529488
    iget-boolean v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->a:Z

    iget-boolean v3, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->a:Z

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 1529489
    goto :goto_0

    .line 1529490
    :cond_3
    iget-boolean v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->b:Z

    iget-boolean v3, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->b:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1529491
    goto :goto_0

    .line 1529492
    :cond_4
    iget-object v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iget-object v3, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 1529493
    goto :goto_0

    .line 1529494
    :cond_5
    iget-object v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->d:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1529495
    goto :goto_0

    .line 1529496
    :cond_6
    iget-object v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->e:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    iget-object v3, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->e:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 1529497
    goto :goto_0

    .line 1529498
    :cond_7
    iget-object v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->f:Lcom/facebook/ipc/composer/model/ComposerLocation;

    iget-object v3, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->f:Lcom/facebook/ipc/composer/model/ComposerLocation;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 1529499
    goto :goto_0

    .line 1529500
    :cond_8
    iget-object v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->g:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 1529501
    goto :goto_0

    .line 1529502
    :cond_9
    iget-boolean v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->h:Z

    iget-boolean v3, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->h:Z

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 1529503
    goto :goto_0

    .line 1529504
    :cond_a
    iget-boolean v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->i:Z

    iget-boolean v3, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->i:Z

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 1529505
    goto :goto_0

    .line 1529506
    :cond_b
    iget-boolean v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->j:Z

    iget-boolean v3, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->j:Z

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 1529507
    goto :goto_0

    .line 1529508
    :cond_c
    iget-boolean v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->k:Z

    iget-boolean v3, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->k:Z

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 1529509
    goto :goto_0

    .line 1529510
    :cond_d
    iget-object v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->l:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->l:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    .line 1529511
    goto :goto_0

    .line 1529512
    :cond_e
    iget-object v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->m:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->m:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    move v0, v1

    .line 1529513
    goto/16 :goto_0

    .line 1529514
    :cond_f
    iget-object v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->n:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->n:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    move v0, v1

    .line 1529515
    goto/16 :goto_0

    .line 1529516
    :cond_10
    iget-object v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->o:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iget-object v3, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->o:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    move v0, v1

    .line 1529517
    goto/16 :goto_0

    .line 1529518
    :cond_11
    iget-object v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->p:Lcom/facebook/ipc/composer/model/ComposerLocation;

    iget-object v3, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->p:Lcom/facebook/ipc/composer/model/ComposerLocation;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    move v0, v1

    .line 1529519
    goto/16 :goto_0

    .line 1529520
    :cond_12
    iget-object v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->q:LX/9jG;

    iget-object v3, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->q:LX/9jG;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    move v0, v1

    .line 1529521
    goto/16 :goto_0

    .line 1529522
    :cond_13
    iget-object v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->r:LX/0Px;

    iget-object v3, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->r:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_14

    move v0, v1

    .line 1529523
    goto/16 :goto_0

    .line 1529524
    :cond_14
    iget-object v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->s:LX/0Px;

    iget-object v3, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->s:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    move v0, v1

    .line 1529525
    goto/16 :goto_0

    .line 1529526
    :cond_15
    iget-boolean v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->t:Z

    iget-boolean v3, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->t:Z

    if-eq v2, v3, :cond_16

    move v0, v1

    .line 1529527
    goto/16 :goto_0

    .line 1529528
    :cond_16
    iget-boolean v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->u:Z

    iget-boolean v3, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->u:Z

    if-eq v2, v3, :cond_17

    move v0, v1

    .line 1529529
    goto/16 :goto_0

    .line 1529530
    :cond_17
    iget-object v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->v:Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-object v3, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->v:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    move v0, v1

    .line 1529531
    goto/16 :goto_0

    .line 1529532
    :cond_18
    iget-object v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->w:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->w:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_19

    move v0, v1

    .line 1529533
    goto/16 :goto_0

    .line 1529534
    :cond_19
    iget-boolean v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->x:Z

    iget-boolean v3, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->x:Z

    if-eq v2, v3, :cond_1a

    move v0, v1

    .line 1529535
    goto/16 :goto_0

    .line 1529536
    :cond_1a
    iget-boolean v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->y:Z

    iget-boolean v3, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->y:Z

    if-eq v2, v3, :cond_1b

    move v0, v1

    .line 1529537
    goto/16 :goto_0

    .line 1529538
    :cond_1b
    iget-boolean v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->z:Z

    iget-boolean v3, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->z:Z

    if-eq v2, v3, :cond_1c

    move v0, v1

    .line 1529539
    goto/16 :goto_0

    .line 1529540
    :cond_1c
    iget-object v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->A:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    iget-object v3, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->A:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1d

    move v0, v1

    .line 1529541
    goto/16 :goto_0

    .line 1529542
    :cond_1d
    iget-object v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->B:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->B:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1529543
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1529482
    const/16 v0, 0x1c

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->e:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->f:Lcom/facebook/ipc/composer/model/ComposerLocation;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->h:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->i:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-boolean v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->j:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-boolean v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->k:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->l:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->m:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->n:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget-object v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->o:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    iget-object v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->p:Lcom/facebook/ipc/composer/model/ComposerLocation;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    iget-object v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->q:LX/9jG;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    iget-object v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->r:LX/0Px;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    iget-object v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->s:LX/0Px;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    iget-boolean v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->t:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x14

    iget-boolean v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->u:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x15

    iget-object v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->v:Lcom/facebook/graphql/model/GraphQLFeedback;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    iget-object v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->w:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    iget-boolean v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->x:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x18

    iget-boolean v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->y:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x19

    iget-boolean v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->z:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    iget-object v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->A:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    iget-object v2, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->B:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1529391
    iget-boolean v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529392
    iget-boolean v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->b:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529393
    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    if-nez v0, :cond_2

    .line 1529394
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529395
    :goto_2
    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->d:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 1529396
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529397
    :goto_3
    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->e:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    if-nez v0, :cond_4

    .line 1529398
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529399
    :goto_4
    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->f:Lcom/facebook/ipc/composer/model/ComposerLocation;

    if-nez v0, :cond_5

    .line 1529400
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529401
    :goto_5
    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->g:Ljava/lang/String;

    if-nez v0, :cond_6

    .line 1529402
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529403
    :goto_6
    iget-boolean v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->h:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529404
    iget-boolean v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->i:Z

    if-eqz v0, :cond_8

    move v0, v1

    :goto_8
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529405
    iget-boolean v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->j:Z

    if-eqz v0, :cond_9

    move v0, v1

    :goto_9
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529406
    iget-boolean v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->k:Z

    if-eqz v0, :cond_a

    move v0, v1

    :goto_a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529407
    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->l:Ljava/lang/String;

    if-nez v0, :cond_b

    .line 1529408
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529409
    :goto_b
    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->m:Ljava/lang/String;

    if-nez v0, :cond_c

    .line 1529410
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529411
    :goto_c
    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->n:Ljava/lang/String;

    if-nez v0, :cond_d

    .line 1529412
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529413
    :goto_d
    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->o:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    if-nez v0, :cond_e

    .line 1529414
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529415
    :goto_e
    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->p:Lcom/facebook/ipc/composer/model/ComposerLocation;

    if-nez v0, :cond_f

    .line 1529416
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529417
    :goto_f
    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->q:LX/9jG;

    if-nez v0, :cond_10

    .line 1529418
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529419
    :goto_10
    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->r:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529420
    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->r:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_11
    if-ge v3, v4, :cond_11

    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->r:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    .line 1529421
    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1529422
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_11

    :cond_0
    move v0, v2

    .line 1529423
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 1529424
    goto/16 :goto_1

    .line 1529425
    :cond_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529426
    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    goto/16 :goto_2

    .line 1529427
    :cond_3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529428
    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1529429
    :cond_4
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529430
    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->e:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_4

    .line 1529431
    :cond_5
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529432
    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->f:Lcom/facebook/ipc/composer/model/ComposerLocation;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerLocation;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_5

    .line 1529433
    :cond_6
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529434
    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_6

    :cond_7
    move v0, v2

    .line 1529435
    goto/16 :goto_7

    :cond_8
    move v0, v2

    .line 1529436
    goto/16 :goto_8

    :cond_9
    move v0, v2

    .line 1529437
    goto/16 :goto_9

    :cond_a
    move v0, v2

    .line 1529438
    goto/16 :goto_a

    .line 1529439
    :cond_b
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529440
    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_b

    .line 1529441
    :cond_c
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529442
    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_c

    .line 1529443
    :cond_d
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529444
    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_d

    .line 1529445
    :cond_e
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529446
    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->o:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_e

    .line 1529447
    :cond_f
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529448
    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->p:Lcom/facebook/ipc/composer/model/ComposerLocation;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerLocation;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_f

    .line 1529449
    :cond_10
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529450
    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->q:LX/9jG;

    invoke-virtual {v0}, LX/9jG;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_10

    .line 1529451
    :cond_11
    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->s:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529452
    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->s:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_12
    if-ge v3, v4, :cond_12

    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->s:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1529453
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {p1, v6, v7}, Landroid/os/Parcel;->writeLong(J)V

    .line 1529454
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_12

    .line 1529455
    :cond_12
    iget-boolean v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->t:Z

    if-eqz v0, :cond_13

    move v0, v1

    :goto_13
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529456
    iget-boolean v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->u:Z

    if-eqz v0, :cond_14

    move v0, v1

    :goto_14
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529457
    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->v:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-nez v0, :cond_15

    .line 1529458
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529459
    :goto_15
    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->w:Ljava/lang/String;

    if-nez v0, :cond_16

    .line 1529460
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529461
    :goto_16
    iget-boolean v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->x:Z

    if-eqz v0, :cond_17

    move v0, v1

    :goto_17
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529462
    iget-boolean v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->y:Z

    if-eqz v0, :cond_18

    move v0, v1

    :goto_18
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529463
    iget-boolean v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->z:Z

    if-eqz v0, :cond_19

    move v0, v1

    :goto_19
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529464
    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->A:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    if-nez v0, :cond_1a

    .line 1529465
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529466
    :goto_1a
    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->B:Ljava/lang/String;

    if-nez v0, :cond_1b

    .line 1529467
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529468
    :goto_1b
    return-void

    :cond_13
    move v0, v2

    .line 1529469
    goto :goto_13

    :cond_14
    move v0, v2

    .line 1529470
    goto :goto_14

    .line 1529471
    :cond_15
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529472
    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->v:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    goto :goto_15

    .line 1529473
    :cond_16
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529474
    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->w:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_16

    :cond_17
    move v0, v2

    .line 1529475
    goto :goto_17

    :cond_18
    move v0, v2

    .line 1529476
    goto :goto_18

    :cond_19
    move v0, v2

    .line 1529477
    goto :goto_19

    .line 1529478
    :cond_1a
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529479
    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->A:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    goto :goto_1a

    .line 1529480
    :cond_1b
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1529481
    iget-object v0, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->B:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_1b
.end method
