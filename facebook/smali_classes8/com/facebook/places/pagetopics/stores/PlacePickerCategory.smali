.class public Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:J

.field public final c:Ljava/lang/String;

.field public final d:Z

.field public final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1532165
    new-instance v0, LX/9kl;

    invoke-direct {v0}, LX/9kl;-><init>()V

    sput-object v0, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JLjava/lang/String;ZLX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "Z",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1532166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1532167
    iput-object p1, p0, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;->a:Ljava/lang/String;

    .line 1532168
    iput-wide p2, p0, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;->b:J

    .line 1532169
    iput-object p4, p0, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;->c:Ljava/lang/String;

    .line 1532170
    iput-boolean p5, p0, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;->d:Z

    .line 1532171
    iput-object p6, p0, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;->e:LX/0Px;

    .line 1532172
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/ipc/model/PageTopic;
    .locals 8

    .prologue
    .line 1532173
    new-instance v1, Lcom/facebook/ipc/model/PageTopic;

    iget-wide v2, p0, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;->b:J

    iget-object v4, p0, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;->c:Ljava/lang/String;

    const/4 v5, 0x0

    .line 1532174
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v6, v0

    .line 1532175
    const/4 v7, 0x0

    invoke-direct/range {v1 .. v7}, Lcom/facebook/ipc/model/PageTopic;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/util/List;I)V

    return-object v1
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1532176
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1532177
    iget-object v0, p0, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1532178
    iget-wide v0, p0, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1532179
    iget-object v0, p0, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1532180
    iget-boolean v0, p0, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1532181
    iget-object v0, p0, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;->e:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1532182
    return-void

    .line 1532183
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
