.class public Lcom/facebook/places/pagetopics/FetchPageTopicsResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/places/pagetopics/FetchPageTopicsResultDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/places/pagetopics/FetchPageTopicsResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private data:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "data"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/model/PageTopic;",
            ">;"
        }
    .end annotation
.end field

.field private mLocale:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "locale"
    .end annotation
.end field

.field private summary:Lcom/facebook/places/pagetopics/FetchPageTopicsResult$PageTopicsGetResponseSummary;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "summary"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1531133
    const-class v0, Lcom/facebook/places/pagetopics/FetchPageTopicsResultDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1531132
    new-instance v0, LX/9kK;

    invoke-direct {v0}, LX/9kK;-><init>()V

    sput-object v0, Lcom/facebook/places/pagetopics/FetchPageTopicsResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1531111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1531112
    iput-object v0, p0, Lcom/facebook/places/pagetopics/FetchPageTopicsResult;->data:LX/0Px;

    .line 1531113
    iput-object v0, p0, Lcom/facebook/places/pagetopics/FetchPageTopicsResult;->summary:Lcom/facebook/places/pagetopics/FetchPageTopicsResult$PageTopicsGetResponseSummary;

    .line 1531114
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1531125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1531126
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1531127
    const-class v1, Lcom/facebook/ipc/model/PageTopic;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 1531128
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/pagetopics/FetchPageTopicsResult;->data:LX/0Px;

    .line 1531129
    const-class v0, Lcom/facebook/places/pagetopics/FetchPageTopicsResult$PageTopicsGetResponseSummary;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/pagetopics/FetchPageTopicsResult$PageTopicsGetResponseSummary;

    iput-object v0, p0, Lcom/facebook/places/pagetopics/FetchPageTopicsResult;->summary:Lcom/facebook/places/pagetopics/FetchPageTopicsResult$PageTopicsGetResponseSummary;

    .line 1531130
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/pagetopics/FetchPageTopicsResult;->mLocale:Ljava/lang/String;

    .line 1531131
    return-void
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/model/PageTopic;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1531124
    iget-object v0, p0, Lcom/facebook/places/pagetopics/FetchPageTopicsResult;->data:LX/0Px;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1531122
    iput-object p1, p0, Lcom/facebook/places/pagetopics/FetchPageTopicsResult;->mLocale:Ljava/lang/String;

    .line 1531123
    return-void
.end method

.method public final b()Lcom/facebook/places/pagetopics/FetchPageTopicsResult$PageTopicsGetResponseSummary;
    .locals 1

    .prologue
    .line 1531121
    iget-object v0, p0, Lcom/facebook/places/pagetopics/FetchPageTopicsResult;->summary:Lcom/facebook/places/pagetopics/FetchPageTopicsResult$PageTopicsGetResponseSummary;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1531120
    iget-object v0, p0, Lcom/facebook/places/pagetopics/FetchPageTopicsResult;->mLocale:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1531119
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1531115
    iget-object v0, p0, Lcom/facebook/places/pagetopics/FetchPageTopicsResult;->data:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1531116
    iget-object v0, p0, Lcom/facebook/places/pagetopics/FetchPageTopicsResult;->summary:Lcom/facebook/places/pagetopics/FetchPageTopicsResult$PageTopicsGetResponseSummary;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1531117
    iget-object v0, p0, Lcom/facebook/places/pagetopics/FetchPageTopicsResult;->mLocale:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1531118
    return-void
.end method
