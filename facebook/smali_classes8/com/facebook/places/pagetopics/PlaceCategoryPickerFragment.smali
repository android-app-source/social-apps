.class public Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;
.super Lcom/facebook/places/pickers/PlaceContentPickerFragment;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/places/pickers/PlaceContentPickerFragment",
        "<",
        "Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/9kk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/9ki;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/9kc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/9kF;

.field private f:LX/9kZ;

.field public g:I

.field public h:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public i:Z

.field private final j:LX/9kO;

.field private final k:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final l:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1531248
    invoke-direct {p0}, Lcom/facebook/places/pickers/PlaceContentPickerFragment;-><init>()V

    .line 1531249
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->g:I

    .line 1531250
    new-instance v0, LX/9kO;

    invoke-direct {v0, p0}, LX/9kO;-><init>(Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;)V

    iput-object v0, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->j:LX/9kO;

    .line 1531251
    new-instance v0, LX/9kP;

    invoke-direct {v0, p0}, LX/9kP;-><init>(Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;)V

    iput-object v0, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->k:LX/0QK;

    .line 1531252
    new-instance v0, LX/9kQ;

    invoke-direct {v0, p0}, LX/9kQ;-><init>(Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;)V

    iput-object v0, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->l:LX/0QK;

    return-void
.end method

.method private static a(Ljava/lang/Iterable;LX/0QK;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;",
            ">;",
            "LX/0QK",
            "<",
            "Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;>;)",
            "LX/0Px",
            "<",
            "LX/9ks",
            "<",
            "Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1531352
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1531353
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;

    .line 1531354
    iget-wide v4, v0, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;->b:J

    iget-object v3, v0, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;->c:Ljava/lang/String;

    invoke-static {v0, v4, v5, v3}, LX/9ks;->a(Ljava/lang/Object;JLjava/lang/String;)LX/9kr;

    move-result-object v3

    invoke-interface {p1, v0}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0am;

    .line 1531355
    iput-object v0, v3, LX/9kr;->e:LX/0am;

    .line 1531356
    move-object v0, v3

    .line 1531357
    invoke-virtual {v0}, LX/9kr;->a()LX/9ks;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1531358
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0am;LX/9kF;ZLX/9kb;Landroid/os/Parcelable;)Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;
    .locals 4
    .param p1    # LX/9kF;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p4    # Landroid/os/Parcelable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;",
            ">;",
            "LX/9kF;",
            "Z",
            "LX/9kb;",
            "Landroid/os/Parcelable;",
            ")",
            "Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;"
        }
    .end annotation

    .prologue
    .line 1531342
    new-instance v1, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;

    invoke-direct {v1}, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;-><init>()V

    .line 1531343
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1531344
    invoke-virtual {p0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1531345
    const-string v3, "extra_parent_category"

    invoke-virtual {p0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1531346
    :cond_0
    const-string v0, "extra_listener"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1531347
    const-string v0, "extra_logger_type"

    invoke-virtual {p3}, LX/9kb;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1531348
    const-string v0, "extra_logger_params"

    invoke-virtual {v2, v0, p4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1531349
    const-string v0, "extra_show_null_state_header"

    invoke-virtual {v2, v0, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1531350
    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1531351
    return-object v1
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p1, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;

    invoke-static {v3}, LX/9kk;->a(LX/0QB;)LX/9kk;

    move-result-object v1

    check-cast v1, LX/9kk;

    invoke-static {v3}, LX/9ki;->a(LX/0QB;)LX/9ki;

    move-result-object v2

    check-cast v2, LX/9ki;

    new-instance v0, LX/9kc;

    const-class p0, LX/9kf;

    invoke-interface {v3, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p0

    check-cast p0, LX/9kf;

    invoke-direct {v0, p0}, LX/9kc;-><init>(LX/9kf;)V

    move-object v3, v0

    check-cast v3, LX/9kc;

    iput-object v1, p1, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->a:LX/9kk;

    iput-object v2, p1, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->b:LX/9ki;

    iput-object v3, p1, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->c:LX/9kc;

    return-void
.end method

.method public static o(Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;)V
    .locals 2

    .prologue
    .line 1531338
    iget-object v0, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->h:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->i:Z

    if-nez v0, :cond_0

    .line 1531339
    iget-object v1, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->f:LX/9kZ;

    iget-object v0, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->h:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1, v0}, LX/9kZ;->a(Ljava/lang/String;)V

    .line 1531340
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->i:Z

    .line 1531341
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "LX/9ks",
            "<",
            "Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1531330
    const/4 v0, 0x2

    iput v0, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->g:I

    .line 1531331
    iget-object v0, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->a:LX/9kk;

    invoke-virtual {v0, p1}, LX/9kk;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v0

    .line 1531332
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->k()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1531333
    iget-object v1, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->h:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result p1

    iget-object v1, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->h:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge p1, v1, :cond_1

    .line 1531334
    invoke-static {p0}, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->o(Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;)V

    .line 1531335
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->k:LX/0QK;

    invoke-static {v0, v1}, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->a(Ljava/lang/Iterable;LX/0QK;)LX/0Px;

    move-result-object v0

    return-object v0

    .line 1531336
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->m()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->h:LX/0am;

    .line 1531337
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->i:Z

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1531306
    invoke-super {p0, p1}, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->a(Landroid/os/Bundle;)V

    .line 1531307
    const-class v0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;

    invoke-static {v0, p0}, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1531308
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1531309
    const-string v1, "extra_parent_category"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;

    .line 1531310
    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->d:LX/0am;

    .line 1531311
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1531312
    const-string v1, "extra_listener"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/9kF;

    iput-object v0, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->e:LX/9kF;

    .line 1531313
    iget-object v0, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->c:LX/9kc;

    .line 1531314
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 1531315
    const-string v2, "extra_logger_type"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/9kb;->valueOf(Ljava/lang/String;)LX/9kb;

    move-result-object v1

    .line 1531316
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 1531317
    const-string v3, "extra_logger_params"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    .line 1531318
    sget-object v3, LX/9ka;->a:[I

    invoke-virtual {v1}, LX/9kb;->ordinal()I

    move-result p1

    aget v3, v3, p1

    packed-switch v3, :pswitch_data_0

    .line 1531319
    new-instance v3, LX/9kd;

    invoke-direct {v3}, LX/9kd;-><init>()V

    :goto_0
    move-object v0, v3

    .line 1531320
    iput-object v0, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->f:LX/9kZ;

    .line 1531321
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->h:LX/0am;

    .line 1531322
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->i:Z

    .line 1531323
    return-void

    .line 1531324
    :pswitch_0
    instance-of v3, v2, Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    if-eqz v3, :cond_0

    .line 1531325
    iget-object v3, v0, LX/9kc;->a:LX/9kf;

    check-cast v2, Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    .line 1531326
    new-instance v0, LX/9ke;

    invoke-static {v3}, LX/96B;->a(LX/0QB;)LX/96B;

    move-result-object p1

    check-cast p1, LX/96B;

    invoke-direct {v0, p1, v2}, LX/9ke;-><init>(LX/96B;Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;)V

    .line 1531327
    move-object v3, v0

    .line 1531328
    goto :goto_0

    .line 1531329
    :cond_0
    new-instance v3, LX/9kd;

    invoke-direct {v3}, LX/9kd;-><init>()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1531283
    check-cast p1, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;

    .line 1531284
    invoke-virtual {p0}, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->m()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1531285
    iget-object v0, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->f:LX/9kZ;

    invoke-virtual {p1}, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;->a()Lcom/facebook/ipc/model/PageTopic;

    move-result-object v1

    invoke-interface {v0, v1}, LX/9kZ;->a(Lcom/facebook/ipc/model/PageTopic;)V

    .line 1531286
    :goto_0
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1531287
    iget-object v0, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v3, p1, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;

    iget-object v0, v0, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;->a:Ljava/lang/String;

    if-ne v3, v0, :cond_2

    move v0, v1

    .line 1531288
    :goto_1
    iget v3, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->g:I

    if-ne v3, v1, :cond_3

    if-nez v0, :cond_3

    iget-object v0, p1, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    :goto_2
    move v0, v1

    .line 1531289
    if-eqz v0, :cond_1

    .line 1531290
    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->e:LX/9kF;

    .line 1531291
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 1531292
    const-string v3, "extra_show_null_state_header"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 1531293
    iget-object v3, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v3

    .line 1531294
    const-string v4, "extra_logger_type"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/9kb;->valueOf(Ljava/lang/String;)LX/9kb;

    move-result-object v3

    .line 1531295
    iget-object v4, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v4, v4

    .line 1531296
    const-string v5, "extra_logger_params"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->a(LX/0am;LX/9kF;ZLX/9kb;Landroid/os/Parcelable;)Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;

    move-result-object v0

    .line 1531297
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v1, v1

    .line 1531298
    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v1

    const v2, 0x7f0400db

    const v3, 0x7f040032

    const v4, 0x7f0400b8

    const v5, 0x7f0400ee

    invoke-virtual {v1, v2, v3, v4, v5}, LX/0hH;->a(IIII)LX/0hH;

    move-result-object v1

    .line 1531299
    iget v2, p0, Landroid/support/v4/app/Fragment;->mFragmentId:I

    move v2, v2

    .line 1531300
    invoke-virtual {v1, v2, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1531301
    :goto_3
    return-void

    .line 1531302
    :cond_0
    iget-object v0, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->f:LX/9kZ;

    invoke-virtual {p1}, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;->a()Lcom/facebook/ipc/model/PageTopic;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->m()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/9kZ;->a(Lcom/facebook/ipc/model/PageTopic;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1531303
    :cond_1
    iget-object v0, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->e:LX/9kF;

    invoke-virtual {p1}, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;->a()Lcom/facebook/ipc/model/PageTopic;

    move-result-object v1

    invoke-interface {v0, p0, v1}, LX/9kF;->a(Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;Lcom/facebook/ipc/model/PageTopic;)V

    goto :goto_3

    :cond_2
    move v0, v2

    .line 1531304
    goto :goto_1

    :cond_3
    move v1, v2

    .line 1531305
    goto :goto_2
.end method

.method public final b()LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/9ks",
            "<",
            "Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1531359
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->g:I

    .line 1531360
    iget-object v0, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1531361
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1531362
    iget-object v0, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;

    iget-boolean v0, v0, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;->d:Z

    if-eqz v0, :cond_0

    .line 1531363
    iget-object v0, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1531364
    :cond_0
    iget-object v2, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->b:LX/9ki;

    iget-object v0, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;

    .line 1531365
    invoke-static {v2, v0}, LX/9ki;->c(LX/9ki;Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1531366
    iget-object v3, v2, LX/9ki;->b:Ljava/util/Map;

    iget-object v4, v0, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;->a:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, v2, LX/9ki;->b:Ljava/util/Map;

    iget-object v4, v0, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;->a:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0Px;

    .line 1531367
    :goto_0
    move-object v0, v3

    .line 1531368
    invoke-virtual {v1, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1531369
    :goto_1
    iget-object v1, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->l:LX/0QK;

    invoke-static {v0, v1}, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->a(Ljava/lang/Iterable;LX/0QK;)LX/0Px;

    move-result-object v0

    return-object v0

    .line 1531370
    :cond_1
    iget-object v0, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->a:LX/9kk;

    const-string v1, ""

    invoke-virtual {v0, v1}, LX/9kk;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v0

    goto :goto_1

    .line 1531371
    :cond_2
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 1531372
    goto :goto_0

    .line 1531373
    :cond_3
    const/4 v6, 0x1

    .line 1531374
    invoke-static {v2, v0}, LX/9ki;->c(LX/9ki;Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1531375
    :goto_2
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 1531376
    goto :goto_0

    .line 1531377
    :cond_4
    iget-object v3, v2, LX/9ki;->d:LX/1Ck;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 1531378
    iget-object v3, v0, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;->a:Ljava/lang/String;

    invoke-static {v3}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v3

    iput-object v3, v2, LX/9ki;->a:LX/0am;

    .line 1531379
    new-instance v3, LX/9kR;

    invoke-direct {v3}, LX/9kR;-><init>()V

    move-object v3, v3

    .line 1531380
    const-string v4, "category_id"

    iget-object v5, v0, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;->a:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    check-cast v3, LX/9kR;

    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    .line 1531381
    iget-object v4, v2, LX/9ki;->d:LX/1Ck;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iget-object v6, v2, LX/9ki;->c:LX/0tX;

    invoke-virtual {v6, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    invoke-static {v3}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    new-instance v6, LX/9kg;

    invoke-direct {v6, v2, v0}, LX/9kg;-><init>(LX/9ki;Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;)V

    invoke-virtual {v4, v5, v3, v6}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1531382
    invoke-virtual {v2}, LX/9kh;->b()V

    goto :goto_2
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 1531282
    const v0, 0x7f081742

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1531281
    const v0, 0x7f081738

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/0am;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "LX/9kq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1531272
    iget-object v0, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1531273
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    .line 1531274
    :goto_0
    return-object v0

    .line 1531275
    :cond_0
    new-instance v0, LX/9kq;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/9kq;-><init>(Landroid/content/Context;)V

    .line 1531276
    const v1, 0x7f02034e

    invoke-virtual {v0, v1}, LX/9kq;->setImage(I)V

    .line 1531277
    const v1, 0x7f081740

    invoke-virtual {v0, v1}, LX/9kq;->setTitle(I)V

    .line 1531278
    const v1, 0x7f081741

    invoke-virtual {v0, v1}, LX/9kq;->setSubTitle(I)V

    .line 1531279
    const v1, 0x7f08173f

    invoke-virtual {v0, v1}, LX/9kq;->setSectionTitle(I)V

    .line 1531280
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 1531269
    iget-object v0, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->b:LX/9ki;

    .line 1531270
    iget-object v1, v0, LX/9ki;->d:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->b()LX/1M1;

    move-result-object v1

    const/4 p0, 0x1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-interface {v1, p0}, LX/1M1;->contains(Ljava/lang/Object;)Z

    move-result v1

    move v0, v1

    .line 1531271
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->a:LX/9kk;

    invoke-virtual {v0}, LX/9kk;->a()Z

    move-result v0

    goto :goto_0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 1531268
    iget-object v0, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->a:LX/9kk;

    invoke-virtual {v0}, LX/9kk;->a()Z

    move-result v0

    return v0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3cdd58c4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1531263
    invoke-super {p0}, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->onPause()V

    .line 1531264
    iget-object v1, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->a:LX/9kk;

    iget-object v2, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->j:LX/9kO;

    invoke-virtual {v1, v2}, LX/9kh;->b(LX/9kO;)V

    .line 1531265
    iget-object v1, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->b:LX/9ki;

    iget-object v2, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->j:LX/9kO;

    invoke-virtual {v1, v2}, LX/9kh;->b(LX/9kO;)V

    .line 1531266
    invoke-static {p0}, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->o(Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;)V

    .line 1531267
    const/16 v1, 0x2b

    const v2, -0x58841aad

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x7069f1b9

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1531253
    invoke-super {p0}, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->onResume()V

    .line 1531254
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 1531255
    iget-object v1, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->d:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1531256
    iget-object v3, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->f:LX/9kZ;

    iget-object v1, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->d:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;

    invoke-virtual {v1}, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;->a()Lcom/facebook/ipc/model/PageTopic;

    move-result-object v1

    invoke-interface {v3, v1}, LX/9kZ;->b(Lcom/facebook/ipc/model/PageTopic;)V

    .line 1531257
    iget-object v1, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->d:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;

    iget-object v1, v1, Lcom/facebook/places/pagetopics/stores/PlacePickerCategory;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 1531258
    :goto_0
    invoke-interface {v0}, LX/1ZF;->lH_()V

    .line 1531259
    iget-object v0, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->a:LX/9kk;

    iget-object v1, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->j:LX/9kO;

    invoke-virtual {v0, v1}, LX/9kh;->a(LX/9kO;)V

    .line 1531260
    iget-object v0, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->b:LX/9ki;

    iget-object v1, p0, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->j:LX/9kO;

    invoke-virtual {v0, v1}, LX/9kh;->a(LX/9kO;)V

    .line 1531261
    const v0, 0x7a8dd0e2

    invoke-static {v0, v2}, LX/02F;->f(II)V

    return-void

    .line 1531262
    :cond_0
    const v1, 0x7f081736

    invoke-interface {v0, v1}, LX/1ZF;->x_(I)V

    goto :goto_0
.end method
