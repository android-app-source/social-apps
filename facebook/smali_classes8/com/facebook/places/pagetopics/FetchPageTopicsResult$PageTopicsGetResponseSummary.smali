.class public final Lcom/facebook/places/pagetopics/FetchPageTopicsResult$PageTopicsGetResponseSummary;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/places/pagetopics/FetchPageTopicsResult_PageTopicsGetResponseSummaryDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/places/pagetopics/FetchPageTopicsResult$PageTopicsGetResponseSummary;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private topicsVersion:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "topics_version"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1531110
    const-class v0, Lcom/facebook/places/pagetopics/FetchPageTopicsResult_PageTopicsGetResponseSummaryDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1531109
    new-instance v0, LX/9kL;

    invoke-direct {v0}, LX/9kL;-><init>()V

    sput-object v0, Lcom/facebook/places/pagetopics/FetchPageTopicsResult$PageTopicsGetResponseSummary;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1531106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1531107
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/places/pagetopics/FetchPageTopicsResult$PageTopicsGetResponseSummary;->topicsVersion:J

    .line 1531108
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1531099
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1531100
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/places/pagetopics/FetchPageTopicsResult$PageTopicsGetResponseSummary;->topicsVersion:J

    .line 1531101
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 1531105
    iget-wide v0, p0, Lcom/facebook/places/pagetopics/FetchPageTopicsResult$PageTopicsGetResponseSummary;->topicsVersion:J

    return-wide v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1531104
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1531102
    iget-wide v0, p0, Lcom/facebook/places/pagetopics/FetchPageTopicsResult$PageTopicsGetResponseSummary;->topicsVersion:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1531103
    return-void
.end method
