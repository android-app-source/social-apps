.class public final Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryPickerSearchModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1531673
    const-class v0, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryPickerSearchModel;

    new-instance v1, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryPickerSearchModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryPickerSearchModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1531674
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1531675
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1531676
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1531677
    const/4 v2, 0x0

    .line 1531678
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1531679
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1531680
    :goto_0
    move v1, v2

    .line 1531681
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1531682
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1531683
    new-instance v1, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryPickerSearchModel;

    invoke-direct {v1}, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryPickerSearchModel;-><init>()V

    .line 1531684
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1531685
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1531686
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1531687
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1531688
    :cond_0
    return-object v1

    .line 1531689
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1531690
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1531691
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1531692
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1531693
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 1531694
    const-string v4, "category_results"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1531695
    const/4 v3, 0x0

    .line 1531696
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_a

    .line 1531697
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1531698
    :goto_2
    move v1, v3

    .line 1531699
    goto :goto_1

    .line 1531700
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1531701
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1531702
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 1531703
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1531704
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_9

    .line 1531705
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1531706
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1531707
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_6

    if-eqz v5, :cond_6

    .line 1531708
    const-string v6, "edges"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1531709
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1531710
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v6, :cond_7

    .line 1531711
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v6, :cond_7

    .line 1531712
    const/4 v6, 0x0

    .line 1531713
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v7, :cond_e

    .line 1531714
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1531715
    :goto_5
    move v5, v6

    .line 1531716
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1531717
    :cond_7
    invoke-static {v4, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v4

    move v4, v4

    .line 1531718
    goto :goto_3

    .line 1531719
    :cond_8
    const-string v6, "page_info"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1531720
    const/4 v5, 0x0

    .line 1531721
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v6, :cond_13

    .line 1531722
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1531723
    :goto_6
    move v1, v5

    .line 1531724
    goto :goto_3

    .line 1531725
    :cond_9
    const/4 v5, 0x2

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1531726
    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 1531727
    const/4 v3, 0x1

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1531728
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto/16 :goto_2

    :cond_a
    move v1, v3

    move v4, v3

    goto :goto_3

    .line 1531729
    :cond_b
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1531730
    :cond_c
    :goto_7
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, p0, :cond_d

    .line 1531731
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1531732
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1531733
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_c

    if-eqz v7, :cond_c

    .line 1531734
    const-string p0, "node"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 1531735
    invoke-static {p1, v0}, LX/9kX;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_7

    .line 1531736
    :cond_d
    const/4 v7, 0x1

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 1531737
    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 1531738
    invoke-virtual {v0}, LX/186;->d()I

    move-result v6

    goto :goto_5

    :cond_e
    move v5, v6

    goto :goto_7

    .line 1531739
    :cond_f
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1531740
    :cond_10
    :goto_8
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, p0, :cond_12

    .line 1531741
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1531742
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1531743
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_10

    if-eqz v7, :cond_10

    .line 1531744
    const-string p0, "end_cursor"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_11

    .line 1531745
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_8

    .line 1531746
    :cond_11
    const-string p0, "start_cursor"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_f

    .line 1531747
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_8

    .line 1531748
    :cond_12
    const/4 v7, 0x2

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 1531749
    invoke-virtual {v0, v5, v6}, LX/186;->b(II)V

    .line 1531750
    const/4 v5, 0x1

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 1531751
    invoke-virtual {v0}, LX/186;->d()I

    move-result v5

    goto/16 :goto_6

    :cond_13
    move v1, v5

    move v6, v5

    goto :goto_8
.end method
