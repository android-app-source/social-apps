.class public final Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryPickerChildrenModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryPickerChildrenModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1531602
    const-class v0, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryPickerChildrenModel;

    new-instance v1, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryPickerChildrenModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryPickerChildrenModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1531603
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1531604
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryPickerChildrenModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1531605
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1531606
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v3, 0x0

    .line 1531607
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1531608
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 1531609
    if-eqz v2, :cond_0

    .line 1531610
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1531611
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1531612
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1531613
    if-eqz v2, :cond_4

    .line 1531614
    const-string v3, "child_categories"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1531615
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1531616
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1531617
    if-eqz v3, :cond_3

    .line 1531618
    const-string v4, "edges"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1531619
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1531620
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result p0

    if-ge v4, p0, :cond_2

    .line 1531621
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result p0

    .line 1531622
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1531623
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1531624
    if-eqz v0, :cond_1

    .line 1531625
    const-string v2, "node"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1531626
    invoke-static {v1, v0, p1, p2}, LX/9kX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1531627
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1531628
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1531629
    :cond_2
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1531630
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1531631
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1531632
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1531633
    check-cast p1, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryPickerChildrenModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryPickerChildrenModel$Serializer;->a(Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryPickerChildrenModel;LX/0nX;LX/0my;)V

    return-void
.end method
