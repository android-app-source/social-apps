.class public final Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1531535
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1531536
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1531533
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1531534
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1531480
    if-nez p1, :cond_0

    move v0, v1

    .line 1531481
    :goto_0
    return v0

    .line 1531482
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1531483
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1531484
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1531485
    const v2, -0x741e820a

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 1531486
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1531487
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1531488
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1531489
    :sswitch_1
    const-class v0, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryRowModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryRowModel;

    .line 1531490
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1531491
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1531492
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1531493
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1531494
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1531495
    const v2, 0x310d57c8

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 1531496
    invoke-virtual {p0, p1, v4}, LX/15i;->p(II)I

    move-result v2

    .line 1531497
    const v3, 0xa529800

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 1531498
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1531499
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1531500
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 1531501
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1531502
    :sswitch_3
    const-class v0, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryRowModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryRowModel;

    .line 1531503
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1531504
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1531505
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1531506
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1531507
    :sswitch_4
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1531508
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1531509
    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1531510
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1531511
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1531512
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1531513
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 1531514
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1531515
    :sswitch_5
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1531516
    const v2, -0x4eeac7fa

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 1531517
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1531518
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1531519
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1531520
    :sswitch_6
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1531521
    const v2, -0x3a670557

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1531522
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1531523
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1531524
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1531525
    :sswitch_7
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1531526
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1531527
    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1531528
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1531529
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1531530
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1531531
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 1531532
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x741e820a -> :sswitch_1
        -0x6e46449d -> :sswitch_2
        -0x4eeac7fa -> :sswitch_6
        -0x3a670557 -> :sswitch_7
        0xa529800 -> :sswitch_4
        0x310d57c8 -> :sswitch_3
        0x4ba7a089 -> :sswitch_5
        0x557451e1 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1531479
    new-instance v0, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1531474
    if-eqz p0, :cond_0

    .line 1531475
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1531476
    if-eq v0, p0, :cond_0

    .line 1531477
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1531478
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1531457
    sparse-switch p2, :sswitch_data_0

    .line 1531458
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1531459
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1531460
    const v1, -0x741e820a

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1531461
    :goto_0
    :sswitch_1
    return-void

    .line 1531462
    :sswitch_2
    const-class v0, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryRowModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryRowModel;

    .line 1531463
    invoke-static {v0, p3}, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    .line 1531464
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1531465
    const v1, 0x310d57c8

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1531466
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1531467
    const v1, 0xa529800

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1531468
    :sswitch_4
    const-class v0, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryRowModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryRowModel;

    .line 1531469
    invoke-static {v0, p3}, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    .line 1531470
    :sswitch_5
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1531471
    const v1, -0x4eeac7fa

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1531472
    :sswitch_6
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1531473
    const v1, -0x3a670557

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x741e820a -> :sswitch_2
        -0x6e46449d -> :sswitch_3
        -0x4eeac7fa -> :sswitch_6
        -0x3a670557 -> :sswitch_1
        0xa529800 -> :sswitch_1
        0x310d57c8 -> :sswitch_4
        0x4ba7a089 -> :sswitch_5
        0x557451e1 -> :sswitch_0
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1531447
    if-nez p1, :cond_0

    move v0, v1

    .line 1531448
    :goto_0
    return v0

    .line 1531449
    :cond_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v2

    .line 1531450
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1531451
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1531452
    invoke-virtual {p0, p1, v1}, LX/15i;->q(II)I

    move-result v3

    .line 1531453
    invoke-static {p0, v3, p2, p3}, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    aput v3, v0, v1

    .line 1531454
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1531455
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1531456
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 1531440
    if-eqz p1, :cond_0

    .line 1531441
    invoke-virtual {p0, p1}, LX/15i;->d(I)I

    move-result v1

    .line 1531442
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1531443
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v2

    .line 1531444
    invoke-static {p0, v2, p2, p3}, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1531445
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1531446
    :cond_0
    return-void
.end method

.method private static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1531434
    if-eqz p1, :cond_0

    .line 1531435
    invoke-static {p0, p1, p2}, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 1531436
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$DraculaImplementation;

    .line 1531437
    if-eq v0, v1, :cond_0

    .line 1531438
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1531439
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1531537
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1531432
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1531433
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1531401
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1531402
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1531403
    :cond_0
    iput-object p1, p0, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1531404
    iput p2, p0, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$DraculaImplementation;->b:I

    .line 1531405
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1531431
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1531430
    new-instance v0, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1531427
    iget v0, p0, LX/1vt;->c:I

    .line 1531428
    move v0, v0

    .line 1531429
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1531424
    iget v0, p0, LX/1vt;->c:I

    .line 1531425
    move v0, v0

    .line 1531426
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1531421
    iget v0, p0, LX/1vt;->b:I

    .line 1531422
    move v0, v0

    .line 1531423
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1531418
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1531419
    move-object v0, v0

    .line 1531420
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1531409
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1531410
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1531411
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1531412
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1531413
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1531414
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1531415
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1531416
    invoke-static {v3, v9, v2}, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1531417
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1531406
    iget v0, p0, LX/1vt;->c:I

    .line 1531407
    move v0, v0

    .line 1531408
    return v0
.end method
