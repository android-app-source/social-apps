.class public final Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryPickerSearchModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryPickerSearchModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1531752
    const-class v0, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryPickerSearchModel;

    new-instance v1, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryPickerSearchModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryPickerSearchModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1531753
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1531754
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryPickerSearchModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 1531755
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1531756
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1531757
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1531758
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1531759
    if-eqz v2, :cond_6

    .line 1531760
    const-string v3, "category_results"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1531761
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1531762
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1531763
    if-eqz v3, :cond_2

    .line 1531764
    const-string v4, "edges"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1531765
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1531766
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result v5

    if-ge v4, v5, :cond_1

    .line 1531767
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result v5

    .line 1531768
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1531769
    const/4 p0, 0x0

    invoke-virtual {v1, v5, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1531770
    if-eqz p0, :cond_0

    .line 1531771
    const-string v0, "node"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1531772
    invoke-static {v1, p0, p1, p2}, LX/9kX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1531773
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1531774
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1531775
    :cond_1
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1531776
    :cond_2
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1531777
    if-eqz v3, :cond_5

    .line 1531778
    const-string v4, "page_info"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1531779
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1531780
    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1531781
    if-eqz v4, :cond_3

    .line 1531782
    const-string v5, "end_cursor"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1531783
    invoke-virtual {p1, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1531784
    :cond_3
    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1531785
    if-eqz v4, :cond_4

    .line 1531786
    const-string v5, "start_cursor"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1531787
    invoke-virtual {p1, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1531788
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1531789
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1531790
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1531791
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1531792
    check-cast p1, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryPickerSearchModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryPickerSearchModel$Serializer;->a(Lcom/facebook/places/pagetopics/graphql/PlaceCategoryPickerGraphQLModels$PlaceCategoryPickerSearchModel;LX/0nX;LX/0my;)V

    return-void
.end method
