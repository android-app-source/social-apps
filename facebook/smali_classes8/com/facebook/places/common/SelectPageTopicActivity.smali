.class public Lcom/facebook/places/common/SelectPageTopicActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements LX/0f2;
.implements LX/10X;


# instance fields
.field public p:LX/9k4;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public q:LX/9kN;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public r:Lcom/facebook/widget/listview/BetterListView;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public s:LX/0Vd;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "Lcom/facebook/places/pagetopics/FetchPageTopicsResult;",
            ">;"
        }
    .end annotation
.end field

.field private t:I

.field private u:J

.field private v:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1530723
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1530724
    new-instance v0, LX/9k1;

    invoke-direct {v0, p0}, LX/9k1;-><init>(Lcom/facebook/places/common/SelectPageTopicActivity;)V

    iput-object v0, p0, Lcom/facebook/places/common/SelectPageTopicActivity;->s:LX/0Vd;

    return-void
.end method

.method public static synthetic a(Lcom/facebook/places/common/SelectPageTopicActivity;I)Landroid/view/View;
    .locals 1

    .prologue
    .line 1530667
    invoke-virtual {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic b(Lcom/facebook/places/common/SelectPageTopicActivity;I)Landroid/view/View;
    .locals 1

    .prologue
    .line 1530722
    invoke-virtual {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private m()Lcom/facebook/widget/listview/BetterListView;
    .locals 1

    .prologue
    .line 1530721
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1530720
    const-string v0, "add_location_category_module"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1530715
    const-class v0, LX/9kN;

    invoke-virtual {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->iz_()LX/0QA;

    move-result-object v1

    .line 1530716
    new-instance p1, LX/9kN;

    invoke-static {v1}, LX/9kE;->b(LX/0QB;)LX/9kE;

    move-result-object v2

    check-cast v2, LX/9kE;

    invoke-static {v1}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v3

    check-cast v3, LX/0aG;

    invoke-direct {p1, v2, v3}, LX/9kN;-><init>(LX/9kE;LX/0aG;)V

    .line 1530717
    move-object v1, p1

    .line 1530718
    invoke-static {v0, v1}, LX/9jy;->a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9kN;

    iput-object v0, p0, Lcom/facebook/places/common/SelectPageTopicActivity;->q:LX/9kN;

    .line 1530719
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1530725
    const v0, 0x7f0816b6

    invoke-virtual {p0, v0}, Lcom/facebook/places/common/SelectPageTopicActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1530699
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1530700
    const v0, 0x7f030eae

    invoke-virtual {p0, v0}, Lcom/facebook/places/common/SelectPageTopicActivity;->setContentView(I)V

    .line 1530701
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 1530702
    const v0, 0x7f0d0d66

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/common/SelectPageTopicActivity;->v:Landroid/view/View;

    .line 1530703
    invoke-virtual {p0}, Lcom/facebook/places/common/SelectPageTopicActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_topic_id"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/places/common/SelectPageTopicActivity;->u:J

    .line 1530704
    invoke-virtual {p0}, Lcom/facebook/places/common/SelectPageTopicActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_category_level"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/places/common/SelectPageTopicActivity;->t:I

    .line 1530705
    new-instance v0, LX/9k4;

    iget v1, p0, Lcom/facebook/places/common/SelectPageTopicActivity;->t:I

    iget-wide v2, p0, Lcom/facebook/places/common/SelectPageTopicActivity;->u:J

    invoke-direct {v0, p0, v1, v2, v3}, LX/9k4;-><init>(Landroid/content/Context;IJ)V

    iput-object v0, p0, Lcom/facebook/places/common/SelectPageTopicActivity;->p:LX/9k4;

    .line 1530706
    invoke-direct {p0}, Lcom/facebook/places/common/SelectPageTopicActivity;->m()Lcom/facebook/widget/listview/BetterListView;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/common/SelectPageTopicActivity;->r:Lcom/facebook/widget/listview/BetterListView;

    .line 1530707
    iget-object v0, p0, Lcom/facebook/places/common/SelectPageTopicActivity;->r:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/places/common/SelectPageTopicActivity;->p:LX/9k4;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1530708
    iget-object v0, p0, Lcom/facebook/places/common/SelectPageTopicActivity;->r:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p0}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1530709
    iget-object v0, p0, Lcom/facebook/places/common/SelectPageTopicActivity;->r:Lcom/facebook/widget/listview/BetterListView;

    const v1, 0x1020004

    invoke-virtual {p0, v1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setEmptyView(Landroid/view/View;)V

    .line 1530710
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/places/common/SelectPageTopicActivity;->b(Z)V

    .line 1530711
    iget-object v0, p0, Lcom/facebook/places/common/SelectPageTopicActivity;->p:LX/9k4;

    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9k4;->a(Ljava/util/List;)V

    .line 1530712
    iget-object v0, p0, Lcom/facebook/places/common/SelectPageTopicActivity;->p:LX/9k4;

    const v1, 0x1390e41f

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1530713
    invoke-virtual {p0}, Lcom/facebook/places/common/SelectPageTopicActivity;->l()V

    .line 1530714
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 1530696
    iget-object v1, p0, Lcom/facebook/places/common/SelectPageTopicActivity;->v:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1530697
    return-void

    .line 1530698
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final l()V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1530693
    iget-object v0, p0, Lcom/facebook/places/common/SelectPageTopicActivity;->q:LX/9kN;

    iget-object v1, p0, Lcom/facebook/places/common/SelectPageTopicActivity;->s:LX/0Vd;

    .line 1530694
    iget-object v2, v0, LX/9kN;->b:LX/9kE;

    new-instance p0, LX/9kM;

    invoke-direct {p0, v0}, LX/9kM;-><init>(LX/9kN;)V

    invoke-virtual {v2, p0, v1}, LX/9kE;->a(Ljava/util/concurrent/Callable;LX/0TF;)V

    .line 1530695
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1530685
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1530686
    if-nez p2, :cond_1

    .line 1530687
    :cond_0
    :goto_0
    return-void

    .line 1530688
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 1530689
    :pswitch_0
    if-ne p2, v1, :cond_0

    .line 1530690
    const-string v0, "object"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1530691
    invoke-virtual {p0, v1, p3}, Lcom/facebook/places/common/SelectPageTopicActivity;->setResult(ILandroid/content/Intent;)V

    .line 1530692
    invoke-virtual {p0}, Lcom/facebook/places/common/SelectPageTopicActivity;->finish()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x762a9d0b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1530681
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 1530682
    iget-object v1, p0, Lcom/facebook/places/common/SelectPageTopicActivity;->q:LX/9kN;

    .line 1530683
    iget-object v2, v1, LX/9kN;->b:LX/9kE;

    invoke-virtual {v2}, LX/9kE;->c()V

    .line 1530684
    const/16 v1, 0x23

    const v2, 0x25c8794b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1530668
    iget-object v0, p0, Lcom/facebook/places/common/SelectPageTopicActivity;->p:LX/9k4;

    invoke-virtual {v0, p3}, LX/3Tf;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/PageTopic;

    .line 1530669
    iget-wide v2, v0, Lcom/facebook/ipc/model/PageTopic;->id:J

    invoke-static {v2, v3}, LX/9k4;->a(J)Ljava/util/List;

    move-result-object v1

    .line 1530670
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    iget-wide v2, v0, Lcom/facebook/ipc/model/PageTopic;->id:J

    iget-wide v4, p0, Lcom/facebook/places/common/SelectPageTopicActivity;->u:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 1530671
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/facebook/places/common/SelectPageTopicActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1530672
    const-string v2, "extra_topic_id"

    iget-wide v4, v0, Lcom/facebook/ipc/model/PageTopic;->id:J

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1530673
    const-string v2, "extra_topic_name"

    iget-object v0, v0, Lcom/facebook/ipc/model/PageTopic;->displayName:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1530674
    const-string v0, "extra_category_level"

    iget v2, p0, Lcom/facebook/places/common/SelectPageTopicActivity;->t:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1530675
    const/4 v0, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/facebook/places/common/SelectPageTopicActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1530676
    :goto_0
    return-void

    .line 1530677
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1530678
    const-string v2, "object"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1530679
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/places/common/SelectPageTopicActivity;->setResult(ILandroid/content/Intent;)V

    .line 1530680
    invoke-virtual {p0}, Lcom/facebook/places/common/SelectPageTopicActivity;->finish()V

    goto :goto_0
.end method
