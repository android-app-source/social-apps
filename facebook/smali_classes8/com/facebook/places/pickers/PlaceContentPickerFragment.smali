.class public abstract Lcom/facebook/places/pickers/PlaceContentPickerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/facebook/base/fragment/FbFragment;"
    }
.end annotation


# instance fields
.field public a:LX/9kw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/9kw",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1531247
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private n()V
    .locals 2

    .prologue
    .line 1531244
    iget-object v0, p0, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->a:LX/9kw;

    invoke-virtual {p0}, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->c()Ljava/lang/String;

    move-result-object v1

    .line 1531245
    iget-object p0, v0, LX/9kw;->a:Landroid/widget/EditText;

    invoke-virtual {p0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1531246
    return-void
.end method


# virtual methods
.method public abstract a(Ljava/lang/String;)LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "LX/9ks",
            "<TT;>;>;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method public abstract b()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/9ks",
            "<TT;>;>;"
        }
    .end annotation
.end method

.method public abstract b(Ljava/lang/String;)Ljava/lang/CharSequence;
.end method

.method public abstract c()Ljava/lang/String;
.end method

.method public abstract d()LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<+",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end method

.method public abstract e()Z
.end method

.method public abstract k()Z
.end method

.method public final l()V
    .locals 5

    .prologue
    const/16 v1, 0x8

    const/4 v0, 0x0

    .line 1531228
    invoke-virtual {p0}, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->m()Ljava/lang/String;

    move-result-object v2

    .line 1531229
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1531230
    iget-object v2, p0, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->a:LX/9kw;

    .line 1531231
    iget-object v3, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v3

    .line 1531232
    const-string v4, "extra_show_null_state_header"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    invoke-virtual {v2, v0}, LX/9kw;->setHeaderVisibility(I)V

    .line 1531233
    iget-object v0, p0, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->a:LX/9kw;

    invoke-virtual {p0}, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->e()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/9kw;->a(Z)V

    .line 1531234
    iget-object v0, p0, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->a:LX/9kw;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/9kw;->setFooterMessage(Ljava/lang/CharSequence;)V

    .line 1531235
    iget-object v0, p0, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->a:LX/9kw;

    invoke-virtual {p0}, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9kw;->setRows(LX/0Px;)V

    .line 1531236
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 1531237
    goto :goto_0

    .line 1531238
    :cond_1
    iget-object v0, p0, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->a:LX/9kw;

    invoke-virtual {v0, v1}, LX/9kw;->setHeaderVisibility(I)V

    .line 1531239
    invoke-virtual {p0, v2}, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v1

    .line 1531240
    iget-object v0, p0, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->a:LX/9kw;

    invoke-virtual {p0}, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->k()Z

    move-result v3

    invoke-virtual {v0, v3}, LX/9kw;->a(Z)V

    .line 1531241
    iget-object v3, p0, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->a:LX/9kw;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->k()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0, v2}, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->b(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_2
    invoke-virtual {v3, v0}, LX/9kw;->setFooterMessage(Ljava/lang/CharSequence;)V

    .line 1531242
    iget-object v0, p0, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->a:LX/9kw;

    invoke-virtual {v0, v1}, LX/9kw;->setRows(LX/0Px;)V

    goto :goto_1

    .line 1531243
    :cond_2
    const-string v0, ""

    goto :goto_2
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1531225
    iget-object v0, p0, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->a:LX/9kw;

    .line 1531226
    iget-object p0, v0, LX/9kw;->a:Landroid/widget/EditText;

    invoke-virtual {p0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    move-object v0, p0

    .line 1531227
    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x4518a9cf

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1531217
    new-instance v1, LX/9kw;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/9kw;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->a:LX/9kw;

    .line 1531218
    iget-object v1, p0, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->a:LX/9kw;

    invoke-virtual {p0}, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->d()LX/0am;

    move-result-object v2

    .line 1531219
    iget-object v3, v1, LX/9kw;->c:LX/0am;

    invoke-virtual {v3}, LX/0am;->isPresent()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1531220
    iget-object v3, v1, LX/9kw;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1531221
    :cond_0
    iput-object v2, v1, LX/9kw;->c:LX/0am;

    .line 1531222
    iget-object v3, v1, LX/9kw;->c:LX/0am;

    invoke-virtual {v3}, LX/0am;->isPresent()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1531223
    iget-object p1, v1, LX/9kw;->g:Landroid/widget/LinearLayout;

    iget-object v3, v1, LX/9kw;->c:LX/0am;

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    invoke-virtual {p1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1531224
    :cond_1
    iget-object v1, p0, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->a:LX/9kw;

    const/16 v2, 0x2b

    const v3, -0x41aa8e06

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x69969ee8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1531214
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1531215
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 1531216
    const/16 v1, 0x2b

    const v2, -0x791c00c7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x51f5c574

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1531211
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1531212
    invoke-virtual {p0}, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->l()V

    .line 1531213
    const/16 v1, 0x2b

    const v2, 0x36883cec

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1531202
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1531203
    invoke-direct {p0}, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->n()V

    .line 1531204
    iget-object v0, p0, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->a:LX/9kw;

    new-instance p1, LX/9kn;

    invoke-direct {p1, p0}, LX/9kn;-><init>(Lcom/facebook/places/pickers/PlaceContentPickerFragment;)V

    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object p1

    .line 1531205
    iput-object p1, v0, LX/9kw;->i:LX/0am;

    .line 1531206
    iget-object v0, p0, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->a:LX/9kw;

    new-instance p1, LX/9ko;

    invoke-direct {p1, p0}, LX/9ko;-><init>(Lcom/facebook/places/pickers/PlaceContentPickerFragment;)V

    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object p1

    .line 1531207
    iput-object p1, v0, LX/9kw;->h:LX/0am;

    .line 1531208
    iget-object v0, p0, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->a:LX/9kw;

    new-instance p1, LX/9kp;

    invoke-direct {p1, p0}, LX/9kp;-><init>(Lcom/facebook/places/pickers/PlaceContentPickerFragment;)V

    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object p1

    .line 1531209
    iget-object p0, v0, LX/9kw;->f:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {p1}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {p0, p2}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1531210
    return-void
.end method
