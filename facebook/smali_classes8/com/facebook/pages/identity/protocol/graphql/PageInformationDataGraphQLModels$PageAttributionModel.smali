.class public final Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x954eab1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLAttributionSource;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1510977
    const-class v0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1510976
    const-class v0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1510974
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1510975
    return-void
.end method

.method private j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1510972
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1510973
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1510962
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1510963
    invoke-direct {p0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1510964
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1510965
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel;->c()Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1510966
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1510967
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1510968
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1510969
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1510970
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1510971
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1510978
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1510979
    invoke-direct {p0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1510980
    invoke-direct {p0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1510981
    invoke-direct {p0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1510982
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel;

    .line 1510983
    iput-object v0, v1, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1510984
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1510985
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()LX/175;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1510961
    invoke-direct {p0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1510958
    new-instance v0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel;

    invoke-direct {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel;-><init>()V

    .line 1510959
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1510960
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1510956
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel;->f:Ljava/lang/String;

    .line 1510957
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/enums/GraphQLAttributionSource;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1510954
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel;->g:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLAttributionSource;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel;->g:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    .line 1510955
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel;->g:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1510953
    const v0, -0x7c95196

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1510952
    const v0, -0x2cd1bcad

    return v0
.end method
