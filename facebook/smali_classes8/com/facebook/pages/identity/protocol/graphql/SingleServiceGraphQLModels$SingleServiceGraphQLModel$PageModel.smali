.class public final Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x27400197
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Z

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1513352
    const-class v0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1513351
    const-class v0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1513349
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1513350
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 1513332
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1513333
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1513334
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1513335
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->l()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1513336
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->m()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, 0x445e5df4

    invoke-static {v4, v3, v5}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1513337
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1513338
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->p()LX/0Px;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/util/List;)I

    move-result v5

    .line 1513339
    const/4 v6, 0x7

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1513340
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 1513341
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1513342
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1513343
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1513344
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1513345
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1513346
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1513347
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1513348
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1513317
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1513318
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->l()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1513319
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->l()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    .line 1513320
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->l()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1513321
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;

    .line 1513322
    iput-object v0, v1, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->g:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    .line 1513323
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1513324
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x445e5df4

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1513325
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1513326
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;

    .line 1513327
    iput v3, v0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->h:I

    move-object v1, v0

    .line 1513328
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1513329
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 1513330
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object p0, v1

    .line 1513331
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1513316
    new-instance v0, LX/9aH;

    invoke-direct {v0, p1}, LX/9aH;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1513315
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1513311
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1513312
    const/4 v0, 0x3

    const v1, 0x445e5df4

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->h:I

    .line 1513313
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->i:Z

    .line 1513314
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1513309
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1513310
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1513289
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1513306
    new-instance v0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;

    invoke-direct {v0}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;-><init>()V

    .line 1513307
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1513308
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1513305
    const v0, -0x3d43e3ad

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1513304
    const v0, 0x25d6af

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1513302
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->e:Ljava/lang/String;

    .line 1513303
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1513300
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->f:Ljava/lang/String;

    .line 1513301
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1513298
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->g:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->g:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    .line 1513299
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->g:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    return-object v0
.end method

.method public final m()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfilePicture"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1513296
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1513297
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final n()Z
    .locals 2

    .prologue
    .line 1513294
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1513295
    iget-boolean v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->i:Z

    return v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1513292
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->j:Ljava/lang/String;

    .line 1513293
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final p()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1513290
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->k:Ljava/util/List;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->k:Ljava/util/List;

    .line 1513291
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->k:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method
