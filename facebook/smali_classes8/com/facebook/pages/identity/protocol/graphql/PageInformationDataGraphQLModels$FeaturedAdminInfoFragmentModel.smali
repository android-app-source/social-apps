.class public final Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x79063290
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:J

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:I

.field private j:I

.field private k:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel$UserModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1510931
    const-class v0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1510930
    const-class v0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1510928
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1510929
    return-void
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1510926
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->g:Ljava/lang/String;

    .line 1510927
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 1510911
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1510912
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1510913
    invoke-direct {p0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1510914
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1510915
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->o()Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel$UserModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1510916
    const/4 v0, 0x7

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1510917
    iget-wide v2, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->e:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1510918
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1510919
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1510920
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1510921
    const/4 v0, 0x4

    iget v2, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->i:I

    invoke-virtual {p1, v0, v2, v1}, LX/186;->a(III)V

    .line 1510922
    const/4 v0, 0x5

    iget v2, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->j:I

    invoke-virtual {p1, v0, v2, v1}, LX/186;->a(III)V

    .line 1510923
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1510924
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1510925
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1510903
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1510904
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->o()Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel$UserModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1510905
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->o()Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel$UserModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel$UserModel;

    .line 1510906
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->o()Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel$UserModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1510907
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;

    .line 1510908
    iput-object v0, v1, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->k:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel$UserModel;

    .line 1510909
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1510910
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1510902
    invoke-direct {p0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1510897
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1510898
    const-wide/16 v0, 0x0

    invoke-virtual {p1, p2, v2, v0, v1}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->e:J

    .line 1510899
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->i:I

    .line 1510900
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->j:I

    .line 1510901
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1510894
    new-instance v0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;

    invoke-direct {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;-><init>()V

    .line 1510895
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1510896
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1510880
    const v0, 0x61c7c69a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1510893
    const v0, -0x7bbb1411

    return v0
.end method

.method public final j()J
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1510891
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1510892
    iget-wide v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->e:J

    return-wide v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1510889
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->f:Ljava/lang/String;

    .line 1510890
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1510887
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->h:Ljava/lang/String;

    .line 1510888
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final m()I
    .locals 2

    .prologue
    .line 1510885
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1510886
    iget v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->i:I

    return v0
.end method

.method public final n()I
    .locals 2

    .prologue
    .line 1510883
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1510884
    iget v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->j:I

    return v0
.end method

.method public final o()Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel$UserModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getUser"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1510881
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->k:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel$UserModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel$UserModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel$UserModel;

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->k:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel$UserModel;

    .line 1510882
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->k:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel$UserModel;

    return-object v0
.end method
