.class public final Lcom/facebook/pages/identity/protocol/graphql/ServicesMutationsModels$ServicesSectionUpdateMutationModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1513059
    const-class v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesMutationsModels$ServicesSectionUpdateMutationModel;

    new-instance v1, Lcom/facebook/pages/identity/protocol/graphql/ServicesMutationsModels$ServicesSectionUpdateMutationModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/pages/identity/protocol/graphql/ServicesMutationsModels$ServicesSectionUpdateMutationModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1513060
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1513018
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1513019
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1513020
    const/4 v2, 0x0

    .line 1513021
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1513022
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1513023
    :goto_0
    move v1, v2

    .line 1513024
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1513025
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1513026
    new-instance v1, Lcom/facebook/pages/identity/protocol/graphql/ServicesMutationsModels$ServicesSectionUpdateMutationModel;

    invoke-direct {v1}, Lcom/facebook/pages/identity/protocol/graphql/ServicesMutationsModels$ServicesSectionUpdateMutationModel;-><init>()V

    .line 1513027
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1513028
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1513029
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1513030
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1513031
    :cond_0
    return-object v1

    .line 1513032
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1513033
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1513034
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1513035
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1513036
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 1513037
    const-string v4, "services_card"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1513038
    const/4 v3, 0x0

    .line 1513039
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_9

    .line 1513040
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1513041
    :goto_2
    move v1, v3

    .line 1513042
    goto :goto_1

    .line 1513043
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1513044
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1513045
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 1513046
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1513047
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, p0, :cond_8

    .line 1513048
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1513049
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1513050
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_6

    if-eqz v5, :cond_6

    .line 1513051
    const-string p0, "card_visibility"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 1513052
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_3

    .line 1513053
    :cond_7
    const-string p0, "description"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1513054
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_3

    .line 1513055
    :cond_8
    const/4 v5, 0x2

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1513056
    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 1513057
    const/4 v3, 0x1

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1513058
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v1, v3

    move v4, v3

    goto :goto_3
.end method
