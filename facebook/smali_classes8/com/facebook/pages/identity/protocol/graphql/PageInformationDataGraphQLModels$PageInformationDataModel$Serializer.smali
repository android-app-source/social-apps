.class public final Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1511334
    const-class v0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    new-instance v1, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1511335
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1511333
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;LX/0nX;LX/0my;)V
    .locals 9

    .prologue
    .line 1511243
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1511244
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/16 v8, 0x10

    const/16 v7, 0xf

    const/16 v6, 0xe

    const/16 v5, 0xd

    const/4 v4, 0x2

    .line 1511245
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1511246
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1511247
    if-eqz v2, :cond_1

    .line 1511248
    const-string v3, "attribution"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1511249
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1511250
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_0

    .line 1511251
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1, p2}, LX/9Zh;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1511252
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1511253
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1511254
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1511255
    if-eqz v2, :cond_3

    .line 1511256
    const-string v3, "business_info"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1511257
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1511258
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_2

    .line 1511259
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1, p2}, LX/9Zk;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1511260
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1511261
    :cond_2
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1511262
    :cond_3
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 1511263
    if-eqz v2, :cond_4

    .line 1511264
    const-string v2, "email_addresses"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1511265
    invoke-virtual {v1, v0, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1511266
    :cond_4
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1511267
    if-eqz v2, :cond_5

    .line 1511268
    const-string v3, "expressed_as_place"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1511269
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1511270
    :cond_5
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1511271
    if-eqz v2, :cond_6

    .line 1511272
    const-string v3, "hours"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1511273
    invoke-static {v1, v2, p1, p2}, LX/4aY;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1511274
    :cond_6
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1511275
    if-eqz v2, :cond_7

    .line 1511276
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1511277
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1511278
    :cond_7
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1511279
    if-eqz v2, :cond_8

    .line 1511280
    const-string v3, "is_always_open"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1511281
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1511282
    :cond_8
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1511283
    if-eqz v2, :cond_9

    .line 1511284
    const-string v3, "is_owned"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1511285
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1511286
    :cond_9
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1511287
    if-eqz v2, :cond_a

    .line 1511288
    const-string v3, "is_permanently_closed"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1511289
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1511290
    :cond_a
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1511291
    if-eqz v2, :cond_b

    .line 1511292
    const-string v3, "location"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1511293
    invoke-static {v1, v2, p1}, LX/9Zl;->a(LX/15i;ILX/0nX;)V

    .line 1511294
    :cond_b
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1511295
    if-eqz v2, :cond_c

    .line 1511296
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1511297
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1511298
    :cond_c
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1511299
    if-eqz v2, :cond_d

    .line 1511300
    const-string v3, "page_featured_admin_info"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1511301
    invoke-static {v1, v2, p1, p2}, LX/9Zn;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1511302
    :cond_d
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1511303
    if-eqz v2, :cond_f

    .line 1511304
    const-string v3, "page_info_sections"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1511305
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1511306
    const/4 v3, 0x0

    :goto_2
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result v4

    if-ge v3, v4, :cond_e

    .line 1511307
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result v4

    invoke-static {v1, v4, p1, p2}, LX/9Zo;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1511308
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1511309
    :cond_e
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1511310
    :cond_f
    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v2

    .line 1511311
    if-eqz v2, :cond_10

    .line 1511312
    const-string v2, "page_payment_options"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1511313
    invoke-virtual {v1, v0, v5}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1511314
    :cond_10
    invoke-virtual {v1, v0, v6}, LX/15i;->g(II)I

    move-result v2

    .line 1511315
    if-eqz v2, :cond_11

    .line 1511316
    const-string v2, "permanently_closed_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1511317
    invoke-virtual {v1, v0, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1511318
    :cond_11
    invoke-virtual {v1, v0, v7}, LX/15i;->g(II)I

    move-result v2

    .line 1511319
    if-eqz v2, :cond_12

    .line 1511320
    const-string v2, "place_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1511321
    invoke-virtual {v1, v0, v7}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1511322
    :cond_12
    invoke-virtual {v1, v0, v8}, LX/15i;->g(II)I

    move-result v2

    .line 1511323
    if-eqz v2, :cond_13

    .line 1511324
    const-string v2, "viewer_profile_permissions"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1511325
    invoke-virtual {v1, v0, v8}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1511326
    :cond_13
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1511327
    if-eqz v2, :cond_14

    .line 1511328
    const-string v2, "websites"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1511329
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1511330
    :cond_14
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1511331
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1511332
    check-cast p1, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$Serializer;->a(Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;LX/0nX;LX/0my;)V

    return-void
.end method
