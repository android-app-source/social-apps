.class public final Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1513219
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1513220
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1513217
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1513218
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1513208
    if-nez p1, :cond_0

    .line 1513209
    :goto_0
    return v0

    .line 1513210
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 1513211
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1513212
    :pswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1513213
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1513214
    const/4 v2, 0x1

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1513215
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1513216
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x445e5df4
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1513207
    new-instance v0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1513204
    packed-switch p0, :pswitch_data_0

    .line 1513205
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1513206
    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch 0x445e5df4
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1513203
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1513201
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$DraculaImplementation;->b(I)V

    .line 1513202
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1513196
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1513197
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1513198
    :cond_0
    iput-object p1, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1513199
    iput p2, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$DraculaImplementation;->b:I

    .line 1513200
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1513195
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1513194
    new-instance v0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1513191
    iget v0, p0, LX/1vt;->c:I

    .line 1513192
    move v0, v0

    .line 1513193
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1513170
    iget v0, p0, LX/1vt;->c:I

    .line 1513171
    move v0, v0

    .line 1513172
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1513188
    iget v0, p0, LX/1vt;->b:I

    .line 1513189
    move v0, v0

    .line 1513190
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1513185
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1513186
    move-object v0, v0

    .line 1513187
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1513176
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1513177
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1513178
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1513179
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1513180
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1513181
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1513182
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1513183
    invoke-static {v3, v9, v2}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1513184
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1513173
    iget v0, p0, LX/1vt;->c:I

    .line 1513174
    move v0, v0

    .line 1513175
    return v0
.end method
