.class public final Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1512366
    const-class v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel;

    new-instance v1, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1512367
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1512386
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1512369
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1512370
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x2

    .line 1512371
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1512372
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1512373
    if-eqz v2, :cond_0

    .line 1512374
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1512375
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1512376
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1512377
    if-eqz v2, :cond_1

    .line 1512378
    const-string v3, "services_card"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1512379
    invoke-static {v1, v2, p1, p2}, LX/9a5;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1512380
    :cond_1
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1512381
    if-eqz v2, :cond_2

    .line 1512382
    const-string v2, "viewer_profile_permissions"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1512383
    invoke-virtual {v1, v0, p0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1512384
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1512385
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1512368
    check-cast p1, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$Serializer;->a(Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel;LX/0nX;LX/0my;)V

    return-void
.end method
