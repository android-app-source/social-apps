.class public final Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;
.implements LX/9Zu;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2821cdc
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1512328
    const-class v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1512327
    const-class v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1512325
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1512326
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1512322
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1512323
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1512324
    return-void
.end method

.method public static a(LX/9Zu;)Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;
    .locals 14

    .prologue
    .line 1512286
    if-nez p0, :cond_0

    .line 1512287
    const/4 p0, 0x0

    .line 1512288
    :goto_0
    return-object p0

    .line 1512289
    :cond_0
    instance-of v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;

    if-eqz v0, :cond_1

    .line 1512290
    check-cast p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;

    goto :goto_0

    .line 1512291
    :cond_1
    new-instance v2, LX/9Zw;

    invoke-direct {v2}, LX/9Zw;-><init>()V

    .line 1512292
    invoke-interface {p0}, LX/9Zu;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/9Zw;->a:Ljava/lang/String;

    .line 1512293
    invoke-interface {p0}, LX/9Zu;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/9Zw;->b:Ljava/lang/String;

    .line 1512294
    invoke-interface {p0}, LX/9Zu;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/9Zw;->c:Ljava/lang/String;

    .line 1512295
    invoke-interface {p0}, LX/9Zu;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/9Zw;->d:Ljava/lang/String;

    .line 1512296
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1512297
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {p0}, LX/9Zu;->gE_()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1512298
    invoke-interface {p0}, LX/9Zu;->gE_()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel;

    invoke-static {v0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel;->a(Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel;)Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1512299
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1512300
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/9Zw;->e:LX/0Px;

    .line 1512301
    const/4 v8, 0x1

    const/4 v13, 0x0

    const/4 v6, 0x0

    .line 1512302
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 1512303
    iget-object v5, v2, LX/9Zw;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1512304
    iget-object v7, v2, LX/9Zw;->b:Ljava/lang/String;

    invoke-virtual {v4, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1512305
    iget-object v9, v2, LX/9Zw;->c:Ljava/lang/String;

    invoke-virtual {v4, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1512306
    iget-object v10, v2, LX/9Zw;->d:Ljava/lang/String;

    invoke-virtual {v4, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1512307
    iget-object v11, v2, LX/9Zw;->e:LX/0Px;

    invoke-static {v4, v11}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v11

    .line 1512308
    const/4 v12, 0x5

    invoke-virtual {v4, v12}, LX/186;->c(I)V

    .line 1512309
    invoke-virtual {v4, v13, v5}, LX/186;->b(II)V

    .line 1512310
    invoke-virtual {v4, v8, v7}, LX/186;->b(II)V

    .line 1512311
    const/4 v5, 0x2

    invoke-virtual {v4, v5, v9}, LX/186;->b(II)V

    .line 1512312
    const/4 v5, 0x3

    invoke-virtual {v4, v5, v10}, LX/186;->b(II)V

    .line 1512313
    const/4 v5, 0x4

    invoke-virtual {v4, v5, v11}, LX/186;->b(II)V

    .line 1512314
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 1512315
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 1512316
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 1512317
    invoke-virtual {v5, v13}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1512318
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1512319
    new-instance v5, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;

    invoke-direct {v5, v4}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;-><init>(LX/15i;)V

    .line 1512320
    move-object p0, v5

    .line 1512321
    goto/16 :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1512272
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1512273
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1512274
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1512275
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1512276
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1512277
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;->gE_()LX/0Px;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v4

    .line 1512278
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1512279
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1512280
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1512281
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1512282
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1512283
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1512284
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1512285
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1512263
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1512264
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;->gE_()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1512265
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;->gE_()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1512266
    if-eqz v1, :cond_0

    .line 1512267
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;

    .line 1512268
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;->i:Ljava/util/List;

    .line 1512269
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1512270
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1512271
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1512329
    new-instance v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;

    invoke-direct {v0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;-><init>()V

    .line 1512330
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1512331
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1512251
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;->e:Ljava/lang/String;

    .line 1512252
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1512253
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;->f:Ljava/lang/String;

    .line 1512254
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1512255
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;->g:Ljava/lang/String;

    .line 1512256
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1512257
    const v0, -0x7e694877

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1512258
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;->h:Ljava/lang/String;

    .line 1512259
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1512260
    const v0, 0xa7c5482

    return v0
.end method

.method public final gE_()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1512261
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;->i:Ljava/util/List;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;->i:Ljava/util/List;

    .line 1512262
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;->i:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method
