.class public final Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5f38e35
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel$ProductCatalogModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1512505
    const-class v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1512467
    const-class v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1512506
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1512507
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1512512
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->f:Ljava/lang/String;

    .line 1512513
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1512508
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->h:Ljava/lang/String;

    .line 1512509
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel$ProductCatalogModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProductCatalog"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1512510
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->i:Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel$ProductCatalogModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel$ProductCatalogModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel$ProductCatalogModel;

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->i:Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel$ProductCatalogModel;

    .line 1512511
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->i:Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel$ProductCatalogModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1512478
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1512479
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1512480
    invoke-direct {p0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1512481
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->d()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 1512482
    invoke-direct {p0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1512483
    invoke-direct {p0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->l()Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel$ProductCatalogModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1512484
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1512485
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1512486
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1512487
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1512488
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1512489
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1512490
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1512491
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1512492
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1512493
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->d()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1512494
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->d()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1512495
    if-eqz v1, :cond_2

    .line 1512496
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;

    .line 1512497
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->g:Ljava/util/List;

    move-object v1, v0

    .line 1512498
    :goto_0
    invoke-direct {p0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->l()Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel$ProductCatalogModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1512499
    invoke-direct {p0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->l()Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel$ProductCatalogModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel$ProductCatalogModel;

    .line 1512500
    invoke-direct {p0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->l()Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel$ProductCatalogModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1512501
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;

    .line 1512502
    iput-object v0, v1, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->i:Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel$ProductCatalogModel;

    .line 1512503
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1512504
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1512477
    invoke-direct {p0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1512474
    new-instance v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;

    invoke-direct {v0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;-><init>()V

    .line 1512475
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1512476
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel$ProductCatalogModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProductCatalog"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1512473
    invoke-direct {p0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->l()Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel$ProductCatalogModel;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1512471
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->e:Ljava/lang/String;

    .line 1512472
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1512469
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->g:Ljava/util/List;

    .line 1512470
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1512468
    const v0, -0x64b54ff2

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1512466
    const v0, -0x5c14c323

    return v0
.end method
