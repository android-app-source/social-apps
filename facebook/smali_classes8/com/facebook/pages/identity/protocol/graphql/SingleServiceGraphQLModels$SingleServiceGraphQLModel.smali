.class public final Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/9Zu;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7b2836cf
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1513409
    const-class v0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1513410
    const-class v0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1513411
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1513412
    return-void
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1513413
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1513414
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1513415
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 1513416
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1513417
    invoke-direct {p0}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->k()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1513418
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1513419
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1513420
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1513421
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1513422
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->gE_()LX/0Px;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 1513423
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->j()Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1513424
    const/4 v7, 0x7

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1513425
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 1513426
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1513427
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1513428
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1513429
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1513430
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1513431
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1513432
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1513433
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1513434
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1513435
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->gE_()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1513436
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->gE_()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1513437
    if-eqz v1, :cond_2

    .line 1513438
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;

    .line 1513439
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->j:Ljava/util/List;

    move-object v1, v0

    .line 1513440
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->j()Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1513441
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->j()Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;

    .line 1513442
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->j()Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1513443
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;

    .line 1513444
    iput-object v0, v1, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->k:Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;

    .line 1513445
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1513446
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1513447
    new-instance v0, LX/9aG;

    invoke-direct {v0, p1}, LX/9aG;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1513448
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1513407
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1513408
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1513449
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1513404
    new-instance v0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;

    invoke-direct {v0}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;-><init>()V

    .line 1513405
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1513406
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1513402
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->f:Ljava/lang/String;

    .line 1513403
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1513390
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->g:Ljava/lang/String;

    .line 1513391
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1513400
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->h:Ljava/lang/String;

    .line 1513401
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1513399
    const v0, -0x403ee64

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1513397
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->i:Ljava/lang/String;

    .line 1513398
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1513396
    const v0, 0x252222

    return v0
.end method

.method public final gE_()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1513394
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->j:Ljava/util/List;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->j:Ljava/util/List;

    .line 1513395
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->j:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final j()Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1513392
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->k:Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->k:Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;

    .line 1513393
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->k:Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;

    return-object v0
.end method
