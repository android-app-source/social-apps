.class public final Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1510932
    const-class v0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel;

    new-instance v1, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1510933
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1510934
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1510935
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1510936
    invoke-static {p1, v0}, LX/9Zh;->b(LX/15w;LX/186;)I

    move-result v1

    .line 1510937
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1510938
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1510939
    new-instance v1, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel;

    invoke-direct {v1}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel;-><init>()V

    .line 1510940
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1510941
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1510942
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1510943
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1510944
    :cond_0
    return-object v1
.end method
