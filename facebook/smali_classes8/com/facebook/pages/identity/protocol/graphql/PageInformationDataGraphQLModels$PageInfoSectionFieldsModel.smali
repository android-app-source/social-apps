.class public final Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x34e4e504
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLPageInfoFieldType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1511037
    const-class v0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1511038
    const-class v0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1511041
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1511042
    return-void
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLPageInfoFieldType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1511039
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLPageInfoFieldType;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageInfoFieldType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageInfoFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageInfoFieldType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageInfoFieldType;

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLPageInfoFieldType;

    .line 1511040
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLPageInfoFieldType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1511017
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1511018
    invoke-direct {p0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;->l()Lcom/facebook/graphql/enums/GraphQLPageInfoFieldType;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1511019
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1511020
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->c(Ljava/util/List;)I

    move-result v2

    .line 1511021
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1511022
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1511023
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1511024
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1511025
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1511026
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1511027
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1511028
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1511029
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1511030
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1511031
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1511032
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1511033
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;

    .line 1511034
    iput-object v0, v1, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;->h:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1511035
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1511036
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1511015
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;->f:Ljava/lang/String;

    .line 1511016
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1511012
    new-instance v0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;

    invoke-direct {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;-><init>()V

    .line 1511013
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1511014
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1511011
    const v0, -0x323a40e2

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1511010
    const v0, 0x7bb566fd

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1511008
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->c(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;->g:Ljava/util/List;

    .line 1511009
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1511006
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;->h:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;->h:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1511007
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;->h:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    return-object v0
.end method
