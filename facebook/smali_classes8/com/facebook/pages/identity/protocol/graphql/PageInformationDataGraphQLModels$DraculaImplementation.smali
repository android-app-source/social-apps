.class public final Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1510745
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1510746
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1510791
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1510792
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 8

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 1510747
    if-nez p1, :cond_0

    .line 1510748
    :goto_0
    return v0

    .line 1510749
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1510750
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1510751
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1510752
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1510753
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1510754
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1510755
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1510756
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1510757
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1510758
    invoke-virtual {p0, p1, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    move-result-object v2

    .line 1510759
    invoke-virtual {p3, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1510760
    invoke-virtual {p0, p1, v6}, LX/15i;->p(II)I

    move-result v3

    .line 1510761
    const v4, 0x2fe11788

    invoke-static {p0, v3, v4, p3}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    .line 1510762
    const/4 v4, 0x3

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1510763
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1510764
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 1510765
    invoke-virtual {p3, v6, v3}, LX/186;->b(II)V

    .line 1510766
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1510767
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1510768
    const v2, -0x7f0a2784

    const/4 v4, 0x0

    .line 1510769
    if-nez v1, :cond_1

    move v3, v4

    .line 1510770
    :goto_1
    move v1, v3

    .line 1510771
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1510772
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1510773
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1510774
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1510775
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 1510776
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1510777
    :sswitch_3
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 1510778
    invoke-virtual {p0, p1, v5, v0}, LX/15i;->a(III)I

    move-result v2

    .line 1510779
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1510780
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 1510781
    invoke-virtual {p3, v5, v2, v0}, LX/186;->a(III)V

    .line 1510782
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1510783
    :cond_1
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v7

    .line 1510784
    if-nez v7, :cond_2

    const/4 v3, 0x0

    .line 1510785
    :goto_2
    if-ge v4, v7, :cond_3

    .line 1510786
    invoke-virtual {p0, v1, v4}, LX/15i;->q(II)I

    move-result p2

    .line 1510787
    invoke-static {p0, p2, v2, p3}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p2

    aput p2, v3, v4

    .line 1510788
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1510789
    :cond_2
    new-array v3, v7, [I

    goto :goto_2

    .line 1510790
    :cond_3
    const/4 v4, 0x1

    invoke-virtual {p3, v3, v4}, LX/186;->a([IZ)I

    move-result v3

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7f0a2784 -> :sswitch_3
        -0x1f29fb27 -> :sswitch_0
        0x2fe11788 -> :sswitch_2
        0x74780072 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1510736
    if-nez p0, :cond_0

    move v0, v1

    .line 1510737
    :goto_0
    return v0

    .line 1510738
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 1510739
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1510740
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1510741
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 1510742
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1510743
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1510744
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 1510729
    const/4 v7, 0x0

    .line 1510730
    const/4 v1, 0x0

    .line 1510731
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 1510732
    invoke-static {v2, v3, v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1510733
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 1510734
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1510735
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1510728
    new-instance v0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 1510714
    sparse-switch p2, :sswitch_data_0

    .line 1510715
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1510716
    :sswitch_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1510717
    const v1, 0x2fe11788

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1510718
    :goto_0
    :sswitch_1
    return-void

    .line 1510719
    :sswitch_2
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1510720
    const v1, -0x7f0a2784

    .line 1510721
    if-eqz v0, :cond_0

    .line 1510722
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 1510723
    const/4 v2, 0x0

    :goto_1
    if-ge v2, p1, :cond_0

    .line 1510724
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 1510725
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1510726
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1510727
    :cond_0
    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7f0a2784 -> :sswitch_1
        -0x1f29fb27 -> :sswitch_1
        0x2fe11788 -> :sswitch_2
        0x74780072 -> :sswitch_0
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1510708
    if-eqz p1, :cond_0

    .line 1510709
    invoke-static {p0, p1, p2}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 1510710
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$DraculaImplementation;

    .line 1510711
    if-eq v0, v1, :cond_0

    .line 1510712
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1510713
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1510707
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1510793
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1510794
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1510702
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1510703
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1510704
    :cond_0
    iput-object p1, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1510705
    iput p2, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$DraculaImplementation;->b:I

    .line 1510706
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1510701
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1510700
    new-instance v0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1510697
    iget v0, p0, LX/1vt;->c:I

    .line 1510698
    move v0, v0

    .line 1510699
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1510694
    iget v0, p0, LX/1vt;->c:I

    .line 1510695
    move v0, v0

    .line 1510696
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1510691
    iget v0, p0, LX/1vt;->b:I

    .line 1510692
    move v0, v0

    .line 1510693
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1510676
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1510677
    move-object v0, v0

    .line 1510678
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1510682
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1510683
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1510684
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1510685
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1510686
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1510687
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1510688
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1510689
    invoke-static {v3, v9, v2}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1510690
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1510679
    iget v0, p0, LX/1vt;->c:I

    .line 1510680
    move v0, v0

    .line 1510681
    return v0
.end method
