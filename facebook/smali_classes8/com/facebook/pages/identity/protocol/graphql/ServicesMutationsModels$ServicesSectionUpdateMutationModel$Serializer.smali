.class public final Lcom/facebook/pages/identity/protocol/graphql/ServicesMutationsModels$ServicesSectionUpdateMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/identity/protocol/graphql/ServicesMutationsModels$ServicesSectionUpdateMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1513061
    const-class v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesMutationsModels$ServicesSectionUpdateMutationModel;

    new-instance v1, Lcom/facebook/pages/identity/protocol/graphql/ServicesMutationsModels$ServicesSectionUpdateMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/identity/protocol/graphql/ServicesMutationsModels$ServicesSectionUpdateMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1513062
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1513063
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/identity/protocol/graphql/ServicesMutationsModels$ServicesSectionUpdateMutationModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1513064
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1513065
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1513066
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1513067
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1513068
    if-eqz v2, :cond_2

    .line 1513069
    const-string p0, "services_card"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1513070
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1513071
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1513072
    if-eqz p0, :cond_0

    .line 1513073
    const-string v0, "card_visibility"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1513074
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1513075
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v2, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1513076
    if-eqz p0, :cond_1

    .line 1513077
    const-string v0, "description"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1513078
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1513079
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1513080
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1513081
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1513082
    check-cast p1, Lcom/facebook/pages/identity/protocol/graphql/ServicesMutationsModels$ServicesSectionUpdateMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/identity/protocol/graphql/ServicesMutationsModels$ServicesSectionUpdateMutationModel$Serializer;->a(Lcom/facebook/pages/identity/protocol/graphql/ServicesMutationsModels$ServicesSectionUpdateMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
