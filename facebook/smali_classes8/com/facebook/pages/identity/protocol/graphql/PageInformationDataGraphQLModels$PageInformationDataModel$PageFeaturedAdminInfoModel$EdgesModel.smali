.class public final Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$PageFeaturedAdminInfoModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3983f516
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$PageFeaturedAdminInfoModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$PageFeaturedAdminInfoModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1511165
    const-class v0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$PageFeaturedAdminInfoModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1511164
    const-class v0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$PageFeaturedAdminInfoModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1511162
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1511163
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1511156
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1511157
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$PageFeaturedAdminInfoModel$EdgesModel;->a()Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1511158
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1511159
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1511160
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1511161
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1511141
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1511142
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$PageFeaturedAdminInfoModel$EdgesModel;->a()Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1511143
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$PageFeaturedAdminInfoModel$EdgesModel;->a()Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;

    .line 1511144
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$PageFeaturedAdminInfoModel$EdgesModel;->a()Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1511145
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$PageFeaturedAdminInfoModel$EdgesModel;

    .line 1511146
    iput-object v0, v1, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$PageFeaturedAdminInfoModel$EdgesModel;->e:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;

    .line 1511147
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1511148
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1511154
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$PageFeaturedAdminInfoModel$EdgesModel;->e:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$PageFeaturedAdminInfoModel$EdgesModel;->e:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;

    .line 1511155
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$PageFeaturedAdminInfoModel$EdgesModel;->e:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1511151
    new-instance v0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$PageFeaturedAdminInfoModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$PageFeaturedAdminInfoModel$EdgesModel;-><init>()V

    .line 1511152
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1511153
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1511150
    const v0, -0xef67b00

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1511149
    const v0, -0x5be378fe

    return v0
.end method
