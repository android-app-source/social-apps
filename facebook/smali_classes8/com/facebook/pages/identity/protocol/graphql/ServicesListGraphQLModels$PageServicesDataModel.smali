.class public final Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x78f3666c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1512551
    const-class v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1512550
    const-class v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1512548
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1512549
    return-void
.end method

.method private j()Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getServicesCard"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1512546
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel;->f:Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel;->f:Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;

    .line 1512547
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel;->f:Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1512536
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1512537
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1512538
    invoke-direct {p0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel;->j()Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1512539
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v2

    .line 1512540
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1512541
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1512542
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1512543
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1512544
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1512545
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1512528
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1512529
    invoke-direct {p0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel;->j()Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1512530
    invoke-direct {p0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel;->j()Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;

    .line 1512531
    invoke-direct {p0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel;->j()Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1512532
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel;

    .line 1512533
    iput-object v0, v1, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel;->f:Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;

    .line 1512534
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1512535
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1512527
    new-instance v0, LX/9Zz;

    invoke-direct {v0, p1}, LX/9Zz;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1512514
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel;->e:Ljava/lang/String;

    .line 1512515
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1512525
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1512526
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1512524
    return-void
.end method

.method public final b()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1512522
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel;->g:Ljava/util/List;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel;->g:Ljava/util/List;

    .line 1512523
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1512519
    new-instance v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel;

    invoke-direct {v0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel;-><init>()V

    .line 1512520
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1512521
    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getServicesCard"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1512518
    invoke-direct {p0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel;->j()Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1512517
    const v0, -0x22eb7723

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1512516
    const v0, 0x25d6af

    return v0
.end method
