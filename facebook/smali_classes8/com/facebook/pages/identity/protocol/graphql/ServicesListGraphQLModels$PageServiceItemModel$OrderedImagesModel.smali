.class public final Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2fbedc31
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel$ImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1512243
    const-class v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1512242
    const-class v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1512240
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1512241
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1512237
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1512238
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1512239
    return-void
.end method

.method public static a(Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel;)Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel;
    .locals 8

    .prologue
    .line 1512217
    if-nez p0, :cond_0

    .line 1512218
    const/4 p0, 0x0

    .line 1512219
    :goto_0
    return-object p0

    .line 1512220
    :cond_0
    instance-of v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel;

    if-eqz v0, :cond_1

    .line 1512221
    check-cast p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel;

    goto :goto_0

    .line 1512222
    :cond_1
    new-instance v0, LX/9Zx;

    invoke-direct {v0}, LX/9Zx;-><init>()V

    .line 1512223
    invoke-virtual {p0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel;->a()Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel$ImageModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel$ImageModel;->a(Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel$ImageModel;)Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel$ImageModel;

    move-result-object v1

    iput-object v1, v0, LX/9Zx;->a:Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel$ImageModel;

    .line 1512224
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1512225
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1512226
    iget-object v3, v0, LX/9Zx;->a:Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel$ImageModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1512227
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1512228
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 1512229
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1512230
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1512231
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1512232
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1512233
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1512234
    new-instance v3, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel;

    invoke-direct {v3, v2}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel;-><init>(LX/15i;)V

    .line 1512235
    move-object p0, v3

    .line 1512236
    goto :goto_0
.end method

.method private j()Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel$ImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1512215
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel;->e:Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel$ImageModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel$ImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel$ImageModel;

    iput-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel;->e:Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel$ImageModel;

    .line 1512216
    iget-object v0, p0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel;->e:Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel$ImageModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1512209
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1512210
    invoke-direct {p0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel;->j()Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel$ImageModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1512211
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1512212
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1512213
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1512214
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1512201
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1512202
    invoke-direct {p0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel;->j()Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel$ImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1512203
    invoke-direct {p0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel;->j()Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel$ImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel$ImageModel;

    .line 1512204
    invoke-direct {p0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel;->j()Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel$ImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1512205
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel;

    .line 1512206
    iput-object v0, v1, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel;->e:Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel$ImageModel;

    .line 1512207
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1512208
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel$ImageModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1512195
    invoke-direct {p0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel;->j()Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel$ImageModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1512198
    new-instance v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel;

    invoke-direct {v0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel;-><init>()V

    .line 1512199
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1512200
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1512197
    const v0, 0x65854003

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1512196
    const v0, 0x450afdcc

    return v0
.end method
