.class public final Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/9Y7;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5bb62c32
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLPageActionType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$DescriptionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1505703
    const-class v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1505702
    const-class v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1505700
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1505701
    return-void
.end method

.method private a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$DescriptionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1505698
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$DescriptionModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$DescriptionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$DescriptionModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$DescriptionModel;

    .line 1505699
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$DescriptionModel;

    return-object v0
.end method

.method private l()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1505696
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;

    .line 1505697
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;

    return-object v0
.end method

.method private m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1505694
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->i:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->i:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;

    .line 1505695
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->i:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1505692
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->j:Ljava/lang/String;

    .line 1505693
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1505690
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->l:Ljava/lang/String;

    .line 1505691
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->l:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    .line 1505670
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1505671
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1505672
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->e()Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1505673
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$DescriptionModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1505674
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->l()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1505675
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1505676
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->n()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1505677
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->q()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1505678
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1505679
    const/16 v8, 0x8

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1505680
    const/4 v8, 0x0

    invoke-virtual {p1, v8, v0}, LX/186;->b(II)V

    .line 1505681
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1505682
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1505683
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1505684
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1505685
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1505686
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1505687
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1505688
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1505689
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1505652
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1505653
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$DescriptionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1505654
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$DescriptionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$DescriptionModel;

    .line 1505655
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$DescriptionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1505656
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;

    .line 1505657
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$DescriptionModel;

    .line 1505658
    :cond_0
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->l()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1505659
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->l()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;

    .line 1505660
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->l()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1505661
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;

    .line 1505662
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;

    .line 1505663
    :cond_1
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1505664
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;

    .line 1505665
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1505666
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;

    .line 1505667
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->i:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;

    .line 1505668
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1505669
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLPageActionType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1505639
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->e:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->e:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 1505640
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->e:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1505649
    new-instance v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;

    invoke-direct {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;-><init>()V

    .line 1505650
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1505651
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1505648
    const v0, -0x29ca2e40

    return v0
.end method

.method public final e()Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1505646
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->f:Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->f:Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    .line 1505647
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->f:Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1505645
    const v0, 0x2aef3e3b

    return v0
.end method

.method public final synthetic j()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1505644
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->l()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1505643
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;

    move-result-object v0

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1505641
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->k:Ljava/lang/String;

    .line 1505642
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel;->k:Ljava/lang/String;

    return-object v0
.end method
