.class public final Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelReplaceInOrderingMutationModels$PageActionChannelReplaceInOrderingMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x9c74b80
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelReplaceInOrderingMutationModels$PageActionChannelReplaceInOrderingMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelReplaceInOrderingMutationModels$PageActionChannelReplaceInOrderingMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelReplaceInOrderingMutationModels$PageActionChannelReplaceInOrderingMutationModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1503721
    const-class v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelReplaceInOrderingMutationModels$PageActionChannelReplaceInOrderingMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1503724
    const-class v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelReplaceInOrderingMutationModels$PageActionChannelReplaceInOrderingMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1503722
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1503723
    return-void
.end method

.method private a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelReplaceInOrderingMutationModels$PageActionChannelReplaceInOrderingMutationModel$PageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1503719
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelReplaceInOrderingMutationModels$PageActionChannelReplaceInOrderingMutationModel;->e:Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelReplaceInOrderingMutationModels$PageActionChannelReplaceInOrderingMutationModel$PageModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelReplaceInOrderingMutationModels$PageActionChannelReplaceInOrderingMutationModel$PageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelReplaceInOrderingMutationModels$PageActionChannelReplaceInOrderingMutationModel$PageModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelReplaceInOrderingMutationModels$PageActionChannelReplaceInOrderingMutationModel;->e:Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelReplaceInOrderingMutationModels$PageActionChannelReplaceInOrderingMutationModel$PageModel;

    .line 1503720
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelReplaceInOrderingMutationModels$PageActionChannelReplaceInOrderingMutationModel;->e:Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelReplaceInOrderingMutationModels$PageActionChannelReplaceInOrderingMutationModel$PageModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1503725
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1503726
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelReplaceInOrderingMutationModels$PageActionChannelReplaceInOrderingMutationModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelReplaceInOrderingMutationModels$PageActionChannelReplaceInOrderingMutationModel$PageModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1503727
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1503728
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1503729
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1503730
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1503711
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1503712
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelReplaceInOrderingMutationModels$PageActionChannelReplaceInOrderingMutationModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelReplaceInOrderingMutationModels$PageActionChannelReplaceInOrderingMutationModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1503713
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelReplaceInOrderingMutationModels$PageActionChannelReplaceInOrderingMutationModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelReplaceInOrderingMutationModels$PageActionChannelReplaceInOrderingMutationModel$PageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelReplaceInOrderingMutationModels$PageActionChannelReplaceInOrderingMutationModel$PageModel;

    .line 1503714
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelReplaceInOrderingMutationModels$PageActionChannelReplaceInOrderingMutationModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelReplaceInOrderingMutationModels$PageActionChannelReplaceInOrderingMutationModel$PageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1503715
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelReplaceInOrderingMutationModels$PageActionChannelReplaceInOrderingMutationModel;

    .line 1503716
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelReplaceInOrderingMutationModels$PageActionChannelReplaceInOrderingMutationModel;->e:Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelReplaceInOrderingMutationModels$PageActionChannelReplaceInOrderingMutationModel$PageModel;

    .line 1503717
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1503718
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1503706
    new-instance v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelReplaceInOrderingMutationModels$PageActionChannelReplaceInOrderingMutationModel;

    invoke-direct {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelReplaceInOrderingMutationModels$PageActionChannelReplaceInOrderingMutationModel;-><init>()V

    .line 1503707
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1503708
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1503710
    const v0, 0x48f3c826

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1503709
    const v0, 0x442bf1fe

    return v0
.end method
