.class public final Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelAddToOrderingMutationModels$PageActionChannelAddToOrderingMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelAddToOrderingMutationModels$PageActionChannelAddToOrderingMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1503112
    const-class v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelAddToOrderingMutationModels$PageActionChannelAddToOrderingMutationModel;

    new-instance v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelAddToOrderingMutationModels$PageActionChannelAddToOrderingMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelAddToOrderingMutationModels$PageActionChannelAddToOrderingMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1503113
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1503114
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelAddToOrderingMutationModels$PageActionChannelAddToOrderingMutationModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1503115
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1503116
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1503117
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1503118
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1503119
    if-eqz v2, :cond_0

    .line 1503120
    const-string p0, "page"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1503121
    invoke-static {v1, v2, p1}, LX/9Xc;->a(LX/15i;ILX/0nX;)V

    .line 1503122
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1503123
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1503124
    check-cast p1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelAddToOrderingMutationModels$PageActionChannelAddToOrderingMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelAddToOrderingMutationModels$PageActionChannelAddToOrderingMutationModel$Serializer;->a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelAddToOrderingMutationModels$PageActionChannelAddToOrderingMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
