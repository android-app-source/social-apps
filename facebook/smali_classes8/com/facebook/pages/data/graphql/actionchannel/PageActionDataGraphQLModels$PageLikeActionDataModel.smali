.class public final Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/9Y5;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5f527735
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLPageActionType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$CalloutTextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$DescriptionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1505144
    const-class v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1505143
    const-class v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1505141
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1505142
    return-void
.end method

.method private a()Lcom/facebook/graphql/enums/GraphQLPageActionType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1505098
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel;->e:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel;->e:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 1505099
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel;->e:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    return-object v0
.end method

.method private j()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$CalloutTextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1505139
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel;->f:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$CalloutTextModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$CalloutTextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$CalloutTextModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel;->f:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$CalloutTextModel;

    .line 1505140
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel;->f:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$CalloutTextModel;

    return-object v0
.end method

.method private k()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$DescriptionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1505137
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel;->g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$DescriptionModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$DescriptionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$DescriptionModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel;->g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$DescriptionModel;

    .line 1505138
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel;->g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$DescriptionModel;

    return-object v0
.end method

.method private l()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$PageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1505135
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel;->h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$PageModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$PageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$PageModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel;->h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$PageModel;

    .line 1505136
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel;->h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$PageModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1505123
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1505124
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel;->a()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1505125
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel;->j()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$CalloutTextModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1505126
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel;->k()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$DescriptionModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1505127
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel;->l()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$PageModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1505128
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1505129
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1505130
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1505131
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1505132
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1505133
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1505134
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1505105
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1505106
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel;->j()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$CalloutTextModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1505107
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel;->j()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$CalloutTextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$CalloutTextModel;

    .line 1505108
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel;->j()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$CalloutTextModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1505109
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel;

    .line 1505110
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel;->f:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$CalloutTextModel;

    .line 1505111
    :cond_0
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel;->k()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$DescriptionModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1505112
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel;->k()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$DescriptionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$DescriptionModel;

    .line 1505113
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel;->k()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$DescriptionModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1505114
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel;

    .line 1505115
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel;->g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$DescriptionModel;

    .line 1505116
    :cond_1
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel;->l()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1505117
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel;->l()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$PageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$PageModel;

    .line 1505118
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel;->l()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$PageModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1505119
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel;

    .line 1505120
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel;->h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$PageModel;

    .line 1505121
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1505122
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1505102
    new-instance v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel;

    invoke-direct {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel;-><init>()V

    .line 1505103
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1505104
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1505101
    const v0, 0x289ef6f8

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1505100
    const v0, -0x1ff0cf44

    return v0
.end method
