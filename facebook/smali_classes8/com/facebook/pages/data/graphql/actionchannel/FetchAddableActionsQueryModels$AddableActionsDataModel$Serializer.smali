.class public final Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1502944
    const-class v0, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel;

    new-instance v1, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1502945
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1502946
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1502947
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1502948
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x2

    .line 1502949
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1502950
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1502951
    if-eqz v2, :cond_0

    .line 1502952
    const-string v3, "addableActionsChannel"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1502953
    invoke-static {v1, v2, p1, p2}, LX/9XW;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1502954
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1502955
    if-eqz v2, :cond_1

    .line 1502956
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1502957
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1502958
    :cond_1
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1502959
    if-eqz v2, :cond_2

    .line 1502960
    const-string v2, "viewer_profile_permissions"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1502961
    invoke-virtual {v1, v0, p0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1502962
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1502963
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1502964
    check-cast p1, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel$Serializer;->a(Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel;LX/0nX;LX/0my;)V

    return-void
.end method
