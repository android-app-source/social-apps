.class public final Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1504808
    const-class v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel;

    new-instance v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1504809
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1504810
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1504811
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1504812
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1504813
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1504814
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1504815
    if-eqz v2, :cond_0

    .line 1504816
    const-string v2, "action_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1504817
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1504818
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1504819
    if-eqz v2, :cond_1

    .line 1504820
    const-string p0, "description"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1504821
    invoke-static {v1, v2, p1}, LX/9YY;->a(LX/15i;ILX/0nX;)V

    .line 1504822
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1504823
    if-eqz v2, :cond_2

    .line 1504824
    const-string p0, "page"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1504825
    invoke-static {v1, v2, p1, p2}, LX/9Ya;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1504826
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1504827
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1504828
    check-cast p1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$Serializer;->a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel;LX/0nX;LX/0my;)V

    return-void
.end method
