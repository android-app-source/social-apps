.class public final Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelRemoveFromOrderingMutationModels$PageActionChannelRemoveFromOrderingMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x504bb282
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelRemoveFromOrderingMutationModels$PageActionChannelRemoveFromOrderingMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelRemoveFromOrderingMutationModels$PageActionChannelRemoveFromOrderingMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelRemoveFromOrderingMutationModels$PageActionChannelRemoveFromOrderingMutationModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1503440
    const-class v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelRemoveFromOrderingMutationModels$PageActionChannelRemoveFromOrderingMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1503439
    const-class v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelRemoveFromOrderingMutationModels$PageActionChannelRemoveFromOrderingMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1503437
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1503438
    return-void
.end method

.method private a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelRemoveFromOrderingMutationModels$PageActionChannelRemoveFromOrderingMutationModel$PageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1503435
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelRemoveFromOrderingMutationModels$PageActionChannelRemoveFromOrderingMutationModel;->e:Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelRemoveFromOrderingMutationModels$PageActionChannelRemoveFromOrderingMutationModel$PageModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelRemoveFromOrderingMutationModels$PageActionChannelRemoveFromOrderingMutationModel$PageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelRemoveFromOrderingMutationModels$PageActionChannelRemoveFromOrderingMutationModel$PageModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelRemoveFromOrderingMutationModels$PageActionChannelRemoveFromOrderingMutationModel;->e:Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelRemoveFromOrderingMutationModels$PageActionChannelRemoveFromOrderingMutationModel$PageModel;

    .line 1503436
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelRemoveFromOrderingMutationModels$PageActionChannelRemoveFromOrderingMutationModel;->e:Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelRemoveFromOrderingMutationModels$PageActionChannelRemoveFromOrderingMutationModel$PageModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1503416
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1503417
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelRemoveFromOrderingMutationModels$PageActionChannelRemoveFromOrderingMutationModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelRemoveFromOrderingMutationModels$PageActionChannelRemoveFromOrderingMutationModel$PageModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1503418
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1503419
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1503420
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1503421
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1503427
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1503428
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelRemoveFromOrderingMutationModels$PageActionChannelRemoveFromOrderingMutationModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelRemoveFromOrderingMutationModels$PageActionChannelRemoveFromOrderingMutationModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1503429
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelRemoveFromOrderingMutationModels$PageActionChannelRemoveFromOrderingMutationModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelRemoveFromOrderingMutationModels$PageActionChannelRemoveFromOrderingMutationModel$PageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelRemoveFromOrderingMutationModels$PageActionChannelRemoveFromOrderingMutationModel$PageModel;

    .line 1503430
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelRemoveFromOrderingMutationModels$PageActionChannelRemoveFromOrderingMutationModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelRemoveFromOrderingMutationModels$PageActionChannelRemoveFromOrderingMutationModel$PageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1503431
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelRemoveFromOrderingMutationModels$PageActionChannelRemoveFromOrderingMutationModel;

    .line 1503432
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelRemoveFromOrderingMutationModels$PageActionChannelRemoveFromOrderingMutationModel;->e:Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelRemoveFromOrderingMutationModels$PageActionChannelRemoveFromOrderingMutationModel$PageModel;

    .line 1503433
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1503434
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1503424
    new-instance v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelRemoveFromOrderingMutationModels$PageActionChannelRemoveFromOrderingMutationModel;

    invoke-direct {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelRemoveFromOrderingMutationModels$PageActionChannelRemoveFromOrderingMutationModel;-><init>()V

    .line 1503425
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1503426
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1503423
    const v0, -0x352c1659    # -6943955.5f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1503422
    const v0, -0x3b695c73

    return v0
.end method
