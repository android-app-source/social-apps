.class public final Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1505744
    const-class v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;

    new-instance v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1505745
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1505746
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 1505747
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1505748
    const/4 v2, 0x0

    .line 1505749
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_9

    .line 1505750
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1505751
    :goto_0
    move v1, v2

    .line 1505752
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1505753
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1505754
    new-instance v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;-><init>()V

    .line 1505755
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1505756
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1505757
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1505758
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1505759
    :cond_0
    return-object v1

    .line 1505760
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1505761
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, p0, :cond_8

    .line 1505762
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1505763
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1505764
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v8, :cond_2

    .line 1505765
    const-string p0, "action_type"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1505766
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    goto :goto_1

    .line 1505767
    :cond_3
    const-string p0, "description"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1505768
    invoke-static {p1, v0}, LX/9Ym;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1505769
    :cond_4
    const-string p0, "page"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1505770
    invoke-static {p1, v0}, LX/9Yn;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1505771
    :cond_5
    const-string p0, "preview_action_type"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1505772
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 1505773
    :cond_6
    const-string p0, "preview_optional_id"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 1505774
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1505775
    :cond_7
    const-string p0, "preview_text"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1505776
    invoke-static {p1, v0}, LX/9Yo;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_1

    .line 1505777
    :cond_8
    const/4 v8, 0x6

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 1505778
    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1505779
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1505780
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1505781
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1505782
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1505783
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1505784
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_9
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    goto/16 :goto_1
.end method
