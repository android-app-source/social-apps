.class public final Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x50a312db
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1505853
    const-class v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1505895
    const-class v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1505893
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1505894
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1505890
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1505891
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1505892
    return-void
.end method

.method public static a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;)Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;
    .locals 8

    .prologue
    .line 1505870
    if-nez p0, :cond_0

    .line 1505871
    const/4 p0, 0x0

    .line 1505872
    :goto_0
    return-object p0

    .line 1505873
    :cond_0
    instance-of v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;

    if-eqz v0, :cond_1

    .line 1505874
    check-cast p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;

    goto :goto_0

    .line 1505875
    :cond_1
    new-instance v0, LX/9YQ;

    invoke-direct {v0}, LX/9YQ;-><init>()V

    .line 1505876
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9YQ;->a:Ljava/lang/String;

    .line 1505877
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1505878
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1505879
    iget-object v3, v0, LX/9YQ;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1505880
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1505881
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 1505882
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1505883
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1505884
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1505885
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1505886
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1505887
    new-instance v3, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;

    invoke-direct {v3, v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;-><init>(LX/15i;)V

    .line 1505888
    move-object p0, v3

    .line 1505889
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1505864
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1505865
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1505866
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1505867
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1505868
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1505869
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1505861
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1505862
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1505863
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1505859
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;->e:Ljava/lang/String;

    .line 1505860
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1505856
    new-instance v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;

    invoke-direct {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;-><init>()V

    .line 1505857
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1505858
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1505855
    const v0, -0x6d2be979

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1505854
    const v0, -0x726d476c

    return v0
.end method
