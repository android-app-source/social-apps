.class public final Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageMessageActionDataModel$PageModel$NuxStateModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x55455ec0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageMessageActionDataModel$PageModel$NuxStateModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageMessageActionDataModel$PageModel$NuxStateModel$Serializer;
.end annotation


# instance fields
.field private e:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1505254
    const-class v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageMessageActionDataModel$PageModel$NuxStateModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1505253
    const-class v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageMessageActionDataModel$PageModel$NuxStateModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1505292
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1505293
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1505289
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1505290
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1505291
    return-void
.end method

.method public static a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageMessageActionDataModel$PageModel$NuxStateModel;)Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageMessageActionDataModel$PageModel$NuxStateModel;
    .locals 8

    .prologue
    .line 1505270
    if-nez p0, :cond_0

    .line 1505271
    const/4 p0, 0x0

    .line 1505272
    :goto_0
    return-object p0

    .line 1505273
    :cond_0
    instance-of v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageMessageActionDataModel$PageModel$NuxStateModel;

    if-eqz v0, :cond_1

    .line 1505274
    check-cast p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageMessageActionDataModel$PageModel$NuxStateModel;

    goto :goto_0

    .line 1505275
    :cond_1
    new-instance v0, LX/9YM;

    invoke-direct {v0}, LX/9YM;-><init>()V

    .line 1505276
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageMessageActionDataModel$PageModel$NuxStateModel;->a()Z

    move-result v1

    iput-boolean v1, v0, LX/9YM;->a:Z

    .line 1505277
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1505278
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1505279
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1505280
    iget-boolean v3, v0, LX/9YM;->a:Z

    invoke-virtual {v2, v5, v3}, LX/186;->a(IZ)V

    .line 1505281
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1505282
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1505283
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1505284
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1505285
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1505286
    new-instance v3, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageMessageActionDataModel$PageModel$NuxStateModel;

    invoke-direct {v3, v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageMessageActionDataModel$PageModel$NuxStateModel;-><init>(LX/15i;)V

    .line 1505287
    move-object p0, v3

    .line 1505288
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1505265
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1505266
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1505267
    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageMessageActionDataModel$PageModel$NuxStateModel;->e:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1505268
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1505269
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1505294
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1505295
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1505296
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1505262
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1505263
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageMessageActionDataModel$PageModel$NuxStateModel;->e:Z

    .line 1505264
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1505260
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1505261
    iget-boolean v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageMessageActionDataModel$PageModel$NuxStateModel;->e:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1505257
    new-instance v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageMessageActionDataModel$PageModel$NuxStateModel;

    invoke-direct {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageMessageActionDataModel$PageModel$NuxStateModel;-><init>()V

    .line 1505258
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1505259
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1505256
    const v0, -0x62811ff4

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1505255
    const v0, 0x4019d74f

    return v0
.end method
