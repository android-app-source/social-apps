.class public final Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1504650
    const-class v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel;

    new-instance v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1504651
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1504652
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1504653
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1504654
    const/4 v2, 0x0

    .line 1504655
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_6

    .line 1504656
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1504657
    :goto_0
    move v1, v2

    .line 1504658
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1504659
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1504660
    new-instance v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel;-><init>()V

    .line 1504661
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1504662
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1504663
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1504664
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1504665
    :cond_0
    return-object v1

    .line 1504666
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1504667
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, p0, :cond_5

    .line 1504668
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1504669
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1504670
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v5, :cond_2

    .line 1504671
    const-string p0, "action_type"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1504672
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    goto :goto_1

    .line 1504673
    :cond_3
    const-string p0, "description"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1504674
    invoke-static {p1, v0}, LX/9YY;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1504675
    :cond_4
    const-string p0, "page"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1504676
    invoke-static {p1, v0}, LX/9Ya;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_1

    .line 1504677
    :cond_5
    const/4 v5, 0x3

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1504678
    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1504679
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1504680
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1504681
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_6
    move v1, v2

    move v3, v2

    move v4, v2

    goto :goto_1
.end method
