.class public final Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x50a312db
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1505631
    const-class v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1505589
    const-class v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1505629
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1505630
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1505626
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1505627
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1505628
    return-void
.end method

.method public static a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;)Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;
    .locals 8

    .prologue
    .line 1505606
    if-nez p0, :cond_0

    .line 1505607
    const/4 p0, 0x0

    .line 1505608
    :goto_0
    return-object p0

    .line 1505609
    :cond_0
    instance-of v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;

    if-eqz v0, :cond_1

    .line 1505610
    check-cast p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;

    goto :goto_0

    .line 1505611
    :cond_1
    new-instance v0, LX/9YO;

    invoke-direct {v0}, LX/9YO;-><init>()V

    .line 1505612
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/9YO;->a:Ljava/lang/String;

    .line 1505613
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1505614
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1505615
    iget-object v3, v0, LX/9YO;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1505616
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1505617
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 1505618
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1505619
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1505620
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1505621
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1505622
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1505623
    new-instance v3, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;

    invoke-direct {v3, v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;-><init>(LX/15i;)V

    .line 1505624
    move-object p0, v3

    .line 1505625
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1505600
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1505601
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1505602
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1505603
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1505604
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1505605
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1505597
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1505598
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1505599
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1505595
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;->e:Ljava/lang/String;

    .line 1505596
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1505592
    new-instance v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;

    invoke-direct {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;-><init>()V

    .line 1505593
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1505594
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1505591
    const v0, 0x2796e1ca

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1505590
    const v0, -0x726d476c

    return v0
.end method
