.class public final Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/9Y8;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6777276c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLPageActionType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$DescriptionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1505981
    const-class v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1505929
    const-class v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1505979
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1505980
    return-void
.end method

.method private a()Lcom/facebook/graphql/enums/GraphQLPageActionType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1505977
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->e:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->e:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 1505978
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->e:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    return-object v0
.end method

.method private j()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$DescriptionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1505975
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->f:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$DescriptionModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$DescriptionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$DescriptionModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->f:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$DescriptionModel;

    .line 1505976
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->f:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$DescriptionModel;

    return-object v0
.end method

.method private k()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1505973
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PageModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PageModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PageModel;

    .line 1505974
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PageModel;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1505971
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->h:Ljava/lang/String;

    .line 1505972
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1505982
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->i:Ljava/lang/String;

    .line 1505983
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method private n()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1505969
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->j:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->j:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;

    .line 1505970
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->j:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 1505953
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1505954
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->a()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1505955
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->j()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$DescriptionModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1505956
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->k()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PageModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1505957
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1505958
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1505959
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->n()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1505960
    const/4 v6, 0x6

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1505961
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 1505962
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1505963
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1505964
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1505965
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1505966
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1505967
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1505968
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1505935
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1505936
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->j()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$DescriptionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1505937
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->j()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$DescriptionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$DescriptionModel;

    .line 1505938
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->j()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$DescriptionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1505939
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;

    .line 1505940
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->f:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$DescriptionModel;

    .line 1505941
    :cond_0
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->k()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1505942
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->k()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PageModel;

    .line 1505943
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->k()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PageModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1505944
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;

    .line 1505945
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PageModel;

    .line 1505946
    :cond_1
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->n()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1505947
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->n()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;

    .line 1505948
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->n()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1505949
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;

    .line 1505950
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;->j:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;

    .line 1505951
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1505952
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1505932
    new-instance v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;

    invoke-direct {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel;-><init>()V

    .line 1505933
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1505934
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1505931
    const v0, 0x4d2b9c24    # 1.79946048E8f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1505930
    const v0, -0x53558405

    return v0
.end method
