.class public final Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageMessageActionDataModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageMessageActionDataModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1505365
    const-class v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageMessageActionDataModel;

    new-instance v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageMessageActionDataModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageMessageActionDataModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1505366
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1505346
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageMessageActionDataModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1505348
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1505349
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1505350
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1505351
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1505352
    if-eqz v2, :cond_0

    .line 1505353
    const-string v2, "action_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1505354
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1505355
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1505356
    if-eqz v2, :cond_1

    .line 1505357
    const-string p0, "description"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1505358
    invoke-static {v1, v2, p1}, LX/9Ye;->a(LX/15i;ILX/0nX;)V

    .line 1505359
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1505360
    if-eqz v2, :cond_2

    .line 1505361
    const-string p0, "page"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1505362
    invoke-static {v1, v2, p1, p2}, LX/9Yg;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1505363
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1505364
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1505347
    check-cast p1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageMessageActionDataModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageMessageActionDataModel$Serializer;->a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageMessageActionDataModel;LX/0nX;LX/0my;)V

    return-void
.end method
