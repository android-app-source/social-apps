.class public final Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7f182c7d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel$AddableActionsChannelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1503003
    const-class v0, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1503002
    const-class v0, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1503000
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1503001
    return-void
.end method

.method private j()Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel$AddableActionsChannelModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1502998
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel;->e:Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel$AddableActionsChannelModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel$AddableActionsChannelModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel$AddableActionsChannelModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel;->e:Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel$AddableActionsChannelModel;

    .line 1502999
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel;->e:Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel$AddableActionsChannelModel;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1502996
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel;->f:Ljava/lang/String;

    .line 1502997
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1502994
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel;->g:Ljava/util/List;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel;->g:Ljava/util/List;

    .line 1502995
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1502965
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1502966
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel;->j()Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel$AddableActionsChannelModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1502967
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1502968
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel;->l()LX/0Px;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v2

    .line 1502969
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1502970
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1502971
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1502972
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1502973
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1502974
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1502986
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1502987
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel;->j()Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel$AddableActionsChannelModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1502988
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel;->j()Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel$AddableActionsChannelModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel$AddableActionsChannelModel;

    .line 1502989
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel;->j()Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel$AddableActionsChannelModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1502990
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel;

    .line 1502991
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel;->e:Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel$AddableActionsChannelModel;

    .line 1502992
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1502993
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1502985
    new-instance v0, LX/9XU;

    invoke-direct {v0, p1}, LX/9XU;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1502984
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1502982
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1502983
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1502981
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1502978
    new-instance v0, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel;

    invoke-direct {v0}, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel;-><init>()V

    .line 1502979
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1502980
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel$AddableActionsChannelModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1502977
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel;->j()Lcom/facebook/pages/data/graphql/actionchannel/FetchAddableActionsQueryModels$AddableActionsDataModel$AddableActionsChannelModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1502976
    const v0, -0x3dc09d54

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1502975
    const v0, 0x25d6af

    return v0
.end method
