.class public final Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/9Y4;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xf5d67b3
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLPageActionType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$DescriptionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1504857
    const-class v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1504864
    const-class v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1504862
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1504863
    return-void
.end method

.method private a()Lcom/facebook/graphql/enums/GraphQLPageActionType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1504860
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel;->e:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel;->e:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 1504861
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel;->e:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    return-object v0
.end method

.method private j()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$DescriptionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1504858
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel;->f:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$DescriptionModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$DescriptionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$DescriptionModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel;->f:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$DescriptionModel;

    .line 1504859
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel;->f:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$DescriptionModel;

    return-object v0
.end method

.method private k()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$PageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1504865
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel;->g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$PageModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$PageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$PageModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel;->g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$PageModel;

    .line 1504866
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel;->g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$PageModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1504829
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1504830
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel;->a()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1504831
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel;->j()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$DescriptionModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1504832
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel;->k()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$PageModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1504833
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1504834
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1504835
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1504836
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1504837
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1504838
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1504839
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1504840
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel;->j()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$DescriptionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1504841
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel;->j()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$DescriptionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$DescriptionModel;

    .line 1504842
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel;->j()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$DescriptionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1504843
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel;

    .line 1504844
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel;->f:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$DescriptionModel;

    .line 1504845
    :cond_0
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel;->k()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1504846
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel;->k()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$PageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$PageModel;

    .line 1504847
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel;->k()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$PageModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1504848
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel;

    .line 1504849
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel;->g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel$PageModel;

    .line 1504850
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1504851
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1504852
    new-instance v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel;

    invoke-direct {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageCallActionDataModel;-><init>()V

    .line 1504853
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1504854
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1504855
    const v0, 0x7593b399

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1504856
    const v0, -0x7e7c75bd

    return v0
.end method
