.class public final Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1507445
    const-class v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel;

    new-instance v1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1507446
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1507447
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1507448
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1507449
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1507450
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1507451
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1507452
    if-eqz v2, :cond_0

    .line 1507453
    const-string p0, "actionBarChannel"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507454
    invoke-static {v1, v2, p1, p2}, LX/9Z8;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1507455
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1507456
    if-eqz v2, :cond_1

    .line 1507457
    const-string p0, "addableTabsChannel"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507458
    invoke-static {v1, v2, p1, p2}, LX/9Z9;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1507459
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1507460
    if-eqz v2, :cond_2

    .line 1507461
    const-string p0, "additionalActionsChannel"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507462
    invoke-static {v1, v2, p1, p2}, LX/9ZA;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1507463
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1507464
    if-eqz v2, :cond_3

    .line 1507465
    const-string p0, "hide_tabs"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507466
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1507467
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1507468
    if-eqz v2, :cond_4

    .line 1507469
    const-string p0, "primaryButtonsChannel"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507470
    invoke-static {v1, v2, p1, p2}, LX/9ZB;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1507471
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1507472
    if-eqz v2, :cond_5

    .line 1507473
    const-string p0, "profileTabNavigationChannel"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507474
    invoke-static {v1, v2, p1, p2}, LX/9ZC;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1507475
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1507476
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1507477
    check-cast p1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$Serializer;->a(Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel;LX/0nX;LX/0my;)V

    return-void
.end method
