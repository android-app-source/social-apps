.class public final Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderMetaboxDataModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderMetaboxDataModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1509058
    const-class v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderMetaboxDataModel;

    new-instance v1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderMetaboxDataModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderMetaboxDataModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1509059
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1509039
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderMetaboxDataModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1509040
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1509041
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x2

    .line 1509042
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1509043
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1509044
    if-eqz v2, :cond_0

    .line 1509045
    const-string v3, "address"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1509046
    invoke-static {v1, v2, p1}, LX/9ZP;->a(LX/15i;ILX/0nX;)V

    .line 1509047
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1509048
    if-eqz v2, :cond_1

    .line 1509049
    const-string v3, "place_open_status"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1509050
    invoke-static {v1, v2, p1}, LX/9ZQ;->a(LX/15i;ILX/0nX;)V

    .line 1509051
    :cond_1
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1509052
    if-eqz v2, :cond_2

    .line 1509053
    const-string v2, "place_open_status_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1509054
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1509055
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1509056
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1509057
    check-cast p1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderMetaboxDataModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderMetaboxDataModel$Serializer;->a(Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderMetaboxDataModel;LX/0nX;LX/0my;)V

    return-void
.end method
