.class public final Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1507844
    const-class v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    new-instance v1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1507845
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1507846
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;LX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 1507847
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1507848
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/16 p0, 0x28

    const/16 v7, 0x25

    const/16 v6, 0x1a

    const/16 v5, 0x19

    const/4 v4, 0x7

    .line 1507849
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1507850
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1507851
    if-eqz v2, :cond_0

    .line 1507852
    const-string v3, "actionBarChannel"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507853
    invoke-static {v1, v2, p1, p2}, LX/9Z8;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1507854
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1507855
    if-eqz v2, :cond_1

    .line 1507856
    const-string v3, "addableTabsChannel"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507857
    invoke-static {v1, v2, p1, p2}, LX/9Z9;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1507858
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1507859
    if-eqz v2, :cond_2

    .line 1507860
    const-string v3, "additionalActionsChannel"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507861
    invoke-static {v1, v2, p1, p2}, LX/9ZA;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1507862
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1507863
    if-eqz v2, :cond_3

    .line 1507864
    const-string v3, "address"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507865
    invoke-static {v1, v2, p1}, LX/9ZP;->a(LX/15i;ILX/0nX;)V

    .line 1507866
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1507867
    if-eqz v2, :cond_4

    .line 1507868
    const-string v3, "admin_display_preference"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507869
    invoke-static {v1, v2, p1}, LX/9ZE;->a(LX/15i;ILX/0nX;)V

    .line 1507870
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1507871
    if-eqz v2, :cond_5

    .line 1507872
    const-string v3, "admin_info"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507873
    invoke-static {v1, v2, p1}, LX/9ZD;->a(LX/15i;ILX/0nX;)V

    .line 1507874
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1507875
    if-eqz v2, :cond_6

    .line 1507876
    const-string v3, "attribution"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507877
    invoke-static {v1, v2, p1, p2}, LX/9ZI;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1507878
    :cond_6
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 1507879
    if-eqz v2, :cond_7

    .line 1507880
    const-string v2, "category_names"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507881
    invoke-virtual {v1, v0, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1507882
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1507883
    if-eqz v2, :cond_8

    .line 1507884
    const-string v3, "contextItemRows"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507885
    invoke-static {v1, v2, p1, p2}, LX/5O0;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1507886
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1507887
    if-eqz v2, :cond_9

    .line 1507888
    const-string v3, "cover_photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507889
    invoke-static {v1, v2, p1, p2}, LX/9ZU;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1507890
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1507891
    if-eqz v2, :cond_a

    .line 1507892
    const-string v3, "does_viewer_like"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507893
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1507894
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1507895
    if-eqz v2, :cond_b

    .line 1507896
    const-string v3, "expressed_as_place"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507897
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1507898
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1507899
    if-eqz v2, :cond_c

    .line 1507900
    const-string v3, "hide_tabs"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507901
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1507902
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1507903
    if-eqz v2, :cond_d

    .line 1507904
    const-string v3, "is_eligible_for_page_verification"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507905
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1507906
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1507907
    if-eqz v2, :cond_e

    .line 1507908
    const-string v3, "is_opted_in_sponsor_tags"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507909
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1507910
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1507911
    if-eqz v2, :cond_f

    .line 1507912
    const-string v3, "is_owned"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507913
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1507914
    :cond_f
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1507915
    if-eqz v2, :cond_10

    .line 1507916
    const-string v3, "is_published"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507917
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1507918
    :cond_10
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1507919
    if-eqz v2, :cond_11

    .line 1507920
    const-string v3, "is_service_page"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507921
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1507922
    :cond_11
    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1507923
    if-eqz v2, :cond_12

    .line 1507924
    const-string v3, "is_verified"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507925
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1507926
    :cond_12
    const/16 v2, 0x13

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1507927
    if-eqz v2, :cond_13

    .line 1507928
    const-string v3, "location"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507929
    invoke-static {v1, v2, p1}, LX/4aX;->a(LX/15i;ILX/0nX;)V

    .line 1507930
    :cond_13
    const/16 v2, 0x14

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1507931
    if-eqz v2, :cond_14

    .line 1507932
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507933
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1507934
    :cond_14
    const/16 v2, 0x15

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1507935
    if-eqz v2, :cond_15

    .line 1507936
    const-string v3, "overall_star_rating"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507937
    invoke-static {v1, v2, p1}, LX/9ZW;->a(LX/15i;ILX/0nX;)V

    .line 1507938
    :cond_15
    const/16 v2, 0x16

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1507939
    if-eqz v2, :cond_16

    .line 1507940
    const-string v3, "page_call_to_action"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507941
    invoke-static {v1, v2, p1, p2}, LX/8FF;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1507942
    :cond_16
    const/16 v2, 0x17

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1507943
    if-eqz v2, :cond_17

    .line 1507944
    const-string v3, "page_likers"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507945
    invoke-static {v1, v2, p1}, LX/9ZG;->a(LX/15i;ILX/0nX;)V

    .line 1507946
    :cond_17
    const/16 v2, 0x18

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1507947
    if-eqz v2, :cond_18

    .line 1507948
    const-string v3, "place_open_status"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507949
    invoke-static {v1, v2, p1}, LX/9ZQ;->a(LX/15i;ILX/0nX;)V

    .line 1507950
    :cond_18
    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v2

    .line 1507951
    if-eqz v2, :cond_19

    .line 1507952
    const-string v2, "place_open_status_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507953
    invoke-virtual {v1, v0, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1507954
    :cond_19
    invoke-virtual {v1, v0, v6}, LX/15i;->g(II)I

    move-result v2

    .line 1507955
    if-eqz v2, :cond_1a

    .line 1507956
    const-string v2, "place_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507957
    invoke-virtual {v1, v0, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1507958
    :cond_1a
    const/16 v2, 0x1b

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1507959
    if-eqz v2, :cond_1b

    .line 1507960
    const-string v3, "primaryButtonsChannel"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507961
    invoke-static {v1, v2, p1, p2}, LX/9ZB;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1507962
    :cond_1b
    const/16 v2, 0x1c

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1507963
    if-eqz v2, :cond_1c

    .line 1507964
    const-string v3, "profilePictureAsCover"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507965
    invoke-static {v1, v2, p1}, LX/9ZV;->a(LX/15i;ILX/0nX;)V

    .line 1507966
    :cond_1c
    const/16 v2, 0x1d

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1507967
    if-eqz v2, :cond_1d

    .line 1507968
    const-string v3, "profileTabNavigationChannel"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507969
    invoke-static {v1, v2, p1, p2}, LX/9ZC;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1507970
    :cond_1d
    const/16 v2, 0x1e

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1507971
    if-eqz v2, :cond_1e

    .line 1507972
    const-string v3, "profile_photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507973
    invoke-static {v1, v2, p1, p2}, LX/9ZO;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1507974
    :cond_1e
    const/16 v2, 0x1f

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1507975
    if-eqz v2, :cond_1f

    .line 1507976
    const-string v3, "profile_picture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507977
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1507978
    :cond_1f
    const/16 v2, 0x20

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1507979
    if-eqz v2, :cond_20

    .line 1507980
    const-string v3, "profile_picture_is_silhouette"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507981
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1507982
    :cond_20
    const/16 v2, 0x21

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1507983
    if-eqz v2, :cond_21

    .line 1507984
    const-string v3, "profile_video"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507985
    invoke-static {v1, v2, p1, p2}, LX/5w2;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1507986
    :cond_21
    const/16 v2, 0x22

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1507987
    if-eqz v2, :cond_22

    .line 1507988
    const-string v3, "recent_posters"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507989
    invoke-static {v1, v2, p1}, LX/9ZF;->a(LX/15i;ILX/0nX;)V

    .line 1507990
    :cond_22
    const/16 v2, 0x23

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1507991
    if-eqz v2, :cond_23

    .line 1507992
    const-string v3, "redirection_info"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507993
    invoke-static {v1, v2, p1, p2}, LX/9ZK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1507994
    :cond_23
    const/16 v2, 0x24

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1507995
    if-eqz v2, :cond_24

    .line 1507996
    const-string v3, "should_show_username"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507997
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1507998
    :cond_24
    invoke-virtual {v1, v0, v7}, LX/15i;->g(II)I

    move-result v2

    .line 1507999
    if-eqz v2, :cond_25

    .line 1508000
    const-string v2, "super_category_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1508001
    invoke-virtual {v1, v0, v7}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1508002
    :cond_25
    const/16 v2, 0x26

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1508003
    if-eqz v2, :cond_26

    .line 1508004
    const-string v3, "timeline_pinned_unit"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1508005
    invoke-static {v1, v2, p1}, LX/9ZL;->a(LX/15i;ILX/0nX;)V

    .line 1508006
    :cond_26
    const/16 v2, 0x27

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1508007
    if-eqz v2, :cond_27

    .line 1508008
    const-string v3, "username"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1508009
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1508010
    :cond_27
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1508011
    if-eqz v2, :cond_28

    .line 1508012
    const-string v2, "verification_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1508013
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1508014
    :cond_28
    const/16 v2, 0x29

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1508015
    if-eqz v2, :cond_29

    .line 1508016
    const-string v2, "viewer_profile_permissions"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1508017
    const/16 v2, 0x29

    invoke-virtual {v1, v0, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1508018
    :cond_29
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1508019
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1508020
    check-cast p1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel$Serializer;->a(Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;LX/0nX;LX/0my;)V

    return-void
.end method
