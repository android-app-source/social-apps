.class public final Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAdminSocialContextDataModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAdminSocialContextDataModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1507686
    const-class v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAdminSocialContextDataModel;

    new-instance v1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAdminSocialContextDataModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAdminSocialContextDataModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1507687
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1507685
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAdminSocialContextDataModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1507671
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1507672
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1507673
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1507674
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1507675
    if-eqz v2, :cond_0

    .line 1507676
    const-string p0, "admin_info"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507677
    invoke-static {v1, v2, p1}, LX/9ZD;->a(LX/15i;ILX/0nX;)V

    .line 1507678
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1507679
    if-eqz v2, :cond_1

    .line 1507680
    const-string p0, "page_likers"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1507681
    invoke-static {v1, v2, p1}, LX/9ZG;->a(LX/15i;ILX/0nX;)V

    .line 1507682
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1507683
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1507684
    check-cast p1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAdminSocialContextDataModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAdminSocialContextDataModel$Serializer;->a(Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAdminSocialContextDataModel;LX/0nX;LX/0my;)V

    return-void
.end method
