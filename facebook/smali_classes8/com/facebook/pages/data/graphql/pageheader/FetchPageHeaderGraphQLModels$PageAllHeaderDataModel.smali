.class public final Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;
.implements LX/9Ys;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7e2e4733
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel$Serializer;
.end annotation


# instance fields
.field private A:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private B:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private C:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private D:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private E:Lcom/facebook/graphql/enums/GraphQLPlaceType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private F:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$PrimaryButtonsChannelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private G:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private H:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ProfileTabNavigationChannelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private I:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private J:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private K:Z

.field private L:Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileHeaderAssociatedVideoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private M:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private N:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$RedirectionInfoModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private O:Z

.field private P:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private Q:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$TimelinePinnedUnitModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private R:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private S:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private T:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ActionBarChannelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AddableTabsChannelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AdditionalActionsChannelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Z

.field private x:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private z:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1508354
    const-class v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1508302
    const-class v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1508303
    const/16 v0, 0x2a

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1508304
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1508305
    const/16 v0, 0x2a

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1508306
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1508307
    return-void
.end method

.method private Q()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAdminDisplayPreference"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1508308
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1508309
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->i:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private R()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1508310
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->m:Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->m:Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;

    .line 1508311
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->m:Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;

    return-object v0
.end method

.method private S()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1508312
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->x:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->x:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 1508313
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->x:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    return-object v0
.end method

.method private T()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1508314
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->A:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->A:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    .line 1508315
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->A:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    return-object v0
.end method

.method private U()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPageLikers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1508316
    const/4 v0, 0x2

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1508317
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->B:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private V()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getRecentPosters"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1508318
    const/4 v0, 0x4

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1508319
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->M:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1508320
    iput-boolean p1, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->o:Z

    .line 1508321
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1508322
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1508323
    if-eqz v0, :cond_0

    .line 1508324
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1508325
    :cond_0
    return-void
.end method


# virtual methods
.method public final A()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1508326
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->D:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->D:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    .line 1508327
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->D:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    return-object v0
.end method

.method public final B()Lcom/facebook/graphql/enums/GraphQLPlaceType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1508328
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->E:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPlaceType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPlaceType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->E:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 1508329
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->E:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    return-object v0
.end method

.method public final C()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$PrimaryButtonsChannelModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1508221
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->F:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$PrimaryButtonsChannelModel;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$PrimaryButtonsChannelModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$PrimaryButtonsChannelModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->F:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$PrimaryButtonsChannelModel;

    .line 1508222
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->F:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$PrimaryButtonsChannelModel;

    return-object v0
.end method

.method public final D()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfilePictureAsCover"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1508330
    const/4 v0, 0x3

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1508331
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->G:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final E()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ProfileTabNavigationChannelModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1508332
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->H:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ProfileTabNavigationChannelModel;

    const/16 v1, 0x1d

    const-class v2, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ProfileTabNavigationChannelModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ProfileTabNavigationChannelModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->H:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ProfileTabNavigationChannelModel;

    .line 1508333
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->H:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ProfileTabNavigationChannelModel;

    return-object v0
.end method

.method public final F()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1508334
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->I:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;

    const/16 v1, 0x1e

    const-class v2, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->I:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;

    .line 1508335
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->I:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;

    return-object v0
.end method

.method public final G()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1508336
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->J:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0x1f

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->J:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1508337
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->J:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method public final H()Z
    .locals 2

    .prologue
    .line 1508338
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1508339
    iget-boolean v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->K:Z

    return v0
.end method

.method public final I()Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileHeaderAssociatedVideoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1508340
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->L:Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileHeaderAssociatedVideoModel;

    const/16 v1, 0x21

    const-class v2, Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileHeaderAssociatedVideoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileHeaderAssociatedVideoModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->L:Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileHeaderAssociatedVideoModel;

    .line 1508341
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->L:Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileHeaderAssociatedVideoModel;

    return-object v0
.end method

.method public final J()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$RedirectionInfoModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1508342
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->N:Ljava/util/List;

    const/16 v1, 0x23

    const-class v2, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$RedirectionInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->N:Ljava/util/List;

    .line 1508343
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->N:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final K()Z
    .locals 1

    .prologue
    const/4 v0, 0x4

    .line 1508344
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1508345
    iget-boolean v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->O:Z

    return v0
.end method

.method public final L()Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1508346
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->P:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const/16 v1, 0x25

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->P:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 1508347
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->P:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    return-object v0
.end method

.method public final M()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$TimelinePinnedUnitModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1508348
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->Q:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$TimelinePinnedUnitModel;

    const/16 v1, 0x26

    const-class v2, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$TimelinePinnedUnitModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$TimelinePinnedUnitModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->Q:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$TimelinePinnedUnitModel;

    .line 1508349
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->Q:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$TimelinePinnedUnitModel;

    return-object v0
.end method

.method public final N()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1508350
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->R:Ljava/lang/String;

    const/16 v1, 0x27

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->R:Ljava/lang/String;

    .line 1508351
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->R:Ljava/lang/String;

    return-object v0
.end method

.method public final O()Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1508352
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->S:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    const/16 v1, 0x28

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->S:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 1508353
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->S:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    return-object v0
.end method

.method public final P()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1508223
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->T:Ljava/util/List;

    const/16 v1, 0x29

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->T:Ljava/util/List;

    .line 1508224
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->T:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 35

    .prologue
    .line 1508225
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1508226
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->j()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ActionBarChannelModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1508227
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->k()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AddableTabsChannelModel;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1508228
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->l()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AdditionalActionsChannelModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1508229
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->m()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    const v8, 0x744b6a79

    invoke-static {v7, v6, v8}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1508230
    invoke-direct/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->Q()LX/1vs;

    move-result-object v7

    iget-object v8, v7, LX/1vs;->a:LX/15i;

    iget v7, v7, LX/1vs;->b:I

    const v9, -0x1ba8526

    invoke-static {v8, v7, v9}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1508231
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->n()LX/1vs;

    move-result-object v8

    iget-object v9, v8, LX/1vs;->a:LX/15i;

    iget v8, v8, LX/1vs;->b:I

    const v10, 0x8c3f43

    invoke-static {v9, v8, v10}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1508232
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->o()LX/2uF;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v9, v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v9

    .line 1508233
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->p()LX/0Px;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/util/List;)I

    move-result v10

    .line 1508234
    invoke-direct/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->R()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1508235
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->q()LX/1vs;

    move-result-object v12

    iget-object v13, v12, LX/1vs;->a:LX/15i;

    iget v12, v12, LX/1vs;->b:I

    const v14, 0x5870cdbb

    invoke-static {v13, v12, v14}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1508236
    invoke-direct/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->S()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 1508237
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->x()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 1508238
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->y()LX/1vs;

    move-result-object v15

    iget-object v0, v15, LX/1vs;->a:LX/15i;

    move-object/from16 v16, v0

    iget v15, v15, LX/1vs;->b:I

    const v17, 0x24160ca9

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v0, v15, v1}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 1508239
    invoke-direct/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->T()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 1508240
    invoke-direct/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->U()LX/1vs;

    move-result-object v17

    move-object/from16 v0, v17

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    iget v0, v0, LX/1vs;->b:I

    move/from16 v17, v0

    const v19, -0x41e35a26

    move-object/from16 v0, v18

    move/from16 v1, v17

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 1508241
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->z()LX/1vs;

    move-result-object v18

    move-object/from16 v0, v18

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    iget v0, v0, LX/1vs;->b:I

    move/from16 v18, v0

    const v20, -0x5dd27faa

    move-object/from16 v0, v19

    move/from16 v1, v18

    move/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 1508242
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->A()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v19

    .line 1508243
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->B()Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v20

    .line 1508244
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->C()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$PrimaryButtonsChannelModel;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 1508245
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->D()LX/1vs;

    move-result-object v22

    move-object/from16 v0, v22

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v23, v0

    move-object/from16 v0, v22

    iget v0, v0, LX/1vs;->b:I

    move/from16 v22, v0

    const v24, 0xb4e594d

    move-object/from16 v0, v23

    move/from16 v1, v22

    move/from16 v2, v24

    invoke-static {v0, v1, v2}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 1508246
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->E()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ProfileTabNavigationChannelModel;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 1508247
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->F()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 1508248
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->G()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 1508249
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->I()Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileHeaderAssociatedVideoModel;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 1508250
    invoke-direct/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->V()LX/1vs;

    move-result-object v27

    move-object/from16 v0, v27

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v28, v0

    move-object/from16 v0, v27

    iget v0, v0, LX/1vs;->b:I

    move/from16 v27, v0

    const v29, -0xb0ab93f

    move-object/from16 v0, v28

    move/from16 v1, v27

    move/from16 v2, v29

    invoke-static {v0, v1, v2}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v27

    .line 1508251
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->J()LX/0Px;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v28

    .line 1508252
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->L()Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v29

    .line 1508253
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->M()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$TimelinePinnedUnitModel;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v30

    .line 1508254
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->N()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v31

    .line 1508255
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->O()Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v32

    .line 1508256
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->P()LX/0Px;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v33

    .line 1508257
    const/16 v34, 0x2a

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1508258
    const/16 v34, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1508259
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 1508260
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 1508261
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1508262
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1508263
    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 1508264
    const/4 v3, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 1508265
    const/4 v3, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 1508266
    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 1508267
    const/16 v3, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 1508268
    const/16 v3, 0xa

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->o:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 1508269
    const/16 v3, 0xb

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->p:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 1508270
    const/16 v3, 0xc

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->q:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 1508271
    const/16 v3, 0xd

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->r:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 1508272
    const/16 v3, 0xe

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->s:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 1508273
    const/16 v3, 0xf

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->t:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 1508274
    const/16 v3, 0x10

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->u:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 1508275
    const/16 v3, 0x11

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->v:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 1508276
    const/16 v3, 0x12

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->w:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 1508277
    const/16 v3, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 1508278
    const/16 v3, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 1508279
    const/16 v3, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 1508280
    const/16 v3, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1508281
    const/16 v3, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1508282
    const/16 v3, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1508283
    const/16 v3, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1508284
    const/16 v3, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1508285
    const/16 v3, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1508286
    const/16 v3, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1508287
    const/16 v3, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1508288
    const/16 v3, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1508289
    const/16 v3, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1508290
    const/16 v3, 0x20

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->K:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 1508291
    const/16 v3, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1508292
    const/16 v3, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1508293
    const/16 v3, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1508294
    const/16 v3, 0x24

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->O:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 1508295
    const/16 v3, 0x25

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1508296
    const/16 v3, 0x26

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1508297
    const/16 v3, 0x27

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1508298
    const/16 v3, 0x28

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1508299
    const/16 v3, 0x29

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1508300
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1508301
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    return v3
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1508023
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1508024
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->j()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ActionBarChannelModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1508025
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->j()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ActionBarChannelModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ActionBarChannelModel;

    .line 1508026
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->j()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ActionBarChannelModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1508027
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    .line 1508028
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ActionBarChannelModel;

    .line 1508029
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->k()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AddableTabsChannelModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1508030
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->k()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AddableTabsChannelModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AddableTabsChannelModel;

    .line 1508031
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->k()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AddableTabsChannelModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1508032
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    .line 1508033
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->f:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AddableTabsChannelModel;

    .line 1508034
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->l()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AdditionalActionsChannelModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1508035
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->l()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AdditionalActionsChannelModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AdditionalActionsChannelModel;

    .line 1508036
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->l()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AdditionalActionsChannelModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1508037
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    .line 1508038
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->g:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AdditionalActionsChannelModel;

    .line 1508039
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_3

    .line 1508040
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x744b6a79

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1508041
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1508042
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    .line 1508043
    iput v3, v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->h:I

    move-object v1, v0

    .line 1508044
    :cond_3
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->Q()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_4

    .line 1508045
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->Q()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x1ba8526

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1508046
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->Q()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1508047
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    .line 1508048
    iput v3, v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->i:I

    move-object v1, v0

    .line 1508049
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->n()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_5

    .line 1508050
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->n()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x8c3f43

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1508051
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->n()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1508052
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    .line 1508053
    iput v3, v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->j:I

    move-object v1, v0

    .line 1508054
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->o()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1508055
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->o()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v2

    .line 1508056
    if-eqz v2, :cond_6

    .line 1508057
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    .line 1508058
    invoke-virtual {v2}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->k:LX/3Sb;

    move-object v1, v0

    .line 1508059
    :cond_6
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->R()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 1508060
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->R()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;

    .line 1508061
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->R()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 1508062
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    .line 1508063
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->m:Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionWithPageInfoFragmentModel;

    .line 1508064
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->q()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_8

    .line 1508065
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->q()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x5870cdbb

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 1508066
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->q()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1508067
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    .line 1508068
    iput v3, v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->n:I

    move-object v1, v0

    .line 1508069
    :cond_8
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->S()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 1508070
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->S()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 1508071
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->S()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 1508072
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    .line 1508073
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->x:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 1508074
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->y()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_a

    .line 1508075
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->y()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x24160ca9

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 1508076
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->y()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_a

    .line 1508077
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    .line 1508078
    iput v3, v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->z:I

    move-object v1, v0

    .line 1508079
    :cond_a
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->T()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 1508080
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->T()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    .line 1508081
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->T()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 1508082
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    .line 1508083
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->A:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    .line 1508084
    :cond_b
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->U()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_c

    .line 1508085
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->U()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x41e35a26

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    .line 1508086
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->U()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_c

    .line 1508087
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    .line 1508088
    iput v3, v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->B:I

    move-object v1, v0

    .line 1508089
    :cond_c
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->z()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_d

    .line 1508090
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->z()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x5dd27faa

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_6
    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_6

    .line 1508091
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->z()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_d

    .line 1508092
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    .line 1508093
    iput v3, v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->C:I

    move-object v1, v0

    .line 1508094
    :cond_d
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->C()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$PrimaryButtonsChannelModel;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 1508095
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->C()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$PrimaryButtonsChannelModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$PrimaryButtonsChannelModel;

    .line 1508096
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->C()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$PrimaryButtonsChannelModel;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 1508097
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    .line 1508098
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->F:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$PrimaryButtonsChannelModel;

    .line 1508099
    :cond_e
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->D()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_f

    .line 1508100
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->D()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0xb4e594d

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_7
    monitor-exit v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_7

    .line 1508101
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->D()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_f

    .line 1508102
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    .line 1508103
    iput v3, v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->G:I

    move-object v1, v0

    .line 1508104
    :cond_f
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->E()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ProfileTabNavigationChannelModel;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 1508105
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->E()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ProfileTabNavigationChannelModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ProfileTabNavigationChannelModel;

    .line 1508106
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->E()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ProfileTabNavigationChannelModel;

    move-result-object v2

    if-eq v2, v0, :cond_10

    .line 1508107
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    .line 1508108
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->H:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ProfileTabNavigationChannelModel;

    .line 1508109
    :cond_10
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->F()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 1508110
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->F()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;

    .line 1508111
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->F()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;

    move-result-object v2

    if-eq v2, v0, :cond_11

    .line 1508112
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    .line 1508113
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->I:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;

    .line 1508114
    :cond_11
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->G()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 1508115
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->G()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1508116
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->G()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_12

    .line 1508117
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    .line 1508118
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->J:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1508119
    :cond_12
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->I()Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileHeaderAssociatedVideoModel;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 1508120
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->I()Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileHeaderAssociatedVideoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileHeaderAssociatedVideoModel;

    .line 1508121
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->I()Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileHeaderAssociatedVideoModel;

    move-result-object v2

    if-eq v2, v0, :cond_13

    .line 1508122
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    .line 1508123
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->L:Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileHeaderAssociatedVideoModel;

    .line 1508124
    :cond_13
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->V()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_14

    .line 1508125
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->V()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0xb0ab93f

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_8
    monitor-exit v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_8

    .line 1508126
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->V()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_14

    .line 1508127
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    .line 1508128
    iput v3, v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->M:I

    move-object v1, v0

    .line 1508129
    :cond_14
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->J()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 1508130
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->J()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1508131
    if-eqz v2, :cond_15

    .line 1508132
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    .line 1508133
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->N:Ljava/util/List;

    move-object v1, v0

    .line 1508134
    :cond_15
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->M()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$TimelinePinnedUnitModel;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 1508135
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->M()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$TimelinePinnedUnitModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$TimelinePinnedUnitModel;

    .line 1508136
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->M()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$TimelinePinnedUnitModel;

    move-result-object v2

    if-eq v2, v0, :cond_16

    .line 1508137
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    .line 1508138
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->Q:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$TimelinePinnedUnitModel;

    .line 1508139
    :cond_16
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1508140
    if-nez v1, :cond_17

    :goto_0
    return-object p0

    .line 1508141
    :catchall_0
    move-exception v0

    :try_start_9
    monitor-exit v4
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    throw v0

    .line 1508142
    :catchall_1
    move-exception v0

    :try_start_a
    monitor-exit v4
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    throw v0

    .line 1508143
    :catchall_2
    move-exception v0

    :try_start_b
    monitor-exit v4
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    throw v0

    .line 1508144
    :catchall_3
    move-exception v0

    :try_start_c
    monitor-exit v4
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    throw v0

    .line 1508145
    :catchall_4
    move-exception v0

    :try_start_d
    monitor-exit v4
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    throw v0

    .line 1508146
    :catchall_5
    move-exception v0

    :try_start_e
    monitor-exit v4
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_5

    throw v0

    .line 1508147
    :catchall_6
    move-exception v0

    :try_start_f
    monitor-exit v4
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_6

    throw v0

    .line 1508148
    :catchall_7
    move-exception v0

    :try_start_10
    monitor-exit v4
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_7

    throw v0

    .line 1508149
    :catchall_8
    move-exception v0

    :try_start_11
    monitor-exit v4
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_8

    throw v0

    :cond_17
    move-object p0, v1

    .line 1508150
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1508194
    new-instance v0, LX/9Yy;

    invoke-direct {v0, p1}, LX/9Yy;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final synthetic a()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1508193
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->T()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1508171
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1508172
    const/4 v0, 0x3

    const v1, 0x744b6a79

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->h:I

    .line 1508173
    const/4 v0, 0x4

    const v1, -0x1ba8526

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->i:I

    .line 1508174
    const/4 v0, 0x5

    const v1, 0x8c3f43

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->j:I

    .line 1508175
    const/16 v0, 0x9

    const v1, 0x5870cdbb

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->n:I

    .line 1508176
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->o:Z

    .line 1508177
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->p:Z

    .line 1508178
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->q:Z

    .line 1508179
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->r:Z

    .line 1508180
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->s:Z

    .line 1508181
    const/16 v0, 0xf

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->t:Z

    .line 1508182
    const/16 v0, 0x10

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->u:Z

    .line 1508183
    const/16 v0, 0x11

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->v:Z

    .line 1508184
    const/16 v0, 0x12

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->w:Z

    .line 1508185
    const/16 v0, 0x15

    const v1, 0x24160ca9

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->z:I

    .line 1508186
    const/16 v0, 0x17

    const v1, -0x41e35a26

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->B:I

    .line 1508187
    const/16 v0, 0x18

    const v1, -0x5dd27faa

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->C:I

    .line 1508188
    const/16 v0, 0x1c

    const v1, 0xb4e594d

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->G:I

    .line 1508189
    const/16 v0, 0x20

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->K:Z

    .line 1508190
    const/16 v0, 0x22

    const v1, -0xb0ab93f

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->M:I

    .line 1508191
    const/16 v0, 0x24

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->O:Z

    .line 1508192
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1508165
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1508166
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->r()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1508167
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1508168
    const/16 v0, 0xa

    iput v0, p2, LX/18L;->c:I

    .line 1508169
    :goto_0
    return-void

    .line 1508170
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1508162
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1508163
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->a(Z)V

    .line 1508164
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1508159
    new-instance v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    invoke-direct {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;-><init>()V

    .line 1508160
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1508161
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1508158
    const v0, 0x38b6a06

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1508157
    const v0, 0x25d6af

    return v0
.end method

.method public final j()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ActionBarChannelModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1508155
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ActionBarChannelModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ActionBarChannelModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ActionBarChannelModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ActionBarChannelModel;

    .line 1508156
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$ActionBarChannelModel;

    return-object v0
.end method

.method public final k()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AddableTabsChannelModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1508153
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->f:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AddableTabsChannelModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AddableTabsChannelModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AddableTabsChannelModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->f:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AddableTabsChannelModel;

    .line 1508154
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->f:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AddableTabsChannelModel;

    return-object v0
.end method

.method public final l()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AdditionalActionsChannelModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1508151
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->g:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AdditionalActionsChannelModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AdditionalActionsChannelModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AdditionalActionsChannelModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->g:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AdditionalActionsChannelModel;

    .line 1508152
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->g:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AdditionalActionsChannelModel;

    return-object v0
.end method

.method public final m()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAddress"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1508021
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1508022
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final n()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAdminInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1508195
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1508196
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->j:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final o()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAttribution"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1508197
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->k:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x6

    const v4, -0x337a4ba7    # -7.0099656E7f

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->k:LX/3Sb;

    .line 1508198
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->k:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method public final p()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1508199
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->l:Ljava/util/List;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->l:Ljava/util/List;

    .line 1508200
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->l:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final q()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCoverPhoto"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 1508201
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1508202
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->n:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final r()Z
    .locals 2

    .prologue
    .line 1508203
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1508204
    iget-boolean v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->o:Z

    return v0
.end method

.method public final s()Z
    .locals 2

    .prologue
    .line 1508205
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1508206
    iget-boolean v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->p:Z

    return v0
.end method

.method public final t()Z
    .locals 2

    .prologue
    .line 1508207
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1508208
    iget-boolean v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->q:Z

    return v0
.end method

.method public final u()Z
    .locals 2

    .prologue
    .line 1508209
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1508210
    iget-boolean v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->r:Z

    return v0
.end method

.method public final v()Z
    .locals 2

    .prologue
    .line 1508211
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1508212
    iget-boolean v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->t:Z

    return v0
.end method

.method public final w()Z
    .locals 2

    .prologue
    .line 1508213
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1508214
    iget-boolean v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->u:Z

    return v0
.end method

.method public final x()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1508215
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->y:Ljava/lang/String;

    const/16 v1, 0x14

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->y:Ljava/lang/String;

    .line 1508216
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->y:Ljava/lang/String;

    return-object v0
.end method

.method public final y()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOverallStarRating"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1508217
    const/4 v0, 0x2

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1508218
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->z:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final z()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPlaceOpenStatus"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1508219
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1508220
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->C:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
