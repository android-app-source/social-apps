.class public final Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1507099
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1507100
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1507172
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1507173
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 10

    .prologue
    const/4 v6, 0x3

    const-wide/16 v4, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 1507101
    if-nez p1, :cond_0

    .line 1507102
    :goto_0
    return v1

    .line 1507103
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1507104
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1507105
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->b(II)Z

    move-result v0

    .line 1507106
    invoke-virtual {p0, p1, v8}, LX/15i;->b(II)Z

    move-result v2

    .line 1507107
    invoke-virtual {p0, p1, v9}, LX/15i;->b(II)Z

    move-result v3

    .line 1507108
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1507109
    invoke-virtual {p3, v1, v0}, LX/186;->a(IZ)V

    .line 1507110
    invoke-virtual {p3, v8, v2}, LX/186;->a(IZ)V

    .line 1507111
    invoke-virtual {p3, v9, v3}, LX/186;->a(IZ)V

    .line 1507112
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 1507113
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->b(II)Z

    move-result v0

    .line 1507114
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 1507115
    invoke-virtual {p3, v1, v0}, LX/186;->a(IZ)V

    .line 1507116
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 1507117
    :sswitch_2
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 1507118
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 1507119
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 1507120
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 1507121
    :sswitch_3
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 1507122
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 1507123
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 1507124
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 1507125
    :sswitch_4
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1507126
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1507127
    invoke-virtual {p0, p1, v8}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLAttributionSource;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    move-result-object v2

    .line 1507128
    invoke-virtual {p3, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1507129
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    .line 1507130
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1507131
    invoke-virtual {p3, v8, v2}, LX/186;->b(II)V

    .line 1507132
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 1507133
    :sswitch_5
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1507134
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1507135
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 1507136
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1507137
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 1507138
    :sswitch_6
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1507139
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1507140
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 1507141
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1507142
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 1507143
    :sswitch_7
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1507144
    const v2, -0x1c3f3beb

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 1507145
    const-class v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {p0, p1, v8, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;

    .line 1507146
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1507147
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    .line 1507148
    invoke-virtual {p3, v1, v2}, LX/186;->b(II)V

    .line 1507149
    invoke-virtual {p3, v8, v0}, LX/186;->b(II)V

    .line 1507150
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 1507151
    :sswitch_8
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1507152
    invoke-virtual {p0, p1, v8, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 1507153
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    move-object v0, p3

    .line 1507154
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    move-object v0, p3

    move v1, v8

    move-wide v2, v6

    .line 1507155
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1507156
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 1507157
    :sswitch_9
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 1507158
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1507159
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1507160
    invoke-virtual {p0, p1, v9, v1}, LX/15i;->a(III)I

    move-result v3

    .line 1507161
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1507162
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 1507163
    invoke-virtual {p3, v8, v2}, LX/186;->b(II)V

    .line 1507164
    invoke-virtual {p3, v9, v3, v1}, LX/186;->a(III)V

    .line 1507165
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 1507166
    :sswitch_a
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 1507167
    invoke-virtual {p0, p1, v8, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1507168
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    .line 1507169
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    move-object v0, p3

    move v1, v8

    .line 1507170
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1507171
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x5dd27faa -> :sswitch_6
        -0x41e35a26 -> :sswitch_3
        -0x337a4ba7 -> :sswitch_4
        -0x1c3f3beb -> :sswitch_8
        -0xb0ab93f -> :sswitch_2
        -0x1ba8526 -> :sswitch_1
        0x8c3f43 -> :sswitch_0
        0xb4e594d -> :sswitch_9
        0x24160ca9 -> :sswitch_a
        0x5870cdbb -> :sswitch_7
        0x744b6a79 -> :sswitch_5
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1507037
    if-nez p0, :cond_0

    move v0, v1

    .line 1507038
    :goto_0
    return v0

    .line 1507039
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 1507040
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1507041
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1507042
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 1507043
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1507044
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1507045
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 1507092
    const/4 v7, 0x0

    .line 1507093
    const/4 v1, 0x0

    .line 1507094
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 1507095
    invoke-static {v2, v3, v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1507096
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 1507097
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1507098
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1507091
    new-instance v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1507086
    if-eqz p0, :cond_0

    .line 1507087
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1507088
    if-eq v0, p0, :cond_0

    .line 1507089
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1507090
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1507079
    sparse-switch p2, :sswitch_data_0

    .line 1507080
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1507081
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1507082
    const v1, -0x1c3f3beb

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1507083
    const/4 v0, 0x1

    const-class v1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;

    .line 1507084
    invoke-static {v0, p3}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 1507085
    :sswitch_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x5dd27faa -> :sswitch_1
        -0x41e35a26 -> :sswitch_1
        -0x337a4ba7 -> :sswitch_1
        -0x1c3f3beb -> :sswitch_1
        -0xb0ab93f -> :sswitch_1
        -0x1ba8526 -> :sswitch_1
        0x8c3f43 -> :sswitch_1
        0xb4e594d -> :sswitch_1
        0x24160ca9 -> :sswitch_1
        0x5870cdbb -> :sswitch_0
        0x744b6a79 -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1507073
    if-eqz p1, :cond_0

    .line 1507074
    invoke-static {p0, p1, p2}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 1507075
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;

    .line 1507076
    if-eq v0, v1, :cond_0

    .line 1507077
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1507078
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1507072
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1507174
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1507175
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1507032
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1507033
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1507034
    :cond_0
    iput-object p1, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1507035
    iput p2, p0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->b:I

    .line 1507036
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1507046
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1507047
    new-instance v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1507048
    iget v0, p0, LX/1vt;->c:I

    .line 1507049
    move v0, v0

    .line 1507050
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1507051
    iget v0, p0, LX/1vt;->c:I

    .line 1507052
    move v0, v0

    .line 1507053
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1507054
    iget v0, p0, LX/1vt;->b:I

    .line 1507055
    move v0, v0

    .line 1507056
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1507057
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1507058
    move-object v0, v0

    .line 1507059
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1507060
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1507061
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1507062
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1507063
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1507064
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1507065
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1507066
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1507067
    invoke-static {v3, v9, v2}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1507068
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1507069
    iget v0, p0, LX/1vt;->c:I

    .line 1507070
    move v0, v0

    .line 1507071
    return v0
.end method
