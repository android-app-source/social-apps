.class public final Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1509255
    const-class v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel;

    new-instance v1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1509256
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1509279
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1509258
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1509259
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1509260
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1509261
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1509262
    if-eqz v2, :cond_0

    .line 1509263
    const-string p0, "cover_photo"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1509264
    invoke-static {v1, v2, p1, p2}, LX/9ZU;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1509265
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1509266
    if-eqz v2, :cond_1

    .line 1509267
    const-string p0, "profilePictureAsCover"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1509268
    invoke-static {v1, v2, p1}, LX/9ZV;->a(LX/15i;ILX/0nX;)V

    .line 1509269
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1509270
    if-eqz v2, :cond_2

    .line 1509271
    const-string p0, "profile_photo"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1509272
    invoke-static {v1, v2, p1, p2}, LX/9ZO;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1509273
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1509274
    if-eqz v2, :cond_3

    .line 1509275
    const-string p0, "profile_picture_is_silhouette"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1509276
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1509277
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1509278
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1509257
    check-cast p1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$Serializer;->a(Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel;LX/0nX;LX/0my;)V

    return-void
.end method
