.class public final Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1508545
    const-class v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel;

    new-instance v1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1508546
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1508548
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel;LX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 1508549
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1508550
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/16 p0, 0x17

    const/16 v7, 0x16

    const/16 v6, 0x13

    const/16 v5, 0xc

    const/4 v4, 0x1

    .line 1508551
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1508552
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1508553
    if-eqz v2, :cond_0

    .line 1508554
    const-string v3, "attribution"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1508555
    invoke-static {v1, v2, p1, p2}, LX/9ZI;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1508556
    :cond_0
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 1508557
    if-eqz v2, :cond_1

    .line 1508558
    const-string v2, "category_names"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1508559
    invoke-virtual {v1, v0, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1508560
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1508561
    if-eqz v2, :cond_2

    .line 1508562
    const-string v3, "cover_photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1508563
    invoke-static {v1, v2, p1, p2}, LX/9ZU;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1508564
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1508565
    if-eqz v2, :cond_3

    .line 1508566
    const-string v3, "does_viewer_like"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1508567
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1508568
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1508569
    if-eqz v2, :cond_4

    .line 1508570
    const-string v3, "expressed_as_place"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1508571
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1508572
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1508573
    if-eqz v2, :cond_5

    .line 1508574
    const-string v3, "is_eligible_for_page_verification"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1508575
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1508576
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1508577
    if-eqz v2, :cond_6

    .line 1508578
    const-string v3, "is_opted_in_sponsor_tags"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1508579
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1508580
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1508581
    if-eqz v2, :cond_7

    .line 1508582
    const-string v3, "is_owned"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1508583
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1508584
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1508585
    if-eqz v2, :cond_8

    .line 1508586
    const-string v3, "is_published"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1508587
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1508588
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1508589
    if-eqz v2, :cond_9

    .line 1508590
    const-string v3, "is_service_page"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1508591
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1508592
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1508593
    if-eqz v2, :cond_a

    .line 1508594
    const-string v3, "is_verified"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1508595
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1508596
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1508597
    if-eqz v2, :cond_b

    .line 1508598
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1508599
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1508600
    :cond_b
    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v2

    .line 1508601
    if-eqz v2, :cond_c

    .line 1508602
    const-string v2, "place_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1508603
    invoke-virtual {v1, v0, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1508604
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1508605
    if-eqz v2, :cond_d

    .line 1508606
    const-string v3, "profilePictureAsCover"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1508607
    invoke-static {v1, v2, p1}, LX/9ZV;->a(LX/15i;ILX/0nX;)V

    .line 1508608
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1508609
    if-eqz v2, :cond_e

    .line 1508610
    const-string v3, "profile_photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1508611
    invoke-static {v1, v2, p1, p2}, LX/9ZO;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1508612
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1508613
    if-eqz v2, :cond_f

    .line 1508614
    const-string v3, "profile_picture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1508615
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1508616
    :cond_f
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1508617
    if-eqz v2, :cond_10

    .line 1508618
    const-string v3, "profile_picture_is_silhouette"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1508619
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1508620
    :cond_10
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1508621
    if-eqz v2, :cond_11

    .line 1508622
    const-string v3, "redirection_info"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1508623
    invoke-static {v1, v2, p1, p2}, LX/9ZK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1508624
    :cond_11
    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1508625
    if-eqz v2, :cond_12

    .line 1508626
    const-string v3, "should_show_username"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1508627
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1508628
    :cond_12
    invoke-virtual {v1, v0, v6}, LX/15i;->g(II)I

    move-result v2

    .line 1508629
    if-eqz v2, :cond_13

    .line 1508630
    const-string v2, "super_category_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1508631
    invoke-virtual {v1, v0, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1508632
    :cond_13
    const/16 v2, 0x14

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1508633
    if-eqz v2, :cond_14

    .line 1508634
    const-string v3, "timeline_pinned_unit"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1508635
    invoke-static {v1, v2, p1}, LX/9ZL;->a(LX/15i;ILX/0nX;)V

    .line 1508636
    :cond_14
    const/16 v2, 0x15

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1508637
    if-eqz v2, :cond_15

    .line 1508638
    const-string v3, "username"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1508639
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1508640
    :cond_15
    invoke-virtual {v1, v0, v7}, LX/15i;->g(II)I

    move-result v2

    .line 1508641
    if-eqz v2, :cond_16

    .line 1508642
    const-string v2, "verification_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1508643
    invoke-virtual {v1, v0, v7}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1508644
    :cond_16
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1508645
    if-eqz v2, :cond_17

    .line 1508646
    const-string v2, "viewer_profile_permissions"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1508647
    invoke-virtual {v1, v0, p0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1508648
    :cond_17
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1508649
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1508547
    check-cast p1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$Serializer;->a(Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel;LX/0nX;LX/0my;)V

    return-void
.end method
