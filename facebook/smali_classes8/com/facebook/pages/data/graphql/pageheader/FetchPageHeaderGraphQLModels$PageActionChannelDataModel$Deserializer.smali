.class public final Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1507311
    const-class v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel;

    new-instance v1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1507312
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1507352
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 1507313
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1507314
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1507315
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_a

    .line 1507316
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1507317
    :goto_0
    move v1, v2

    .line 1507318
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1507319
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1507320
    new-instance v1, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel;-><init>()V

    .line 1507321
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1507322
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1507323
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1507324
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1507325
    :cond_0
    return-object v1

    .line 1507326
    :cond_1
    const-string p0, "hide_tabs"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1507327
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v6, v1

    move v1, v3

    .line 1507328
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, p0, :cond_8

    .line 1507329
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1507330
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1507331
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v10, :cond_2

    .line 1507332
    const-string p0, "actionBarChannel"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1507333
    invoke-static {p1, v0}, LX/9Z8;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1507334
    :cond_3
    const-string p0, "addableTabsChannel"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1507335
    invoke-static {p1, v0}, LX/9Z9;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1507336
    :cond_4
    const-string p0, "additionalActionsChannel"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 1507337
    invoke-static {p1, v0}, LX/9ZA;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1507338
    :cond_5
    const-string p0, "primaryButtonsChannel"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1507339
    invoke-static {p1, v0}, LX/9ZB;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1507340
    :cond_6
    const-string p0, "profileTabNavigationChannel"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 1507341
    invoke-static {p1, v0}, LX/9ZC;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1507342
    :cond_7
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1507343
    :cond_8
    const/4 v10, 0x6

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 1507344
    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1507345
    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 1507346
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1507347
    if-eqz v1, :cond_9

    .line 1507348
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v6}, LX/186;->a(IZ)V

    .line 1507349
    :cond_9
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1507350
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1507351
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_a
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    goto/16 :goto_1
.end method
