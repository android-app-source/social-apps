.class public Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/0aG;

.field public final d:LX/1Ck;

.field public final e:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public final f:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation
.end field

.field public final g:LX/0kL;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1501479
    const-class v0, Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0aG;LX/1Ck;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/String;LX/0kL;)V
    .locals 0
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1501480
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1501481
    iput-object p1, p0, Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;->b:Landroid/content/Context;

    .line 1501482
    iput-object p2, p0, Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;->c:LX/0aG;

    .line 1501483
    iput-object p3, p0, Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;->d:LX/1Ck;

    .line 1501484
    iput-object p4, p0, Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1501485
    iput-object p5, p0, Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;->f:Ljava/lang/String;

    .line 1501486
    iput-object p6, p0, Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;->g:LX/0kL;

    .line 1501487
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;
    .locals 7

    .prologue
    .line 1501488
    new-instance v0, Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v2

    check-cast v2, LX/0aG;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {p0}, LX/0eQ;->b(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v4

    check-cast v4, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {p0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v6

    check-cast v6, LX/0kL;

    invoke-direct/range {v0 .. v6}, Lcom/facebook/pages/common/bannedusers/api/PagesBanUserHelper;-><init>(Landroid/content/Context;LX/0aG;LX/1Ck;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/String;LX/0kL;)V

    .line 1501489
    return-object v0
.end method
