.class public final Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInStatusQueryModels$BrandedContentProfileOptInStatusQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7630e431
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInStatusQueryModels$BrandedContentProfileOptInStatusQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInStatusQueryModels$BrandedContentProfileOptInStatusQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:J

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1501730
    const-class v0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInStatusQueryModels$BrandedContentProfileOptInStatusQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1501729
    const-class v0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInStatusQueryModels$BrandedContentProfileOptInStatusQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1501727
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1501728
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1501725
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInStatusQueryModels$BrandedContentProfileOptInStatusQueryModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInStatusQueryModels$BrandedContentProfileOptInStatusQueryModel;->g:Ljava/lang/String;

    .line 1501726
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInStatusQueryModels$BrandedContentProfileOptInStatusQueryModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 1501717
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1501718
    invoke-direct {p0}, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInStatusQueryModels$BrandedContentProfileOptInStatusQueryModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1501719
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1501720
    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInStatusQueryModels$BrandedContentProfileOptInStatusQueryModel;->e:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1501721
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInStatusQueryModels$BrandedContentProfileOptInStatusQueryModel;->f:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1501722
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1501723
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1501724
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1501714
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1501715
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1501716
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 1501710
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1501711
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInStatusQueryModels$BrandedContentProfileOptInStatusQueryModel;->e:Z

    .line 1501712
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInStatusQueryModels$BrandedContentProfileOptInStatusQueryModel;->f:J

    .line 1501713
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1501703
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1501704
    iget-boolean v0, p0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInStatusQueryModels$BrandedContentProfileOptInStatusQueryModel;->e:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1501707
    new-instance v0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInStatusQueryModels$BrandedContentProfileOptInStatusQueryModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInStatusQueryModels$BrandedContentProfileOptInStatusQueryModel;-><init>()V

    .line 1501708
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1501709
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1501706
    const v0, -0x349813aa    # -1.5199318E7f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1501705
    const v0, -0x4160b73b

    return v0
.end method
