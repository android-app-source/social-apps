.class public final Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1501894
    const-class v0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel;

    new-instance v1, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1501895
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1501938
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 1501897
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1501898
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1501899
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1501900
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1501901
    if-eqz v2, :cond_7

    .line 1501902
    const-string v3, "search_results"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1501903
    const/4 v3, 0x0

    .line 1501904
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1501905
    invoke-virtual {v1, v2, v3, v3}, LX/15i;->a(III)I

    move-result v3

    .line 1501906
    if-eqz v3, :cond_0

    .line 1501907
    const-string v4, "count"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1501908
    invoke-virtual {p1, v3}, LX/0nX;->b(I)V

    .line 1501909
    :cond_0
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1501910
    if-eqz v3, :cond_5

    .line 1501911
    const-string v4, "edges"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1501912
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1501913
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result v5

    if-ge v4, v5, :cond_4

    .line 1501914
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result v5

    .line 1501915
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1501916
    const/4 p0, 0x0

    invoke-virtual {v1, v5, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1501917
    if-eqz p0, :cond_1

    .line 1501918
    const-string v0, "category"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1501919
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1501920
    :cond_1
    const/4 p0, 0x1

    invoke-virtual {v1, v5, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1501921
    if-eqz p0, :cond_2

    .line 1501922
    const-string v0, "node"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1501923
    invoke-static {v1, p0, p1, p2}, LX/9Ww;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1501924
    :cond_2
    const/4 p0, 0x2

    invoke-virtual {v1, v5, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1501925
    if-eqz p0, :cond_3

    .line 1501926
    const-string v0, "subtext"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1501927
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1501928
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1501929
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1501930
    :cond_4
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1501931
    :cond_5
    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1501932
    if-eqz v3, :cond_6

    .line 1501933
    const-string v4, "page_info"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1501934
    invoke-static {v1, v3, p1}, LX/3Bn;->a(LX/15i;ILX/0nX;)V

    .line 1501935
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1501936
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1501937
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1501896
    check-cast p1, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel$Serializer;->a(Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
