.class public final Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1502001
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1502002
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1502065
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1502066
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 10

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1502030
    if-nez p1, :cond_0

    move v0, v1

    .line 1502031
    :goto_0
    return v0

    .line 1502032
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1502033
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1502034
    :sswitch_0
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v2

    .line 1502035
    invoke-virtual {p0, p1, v4}, LX/15i;->p(II)I

    move-result v0

    .line 1502036
    const v3, 0x3b86340b

    const/4 v8, 0x0

    .line 1502037
    if-nez v0, :cond_1

    move v7, v8

    .line 1502038
    :goto_1
    move v3, v7

    .line 1502039
    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-virtual {p0, p1, v5, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 1502040
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1502041
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1502042
    invoke-virtual {p3, v1, v2, v1}, LX/186;->a(III)V

    .line 1502043
    invoke-virtual {p3, v4, v3}, LX/186;->b(II)V

    .line 1502044
    invoke-virtual {p3, v5, v0}, LX/186;->b(II)V

    .line 1502045
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1502046
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1502047
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1502048
    const-class v0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel$SearchResultsModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v4, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel$SearchResultsModel$EdgesModel$NodeModel;

    .line 1502049
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1502050
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1502051
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1502052
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1502053
    invoke-virtual {p3, v1, v2}, LX/186;->b(II)V

    .line 1502054
    invoke-virtual {p3, v4, v0}, LX/186;->b(II)V

    .line 1502055
    invoke-virtual {p3, v5, v3}, LX/186;->b(II)V

    .line 1502056
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1502057
    :cond_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v9

    .line 1502058
    if-nez v9, :cond_2

    const/4 v7, 0x0

    .line 1502059
    :goto_2
    if-ge v8, v9, :cond_3

    .line 1502060
    invoke-virtual {p0, v0, v8}, LX/15i;->q(II)I

    move-result p2

    .line 1502061
    invoke-static {p0, p2, v3, p3}, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p2

    aput p2, v7, v8

    .line 1502062
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 1502063
    :cond_2
    new-array v7, v9, [I

    goto :goto_2

    .line 1502064
    :cond_3
    const/4 v8, 0x1

    invoke-virtual {p3, v7, v8}, LX/186;->a([IZ)I

    move-result v7

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x3e0ab918 -> :sswitch_0
        0x3b86340b -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1502029
    new-instance v0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1502024
    if-eqz p0, :cond_0

    .line 1502025
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1502026
    if-eq v0, p0, :cond_0

    .line 1502027
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1502028
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1502009
    sparse-switch p2, :sswitch_data_0

    .line 1502010
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1502011
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1502012
    const v1, 0x3b86340b

    .line 1502013
    if-eqz v0, :cond_0

    .line 1502014
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result v3

    .line 1502015
    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    .line 1502016
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 1502017
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1502018
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1502019
    :cond_0
    const/4 v0, 0x2

    const-class v1, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 1502020
    invoke-static {v0, p3}, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 1502021
    :goto_1
    return-void

    .line 1502022
    :sswitch_1
    const-class v0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel$SearchResultsModel$EdgesModel$NodeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel$SearchResultsModel$EdgesModel$NodeModel;

    .line 1502023
    invoke-static {v0, p3}, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x3e0ab918 -> :sswitch_0
        0x3b86340b -> :sswitch_1
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1502003
    if-eqz p1, :cond_0

    .line 1502004
    invoke-static {p0, p1, p2}, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$DraculaImplementation;

    move-result-object v1

    .line 1502005
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$DraculaImplementation;

    .line 1502006
    if-eq v0, v1, :cond_0

    .line 1502007
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1502008
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1502000
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1502067
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1502068
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1501995
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1501996
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1501997
    :cond_0
    iput-object p1, p0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$DraculaImplementation;->a:LX/15i;

    .line 1501998
    iput p2, p0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$DraculaImplementation;->b:I

    .line 1501999
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1501994
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1501969
    new-instance v0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1501991
    iget v0, p0, LX/1vt;->c:I

    .line 1501992
    move v0, v0

    .line 1501993
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1501988
    iget v0, p0, LX/1vt;->c:I

    .line 1501989
    move v0, v0

    .line 1501990
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1501985
    iget v0, p0, LX/1vt;->b:I

    .line 1501986
    move v0, v0

    .line 1501987
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1501982
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1501983
    move-object v0, v0

    .line 1501984
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1501973
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1501974
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1501975
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1501976
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1501977
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1501978
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1501979
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1501980
    invoke-static {v3, v9, v2}, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1501981
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1501970
    iget v0, p0, LX/1vt;->c:I

    .line 1501971
    move v0, v0

    .line 1501972
    return v0
.end method
