.class public final Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInMutationQueryModels$BrandedContentProfileOptInMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInMutationQueryModels$BrandedContentProfileOptInMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1501529
    const-class v0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInMutationQueryModels$BrandedContentProfileOptInMutationModel;

    new-instance v1, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInMutationQueryModels$BrandedContentProfileOptInMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInMutationQueryModels$BrandedContentProfileOptInMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1501530
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1501531
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInMutationQueryModels$BrandedContentProfileOptInMutationModel;LX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 1501532
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1501533
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1501534
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1501535
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1501536
    if-eqz v2, :cond_3

    .line 1501537
    const-string v3, "sponsor_tags_opt_in"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1501538
    const-wide/16 v6, 0x0

    .line 1501539
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1501540
    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4}, LX/15i;->b(II)Z

    move-result v4

    .line 1501541
    if-eqz v4, :cond_0

    .line 1501542
    const-string v5, "is_opted_in"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1501543
    invoke-virtual {p1, v4}, LX/0nX;->a(Z)V

    .line 1501544
    :cond_0
    const/4 v4, 0x1

    invoke-virtual {v1, v2, v4, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v4

    .line 1501545
    cmp-long v6, v4, v6

    if-eqz v6, :cond_1

    .line 1501546
    const-string v6, "opted_in_time"

    invoke-virtual {p1, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1501547
    invoke-virtual {p1, v4, v5}, LX/0nX;->a(J)V

    .line 1501548
    :cond_1
    const/4 v4, 0x2

    invoke-virtual {v1, v2, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1501549
    if-eqz v4, :cond_2

    .line 1501550
    const-string v5, "owner_id"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1501551
    invoke-virtual {p1, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1501552
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1501553
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1501554
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1501555
    check-cast p1, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInMutationQueryModels$BrandedContentProfileOptInMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInMutationQueryModels$BrandedContentProfileOptInMutationModel$Serializer;->a(Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInMutationQueryModels$BrandedContentProfileOptInMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
