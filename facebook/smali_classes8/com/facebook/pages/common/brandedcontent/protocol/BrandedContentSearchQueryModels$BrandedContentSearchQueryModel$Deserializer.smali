.class public final Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1501752
    const-class v0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel;

    new-instance v1, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1501753
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1501823
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 1501754
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1501755
    const/4 v2, 0x0

    .line 1501756
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1501757
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1501758
    :goto_0
    move v1, v2

    .line 1501759
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1501760
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1501761
    new-instance v1, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel;

    invoke-direct {v1}, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentSearchQueryModels$BrandedContentSearchQueryModel;-><init>()V

    .line 1501762
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1501763
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1501764
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1501765
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1501766
    :cond_0
    return-object v1

    .line 1501767
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1501768
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1501769
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1501770
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1501771
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 1501772
    const-string v4, "search_results"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1501773
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1501774
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v5, :cond_c

    .line 1501775
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1501776
    :goto_2
    move v1, v3

    .line 1501777
    goto :goto_1

    .line 1501778
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1501779
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1501780
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 1501781
    :cond_5
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_a

    .line 1501782
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1501783
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1501784
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_5

    if-eqz v8, :cond_5

    .line 1501785
    const-string v9, "count"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 1501786
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v1

    move v7, v1

    move v1, v4

    goto :goto_3

    .line 1501787
    :cond_6
    const-string v9, "edges"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 1501788
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1501789
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_ARRAY:LX/15z;

    if-ne v8, v9, :cond_7

    .line 1501790
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_ARRAY:LX/15z;

    if-eq v8, v9, :cond_7

    .line 1501791
    const/4 v9, 0x0

    .line 1501792
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v8, v10, :cond_12

    .line 1501793
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1501794
    :goto_5
    move v8, v9

    .line 1501795
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1501796
    :cond_7
    invoke-static {v6, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v6

    move v6, v6

    .line 1501797
    goto :goto_3

    .line 1501798
    :cond_8
    const-string v9, "page_info"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 1501799
    invoke-static {p1, v0}, LX/3Bn;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_3

    .line 1501800
    :cond_9
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 1501801
    :cond_a
    const/4 v8, 0x3

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 1501802
    if-eqz v1, :cond_b

    .line 1501803
    invoke-virtual {v0, v3, v7, v3}, LX/186;->a(III)V

    .line 1501804
    :cond_b
    invoke-virtual {v0, v4, v6}, LX/186;->b(II)V

    .line 1501805
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1501806
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto/16 :goto_2

    :cond_c
    move v1, v3

    move v5, v3

    move v6, v3

    move v7, v3

    goto/16 :goto_3

    .line 1501807
    :cond_d
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1501808
    :cond_e
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, p0, :cond_11

    .line 1501809
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 1501810
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1501811
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_e

    if-eqz v12, :cond_e

    .line 1501812
    const-string p0, "category"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_f

    .line 1501813
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto :goto_6

    .line 1501814
    :cond_f
    const-string p0, "node"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_10

    .line 1501815
    invoke-static {p1, v0}, LX/9Ww;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_6

    .line 1501816
    :cond_10
    const-string p0, "subtext"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_d

    .line 1501817
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_6

    .line 1501818
    :cond_11
    const/4 v12, 0x3

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 1501819
    invoke-virtual {v0, v9, v11}, LX/186;->b(II)V

    .line 1501820
    const/4 v9, 0x1

    invoke-virtual {v0, v9, v10}, LX/186;->b(II)V

    .line 1501821
    const/4 v9, 0x2

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 1501822
    invoke-virtual {v0}, LX/186;->d()I

    move-result v9

    goto/16 :goto_5

    :cond_12
    move v8, v9

    move v10, v9

    move v11, v9

    goto :goto_6
.end method
