.class public Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field private final c:Ljava/lang/Integer;

.field public final d:Landroid/net/Uri;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field public final g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1502252
    new-instance v0, LX/9X0;

    invoke-direct {v0}, LX/9X0;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/9X1;)V
    .locals 1

    .prologue
    .line 1502236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1502237
    iget-object v0, p1, LX/9X1;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1502238
    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->a:Ljava/lang/String;

    .line 1502239
    iget-object v0, p1, LX/9X1;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1502240
    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->b:Ljava/lang/String;

    .line 1502241
    iget-object v0, p1, LX/9X1;->c:Ljava/lang/Integer;

    move-object v0, v0

    .line 1502242
    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->c:Ljava/lang/Integer;

    .line 1502243
    iget-object v0, p1, LX/9X1;->d:Landroid/net/Uri;

    move-object v0, v0

    .line 1502244
    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->d:Landroid/net/Uri;

    .line 1502245
    iget-object v0, p1, LX/9X1;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1502246
    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->f:Ljava/lang/String;

    .line 1502247
    iget-object v0, p1, LX/9X1;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1502248
    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->e:Ljava/lang/String;

    .line 1502249
    iget-boolean v0, p1, LX/9X1;->g:Z

    move v0, v0

    .line 1502250
    iput-boolean v0, p0, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->g:Z

    .line 1502251
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1502226
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1502227
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->a:Ljava/lang/String;

    .line 1502228
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->b:Ljava/lang/String;

    .line 1502229
    invoke-static {p1}, LX/46R;->c(Landroid/os/Parcel;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->c:Ljava/lang/Integer;

    .line 1502230
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->d:Landroid/net/Uri;

    .line 1502231
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->e:Ljava/lang/String;

    .line 1502232
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->f:Ljava/lang/String;

    .line 1502233
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->g:Z

    .line 1502234
    return-void

    .line 1502235
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static newBuilder()LX/9X1;
    .locals 1

    .prologue
    .line 1502225
    new-instance v0, LX/9X1;

    invoke-direct {v0}, LX/9X1;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1502213
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1502224
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1502223
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1502214
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1502215
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1502216
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->c:Ljava/lang/Integer;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Integer;)V

    .line 1502217
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->d:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1502218
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1502219
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1502220
    iget-boolean v0, p0, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1502221
    return-void

    .line 1502222
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
