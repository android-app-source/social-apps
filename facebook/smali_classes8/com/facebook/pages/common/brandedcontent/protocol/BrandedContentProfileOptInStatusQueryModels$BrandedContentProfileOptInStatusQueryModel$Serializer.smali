.class public final Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInStatusQueryModels$BrandedContentProfileOptInStatusQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInStatusQueryModels$BrandedContentProfileOptInStatusQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1501682
    const-class v0, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInStatusQueryModels$BrandedContentProfileOptInStatusQueryModel;

    new-instance v1, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInStatusQueryModels$BrandedContentProfileOptInStatusQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInStatusQueryModels$BrandedContentProfileOptInStatusQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1501683
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1501684
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInStatusQueryModels$BrandedContentProfileOptInStatusQueryModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 1501685
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1501686
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v4, 0x0

    .line 1501687
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1501688
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1501689
    if-eqz v2, :cond_0

    .line 1501690
    const-string v3, "is_opted_in"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1501691
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1501692
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 1501693
    cmp-long v4, v2, v4

    if-eqz v4, :cond_1

    .line 1501694
    const-string v4, "opted_in_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1501695
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 1501696
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1501697
    if-eqz v2, :cond_2

    .line 1501698
    const-string v3, "owner_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1501699
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1501700
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1501701
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1501702
    check-cast p1, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInStatusQueryModels$BrandedContentProfileOptInStatusQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInStatusQueryModels$BrandedContentProfileOptInStatusQueryModel$Serializer;->a(Lcom/facebook/pages/common/brandedcontent/protocol/BrandedContentProfileOptInStatusQueryModels$BrandedContentProfileOptInStatusQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
