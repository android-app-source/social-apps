.class public Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;
.super Landroid/view/View;
.source ""


# instance fields
.field private a:I

.field private b:I

.field private c:Landroid/graphics/Paint;

.field private d:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1413611
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1413612
    invoke-direct {p0}, Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;->a()V

    .line 1413613
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1413608
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1413609
    invoke-direct {p0}, Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;->a()V

    .line 1413610
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1413605
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1413606
    invoke-direct {p0}, Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;->a()V

    .line 1413607
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1413588
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;->c:Landroid/graphics/Paint;

    .line 1413589
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;->d:Landroid/graphics/Paint;

    .line 1413590
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 0

    .prologue
    .line 1413601
    iput p1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;->a:I

    .line 1413602
    iput p2, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;->b:I

    .line 1413603
    invoke-virtual {p0}, Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;->invalidate()V

    .line 1413604
    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 1413595
    invoke-virtual {p0}, Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;->getHeight()I

    move-result v8

    .line 1413596
    invoke-virtual {p0}, Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;->getWidth()I

    move-result v9

    .line 1413597
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;->a:I

    int-to-float v3, v0

    int-to-float v4, v8

    iget-object v5, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;->d:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1413598
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;->a:I

    int-to-float v3, v0

    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;->b:I

    int-to-float v5, v0

    int-to-float v6, v8

    iget-object v7, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;->c:Landroid/graphics/Paint;

    move-object v2, p1

    move v4, v1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1413599
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;->b:I

    int-to-float v3, v0

    int-to-float v5, v9

    int-to-float v6, v8

    iget-object v7, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;->d:Landroid/graphics/Paint;

    move-object v2, p1

    move v4, v1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1413600
    return-void
.end method

.method public setSelectedColor(I)V
    .locals 1

    .prologue
    .line 1413593
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1413594
    return-void
.end method

.method public setUnselectedColor(I)V
    .locals 1

    .prologue
    .line 1413591
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1413592
    return-void
.end method
