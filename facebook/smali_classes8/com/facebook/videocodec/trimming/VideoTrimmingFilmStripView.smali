.class public Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:Landroid/widget/ImageView;

.field private c:Landroid/widget/ImageView;

.field private d:Landroid/widget/ImageView;

.field private e:Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;

.field private f:Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;

.field private g:Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/3rW;

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:I

.field private s:I

.field private t:LX/8tW;

.field private u:Landroid/view/View;

.field private v:LX/8tY;

.field public w:I

.field public x:I

.field public y:I

.field public z:LX/8tO;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1413374
    const-class v0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;

    sput-object v0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1413377
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1413378
    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->o:I

    .line 1413379
    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    .line 1413380
    const/4 v0, -0x2

    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    .line 1413381
    invoke-direct {p0, p1}, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->a(Landroid/content/Context;)V

    .line 1413382
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1413383
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1413384
    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->o:I

    .line 1413385
    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    .line 1413386
    const/4 v0, -0x2

    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    .line 1413387
    invoke-direct {p0, p1}, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->a(Landroid/content/Context;)V

    .line 1413388
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1413389
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1413390
    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->o:I

    .line 1413391
    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    .line 1413392
    const/4 v0, -0x2

    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    .line 1413393
    invoke-direct {p0, p1}, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->a(Landroid/content/Context;)V

    .line 1413394
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1413395
    const v0, 0x7f0315ba

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1413396
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->h:Ljava/util/List;

    .line 1413397
    const v0, 0x7f0d309b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->b:Landroid/widget/ImageView;

    .line 1413398
    const v0, 0x7f0d309c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->c:Landroid/widget/ImageView;

    .line 1413399
    const v0, 0x7f0d3108

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->d:Landroid/widget/ImageView;

    .line 1413400
    const v0, 0x7f0d3105

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;

    iput-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->e:Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;

    .line 1413401
    const v0, 0x7f0d3106

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;

    iput-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->f:Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;

    .line 1413402
    const v0, 0x7f0d3107

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;

    iput-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->g:Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;

    .line 1413403
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->h:Ljava/util/List;

    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->b:Landroid/widget/ImageView;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1413404
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->h:Ljava/util/List;

    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->c:Landroid/widget/ImageView;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1413405
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->h:Ljava/util/List;

    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->d:Landroid/widget/ImageView;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1413406
    invoke-virtual {p0}, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1413407
    const v1, 0x7f0b06cd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->j:I

    .line 1413408
    const v1, 0x7f0b06ce

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->k:I

    .line 1413409
    new-instance v1, LX/3rW;

    new-instance v2, LX/8tU;

    invoke-direct {v2, p0}, LX/8tU;-><init>(Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;)V

    invoke-direct {v1, p1, v2}, LX/3rW;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->i:LX/3rW;

    .line 1413410
    new-instance v1, LX/8tV;

    invoke-direct {v1, p0}, LX/8tV;-><init>(Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;)V

    invoke-virtual {p0, v1}, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1413411
    invoke-virtual {p0, v3}, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->setWillNotDraw(Z)V

    .line 1413412
    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->f:Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;

    invoke-virtual {v1, v3}, Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;->setSelectedColor(I)V

    .line 1413413
    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->f:Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;

    const v2, 0x7f0a03e8

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;->setUnselectedColor(I)V

    .line 1413414
    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->g:Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;

    const v2, 0x7f0a03e6

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;->setSelectedColor(I)V

    .line 1413415
    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->g:Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;

    const v2, 0x7f0a03e7

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;->setUnselectedColor(I)V

    .line 1413416
    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    const/16 v3, 0x3e8

    .line 1413417
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->o:I

    if-lez v0, :cond_0

    .line 1413418
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->o:I

    iget v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->n:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1413419
    iget v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    iget v2, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    sub-int/2addr v1, v2

    if-le v1, v0, :cond_0

    .line 1413420
    if-eqz p1, :cond_2

    .line 1413421
    iget v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    sub-int v0, v1, v0

    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    .line 1413422
    :cond_0
    :goto_0
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    iget v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    sub-int/2addr v0, v1

    .line 1413423
    if-ge v0, v3, :cond_1

    .line 1413424
    if-eqz p1, :cond_3

    .line 1413425
    iget v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    rsub-int v0, v0, 0x3e8

    sub-int v0, v1, v0

    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    .line 1413426
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    if-gez v0, :cond_1

    .line 1413427
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    .line 1413428
    iput v3, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    .line 1413429
    :cond_1
    :goto_1
    return-void

    .line 1413430
    :cond_2
    iget v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    goto :goto_0

    .line 1413431
    :cond_3
    iget v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    rsub-int v0, v0, 0x3e8

    add-int/2addr v0, v1

    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    .line 1413432
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    iget v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->n:I

    if-le v0, v1, :cond_1

    .line 1413433
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->n:I

    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    .line 1413434
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->n:I

    add-int/lit16 v0, v0, -0x3e8

    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    goto :goto_1
.end method

.method private a(F)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1413488
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->t:LX/8tW;

    sget-object v1, LX/8tW;->LEFT_TRIM:LX/8tW;

    if-ne v0, v1, :cond_2

    .line 1413489
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->v:LX/8tY;

    iget v1, v0, LX/8tY;->b:F

    neg-float v2, p1

    add-float/2addr v1, v2

    iput v1, v0, LX/8tY;->b:F

    .line 1413490
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->v:LX/8tY;

    iget v0, v0, LX/8tY;->a:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->v:LX/8tY;

    iget v1, v1, LX/8tY;->b:F

    iget v2, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->p:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iget v2, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->n:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    .line 1413491
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    iget v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    invoke-static {v0, v3, v1}, LX/0yq;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    .line 1413492
    invoke-direct {p0, v3}, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->a(Z)V

    .line 1413493
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->z:LX/8tO;

    if-eqz v0, :cond_0

    .line 1413494
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->z:LX/8tO;

    iget v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    iget v2, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    invoke-interface {v0, v1, v2}, LX/8tO;->a(II)V

    .line 1413495
    :cond_0
    invoke-direct {p0}, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->b()V

    .line 1413496
    invoke-direct {p0}, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->c()V

    .line 1413497
    :cond_1
    :goto_0
    return v4

    .line 1413498
    :cond_2
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->t:LX/8tW;

    sget-object v1, LX/8tW;->RIGHT_TRIM:LX/8tW;

    if-ne v0, v1, :cond_4

    .line 1413499
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->v:LX/8tY;

    iget v1, v0, LX/8tY;->b:F

    neg-float v2, p1

    add-float/2addr v1, v2

    iput v1, v0, LX/8tY;->b:F

    .line 1413500
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->v:LX/8tY;

    iget v0, v0, LX/8tY;->a:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->v:LX/8tY;

    iget v1, v1, LX/8tY;->b:F

    iget v2, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->p:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iget v2, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->n:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    .line 1413501
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    iget v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    iget v2, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->n:I

    invoke-static {v0, v1, v2}, LX/0yq;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    .line 1413502
    invoke-direct {p0, v4}, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->a(Z)V

    .line 1413503
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->z:LX/8tO;

    if-eqz v0, :cond_3

    .line 1413504
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->z:LX/8tO;

    iget v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    iget v2, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    invoke-interface {v0, v1, v2}, LX/8tO;->a(II)V

    .line 1413505
    :cond_3
    invoke-direct {p0}, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->b()V

    .line 1413506
    invoke-direct {p0}, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->c()V

    goto :goto_0

    .line 1413507
    :cond_4
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->t:LX/8tW;

    sget-object v1, LX/8tW;->SCRUBBER:LX/8tW;

    if-ne v0, v1, :cond_1

    .line 1413508
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->v:LX/8tY;

    iget v1, v0, LX/8tY;->b:F

    neg-float v2, p1

    add-float/2addr v1, v2

    iput v1, v0, LX/8tY;->b:F

    .line 1413509
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->v:LX/8tY;

    iget v0, v0, LX/8tY;->a:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->v:LX/8tY;

    iget v1, v1, LX/8tY;->b:F

    iget v2, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->p:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iget v2, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->n:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->y:I

    .line 1413510
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->y:I

    iget v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    iget v2, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    invoke-static {v0, v1, v2}, LX/0yq;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->y:I

    .line 1413511
    invoke-direct {p0}, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->b()V

    .line 1413512
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->z:LX/8tO;

    if-eqz v0, :cond_1

    .line 1413513
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->z:LX/8tO;

    iget v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->y:I

    invoke-interface {v0, v1}, LX/8tO;->a(I)V

    goto/16 :goto_0
.end method

.method private a(Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, -0x1

    .line 1413435
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 1413436
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v5, v0

    .line 1413437
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v6, v0

    .line 1413438
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 1413439
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->getHitRect(Landroid/graphics/Rect;)V

    .line 1413440
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1413441
    iput-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->u:Landroid/view/View;

    .line 1413442
    iget-object v8, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->b:Landroid/widget/ImageView;

    if-ne v0, v8, :cond_2

    .line 1413443
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    .line 1413444
    sget-object v8, LX/8tW;->LEFT_TRIM:LX/8tW;

    iput-object v8, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->t:LX/8tW;

    .line 1413445
    :goto_0
    if-eq v0, v3, :cond_0

    .line 1413446
    new-instance v3, LX/8tY;

    invoke-direct {v3, v0}, LX/8tY;-><init>(I)V

    iput-object v3, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->v:LX/8tY;

    .line 1413447
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->u:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setPressed(Z)V

    .line 1413448
    const/4 v0, 0x2

    const/4 v2, 0x0

    invoke-static {p0, v0, v2}, LX/0vv;->a(Landroid/view/View;ILandroid/graphics/Paint;)V

    .line 1413449
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->z:LX/8tO;

    if-eqz v0, :cond_1

    .line 1413450
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->z:LX/8tO;

    iget-object v2, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->t:LX/8tW;

    invoke-interface {v0, v2}, LX/8tO;->a(LX/8tW;)V

    :cond_1
    move v0, v1

    .line 1413451
    :goto_1
    return v0

    .line 1413452
    :cond_2
    iget-object v8, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->c:Landroid/widget/ImageView;

    if-ne v0, v8, :cond_3

    .line 1413453
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    .line 1413454
    sget-object v8, LX/8tW;->RIGHT_TRIM:LX/8tW;

    iput-object v8, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->t:LX/8tW;

    goto :goto_0

    .line 1413455
    :cond_3
    iget-object v8, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->d:Landroid/widget/ImageView;

    if-ne v0, v8, :cond_5

    .line 1413456
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->y:I

    .line 1413457
    sget-object v8, LX/8tW;->SCRUBBER:LX/8tW;

    iput-object v8, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->t:LX/8tW;

    goto :goto_0

    :cond_4
    move v0, v2

    .line 1413458
    goto :goto_1

    :cond_5
    move v0, v3

    goto :goto_0
.end method

.method public static synthetic a(Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1413487
    invoke-direct {p0, p1}, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    .prologue
    .line 1413486
    invoke-direct {p0, p3}, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->a(F)Z

    move-result v0

    return v0
.end method

.method public static a$redex0(Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1413479
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->u:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1413480
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->u:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setPressed(Z)V

    .line 1413481
    iput-object v3, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->u:Landroid/view/View;

    .line 1413482
    :cond_0
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->z:LX/8tO;

    if-eqz v0, :cond_1

    .line 1413483
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->z:LX/8tO;

    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->t:LX/8tW;

    invoke-interface {v0, v1}, LX/8tO;->b(LX/8tW;)V

    .line 1413484
    :cond_1
    invoke-static {p0, v2, v3}, LX/0vv;->a(Landroid/view/View;ILandroid/graphics/Paint;)V

    .line 1413485
    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    .line 1413465
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->n:I

    if-nez v0, :cond_0

    .line 1413466
    :goto_0
    return-void

    .line 1413467
    :cond_0
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLeft()I

    move-result v0

    .line 1413468
    iget v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->q:I

    iget v2, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    iget v3, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->p:I

    mul-int/2addr v2, v3

    iget v3, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->n:I

    div-int/2addr v2, v3

    add-int/2addr v1, v2

    .line 1413469
    sub-int v0, v1, v0

    .line 1413470
    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->offsetLeftAndRight(I)V

    .line 1413471
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getRight()I

    move-result v0

    .line 1413472
    iget v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->r:I

    iget v2, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->n:I

    iget v3, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->p:I

    mul-int/2addr v2, v3

    iget v3, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->n:I

    div-int/2addr v2, v3

    sub-int/2addr v1, v2

    .line 1413473
    sub-int v0, v1, v0

    .line 1413474
    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->offsetLeftAndRight(I)V

    .line 1413475
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->d:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLeft()I

    move-result v0

    .line 1413476
    iget v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->s:I

    iget v2, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->y:I

    iget v3, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->p:I

    mul-int/2addr v2, v3

    iget v3, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->n:I

    div-int/2addr v2, v3

    add-int/2addr v1, v2

    .line 1413477
    sub-int v0, v1, v0

    .line 1413478
    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->offsetLeftAndRight(I)V

    goto :goto_0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 1413459
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->n:I

    if-nez v0, :cond_0

    .line 1413460
    :goto_0
    return-void

    .line 1413461
    :cond_0
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->f:Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;

    invoke-virtual {v1}, Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;->getWidth()I

    move-result v1

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->n:I

    div-int/2addr v0, v1

    .line 1413462
    iget v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    iget-object v2, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->f:Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;

    invoke-virtual {v2}, Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;->getWidth()I

    move-result v2

    mul-int/2addr v1, v2

    iget v2, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->n:I

    div-int/2addr v1, v2

    .line 1413463
    iget-object v2, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->f:Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;

    invoke-virtual {v2, v0, v1}, Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;->a(II)V

    .line 1413464
    iget-object v2, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->g:Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;

    invoke-virtual {v2, v0, v1}, Lcom/facebook/videocodec/trimming/VideoTrimmingSelectionMaskView;->a(II)V

    goto :goto_0
.end method


# virtual methods
.method public final a(ILandroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 1413375
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->e:Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;->a(ILandroid/graphics/Bitmap;)V

    .line 1413376
    return-void
.end method

.method public getClipTimeMs()I
    .locals 1

    .prologue
    .line 1413326
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->y:I

    return v0
.end method

.method public getEndTimeMs()I
    .locals 1

    .prologue
    .line 1413329
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    return v0
.end method

.method public getFilmStripHeight()I
    .locals 1

    .prologue
    .line 1413330
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->e:Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;

    invoke-virtual {v0}, Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;->getHeight()I

    move-result v0

    return v0
.end method

.method public getNumPreviewImages()I
    .locals 1

    .prologue
    .line 1413327
    const/4 p0, 0x7

    move v0, p0

    .line 1413328
    return v0
.end method

.method public getStartTimeMs()I
    .locals 1

    .prologue
    .line 1413331
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    return v0
.end method

.method public getVideoDurationMs()I
    .locals 1

    .prologue
    .line 1413332
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->n:I

    return v0
.end method

.method public final onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 1413333
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomRelativeLayout;->onLayout(ZIIII)V

    .line 1413334
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->n:I

    if-nez v0, :cond_0

    .line 1413335
    :goto_0
    return-void

    .line 1413336
    :cond_0
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->e:Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;

    invoke-virtual {v0}, Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->p:I

    .line 1413337
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->e:Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;

    invoke-virtual {v0}, Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->b:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->j:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->q:I

    .line 1413338
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->e:Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;

    invoke-virtual {v0}, Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;->getRight()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->c:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getWidth()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->k:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->r:I

    .line 1413339
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->e:Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;

    invoke-virtual {v0}, Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->d:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->s:I

    .line 1413340
    invoke-direct {p0}, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->b()V

    .line 1413341
    invoke-direct {p0}, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->c()V

    goto :goto_0
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 1413342
    check-cast p1, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView$SavedState;

    .line 1413343
    invoke-virtual {p1}, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1413344
    iget v0, p1, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView$SavedState;->a:I

    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    .line 1413345
    iget v0, p1, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView$SavedState;->b:I

    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    .line 1413346
    iget v0, p1, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView$SavedState;->c:I

    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->y:I

    .line 1413347
    return-void
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 5

    .prologue
    .line 1413348
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 1413349
    new-instance v1, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView$SavedState;

    iget v2, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    iget v3, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    iget v4, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->y:I

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView$SavedState;-><init>(Landroid/os/Parcelable;III)V

    return-object v1
.end method

.method public setClipTimeMs(I)V
    .locals 2

    .prologue
    .line 1413350
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->y:I

    if-eq v0, p1, :cond_0

    .line 1413351
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    iget v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    invoke-static {p1, v0, v1}, LX/0yq;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->y:I

    .line 1413352
    invoke-direct {p0}, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->b()V

    .line 1413353
    :cond_0
    return-void
.end method

.method public setListener(LX/8tO;)V
    .locals 0

    .prologue
    .line 1413354
    iput-object p1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->z:LX/8tO;

    .line 1413355
    return-void
.end method

.method public setMaximumDuration(I)V
    .locals 1

    .prologue
    .line 1413356
    mul-int/lit16 v0, p1, 0x3e8

    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->o:I

    .line 1413357
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->a(Z)V

    .line 1413358
    invoke-virtual {p0}, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->requestLayout()V

    .line 1413359
    return-void
.end method

.method public setVideoMetaData(LX/60x;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1413360
    iget v0, p1, LX/60x;->b:I

    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->l:I

    .line 1413361
    iget v0, p1, LX/60x;->c:I

    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->m:I

    .line 1413362
    iget-wide v0, p1, LX/60x;->a:J

    long-to-int v0, v0

    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->n:I

    .line 1413363
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 1413364
    iput v3, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    .line 1413365
    :cond_0
    :goto_0
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_3

    .line 1413366
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->n:I

    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    .line 1413367
    :cond_1
    :goto_1
    invoke-direct {p0, v3}, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->a(Z)V

    .line 1413368
    invoke-virtual {p0}, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->requestLayout()V

    .line 1413369
    return-void

    .line 1413370
    :cond_2
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    iget v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->n:I

    if-le v0, v1, :cond_0

    .line 1413371
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    iget v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->n:I

    invoke-static {v0, v3, v1}, LX/0yq;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    goto :goto_0

    .line 1413372
    :cond_3
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    iget v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->n:I

    if-le v0, v1, :cond_1

    .line 1413373
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    iget v1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    iget v2, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->n:I

    invoke-static {v0, v1, v2}, LX/0yq;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    goto :goto_1
.end method
