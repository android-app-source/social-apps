.class public final Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView$SavedState;
.super Landroid/view/View$BaseSavedState;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1413305
    new-instance v0, LX/8tX;

    invoke-direct {v0}, LX/8tX;-><init>()V

    sput-object v0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1413306
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 1413307
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView$SavedState;->a:I

    .line 1413308
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView$SavedState;->b:I

    .line 1413309
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView$SavedState;->c:I

    .line 1413310
    return-void
.end method

.method public synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 1413311
    invoke-direct {p0, p1}, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView$SavedState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcelable;III)V
    .locals 0

    .prologue
    .line 1413312
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 1413313
    iput p2, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView$SavedState;->a:I

    .line 1413314
    iput p3, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView$SavedState;->b:I

    .line 1413315
    iput p4, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView$SavedState;->c:I

    .line 1413316
    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1413317
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1413318
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView$SavedState;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1413319
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView$SavedState;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1413320
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView$SavedState;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1413321
    return-void
.end method
