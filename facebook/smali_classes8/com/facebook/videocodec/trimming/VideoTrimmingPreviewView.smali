.class public Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;
.super LX/8tJ;
.source ""


# static fields
.field private static final d:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public e:LX/03V;

.field public f:LX/1CX;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1413569
    const-class v0, Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    sput-object v0, Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;->d:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1413570
    invoke-direct {p0, p1}, LX/8tJ;-><init>(Landroid/content/Context;)V

    .line 1413571
    invoke-direct {p0}, Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;->f()V

    .line 1413572
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1413573
    invoke-direct {p0, p1, p2}, LX/8tJ;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1413574
    invoke-direct {p0}, Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;->f()V

    .line 1413575
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1413576
    invoke-direct {p0, p1, p2, p3}, LX/8tJ;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1413577
    invoke-direct {p0}, Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;->f()V

    .line 1413578
    return-void
.end method

.method private a(LX/03V;LX/1CX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1413579
    iput-object p1, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;->e:LX/03V;

    .line 1413580
    iput-object p2, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;->f:LX/1CX;

    .line 1413581
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-static {v1}, LX/1CX;->a(LX/0QB;)LX/1CX;

    move-result-object v1

    check-cast v1, LX/1CX;

    invoke-direct {p0, v0, v1}, Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;->a(LX/03V;LX/1CX;)V

    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 1413582
    const-class v0, Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    invoke-static {v0, p0}, Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1413583
    new-instance v0, LX/8tZ;

    invoke-direct {v0, p0}, LX/8tZ;-><init>(Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1413584
    iget-object v0, p0, LX/8tJ;->a:Landroid/widget/VideoView;

    new-instance v1, LX/8ta;

    invoke-direct {v1, p0}, LX/8ta;-><init>(Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;)V

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 1413585
    iget-object v0, p0, LX/8tJ;->a:Landroid/widget/VideoView;

    new-instance v1, LX/8tb;

    invoke-direct {v1, p0}, LX/8tb;-><init>(Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;)V

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 1413586
    iget-object v0, p0, LX/8tJ;->a:Landroid/widget/VideoView;

    new-instance v1, LX/8tc;

    invoke-direct {v1, p0}, LX/8tc;-><init>(Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;)V

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 1413587
    return-void
.end method
