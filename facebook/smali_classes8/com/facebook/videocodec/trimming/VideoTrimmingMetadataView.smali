.class public Lcom/facebook/videocodec/trimming/VideoTrimmingMetadataView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1413514
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1413515
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/videocodec/trimming/VideoTrimmingMetadataView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1413516
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1413517
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1413518
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/videocodec/trimming/VideoTrimmingMetadataView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1413519
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1413520
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1413521
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/videocodec/trimming/VideoTrimmingMetadataView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1413522
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 1413523
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/videocodec/trimming/VideoTrimmingMetadataView;->setOrientation(I)V

    .line 1413524
    const v0, 0x7f0315bc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1413525
    sget-object v0, LX/03r;->VideoTrimmingMetadataView:[I

    invoke-virtual {p1, p2, v0, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 1413526
    const/16 v0, 0x1

    invoke-virtual {v1, v0, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    .line 1413527
    const/16 v0, 0x2

    invoke-virtual {v1, v0, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    .line 1413528
    const v0, 0x7f0d310c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingMetadataView;->a:Landroid/widget/TextView;

    .line 1413529
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingMetadataView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1413530
    const/16 v0, 0x0

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 1413531
    if-eqz v0, :cond_0

    .line 1413532
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1413533
    iget-object v2, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingMetadataView;->a:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1413534
    :cond_0
    const v0, 0x7f0d310d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingMetadataView;->b:Landroid/widget/TextView;

    .line 1413535
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingMetadataView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1413536
    const v0, 0x7f0d310e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingMetadataView;->c:Landroid/widget/TextView;

    .line 1413537
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingMetadataView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1413538
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 1413539
    return-void
.end method


# virtual methods
.method public setDurationString(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1413540
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingMetadataView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1413541
    return-void
.end method

.method public setSizeString(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1413542
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoTrimmingMetadataView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1413543
    return-void
.end method
