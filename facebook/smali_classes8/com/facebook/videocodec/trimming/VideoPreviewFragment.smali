.class public Lcom/facebook/videocodec/trimming/VideoPreviewFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/1jv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/base/fragment/FbFragment;",
        "LX/1jv",
        "<",
        "LX/8tL;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:LX/2Mj;

.field private c:LX/8tH;

.field private d:LX/2MV;

.field private e:LX/0TD;

.field private f:Ljava/util/concurrent/Executor;

.field public g:LX/03V;

.field private h:Lcom/facebook/widget/listview/EmptyListViewItem;

.field public i:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

.field private j:Landroid/view/View;

.field private k:Landroid/widget/LinearLayout;

.field public l:Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;

.field private m:Lcom/facebook/videocodec/trimming/VideoTrimmingMetadataView;

.field private n:Lcom/facebook/videocodec/trimming/VideoTrimmingMetadataView;

.field private o:LX/60x;

.field public p:Landroid/net/Uri;

.field public q:I

.field private r:LX/1Mv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Mv",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private s:I

.field private t:I

.field private u:Z

.field private v:Z

.field private w:Z

.field public x:LX/8tT;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1413221
    const-class v0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;

    sput-object v0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1413157
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1413158
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->q:I

    .line 1413159
    iput-boolean v1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->v:Z

    .line 1413160
    iput-boolean v1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->w:Z

    .line 1413161
    return-void
.end method

.method private a(ILandroid/media/MediaMetadataRetriever;)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 1413162
    mul-int/lit16 v0, p1, 0x3e8

    int-to-long v0, v0

    const/4 v2, 0x2

    invoke-virtual {p2, v0, v1, v2}, Landroid/media/MediaMetadataRetriever;->getFrameAtTime(JI)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1413163
    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->l:Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;

    invoke-virtual {v1}, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->getFilmStripHeight()I

    move-result v1

    .line 1413164
    int-to-float v1, v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 1413165
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v1

    float-to-int v2, v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v1, v3

    float-to-int v1, v1

    const/4 v3, 0x0

    invoke-static {v0, v2, v1, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1413166
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1413167
    return-object v1
.end method

.method private a(JZ)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1413168
    const/4 v0, 0x0

    .line 1413169
    const-wide/16 v2, 0x0

    cmp-long v1, p1, v2

    if-lez v1, :cond_0

    .line 1413170
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->c:LX/8tH;

    long-to-int v1, p1

    invoke-virtual {v0, v1}, LX/8tH;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 1413171
    if-eqz p3, :cond_0

    .line 1413172
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080df9

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1413173
    :cond_0
    return-object v0
.end method

.method private a(LX/60x;)V
    .locals 4

    .prologue
    .line 1413174
    iput-object p1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->o:LX/60x;

    .line 1413175
    invoke-direct {p0}, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->e()V

    .line 1413176
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->l:Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;

    invoke-virtual {v0, p1}, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->setVideoMetaData(LX/60x;)V

    .line 1413177
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->q:I

    if-lez v0, :cond_0

    .line 1413178
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->b:LX/2Mj;

    iget v1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->q:I

    .line 1413179
    if-gez v1, :cond_4

    .line 1413180
    const/4 v2, -0x1

    .line 1413181
    :goto_0
    move v0, v2

    .line 1413182
    if-lez v0, :cond_0

    .line 1413183
    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->l:Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;

    invoke-virtual {v1, v0}, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->setMaximumDuration(I)V

    .line 1413184
    :cond_0
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->c:LX/8tH;

    iget-wide v2, p1, LX/60x;->a:J

    invoke-virtual {v0, v2, v3}, LX/8tH;->a(J)Ljava/lang/String;

    move-result-object v0

    .line 1413185
    iget-wide v2, p1, LX/60x;->f:J

    const/4 v1, 0x0

    invoke-direct {p0, v2, v3, v1}, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->a(JZ)Ljava/lang/String;

    move-result-object v1

    .line 1413186
    iget-object v2, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->m:Lcom/facebook/videocodec/trimming/VideoTrimmingMetadataView;

    invoke-virtual {v2, v0}, Lcom/facebook/videocodec/trimming/VideoTrimmingMetadataView;->setDurationString(Ljava/lang/String;)V

    .line 1413187
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->m:Lcom/facebook/videocodec/trimming/VideoTrimmingMetadataView;

    invoke-virtual {v0, v1}, Lcom/facebook/videocodec/trimming/VideoTrimmingMetadataView;->setSizeString(Ljava/lang/String;)V

    .line 1413188
    invoke-static {p0}, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->l(Lcom/facebook/videocodec/trimming/VideoPreviewFragment;)V

    .line 1413189
    invoke-direct {p0}, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->m()V

    .line 1413190
    iget v0, p1, LX/60x;->d:I

    const/16 v1, 0x5a

    if-eq v0, v1, :cond_1

    iget v0, p1, LX/60x;->d:I

    const/16 v1, 0x10e

    if-ne v0, v1, :cond_3

    .line 1413191
    :cond_1
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->i:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    iget v1, p1, LX/60x;->c:I

    iget v2, p1, LX/60x;->b:I

    invoke-virtual {v0, v1, v2}, LX/8tJ;->a(II)V

    .line 1413192
    :goto_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 1413193
    invoke-static {p1}, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->b(LX/60x;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 1413194
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->j:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1413195
    const/4 v1, -0x2

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 1413196
    const/4 v1, 0x0

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 1413197
    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->j:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1413198
    :cond_2
    return-void

    .line 1413199
    :cond_3
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->i:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    iget v1, p1, LX/60x;->b:I

    iget v2, p1, LX/60x;->c:I

    invoke-virtual {v0, v1, v2}, LX/8tJ;->a(II)V

    goto :goto_1

    .line 1413200
    :cond_4
    iget v2, p1, LX/60x;->g:I

    if-lez v2, :cond_5

    iget v2, p1, LX/60x;->g:I

    .line 1413201
    :goto_2
    invoke-static {v0, p1}, LX/2Mj;->a(LX/2Mj;LX/60x;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1413202
    div-int/lit8 v2, v2, 0x8

    div-int v2, v1, v2

    goto :goto_0

    .line 1413203
    :cond_5
    const/4 v2, 0x0

    goto :goto_2
.end method

.method private a(LX/8tH;LX/8tK;LX/2MV;LX/0TD;Ljava/util/concurrent/Executor;LX/03V;)V
    .locals 0
    .param p4    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p5    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1413204
    iput-object p2, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->b:LX/2Mj;

    .line 1413205
    iput-object p1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->c:LX/8tH;

    .line 1413206
    iput-object p3, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->d:LX/2MV;

    .line 1413207
    iput-object p4, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->e:LX/0TD;

    .line 1413208
    iput-object p5, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->f:Ljava/util/concurrent/Executor;

    .line 1413209
    iput-object p6, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->g:LX/03V;

    .line 1413210
    return-void
.end method

.method private a(LX/8tL;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8tL;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1413211
    iget-object v0, p1, LX/8tL;->b:Ljava/lang/Exception;

    if-eqz v0, :cond_0

    .line 1413212
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->g:LX/03V;

    const-string v1, "VideoTrimmingFragment_METADATA_EXTRACT_FAILED"

    iget-object v2, p1, LX/8tL;->b:Ljava/lang/Exception;

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1413213
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->b(Z)V

    .line 1413214
    :goto_0
    return-void

    .line 1413215
    :cond_0
    iget-object v0, p1, LX/8tL;->a:LX/60x;

    invoke-direct {p0, v0}, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->a(LX/60x;)V

    .line 1413216
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->b(Z)V

    goto :goto_0
.end method

.method private a(Landroid/net/Uri;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 1413217
    invoke-direct {p0}, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->d()V

    .line 1413218
    invoke-direct {p0}, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->k()V

    .line 1413219
    invoke-direct {p0, p1, p2}, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->b(Landroid/net/Uri;Landroid/net/Uri;)V

    .line 1413220
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;

    invoke-static {v6}, LX/8tH;->a(LX/0QB;)LX/8tH;

    move-result-object v1

    check-cast v1, LX/8tH;

    invoke-static {v6}, LX/8tK;->a(LX/0QB;)LX/8tK;

    move-result-object v2

    check-cast v2, LX/8tK;

    invoke-static {v6}, LX/2MU;->b(LX/0QB;)LX/2MU;

    move-result-object v3

    check-cast v3, LX/2MV;

    invoke-static {v6}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, LX/0TD;

    invoke-static {v6}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/Executor;

    invoke-static {v6}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-direct/range {v0 .. v6}, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->a(LX/8tH;LX/8tK;LX/2MV;LX/0TD;Ljava/util/concurrent/Executor;LX/03V;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/videocodec/trimming/VideoPreviewFragment;LX/8tW;)V
    .locals 1

    .prologue
    .line 1413280
    sget-object v0, LX/8tW;->SCRUBBER:LX/8tW;

    if-ne p1, v0, :cond_0

    .line 1413281
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->u:Z

    .line 1413282
    :goto_0
    return-void

    .line 1413283
    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->b(LX/8tW;)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/videocodec/trimming/VideoPreviewFragment;Landroid/net/Uri;ILjava/util/concurrent/Executor;)V
    .locals 6

    .prologue
    .line 1413222
    new-instance v1, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v1}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 1413223
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1, v0, p1}, Landroid/media/MediaMetadataRetriever;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 1413224
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_0

    .line 1413225
    int-to-long v2, v0

    :try_start_0
    iget-object v4, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->o:LX/60x;

    iget-wide v4, v4, LX/60x;->a:J

    mul-long/2addr v2, v4

    int-to-long v4, p2

    div-long/2addr v2, v4

    long-to-int v2, v2

    .line 1413226
    invoke-direct {p0, v2, v1}, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->a(ILandroid/media/MediaMetadataRetriever;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1413227
    new-instance v3, Lcom/facebook/videocodec/trimming/VideoPreviewFragment$6;

    invoke-direct {v3, p0, v0, v2}, Lcom/facebook/videocodec/trimming/VideoPreviewFragment$6;-><init>(Lcom/facebook/videocodec/trimming/VideoPreviewFragment;ILandroid/graphics/Bitmap;)V

    const v2, -0x32c44759

    invoke-static {p3, v3, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1413228
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1413229
    :cond_0
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 1413230
    return-void

    .line 1413231
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V

    throw v0
.end method

.method private b(LX/8tW;)V
    .locals 6

    .prologue
    const/4 v1, -0x1

    .line 1413232
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->l:Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;

    .line 1413233
    iget v2, v0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    move v0, v2

    .line 1413234
    iget-object v2, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->l:Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;

    .line 1413235
    iget v3, v2, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    move v2, v3

    .line 1413236
    iget v3, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->s:I

    if-ne v0, v3, :cond_1

    iget v3, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->t:I

    if-ne v2, v3, :cond_1

    .line 1413237
    :cond_0
    :goto_0
    return-void

    .line 1413238
    :cond_1
    iget-object v3, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->l:Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;

    .line 1413239
    iget v4, v3, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->y:I

    move v3, v4

    .line 1413240
    sget-object v4, LX/8tW;->LEFT_TRIM:LX/8tW;

    if-eq p1, v4, :cond_2

    sget-object v4, LX/8tW;->RIGHT_TRIM:LX/8tW;

    if-ne p1, v4, :cond_4

    .line 1413241
    :cond_2
    if-gt v0, v3, :cond_3

    iget v4, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->s:I

    if-ne v4, v3, :cond_4

    .line 1413242
    :cond_3
    iget-object v4, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->l:Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;

    invoke-virtual {v4, v0}, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->setClipTimeMs(I)V

    .line 1413243
    iget-object v4, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->i:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    invoke-virtual {v4, v0}, LX/8tJ;->a(I)V

    .line 1413244
    :cond_4
    sget-object v4, LX/8tW;->RIGHT_TRIM:LX/8tW;

    if-ne p1, v4, :cond_0

    .line 1413245
    iget v4, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->t:I

    if-ge v2, v4, :cond_7

    .line 1413246
    if-gt v3, v2, :cond_5

    iget v4, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->t:I

    sub-int/2addr v4, v3

    const/16 v5, 0xbb8

    if-ge v4, v5, :cond_7

    .line 1413247
    :cond_5
    iget v4, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->t:I

    sub-int v3, v4, v3

    .line 1413248
    sub-int v3, v2, v3

    .line 1413249
    invoke-static {v3, v0, v2}, LX/0yq;->a(III)I

    move-result v0

    .line 1413250
    :goto_1
    if-eq v0, v1, :cond_6

    .line 1413251
    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->l:Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;

    invoke-virtual {v1, v0}, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->setClipTimeMs(I)V

    .line 1413252
    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->i:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    invoke-virtual {v1, v0}, LX/8tJ;->a(I)V

    .line 1413253
    :cond_6
    iget v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->t:I

    if-le v2, v0, :cond_0

    .line 1413254
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->u:Z

    goto :goto_0

    :cond_7
    move v0, v1

    goto :goto_1
.end method

.method private b(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 1413255
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->b(Landroid/net/Uri;Landroid/net/Uri;)V

    .line 1413256
    return-void
.end method

.method private b(Landroid/net/Uri;Landroid/net/Uri;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1413257
    if-eqz p1, :cond_0

    invoke-virtual {p1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1413258
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getLoaderManager()LX/0k5;

    move-result-object v0

    invoke-virtual {v0, v1, v2, p0}, LX/0k5;->b(ILandroid/os/Bundle;LX/1jv;)LX/0k9;

    .line 1413259
    :goto_0
    return-void

    .line 1413260
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getLoaderManager()LX/0k5;

    move-result-object v0

    invoke-virtual {v0, v1, v2, p0}, LX/0k5;->a(ILandroid/os/Bundle;LX/1jv;)LX/0k9;

    goto :goto_0
.end method

.method private b(Z)V
    .locals 1

    .prologue
    .line 1413261
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->x:LX/8tT;

    if-eqz v0, :cond_0

    .line 1413262
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->x:LX/8tT;

    invoke-interface {v0, p1}, LX/8tT;->a(Z)V

    .line 1413263
    :cond_0
    return-void
.end method

.method private static b(LX/60x;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1413264
    iget v2, p0, LX/60x;->b:I

    iget v3, p0, LX/60x;->c:I

    if-le v2, v3, :cond_2

    .line 1413265
    iget v2, p0, LX/60x;->d:I

    if-eqz v2, :cond_0

    iget v2, p0, LX/60x;->d:I

    const/16 v3, 0xb4

    if-ne v2, v3, :cond_1

    :cond_0
    move v0, v1

    .line 1413266
    :cond_1
    :goto_0
    return v0

    :cond_2
    iget v2, p0, LX/60x;->d:I

    const/16 v3, 0x5a

    if-eq v2, v3, :cond_3

    iget v2, p0, LX/60x;->d:I

    const/16 v3, 0x10e

    if-ne v2, v3, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 1413267
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->h:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->setVisibility(I)V

    .line 1413268
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->h:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 1413269
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->i:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;->setVisibility(I)V

    .line 1413270
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 1413271
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->h:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->setVisibility(I)V

    .line 1413272
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->i:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;->setVisibility(I)V

    .line 1413273
    return-void
.end method

.method public static e(Lcom/facebook/videocodec/trimming/VideoPreviewFragment;I)V
    .locals 1

    .prologue
    .line 1413274
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->i:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    invoke-virtual {v0, p1}, LX/8tJ;->a(I)V

    .line 1413275
    return-void
.end method

.method private k()V
    .locals 2

    .prologue
    .line 1413276
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->r:LX/1Mv;

    if-eqz v0, :cond_0

    .line 1413277
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->r:LX/1Mv;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1Mv;->a(Z)V

    .line 1413278
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->r:LX/1Mv;

    .line 1413279
    :cond_0
    return-void
.end method

.method public static l(Lcom/facebook/videocodec/trimming/VideoPreviewFragment;)V
    .locals 5

    .prologue
    .line 1413138
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->o:LX/60x;

    if-eqz v0, :cond_0

    .line 1413139
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->b:LX/2Mj;

    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->o:LX/60x;

    iget-object v2, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->l:Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;

    .line 1413140
    iget v3, v2, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    move v2, v3

    .line 1413141
    iget-object v3, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->l:Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;

    .line 1413142
    iget v4, v3, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    move v3, v4

    .line 1413143
    invoke-virtual {v0, v1, v2, v3}, LX/2Mj;->a(LX/60x;II)LX/7Sw;

    move-result-object v0

    .line 1413144
    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->c:LX/8tH;

    iget v2, v0, LX/7Sw;->d:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, LX/8tH;->a(J)Ljava/lang/String;

    move-result-object v1

    .line 1413145
    iget v0, v0, LX/7Sw;->c:I

    int-to-long v2, v0

    const/4 v0, 0x1

    invoke-direct {p0, v2, v3, v0}, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->a(JZ)Ljava/lang/String;

    move-result-object v0

    .line 1413146
    iget-object v2, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->n:Lcom/facebook/videocodec/trimming/VideoTrimmingMetadataView;

    invoke-virtual {v2, v1}, Lcom/facebook/videocodec/trimming/VideoTrimmingMetadataView;->setDurationString(Ljava/lang/String;)V

    .line 1413147
    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->n:Lcom/facebook/videocodec/trimming/VideoTrimmingMetadataView;

    invoke-virtual {v1, v0}, Lcom/facebook/videocodec/trimming/VideoTrimmingMetadataView;->setSizeString(Ljava/lang/String;)V

    .line 1413148
    :cond_0
    return-void
.end method

.method private m()V
    .locals 4

    .prologue
    .line 1413149
    new-instance v0, LX/8tS;

    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->f:Ljava/util/concurrent/Executor;

    invoke-direct {v0, v1}, LX/8tS;-><init>(Ljava/util/concurrent/Executor;)V

    .line 1413150
    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->l:Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;

    invoke-virtual {v1}, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->getNumPreviewImages()I

    move-result v1

    .line 1413151
    iget-object v2, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->e:LX/0TD;

    new-instance v3, LX/8tQ;

    invoke-direct {v3, p0, v1, v0}, LX/8tQ;-><init>(Lcom/facebook/videocodec/trimming/VideoPreviewFragment;ILX/8tS;)V

    invoke-interface {v2, v3}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 1413152
    new-instance v2, LX/8tR;

    invoke-direct {v2, p0, v0}, LX/8tR;-><init>(Lcom/facebook/videocodec/trimming/VideoPreviewFragment;LX/8tS;)V

    .line 1413153
    invoke-static {v1, v2}, LX/1Mv;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)LX/1Mv;

    move-result-object v0

    .line 1413154
    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v3

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1413155
    iput-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->r:LX/1Mv;

    .line 1413156
    return-void
.end method

.method public static n(Lcom/facebook/videocodec/trimming/VideoPreviewFragment;)V
    .locals 3

    .prologue
    .line 1413017
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->l:Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;

    .line 1413018
    iget v1, v0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->y:I

    move v0, v1

    .line 1413019
    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->l:Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;

    .line 1413020
    iget v2, v1, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    move v1, v2

    .line 1413021
    if-lez v0, :cond_0

    if-eq v0, v1, :cond_0

    .line 1413022
    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->i:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    invoke-virtual {v1, v0}, LX/8tJ;->a(I)V

    .line 1413023
    iget-boolean v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->w:Z

    if-eqz v0, :cond_0

    .line 1413024
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->i:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    invoke-virtual {v0}, LX/8tJ;->b()V

    .line 1413025
    :cond_0
    return-void
.end method

.method public static o(Lcom/facebook/videocodec/trimming/VideoPreviewFragment;)V
    .locals 3

    .prologue
    .line 1413059
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->i:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    .line 1413060
    iget-object v1, v0, LX/8tJ;->g:LX/8tI;

    move-object v0, v1

    .line 1413061
    sget-object v1, LX/8tI;->PLAYING:LX/8tI;

    if-ne v0, v1, :cond_1

    .line 1413062
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->w:Z

    .line 1413063
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->i:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    invoke-virtual {v0}, LX/8tJ;->c()V

    .line 1413064
    :cond_0
    :goto_0
    return-void

    .line 1413065
    :cond_1
    sget-object v1, LX/8tI;->PAUSED:LX/8tI;

    if-ne v0, v1, :cond_0

    .line 1413066
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->l:Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;

    .line 1413067
    iget v1, v0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->y:I

    move v0, v1

    .line 1413068
    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->l:Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;

    .line 1413069
    iget v2, v1, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    move v1, v2

    .line 1413070
    iget-boolean v2, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->u:Z

    if-nez v2, :cond_2

    if-ne v0, v1, :cond_3

    .line 1413071
    :cond_2
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->l:Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;

    .line 1413072
    iget v1, v0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    move v0, v1

    .line 1413073
    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->i:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    invoke-virtual {v1, v0}, LX/8tJ;->a(I)V

    .line 1413074
    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->l:Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;

    invoke-virtual {v1, v0}, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->setClipTimeMs(I)V

    .line 1413075
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->w:Z

    .line 1413076
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->i:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    invoke-virtual {v0}, LX/8tJ;->b()V

    goto :goto_0
.end method

.method public static p(Lcom/facebook/videocodec/trimming/VideoPreviewFragment;)V
    .locals 1

    .prologue
    .line 1413056
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->w:Z

    .line 1413057
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->u:Z

    .line 1413058
    return-void
.end method

.method public static q(Lcom/facebook/videocodec/trimming/VideoPreviewFragment;)V
    .locals 1

    .prologue
    .line 1413053
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->u:Z

    .line 1413054
    invoke-static {p0}, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->r(Lcom/facebook/videocodec/trimming/VideoPreviewFragment;)V

    .line 1413055
    return-void
.end method

.method public static r(Lcom/facebook/videocodec/trimming/VideoPreviewFragment;)V
    .locals 2

    .prologue
    .line 1413047
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->i:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    .line 1413048
    iget-object v1, v0, LX/8tJ;->a:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->isPlaying()Z

    move-result v1

    move v0, v1

    .line 1413049
    if-eqz v0, :cond_0

    .line 1413050
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->i:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    new-instance v1, Lcom/facebook/videocodec/trimming/VideoPreviewFragment$5;

    invoke-direct {v1, p0}, Lcom/facebook/videocodec/trimming/VideoPreviewFragment$5;-><init>(Lcom/facebook/videocodec/trimming/VideoPreviewFragment;)V

    invoke-static {v0, v1}, LX/0vv;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 1413051
    :goto_0
    return-void

    .line 1413052
    :cond_0
    invoke-static {p0}, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->s(Lcom/facebook/videocodec/trimming/VideoPreviewFragment;)V

    goto :goto_0
.end method

.method public static s(Lcom/facebook/videocodec/trimming/VideoPreviewFragment;)V
    .locals 3

    .prologue
    .line 1413041
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->i:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    invoke-virtual {v0}, LX/8tJ;->getCurrentPosition()I

    move-result v0

    .line 1413042
    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->l:Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;

    .line 1413043
    iget v2, v1, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->y:I

    move v1, v2

    .line 1413044
    if-le v0, v1, :cond_0

    .line 1413045
    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->l:Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;

    invoke-virtual {v1, v0}, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->setClipTimeMs(I)V

    .line 1413046
    :cond_0
    return-void
.end method

.method public static t(Lcom/facebook/videocodec/trimming/VideoPreviewFragment;)V
    .locals 3

    .prologue
    .line 1413035
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->i:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    invoke-virtual {v0}, LX/8tJ;->getCurrentPosition()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->l:Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;

    .line 1413036
    iget v2, v1, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    move v1, v2

    .line 1413037
    if-lt v0, v1, :cond_0

    .line 1413038
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->i:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    invoke-virtual {v0}, LX/8tJ;->c()V

    .line 1413039
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->u:Z

    .line 1413040
    :cond_0
    return-void
.end method

.method public static u(Lcom/facebook/videocodec/trimming/VideoPreviewFragment;)V
    .locals 2

    .prologue
    .line 1413028
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->l:Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;

    .line 1413029
    iget v1, v0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    move v0, v1

    .line 1413030
    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->s:I

    .line 1413031
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->l:Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;

    .line 1413032
    iget v1, v0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    move v0, v1

    .line 1413033
    iput v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->t:I

    .line 1413034
    return-void
.end method


# virtual methods
.method public final a(I)LX/0k9;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/0k9",
            "<",
            "LX/8tL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1413027
    new-instance v0, LX/8tM;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->d:LX/2MV;

    iget-object v3, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->p:Landroid/net/Uri;

    invoke-direct {v0, v1, v2, v3}, LX/8tM;-><init>(Landroid/content/Context;LX/2MV;Landroid/net/Uri;)V

    return-object v0
.end method

.method public final a()V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1413026
    return-void
.end method

.method public final bridge synthetic a(LX/0k9;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1413016
    check-cast p2, LX/8tL;

    invoke-direct {p0, p2}, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->a(LX/8tL;)V

    return-void
.end method

.method public final a(LX/2Mj;)V
    .locals 0

    .prologue
    .line 1413077
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1413078
    iput-object p1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->b:LX/2Mj;

    .line 1413079
    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 1413080
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->p:Landroid/net/Uri;

    .line 1413081
    iput-object p1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->p:Landroid/net/Uri;

    .line 1413082
    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->p:Landroid/net/Uri;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1413083
    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->i:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    invoke-virtual {v1}, LX/8tJ;->c()V

    .line 1413084
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->w:Z

    .line 1413085
    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->i:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    .line 1413086
    iget-object v2, v1, LX/8tJ;->a:Landroid/widget/VideoView;

    invoke-virtual {v2, p1}, Landroid/widget/VideoView;->setVideoURI(Landroid/net/Uri;)V

    .line 1413087
    sget-object v2, LX/8tI;->PREPARING:LX/8tI;

    invoke-virtual {v1, v2}, LX/8tJ;->a(LX/8tI;)V

    .line 1413088
    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->p:Landroid/net/Uri;

    invoke-direct {p0, v0, v1}, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->a(Landroid/net/Uri;Landroid/net/Uri;)V

    .line 1413089
    :goto_0
    return-void

    .line 1413090
    :cond_0
    invoke-direct {p0}, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->k()V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1413091
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1413092
    if-eqz p1, :cond_0

    .line 1413093
    const-string v0, "previous_video_uri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->p:Landroid/net/Uri;

    .line 1413094
    :cond_0
    const-class v0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;

    invoke-static {v0, p0}, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1413095
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1413133
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 1413134
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1413135
    iput-boolean p1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->v:Z

    .line 1413136
    return-void

    .line 1413137
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1413096
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->l:Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;

    .line 1413097
    iget p0, v0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->w:I

    move v0, p0

    .line 1413098
    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1413099
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->l:Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;

    .line 1413100
    iget p0, v0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->x:I

    move v0, p0

    .line 1413101
    return v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3a5b97cf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1413102
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1413103
    if-eqz p1, :cond_0

    .line 1413104
    const-string v1, "is_video_playing"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->w:Z

    .line 1413105
    :cond_0
    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->p:Landroid/net/Uri;

    if-eqz v1, :cond_1

    .line 1413106
    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->p:Landroid/net/Uri;

    invoke-direct {p0, v1}, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->b(Landroid/net/Uri;)V

    .line 1413107
    :cond_1
    const/16 v1, 0x2b

    const v2, 0x57692f44

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x54fc2a0c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1413108
    const v1, 0x7f0315bb

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x56daa8e4

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6ee6da26

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1413109
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1413110
    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->i:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    invoke-virtual {v1}, LX/8tJ;->c()V

    .line 1413111
    const/16 v1, 0x2b

    const v2, -0x4151be1a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1413112
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1413113
    const-string v0, "is_video_playing"

    iget-boolean v1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->w:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1413114
    const-string v0, "previous_video_uri"

    iget-object v1, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->p:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1413115
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1413116
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1413117
    const v0, 0x7f0d3109

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->j:Landroid/view/View;

    .line 1413118
    const v0, 0x7f0d0abe

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/EmptyListViewItem;

    iput-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->h:Lcom/facebook/widget/listview/EmptyListViewItem;

    .line 1413119
    const v0, 0x7f0d310a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    iput-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->i:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    .line 1413120
    const v0, 0x7f0d310b

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->k:Landroid/widget/LinearLayout;

    .line 1413121
    const v0, 0x7f0d3102

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;

    iput-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->l:Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;

    .line 1413122
    const v0, 0x7f0d3103

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/videocodec/trimming/VideoTrimmingMetadataView;

    iput-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->m:Lcom/facebook/videocodec/trimming/VideoTrimmingMetadataView;

    .line 1413123
    const v0, 0x7f0d3104

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/videocodec/trimming/VideoTrimmingMetadataView;

    iput-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->n:Lcom/facebook/videocodec/trimming/VideoTrimmingMetadataView;

    .line 1413124
    iget-boolean v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->v:Z

    if-eqz v0, :cond_0

    .line 1413125
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1413126
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->m:Lcom/facebook/videocodec/trimming/VideoTrimmingMetadataView;

    invoke-virtual {v0, v1}, Lcom/facebook/videocodec/trimming/VideoTrimmingMetadataView;->setVisibility(I)V

    .line 1413127
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->n:Lcom/facebook/videocodec/trimming/VideoTrimmingMetadataView;

    invoke-virtual {v0, v1}, Lcom/facebook/videocodec/trimming/VideoTrimmingMetadataView;->setVisibility(I)V

    .line 1413128
    :cond_0
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->i:Lcom/facebook/videocodec/trimming/VideoTrimmingPreviewView;

    new-instance v1, LX/8tN;

    invoke-direct {v1, p0}, LX/8tN;-><init>(Lcom/facebook/videocodec/trimming/VideoPreviewFragment;)V

    .line 1413129
    iput-object v1, v0, LX/8tJ;->c:LX/8tN;

    .line 1413130
    iget-object v0, p0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->l:Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;

    new-instance v1, LX/8tP;

    invoke-direct {v1, p0}, LX/8tP;-><init>(Lcom/facebook/videocodec/trimming/VideoPreviewFragment;)V

    .line 1413131
    iput-object v1, v0, Lcom/facebook/videocodec/trimming/VideoTrimmingFilmStripView;->z:LX/8tO;

    .line 1413132
    return-void
.end method
