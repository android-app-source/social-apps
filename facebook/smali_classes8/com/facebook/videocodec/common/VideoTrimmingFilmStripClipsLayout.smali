.class public Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1412920
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 1412921
    invoke-direct {p0, p1}, Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;->a(Landroid/content/Context;)V

    .line 1412922
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1412917
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1412918
    invoke-direct {p0, p1}, Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;->a(Landroid/content/Context;)V

    .line 1412919
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1412914
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1412915
    invoke-direct {p0, p1}, Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;->a(Landroid/content/Context;)V

    .line 1412916
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1412904
    invoke-virtual {p0}, Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1412905
    const v1, 0x7f0b0c24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;->b:I

    .line 1412906
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;->a:Ljava/util/List;

    .line 1412907
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x7

    if-ge v0, v1, :cond_0

    .line 1412908
    new-instance v1, Landroid/widget/ImageView;

    invoke-direct {v1, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1412909
    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1412910
    invoke-virtual {p0, v1, v0}, Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;->addView(Landroid/view/View;I)V

    .line 1412911
    iget-object v2, p0, Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1412912
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1412913
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(ILandroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 1412876
    iget-object v0, p0, Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 1412877
    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1412878
    return-void
.end method

.method public getNumPreviewImages()I
    .locals 1

    .prologue
    .line 1412903
    const/4 v0, 0x7

    return v0
.end method

.method public final onLayout(ZIIII)V
    .locals 7

    .prologue
    .line 1412892
    invoke-virtual {p0}, Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;->getPaddingLeft()I

    move-result v0

    .line 1412893
    invoke-virtual {p0}, Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;->getRight()I

    invoke-virtual {p0}, Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;->getLeft()I

    invoke-virtual {p0}, Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;->getPaddingRight()I

    .line 1412894
    invoke-virtual {p0}, Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;->getPaddingTop()I

    move-result v2

    .line 1412895
    invoke-virtual {p0}, Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;->getBottom()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;->getTop()I

    move-result v3

    sub-int/2addr v1, v3

    invoke-virtual {p0}, Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;->getPaddingBottom()I

    move-result v3

    sub-int v3, v1, v3

    .line 1412896
    iget-object v1, p0, Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 1412897
    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v5

    .line 1412898
    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v1

    .line 1412899
    invoke-virtual {v0, v1, v2, v6, v3}, Landroid/widget/ImageView;->layout(IIII)V

    .line 1412900
    iget v0, p0, Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;->b:I

    add-int/2addr v0, v5

    add-int/2addr v0, v1

    move v1, v0

    .line 1412901
    goto :goto_0

    .line 1412902
    :cond_0
    return-void
.end method

.method public final onMeasure(II)V
    .locals 4

    .prologue
    .line 1412879
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomViewGroup;->onMeasure(II)V

    .line 1412880
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    .line 1412881
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 1412882
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    .line 1412883
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    .line 1412884
    iget v1, p0, Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;->b:I

    mul-int/lit8 v1, v1, 0x7

    .line 1412885
    invoke-virtual {p0}, Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;->getPaddingRight()I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    .line 1412886
    sub-int/2addr v0, v1

    .line 1412887
    div-int/lit8 v0, v0, 0x7

    iput v0, p0, Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;->c:I

    .line 1412888
    iget v0, p0, Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;->c:I

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 1412889
    iget-object v0, p0, Lcom/facebook/videocodec/common/VideoTrimmingFilmStripClipsLayout;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 1412890
    invoke-virtual {v0, v1, p2}, Landroid/widget/ImageView;->measure(II)V

    goto :goto_0

    .line 1412891
    :cond_0
    return-void
.end method
