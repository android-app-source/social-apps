.class public final Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1650544
    const-class v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel;

    new-instance v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1650545
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1650546
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1650547
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1650548
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1650549
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_a

    .line 1650550
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1650551
    :goto_0
    move v1, v2

    .line 1650552
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1650553
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1650554
    new-instance v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel;

    invoke-direct {v1}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel;-><init>()V

    .line 1650555
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1650556
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1650557
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1650558
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1650559
    :cond_0
    return-object v1

    .line 1650560
    :cond_1
    const-string p0, "unseen"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 1650561
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v4, v1

    move v1, v3

    .line 1650562
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, p0, :cond_8

    .line 1650563
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1650564
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1650565
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v9, :cond_2

    .line 1650566
    const-string p0, "__type__"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_3

    const-string p0, "__typename"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1650567
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v8

    goto :goto_1

    .line 1650568
    :cond_4
    const-string p0, "id"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1650569
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 1650570
    :cond_5
    const-string p0, "posts"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1650571
    invoke-static {p1, v0}, LX/AGX;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1650572
    :cond_6
    const-string p0, "thread_participants"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 1650573
    invoke-static {p1, v0}, LX/AGa;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1650574
    :cond_7
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1650575
    :cond_8
    const/4 v9, 0x5

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 1650576
    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1650577
    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1650578
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1650579
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1650580
    if-eqz v1, :cond_9

    .line 1650581
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v4}, LX/186;->a(IZ)V

    .line 1650582
    :cond_9
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_a
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    goto/16 :goto_1
.end method
