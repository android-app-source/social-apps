.class public final Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$LastPostsModel$LastPostsEdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x47fc8316
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$LastPostsModel$LastPostsEdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$LastPostsModel$LastPostsEdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1653840
    const-class v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$LastPostsModel$LastPostsEdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1653839
    const-class v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$LastPostsModel$LastPostsEdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1653837
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1653838
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1653831
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1653832
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$LastPostsModel$LastPostsEdgesModel;->a()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1653833
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1653834
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1653835
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1653836
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1653823
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1653824
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$LastPostsModel$LastPostsEdgesModel;->a()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1653825
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$LastPostsModel$LastPostsEdgesModel;->a()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;

    .line 1653826
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$LastPostsModel$LastPostsEdgesModel;->a()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1653827
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$LastPostsModel$LastPostsEdgesModel;

    .line 1653828
    iput-object v0, v1, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$LastPostsModel$LastPostsEdgesModel;->e:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;

    .line 1653829
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1653830
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1653816
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$LastPostsModel$LastPostsEdgesModel;->e:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$LastPostsModel$LastPostsEdgesModel;->e:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;

    .line 1653817
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$LastPostsModel$LastPostsEdgesModel;->e:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1653820
    new-instance v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$LastPostsModel$LastPostsEdgesModel;

    invoke-direct {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel$DirectInboxThreadsEdgesModel$DirectInboxThreadsEdgesNodeModel$LastPostsModel$LastPostsEdgesModel;-><init>()V

    .line 1653821
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1653822
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1653819
    const v0, -0x3fecfa23

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1653818
    const v0, -0x2f5bfe11

    return v0
.end method
