.class public final Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4859d157
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel$MessageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel$OwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1648818
    const-class v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1648817
    const-class v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1648815
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1648816
    return-void
.end method

.method private a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1648812
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1648813
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1648814
    :cond_0
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private a(Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel$MessageModel;)V
    .locals 3
    .param p1    # Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel$MessageModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1648806
    iput-object p1, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel;->f:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel$MessageModel;

    .line 1648807
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1648808
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1648809
    if-eqz v0, :cond_0

    .line 1648810
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 1648811
    :cond_0
    return-void
.end method

.method private j()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel$MessageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1648804
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel;->f:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel$MessageModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel$MessageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel$MessageModel;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel;->f:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel$MessageModel;

    .line 1648805
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel;->f:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel$MessageModel;

    return-object v0
.end method

.method private k()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel$OwnerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1648791
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel;->g:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel$OwnerModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel$OwnerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel$OwnerModel;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel;->g:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel$OwnerModel;

    .line 1648792
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel;->g:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel$OwnerModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1648794
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1648795
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1648796
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel;->j()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel$MessageModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1648797
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel;->k()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel$OwnerModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1648798
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1648799
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1648800
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1648801
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1648802
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1648803
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1648819
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1648820
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel;->j()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel$MessageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1648821
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel;->j()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel$MessageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel$MessageModel;

    .line 1648822
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel;->j()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel$MessageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1648823
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel;

    .line 1648824
    iput-object v0, v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel;->f:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel$MessageModel;

    .line 1648825
    :cond_0
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel;->k()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel$OwnerModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1648826
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel;->k()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel$OwnerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel$OwnerModel;

    .line 1648827
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel;->k()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel$OwnerModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1648828
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel;

    .line 1648829
    iput-object v0, v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel;->g:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel$OwnerModel;

    .line 1648830
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1648831
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1648793
    new-instance v0, LX/AFn;

    invoke-direct {v0, p1}, LX/AFn;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1648789
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1648790
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1648786
    const-string v0, "message"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1648787
    check-cast p2, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel$MessageModel;

    invoke-direct {p0, p2}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel;->a(Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel$MessageModel;)V

    .line 1648788
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1648785
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1648782
    new-instance v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel;

    invoke-direct {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel$PostMediaModel;-><init>()V

    .line 1648783
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1648784
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1648781
    const v0, -0x691c18e2

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1648780
    const v0, 0x46c7fc4

    return v0
.end method
