.class public final Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x77db9ca3
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$BasicPageInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1652727
    const-class v0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1652758
    const-class v0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1652756
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1652757
    return-void
.end method

.method private j()Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$BasicPageInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1652754
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel;->f:Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$BasicPageInfoModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$BasicPageInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$BasicPageInfoModel;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel;->f:Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$BasicPageInfoModel;

    .line 1652755
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel;->f:Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$BasicPageInfoModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1652746
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1652747
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1652748
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel;->j()Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$BasicPageInfoModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1652749
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1652750
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1652751
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1652752
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1652753
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1652759
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel$EdgesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel;->e:Ljava/util/List;

    .line 1652760
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1652733
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1652734
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1652735
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1652736
    if-eqz v1, :cond_2

    .line 1652737
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel;

    .line 1652738
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 1652739
    :goto_0
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel;->j()Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$BasicPageInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1652740
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel;->j()Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$BasicPageInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$BasicPageInfoModel;

    .line 1652741
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel;->j()Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$BasicPageInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1652742
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel;

    .line 1652743
    iput-object v0, v1, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel;->f:Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$BasicPageInfoModel;

    .line 1652744
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1652745
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1652730
    new-instance v0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel;

    invoke-direct {v0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel;-><init>()V

    .line 1652731
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1652732
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1652729
    const v0, 0x7a421793

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1652728
    const v0, 0x1647390a

    return v0
.end method
