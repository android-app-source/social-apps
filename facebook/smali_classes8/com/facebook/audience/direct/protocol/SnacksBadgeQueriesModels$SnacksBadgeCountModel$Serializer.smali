.class public final Lcom/facebook/audience/direct/protocol/SnacksBadgeQueriesModels$SnacksBadgeCountModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/audience/direct/protocol/SnacksBadgeQueriesModels$SnacksBadgeCountModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1653334
    const-class v0, Lcom/facebook/audience/direct/protocol/SnacksBadgeQueriesModels$SnacksBadgeCountModel;

    new-instance v1, Lcom/facebook/audience/direct/protocol/SnacksBadgeQueriesModels$SnacksBadgeCountModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/audience/direct/protocol/SnacksBadgeQueriesModels$SnacksBadgeCountModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1653335
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1653336
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/audience/direct/protocol/SnacksBadgeQueriesModels$SnacksBadgeCountModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1653337
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1653338
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x0

    .line 1653339
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1653340
    invoke-virtual {v1, v0, p2}, LX/15i;->g(II)I

    move-result p0

    .line 1653341
    if-eqz p0, :cond_0

    .line 1653342
    const-string p0, "__type__"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1653343
    invoke-static {v1, v0, p2, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1653344
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v0, p0, p2}, LX/15i;->a(III)I

    move-result p0

    .line 1653345
    if-eqz p0, :cond_1

    .line 1653346
    const-string p2, "backstage_inbox_unseen_count"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1653347
    invoke-virtual {p1, p0}, LX/0nX;->b(I)V

    .line 1653348
    :cond_1
    const/4 p0, 0x2

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1653349
    if-eqz p0, :cond_2

    .line 1653350
    const-string p2, "id"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1653351
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1653352
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1653353
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1653354
    check-cast p1, Lcom/facebook/audience/direct/protocol/SnacksBadgeQueriesModels$SnacksBadgeCountModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/audience/direct/protocol/SnacksBadgeQueriesModels$SnacksBadgeCountModel$Serializer;->a(Lcom/facebook/audience/direct/protocol/SnacksBadgeQueriesModels$SnacksBadgeCountModel;LX/0nX;LX/0my;)V

    return-void
.end method
