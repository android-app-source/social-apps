.class public final Lcom/facebook/audience/direct/protocol/SnacksBadgeQueriesModels$SnacksBadgeCountModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x63a2b5c8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/direct/protocol/SnacksBadgeQueriesModels$SnacksBadgeCountModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/direct/protocol/SnacksBadgeQueriesModels$SnacksBadgeCountModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1653381
    const-class v0, Lcom/facebook/audience/direct/protocol/SnacksBadgeQueriesModels$SnacksBadgeCountModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1653389
    const-class v0, Lcom/facebook/audience/direct/protocol/SnacksBadgeQueriesModels$SnacksBadgeCountModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1653387
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1653388
    return-void
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1653384
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksBadgeQueriesModels$SnacksBadgeCountModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1653385
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksBadgeQueriesModels$SnacksBadgeCountModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1653386
    :cond_0
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksBadgeQueriesModels$SnacksBadgeCountModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1653382
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksBadgeQueriesModels$SnacksBadgeCountModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksBadgeQueriesModels$SnacksBadgeCountModel;->g:Ljava/lang/String;

    .line 1653383
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksBadgeQueriesModels$SnacksBadgeCountModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1653355
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1653356
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/SnacksBadgeQueriesModels$SnacksBadgeCountModel;->k()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1653357
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/SnacksBadgeQueriesModels$SnacksBadgeCountModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1653358
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1653359
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1653360
    const/4 v0, 0x1

    iget v2, p0, Lcom/facebook/audience/direct/protocol/SnacksBadgeQueriesModels$SnacksBadgeCountModel;->f:I

    invoke-virtual {p1, v0, v2, v3}, LX/186;->a(III)V

    .line 1653361
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1653362
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1653363
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1653378
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1653379
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1653380
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1653377
    new-instance v0, LX/AGv;

    invoke-direct {v0, p1}, LX/AGv;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1653390
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/SnacksBadgeQueriesModels$SnacksBadgeCountModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1653374
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1653375
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/audience/direct/protocol/SnacksBadgeQueriesModels$SnacksBadgeCountModel;->f:I

    .line 1653376
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1653372
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1653373
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1653371
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1653368
    new-instance v0, Lcom/facebook/audience/direct/protocol/SnacksBadgeQueriesModels$SnacksBadgeCountModel;

    invoke-direct {v0}, Lcom/facebook/audience/direct/protocol/SnacksBadgeQueriesModels$SnacksBadgeCountModel;-><init>()V

    .line 1653369
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1653370
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1653367
    const v0, -0x41f3f0a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1653366
    const v0, 0x3c2b9d5

    return v0
.end method

.method public final j()I
    .locals 2

    .prologue
    .line 1653364
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1653365
    iget v0, p0, Lcom/facebook/audience/direct/protocol/SnacksBadgeQueriesModels$SnacksBadgeCountModel;->f:I

    return v0
.end method
