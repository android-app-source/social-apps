.class public final Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x38aa6d6d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1656294
    const-class v0, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1656293
    const-class v0, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1656291
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1656292
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1656279
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1656280
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1656281
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;->k()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, -0x49927c33

    invoke-static {v2, v1, v3}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1656282
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1656283
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;->m()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, 0x2c0bfd70

    invoke-static {v4, v3, v5}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$DraculaImplementation;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1656284
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1656285
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1656286
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1656287
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1656288
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1656289
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1656290
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1656263
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1656264
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1656265
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x49927c33

    invoke-static {v2, v0, v3}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1656266
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1656267
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;

    .line 1656268
    iput v3, v0, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;->f:I

    move-object v1, v0

    .line 1656269
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1656270
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x2c0bfd70

    invoke-static {v2, v0, v3}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1656271
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1656272
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;

    .line 1656273
    iput v3, v0, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;->h:I

    move-object v1, v0

    .line 1656274
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1656275
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 1656276
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1656277
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_2
    move-object p0, v1

    .line 1656278
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1656262
    new-instance v0, LX/AHj;

    invoke-direct {v0, p1}, LX/AHj;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1656261
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1656257
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1656258
    const/4 v0, 0x1

    const v1, -0x49927c33

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;->f:I

    .line 1656259
    const/4 v0, 0x3

    const v1, 0x2c0bfd70

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;->h:I

    .line 1656260
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1656295
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1656296
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1656256
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1656253
    new-instance v0, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;-><init>()V

    .line 1656254
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1656255
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1656252
    const v0, 0x7cea53bf

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1656251
    const v0, 0x285feb

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1656249
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    .line 1656250
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getLowresProfile"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1656247
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1656248
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1656245
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    .line 1656246
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfilePicture"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1656243
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1656244
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/audience/direct/protocol/SnacksSubscriptionsModels$SnacksThreadSubscriptionModel$ThreadModel$ThreadParticipantsModel$EdgesModel$NodeModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
