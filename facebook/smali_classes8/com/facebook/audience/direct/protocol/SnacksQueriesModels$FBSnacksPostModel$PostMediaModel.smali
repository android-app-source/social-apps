.class public final Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6f993b90
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:J

.field private g:I

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$MessageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$OwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:I

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:I

.field private o:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1654451
    const-class v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1654450
    const-class v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1654448
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1654449
    return-void
.end method

.method private a(Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$MessageModel;)V
    .locals 3
    .param p1    # Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$MessageModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1654442
    iput-object p1, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->j:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$MessageModel;

    .line 1654443
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1654444
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1654445
    if-eqz v0, :cond_0

    .line 1654446
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 1654447
    :cond_0
    return-void
.end method

.method private n()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1654439
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1654440
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1654441
    :cond_0
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private o()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$MessageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1654437
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->j:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$MessageModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$MessageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$MessageModel;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->j:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$MessageModel;

    .line 1654438
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->j:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$MessageModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 1654416
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1654417
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1654418
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, 0x32524de3

    invoke-static {v2, v1, v3}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1654419
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->k()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, -0x3c940a79

    invoke-static {v2, v1, v3}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1654420
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->o()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$MessageModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1654421
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->l()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$OwnerModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1654422
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1654423
    const/16 v1, 0xb

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1654424
    invoke-virtual {p1, v11, v0}, LX/186;->b(II)V

    .line 1654425
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->f:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1654426
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->g:I

    invoke-virtual {p1, v0, v1, v11}, LX/186;->a(III)V

    .line 1654427
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1654428
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1654429
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1654430
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1654431
    const/4 v0, 0x7

    iget v1, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->l:I

    invoke-virtual {p1, v0, v1, v11}, LX/186;->a(III)V

    .line 1654432
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 1654433
    const/16 v0, 0x9

    iget v1, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->n:I

    invoke-virtual {p1, v0, v1, v11}, LX/186;->a(III)V

    .line 1654434
    const/16 v0, 0xa

    iget v1, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->o:I

    invoke-virtual {p1, v0, v1, v11}, LX/186;->a(III)V

    .line 1654435
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1654436
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()J
    .locals 2

    .prologue
    .line 1654414
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1654415
    iget-wide v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->f:J

    return-wide v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1654388
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1654389
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1654390
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x32524de3

    invoke-static {v2, v0, v3}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1654391
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1654392
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;

    .line 1654393
    iput v3, v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->h:I

    move-object v1, v0

    .line 1654394
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1654395
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x3c940a79

    invoke-static {v2, v0, v3}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1654396
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1654397
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;

    .line 1654398
    iput v3, v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->i:I

    move-object v1, v0

    .line 1654399
    :cond_1
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->o()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$MessageModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1654400
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->o()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$MessageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$MessageModel;

    .line 1654401
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->o()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$MessageModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1654402
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;

    .line 1654403
    iput-object v0, v1, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->j:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$MessageModel;

    .line 1654404
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->l()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$OwnerModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1654405
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->l()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$OwnerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$OwnerModel;

    .line 1654406
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->l()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$OwnerModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1654407
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;

    .line 1654408
    iput-object v0, v1, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->k:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$OwnerModel;

    .line 1654409
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1654410
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    .line 1654411
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1654412
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_4
    move-object p0, v1

    .line 1654413
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1654387
    new-instance v0, LX/AH3;

    invoke-direct {v0, p1}, LX/AH3;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1654452
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1654453
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->f:J

    .line 1654454
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->g:I

    .line 1654455
    const/4 v0, 0x3

    const v1, 0x32524de3

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->h:I

    .line 1654456
    const/4 v0, 0x4

    const v1, -0x3c940a79

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->i:I

    .line 1654457
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->l:I

    .line 1654458
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->n:I

    .line 1654459
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->o:I

    .line 1654460
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1654368
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1654369
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1654372
    const-string v0, "message"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1654373
    check-cast p2, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$MessageModel;

    invoke-direct {p0, p2}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->a(Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$MessageModel;)V

    .line 1654374
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1654375
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1654376
    new-instance v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;

    invoke-direct {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;-><init>()V

    .line 1654377
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1654378
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1654379
    const v0, -0xddfa566

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1654380
    const v0, 0x46c7fc4

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1654370
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1654371
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final k()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getLowres"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1654381
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1654382
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->i:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final l()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$OwnerModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOwner"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1654383
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->k:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$OwnerModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$OwnerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$OwnerModel;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->k:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$OwnerModel;

    .line 1654384
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->k:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel$OwnerModel;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1654385
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->m:Ljava/lang/String;

    .line 1654386
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;->m:Ljava/lang/String;

    return-object v0
.end method
