.class public final Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1649598
    const-class v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel;

    new-instance v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1649599
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1649669
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 1649600
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1649601
    const/4 v2, 0x0

    .line 1649602
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_b

    .line 1649603
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1649604
    :goto_0
    move v1, v2

    .line 1649605
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1649606
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1649607
    new-instance v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel;

    invoke-direct {v1}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel;-><init>()V

    .line 1649608
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1649609
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1649610
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1649611
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1649612
    :cond_0
    return-object v1

    .line 1649613
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1649614
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_a

    .line 1649615
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1649616
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1649617
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, p0, :cond_2

    if-eqz v9, :cond_2

    .line 1649618
    const-string v10, "__type__"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_3

    const-string v10, "__typename"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1649619
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v8

    goto :goto_1

    .line 1649620
    :cond_4
    const-string v10, "backstage"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 1649621
    invoke-static {p1, v0}, LX/AGJ;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1649622
    :cond_5
    const-string v10, "backstage_audience_mode"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 1649623
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 1649624
    :cond_6
    const-string v10, "backstage_friends"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 1649625
    invoke-static {p1, v0}, LX/AGH;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1649626
    :cond_7
    const-string v10, "lowres_profile"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 1649627
    const/4 v9, 0x0

    .line 1649628
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v10, :cond_f

    .line 1649629
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1649630
    :goto_2
    move v4, v9

    .line 1649631
    goto :goto_1

    .line 1649632
    :cond_8
    const-string v10, "name"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 1649633
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 1649634
    :cond_9
    const-string v10, "profile_picture"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1649635
    const/4 v9, 0x0

    .line 1649636
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v10, :cond_13

    .line 1649637
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1649638
    :goto_3
    move v1, v9

    .line 1649639
    goto/16 :goto_1

    .line 1649640
    :cond_a
    const/4 v9, 0x7

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 1649641
    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1649642
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1649643
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1649644
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1649645
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1649646
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1649647
    const/4 v2, 0x6

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1649648
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_b
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    goto/16 :goto_1

    .line 1649649
    :cond_c
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1649650
    :cond_d
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, p0, :cond_e

    .line 1649651
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1649652
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1649653
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_d

    if-eqz v10, :cond_d

    .line 1649654
    const-string p0, "uri"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_c

    .line 1649655
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_4

    .line 1649656
    :cond_e
    const/4 v10, 0x1

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 1649657
    invoke-virtual {v0, v9, v4}, LX/186;->b(II)V

    .line 1649658
    invoke-virtual {v0}, LX/186;->d()I

    move-result v9

    goto/16 :goto_2

    :cond_f
    move v4, v9

    goto :goto_4

    .line 1649659
    :cond_10
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1649660
    :cond_11
    :goto_5
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, p0, :cond_12

    .line 1649661
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1649662
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1649663
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_11

    if-eqz v10, :cond_11

    .line 1649664
    const-string p0, "uri"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_10

    .line 1649665
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_5

    .line 1649666
    :cond_12
    const/4 v10, 0x1

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 1649667
    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 1649668
    invoke-virtual {v0}, LX/186;->d()I

    move-result v9

    goto/16 :goto_3

    :cond_13
    move v1, v9

    goto :goto_5
.end method
