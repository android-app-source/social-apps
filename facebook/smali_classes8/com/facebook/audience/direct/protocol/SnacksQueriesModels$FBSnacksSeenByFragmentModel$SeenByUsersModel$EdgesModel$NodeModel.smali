.class public final Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xc22f5dc
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1654636
    const-class v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1654635
    const-class v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1654633
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1654634
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1654631
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    .line 1654632
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfilePicture"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1654629
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1654630
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1654619
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1654620
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1654621
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1654622
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;->l()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, -0x700bd4fb

    invoke-static {v3, v2, v4}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1654623
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1654624
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1654625
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1654626
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1654627
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1654628
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1654609
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1654610
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1654611
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x700bd4fb

    invoke-static {v2, v0, v3}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1654612
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1654613
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;

    .line 1654614
    iput v3, v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;->g:I

    .line 1654615
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1654616
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 1654617
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 1654618
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1654637
    new-instance v0, LX/AH5;

    invoke-direct {v0, p1}, LX/AH5;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1654608
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1654605
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1654606
    const/4 v0, 0x2

    const v1, -0x700bd4fb

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;->g:I

    .line 1654607
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1654603
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1654604
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1654595
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1654600
    new-instance v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;-><init>()V

    .line 1654601
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1654602
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1654599
    const v0, 0x36d202c9

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1654598
    const v0, 0x285feb

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1654596
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    .line 1654597
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    return-object v0
.end method
