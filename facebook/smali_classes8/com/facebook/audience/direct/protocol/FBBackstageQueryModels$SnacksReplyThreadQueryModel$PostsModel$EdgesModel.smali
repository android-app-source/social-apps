.class public final Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x69949f9e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1650642
    const-class v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1650641
    const-class v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1650639
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1650640
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1650633
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1650634
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1650635
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1650636
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1650637
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1650638
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1650625
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1650626
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1650627
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;

    .line 1650628
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1650629
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;

    .line 1650630
    iput-object v0, v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;->e:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;

    .line 1650631
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1650632
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1650623
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;->e:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;->e:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;

    .line 1650624
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;->e:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1650618
    new-instance v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$SnacksReplyThreadQueryModel$PostsModel$EdgesModel;-><init>()V

    .line 1650619
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1650620
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1650622
    const v0, 0x1da257ad

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1650621
    const v0, -0x2f5bfe11

    return v0
.end method
