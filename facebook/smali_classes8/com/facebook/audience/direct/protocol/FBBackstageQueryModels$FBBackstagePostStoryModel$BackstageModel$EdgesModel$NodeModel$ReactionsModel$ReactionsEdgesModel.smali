.class public final Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4338d6f6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1648871
    const-class v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1648872
    const-class v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1648873
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1648874
    return-void
.end method

.method private a()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1648875
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel;->e:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel;->e:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel;

    .line 1648876
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel;->e:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1648877
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1648878
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel;->a()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1648879
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1648880
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1648881
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1648882
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1648883
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1648884
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel;->a()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1648885
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel;->a()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel;

    .line 1648886
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel;->a()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1648887
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel;

    .line 1648888
    iput-object v0, v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel;->e:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel$ReactionsEdgesNodeModel;

    .line 1648889
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1648890
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1648891
    new-instance v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel;

    invoke-direct {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel$ReactionsEdgesModel;-><init>()V

    .line 1648892
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1648893
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1648894
    const v0, -0x2533215d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1648895
    const v0, -0xefee6e8

    return v0
.end method
