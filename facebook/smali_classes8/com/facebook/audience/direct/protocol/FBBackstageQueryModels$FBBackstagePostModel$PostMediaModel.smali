.class public final Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x539c00f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:J

.field private g:I

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$MessageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$OwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:I

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:I

.field private o:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1648504
    const-class v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1648516
    const-class v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1648514
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1648515
    return-void
.end method

.method private a(Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$MessageModel;)V
    .locals 3
    .param p1    # Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$MessageModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1648508
    iput-object p1, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->j:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$MessageModel;

    .line 1648509
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1648510
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1648511
    if-eqz v0, :cond_0

    .line 1648512
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 1648513
    :cond_0
    return-void
.end method

.method private n()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1648505
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1648506
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1648507
    :cond_0
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private o()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getLowres"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1648433
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1648434
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->i:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 1648483
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1648484
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1648485
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, 0x22319076

    invoke-static {v2, v1, v3}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1648486
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->o()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, 0x6e6ab5bc

    invoke-static {v2, v1, v3}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1648487
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->k()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$MessageModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1648488
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->l()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$OwnerModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1648489
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1648490
    const/16 v1, 0xb

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1648491
    invoke-virtual {p1, v11, v0}, LX/186;->b(II)V

    .line 1648492
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->f:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1648493
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->g:I

    invoke-virtual {p1, v0, v1, v11}, LX/186;->a(III)V

    .line 1648494
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1648495
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1648496
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1648497
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1648498
    const/4 v0, 0x7

    iget v1, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->l:I

    invoke-virtual {p1, v0, v1, v11}, LX/186;->a(III)V

    .line 1648499
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 1648500
    const/16 v0, 0x9

    iget v1, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->n:I

    invoke-virtual {p1, v0, v1, v11}, LX/186;->a(III)V

    .line 1648501
    const/16 v0, 0xa

    iget v1, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->o:I

    invoke-virtual {p1, v0, v1, v11}, LX/186;->a(III)V

    .line 1648502
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1648503
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()J
    .locals 2

    .prologue
    .line 1648481
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1648482
    iget-wide v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->f:J

    return-wide v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1648455
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1648456
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1648457
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x22319076

    invoke-static {v2, v0, v3}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1648458
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1648459
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    .line 1648460
    iput v3, v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->h:I

    move-object v1, v0

    .line 1648461
    :cond_0
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->o()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1648462
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->o()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x6e6ab5bc

    invoke-static {v2, v0, v3}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1648463
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->o()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1648464
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    .line 1648465
    iput v3, v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->i:I

    move-object v1, v0

    .line 1648466
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->k()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$MessageModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1648467
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->k()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$MessageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$MessageModel;

    .line 1648468
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->k()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$MessageModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1648469
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    .line 1648470
    iput-object v0, v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->j:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$MessageModel;

    .line 1648471
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->l()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$OwnerModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1648472
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->l()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$OwnerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$OwnerModel;

    .line 1648473
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->l()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$OwnerModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1648474
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    .line 1648475
    iput-object v0, v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->k:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$OwnerModel;

    .line 1648476
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1648477
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    .line 1648478
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1648479
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_4
    move-object p0, v1

    .line 1648480
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1648454
    new-instance v0, LX/AFl;

    invoke-direct {v0, p1}, LX/AFl;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1648517
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1648518
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->f:J

    .line 1648519
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->g:I

    .line 1648520
    const/4 v0, 0x3

    const v1, 0x22319076

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->h:I

    .line 1648521
    const/4 v0, 0x4

    const v1, 0x6e6ab5bc

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->i:I

    .line 1648522
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->l:I

    .line 1648523
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->n:I

    .line 1648524
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->o:I

    .line 1648525
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1648452
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1648453
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1648449
    const-string v0, "message"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1648450
    check-cast p2, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$MessageModel;

    invoke-direct {p0, p2}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->a(Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$MessageModel;)V

    .line 1648451
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1648448
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1648445
    new-instance v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    invoke-direct {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;-><init>()V

    .line 1648446
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1648447
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1648444
    const v0, -0x2cd1e9bd

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1648443
    const v0, 0x46c7fc4

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1648441
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1648442
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final k()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$MessageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1648439
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->j:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$MessageModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$MessageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$MessageModel;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->j:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$MessageModel;

    .line 1648440
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->j:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$MessageModel;

    return-object v0
.end method

.method public final l()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$OwnerModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOwner"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1648437
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->k:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$OwnerModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$OwnerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$OwnerModel;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->k:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$OwnerModel;

    .line 1648438
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->k:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel$OwnerModel;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1648435
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->m:Ljava/lang/String;

    .line 1648436
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;->m:Ljava/lang/String;

    return-object v0
.end method
