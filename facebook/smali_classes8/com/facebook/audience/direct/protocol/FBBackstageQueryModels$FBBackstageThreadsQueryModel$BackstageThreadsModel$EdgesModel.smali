.class public final Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6a1ccb5
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1650429
    const-class v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1650405
    const-class v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1650406
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1650407
    return-void
.end method

.method private a()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1650408
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel;->e:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel;->e:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel;

    .line 1650409
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel;->e:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1650410
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1650411
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1650412
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1650413
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1650414
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1650415
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1650416
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1650417
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1650418
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel;

    .line 1650419
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1650420
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel;

    .line 1650421
    iput-object v0, v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel;->e:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel;

    .line 1650422
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1650423
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1650424
    new-instance v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel;-><init>()V

    .line 1650425
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1650426
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1650427
    const v0, -0x64376077

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1650428
    const v0, -0x1739a50b

    return v0
.end method
