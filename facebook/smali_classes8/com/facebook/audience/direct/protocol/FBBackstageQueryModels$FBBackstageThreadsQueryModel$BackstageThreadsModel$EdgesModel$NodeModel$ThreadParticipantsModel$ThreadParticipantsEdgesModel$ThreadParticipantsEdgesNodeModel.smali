.class public final Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel$ThreadParticipantsModel$ThreadParticipantsEdgesModel$ThreadParticipantsEdgesNodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3271ba90
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel$ThreadParticipantsModel$ThreadParticipantsEdgesModel$ThreadParticipantsEdgesNodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel$ThreadParticipantsModel$ThreadParticipantsEdgesModel$ThreadParticipantsEdgesNodeModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1650308
    const-class v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel$ThreadParticipantsModel$ThreadParticipantsEdgesModel$ThreadParticipantsEdgesNodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1650307
    const-class v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel$ThreadParticipantsModel$ThreadParticipantsEdgesModel$ThreadParticipantsEdgesNodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1650305
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1650306
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1650303
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel$ThreadParticipantsModel$ThreadParticipantsEdgesModel$ThreadParticipantsEdgesNodeModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel$ThreadParticipantsModel$ThreadParticipantsEdgesModel$ThreadParticipantsEdgesNodeModel;->e:Ljava/lang/String;

    .line 1650304
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel$ThreadParticipantsModel$ThreadParticipantsEdgesModel$ThreadParticipantsEdgesNodeModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getLowresProfile"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1650301
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1650302
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel$ThreadParticipantsModel$ThreadParticipantsEdgesModel$ThreadParticipantsEdgesNodeModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1650299
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel$ThreadParticipantsModel$ThreadParticipantsEdgesModel$ThreadParticipantsEdgesNodeModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel$ThreadParticipantsModel$ThreadParticipantsEdgesModel$ThreadParticipantsEdgesNodeModel;->g:Ljava/lang/String;

    .line 1650300
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel$ThreadParticipantsModel$ThreadParticipantsEdgesModel$ThreadParticipantsEdgesNodeModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private m()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfilePicture"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1650297
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1650298
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel$ThreadParticipantsModel$ThreadParticipantsEdgesModel$ThreadParticipantsEdgesNodeModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1650285
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1650286
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel$ThreadParticipantsModel$ThreadParticipantsEdgesModel$ThreadParticipantsEdgesNodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1650287
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel$ThreadParticipantsModel$ThreadParticipantsEdgesModel$ThreadParticipantsEdgesNodeModel;->k()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, 0x6c57886c

    invoke-static {v2, v1, v3}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1650288
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel$ThreadParticipantsModel$ThreadParticipantsEdgesModel$ThreadParticipantsEdgesNodeModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1650289
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel$ThreadParticipantsModel$ThreadParticipantsEdgesModel$ThreadParticipantsEdgesNodeModel;->m()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, -0x2b8fdefb

    invoke-static {v4, v3, v5}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1650290
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1650291
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1650292
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1650293
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1650294
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1650295
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1650296
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1650269
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1650270
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel$ThreadParticipantsModel$ThreadParticipantsEdgesModel$ThreadParticipantsEdgesNodeModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1650271
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel$ThreadParticipantsModel$ThreadParticipantsEdgesModel$ThreadParticipantsEdgesNodeModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x6c57886c

    invoke-static {v2, v0, v3}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1650272
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel$ThreadParticipantsModel$ThreadParticipantsEdgesModel$ThreadParticipantsEdgesNodeModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1650273
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel$ThreadParticipantsModel$ThreadParticipantsEdgesModel$ThreadParticipantsEdgesNodeModel;

    .line 1650274
    iput v3, v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel$ThreadParticipantsModel$ThreadParticipantsEdgesModel$ThreadParticipantsEdgesNodeModel;->f:I

    move-object v1, v0

    .line 1650275
    :cond_0
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel$ThreadParticipantsModel$ThreadParticipantsEdgesModel$ThreadParticipantsEdgesNodeModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1650276
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel$ThreadParticipantsModel$ThreadParticipantsEdgesModel$ThreadParticipantsEdgesNodeModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x2b8fdefb

    invoke-static {v2, v0, v3}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1650277
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel$ThreadParticipantsModel$ThreadParticipantsEdgesModel$ThreadParticipantsEdgesNodeModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1650278
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel$ThreadParticipantsModel$ThreadParticipantsEdgesModel$ThreadParticipantsEdgesNodeModel;

    .line 1650279
    iput v3, v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel$ThreadParticipantsModel$ThreadParticipantsEdgesModel$ThreadParticipantsEdgesNodeModel;->h:I

    move-object v1, v0

    .line 1650280
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1650281
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 1650282
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1650283
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_2
    move-object p0, v1

    .line 1650284
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1650255
    new-instance v0, LX/AFt;

    invoke-direct {v0, p1}, LX/AFt;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1650268
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel$ThreadParticipantsModel$ThreadParticipantsEdgesModel$ThreadParticipantsEdgesNodeModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1650264
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1650265
    const/4 v0, 0x1

    const v1, 0x6c57886c

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel$ThreadParticipantsModel$ThreadParticipantsEdgesModel$ThreadParticipantsEdgesNodeModel;->f:I

    .line 1650266
    const/4 v0, 0x3

    const v1, -0x2b8fdefb

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel$ThreadParticipantsModel$ThreadParticipantsEdgesModel$ThreadParticipantsEdgesNodeModel;->h:I

    .line 1650267
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1650262
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1650263
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1650261
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1650258
    new-instance v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel$ThreadParticipantsModel$ThreadParticipantsEdgesModel$ThreadParticipantsEdgesNodeModel;

    invoke-direct {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageThreadsQueryModel$BackstageThreadsModel$EdgesModel$NodeModel$ThreadParticipantsModel$ThreadParticipantsEdgesModel$ThreadParticipantsEdgesNodeModel;-><init>()V

    .line 1650259
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1650260
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1650257
    const v0, 0x65cbe953

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1650256
    const v0, 0x285feb

    return v0
.end method
