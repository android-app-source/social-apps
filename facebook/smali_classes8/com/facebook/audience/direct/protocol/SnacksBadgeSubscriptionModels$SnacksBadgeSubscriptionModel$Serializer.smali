.class public final Lcom/facebook/audience/direct/protocol/SnacksBadgeSubscriptionModels$SnacksBadgeSubscriptionModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/audience/direct/protocol/SnacksBadgeSubscriptionModels$SnacksBadgeSubscriptionModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1653422
    const-class v0, Lcom/facebook/audience/direct/protocol/SnacksBadgeSubscriptionModels$SnacksBadgeSubscriptionModel;

    new-instance v1, Lcom/facebook/audience/direct/protocol/SnacksBadgeSubscriptionModels$SnacksBadgeSubscriptionModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/audience/direct/protocol/SnacksBadgeSubscriptionModels$SnacksBadgeSubscriptionModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1653423
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1653424
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/audience/direct/protocol/SnacksBadgeSubscriptionModels$SnacksBadgeSubscriptionModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1653425
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1653426
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1653427
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1653428
    invoke-virtual {v1, v0, p0, p0}, LX/15i;->a(III)I

    move-result p0

    .line 1653429
    if-eqz p0, :cond_0

    .line 1653430
    const-string p2, "badge_count"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1653431
    invoke-virtual {p1, p0}, LX/0nX;->b(I)V

    .line 1653432
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1653433
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1653434
    check-cast p1, Lcom/facebook/audience/direct/protocol/SnacksBadgeSubscriptionModels$SnacksBadgeSubscriptionModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/audience/direct/protocol/SnacksBadgeSubscriptionModels$SnacksBadgeSubscriptionModel$Serializer;->a(Lcom/facebook/audience/direct/protocol/SnacksBadgeSubscriptionModels$SnacksBadgeSubscriptionModel;LX/0nX;LX/0my;)V

    return-void
.end method
