.class public final Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1653557
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1653558
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1653555
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1653556
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 1653501
    if-nez p1, :cond_0

    .line 1653502
    :goto_0
    return v0

    .line 1653503
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1653504
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1653505
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1653506
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1653507
    invoke-virtual {p0, p1, v6}, LX/15i;->b(II)Z

    move-result v2

    .line 1653508
    invoke-virtual {p0, p1, v7}, LX/15i;->b(II)Z

    move-result v3

    .line 1653509
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1653510
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1653511
    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1653512
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1653513
    invoke-virtual {p3, v6, v2}, LX/186;->a(IZ)V

    .line 1653514
    invoke-virtual {p3, v7, v3}, LX/186;->a(IZ)V

    .line 1653515
    invoke-virtual {p3, v8, v4}, LX/186;->b(II)V

    .line 1653516
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1653517
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1653518
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1653519
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1653520
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1653521
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1653522
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1653523
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1653524
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1653525
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1653526
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1653527
    :sswitch_3
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 1653528
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1653529
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1653530
    invoke-virtual {p0, p1, v7, v0}, LX/15i;->a(III)I

    move-result v3

    .line 1653531
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 1653532
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 1653533
    invoke-virtual {p3, v6, v2}, LX/186;->b(II)V

    .line 1653534
    invoke-virtual {p3, v7, v3, v0}, LX/186;->a(III)V

    .line 1653535
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1653536
    :sswitch_4
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 1653537
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1653538
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1653539
    invoke-virtual {p0, p1, v7, v0}, LX/15i;->a(III)I

    move-result v3

    .line 1653540
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 1653541
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 1653542
    invoke-virtual {p3, v6, v2}, LX/186;->b(II)V

    .line 1653543
    invoke-virtual {p3, v7, v3, v0}, LX/186;->a(III)V

    .line 1653544
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1653545
    :sswitch_5
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1653546
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1653547
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1653548
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1653549
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1653550
    :sswitch_6
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1653551
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1653552
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1653553
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1653554
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x700bd4fb -> :sswitch_6
        -0x3c940a79 -> :sswitch_4
        0xa0633db -> :sswitch_2
        0x102c5bc0 -> :sswitch_1
        0x32524de3 -> :sswitch_3
        0x385bd82f -> :sswitch_5
        0x71b74196 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1653500
    new-instance v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1653497
    sparse-switch p0, :sswitch_data_0

    .line 1653498
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1653499
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x700bd4fb -> :sswitch_0
        -0x3c940a79 -> :sswitch_0
        0xa0633db -> :sswitch_0
        0x102c5bc0 -> :sswitch_0
        0x32524de3 -> :sswitch_0
        0x385bd82f -> :sswitch_0
        0x71b74196 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1653496
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1653463
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$DraculaImplementation;->b(I)V

    .line 1653464
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1653491
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1653492
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1653493
    :cond_0
    iput-object p1, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$DraculaImplementation;->a:LX/15i;

    .line 1653494
    iput p2, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$DraculaImplementation;->b:I

    .line 1653495
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1653490
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1653489
    new-instance v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1653486
    iget v0, p0, LX/1vt;->c:I

    .line 1653487
    move v0, v0

    .line 1653488
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1653483
    iget v0, p0, LX/1vt;->c:I

    .line 1653484
    move v0, v0

    .line 1653485
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1653480
    iget v0, p0, LX/1vt;->b:I

    .line 1653481
    move v0, v0

    .line 1653482
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1653477
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1653478
    move-object v0, v0

    .line 1653479
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1653468
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1653469
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1653470
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1653471
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1653472
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1653473
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1653474
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1653475
    invoke-static {v3, v9, v2}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1653476
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1653465
    iget v0, p0, LX/1vt;->c:I

    .line 1653466
    move v0, v0

    .line 1653467
    return v0
.end method
