.class public final Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7d509635
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1654035
    const-class v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1654036
    const-class v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1654037
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1654038
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1654039
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1654040
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;->j()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1654041
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1654042
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1654043
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;->m()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, 0x102c5bc0

    invoke-static {v4, v3, v5}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$DraculaImplementation;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1654044
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1654045
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1654046
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1654047
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1654048
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1654049
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1654050
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1654051
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1654052
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;->j()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1654053
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;->j()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel;

    .line 1654054
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;->j()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1654055
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;

    .line 1654056
    iput-object v0, v1, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;->e:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel;

    .line 1654057
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1654058
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x102c5bc0

    invoke-static {v2, v0, v3}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1654059
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1654060
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;

    .line 1654061
    iput v3, v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;->h:I

    move-object v1, v0

    .line 1654062
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1654063
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 1654064
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object p0, v1

    .line 1654065
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1654066
    new-instance v0, LX/AH1;

    invoke-direct {v0, p1}, LX/AH1;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1654067
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1654032
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1654033
    const/4 v0, 0x3

    const v1, 0x102c5bc0

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;->h:I

    .line 1654034
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1654068
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1654069
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1654031
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1654028
    new-instance v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;-><init>()V

    .line 1654029
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1654030
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1654018
    const v0, 0x71ea02c6

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1654027
    const v0, 0x285feb

    return v0
.end method

.method public final j()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getDirectInboxThreads"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1654025
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;->e:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;->e:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel;

    .line 1654026
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;->e:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel$DirectInboxThreadsModel;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1654023
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    .line 1654024
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1654021
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    .line 1654022
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfilePicture"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1654019
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1654020
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBDirectSharingInboxQueryModel$DirectInboxModel$EdgesModel$NodeModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
