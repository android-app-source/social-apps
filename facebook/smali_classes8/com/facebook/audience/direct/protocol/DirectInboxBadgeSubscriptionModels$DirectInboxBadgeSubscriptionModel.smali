.class public final Lcom/facebook/audience/direct/protocol/DirectInboxBadgeSubscriptionModels$DirectInboxBadgeSubscriptionModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x46f94863
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/direct/protocol/DirectInboxBadgeSubscriptionModels$DirectInboxBadgeSubscriptionModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/direct/protocol/DirectInboxBadgeSubscriptionModels$DirectInboxBadgeSubscriptionModel$Serializer;
.end annotation


# instance fields
.field private e:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1648011
    const-class v0, Lcom/facebook/audience/direct/protocol/DirectInboxBadgeSubscriptionModels$DirectInboxBadgeSubscriptionModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1648014
    const-class v0, Lcom/facebook/audience/direct/protocol/DirectInboxBadgeSubscriptionModels$DirectInboxBadgeSubscriptionModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1648012
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1648013
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1648004
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1648005
    iget v0, p0, Lcom/facebook/audience/direct/protocol/DirectInboxBadgeSubscriptionModels$DirectInboxBadgeSubscriptionModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1648006
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1648007
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1648008
    iget v0, p0, Lcom/facebook/audience/direct/protocol/DirectInboxBadgeSubscriptionModels$DirectInboxBadgeSubscriptionModel;->e:I

    invoke-virtual {p1, v1, v0, v1}, LX/186;->a(III)V

    .line 1648009
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1648010
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1647993
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1647994
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1647995
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1647996
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1647997
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/audience/direct/protocol/DirectInboxBadgeSubscriptionModels$DirectInboxBadgeSubscriptionModel;->e:I

    .line 1647998
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1647999
    new-instance v0, Lcom/facebook/audience/direct/protocol/DirectInboxBadgeSubscriptionModels$DirectInboxBadgeSubscriptionModel;

    invoke-direct {v0}, Lcom/facebook/audience/direct/protocol/DirectInboxBadgeSubscriptionModels$DirectInboxBadgeSubscriptionModel;-><init>()V

    .line 1648000
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1648001
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1648002
    const v0, 0x2a184b4f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1648003
    const v0, -0x1f6b7e62

    return v0
.end method
