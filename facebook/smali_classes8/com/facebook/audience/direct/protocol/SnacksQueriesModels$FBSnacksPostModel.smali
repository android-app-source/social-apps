.class public final Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x27cf72eb
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:J

.field private k:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1654468
    const-class v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1654520
    const-class v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1654518
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1654519
    return-void
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1654516
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->e:Ljava/lang/String;

    .line 1654517
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1654514
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->f:Ljava/lang/String;

    .line 1654515
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1654498
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1654499
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1654500
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1654501
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1654502
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->k()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1654503
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->l()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1654504
    const/4 v5, 0x7

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1654505
    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 1654506
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1654507
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1654508
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1654509
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1654510
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->j:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1654511
    const/4 v0, 0x6

    iget v1, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->k:I

    invoke-virtual {p1, v0, v1, v6}, LX/186;->a(III)V

    .line 1654512
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1654513
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1654485
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1654486
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->k()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1654487
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->k()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;

    .line 1654488
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->k()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1654489
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;

    .line 1654490
    iput-object v0, v1, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->h:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;

    .line 1654491
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->l()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1654492
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->l()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel;

    .line 1654493
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->l()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1654494
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;

    .line 1654495
    iput-object v0, v1, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->i:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel;

    .line 1654496
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1654497
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1654484
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 1654480
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1654481
    const/4 v0, 0x5

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->j:J

    .line 1654482
    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->k:I

    .line 1654483
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1654477
    new-instance v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;

    invoke-direct {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;-><init>()V

    .line 1654478
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1654479
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1654476
    const v0, 0x22608020

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1654475
    const v0, -0x7b4a2069

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1654473
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->g:Ljava/lang/String;

    .line 1654474
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPostMedia"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1654471
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->h:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->h:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;

    .line 1654472
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->h:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel$PostMediaModel;

    return-object v0
.end method

.method public final l()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSeenByUsers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1654469
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->i:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->i:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel;

    .line 1654470
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksPostModel;->i:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel;

    return-object v0
.end method
