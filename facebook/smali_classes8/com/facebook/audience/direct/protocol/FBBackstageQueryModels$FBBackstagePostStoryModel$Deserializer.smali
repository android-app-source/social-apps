.class public final Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1649061
    const-class v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel;

    new-instance v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1649062
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1649063
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1649064
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1649065
    const/4 v2, 0x0

    .line 1649066
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_a

    .line 1649067
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1649068
    :goto_0
    move v1, v2

    .line 1649069
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1649070
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1649071
    new-instance v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel;

    invoke-direct {v1}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel;-><init>()V

    .line 1649072
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1649073
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1649074
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1649075
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1649076
    :cond_0
    return-object v1

    .line 1649077
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1649078
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_9

    .line 1649079
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1649080
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1649081
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, p0, :cond_2

    if-eqz v8, :cond_2

    .line 1649082
    const-string v9, "__type__"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    const-string v9, "__typename"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1649083
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v7

    goto :goto_1

    .line 1649084
    :cond_4
    const-string v9, "backstage"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1649085
    invoke-static {p1, v0}, LX/AGC;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1649086
    :cond_5
    const-string v9, "id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 1649087
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 1649088
    :cond_6
    const-string v9, "lowres_profile"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 1649089
    const/4 v8, 0x0

    .line 1649090
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v9, :cond_e

    .line 1649091
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1649092
    :goto_2
    move v4, v8

    .line 1649093
    goto :goto_1

    .line 1649094
    :cond_7
    const-string v9, "name"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 1649095
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1649096
    :cond_8
    const-string v9, "profile_picture"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1649097
    const/4 v8, 0x0

    .line 1649098
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v9, :cond_12

    .line 1649099
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1649100
    :goto_3
    move v1, v8

    .line 1649101
    goto/16 :goto_1

    .line 1649102
    :cond_9
    const/4 v8, 0x6

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 1649103
    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1649104
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1649105
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1649106
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1649107
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1649108
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1649109
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_a
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    goto/16 :goto_1

    .line 1649110
    :cond_b
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1649111
    :cond_c
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, p0, :cond_d

    .line 1649112
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1649113
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1649114
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_c

    if-eqz v9, :cond_c

    .line 1649115
    const-string p0, "uri"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_b

    .line 1649116
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_4

    .line 1649117
    :cond_d
    const/4 v9, 0x1

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 1649118
    invoke-virtual {v0, v8, v4}, LX/186;->b(II)V

    .line 1649119
    invoke-virtual {v0}, LX/186;->d()I

    move-result v8

    goto/16 :goto_2

    :cond_e
    move v4, v8

    goto :goto_4

    .line 1649120
    :cond_f
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1649121
    :cond_10
    :goto_5
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, p0, :cond_11

    .line 1649122
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1649123
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1649124
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_10

    if-eqz v9, :cond_10

    .line 1649125
    const-string p0, "uri"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_f

    .line 1649126
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_5

    .line 1649127
    :cond_11
    const/4 v9, 0x1

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 1649128
    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 1649129
    invoke-virtual {v0}, LX/186;->d()I

    move-result v8

    goto/16 :goto_3

    :cond_12
    move v1, v8

    goto :goto_5
.end method
