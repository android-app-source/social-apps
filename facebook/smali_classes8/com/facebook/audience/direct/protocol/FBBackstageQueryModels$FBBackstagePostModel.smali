.class public final Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x16c58b22
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:J

.field private k:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1648584
    const-class v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1648583
    const-class v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1648581
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1648582
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1648565
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1648566
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1648567
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1648568
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1648569
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->m()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1648570
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->n()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1648571
    const/4 v5, 0x7

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1648572
    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 1648573
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1648574
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1648575
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1648576
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1648577
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->j:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1648578
    const/4 v0, 0x6

    iget v1, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->k:I

    invoke-virtual {p1, v0, v1, v6}, LX/186;->a(III)V

    .line 1648579
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1648580
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1648552
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1648553
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->m()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1648554
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->m()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    .line 1648555
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->m()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1648556
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;

    .line 1648557
    iput-object v0, v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->h:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    .line 1648558
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->n()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1648559
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->n()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel;

    .line 1648560
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->n()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1648561
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;

    .line 1648562
    iput-object v0, v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->i:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel;

    .line 1648563
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1648564
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1648551
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 1648547
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1648548
    const/4 v0, 0x5

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->j:J

    .line 1648549
    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->k:I

    .line 1648550
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1648585
    new-instance v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;

    invoke-direct {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;-><init>()V

    .line 1648586
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1648587
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1648546
    const v0, -0x2b869f44

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1648533
    const v0, -0x7b4a2069

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1648544
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->e:Ljava/lang/String;

    .line 1648545
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1648542
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->f:Ljava/lang/String;

    .line 1648543
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1648540
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->g:Ljava/lang/String;

    .line 1648541
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPostMedia"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1648538
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->h:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->h:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    .line 1648539
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->h:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    return-object v0
.end method

.method public final n()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1648536
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->i:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->i:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel;

    .line 1648537
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->i:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel;

    return-object v0
.end method

.method public final o()I
    .locals 2

    .prologue
    .line 1648534
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1648535
    iget v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel;->k:I

    return v0
.end method
