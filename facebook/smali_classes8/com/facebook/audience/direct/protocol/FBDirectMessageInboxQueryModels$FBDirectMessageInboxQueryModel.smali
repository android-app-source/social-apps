.class public final Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1fb93f31
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1652814
    const-class v0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1652812
    const-class v0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1652815
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1652816
    return-void
.end method

.method private n()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1652817
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1652818
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1652819
    :cond_0
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private o()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1652820
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->h:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->h:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    .line 1652821
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->h:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 1652822
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1652823
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1652824
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->j()Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1652825
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1652826
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->o()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1652827
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1652828
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1652829
    const/4 v6, 0x6

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1652830
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 1652831
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1652832
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1652833
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1652834
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1652835
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1652836
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1652837
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1652838
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1652839
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->j()Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1652840
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->j()Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel;

    .line 1652841
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->j()Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1652842
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;

    .line 1652843
    iput-object v0, v1, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->f:Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel;

    .line 1652844
    :cond_0
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->o()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1652845
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->o()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    .line 1652846
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->o()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1652847
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;

    .line 1652848
    iput-object v0, v1, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->h:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$LowresProfilePictureModel;

    .line 1652849
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1652850
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    .line 1652851
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1652852
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;

    .line 1652853
    iput-object v0, v1, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->j:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    .line 1652854
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1652855
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1652856
    new-instance v0, LX/AGe;

    invoke-direct {v0, p1}, LX/AGe;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1652813
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1652810
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1652811
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1652809
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1652806
    new-instance v0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;

    invoke-direct {v0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;-><init>()V

    .line 1652807
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1652808
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1652805
    const v0, -0x46dd2d31

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1652804
    const v0, 0x3c2b9d5

    return v0
.end method

.method public final j()Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1652802
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->f:Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->f:Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel;

    .line 1652803
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->f:Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1652800
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->g:Ljava/lang/String;

    .line 1652801
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1652796
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->i:Ljava/lang/String;

    .line 1652797
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1652798
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->j:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->j:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    .line 1652799
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel;->j:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$InboxUserModel$ProfilePictureModel;

    return-object v0
.end method
