.class public final Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6761a290
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:J

.field private l:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1648993
    const-class v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1648994
    const-class v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1648991
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1648992
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1648989
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    .line 1648990
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1648987
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    .line 1648988
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1648985
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    .line 1648986
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPostMedia"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1648983
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->h:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->h:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    .line 1648984
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->h:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    return-object v0
.end method

.method private n()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1648995
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->i:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->i:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel;

    .line 1648996
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->i:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel;

    return-object v0
.end method

.method private o()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1648981
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->j:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->j:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel;

    .line 1648982
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->j:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1648963
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1648964
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1648965
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1648966
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1648967
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->m()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1648968
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->n()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1648969
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->o()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1648970
    const/16 v6, 0x8

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1648971
    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 1648972
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1648973
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1648974
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1648975
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1648976
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1648977
    const/4 v1, 0x6

    iget-wide v2, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->k:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1648978
    const/4 v0, 0x7

    iget v1, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->l:I

    invoke-virtual {p1, v0, v1, v7}, LX/186;->a(III)V

    .line 1648979
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1648980
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1648945
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1648946
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->m()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1648947
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->m()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    .line 1648948
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->m()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1648949
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;

    .line 1648950
    iput-object v0, v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->h:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostModel$PostMediaModel;

    .line 1648951
    :cond_0
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->n()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1648952
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->n()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel;

    .line 1648953
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->n()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1648954
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;

    .line 1648955
    iput-object v0, v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->i:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel$ReactionsModel;

    .line 1648956
    :cond_1
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->o()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1648957
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->o()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel;

    .line 1648958
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->o()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1648959
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;

    .line 1648960
    iput-object v0, v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->j:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel;

    .line 1648961
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1648962
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1648944
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 1648940
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1648941
    const/4 v0, 0x6

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->k:J

    .line 1648942
    const/4 v0, 0x7

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;->l:I

    .line 1648943
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1648937
    new-instance v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$BackstageModel$EdgesModel$NodeModel;-><init>()V

    .line 1648938
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1648939
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1648935
    const v0, -0x108f3e7

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1648936
    const v0, -0x7b4a2069

    return v0
.end method
