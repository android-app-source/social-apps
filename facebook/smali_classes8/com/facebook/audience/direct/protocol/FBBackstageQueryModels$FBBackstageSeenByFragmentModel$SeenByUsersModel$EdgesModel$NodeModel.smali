.class public final Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6f07c874
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1649929
    const-class v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1649930
    const-class v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1649931
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1649932
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1649945
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    .line 1649946
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel$ProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1649933
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;->g:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel$ProfilePictureModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel$ProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel$ProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;->g:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel$ProfilePictureModel;

    .line 1649934
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;->g:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel$ProfilePictureModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1649935
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1649936
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1649937
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1649938
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;->l()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel$ProfilePictureModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1649939
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1649940
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1649941
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1649942
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1649943
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1649944
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1649920
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1649921
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;->l()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1649922
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;->l()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel$ProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel$ProfilePictureModel;

    .line 1649923
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;->l()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel$ProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1649924
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;

    .line 1649925
    iput-object v0, v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;->g:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel$ProfilePictureModel;

    .line 1649926
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1649927
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1649928
    new-instance v0, LX/AFs;

    invoke-direct {v0, p1}, LX/AFs;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1649912
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1649909
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1649910
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1649911
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1649913
    new-instance v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;-><init>()V

    .line 1649914
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1649915
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1649919
    const v0, 0x3cd72807

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1649916
    const v0, 0x285feb

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1649917
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    .line 1649918
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstageSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    return-object v0
.end method
