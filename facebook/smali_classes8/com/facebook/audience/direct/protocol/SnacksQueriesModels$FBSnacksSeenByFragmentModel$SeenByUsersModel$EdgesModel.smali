.class public final Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x166e4da4
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:J


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1654673
    const-class v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1654672
    const-class v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1654647
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1654648
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1654665
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1654666
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1654667
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1654668
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1654669
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel;->f:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1654670
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1654671
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1654657
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1654658
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1654659
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;

    .line 1654660
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel;->a()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1654661
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel;

    .line 1654662
    iput-object v0, v1, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel;->e:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;

    .line 1654663
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1654664
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1654655
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel;->e:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel;->e:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;

    .line 1654656
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel;->e:Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel$NodeModel;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 1654652
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1654653
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel;->f:J

    .line 1654654
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1654649
    new-instance v0, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/audience/direct/protocol/SnacksQueriesModels$FBSnacksSeenByFragmentModel$SeenByUsersModel$EdgesModel;-><init>()V

    .line 1654650
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1654651
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1654646
    const v0, 0x6e977f50

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1654645
    const v0, 0x3313058

    return v0
.end method
