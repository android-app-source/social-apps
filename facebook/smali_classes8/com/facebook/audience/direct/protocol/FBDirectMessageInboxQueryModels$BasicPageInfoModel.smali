.class public final Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$BasicPageInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x36eecc3f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$BasicPageInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$BasicPageInfoModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Z

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1652425
    const-class v0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$BasicPageInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1652424
    const-class v0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$BasicPageInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1652422
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1652423
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1652420
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$BasicPageInfoModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$BasicPageInfoModel;->e:Ljava/lang/String;

    .line 1652421
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$BasicPageInfoModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1652418
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$BasicPageInfoModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$BasicPageInfoModel;->h:Ljava/lang/String;

    .line 1652419
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$BasicPageInfoModel;->h:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1652408
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1652409
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$BasicPageInfoModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1652410
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$BasicPageInfoModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1652411
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1652412
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1652413
    const/4 v0, 0x1

    iget-boolean v2, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$BasicPageInfoModel;->f:Z

    invoke-virtual {p1, v0, v2}, LX/186;->a(IZ)V

    .line 1652414
    const/4 v0, 0x2

    iget-boolean v2, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$BasicPageInfoModel;->g:Z

    invoke-virtual {p1, v0, v2}, LX/186;->a(IZ)V

    .line 1652415
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1652416
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1652417
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1652405
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1652406
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1652407
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1652396
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1652397
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$BasicPageInfoModel;->f:Z

    .line 1652398
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$BasicPageInfoModel;->g:Z

    .line 1652399
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1652402
    new-instance v0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$BasicPageInfoModel;

    invoke-direct {v0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$BasicPageInfoModel;-><init>()V

    .line 1652403
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1652404
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1652401
    const v0, 0x6f141409

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1652400
    const v0, 0x370fbffd

    return v0
.end method
