.class public final Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x665a1196
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel$BackstageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1649422
    const-class v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1649429
    const-class v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1649427
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1649428
    return-void
.end method

.method private j()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel$BackstageModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getBackstage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1649425
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;->e:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel$BackstageModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel$BackstageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel$BackstageModel;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;->e:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel$BackstageModel;

    .line 1649426
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;->e:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel$BackstageModel;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1649423
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    .line 1649424
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getLowresProfile"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1649420
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1649421
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1649418
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    .line 1649419
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method private n()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfilePicture"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1649416
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1649417
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;->i:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 1649430
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1649431
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;->j()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel$BackstageModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1649432
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1649433
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;->l()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, 0x7025d551

    invoke-static {v3, v2, v4}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1649434
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1649435
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;->n()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const v6, -0x2907ff97

    invoke-static {v5, v4, v6}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1649436
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1649437
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1649438
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1649439
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1649440
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1649441
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1649442
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1649443
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1649395
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1649396
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;->j()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel$BackstageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1649397
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;->j()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel$BackstageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel$BackstageModel;

    .line 1649398
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;->j()Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel$BackstageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1649399
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;

    .line 1649400
    iput-object v0, v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;->e:Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel$BackstageModel;

    .line 1649401
    :cond_0
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1649402
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x7025d551

    invoke-static {v2, v0, v3}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1649403
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1649404
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;

    .line 1649405
    iput v3, v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;->g:I

    move-object v1, v0

    .line 1649406
    :cond_1
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;->n()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 1649407
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;->n()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x2907ff97

    invoke-static {v2, v0, v3}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1649408
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;->n()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1649409
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;

    .line 1649410
    iput v3, v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;->i:I

    move-object v1, v0

    .line 1649411
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1649412
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    .line 1649413
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1649414
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_3
    move-object p0, v1

    .line 1649415
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1649394
    new-instance v0, LX/AFq;

    invoke-direct {v0, p1}, LX/AFq;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1649393
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1649389
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1649390
    const/4 v0, 0x2

    const v1, 0x7025d551

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;->g:I

    .line 1649391
    const/4 v0, 0x4

    const v1, -0x2907ff97

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;->i:I

    .line 1649392
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1649381
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1649382
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1649388
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1649385
    new-instance v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$BackstageFriendsModel$EdgesModel$NodeModel;-><init>()V

    .line 1649386
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1649387
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1649384
    const v0, 0x75f7fdf

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1649383
    const v0, 0x285feb

    return v0
.end method
