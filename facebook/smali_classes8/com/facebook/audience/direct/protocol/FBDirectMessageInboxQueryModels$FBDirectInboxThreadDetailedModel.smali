.class public final Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5fd40ee6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$AllDirectUsersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel$DirectMessagesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$DirectReactionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Z

.field private j:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$SeenDirectUsersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:J

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1652620
    const-class v0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1652619
    const-class v0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1652617
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1652618
    return-void
.end method

.method private o()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$DirectReactionsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1652615
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->g:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$DirectReactionsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$DirectReactionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$DirectReactionsModel;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->g:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$DirectReactionsModel;

    .line 1652616
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->g:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$DirectReactionsModel;

    return-object v0
.end method

.method private p()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1652613
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->j:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->j:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    .line 1652614
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->j:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1652539
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->n:Ljava/lang/String;

    .line 1652540
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->n:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    .line 1652591
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1652592
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->j()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$AllDirectUsersModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1652593
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->k()Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel$DirectMessagesModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1652594
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->o()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$DirectReactionsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1652595
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1652596
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->p()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1652597
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1652598
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->n()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$SeenDirectUsersModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1652599
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->q()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1652600
    const/16 v8, 0xa

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1652601
    const/4 v8, 0x0

    invoke-virtual {p1, v8, v0}, LX/186;->b(II)V

    .line 1652602
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1652603
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1652604
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1652605
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1652606
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1652607
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1652608
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1652609
    const/16 v1, 0x8

    iget-wide v2, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->m:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1652610
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1652611
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1652612
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1652558
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1652559
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->j()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$AllDirectUsersModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1652560
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->j()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$AllDirectUsersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$AllDirectUsersModel;

    .line 1652561
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->j()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$AllDirectUsersModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1652562
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;

    .line 1652563
    iput-object v0, v1, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->e:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$AllDirectUsersModel;

    .line 1652564
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->k()Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel$DirectMessagesModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1652565
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->k()Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel$DirectMessagesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel$DirectMessagesModel;

    .line 1652566
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->k()Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel$DirectMessagesModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1652567
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;

    .line 1652568
    iput-object v0, v1, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->f:Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel$DirectMessagesModel;

    .line 1652569
    :cond_1
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->o()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$DirectReactionsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1652570
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->o()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$DirectReactionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$DirectReactionsModel;

    .line 1652571
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->o()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$DirectReactionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1652572
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;

    .line 1652573
    iput-object v0, v1, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->g:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$DirectReactionsModel;

    .line 1652574
    :cond_2
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->p()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1652575
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->p()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    .line 1652576
    invoke-direct {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->p()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1652577
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;

    .line 1652578
    iput-object v0, v1, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->j:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    .line 1652579
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1652580
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    .line 1652581
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1652582
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;

    .line 1652583
    iput-object v0, v1, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->k:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    .line 1652584
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->n()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$SeenDirectUsersModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1652585
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->n()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$SeenDirectUsersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$SeenDirectUsersModel;

    .line 1652586
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->n()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$SeenDirectUsersModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 1652587
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;

    .line 1652588
    iput-object v0, v1, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->l:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$SeenDirectUsersModel;

    .line 1652589
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1652590
    if-nez v1, :cond_6

    :goto_0
    return-object p0

    :cond_6
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1652557
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 1652553
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1652554
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->i:Z

    .line 1652555
    const/16 v0, 0x8

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->m:J

    .line 1652556
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1652536
    new-instance v0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;

    invoke-direct {v0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;-><init>()V

    .line 1652537
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1652538
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1652541
    const v0, 0x24c83e7e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1652542
    const v0, 0x177e5768

    return v0
.end method

.method public final j()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$AllDirectUsersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1652543
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->e:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$AllDirectUsersModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$AllDirectUsersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$AllDirectUsersModel;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->e:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$AllDirectUsersModel;

    .line 1652544
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->e:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$AllDirectUsersModel;

    return-object v0
.end method

.method public final k()Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel$DirectMessagesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1652545
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->f:Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel$DirectMessagesModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel$DirectMessagesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel$DirectMessagesModel;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->f:Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel$DirectMessagesModel;

    .line 1652546
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->f:Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel$DirectMessagesModel;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1652547
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->h:Ljava/lang/String;

    .line 1652548
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1652549
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->k:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->k:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    .line 1652550
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->k:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxMessageModel;

    return-object v0
.end method

.method public final n()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$SeenDirectUsersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1652551
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->l:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$SeenDirectUsersModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$SeenDirectUsersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$SeenDirectUsersModel;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->l:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$SeenDirectUsersModel;

    .line 1652552
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectInboxThreadDetailedModel;->l:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxThreadModel$SeenDirectUsersModel;

    return-object v0
.end method
