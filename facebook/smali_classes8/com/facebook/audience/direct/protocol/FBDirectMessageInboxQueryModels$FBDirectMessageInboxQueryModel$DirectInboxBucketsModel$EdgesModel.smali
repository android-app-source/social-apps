.class public final Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x39a3dd28
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1652719
    const-class v0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1652718
    const-class v0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1652716
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1652717
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1652710
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1652711
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1652712
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1652713
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1652714
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1652715
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1652695
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1652696
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1652697
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;

    .line 1652698
    invoke-virtual {p0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel$EdgesModel;->a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1652699
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel$EdgesModel;

    .line 1652700
    iput-object v0, v1, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel$EdgesModel;->e:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;

    .line 1652701
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1652702
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1652708
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel$EdgesModel;->e:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;

    iput-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel$EdgesModel;->e:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;

    .line 1652709
    iget-object v0, p0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel$EdgesModel;->e:Lcom/facebook/audience/protocol/FBInboxQueryFragmentsModels$FBDirectInboxBucketModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1652705
    new-instance v0, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/audience/direct/protocol/FBDirectMessageInboxQueryModels$FBDirectMessageInboxQueryModel$DirectInboxBucketsModel$EdgesModel;-><init>()V

    .line 1652706
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1652707
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1652704
    const v0, -0x3a6a02c5

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1652703
    const v0, -0x5cdbc717

    return v0
.end method
