.class public final Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1649132
    const-class v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel;

    new-instance v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1649133
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1649134
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1649135
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1649136
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1649137
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1649138
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1649139
    if-eqz v2, :cond_0

    .line 1649140
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1649141
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1649142
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1649143
    if-eqz v2, :cond_1

    .line 1649144
    const-string p0, "backstage"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1649145
    invoke-static {v1, v2, p1, p2}, LX/AGC;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1649146
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1649147
    if-eqz v2, :cond_2

    .line 1649148
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1649149
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1649150
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1649151
    if-eqz v2, :cond_4

    .line 1649152
    const-string p0, "lowres_profile"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1649153
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1649154
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1649155
    if-eqz p0, :cond_3

    .line 1649156
    const-string p2, "uri"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1649157
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1649158
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1649159
    :cond_4
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1649160
    if-eqz v2, :cond_5

    .line 1649161
    const-string p0, "name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1649162
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1649163
    :cond_5
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1649164
    if-eqz v2, :cond_7

    .line 1649165
    const-string p0, "profile_picture"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1649166
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1649167
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1649168
    if-eqz p0, :cond_6

    .line 1649169
    const-string v0, "uri"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1649170
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1649171
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1649172
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1649173
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1649174
    check-cast p1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel$Serializer;->a(Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostStoryModel;LX/0nX;LX/0my;)V

    return-void
.end method
