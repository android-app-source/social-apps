.class public final Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1649672
    const-class v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel;

    new-instance v1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1649673
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1649674
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1649675
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1649676
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1649677
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1649678
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1649679
    if-eqz v2, :cond_0

    .line 1649680
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1649681
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1649682
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1649683
    if-eqz v2, :cond_1

    .line 1649684
    const-string p0, "backstage"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1649685
    invoke-static {v1, v2, p1, p2}, LX/AGJ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1649686
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1649687
    if-eqz v2, :cond_2

    .line 1649688
    const-string p0, "backstage_audience_mode"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1649689
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1649690
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1649691
    if-eqz v2, :cond_3

    .line 1649692
    const-string p0, "backstage_friends"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1649693
    invoke-static {v1, v2, p1, p2}, LX/AGH;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1649694
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1649695
    if-eqz v2, :cond_5

    .line 1649696
    const-string p0, "lowres_profile"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1649697
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1649698
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1649699
    if-eqz p0, :cond_4

    .line 1649700
    const-string p2, "uri"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1649701
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1649702
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1649703
    :cond_5
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1649704
    if-eqz v2, :cond_6

    .line 1649705
    const-string p0, "name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1649706
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1649707
    :cond_6
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1649708
    if-eqz v2, :cond_8

    .line 1649709
    const-string p0, "profile_picture"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1649710
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1649711
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1649712
    if-eqz p0, :cond_7

    .line 1649713
    const-string v0, "uri"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1649714
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1649715
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1649716
    :cond_8
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1649717
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1649718
    check-cast p1, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel$Serializer;->a(Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$FBBackstagePostsSummaryModel;LX/0nX;LX/0my;)V

    return-void
.end method
