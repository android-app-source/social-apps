.class public final Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1648286
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1648287
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1648284
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1648285
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 1648203
    if-nez p1, :cond_0

    .line 1648204
    :goto_0
    return v0

    .line 1648205
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1648206
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1648207
    :sswitch_0
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 1648208
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1648209
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 1648210
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1648211
    :sswitch_1
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 1648212
    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1648213
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1648214
    invoke-virtual {p0, p1, v5, v0}, LX/15i;->a(III)I

    move-result v3

    .line 1648215
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1648216
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 1648217
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 1648218
    invoke-virtual {p3, v5, v3, v0}, LX/186;->a(III)V

    .line 1648219
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1648220
    :sswitch_2
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 1648221
    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1648222
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1648223
    invoke-virtual {p0, p1, v5, v0}, LX/15i;->a(III)I

    move-result v3

    .line 1648224
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1648225
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 1648226
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 1648227
    invoke-virtual {p3, v5, v3, v0}, LX/186;->a(III)V

    .line 1648228
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1648229
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1648230
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1648231
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1648232
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1648233
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1648234
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1648235
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1648236
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1648237
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1648238
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1648239
    :sswitch_5
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1648240
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1648241
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1648242
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1648243
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1648244
    :sswitch_6
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1648245
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1648246
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1648247
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1648248
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1648249
    :sswitch_7
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1648250
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1648251
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1648252
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1648253
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1648254
    :sswitch_8
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1648255
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1648256
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1648257
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1648258
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1648259
    :sswitch_9
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1648260
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1648261
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1648262
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1648263
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1648264
    :sswitch_a
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1648265
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1648266
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1648267
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1648268
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1648269
    :sswitch_b
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1648270
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1648271
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1648272
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1648273
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1648274
    :sswitch_c
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1648275
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1648276
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1648277
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1648278
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1648279
    :sswitch_d
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1648280
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1648281
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1648282
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1648283
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x2b8fdefb -> :sswitch_b
        -0x2907ff97 -> :sswitch_5
        -0x100ca846 -> :sswitch_0
        0x9eadb72 -> :sswitch_3
        0xa5fec93 -> :sswitch_7
        0x22319076 -> :sswitch_1
        0x34db42d2 -> :sswitch_d
        0x65ecfad4 -> :sswitch_8
        0x666b9cb6 -> :sswitch_9
        0x6c57886c -> :sswitch_a
        0x6e49672c -> :sswitch_6
        0x6e6ab5bc -> :sswitch_2
        0x7025d551 -> :sswitch_4
        0x75cd8fca -> :sswitch_c
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1648202
    new-instance v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1648199
    sparse-switch p0, :sswitch_data_0

    .line 1648200
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1648201
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x2b8fdefb -> :sswitch_0
        -0x2907ff97 -> :sswitch_0
        -0x100ca846 -> :sswitch_0
        0x9eadb72 -> :sswitch_0
        0xa5fec93 -> :sswitch_0
        0x22319076 -> :sswitch_0
        0x34db42d2 -> :sswitch_0
        0x65ecfad4 -> :sswitch_0
        0x666b9cb6 -> :sswitch_0
        0x6c57886c -> :sswitch_0
        0x6e49672c -> :sswitch_0
        0x6e6ab5bc -> :sswitch_0
        0x7025d551 -> :sswitch_0
        0x75cd8fca -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1648198
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1648196
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;->b(I)V

    .line 1648197
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1648165
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1648166
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1648167
    :cond_0
    iput-object p1, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;->a:LX/15i;

    .line 1648168
    iput p2, p0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;->b:I

    .line 1648169
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1648195
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1648194
    new-instance v0, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1648191
    iget v0, p0, LX/1vt;->c:I

    .line 1648192
    move v0, v0

    .line 1648193
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1648188
    iget v0, p0, LX/1vt;->c:I

    .line 1648189
    move v0, v0

    .line 1648190
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1648185
    iget v0, p0, LX/1vt;->b:I

    .line 1648186
    move v0, v0

    .line 1648187
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1648182
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1648183
    move-object v0, v0

    .line 1648184
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1648173
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1648174
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1648175
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1648176
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1648177
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1648178
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1648179
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1648180
    invoke-static {v3, v9, v2}, Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/audience/direct/protocol/FBBackstageQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1648181
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1648170
    iget v0, p0, LX/1vt;->c:I

    .line 1648171
    move v0, v0

    .line 1648172
    return v0
.end method
