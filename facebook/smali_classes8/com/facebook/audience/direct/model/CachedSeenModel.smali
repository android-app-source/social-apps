.class public Lcom/facebook/audience/direct/model/CachedSeenModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/audience/direct/model/CachedSeenModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field private final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1647800
    new-instance v0, LX/AFT;

    invoke-direct {v0}, LX/AFT;-><init>()V

    sput-object v0, Lcom/facebook/audience/direct/model/CachedSeenModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/AFU;)V
    .locals 2

    .prologue
    .line 1647819
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1647820
    iget-wide v0, p1, LX/AFU;->a:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/audience/direct/model/CachedSeenModel;->a:J

    .line 1647821
    iget-object v0, p1, LX/AFU;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/direct/model/CachedSeenModel;->b:Ljava/lang/String;

    .line 1647822
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1647815
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1647816
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/audience/direct/model/CachedSeenModel;->a:J

    .line 1647817
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/model/CachedSeenModel;->b:Ljava/lang/String;

    .line 1647818
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1647814
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1647805
    if-ne p0, p1, :cond_1

    .line 1647806
    :cond_0
    :goto_0
    return v0

    .line 1647807
    :cond_1
    instance-of v2, p1, Lcom/facebook/audience/direct/model/CachedSeenModel;

    if-nez v2, :cond_2

    move v0, v1

    .line 1647808
    goto :goto_0

    .line 1647809
    :cond_2
    check-cast p1, Lcom/facebook/audience/direct/model/CachedSeenModel;

    .line 1647810
    iget-wide v2, p0, Lcom/facebook/audience/direct/model/CachedSeenModel;->a:J

    iget-wide v4, p1, Lcom/facebook/audience/direct/model/CachedSeenModel;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    move v0, v1

    .line 1647811
    goto :goto_0

    .line 1647812
    :cond_3
    iget-object v2, p0, Lcom/facebook/audience/direct/model/CachedSeenModel;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/direct/model/CachedSeenModel;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1647813
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 1647804
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/audience/direct/model/CachedSeenModel;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/audience/direct/model/CachedSeenModel;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1647801
    iget-wide v0, p0, Lcom/facebook/audience/direct/model/CachedSeenModel;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1647802
    iget-object v0, p0, Lcom/facebook/audience/direct/model/CachedSeenModel;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1647803
    return-void
.end method
