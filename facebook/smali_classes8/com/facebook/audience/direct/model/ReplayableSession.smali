.class public Lcom/facebook/audience/direct/model/ReplayableSession;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/audience/direct/model/ReplayableSession;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1647870
    new-instance v0, LX/AFX;

    invoke-direct {v0}, LX/AFX;-><init>()V

    sput-object v0, Lcom/facebook/audience/direct/model/ReplayableSession;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/AFY;)V
    .locals 2

    .prologue
    .line 1647866
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1647867
    iget-wide v0, p1, LX/AFY;->a:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/audience/direct/model/ReplayableSession;->a:J

    .line 1647868
    iget-object v0, p1, LX/AFY;->b:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/audience/direct/model/ReplayableSession;->b:LX/0Px;

    .line 1647869
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    .line 1647857
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1647858
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/audience/direct/model/ReplayableSession;->a:J

    .line 1647859
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v1, v0, [Ljava/lang/String;

    .line 1647860
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 1647861
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 1647862
    aput-object v2, v1, v0

    .line 1647863
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1647864
    :cond_0
    invoke-static {v1}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/model/ReplayableSession;->b:LX/0Px;

    .line 1647865
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1647856
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1647847
    if-ne p0, p1, :cond_1

    .line 1647848
    :cond_0
    :goto_0
    return v0

    .line 1647849
    :cond_1
    instance-of v2, p1, Lcom/facebook/audience/direct/model/ReplayableSession;

    if-nez v2, :cond_2

    move v0, v1

    .line 1647850
    goto :goto_0

    .line 1647851
    :cond_2
    check-cast p1, Lcom/facebook/audience/direct/model/ReplayableSession;

    .line 1647852
    iget-wide v2, p0, Lcom/facebook/audience/direct/model/ReplayableSession;->a:J

    iget-wide v4, p1, Lcom/facebook/audience/direct/model/ReplayableSession;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    move v0, v1

    .line 1647853
    goto :goto_0

    .line 1647854
    :cond_3
    iget-object v2, p0, Lcom/facebook/audience/direct/model/ReplayableSession;->b:LX/0Px;

    iget-object v3, p1, Lcom/facebook/audience/direct/model/ReplayableSession;->b:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1647855
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 1647840
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/audience/direct/model/ReplayableSession;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/audience/direct/model/ReplayableSession;->b:LX/0Px;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 1647841
    iget-wide v0, p0, Lcom/facebook/audience/direct/model/ReplayableSession;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1647842
    iget-object v0, p0, Lcom/facebook/audience/direct/model/ReplayableSession;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1647843
    iget-object v0, p0, Lcom/facebook/audience/direct/model/ReplayableSession;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/facebook/audience/direct/model/ReplayableSession;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1647844
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1647845
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1647846
    :cond_0
    return-void
.end method
