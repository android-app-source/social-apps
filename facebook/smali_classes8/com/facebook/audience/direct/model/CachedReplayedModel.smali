.class public Lcom/facebook/audience/direct/model/CachedReplayedModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/audience/direct/model/CachedReplayedModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field private final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1647792
    new-instance v0, LX/AFR;

    invoke-direct {v0}, LX/AFR;-><init>()V

    sput-object v0, Lcom/facebook/audience/direct/model/CachedReplayedModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/AFS;)V
    .locals 2

    .prologue
    .line 1647788
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1647789
    iget-wide v0, p1, LX/AFS;->a:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/audience/direct/model/CachedReplayedModel;->a:J

    .line 1647790
    iget-object v0, p1, LX/AFS;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/audience/direct/model/CachedReplayedModel;->b:Ljava/lang/String;

    .line 1647791
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1647784
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1647785
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/audience/direct/model/CachedReplayedModel;->a:J

    .line 1647786
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/direct/model/CachedReplayedModel;->b:Ljava/lang/String;

    .line 1647787
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1647793
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1647775
    if-ne p0, p1, :cond_1

    .line 1647776
    :cond_0
    :goto_0
    return v0

    .line 1647777
    :cond_1
    instance-of v2, p1, Lcom/facebook/audience/direct/model/CachedReplayedModel;

    if-nez v2, :cond_2

    move v0, v1

    .line 1647778
    goto :goto_0

    .line 1647779
    :cond_2
    check-cast p1, Lcom/facebook/audience/direct/model/CachedReplayedModel;

    .line 1647780
    iget-wide v2, p0, Lcom/facebook/audience/direct/model/CachedReplayedModel;->a:J

    iget-wide v4, p1, Lcom/facebook/audience/direct/model/CachedReplayedModel;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    move v0, v1

    .line 1647781
    goto :goto_0

    .line 1647782
    :cond_3
    iget-object v2, p0, Lcom/facebook/audience/direct/model/CachedReplayedModel;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/audience/direct/model/CachedReplayedModel;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1647783
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 1647774
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/audience/direct/model/CachedReplayedModel;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/audience/direct/model/CachedReplayedModel;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1647771
    iget-wide v0, p0, Lcom/facebook/audience/direct/model/CachedReplayedModel;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1647772
    iget-object v0, p0, Lcom/facebook/audience/direct/model/CachedReplayedModel;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1647773
    return-void
.end method
