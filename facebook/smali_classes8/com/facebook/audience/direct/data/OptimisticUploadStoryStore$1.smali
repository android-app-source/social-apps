.class public final Lcom/facebook/audience/direct/data/OptimisticUploadStoryStore$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0Pz;

.field public final synthetic b:LX/AF8;

.field public final synthetic c:LX/1EY;


# direct methods
.method public constructor <init>(LX/1EY;LX/0Pz;LX/AF8;)V
    .locals 0

    .prologue
    .line 1647461
    iput-object p1, p0, Lcom/facebook/audience/direct/data/OptimisticUploadStoryStore$1;->c:LX/1EY;

    iput-object p2, p0, Lcom/facebook/audience/direct/data/OptimisticUploadStoryStore$1;->a:LX/0Pz;

    iput-object p3, p0, Lcom/facebook/audience/direct/data/OptimisticUploadStoryStore$1;->b:LX/AF8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 1647462
    iget-object v0, p0, Lcom/facebook/audience/direct/data/OptimisticUploadStoryStore$1;->c:LX/1EY;

    iget-object v0, v0, LX/1EY;->f:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1647463
    iget-object v1, p0, Lcom/facebook/audience/direct/data/OptimisticUploadStoryStore$1;->c:LX/1EY;

    iget-object v1, v1, LX/1EY;->a:LX/1EZ;

    invoke-virtual {v1, v0}, LX/1EZ;->f(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1647464
    iget-object v1, p0, Lcom/facebook/audience/direct/data/OptimisticUploadStoryStore$1;->c:LX/1EY;

    iget-object v1, v1, LX/1EY;->a:LX/1EZ;

    invoke-virtual {v1, v0}, LX/1EZ;->e(Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v1

    .line 1647465
    iget-object v4, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->am:Lcom/facebook/audience/model/UploadShot;

    move-object v1, v4

    .line 1647466
    :goto_1
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 1647467
    iget-object v4, p0, Lcom/facebook/audience/direct/data/OptimisticUploadStoryStore$1;->c:LX/1EY;

    iget-object v5, p0, Lcom/facebook/audience/direct/data/OptimisticUploadStoryStore$1;->a:LX/0Pz;

    .line 1647468
    sget-object v6, LX/7gz;->SENDING:LX/7gz;

    .line 1647469
    iget-object v7, v4, LX/1EY;->g:LX/0Ri;

    invoke-interface {v7}, LX/0Ri;->a_()LX/0Ri;

    move-result-object v7

    invoke-interface {v7, v0}, LX/0Ri;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1647470
    sget-object v6, LX/7gz;->SENT:LX/7gz;

    .line 1647471
    :cond_1
    :goto_2
    move-object v8, v6

    .line 1647472
    invoke-virtual {v1}, Lcom/facebook/audience/model/UploadShot;->getIsPrivate()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1647473
    invoke-virtual {v1}, Lcom/facebook/audience/model/UploadShot;->getAudience()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    const/4 v6, 0x0

    move v7, v6

    :goto_3
    if-ge v7, v10, :cond_3

    invoke-virtual {v9, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/audience/model/AudienceControlData;

    .line 1647474
    invoke-static {v1, v0, v6, v8}, LX/1EY;->a(Lcom/facebook/audience/model/UploadShot;Ljava/lang/String;Lcom/facebook/audience/model/AudienceControlData;LX/7gz;)LX/7h0;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1647475
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_3

    .line 1647476
    :cond_2
    invoke-static {}, Lcom/facebook/audience/model/AudienceControlData;->newBuilder()Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v6

    iget-object v7, v4, LX/1EY;->e:Lcom/facebook/user/model/User;

    .line 1647477
    iget-object v9, v7, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v7, v9

    .line 1647478
    invoke-virtual {v6, v7}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setId(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v6

    iget-object v7, v4, LX/1EY;->e:Lcom/facebook/user/model/User;

    invoke-virtual {v7}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setName(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v6

    iget-object v7, v4, LX/1EY;->e:Lcom/facebook/user/model/User;

    invoke-virtual {v7}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setProfileUri(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v6

    iget-object v7, v4, LX/1EY;->e:Lcom/facebook/user/model/User;

    invoke-virtual {v7}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/facebook/audience/model/AudienceControlData$Builder;->setLowResProfileUri(Ljava/lang/String;)Lcom/facebook/audience/model/AudienceControlData$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/audience/model/AudienceControlData$Builder;->a()Lcom/facebook/audience/model/AudienceControlData;

    move-result-object v6

    move-object v6, v6

    .line 1647479
    invoke-static {v1, v0, v6, v8}, LX/1EY;->a(Lcom/facebook/audience/model/UploadShot;Ljava/lang/String;Lcom/facebook/audience/model/AudienceControlData;LX/7gz;)LX/7h0;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1647480
    :cond_3
    goto/16 :goto_0

    .line 1647481
    :cond_4
    iget-object v1, p0, Lcom/facebook/audience/direct/data/OptimisticUploadStoryStore$1;->c:LX/1EY;

    iget-object v1, v1, LX/1EY;->g:LX/0Ri;

    invoke-interface {v1, v0}, LX/0Ri;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1647482
    iget-object v1, p0, Lcom/facebook/audience/direct/data/OptimisticUploadStoryStore$1;->c:LX/1EY;

    iget-object v1, v1, LX/1EY;->g:LX/0Ri;

    invoke-interface {v1, v0}, LX/0Ri;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1647483
    iget-object v1, p0, Lcom/facebook/audience/direct/data/OptimisticUploadStoryStore$1;->c:LX/1EY;

    iget-object v1, v1, LX/1EY;->h:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/audience/model/UploadShot;

    goto/16 :goto_1

    .line 1647484
    :cond_5
    iget-object v0, p0, Lcom/facebook/audience/direct/data/OptimisticUploadStoryStore$1;->c:LX/1EY;

    iget-object v0, v0, LX/1EY;->c:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/audience/direct/data/OptimisticUploadStoryStore$1$1;

    invoke-direct {v1, p0}, Lcom/facebook/audience/direct/data/OptimisticUploadStoryStore$1$1;-><init>(Lcom/facebook/audience/direct/data/OptimisticUploadStoryStore$1;)V

    const v2, -0x31d81459

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1647485
    return-void

    :cond_6
    move-object v0, v2

    move-object v1, v2

    goto/16 :goto_1

    .line 1647486
    :cond_7
    iget-object v7, v4, LX/1EY;->i:Ljava/util/Set;

    invoke-interface {v7, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1647487
    sget-object v6, LX/7gz;->FAILED:LX/7gz;

    goto/16 :goto_2
.end method
