.class public final Lcom/facebook/audience/direct/data/MultiKeyParcelDiskCache$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/util/concurrent/CountDownLatch;

.field public final synthetic b:LX/AEs;

.field public final synthetic c:Ljava/util/Map;

.field public final synthetic d:LX/AFD;


# direct methods
.method public constructor <init>(LX/AFD;Ljava/util/concurrent/CountDownLatch;LX/AEs;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 1647363
    iput-object p1, p0, Lcom/facebook/audience/direct/data/MultiKeyParcelDiskCache$2;->d:LX/AFD;

    iput-object p2, p0, Lcom/facebook/audience/direct/data/MultiKeyParcelDiskCache$2;->a:Ljava/util/concurrent/CountDownLatch;

    iput-object p3, p0, Lcom/facebook/audience/direct/data/MultiKeyParcelDiskCache$2;->b:LX/AEs;

    iput-object p4, p0, Lcom/facebook/audience/direct/data/MultiKeyParcelDiskCache$2;->c:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 1647364
    :try_start_0
    iget-object v0, p0, Lcom/facebook/audience/direct/data/MultiKeyParcelDiskCache$2;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 1647365
    iget-object v0, p0, Lcom/facebook/audience/direct/data/MultiKeyParcelDiskCache$2;->b:LX/AEs;

    iget-object v1, p0, Lcom/facebook/audience/direct/data/MultiKeyParcelDiskCache$2;->c:Ljava/util/Map;

    invoke-interface {v0, v1}, LX/AEs;->a(Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1647366
    :goto_0
    return-void

    .line 1647367
    :catch_0
    move-exception v0

    .line 1647368
    sget-object v1, LX/AFD;->a:Ljava/lang/String;

    const-string v2, "interruption occured while trying to fetch for many items"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1647369
    iget-object v0, p0, Lcom/facebook/audience/direct/data/MultiKeyParcelDiskCache$2;->b:LX/AEs;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/AEs;->a(Ljava/util/Map;)V

    goto :goto_0
.end method
