.class public final Lcom/facebook/audience/direct/data/DirectUserThreadProvider$8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/util/List;

.field public final synthetic b:LX/0Px;

.field public final synthetic c:Z

.field public final synthetic d:LX/0gI;


# direct methods
.method public constructor <init>(LX/0gI;Ljava/util/List;LX/0Px;Z)V
    .locals 0

    .prologue
    .line 1647340
    iput-object p1, p0, Lcom/facebook/audience/direct/data/DirectUserThreadProvider$8;->d:LX/0gI;

    iput-object p2, p0, Lcom/facebook/audience/direct/data/DirectUserThreadProvider$8;->a:Ljava/util/List;

    iput-object p3, p0, Lcom/facebook/audience/direct/data/DirectUserThreadProvider$8;->b:LX/0Px;

    iput-boolean p4, p0, Lcom/facebook/audience/direct/data/DirectUserThreadProvider$8;->c:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 1647341
    iget-object v0, p0, Lcom/facebook/audience/direct/data/DirectUserThreadProvider$8;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 1647342
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ef4;

    .line 1647343
    if-eqz v0, :cond_0

    .line 1647344
    iget-object v2, p0, Lcom/facebook/audience/direct/data/DirectUserThreadProvider$8;->b:LX/0Px;

    iget-boolean v3, p0, Lcom/facebook/audience/direct/data/DirectUserThreadProvider$8;->c:Z

    .line 1647345
    if-nez v3, :cond_1

    .line 1647346
    iget-object v4, v0, LX/Ef4;->a:Lcom/facebook/audience/direct/app/SnacksInboxFragment;

    iget-object v4, v4, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->e:LX/0hg;

    const/4 v5, 0x1

    const v3, 0xbc0006

    .line 1647347
    iget-object v6, v4, LX/0hg;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v6, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->f(I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1647348
    iget-object v7, v4, LX/0hg;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    if-eqz v5, :cond_3

    const/4 v6, 0x2

    :goto_1
    invoke-interface {v7, v3, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1647349
    :cond_1
    iget-object v4, v0, LX/Ef4;->a:Lcom/facebook/audience/direct/app/SnacksInboxFragment;

    invoke-static {v4, v2}, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->a$redex0(Lcom/facebook/audience/direct/app/SnacksInboxFragment;LX/0Px;)V

    .line 1647350
    iget-object v4, v0, LX/Ef4;->a:Lcom/facebook/audience/direct/app/SnacksInboxFragment;

    iget-object v4, v4, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->h:Lcom/facebook/audience/direct/ui/SnacksInboxView;

    invoke-virtual {v4, v2}, Lcom/facebook/audience/direct/ui/SnacksInboxView;->a(LX/0Px;)V

    .line 1647351
    iget-object v4, v0, LX/Ef4;->a:Lcom/facebook/audience/direct/app/SnacksInboxFragment;

    iget-object v4, v4, Lcom/facebook/audience/direct/app/SnacksInboxFragment;->h:Lcom/facebook/audience/direct/ui/SnacksInboxView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/facebook/audience/direct/ui/SnacksInboxView;->setRefreshing(Z)V

    .line 1647352
    goto :goto_0

    .line 1647353
    :cond_2
    return-void

    .line 1647354
    :cond_3
    const/4 v6, 0x3

    goto :goto_1
.end method
