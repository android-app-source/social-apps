.class public final Lcom/facebook/audience/direct/data/MultiKeyParcelDiskCache$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/ListenableFuture;

.field public final synthetic b:LX/AFC;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/AFD;


# direct methods
.method public constructor <init>(LX/AFD;Lcom/google/common/util/concurrent/ListenableFuture;LX/AFC;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1647370
    iput-object p1, p0, Lcom/facebook/audience/direct/data/MultiKeyParcelDiskCache$3;->d:LX/AFD;

    iput-object p2, p0, Lcom/facebook/audience/direct/data/MultiKeyParcelDiskCache$3;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    iput-object p3, p0, Lcom/facebook/audience/direct/data/MultiKeyParcelDiskCache$3;->b:LX/AFC;

    iput-object p4, p0, Lcom/facebook/audience/direct/data/MultiKeyParcelDiskCache$3;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 1647371
    :try_start_0
    iget-object v0, p0, Lcom/facebook/audience/direct/data/MultiKeyParcelDiskCache$3;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    const v1, 0x20232cfe

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 1647372
    if-eqz v0, :cond_0

    array-length v1, v0

    if-nez v1, :cond_1

    .line 1647373
    :cond_0
    iget-object v0, p0, Lcom/facebook/audience/direct/data/MultiKeyParcelDiskCache$3;->b:LX/AFC;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/AFC;->a(Landroid/os/Parcel;)V

    .line 1647374
    :goto_0
    return-void

    .line 1647375
    :cond_1
    iget-object v1, p0, Lcom/facebook/audience/direct/data/MultiKeyParcelDiskCache$3;->b:LX/AFC;

    const/4 v7, 0x0

    .line 1647376
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 1647377
    array-length v4, v0

    invoke-virtual {v2, v0, v7, v4}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 1647378
    invoke-virtual {v2, v7}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 1647379
    move-object v0, v2

    .line 1647380
    invoke-virtual {v1, v0}, LX/AFC;->a(Landroid/os/Parcel;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 1647381
    :catch_0
    move-exception v0

    .line 1647382
    sget-object v1, LX/AFD;->a:Ljava/lang/String;

    const-string v2, "cache fetching was interrupted while trying to fetch for key: %s"

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/facebook/audience/direct/data/MultiKeyParcelDiskCache$3;->c:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1647383
    iget-object v0, p0, Lcom/facebook/audience/direct/data/MultiKeyParcelDiskCache$3;->b:LX/AFC;

    invoke-virtual {v0, v5}, LX/AFC;->a(Landroid/os/Parcel;)V

    goto :goto_0

    .line 1647384
    :catch_1
    move-exception v0

    .line 1647385
    sget-object v1, LX/AFD;->a:Ljava/lang/String;

    const-string v2, "error occured with the execution for cache fetching for key: %s"

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/facebook/audience/direct/data/MultiKeyParcelDiskCache$3;->c:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1647386
    iget-object v0, p0, Lcom/facebook/audience/direct/data/MultiKeyParcelDiskCache$3;->b:LX/AFC;

    invoke-virtual {v0, v5}, LX/AFC;->a(Landroid/os/Parcel;)V

    goto :goto_0
.end method
