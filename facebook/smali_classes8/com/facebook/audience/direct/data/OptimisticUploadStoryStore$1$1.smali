.class public final Lcom/facebook/audience/direct/data/OptimisticUploadStoryStore$1$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/audience/direct/data/OptimisticUploadStoryStore$1;


# direct methods
.method public constructor <init>(Lcom/facebook/audience/direct/data/OptimisticUploadStoryStore$1;)V
    .locals 0

    .prologue
    .line 1647421
    iput-object p1, p0, Lcom/facebook/audience/direct/data/OptimisticUploadStoryStore$1$1;->a:Lcom/facebook/audience/direct/data/OptimisticUploadStoryStore$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 1647422
    iget-object v0, p0, Lcom/facebook/audience/direct/data/OptimisticUploadStoryStore$1$1;->a:Lcom/facebook/audience/direct/data/OptimisticUploadStoryStore$1;

    iget-object v0, v0, Lcom/facebook/audience/direct/data/OptimisticUploadStoryStore$1;->b:LX/AF8;

    iget-object v1, p0, Lcom/facebook/audience/direct/data/OptimisticUploadStoryStore$1$1;->a:Lcom/facebook/audience/direct/data/OptimisticUploadStoryStore$1;

    iget-object v1, v1, Lcom/facebook/audience/direct/data/OptimisticUploadStoryStore$1;->a:LX/0Pz;

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1647423
    iget-object v2, v0, LX/AF8;->a:LX/AF9;

    iget-object v2, v2, LX/AF9;->a:LX/AFA;

    iget-object v2, v2, LX/AFA;->b:LX/0gI;

    iget-object v2, v2, LX/0gI;->f:LX/1El;

    .line 1647424
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 1647425
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v6

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_0

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/7h0;

    .line 1647426
    invoke-static {v2, v3}, LX/1El;->a(LX/1El;LX/7h0;)LX/7h0;

    move-result-object v3

    .line 1647427
    invoke-virtual {v5, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1647428
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    .line 1647429
    :cond_0
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    move-object v2, v3

    .line 1647430
    iget-object v3, v0, LX/AF8;->a:LX/AF9;

    iget-object v3, v3, LX/AF9;->a:LX/AFA;

    iget-object v3, v3, LX/AFA;->b:LX/0gI;

    iget-object v3, v3, LX/0gI;->h:LX/AF0;

    .line 1647431
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 1647432
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result p0

    const/4 v4, 0x0

    move v6, v4

    :goto_1
    if-ge v6, p0, :cond_2

    invoke-virtual {v2, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/7h0;

    .line 1647433
    iget-object v5, v4, LX/7h0;->d:Lcom/facebook/audience/model/AudienceControlData;

    move-object v5, v5

    .line 1647434
    invoke-virtual {v5}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v5

    .line 1647435
    invoke-interface {v7, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1647436
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v7, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1647437
    :cond_1
    invoke-interface {v7, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1647438
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_1

    .line 1647439
    :cond_2
    move-object v4, v7

    .line 1647440
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1647441
    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_3
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 1647442
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    .line 1647443
    invoke-static {v3, v5}, LX/AF0;->a(LX/AF0;Ljava/util/List;)LX/AFW;

    move-result-object v5

    .line 1647444
    if-eqz v5, :cond_3

    .line 1647445
    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1647446
    :cond_4
    move-object v4, v6

    .line 1647447
    iget-object v5, v3, LX/AF0;->b:Ljava/util/Comparator;

    invoke-static {v4, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1647448
    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    move-object v2, v4

    .line 1647449
    iget-object v3, v0, LX/AF8;->a:LX/AF9;

    iget-object v3, v3, LX/AF9;->a:LX/AFA;

    iget-object v3, v3, LX/AFA;->b:LX/0gI;

    const/4 v4, 0x0

    .line 1647450
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v7

    move v6, v4

    move v5, v4

    :goto_3
    if-ge v6, v7, :cond_5

    invoke-virtual {v2, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/AFW;

    .line 1647451
    iget-object p0, v4, LX/AFW;->a:LX/7h0;

    move-object v4, p0

    .line 1647452
    iget-boolean p0, v4, LX/7h0;->h:Z

    move v4, p0

    .line 1647453
    if-nez v4, :cond_6

    .line 1647454
    add-int/lit8 v4, v5, 0x1

    .line 1647455
    :goto_4
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    move v5, v4

    goto :goto_3

    .line 1647456
    :cond_5
    iget-object v4, v3, LX/0gI;->a:LX/0xB;

    sget-object v6, LX/12j;->BACKSTAGE:LX/12j;

    invoke-virtual {v4, v6, v5}, LX/0xB;->a(LX/12j;I)V

    .line 1647457
    iget-object v3, v0, LX/AF8;->a:LX/AF9;

    iget-object v3, v3, LX/AF9;->a:LX/AFA;

    iget-object v3, v3, LX/AFA;->b:LX/0gI;

    iget-object v4, v0, LX/AF8;->a:LX/AF9;

    iget-object v4, v4, LX/AF9;->a:LX/AFA;

    iget-boolean v4, v4, LX/AFA;->a:Z

    .line 1647458
    iget-object v5, v3, LX/0gI;->e:LX/7gh;

    invoke-virtual {v5}, LX/7gh;->a()Ljava/util/List;

    move-result-object v5

    .line 1647459
    iget-object v6, v3, LX/0gI;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v7, Lcom/facebook/audience/direct/data/DirectUserThreadProvider$8;

    invoke-direct {v7, v3, v5, v2, v4}, Lcom/facebook/audience/direct/data/DirectUserThreadProvider$8;-><init>(LX/0gI;Ljava/util/List;LX/0Px;Z)V

    const v5, -0x3bb3a716

    invoke-static {v6, v7, v5}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1647460
    return-void

    :cond_6
    move v4, v5

    goto :goto_4
.end method
