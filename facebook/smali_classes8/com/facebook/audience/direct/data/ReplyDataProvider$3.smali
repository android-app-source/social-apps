.class public final Lcom/facebook/audience/direct/data/ReplyDataProvider$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/util/List;

.field public final synthetic b:Lcom/facebook/audience/model/ReplyThread;

.field public final synthetic c:LX/AFO;


# direct methods
.method public constructor <init>(LX/AFO;Ljava/util/List;Lcom/facebook/audience/model/ReplyThread;)V
    .locals 0

    .prologue
    .line 1647619
    iput-object p1, p0, Lcom/facebook/audience/direct/data/ReplyDataProvider$3;->c:LX/AFO;

    iput-object p2, p0, Lcom/facebook/audience/direct/data/ReplyDataProvider$3;->a:Ljava/util/List;

    iput-object p3, p0, Lcom/facebook/audience/direct/data/ReplyDataProvider$3;->b:Lcom/facebook/audience/model/ReplyThread;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1647620
    iget-object v0, p0, Lcom/facebook/audience/direct/data/ReplyDataProvider$3;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 1647621
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EfK;

    .line 1647622
    if-eqz v0, :cond_0

    .line 1647623
    iget-object v2, v0, LX/EfK;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1647624
    iget-object v3, p0, Lcom/facebook/audience/direct/data/ReplyDataProvider$3;->b:Lcom/facebook/audience/model/ReplyThread;

    .line 1647625
    iget-object v4, v3, Lcom/facebook/audience/model/ReplyThread;->a:Ljava/lang/String;

    move-object v3, v4

    .line 1647626
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1647627
    iget-object v2, p0, Lcom/facebook/audience/direct/data/ReplyDataProvider$3;->b:Lcom/facebook/audience/model/ReplyThread;

    invoke-virtual {v0, v2}, LX/EfK;->a(Lcom/facebook/audience/model/ReplyThread;)V

    goto :goto_0

    .line 1647628
    :cond_1
    return-void
.end method
