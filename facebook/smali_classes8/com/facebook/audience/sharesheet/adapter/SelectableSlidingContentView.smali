.class public Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;
.super Lcom/facebook/resources/ui/FbFrameLayout;
.source ""


# instance fields
.field public a:LX/AIy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:LX/AIx;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private e:I

.field private f:I

.field public g:Z

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1660519
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1660520
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1660521
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1660522
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1660523
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 1660505
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1660506
    const-class v0, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;

    invoke-static {v0, p0}, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1660507
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, LX/03r;->SelectableSlidingContentView:[I

    invoke-virtual {v0, p2, v1, p3, v4}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1660508
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/16 v2, 0x0

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 1660509
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->c:Landroid/view/View;

    .line 1660510
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->d:Landroid/view/View;

    .line 1660511
    const/16 v1, 0x3

    const v2, 0x7f0b1b73

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->e:I

    .line 1660512
    const/16 v1, 0x4

    iget v2, p0, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->e:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->f:I

    .line 1660513
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1660514
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->a:LX/AIy;

    iget-object v1, p0, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->c:Landroid/view/View;

    iget-object v2, p0, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->d:Landroid/view/View;

    .line 1660515
    new-instance v4, LX/AIx;

    invoke-static {v0}, LX/4mV;->a(LX/0QB;)LX/4mV;

    move-result-object v3

    check-cast v3, LX/4mV;

    invoke-direct {v4, v1, v2, v3}, LX/AIx;-><init>(Landroid/view/View;Landroid/view/View;LX/4mV;)V

    .line 1660516
    move-object v0, v4

    .line 1660517
    iput-object v0, p0, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->b:LX/AIx;

    .line 1660518
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;

    const-class v1, LX/AIy;

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/AIy;

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->a:LX/AIy;

    return-void
.end method


# virtual methods
.method public final b(Z)V
    .locals 2

    .prologue
    .line 1660500
    iput-boolean p1, p0, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->g:Z

    .line 1660501
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->h:Z

    .line 1660502
    iget-object v1, p0, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->b:LX/AIx;

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->f:I

    :goto_0
    invoke-virtual {v1, p1, v0}, LX/AIx;->a(ZI)V

    .line 1660503
    return-void

    .line 1660504
    :cond_0
    iget v0, p0, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->e:I

    goto :goto_0
.end method

.method public final onLayout(ZIIII)V
    .locals 5

    .prologue
    .line 1660465
    invoke-super/range {p0 .. p5}, Lcom/facebook/resources/ui/FbFrameLayout;->onLayout(ZIIII)V

    .line 1660466
    iget-boolean v0, p0, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->h:Z

    if-nez v0, :cond_8

    .line 1660467
    iget-object v1, p0, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->b:LX/AIx;

    iget-boolean v2, p0, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->g:Z

    iget-object v0, p0, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v3

    iget-object v0, p0, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v4

    iget-boolean v0, p0, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->g:Z

    if-eqz v0, :cond_7

    iget v0, p0, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->f:I

    :goto_0
    const/4 p3, 0x4

    const/high16 p2, 0x3f800000    # 1.0f

    const/4 p1, 0x0

    .line 1660468
    iget-object p0, v1, LX/AIx;->a:Landroid/view/View;

    if-eqz p0, :cond_0

    .line 1660469
    iget-object p0, v1, LX/AIx;->a:Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p0

    .line 1660470
    iput v0, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1660471
    iget-object p4, v1, LX/AIx;->a:Landroid/view/View;

    invoke-virtual {p4, p0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1660472
    :cond_0
    iget p0, v1, LX/AIx;->g:I

    if-eqz p0, :cond_1

    iget p0, v1, LX/AIx;->h:I

    if-nez p0, :cond_d

    :cond_1
    const/4 p0, 0x1

    :goto_1
    move p0, p0

    .line 1660473
    if-eqz p0, :cond_2

    .line 1660474
    sub-int p0, v0, v4

    sub-int/2addr p0, v3

    div-int/lit8 p0, p0, 0x2

    .line 1660475
    div-int/lit8 p4, v0, 0x2

    sub-int/2addr p4, p0

    div-int/lit8 p5, v3, 0x2

    sub-int/2addr p4, p5

    iput p4, v1, LX/AIx;->g:I

    .line 1660476
    div-int/lit8 p4, v0, 0x2

    sub-int p0, p4, p0

    div-int/lit8 p4, v4, 0x2

    sub-int/2addr p0, p4

    iput p0, v1, LX/AIx;->h:I

    .line 1660477
    :cond_2
    if-eqz v2, :cond_9

    .line 1660478
    iget-object p0, v1, LX/AIx;->b:Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getTranslationY()F

    move-result p0

    iget p1, v1, LX/AIx;->g:I

    neg-int p1, p1

    int-to-float p1, p1

    cmpl-float p0, p0, p1

    if-eqz p0, :cond_3

    .line 1660479
    iget-object p0, v1, LX/AIx;->b:Landroid/view/View;

    iget p1, v1, LX/AIx;->g:I

    neg-int p1, p1

    int-to-float p1, p1

    invoke-virtual {p0, p1}, Landroid/view/View;->setTranslationY(F)V

    .line 1660480
    :cond_3
    iget-object p0, v1, LX/AIx;->c:Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getTranslationY()F

    move-result p0

    iget p1, v1, LX/AIx;->h:I

    int-to-float p1, p1

    cmpl-float p0, p0, p1

    if-eqz p0, :cond_4

    .line 1660481
    iget-object p0, v1, LX/AIx;->c:Landroid/view/View;

    iget p1, v1, LX/AIx;->h:I

    int-to-float p1, p1

    invoke-virtual {p0, p1}, Landroid/view/View;->setTranslationY(F)V

    .line 1660482
    :cond_4
    iget-object p0, v1, LX/AIx;->c:Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getAlpha()F

    move-result p0

    cmpl-float p0, p0, p2

    if-eqz p0, :cond_5

    .line 1660483
    iget-object p0, v1, LX/AIx;->c:Landroid/view/View;

    invoke-virtual {p0, p2}, Landroid/view/View;->setAlpha(F)V

    .line 1660484
    :cond_5
    iget-object p0, v1, LX/AIx;->c:Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result p0

    if-eqz p0, :cond_6

    .line 1660485
    iget-object p0, v1, LX/AIx;->c:Landroid/view/View;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 1660486
    :cond_6
    :goto_2
    return-void

    .line 1660487
    :cond_7
    iget v0, p0, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->e:I

    goto/16 :goto_0

    .line 1660488
    :cond_8
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->h:Z

    goto :goto_2

    .line 1660489
    :cond_9
    iget-object p0, v1, LX/AIx;->b:Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getTranslationY()F

    move-result p0

    cmpl-float p0, p0, p1

    if-eqz p0, :cond_a

    .line 1660490
    iget-object p0, v1, LX/AIx;->b:Landroid/view/View;

    invoke-virtual {p0, p1}, Landroid/view/View;->setTranslationY(F)V

    .line 1660491
    :cond_a
    iget-object p0, v1, LX/AIx;->c:Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getTranslationY()F

    move-result p0

    cmpl-float p0, p0, p1

    if-eqz p0, :cond_b

    .line 1660492
    iget-object p0, v1, LX/AIx;->c:Landroid/view/View;

    invoke-virtual {p0, p1}, Landroid/view/View;->setTranslationY(F)V

    .line 1660493
    :cond_b
    iget-object p0, v1, LX/AIx;->c:Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getAlpha()F

    move-result p0

    cmpl-float p0, p0, p1

    if-eqz p0, :cond_c

    .line 1660494
    iget-object p0, v1, LX/AIx;->c:Landroid/view/View;

    invoke-virtual {p0, p1}, Landroid/view/View;->setAlpha(F)V

    .line 1660495
    :cond_c
    iget-object p0, v1, LX/AIx;->c:Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result p0

    if-eq p0, p3, :cond_6

    .line 1660496
    iget-object p0, v1, LX/AIx;->c:Landroid/view/View;

    invoke-virtual {p0, p3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_d
    const/4 p0, 0x0

    goto/16 :goto_1
.end method

.method public setParentForHeightAnimation(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1660497
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->b:LX/AIx;

    .line 1660498
    iput-object p1, v0, LX/AIx;->a:Landroid/view/View;

    .line 1660499
    return-void
.end method
