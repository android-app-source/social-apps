.class public Lcom/facebook/audience/sharesheet/adapter/MyDayViewHolder;
.super LX/AIq;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# static fields
.field private static final m:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private n:Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;

.field private o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1660418
    const-class v0, Lcom/facebook/audience/sharesheet/adapter/MyDayViewHolder;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/audience/sharesheet/adapter/MyDayViewHolder;->m:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1660419
    invoke-direct {p0, p1}, LX/AIq;-><init>(Landroid/view/View;)V

    .line 1660420
    const v0, 0x7f0d2c52

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/adapter/MyDayViewHolder;->n:Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;

    .line 1660421
    const v0, 0x7f0d2c51

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/adapter/MyDayViewHolder;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1660422
    return-void
.end method


# virtual methods
.method public final a(LX/AJ2;LX/AJ8;Landroid/net/Uri;)V
    .locals 2
    .param p3    # Landroid/net/Uri;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1660423
    invoke-virtual {p2}, LX/AJ8;->c()Z

    move-result v0

    invoke-super {p0, v0, p1, p2}, LX/AIq;->a(ZLX/AJ2;LX/AJ8;)V

    .line 1660424
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/adapter/MyDayViewHolder;->n:Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;

    invoke-virtual {p2}, LX/AJ8;->c()Z

    move-result v1

    .line 1660425
    iput-boolean v1, v0, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->g:Z

    .line 1660426
    if-eqz p3, :cond_0

    .line 1660427
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/adapter/MyDayViewHolder;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Lcom/facebook/audience/sharesheet/adapter/MyDayViewHolder;->m:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p3, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1660428
    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 1660429
    invoke-super {p0, p1}, LX/AIq;->b(Z)V

    .line 1660430
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/adapter/MyDayViewHolder;->n:Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;

    invoke-virtual {v0, p1}, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->b(Z)V

    .line 1660431
    return-void
.end method
