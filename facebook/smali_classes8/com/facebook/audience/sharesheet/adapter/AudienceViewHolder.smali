.class public Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;
.super LX/1a1;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final l:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public m:LX/AJ8;

.field public n:Lcom/facebook/audience/model/AudienceControlData;

.field private o:Landroid/content/Context;

.field public p:Ljava/lang/String;

.field private q:Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;

.field public r:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private s:Lcom/facebook/resources/ui/FbTextView;

.field private t:Lcom/facebook/resources/ui/FbTextView;

.field public u:Lcom/facebook/resources/ui/FbCheckBox;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1660363
    const-class v0, LX/AIu;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;->l:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;LX/AJ8;)V
    .locals 2

    .prologue
    .line 1660364
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 1660365
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;->o:Landroid/content/Context;

    .line 1660366
    iput-object p2, p0, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;->m:LX/AJ8;

    .line 1660367
    const v0, 0x7f0d2c47

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;->q:Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;

    .line 1660368
    const v0, 0x7f0d2c46

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;->r:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1660369
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;->q:Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;

    const v1, 0x7f0d2c43

    invoke-virtual {v0, v1}, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;->s:Lcom/facebook/resources/ui/FbTextView;

    .line 1660370
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;->q:Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;

    const v1, 0x7f0d2c45

    invoke-virtual {v0, v1}, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;->t:Lcom/facebook/resources/ui/FbTextView;

    .line 1660371
    const v0, 0x7f0d2c48

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbCheckBox;

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;->u:Lcom/facebook/resources/ui/FbCheckBox;

    .line 1660372
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;->o:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082886

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;->p:Ljava/lang/String;

    .line 1660373
    new-instance v0, LX/AIo;

    invoke-direct {v0, p0}, LX/AIo;-><init>(Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1660374
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;->u:Lcom/facebook/resources/ui/FbCheckBox;

    new-instance v1, LX/AIp;

    invoke-direct {v1, p0}, LX/AIp;-><init>(Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbCheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1660375
    return-void
.end method

.method public static a(Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1660376
    iget-object v2, p0, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;->q:Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;

    .line 1660377
    iput-boolean p1, v2, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->g:Z

    .line 1660378
    iget-object v2, p0, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;->s:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, p2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1660379
    if-eqz p1, :cond_0

    .line 1660380
    iget-object v2, p0, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;->t:Lcom/facebook/resources/ui/FbTextView;

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {p2}, LX/7hK;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {p3, v3}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1660381
    :cond_0
    iget-object v2, p0, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;->s:Lcom/facebook/resources/ui/FbTextView;

    iget-object v3, p0, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;->s:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3}, Lcom/facebook/resources/ui/FbTextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v3

    if-eqz p1, :cond_1

    :goto_0
    invoke-static {v3, v0}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1660382
    return-void

    :cond_1
    move v0, v1

    .line 1660383
    goto :goto_0
.end method

.method public static b(Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;Z)V
    .locals 2

    .prologue
    .line 1660384
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;->n:Lcom/facebook/audience/model/AudienceControlData;

    invoke-virtual {v0}, Lcom/facebook/audience/model/AudienceControlData;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;->p:Ljava/lang/String;

    invoke-static {p0, p1, v0, v1}, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;->a(Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;ZLjava/lang/String;Ljava/lang/String;)V

    .line 1660385
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/adapter/AudienceViewHolder;->q:Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;

    invoke-virtual {v0, p1}, Lcom/facebook/audience/sharesheet/adapter/SelectableSlidingContentView;->b(Z)V

    .line 1660386
    return-void
.end method
