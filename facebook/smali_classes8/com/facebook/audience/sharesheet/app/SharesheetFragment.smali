.class public Lcom/facebook/audience/sharesheet/app/SharesheetFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
    fragment = .enum LX/0cQ;->SHARESHEET_FRAGMENT:LX/0cQ;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:LX/7hN;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0fO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/7gT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/AJR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/AJI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/AJT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/8RJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/AJ4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/AJK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/AIn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/AJu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;

.field public n:Landroid/support/v7/widget/RecyclerView;

.field public o:LX/AIu;

.field public p:Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;

.field public q:LX/AJt;

.field public r:LX/AJQ;

.field public s:LX/AJH;

.field public t:LX/AJS;

.field public u:LX/AJZ;

.field public final v:LX/AJ8;

.field public final w:Landroid/view/View$OnClickListener;

.field private final x:LX/AJA;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1660739
    const-class v0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1660815
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1660816
    new-instance v0, LX/AJ8;

    invoke-direct {v0, p0}, LX/AJ8;-><init>(Lcom/facebook/audience/sharesheet/app/SharesheetFragment;)V

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->v:LX/AJ8;

    .line 1660817
    new-instance v0, LX/AJ9;

    invoke-direct {v0, p0}, LX/AJ9;-><init>(Lcom/facebook/audience/sharesheet/app/SharesheetFragment;)V

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->w:Landroid/view/View$OnClickListener;

    .line 1660818
    new-instance v0, LX/AJA;

    invoke-direct {v0, p0}, LX/AJA;-><init>(Lcom/facebook/audience/sharesheet/app/SharesheetFragment;)V

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->x:LX/AJA;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/audience/sharesheet/app/SharesheetFragment;ZZ)V
    .locals 1

    .prologue
    .line 1660819
    if-eqz p1, :cond_0

    .line 1660820
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->p:Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;

    invoke-virtual {v0, p2}, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->a(Z)V

    .line 1660821
    :goto_0
    return-void

    .line 1660822
    :cond_0
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->p:Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;

    invoke-virtual {v0, p2}, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->b(Z)V

    goto :goto_0
.end method

.method private b(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    .line 1660825
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1660826
    invoke-static {v0, p1}, LX/AJD;->a(Landroid/os/Bundle;Landroid/os/Bundle;)Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;

    move-result-object v0

    .line 1660827
    invoke-virtual {v0}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->isVideo()Z

    move-result v1

    .line 1660828
    iget-object v3, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->b:LX/7hN;

    if-eqz v1, :cond_3

    const v2, 0x7f08287d

    :goto_0
    const/4 v4, -0x1

    const/4 v5, 0x0

    const/4 p1, 0x0

    invoke-virtual {v3, v2, v4, v5, p1}, LX/7hN;->a(IIZLX/107;)Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    move-result-object v2

    .line 1660829
    iget-object v3, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->m:Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;

    .line 1660830
    iput-object v2, v3, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->d:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 1660831
    iget-object v1, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->g:LX/AJT;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 1660832
    new-instance v5, LX/AJS;

    invoke-static {v1}, LX/7gT;->a(LX/0QB;)LX/7gT;

    move-result-object v8

    check-cast v8, LX/7gT;

    invoke-static {v1}, LX/AJK;->a(LX/0QB;)LX/AJK;

    move-result-object v9

    check-cast v9, LX/AJK;

    invoke-static {v1}, LX/0he;->a(LX/0QB;)LX/0he;

    move-result-object v10

    check-cast v10, LX/0he;

    invoke-static {v1}, LX/0hg;->a(LX/0QB;)LX/0hg;

    move-result-object v11

    check-cast v11, LX/0hg;

    move-object v6, v0

    move-object v7, v2

    invoke-direct/range {v5 .. v11}, LX/AJS;-><init>(Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;Landroid/app/Activity;LX/7gT;LX/AJK;LX/0he;LX/0hg;)V

    .line 1660833
    move-object v1, v5

    .line 1660834
    iput-object v1, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->t:LX/AJS;

    .line 1660835
    iget-object v1, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->u:LX/AJZ;

    invoke-virtual {v0}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->getSelectedAudience()LX/0Px;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->isNewsFeedSelected()Z

    move-result v3

    invoke-virtual {v0}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->isMyDaySelected()Z

    move-result v4

    .line 1660836
    iget-object v5, v1, LX/AJZ;->a:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->clear()V

    .line 1660837
    iget-object v5, v1, LX/AJZ;->c:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 1660838
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v7

    const/4 v5, 0x0

    move v6, v5

    :goto_1
    if-ge v6, v7, :cond_0

    invoke-virtual {v2, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/audience/model/AudienceControlData;

    .line 1660839
    iget-object v8, v1, LX/AJZ;->a:Ljava/util/Map;

    invoke-virtual {v5}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1660840
    iget-object v8, v1, LX/AJZ;->b:Ljava/util/Set;

    invoke-interface {v8, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1660841
    iget-object v8, v1, LX/AJZ;->c:Ljava/util/List;

    invoke-virtual {v5}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v8, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1660842
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_1

    .line 1660843
    :cond_0
    iput-boolean v3, v1, LX/AJZ;->e:Z

    .line 1660844
    iput-boolean v4, v1, LX/AJZ;->f:Z

    .line 1660845
    invoke-virtual {v0}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->getSearchQuery()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1660846
    iget-object v1, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->m:Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;

    invoke-virtual {v0}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->getSearchQuery()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->a(Ljava/lang/String;)V

    .line 1660847
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/audience/sharesheet/data/SharesheetParseResult;->getSelectablePrivacyData()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    .line 1660848
    iget-object v1, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->e:LX/AJR;

    iget-object v2, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->x:LX/AJA;

    .line 1660849
    new-instance v5, LX/AJQ;

    invoke-static {v1}, LX/0fO;->b(LX/0QB;)LX/0fO;

    move-result-object v6

    check-cast v6, LX/0fO;

    const-class v7, LX/93y;

    invoke-interface {v1, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/93y;

    invoke-static {v1}, LX/7gT;->a(LX/0QB;)LX/7gT;

    move-result-object v8

    check-cast v8, LX/7gT;

    invoke-static {v1}, LX/AJK;->a(LX/0QB;)LX/AJK;

    move-result-object v9

    check-cast v9, LX/AJK;

    move-object v10, v2

    move-object v11, v0

    invoke-direct/range {v5 .. v11}, LX/AJQ;-><init>(LX/0fO;LX/93y;LX/7gT;LX/AJK;LX/AJA;Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    .line 1660850
    move-object v0, v5

    .line 1660851
    iput-object v0, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->r:LX/AJQ;

    .line 1660852
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->b(Lcom/facebook/audience/sharesheet/app/SharesheetFragment;Z)V

    .line 1660853
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->o:LX/AIu;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 1660854
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->s:LX/AJH;

    .line 1660855
    iget-object v1, v0, LX/AJH;->h:LX/0hg;

    .line 1660856
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/0hg;->e:Z

    .line 1660857
    const v2, 0xbc0001

    invoke-static {v1}, LX/0hg;->h(LX/0hg;)Ljava/util/Map;

    move-result-object v3

    invoke-static {v1, v2, v3}, LX/0hg;->a(LX/0hg;ILjava/util/Map;)V

    .line 1660858
    iget-object v1, v0, LX/AJH;->b:LX/AJX;

    iget-object v2, v0, LX/AJH;->k:LX/AJF;

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1660859
    const/4 v4, 0x0

    .line 1660860
    move v3, v4

    :goto_2
    iget-object v0, v1, LX/AJX;->e:[Z

    array-length v0, v0

    if-ge v3, v0, :cond_2

    .line 1660861
    iget-object v0, v1, LX/AJX;->e:[Z

    aput-boolean v4, v0, v3

    .line 1660862
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1660863
    :cond_2
    iget-object v3, v1, LX/AJX;->f:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1660864
    invoke-static {v1, v6, v2, v5}, LX/AJX;->a(LX/AJX;ZLX/AJF;I)V

    .line 1660865
    :goto_3
    invoke-static {v1, v5, v2, v5}, LX/AJX;->a(LX/AJX;ZLX/AJF;I)V

    .line 1660866
    invoke-static {v1, v6, v2, v6}, LX/AJX;->a(LX/AJX;ZLX/AJF;I)V

    .line 1660867
    invoke-static {v1, v5, v2, v6}, LX/AJX;->a(LX/AJX;ZLX/AJF;I)V

    .line 1660868
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->s:LX/AJH;

    .line 1660869
    iget-object v5, v0, LX/AJH;->g:LX/0aG;

    const-string v6, "sync_contacts_partial"

    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    sget-object v8, LX/1ME;->BY_EXCEPTION:LX/1ME;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v9

    const v10, -0x2bdce425

    invoke-static/range {v5 .. v10}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v5

    invoke-interface {v5}, LX/1MF;->start()LX/1ML;

    .line 1660870
    return-void

    .line 1660871
    :cond_3
    const v2, 0x7f08287c

    goto/16 :goto_0

    .line 1660872
    :cond_4
    iget-object v3, v1, LX/AJX;->e:[Z

    aput-boolean v6, v3, v5

    .line 1660873
    iget-object v3, v1, LX/AJX;->f:LX/0Px;

    invoke-static {v1}, LX/AJX;->c(LX/AJX;)Z

    move-result v4

    invoke-virtual {v2, v3, v4}, LX/AJF;->a(LX/0Px;Z)V

    .line 1660874
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 1660875
    iput-object v3, v1, LX/AJX;->f:LX/0Px;

    goto :goto_3
.end method

.method public static b(Lcom/facebook/audience/sharesheet/app/SharesheetFragment;Z)V
    .locals 1

    .prologue
    .line 1660823
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->q:LX/AJt;

    invoke-virtual {v0}, LX/AJt;->g()Z

    move-result v0

    invoke-static {p0, v0, p1}, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->a$redex0(Lcom/facebook/audience/sharesheet/app/SharesheetFragment;ZZ)V

    .line 1660824
    return-void
.end method

.method public static k(Lcom/facebook/audience/sharesheet/app/SharesheetFragment;)V
    .locals 4

    .prologue
    .line 1660788
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->d:LX/7gT;

    sget-object v1, LX/7gR;->SELECTED_FRIENDS_EVER:LX/7gR;

    iget-object v2, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->u:LX/AJZ;

    .line 1660789
    iget-object v3, v2, LX/AJZ;->b:Ljava/util/Set;

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    move-object v2, v3

    .line 1660790
    invoke-virtual {v0, v1, v2}, LX/7gT;->a(LX/7gR;LX/0Px;)V

    .line 1660791
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->d:LX/7gT;

    sget-object v1, LX/7gR;->DESELECTED_FRIENDS_EVER:LX/7gR;

    iget-object v2, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->u:LX/AJZ;

    .line 1660792
    new-instance v3, Ljava/util/HashSet;

    iget-object p0, v2, LX/AJZ;->b:Ljava/util/Set;

    invoke-direct {v3, p0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 1660793
    iget-object p0, v2, LX/AJZ;->a:Ljava/util/Map;

    invoke-interface {p0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p0

    invoke-interface {v3, p0}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 1660794
    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    move-object v2, v3

    .line 1660795
    invoke-virtual {v0, v1, v2}, LX/7gT;->a(LX/7gR;LX/0Px;)V

    .line 1660796
    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1660797
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->m:Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;

    invoke-virtual {v0}, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1660798
    :goto_0
    return v7

    .line 1660799
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1660800
    const-string v1, "extra_selected_audience"

    iget-object v2, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->u:LX/AJZ;

    invoke-virtual {v2}, LX/AJZ;->e()LX/0Px;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->r:LX/AJQ;

    invoke-virtual {v3}, LX/AJQ;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->u:LX/AJZ;

    .line 1660801
    iget-boolean v5, v4, LX/AJZ;->f:Z

    move v4, v5

    .line 1660802
    iget-object v5, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->u:LX/AJZ;

    .line 1660803
    iget-boolean v8, v5, LX/AJZ;->e:Z

    move v5, v8

    .line 1660804
    invoke-static {v2, v3, v4, v5}, LX/AJS;->b(LX/0Px;Lcom/facebook/privacy/model/SelectablePrivacyData;ZZ)Lcom/facebook/audience/model/SharesheetSelectedAudience;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1660805
    iget-object v1, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->j:LX/AJK;

    .line 1660806
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1660807
    invoke-static {v1, v2}, LX/AJK;->a(LX/AJK;Landroid/os/Bundle;)V

    .line 1660808
    const-string v3, "extra_sharesheet_survey_data"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1660809
    const-string v2, "extra_sharesheet_integration_point_id"

    const-string v3, "162429737526884"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1660810
    iget-object v1, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->d:LX/7gT;

    sget-object v2, LX/7gR;->BACK_TO_CAMERA:LX/7gR;

    invoke-virtual {v1, v2}, LX/7gT;->a(LX/7gR;)V

    .line 1660811
    invoke-static {p0}, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->k(Lcom/facebook/audience/sharesheet/app/SharesheetFragment;)V

    .line 1660812
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, v6, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 1660813
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 1660814
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f040015

    invoke-virtual {v0, v6, v1}, Landroid/support/v4/app/FragmentActivity;->overridePendingTransition(II)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 13
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1660785
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1660786
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;

    invoke-static {p1}, LX/7hN;->b(LX/0QB;)LX/7hN;

    move-result-object v3

    check-cast v3, LX/7hN;

    invoke-static {p1}, LX/0fO;->b(LX/0QB;)LX/0fO;

    move-result-object v4

    check-cast v4, LX/0fO;

    invoke-static {p1}, LX/7gT;->a(LX/0QB;)LX/7gT;

    move-result-object v5

    check-cast v5, LX/7gT;

    const-class v6, LX/AJR;

    invoke-interface {p1, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/AJR;

    const-class v7, LX/AJI;

    invoke-interface {p1, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/AJI;

    const-class v8, LX/AJT;

    invoke-interface {p1, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/AJT;

    invoke-static {p1}, LX/8RJ;->a(LX/0QB;)LX/8RJ;

    move-result-object v9

    check-cast v9, LX/8RJ;

    const-class v10, LX/AJ4;

    invoke-interface {p1, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/AJ4;

    invoke-static {p1}, LX/AJK;->a(LX/0QB;)LX/AJK;

    move-result-object v11

    check-cast v11, LX/AJK;

    const-class v12, LX/AIn;

    invoke-interface {p1, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/AIn;

    const-class v0, LX/AJu;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/AJu;

    iput-object v3, v2, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->b:LX/7hN;

    iput-object v4, v2, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->c:LX/0fO;

    iput-object v5, v2, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->d:LX/7gT;

    iput-object v6, v2, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->e:LX/AJR;

    iput-object v7, v2, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->f:LX/AJI;

    iput-object v8, v2, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->g:LX/AJT;

    iput-object v9, v2, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->h:LX/8RJ;

    iput-object v10, v2, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->i:LX/AJ4;

    iput-object v11, v2, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->j:LX/AJK;

    iput-object v12, v2, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->k:LX/AIn;

    iput-object p1, v2, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->l:LX/AJu;

    .line 1660787
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x64ef4ccf

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1660784
    const v1, 0x7f031313

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x7becb967

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x2f427db1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1660781
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1660782
    iget-object v1, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->s:LX/AJH;

    invoke-virtual {v1}, LX/AJH;->d()V

    .line 1660783
    const/16 v1, 0x2b

    const v2, -0x1e006416

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 1660765
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1660766
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->s:LX/AJH;

    .line 1660767
    iget-object v1, v0, LX/AJH;->i:Ljava/lang/String;

    move-object v0, v1

    .line 1660768
    iget-object v1, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->u:LX/AJZ;

    iget-object v2, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->r:LX/AJQ;

    invoke-virtual {v2}, LX/AJQ;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->t:LX/AJS;

    .line 1660769
    iget-object p0, v3, LX/AJS;->f:Ljava/lang/String;

    move-object v3, p0

    .line 1660770
    const-string v4, "newsfeed_selected"

    .line 1660771
    iget-boolean v5, v1, LX/AJZ;->e:Z

    move v5, v5

    .line 1660772
    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1660773
    const-string v4, "my_day_selected"

    .line 1660774
    iget-boolean v5, v1, LX/AJZ;->f:Z

    move v5, v5

    .line 1660775
    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1660776
    const-string v4, "selected_audience"

    new-instance v5, Ljava/util/ArrayList;

    invoke-virtual {v1}, LX/AJZ;->e()LX/0Px;

    move-result-object p0

    invoke-direct {v5, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1660777
    const-string v4, "search_query"

    invoke-virtual {p1, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1660778
    const-string v4, "selectable_privacy_data"

    invoke-virtual {p1, v4, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1660779
    const-string v4, "session_id"

    invoke-virtual {p1, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1660780
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 10
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1660740
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1660741
    const v0, 0x7f0d2c4e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->n:Landroid/support/v7/widget/RecyclerView;

    .line 1660742
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->n:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, LX/1P1;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1P1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1660743
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->n:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, LX/AJB;

    invoke-direct {v1, p0}, LX/AJB;-><init>(Lcom/facebook/audience/sharesheet/app/SharesheetFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1660744
    new-instance v0, LX/AJZ;

    iget-object v1, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->d:LX/7gT;

    invoke-direct {v0, v1}, LX/AJZ;-><init>(LX/7gT;)V

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->u:LX/AJZ;

    .line 1660745
    new-instance v1, LX/AIu;

    iget-object v0, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->i:LX/AJ4;

    sget-object v2, LX/AJ2;->NEWSFEED:LX/AJ2;

    iget-object v3, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->v:LX/AJ8;

    invoke-virtual {v0, v2, v3}, LX/AJ4;->a(LX/AJ2;LX/AJ8;)LX/AJ3;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->c:LX/0fO;

    invoke-virtual {v0}, LX/0fO;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->i:LX/AJ4;

    sget-object v3, LX/AJ2;->MY_DAY:LX/AJ2;

    iget-object v4, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->v:LX/AJ8;

    invoke-virtual {v0, v3, v4}, LX/AJ4;->a(LX/AJ2;LX/AJ8;)LX/AJ3;

    move-result-object v0

    :goto_0
    iget-object v3, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->k:LX/AIn;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->v:LX/AJ8;

    .line 1660746
    new-instance v7, LX/AIm;

    const-class v6, LX/AJ0;

    invoke-interface {v3, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/AJ0;

    invoke-direct {v7, v4, v5, v6}, LX/AIm;-><init>(ZLX/AJ8;LX/AJ0;)V

    .line 1660747
    move-object v3, v7

    .line 1660748
    invoke-direct {v1, v2, v0, v3}, LX/AIu;-><init>(LX/AJ3;LX/AJ3;LX/AIm;)V

    iput-object v1, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->o:LX/AIu;

    .line 1660749
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->n:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->o:LX/AIu;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1660750
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->f:LX/AJI;

    iget-object v1, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->o:LX/AIu;

    .line 1660751
    new-instance v2, LX/AJH;

    invoke-static {v0}, LX/AJX;->a(LX/0QB;)LX/AJX;

    move-result-object v4

    check-cast v4, LX/AJX;

    invoke-static {v0}, LX/BMr;->a(LX/0QB;)LX/BMr;

    move-result-object v5

    check-cast v5, LX/BMr;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-static {v0}, LX/7gT;->a(LX/0QB;)LX/7gT;

    move-result-object v7

    check-cast v7, LX/7gT;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v8

    check-cast v8, LX/0aG;

    invoke-static {v0}, LX/0hg;->a(LX/0QB;)LX/0hg;

    move-result-object v9

    check-cast v9, LX/0hg;

    move-object v3, v1

    invoke-direct/range {v2 .. v9}, LX/AJH;-><init>(LX/AIu;LX/AJX;LX/BMr;LX/1Ck;LX/7gT;LX/0aG;LX/0hg;)V

    .line 1660752
    move-object v0, v2

    .line 1660753
    iput-object v0, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->s:LX/AJH;

    .line 1660754
    const v0, 0x7f0d2c4d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->m:Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;

    .line 1660755
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->m:Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;

    iget-object v1, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->s:LX/AJH;

    .line 1660756
    iget-object v2, v1, LX/AJH;->j:Landroid/text/TextWatcher;

    move-object v1, v2

    .line 1660757
    invoke-virtual {v0, v1}, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->a(Landroid/text/TextWatcher;)V

    .line 1660758
    const v0, 0x7f0d2c4f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->p:Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;

    .line 1660759
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->l:LX/AJu;

    iget-object v1, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->u:LX/AJZ;

    invoke-virtual {v0, v1}, LX/AJu;->a(LX/AJZ;)LX/AJt;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->q:LX/AJt;

    .line 1660760
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->p:Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;

    iget-object v1, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->q:LX/AJt;

    iget-object v2, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->w:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/audience/sharesheet/ui/postbar/SharesheetPostBar;->a(LX/AJt;Landroid/view/View$OnClickListener;)V

    .line 1660761
    invoke-direct {p0, p2}, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->b(Landroid/os/Bundle;)V

    .line 1660762
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/app/SharesheetFragment;->d:LX/7gT;

    sget-object v1, LX/7gR;->ENTER_SHARE_SHEET:LX/7gR;

    invoke-virtual {v0, v1}, LX/7gT;->a(LX/7gR;)V

    .line 1660763
    return-void

    .line 1660764
    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
