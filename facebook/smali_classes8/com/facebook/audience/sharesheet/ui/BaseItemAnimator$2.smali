.class public final Lcom/facebook/audience/sharesheet/ui/BaseItemAnimator$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/AJl;


# direct methods
.method public constructor <init>(LX/AJl;)V
    .locals 0

    .prologue
    .line 1661825
    iput-object p1, p0, Lcom/facebook/audience/sharesheet/ui/BaseItemAnimator$2;->a:LX/AJl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    .line 1661826
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/BaseItemAnimator$2;->a:LX/AJl;

    iget-object v0, v0, LX/AJl;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/BaseItemAnimator$2;->a:LX/AJl;

    iget-object v0, v0, LX/AJl;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 1661827
    iget-object v3, p0, Lcom/facebook/audience/sharesheet/ui/BaseItemAnimator$2;->a:LX/AJl;

    .line 1661828
    iget-object v4, v0, LX/1a1;->a:Landroid/view/View;

    .line 1661829
    invoke-static {v4}, LX/0vv;->v(Landroid/view/View;)LX/3sU;

    move-result-object v4

    .line 1661830
    iget-object v5, v3, LX/AJl;->f:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1661831
    invoke-virtual {v3, v4}, LX/AJl;->a(LX/3sU;)V

    .line 1661832
    iget-wide v8, v3, LX/1Of;->c:J

    move-wide v6, v8

    .line 1661833
    invoke-virtual {v4, v6, v7}, LX/3sU;->a(J)LX/3sU;

    move-result-object v5

    new-instance v6, LX/AJi;

    invoke-direct {v6, v3, v0, v4}, LX/AJi;-><init>(LX/AJl;LX/1a1;LX/3sU;)V

    invoke-virtual {v5, v6}, LX/3sU;->a(LX/3oQ;)LX/3sU;

    move-result-object v4

    invoke-virtual {v4}, LX/3sU;->b()V

    .line 1661834
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1661835
    :cond_0
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/BaseItemAnimator$2;->a:LX/AJl;

    iget-object v0, v0, LX/AJl;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1661836
    return-void
.end method
