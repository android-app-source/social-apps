.class public Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Landroid/view/inputmethod/InputMethodManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/7gT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Lcom/facebook/resources/ui/FbEditText;

.field public d:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

.field private e:Landroid/text/TextWatcher;

.field public f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1662103
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1662104
    invoke-direct {p0}, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->d()V

    .line 1662105
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1662106
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1662107
    invoke-direct {p0}, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->d()V

    .line 1662108
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1662109
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1662110
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->f:Z

    .line 1662111
    invoke-direct {p0}, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->d()V

    .line 1662112
    return-void
.end method

.method private static a(Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;Landroid/view/inputmethod/InputMethodManager;LX/7gT;)V
    .locals 0

    .prologue
    .line 1662113
    iput-object p1, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->a:Landroid/view/inputmethod/InputMethodManager;

    iput-object p2, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->b:LX/7gT;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;

    invoke-static {v1}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-static {v1}, LX/7gT;->a(LX/0QB;)LX/7gT;

    move-result-object v1

    check-cast v1, LX/7gT;

    invoke-static {p0, v0, v1}, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->a(Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;Landroid/view/inputmethod/InputMethodManager;LX/7gT;)V

    return-void
.end method

.method private d()V
    .locals 1

    .prologue
    .line 1662091
    const-class v0, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;

    invoke-static {v0, p0}, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1662092
    const v0, 0x7f03131b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1662093
    return-void
.end method

.method public static e(Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;)V
    .locals 4

    .prologue
    .line 1662077
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->f:Z

    .line 1662078
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->setVisibility(I)V

    .line 1662079
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->d:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iget-object v1, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->c:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setCustomTitleView(Landroid/view/View;)V

    .line 1662080
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->d:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00d5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1662081
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->d:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-virtual {p0}, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setUpButtonColor(I)V

    .line 1662082
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->c:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->requestFocus()Z

    .line 1662083
    invoke-direct {p0}, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->g()V

    .line 1662084
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->b:LX/7gT;

    sget-object v1, LX/7gR;->ENTER_SEARCH_FRIENDS:LX/7gR;

    invoke-virtual {v0, v1}, LX/7gT;->a(LX/7gR;)V

    .line 1662085
    return-void
.end method

.method private f()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1662094
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->c:Lcom/facebook/resources/ui/FbEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1662095
    iput-boolean v2, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->f:Z

    .line 1662096
    invoke-virtual {p0, v2}, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->setVisibility(I)V

    .line 1662097
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->d:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setCustomTitleView(Landroid/view/View;)V

    .line 1662098
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->d:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00d1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1662099
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->d:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-virtual {p0}, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setUpButtonColor(I)V

    .line 1662100
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->c:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->clearFocus()V

    .line 1662101
    invoke-direct {p0}, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->h()V

    .line 1662102
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1662086
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->a:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0, p0, v1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1662087
    invoke-direct {p0}, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->h()V

    .line 1662088
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->a:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0, v1, v1}, Landroid/view/inputmethod/InputMethodManager;->toggleSoftInput(II)V

    .line 1662089
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->a:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0, p0, v1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 1662090
    :cond_0
    return-void
.end method

.method private h()V
    .locals 3

    .prologue
    .line 1662075
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->a:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p0}, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1662076
    return-void
.end method


# virtual methods
.method public final a(Landroid/text/TextWatcher;)V
    .locals 4

    .prologue
    .line 1662070
    iput-object p1, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->e:Landroid/text/TextWatcher;

    .line 1662071
    invoke-virtual {p0}, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03131c

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->c:Lcom/facebook/resources/ui/FbEditText;

    .line 1662072
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->c:Lcom/facebook/resources/ui/FbEditText;

    iget-object v1, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->e:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1662073
    new-instance v0, LX/AJm;

    invoke-direct {v0, p0}, LX/AJm;-><init>(Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;)V

    invoke-virtual {p0, v0}, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1662074
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1662067
    invoke-static {p0}, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->e(Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;)V

    .line 1662068
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->c:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1662069
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1662063
    iget-boolean v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->f:Z

    if-eqz v0, :cond_0

    .line 1662064
    invoke-direct {p0}, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->f()V

    .line 1662065
    const/4 v0, 0x1

    .line 1662066
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1662060
    iget-boolean v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->f:Z

    if-eqz v0, :cond_0

    .line 1662061
    invoke-direct {p0}, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->f()V

    .line 1662062
    :cond_0
    return-void
.end method

.method public setSearchText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1662058
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->c:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1662059
    return-void
.end method

.method public setTitleBar(Lcom/facebook/ui/titlebar/Fb4aTitleBar;)V
    .locals 0

    .prologue
    .line 1662056
    iput-object p1, p0, Lcom/facebook/audience/sharesheet/ui/SharesheetSearchBar;->d:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 1662057
    return-void
.end method
