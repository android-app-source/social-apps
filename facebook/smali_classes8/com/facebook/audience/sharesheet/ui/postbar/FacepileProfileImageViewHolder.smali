.class public Lcom/facebook/audience/sharesheet/ui/postbar/FacepileProfileImageViewHolder;
.super LX/1a1;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final m:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1662152
    const-class v0, Lcom/facebook/audience/sharesheet/ui/postbar/FacepileProfileImageViewHolder;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/audience/sharesheet/ui/postbar/FacepileProfileImageViewHolder;->m:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1662153
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 1662154
    const v0, 0x7f0d2c49

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/FacepileProfileImageViewHolder;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1662155
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 1662156
    iget-object v0, p0, Lcom/facebook/audience/sharesheet/ui/postbar/FacepileProfileImageViewHolder;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/audience/sharesheet/ui/postbar/FacepileProfileImageViewHolder;->m:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1662157
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getScaleX()F

    move-result v0

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getScaleY()F

    move-result v0

    cmpl-float v0, v0, v3

    if-nez v0, :cond_1

    .line 1662158
    :cond_0
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setScaleX(F)V

    .line 1662159
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setScaleY(F)V

    .line 1662160
    :cond_1
    return-void
.end method
